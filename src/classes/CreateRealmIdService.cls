public with sharing class CreateRealmIdService {
  
  public void processTrigger( List<Opportunity> newOpportunities, Map<Id, Opportunity> OldOpps )
  {
    List<Opportunity> filteredOpportunities = filterOpportunitiesOnUpdate(newOpportunities, oldOpps);
    if(!filteredOpportunities.isEmpty())
    {
      processRealmIds(filteredOpportunities, newOpportunities);
    }
  }

  public static void processRealmIds( List<Opportunity> filteredOpportunities, List<Opportunity>newOpportunities )
  {
    List<RealmID__c> InsertRealms = new List<RealmID__c>();
  
    
    for( Opportunity o : filteredOpportunities)
    {
      o.Realm_Id_Created__c = true;
      
      InsertRealms.add(createRealm(o));
    }
    
    insert InsertRealms;
    
  }
  
  public static List<Opportunity> filterOpportunitiesOnUpdate( List<Opportunity> newOpportunities, Map<Id, Opportunity> OldOpps )
  {
    List<Opportunity>filteredOpportunities = new List<Opportunity>();
    
    for(Opportunity o : newOpportunities)
    {
      Opportunity oldOpp = OldOpps.get(o.Id);
      if(o.RealmId__c != null && o.Realm_Id_Created__c == false)
      {
        filteredOpportunities.add(o);
      }
    }
    return filteredOpportunities;
  }
  
  public static RealmID__c createRealm (Opportunity o)
  {
    RealmID__c newRealm = new RealmID__c();
    newRealm.Name = o.RealmId__c;
    newRealm.Realm_ID_External_ID__c = o.RealmId__c;
    newRealm.Account__c = o.AccountId;
    newRealm.Opportunity__c = o.Id;
    return newRealm;
  }
}