@isTest
public class CustomerProductDetailTerminationBtchTest {
	
	public static final String ACTIVE_OPTY_STAGE_CLOSED_WON = 'Finance Approved';
	public static final String ACTIVE_OPTY_STAGE_OPEN = 'Obtaining Commitment';
	public static final String ACTIVE_OPTY_TYPE = CustomerProductDetailTerminationBatch.VALID_OPTY_TYPE[0];
	
	public static Account testAccount;
	public static Opportunity testOpty;
	public static Product2 testProduct;
	public static OpportunityLineItem testOlis;

	public static void setup()
	{
		RecordType actRecordType = [ SELECT Id FROM RecordType WHERE SobjectType = 'Account' AND isActive = true LIMIT 1];
		testAccount = TestUtils.createAccount( actRecordType.Id, true );
		
		RecordType optyRecordType = [ SELECT Id FROM RecordType WHERE SobjectType = 'Opportunity' AND isActive = true LIMIT 1];
		testOpty = TestUtils.createOpportunity( optyRecordType.Id, testAccount.Id, true );
		
		testOlis = TestUtils.createOpportunityLineItems( testOpty.Id, test.getStandardPricebookId(), 1, false )[0];
		testOlis.End_Date__c = Date.today().addDays( -1 );
		
		testProduct = TestUtils.createProduct( true );
	}
	
	public static testMethod void terminateCustomerProductDetail_cpd_validOLI_noMatch()
	{
		setup();
		
		Customer_Product_Detail__c cpd = new Customer_Product_Detail__c();
		cpd.Product_Status__c = 'Active';
		cpd.Renewal_Date__c = Date.today().addDays( -5 );
		cpd.Product2Id__c = testProduct.Id;
		cpd.Account__c = testAccount.Id;
		cpd.Last_12_Months__c = 1;
		insert cpd;
		
		Test.startTest();
			CustomerProductDetailTerminationBatch batch = new CustomerProductDetailTerminationBatch();
			Database.executeBatch( batch );
		Test.stopTest();
		
		List<Customer_Product_Detail__c> activeCPDs = [ SELECT Id FROM Customer_Product_Detail__c WHERE Product_Status__c = 'Active' OR Last_12_Months__c != 0 OR Renewal_Date__c != null ];
		System.assert( activeCPDs.isEmpty(), 'We expect no Active Product Portfolios');
	}

	public static testMethod void terminateCustomerProductDetail_cpd_validOLI_match_closedWon()
	{
		setup();
		
		testOpty.StageName = ACTIVE_OPTY_STAGE_CLOSED_WON;
		testOpty.Type = ACTIVE_OPTY_TYPE;
		update testOpty;
		
		PricebookEntry pbe = TestUtils.createPricebookEntry( testProduct.Id, test.getStandardPricebookId(), true);
		OpportunityLineItem testOli = TestUtils.createOpportunityLineItems( testOpty.Id, new List<PricebookEntry>{ pbe }, false )[0];
		testOli.Start_Date__c = system.today().addDays(-370);
		testOli.End_Date__c = Date.today().addDays( -5 );
		insert testOli;
		
		Customer_Product_Detail__c cpd = new Customer_Product_Detail__c();
		cpd.Product_Status__c = 'Active';
		cpd.Renewal_Date__c = testOli.End_Date__c.addDays( -1 );
		cpd.Product2Id__c = testProduct.Id;
		cpd.Account__c = testAccount.Id;
		cpd.Last_12_Months__c = 1;
		insert cpd;
		
		System.debug( [ Select Id, Product_Status__c, Product2Id__c, Renewal_Date__c FROM Customer_Product_Detail__c ]);
		System.debug( CustomerProductDetailTerminationBatch.getValidatedOLIs( new Set<Id>{ testAccount.Id } ) );
		
		Test.startTest();
			CustomerProductDetailTerminationBatch batch = new CustomerProductDetailTerminationBatch();
			Database.executeBatch( batch );
		Test.stopTest();
		
		List<Customer_Product_Detail__c> activeCPDs = [ SELECT Id FROM Customer_Product_Detail__c WHERE Product_Status__c = 'Active' ];
		System.assertEquals( false, activeCPDs.isEmpty(), 'We expect Active Product Portfolios');
	}

	public static testMethod void terminateCustomerProductDetail_cpd_validOLI_match_open()
	{
		setup();
		
		PricebookEntry pbe = TestUtils.createPricebookEntry( testProduct.Id, test.getStandardPricebookId(), true);
		OpportunityLineItem testOli = TestUtils.createOpportunityLineItems( testOpty.Id, new List<PricebookEntry>{ pbe }, false )[0];
		testOli.Start_Date__c = system.today().addDays(-370);
		testOli.End_Date__c = Date.today().addDays( -5 );
		insert testOli;

		testOpty.StageName = ACTIVE_OPTY_STAGE_OPEN;
		testOpty.Type = ACTIVE_OPTY_TYPE;
		update testOpty;
		
		Customer_Product_Detail__c cpd = new Customer_Product_Detail__c();
		cpd.Product_Status__c = 'Active';
		cpd.Renewal_Date__c = testOli.End_Date__c.addDays( -1 );
		cpd.Product2Id__c = testProduct.Id;
		cpd.Account__c = testAccount.Id;
		cpd.Last_12_Months__c = 1;
		insert cpd;
		
		System.debug( [ Select Id, Product_Status__c, Product2Id__c, Renewal_Date__c FROM Customer_Product_Detail__c ]);
		System.debug( CustomerProductDetailTerminationBatch.getValidatedOLIs( new Set<Id>{ testAccount.Id } ) );
		
		Test.startTest();
			CustomerProductDetailTerminationBatch batch = new CustomerProductDetailTerminationBatch();
			Database.executeBatch( batch );
		Test.stopTest();
		
		List<Customer_Product_Detail__c> activeCPDs = [ SELECT Id FROM Customer_Product_Detail__c WHERE Product_Status__c = 'Active' OR Last_12_Months__c != 0 OR Renewal_Date__c != null ];
		System.assertEquals( false, activeCPDs.isEmpty(), 'We expect Active Product Portfolios');
	}
	
	public static testMethod void terminateCustomerProductDetail_cpd_validOLI_endDateLessThenRenewalDate()
	{
		setup();
		
		PricebookEntry pbe = TestUtils.createPricebookEntry( testProduct.Id, test.getStandardPricebookId(), true);
		OpportunityLineItem testOli = TestUtils.createOpportunityLineItems( testOpty.Id, new List<PricebookEntry>{ pbe }, false )[0];
		testOli.Start_Date__c = system.today().addDays(-370);
		testOli.End_Date__c = Date.today().addDays( -5 );
		insert testOli;

		testOpty.StageName = ACTIVE_OPTY_STAGE_OPEN;
		testOpty.Type = ACTIVE_OPTY_TYPE;
		update testOpty;
		
		Customer_Product_Detail__c cpd = new Customer_Product_Detail__c();
		cpd.Product_Status__c = 'Active';
		cpd.Renewal_Date__c = testOli.End_Date__c.addDays( +1 );
		cpd.Product2Id__c = testProduct.Id;
		cpd.Account__c = testAccount.Id;
		cpd.Last_12_Months__c = 1;
		insert cpd;
		
		System.debug( [ Select Id, Product_Status__c, Product2Id__c, Renewal_Date__c FROM Customer_Product_Detail__c ]);
		System.debug( CustomerProductDetailTerminationBatch.getValidatedOLIs( new Set<Id>{ testAccount.Id } ) );
		
		Test.startTest();
			CustomerProductDetailTerminationBatch batch = new CustomerProductDetailTerminationBatch();
			Database.executeBatch( batch );
		Test.stopTest();
		
		List<Customer_Product_Detail__c> activeCPDs = [ SELECT Id FROM Customer_Product_Detail__c WHERE Product_Status__c = 'Active' OR Last_12_Months__c != 0 OR Renewal_Date__c != null ];
		System.assert( activeCPDs.isEmpty(), 'We expect No Active Product Portfolios');
	}
	
	public static testMethod void terminateCustomerProductDetail_cpd_validOLI_toolsChildProduct()
	{
		setup();
		
		PricebookEntry pbe = TestUtils.createPricebookEntry( testProduct.Id, test.getStandardPricebookId(), true);
		OpportunityLineItem testOli = TestUtils.createOpportunityLineItems( testOpty.Id, new List<PricebookEntry>{ pbe }, false )[0];
		testOli.Start_Date__c = system.today().addDays(-370);
		testOli.End_Date__c = Date.today().addDays( -5 );
		insert testOli;

		testOpty.StageName = ACTIVE_OPTY_STAGE_OPEN;
		testOpty.Type = ACTIVE_OPTY_TYPE;
		update testOpty;
		
		Customer_Product_Detail__c cpd = new Customer_Product_Detail__c();
		cpd.Name = CustomerProductDetailTerminationBatch.NAME_TO_EXCLUDE;
		cpd.Product_Status__c = 'Active';
		cpd.Renewal_Date__c = testOli.End_Date__c.addDays( +1 );
		cpd.Product2Id__c = testProduct.Id;
		cpd.Account__c = testAccount.Id;
		cpd.Last_12_Months__c = 1;
		insert cpd;
		
		System.debug( [ Select Id, Product_Status__c, Product2Id__c, Renewal_Date__c FROM Customer_Product_Detail__c ]);
		System.debug( CustomerProductDetailTerminationBatch.getValidatedOLIs( new Set<Id>{ testAccount.Id } ) );
		
		Test.startTest();
			CustomerProductDetailTerminationBatch batch = new CustomerProductDetailTerminationBatch();
			Database.executeBatch( batch );
		Test.stopTest();
		
		List<Customer_Product_Detail__c> activeCPDs = [ SELECT Id FROM Customer_Product_Detail__c WHERE Product_Status__c = 'Active' OR Last_12_Months__c != 0 OR Renewal_Date__c != null ];
		System.assertEquals( false, activeCPDs.isEmpty(), 'We expect active Product Portfolios because the Product Name is ' + CustomerProductDetailTerminationBatch.NAME_TO_EXCLUDE);
	}
	
	public static testMethod void terminateCustomerProductDetail_cpd_validOLI_nullAVC()
	{
		setup();
		
		PricebookEntry pbe = TestUtils.createPricebookEntry( testProduct.Id, test.getStandardPricebookId(), true);
		OpportunityLineItem testOli = TestUtils.createOpportunityLineItems( testOpty.Id, new List<PricebookEntry>{ pbe }, false )[0];
		testOli.Start_Date__c = system.today().addDays(-370);
		testOli.End_Date__c = Date.today().addDays( -5 );
		insert testOli;

		testOpty.StageName = ACTIVE_OPTY_STAGE_OPEN;
		testOpty.Type = ACTIVE_OPTY_TYPE;
		update testOpty;
		
		Customer_Product_Detail__c cpd = new Customer_Product_Detail__c();
		cpd.Product_Status__c = 'Active';
		cpd.Renewal_Date__c = testOli.End_Date__c.addDays( +1 );
		cpd.Product2Id__c = testProduct.Id;
		cpd.Account__c = testAccount.Id;
		insert cpd;
		
		System.debug( [ Select Id, Product_Status__c, Product2Id__c, Renewal_Date__c FROM Customer_Product_Detail__c ]);
		System.debug( CustomerProductDetailTerminationBatch.getValidatedOLIs( new Set<Id>{ testAccount.Id } ) );
		
		Test.startTest();
			CustomerProductDetailTerminationBatch batch = new CustomerProductDetailTerminationBatch();
			Database.executeBatch( batch );
		Test.stopTest();
		
		List<Customer_Product_Detail__c> activeCPDs = [ SELECT Id FROM Customer_Product_Detail__c WHERE Product_Status__c = 'Active' OR Last_12_Months__c != 0 OR Renewal_Date__c != null ];
		System.assertEquals( false, activeCPDs.isEmpty(), 'We expect active Product Portfolios because the AVC is set to null');
	}
}