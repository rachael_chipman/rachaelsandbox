@isTest

public class RPCstartdate_Test {
     static testMethod void RPCstartdate_Test() {

Account acct = new Account();
        acct.name = 'RPTestAcct1';
        acct.accountmanager__c = 'BIO Automation';
        Insert acct;
        
Contact con = new Contact();
        con.AccountId = acct.Id;
        con.LastName = 'Test';
        con.FirstName = 'Trigger';
        con.ContactsRole__c = 'Certification Daily Report Recipient';
        con.Email = 'test@returnpath.com';
        Insert Con;
        
        acct.SSC_Start_Date__c = system.today();
        update acct;
        }
        }