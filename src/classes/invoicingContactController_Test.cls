@ isTest (SeeAllData = true)
Public class invoicingContactController_Test{
    /*public static testMethod void testinvoicingContactController(){
    PageReference pageRef = Page.InvoicingContactInfo;
    Test.setCurrentPageReference(pageRef);

   Account acct = new Account();
        acct.name = 'RPTestAcct1';
        acct.accountmanager__c = 'BIO Automation';
        acct.Customer_Relationship_Manager_Primary__c = 'BIO Automation';
        acct.Invoicing_Contact__c = null;
        acct.c2g__CODAInvoiceEmail__c = null;
        Insert acct;
        
   Contact con = new Contact();
       con.FirstName = 'Test';
       con.LastName = 'Contact';
       con.AccountId = acct.Id;
       con.Email = 'test@returnpath.com';
       Insert con; 
       
       Contact con2 = new Contact();
       con2.FirstName = 'Test';
       con2.LastName = 'Contact';
       con2.AccountId = acct.Id;
       con2.Email = 'test@returnpath.com';
       Insert con2; 
       
       acct.Invoicing_Contact__c = con2.Id;
       update acct;
        
   Opportunity opp = new Opportunity();
       opp.AccountId = acct.Id;
       opp.Name = 'Test Opp';
       opp.CloseDate = system.today();
       opp.Type = 'New Business';
       opp.StageName = 'Signature';
       opp.CurrencyISOCode = 'USD';
       insert opp;

       
       
ApexPages.StandardController sc = new ApexPages.standardController(opp);

System.currentPagereference().getParameters().put('id',opp.Id);
invoicingContactController oPEE = new invoicingContactController(sc);   

oPEE.ShowSection();
oPEE.getHideshow();
oPEE.getHideshow2();
oPEE.getEmail();
oPEE.conUpdate2();
oPEE.addContact();

}

public static testMethod void testinvoicingContactController2(){

    PageReference pageRef = Page.InvoicingContactInfo;
Test.setCurrentPageReference(pageRef);

   Account acct = new Account();
        acct.name = 'RPTestAcct1';
        acct.accountmanager__c = 'BIO Automation';
        acct.Customer_Relationship_Manager_Primary__c = 'BIO Automation';
        acct.Invoicing_Contact__c = null;
        acct.c2g__CODAInvoiceEmail__c = null;
        Insert acct;
        
   Contact con = new Contact();
       con.FirstName = 'Test';
       con.LastName = 'Contact';
       con.AccountId = acct.Id;
       con.Email = 'test@returnpath.com';
       Insert con; 
       
       Contact con2 = new Contact();
       con2.FirstName = 'Test';
       con2.LastName = 'Contact2';
       con2.AccountId = acct.Id;
       con2.Email = 'test@returnpath.com';
       Insert con2; 
       
       acct.Invoicing_Contact__c = con2.Id;
       update acct;
        
   Opportunity opp = new Opportunity();
       opp.AccountId = acct.Id;
       opp.Name = 'Test Opp';
       opp.CloseDate = system.today();
       opp.Type = 'New Business';
       opp.StageName = 'Signature';
       opp.CurrencyISOCode = 'USD';
       insert opp;

       
System.currentPagereference().getParameters().put('id',opp.Id);     
ApexPages.StandardController sc = new ApexPages.standardController(opp);
invoicingContactController oPEE = new invoicingContactController(sc);   

oPEE.newCon.LastName = 'test lastname';

oPEE.ShowSection();
oPEE.getHideshow();
oPEE.getHideshow2();
oPEE.getEmail();
oPEE.conSave();


}
public static testMethod void testinvoicingContactController3(){

    PageReference pageRef = Page.InvoicingContactInfo;
Test.setCurrentPageReference(pageRef);

   Account acct = new Account();
        acct.name = 'RPTestAcct1';
        acct.accountmanager__c = 'BIO Automation';
        acct.Customer_Relationship_Manager_Primary__c = 'BIO Automation';
        acct.Invoicing_Contact__c = null;
        acct.c2g__CODAInvoiceEmail__c = null;
        Insert acct;
        
   Contact con = new Contact();
       con.FirstName = 'Test';
       con.LastName = 'Contact';
       con.AccountId = acct.Id;
       con.Email = 'test@returnpath.com';
       Insert con; 
       
       Contact con2 = new Contact();
       con2.FirstName = 'Test';
       con2.LastName = 'Contact2';
       con2.AccountId = acct.Id;
       con2.Email = 'test@returnpath.com';
       Insert con2; 
       
       acct.Invoicing_Contact__c = con2.Id;
       update acct;
        
   Opportunity opp = new Opportunity();
       opp.AccountId = acct.Id;
       opp.Name = 'Test Opp';
       opp.CloseDate = system.today();
       opp.Type = 'New Business';
       opp.StageName = 'Signature';
       opp.CurrencyISOCode = 'USD';
       insert opp;

       
System.currentPagereference().getParameters().put('id',opp.Id);     
ApexPages.StandardController sc = new ApexPages.standardController(opp);
invoicingContactController oPEE = new invoicingContactController(sc);   

oPEE.newCon.LastName = 'test lastname';

oPEE.ShowSection();
oPEE.getHideshow();
oPEE.getHideshow2();
oPEE.getEmail();
oPEE.conSave2();


}

public static testMethod void testinvoicingContactController4(){

    PageReference pageRef = Page.InvoicingContactInfo;
Test.setCurrentPageReference(pageRef);

   Account acct = new Account();
        acct.name = 'RPTestAcct1';
        acct.accountmanager__c = 'BIO Automation';
        acct.Customer_Relationship_Manager_Primary__c = 'BIO Automation';
        acct.Invoicing_Contact__c = null;
        acct.c2g__CODAInvoiceEmail__c = null;
        Insert acct;
        
   Contact con = new Contact();
       con.FirstName = 'Test';
       con.LastName = 'Contact';
       con.AccountId = acct.Id;
       con.Email = 'test@returnpath.com';
       Insert con; 
       
       Contact con2 = new Contact();
       con2.FirstName = 'Test';
       con2.LastName = 'Contact2';
       con2.AccountId = acct.Id;
       con2.Email = 'test@returnpath.com';
       Insert con2; 
       
       acct.Invoicing_Contact__c = con2.Id;
       update acct;
        
   Opportunity opp = new Opportunity();
       opp.AccountId = acct.Id;
       opp.Name = 'Test Opp';
       opp.CloseDate = system.today();
       opp.Type = 'New Business';
       opp.StageName = 'Signature';
       opp.CurrencyISOCode = 'USD';
       insert opp;

       
System.currentPagereference().getParameters().put('id',opp.Id);     
ApexPages.StandardController sc = new ApexPages.standardController(opp);
invoicingContactController oPEE = new invoicingContactController(sc);   

oPEE.newCon.LastName = 'test lastname';

oPEE.ShowSection();
oPEE.getHideshow();
oPEE.getHideshow2();
oPEE.getEmail();
oPEE.conUpdate3();


}*/
}