public class OpportunityAcceptExtension 
{
    private final Opportunity Opp;
    private PageReference pageref;
    public Boolean canAccept = false;
    public static String currentUserProfile
    {
        get
        {
            if(currentUserProfile == NULL)
            {
                Id profileId = UserInfo.getProfileId();
                currentUserProfile = [SELECT Id, Name FROM Profile WHERE Id = :profileId].Name;
            }
            return currentUserProfile;
        }
        private set;
    }

    public OpportunityAcceptExtension(ApexPages.StandardController stdController)
    {
        Opp = [SELECT Id, OwnerId FROM Opportunity WHERE Id = :ApexPages.currentPage().getParameters().get('Id')];
        pageref = stdController.view();

        if(UserInfo.getUserId() == Opp.OwnerId || currentUserProfile == 'Exec Admin' || currentUserProfile == 'Salesforce Super Users')
        {
          canAccept = true;
        }
        
    }
    
    public PageReference updateContact()
    {
        List<Contact> contactstoUpdate = new List<Contact>();
        List<Id> ContactId = new List<Id>();
        List<OpportunityContactRole> conroles = [Select Id,ContactId from OpportunityContactRole where OpportunityId =:Opp.Id];
        if(canAccept)
        {
            if(conroles.size() > 0)
            {
                for(OpportunityContactRole Tconrole : conroles)
                {
                    ContactId.add(Tconrole.ContactId);
                }
                List<Contact> contacts = [Select Id,name, Qualifying_Status__c from Contact where Id in:ContactId  ];
                if(contacts.size() > 0)
                {
                    for(Contact tcon: contacts)
                    {
                       tcon.Qualifying_Status__c = 'RSE Accept';
                       //tcon.Qualifying_Status__c =  getPicklistValue('Qualifying_Status__c', 'RSE Accept');
                       contactstoUpdate.add(tcon);
                    }
                }
            }
          
          
            //update the contacts 
            //add try catch
            update contactstoUpdate;
            
            //update the Opportunity
            updateOpportunity();
            return Pageref;
        }
        else
        {
            ApexPages.Message cannotAccept = new ApexPages.Message(ApexPages.Severity.ERROR, 'Only the Opportunity Owner or an Administrator can Acknowledge this Opportunity. Please use your browser\'s back button to return to the Opportunity.');
            ApexPages.addMessage(cannotAccept);
            return null;
        }
    }
    
    
    public void updateOpportunity()
    {
        List<Opportunity> OppsToUpdate = new List<Opportunity>();
        //add to the  query for the obj type opp
        List<RecordType> OppRecType = [Select  name, id from RecordType where name='Research' AND SObjectType = 'Opportunity'];
        List<Opportunity> TOpp = [Select Id,Opportunity_Accepted__c,RecordTypeId  from Opportunity where Id =:Opp.Id ];
        
        if(TOpp.size() > 0  && OppRecType.size() > 0)
        {
           Opportunity opp = Topp[0];
           opp.Opportunity_Accepted__c = System.now();
           opp.RecordTypeId = OppRecType[0].Id;
           opp.Record_Locked__c = false;
           OppsToUpdate.add(opp);
        }
        
        //add try cactch
        update OppsToUpdate; 
        
        
        
    }
    
  public string getPicklistValue(String fieldName, string selectedValue)
   {
      list<SelectOption> options = new list<SelectOption>();
      string selectedStatus;
      // Get the object type of the SObject.
      Schema.sObjectType objType = Contact.getSObjectType(); 
      // Describe the SObject using its object type.
      Schema.DescribeSObjectResult objDescribe = objType.getDescribe();       
      // Get a map of fields for the SObject
      map<String, Schema.SObjectField> fieldMap = objDescribe.fields.getMap(); 
      // Get the list of picklist values for this field.
      list<Schema.PicklistEntry> values =
         fieldMap.get(fieldName).getDescribe().getPickListValues();
      // Add these values to the selectoption list.
      for (Schema.PicklistEntry a : values)
      { 
          if(a.getLabel().equals(selectedValue))
          {
              selectedStatus = a.getValue();
              break;
          }
        
      }
      return selectedStatus;
   }
    
    
    

}