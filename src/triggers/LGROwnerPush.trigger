trigger LGROwnerPush on Lead(before Update) {
                  
    for(Lead x : Trigger.new) {
    
    //Back to LGR Owner Update - push in the Previous Lead Owner's Name (into Previous LGR User ID field) if there isn't already a Prev LGR listed
        if(x.Back_to_ISR_Owner_Update__c == true){
            if(x.Previous_ISR_User_ID__c == null){
                for(Lead y : Trigger.old){
                    if(y.Id == x.Id){
                        x.Previous_ISR_User_ID__c = y.OwnerId;
                        x.Back_to_ISR_Owner_Update__c = false;
                        break;
                    }
                }
            }
    //If there is a Prev LGR listed make them the Lead Owner and then if Qualifying for is blank then push the Original owner (FSR) into that field        
            else if(x.Previous_ISR_User_ID__c <> null){      
                String fieldsalesowner = x.OwnerId;
                x.OwnerId = x.Previous_ISR_User_ID__c;
                if(x.Previously_Sent_Back_to_Marketing__c == true){
                    x.Previous_ISR_User_ID__c = null;
                    x.Previously_Sent_Back_to_Marketing__c = false;
                }
                else if(x.Prospecting_For__c == null) x.Prospecting_For__c = fieldsalesowner;
                x.Back_to_ISR_Owner_Update__c = false;
                break;
            }
         }
    //Back to FSR Owner Update - If Qualifying for is blank and Status != Nurture then Owner becomes the Prev LGR, otherwise Owner for becomes Qualifying for     
         else if(x.Back_to_FSR_Owner_Update__c == true){
            if(x.Prospecting_For__c == null){
                for(Lead y : Trigger.old){
                    if(y.Id == x.Id){
                        if(x.Status <> 'Nurture') x.Previous_ISR_User_ID__c = y.OwnerId;
                        else x.Prospecting_For__c = y.OwnerId;
                        x.Back_to_FSR_Owner_Update__c = false;
                        x.Assigned__c = false;
                        break;
                    }
                }
            }
     //If Qualifying for != blank then they become the Owner and Qualifying for becomes blank, and the Previous Owner becomes the Prev LGR User Id       
            else if(x.Prospecting_For__c <> null){      
                String LGowner = x.OwnerId;
                x.OwnerId = x.Prospecting_For__c;
                x.Prospecting_For__c = null;
                x.Previous_ISR_User_ID__c = LGowner;
                x.Back_to_FSR_Owner_Update__c = false;
                break;
            }
        }
    }
    
}