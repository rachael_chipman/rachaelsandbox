@isTEST

Public Class RenewalCertInfoPassFail_test{

    /*private static testmethod void testUpdateRenewalCertInfoAppPassed1(){


User user = new user(Alias = 'standt', Email='tes@rp.com', 
      EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
      LocaleSidKey='en_US', ProfileId = '00e0000000734LT', 
      TimeZoneSidKey='America/Los_Angeles', UserName='test@rp.com.rp');
      insert user;    

Account a = new Account(
        OwnerId = user.Id,
        Name = 'Test Account',
        phone = '(303) 555-5555');
        insert a;
        
            Opportunity o = new Opportunity();
                o.OwnerId = user.Id;
                o.Name = 'Test1';
                o.AccountId = a.Id;
                o.CloseDate=system.today();
                o.StageName='Pending Certification Qualification';
                o.Approval_Type__c = 'Certification Approval';
                o.Pricebook2Id = '01s000000004NS6AAM';
                
                insert o;
        
            Opportunity oClone = new Opportunity();
                oClone.OwnerId = user.Id;
                oClone.Name = 'Test2';
                oClone.AccountId = a.Id;
                oClone.CloseDate=system.today();
                oClone.StageName='Pending Certification Qualification';
                oClone.Approval_Type__c = 'Certification Approval';
                oClone.Pricebook2Id = '01s000000004NS6AAM';
                oClone.Cloned_From_ID__c = o.id;
                
                insert oClone;
                
                    OpportunityLineItem oli = new OpportunityLineItem();
                    oli.OpportunityId = oClone.Id;
                    oli.Quantity = 1;
                    oli.UnitPrice = 100;
                    oli.Product_Status__c = 'Current';
                    oli.Description__c = 'Return Path Certification';
                    oli.Active__c = true;
                    oli.Recurring__c = false;
                    oli.Start_Date__c = system.today();
                    oli.End_Date__c = system.today();
                    oli.PricebookentryId = '01u00000000FGwSAAW';
                    
                    insert oli;
                
            Opportunity Renewal = new Opportunity();
                Renewal.OwnerId = user.Id;
                Renewal.Name = 'Test3';
                Renewal.AccountId = a.Id;
                Renewal.CloseDate=system.today();
                Renewal.StageName='Pending Certification Qualification';
                Renewal.Approval_Type__c = 'Certification Approval';
                Renewal.Previous_Opportunity_Lookup__c = o.id;
                Renewal.Pricebook2Id = '01s000000004NS6AAM';
                Renewal.Activation_Date__c = system.today();
                Renewal.Certification_Tier_Sold__c = 'No Certification Product';
                
                insert Renewal;
                
                    OpportunityLineItem Renewaloli = new OpportunityLineItem();
                    Renewaloli.OpportunityId = Renewal.Id;
                    Renewaloli.Quantity = 1;
                    Renewaloli.UnitPrice = 100;
                    Renewaloli.Product_Status__c = 'Current';
                    Renewaloli.Description__c = 'Return Path Certification';
                    Renewaloli.Active__c = true;
                    Renewaloli.Recurring__c = false;
                    Renewaloli.Start_Date__c = system.today();
                    Renewaloli.End_Date__c = system.today();
                    Renewaloli.PricebookentryId = '01u00000000FGwSAAW';
                    
                    insert Renewaloli;
                    
                oClone.Application_Result__c = 'Pass';
                oClone.StageName = 'Finance Approved';
                
                update oClone;
}
    private static testmethod void testUpdateRenewalCertInfoAppPassed2(){


User user = new user(Alias = 'standt', Email='tes@rp.com', 
      EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
      LocaleSidKey='en_US', ProfileId = '00e0000000734LT', 
      TimeZoneSidKey='America/Los_Angeles', UserName='test@rp.com.rp');
      insert user;    

Account a = new Account(
        OwnerId = user.Id,
        Name = 'Test Account',
        phone = '(303) 555-5555');
        insert a;
        
            Opportunity o = new Opportunity();
                o.OwnerId = user.Id;
                o.Name = 'Test1';
                o.AccountId = a.Id;
                o.CloseDate=system.today();
                o.StageName='Pending Certification Qualification';
                o.Approval_Type__c = 'Certification Approval';
                o.Pricebook2Id = '01s000000004NS6AAM';
                
                insert o;
        
            Opportunity oClone = new Opportunity();
                oClone.OwnerId = user.Id;
                oClone.Name = 'Test2';
                oClone.AccountId = a.Id;
                oClone.CloseDate=system.today();
                oClone.StageName='Pending Certification Qualification';
                oClone.Approval_Type__c = 'Certification Approval';
                oClone.Pricebook2Id = '01s000000004NS6AAM';
                oClone.Cloned_From_ID__c = o.id;
                
                insert oClone;
                
                    OpportunityLineItem oli = new OpportunityLineItem();
                    oli.OpportunityId = oClone.Id;
                    oli.Quantity = 1;
                    oli.UnitPrice = 100;
                    oli.Product_Status__c = 'Current';
                    oli.Description__c = 'Return Path Certification';
                    oli.Active__c = true;
                    oli.Recurring__c = false;
                    oli.PricebookentryId = '01u00000000FGwSAAW';
                    
                    insert oli;
                
            Opportunity Renewal = new Opportunity();
                Renewal.OwnerId = user.Id;
                Renewal.Name = 'Test3';
                Renewal.AccountId = a.Id;
                Renewal.CloseDate=system.today();
                Renewal.StageName='Pending Certification Qualification';
                Renewal.Approval_Type__c = 'Certification Approval';
                Renewal.Previous_Opportunity_Lookup__c = o.id;
                Renewal.Pricebook2Id = '01s000000004NS6AAM';
                
                insert Renewal;
                
                    OpportunityLineItem Renewaloli = new OpportunityLineItem();
                    Renewaloli.OpportunityId = Renewal.Id;
                    Renewaloli.Quantity = 1;
                    Renewaloli.UnitPrice = 100;
                    Renewaloli.Product_Status__c = 'Current';
                    Renewaloli.Description__c = 'Return Path Certification';
                    Renewaloli.Active__c = true;
                    Renewaloli.Recurring__c = false;
                    Renewaloli.PricebookentryId = '01u00000000FGwSAAW';
                    
                    insert Renewaloli;
                    
                oClone.Application_Result__c = 'Fail';
                oClone.StageName = 'Terminated Contract'; 
                
                update oClone;                 
}

private static testmethod void testUpdateRenewalCertInfoAppPassed3(){


User user = new user(Alias = 'standt', Email='tes@rp.com', 
      EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
      LocaleSidKey='en_US', ProfileId = '00e0000000734LT', 
      TimeZoneSidKey='America/Los_Angeles', UserName='test@rp.com.rp');
      insert user;    

Account a = new Account(
        OwnerId = user.Id,
        Name = 'Test Account',
        phone = '(303) 555-5555');
        insert a;
        
            Opportunity o = new Opportunity();
                o.OwnerId = user.Id;
                o.Name = 'Test1';
                o.AccountId = a.Id;
                o.CloseDate=system.today();
                o.StageName='Pending Certification Qualification';
                o.Approval_Type__c = 'Certification Approval';
                o.Pricebook2Id = '01s000000004NS6AAM';
                
                insert o;
        
            Opportunity oClone = new Opportunity();
                oClone.OwnerId = user.Id;
                oClone.Name = 'Test2';
                oClone.AccountId = a.Id;
                oClone.CloseDate=system.today();
                oClone.StageName='Pending Certification Qualification';
                oClone.Approval_Type__c = 'Certification Approval';
                oClone.Pricebook2Id = '01s000000004NS6AAM';
                oClone.Cloned_From_ID__c = o.id;
                
                insert oClone;
                
                    OpportunityLineItem oli = new OpportunityLineItem();
                    oli.OpportunityId = oClone.Id;
                    oli.Quantity = 1;
                    oli.UnitPrice = 100;
                    oli.Product_Status__c = 'Current';
                    oli.Description__c = 'Return Path Certification';
                    oli.Active__c = true;
                    oli.Recurring__c = false;
                    oli.PricebookentryId = '01u00000000FGwSAAW';
                    
                    insert oli;
                
            Opportunity Renewal = new Opportunity();
                Renewal.OwnerId = user.Id;
                Renewal.Name = 'Test3';
                Renewal.AccountId = a.Id;
                Renewal.CloseDate=system.today();
                Renewal.StageName='Pending Certification Qualification';
                Renewal.Approval_Type__c = 'Certification Approval';
                Renewal.Previous_Opportunity_Lookup__c = o.id;
                Renewal.Pricebook2Id = '01s000000004NS6AAM';
                
                insert Renewal;
                
                    OpportunityLineItem Renewaloli = new OpportunityLineItem();
                    Renewaloli.OpportunityId = Renewal.Id;
                    Renewaloli.Quantity = 1;
                    Renewaloli.UnitPrice = 100;
                    Renewaloli.Product_Status__c = 'Current';
                    Renewaloli.Description__c = 'Return Path Certification';
                    Renewaloli.Active__c = true;
                    Renewaloli.Recurring__c = false;
                    Renewaloli.PricebookentryId = '01u00000000FGwSAAW';
                    
                    insert Renewaloli;
                    
                    OpportunityLineItem oli2 = new OpportunityLineItem();
                    oli2.OpportunityId = Renewal.Id;
                    oli2.Quantity = 1;
                    oli2.UnitPrice = 100;
                    oli2.Product_Status__c = 'Current';
                    oli2.Description__c = 'Gold';
                    oli2.Active__c = true;
                    oli2.Recurring__c = false;
                    oli2.PricebookentryId = '01u00000000FGw7AAG';
                    insert oli2;   
                    
                oClone.Application_Result__c = 'Fail'; 
                oClone.StageName = 'Terminated Contract';
                update oClone;                 
}*/
}