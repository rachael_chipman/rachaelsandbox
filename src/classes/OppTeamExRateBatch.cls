global class OppTeamExRateBatch implements Database.Batchable<sObject>{
global final String Query;
global final String OppId;
global OppTeamExRateBatch(String q, String op){
Query=q;
OppId=op;
}

global Database.QueryLocator start(Database.BatchableContext BC){return Database.getQueryLocator(query);}

global void execute(Database.BatchableContext BC,List<OpportunityTeamMember> scope){

Opportunity o = [Select Id, CurrencyIsoCode, closeDate from Opportunity where Id = :OppId];

DatedConversionRate dcr = [select ConversionRate from DatedConversionRate where StartDate <= :o.closeDate and NextStartDate > :o.closeDate and IsoCode = :o.CurrencyIsoCode limit 1];

CurrencyType ct = [select ConversionRate from CurrencyType where IsoCode = :o.CurrencyIsoCode limit 1];

if (scope.isEmpty()) return;
    
    for (OpportunityTeamMember otms: scope) {
    otms.Exchange_Rate__c = dcr.ConversionRate;
    otms.Company_Exchange_Rate__c = ct.ConversionRate;
    otms.CurrencyIsoCode = 'USD';
    
    update otms;}
    }
    
    global void finish(Database.BatchableContext BC){

}
}