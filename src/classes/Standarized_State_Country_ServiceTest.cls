@isTest(SeeAllData= true)
private class Standarized_State_Country_ServiceTest
{
    public static Contact TestContact;
    public static Account TestAccount;
    public static User salesPerson = TestUtils.SALES_USER;
    
    static void setupTestAccountdata(string billCountry, string billstate, string shipcountry, string shipstate)
    {
        //Create the Account
        TestAccount = new Account();
        TestAccount.Name = 'Test Account';
        TestAccount.BillingCountry = billCountry;
        TestAccount.BillingState = billstate;
        TestAccount.ShippingState = shipstate ;
        TestAccount.ShippingCountry = shipcountry;
        TestAccount.Salesperson__c = salesPerson.Id;
        insert TestAccount;

        Id ownerId = [SELECT Id, OwnerId FROM Account WHERE Id = : TestAccount.Id].OwnerId;
        system.assertEquals(salesPerson.Id, ownerId);

    }

    static void setupTestContactdata(string mailCountry, string mailstate)
    {
        //Create the Account
        TestContact = new Contact();
        //TestContact.AccountId = accId;
        TestContact.LastName = 'Test Con 45';
        TestContact.FirstName = 'Test';
        TestContact.MailingState = mailstate;
        TestContact.MailingCountry = mailCountry;
        insert TestContact;

    }

    static testMethod void testSingleAccountAddress()
    {
        system.runAs(salesPerson)
        {
            Test.startTest();
            setupTestAccountdata('USA','Colorado','United States', 'California');
            Test.stopTest();
        }
        
        Account Tacc = [Select Id, Name, BillingState, BillingCountry,ShippingCountry,ShippingState from Account where Id =:TestAccount.Id limit 1];
        if(Tacc!= null)
        {
            System.assertEquals('United States', Tacc.BillingCountry,'We expect the country to be United States');
            System.assertEquals('CO', Tacc.BillingState, 'We expect state to be Colorado');
            System.assertEquals('United States', Tacc.ShippingCountry, 'We expect the country to be United States');
            System.assertEquals('CA', Tacc.ShippingState, 'We expect the state to be California');
        }
    }

    @isTest static void testSingleAccountAddressUpdate()
    {
        
        system.runAs(salesPerson)
        {
            setupTestAccountdata('US', 'CO', 'USA', 'CA');
            TestAccount.BillingCountry = 'US';
            TestAccount.BillingState = '';
            TestAccount.ShippingCountry = 'USA';
            TestAccount.ShippingState = 'California';
            TestAccount.Shipping_and_Billing_Address_the_Same__c = 'Addresses are not the same';

                Test.startTest();
                update TestAccount;
                Test.stopTest();
        }
        
        Account Tacc = [Select Id, Name, BillingState, BillingCountry,ShippingCountry,ShippingState from Account where Id =:TestAccount.Id limit 1];
        if(Tacc!= null)
        {
            System.assertEquals('United States', Tacc.BillingCountry, 'We expect the country to be United States');
            System.assertEquals(null, Tacc.BillingState, 'We expect the state to be empty');
            System.assertEquals('United States', Tacc.ShippingCountry, 'We expect the country to be United States');
            System.assertEquals('CA', Tacc.ShippingState, 'We expect the state to be CA');
        }
    }

    /*@isTest static void testSingleAccountAddressUpdateStateEqualsState()
    {
        system.runAs(TestUtils.SALES_USER)
        {
            Test.startTest();
            setupTestAccountdata('United States', 'Colorado', 'United States', 'California');
            Test.stopTest();
        }
        
        Account Tacc = [Select Id, Name, BillingState, BillingCountry,ShippingCountry,ShippingState from Account where Id =:TestAccount.Id limit 1];
        if(Tacc!= null)
        {
            System.assertEquals('United States', Tacc.BillingCountry, 'We expect the country to be United States');
            System.assertEquals(null, Tacc.BillingState, 'We expect the country to be empty');
            System.assertEquals('United States', Tacc.ShippingCountry, 'We expect the country to be United States');
            System.assertEquals('California', Tacc.ShippingState, 'We expect the country to be California');
        }
    }*/

    @isTest static void testSingleAccountInValidUpdate()
    {
        Boolean errorCaught = false;
        system.runAs(salesPerson)
        {
            setupTestAccountdata('US', 'CO', 'US', 'CA');

            TestAccount.BillingCountry = 'tytututut';
            TestAccount.BillingState = '';
            TestAccount.ShippingCountry = 'jkjqljljlq';
            TestAccount.ShippingState = 'hkhqkhkhqsk ';
            
                Test.startTest();
                try
                {
                    update TestAccount;
                }
                catch (Exception ex)
                {
                    System.debug('Exception occured');
                    errorCaught = true;
                }
                Test.stopTest();
        }
        system.assertEquals(true, errorCaught, 'Exception should be thrown');
    }


    @isTest static void testSingleAccountAddressUpdate1()
    {
        system.runAs(salesPerson)
        {
            setupTestAccountdata('USA','CO','US', 'Colorado');

            TestAccount.BillingCountry = '';
            TestAccount.BillingState = 'CA';
            TestAccount.ShippingCountry = 'USA';
            TestAccount.ShippingState = 'Colorado';
            TestAccount.Shipping_and_Billing_Address_the_Same__c = 'Addresses are not the same';

                Test.startTest();
                update TestAccount;
                Test.stopTest();
        }
        
        Account Tacc = [Select Id, Name, BillingState, BillingCountry,ShippingCountry,ShippingState from Account where Id =:TestAccount.Id limit 1];
        if(Tacc!= null)
        {
            System.assertEquals(null, Tacc.BillingCountry, 'We expect the country to be null');
            System.assertEquals('CA', Tacc.BillingState, 'We expect the billstate to be CA');
            System.assertEquals('United States', Tacc.ShippingCountry, 'We expect the country to be United States');
            System.assertEquals('CO', Tacc.ShippingState, 'We expect the shipstate to be CO');
        }
    
    }


    @isTest static void testSingleAccountAddressUpdate2()
    {
        system.runAs(salesPerson)
        {
            setupTestAccountdata('USA','CO','US', 'Colorado');
        
            TestAccount.BillingCountry = 'US';
            TestAccount.BillingState = 'CO';
            TestAccount.ShippingCountry = 'USA';
            TestAccount.ShippingState = 'Colorado';
            TestAccount.Shipping_and_Billing_Address_the_Same__c = 'Addresses are not the same';
            
                Test.startTest();
                update TestAccount;
                Test.stopTest();
        }
        
        Account Tacc = [Select Id, Name, BillingState, BillingCountry,ShippingCountry,ShippingState from Account where Id =:TestAccount.Id limit 1];
        if(Tacc!= null)
        {
            System.assertEquals('United States', Tacc.BillingCountry, 'We expect the country to be United States');
            System.assertEquals('CO', Tacc.BillingState, 'We expect the billstate to be CO');
            System.assertEquals('United States', Tacc.ShippingCountry, 'We expect the country to be United States');
            System.assertEquals('CO', Tacc.ShippingState, 'We expect the shipstate to be CO');
        }

    }

    @isTest static void testSingleContacttAddress()
    {
        
        //setupTestAccountdata('USA','CO','US', 'Colorado');
        system.runAs(salesPerson)
        {
            Test.startTest();
            setupTestContactdata( 'USA', 'Colorado');
            Test.stopTest();
        }
        //System.assertEquals(TCon.MailingCountry, 'United States', 'We expect the country to be United States');
        Contact TCon = [Select Id, Name, MailingState, MailingCountry from Contact where Id =:TestContact.Id limit 1];
        System.assertEquals('CO', TCon.MailingState, 'We expect the mailstate to be CO');
        
    }
    

    @isTest static void testSingleContacttAddress2()
    {
        //setupTestAccountdata('USA','CO','US', 'Colorado');
        system.runAs(salesPerson)
        {
            setupTestContactdata( 'United States', 'Colorado');
            
            Contact TCon = [Select Id, Name, MailingState, MailingCountry from Contact where Id =:TestContact.Id limit 1];
            
            TCon.MailingState = 'California';
            TCon.MailingCountry = 'USA';
            
                Test.startTest();
                update TCon;
                Test.stopTest();
            TCon = [Select Id, Name, MailingState, MailingCountry from Contact where Id =:TCon.Id limit 1];
            System.debug('After updating the contact' + Tcon);
            
            System.debug('Mailing State and Country is :' + Tcon.MailingState + ' ' +Tcon.MailingCountry);
            if(Tcon!= null)
            {
                System.debug('Mailing State and Country is :' + Tcon.MailingState + ' ' +Tcon.MailingCountry);
                System.assertEquals('United States', Tcon.MailingCountry, 'We expect the country to be United States');
                System.assertEquals('CA', Tcon.MailingState, 'We expect the mailstate to be CA');
            }
        }

    }
}