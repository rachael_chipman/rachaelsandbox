@isTest(SeeAllData=true) 
public class OverageGenerationTest {
    
    //ipEvent = Inbox Preview Event - API Name Campaign_Preview_Events
    //imEvent = Inbox Monitor Event - API Name Mailbox_Monitor_Events
    //ppc = Product Portfolio Component - API Name Customer_Product_Detail
    
    /*public static Campaign_Preview_Events__c ipEvent;
    public static Mailbox_Monitor_Events__c imEvent;
    //public static Account account;
    //public static Customer_Product_Detail__c ppc;
    public static User testUser;
    public static Opportunity requiredOpp;
    
    private static Account testAccount;
    private static List<Account> testAccounts;
    private static Zuora__CustomerAccount__c testBillingAccount;

    private static Zuora__Subscription__c testSub;
    private static Zuora__SubscriptionProductCharge__c testSubProdCharge;
    private static Zuora__Product__c testProduct;

    
    public static void setupObjects()
    {
        testUser = [SELECT Id FROM User WHERE isActive = true LIMIT 1];

        testAccounts = new List<Account>();

        testAccounts.add(testAccount = TestUtils.createAccount(TestUtils.MARKETER_RECORD_TYPE.Id, true));
        system.assert(testAccount != NULL);

        testBillingAccount = TestUtils.createBillingAccounts(testAccounts, true)[0];
        system.assert(testBillingAccount != NULL);

        testSub = TestUtils.createZ_Subscription(testAccount.Id, testBillingAccount.Id, testBillingAccount.Id, 'Test Cert Subscription', 'TERMED', true);
        system.assert(testSub != NULL);
        testSub.Zuora__Status__c = 'Active';
        update testSub;

        testProduct = TestUtils.createZuoraProduct('Email Optimization', true);
        system.assert(testProduct != NULL);

        testSubProdCharge = TestUtils.createZ_SubscriptionCharge('Inbox Monitor Events', testSub.Id, testAccount.Id, testProduct.Id, 'Email Intelligence Solutions - Gold - Monthly', 'Recurring', true);
        system.assert(testSubProdCharge != NULL);
        testSubProdCharge.IncludedUnits__c = 25;
        testSubProdCharge.Zuora__Price__c = 25.00;
        update testSubProdCharge;
        
        /*account = new Account(Name = 'Test Account', CurrencyISOCode = 'GBP', Type = 'Current Client/Partner', AM__c = testUser.Id );
        insert account;*/
        
        /*requiredOpp = new Opportunity (Name = 'Test Opportunity', AccountId = testAccount.Id, StageName = 'Finance Approved', CloseDate = system.today().addDays(-365));
        insert requiredOpp;
        
        ipEvent = new Campaign_Preview_Events__c();
        ipEvent.Account__c = testAccount.Id;
        ipEvent.Opportunity__c = requiredOpp.Id;
        ipEvent.Messages__c = 50;
        
        imEvent = new Mailbox_Monitor_Events__c();
        imEvent.Account__c = testAccount.Id;
        imEvent.Opportunity__c = requiredOpp.Id;
        imEvent.Campaigns__c = 50;
        
        /*ppc = new Customer_Product_Detail__c();
        ppc.Product_Name__c = 'Email Intelligence Solutions - Gold Service';
        ppc.Account__c = account.Id;
        ppc.Combined_Events__c = 'No';
        ppc.Product_Family__c = 'Email Intelligence for Marketers';
        ppc.Inbox_Preview_Events__c = 25;
        ppc.Inbox_Preview_Fee__c = 25.00;
        ppc.Inbox_Monitor_Events__c = 25;
        ppc.Inbox_Monitor_Fee__c = 25.00;*/
    //}
    
    /*public static /*testMethod*/ /*void testOverageFeeOppGenerationIMEventInsert()
    {
        OverageFeeGenerationServices.qCurrency = 'GBP';
        setupObjects();
        //insert ppc;
		//system.assertEquals('Current Client/Partner', account.Type, 'We expect the account to be a client');
        //PricebookEntry pbe = [SELECT Id, CurrencyISOCode, Name, Pricebook2.Name FROM PricebookEntry WHERE Name = :ppc.Product_Name__c and CurrencyISOCode = :account.CurrencyISOCode and Pricebook2.Name = 'Return Path Pricebook' LIMIT 1];
        
        List<AsyncApexJob> myJobs = [select id from AsyncApexJob where Status != 'Completed'];
        for(AsyncApexJob myJob : myJobs)
        {
            system.abortJob(myJob.Id);
        }

        test.startTest();
        insert imEvent;
        test.stopTest();

        //Get overage amount for assertions
        Decimal expectedAmount = (((imEvent.Campaigns__c - testSubProdCharge.IncludedUnits__c) * testSubProdCharge.Zuora__Price__c)).setScale(2);
        system.debug('What amount do we expect? ' + expectedAmount);
        
        /*List<Opportunity> feeOpps = [SELECT Id, OwnerId, CurrencyISOCode, Type, AccountId FROM Opportunity WHERE AccountId = :testAccount.Id and Id != :requiredOpp.Id];
        system.assertEquals(1, feeOpps.size());
        system.assertEquals('GBP', feeOpps[0].CurrencyISOCode);
        //system.assertEquals(account.OwnerId, feeOpps[0].OwnerId);
        
        /*List<OpportunityLineItem> feeOlis = [SELECT Id, PricebookEntryId, Product_Name__c, CurrencyISOCode FROM OpportunityLineItem WHERE OpportunityId IN :feeOpps];
        system.assertEquals(1, feeOlis.size());
        system.assertEquals(pbe.Id, feeOlis[0].PricebookEntryId);*/

        //Query for fee opps
        /*list<Opportunity> opportunitiesCreated = [SELECT Id FROM Opportunity WHERE AccountId = :testAccount.Id and Id != :requiredOpp.Id];
        system.assertEquals(1, opportunitiesCreated.size(), 'We expect there to be 1 new opportunity');
        Set<Id> oppIds = pluck.ids(opportunitiesCreated);
        //Query for fee quotes
        List<zqu__Quote__c> feeQuotes = [SELECT Id, zqu__Account__c, RealmId__c, Name, RecordTypeId, zqu__Amendment_Name__c, zqu__StartDate__c, zqu__SubscriptionType__c, zqu__ZuoraAccountID__c, zqu__ExistSubscriptionID__c
        FROM zqu__Quote__c WHERE zqu__Opportunity__c IN : oppIds];
        system.assertEquals(opportunitiesCreated.size(), feeQuotes.size(), 'We expect a quote for each opportunity');
        for(zqu__Quote__c quote : feeQuotes)
        {
            system.assertEquals(testAccount.Id, quote.zqu__Account__c, 'Each quote belongs to the same account');
            system.assertEquals(quote.RecordTypeId, OverageFeeGenerationServices.amendmentQuote.Id, 'Each quote should be an amendmentQuote');
            system.assertEquals((String) OverageFeeGenerationServices.subObject.getValue('Id'), quote.zqu__ExistSubscriptionID__c, 'Quote should be related to exisiting Subscription');
            system.assertEquals((String) OverageFeeGenerationServices.accObject.getValue('Id'), quote.zqu__ZuoraAccountID__c, 'Quote should be related to exisiting Billing Account');
        }
        
        //Query for quote amendments
        List<zqu__QuoteAmendment__c> amendments = [SELECT Id, zqu__TotalAmount__c, zqu__Quote__c FROM zqu__QuoteAmendment__c WHERE zqu__Quote__c  = :feeQuotes[0].Id ];
        system.assertEquals(feeQuotes.size(), amendments.size(), 'We expect an amendment for each quote.');
        system.assertEquals(expectedAmount, amendments[0].zqu__TotalAmount__c, 'We expect the overage amount to be the event usage less included units, multiplied by the price.');

        //Query for the rate plans
        List<zqu__QuoteRatePlan__c> ratePlans = [SELECT Id, zqu__QuoteAmendment__c FROM zqu__QuoteRatePlan__c WHERE zqu__QuoteAmendment__c IN : amendments];
        system.assertEquals(amendments.size(), ratePlans.size(), 'We expect one rate plan for each amendment');
        system.assertEquals(amendments[0].Id, ratePlans[0].zqu__QuoteAmendment__c, 'We expect the rate plan to be attached to the amendment');
        
        //Query for the rate plan charges
        List<zqu__QuoteRatePlanCharge__c> ratePlanCharges = [SELECT Id, zqu__QuoteRatePlan__c, zqu__EffectivePrice__c, zqu__TCV__c FROM zqu__QuoteRatePlanCharge__c WHERE zqu__QuoteRatePlan__c = : ratePlans[0].Id];
        system.assertEquals(ratePlans.size(), ratePlanCharges.size(), 'We expect there to be one rate plan charge for each rate plan');
        system.assertEquals(ratePlans[0].Id, ratePlanCharges[0].zqu__QuoteRatePlan__c, 'We expect the charge to be attached to the rate plan');
        system.assertEquals(expectedAmount, ratePlanCharges[0].zqu__TCV__c, 'We expect the overage amount to be the event usage less included units, multiplied by the price.');
        system.assertEquals(expectedAmount, ratePlanCharges[0].zqu__EffectivePrice__c, 'We expect the overage amount to be the event usage less included units, multiplied by the price.');

        //Query for the charge summaries
        List<zqu__QuoteChargeSummary__c> chargeSummaries = [SELECT Id, zqu__QuoteRatePlan__c, zqu__EffectivePrice__c, zqu__TotalPrice__c FROM zqu__QuoteChargeSummary__c WHERE zqu__QuoteRatePlan__c = : ratePlans[0].Id];
        system.assertEquals(ratePlans.size(), chargeSummaries.size(), 'We expect one charge summary for each rate plan.');
        system.assertEquals(expectedAmount, chargeSummaries[0].zqu__EffectivePrice__c, 'We expect the overage amount to be the event usage less included units, multiplied by the price.');
        system.assertEquals(expectedAmount, chargeSummaries[0].zqu__TotalPrice__c, 'We expect the overage amount to be the event usage less included units, multiplied by the price.');
    }
    
    public static /*testMethod*/ /*void testOverageFeeOppGenerationIMEventUpdate()
    {
         OverageFeeGenerationServices.qCurrency = 'GBP';
        setupObjects();
        //insert ppc;
        insert imEvent;

        //Bring this up in code review!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! THERE SHOULD BE 2 OPPS CREATED (I THINK THE CALL TO ABORT THE BATCHES IS THE ISSUE)
        
        //PricebookEntry pbe = [SELECT Id, CurrencyISOCode, Name, Pricebook2.Name FROM PricebookEntry WHERE Name = :ppc.Product_Name__c and CurrencyISOCode = :account.CurrencyISOCode and Pricebook2.Name = 'Return Path Pricebook' LIMIT 1];
        List<AsyncApexJob> myJobs = [select id from AsyncApexJob where Status != 'Completed' and MethodName != :InboxMonitorOverageBatch.BATCH_NAME];
        for(AsyncApexJob myJob : myJobs)
        {
            system.abortJob(myJob.Id);
        }

        test.startTest();
        imEvent.Generate_Fee_Opportunity__c = true;
        update imEvent;
        test.stopTest();

        Decimal expectedAmount = (((imEvent.Campaigns__c - testSubProdCharge.IncludedUnits__c) * testSubProdCharge.Zuora__Price__c)).setScale(2);
        system.debug('What amount do we expect? ' + expectedAmount);
        
        /*List<Opportunity> feeOpps = [SELECT Id, OwnerId, CurrencyISOCode, Type, AccountId FROM Opportunity WHERE AccountId = :account.Id and Id != :requiredOpp.Id];
        system.assertEquals(2, feeOpps.size());
        system.assertEquals('GBP', feeOpps[0].CurrencyISOCode);
        //system.assertEquals(account.OwnerId, feeOpps[0].OwnerId);
        
        List<OpportunityLineItem> feeOlis = [SELECT Id, PricebookEntryId, Product_Name__c, CurrencyISOCode FROM OpportunityLineItem WHERE OpportunityId IN :feeOpps];
        system.assertEquals(2, feeOlis.size());
        system.assertEquals(pbe.Id, feeOlis[0].PricebookEntryId);*/

        //Query for fee opps
        /*list<Opportunity> opportunitiesCreated = [SELECT Id FROM Opportunity WHERE AccountId = :testAccount.Id and Id != :requiredOpp.Id];
        system.assertEquals(1, opportunitiesCreated.size(), 'We expect there to be 2 new opportunities');
        Set<Id> oppIds = pluck.ids(opportunitiesCreated);
        //Query for fee quotes
        List<zqu__Quote__c> feeQuotes = [SELECT Id, zqu__Account__c, RealmId__c, Name, RecordTypeId, zqu__Amendment_Name__c, zqu__StartDate__c, zqu__SubscriptionType__c, zqu__ZuoraAccountID__c, zqu__ExistSubscriptionID__c
        FROM zqu__Quote__c WHERE zqu__Opportunity__c IN : oppIds];
        system.assertEquals(opportunitiesCreated.size(), feeQuotes.size(), 'We expect a quote for each opportunity');
        for(zqu__Quote__c quote : feeQuotes)
        {
            system.assertEquals(testAccount.Id, quote.zqu__Account__c, 'Each quote belongs to the same account');
            system.assertEquals(quote.RecordTypeId, OverageFeeGenerationServices.amendmentQuote.Id, 'Each quote should be an amendmentQuote');
            system.assertEquals((String) OverageFeeGenerationServices.subObject.getValue('Id'), quote.zqu__ExistSubscriptionID__c, 'Quote should be related to exisiting Subscription');
            system.assertEquals((String) OverageFeeGenerationServices.accObject.getValue('Id'), quote.zqu__ZuoraAccountID__c, 'Quote should be related to exisiting Billing Account');
        }
        
        //Query for quote amendments
        List<zqu__QuoteAmendment__c> amendments = [SELECT Id, zqu__TotalAmount__c, zqu__Quote__c FROM zqu__QuoteAmendment__c WHERE zqu__Quote__c  IN :feeQuotes ];
        system.assertEquals(feeQuotes.size(), amendments.size(), 'We expect an amendment for each quote.');
        Map<Id, zqu__QuoteAmendment__c> idToAmendment = new Map<Id, zqu__QuoteAmendment__c>(amendments);
        for(zqu__QuoteAmendment__c amendment : amendments)
        {
            system.assertEquals(expectedAmount, amendment.zqu__TotalAmount__c, 'We expect the overage amount to be the event usage less included units, multiplied by the price.');
        }
        

        //Query for the rate plans
        List<zqu__QuoteRatePlan__c> ratePlans = [SELECT Id, zqu__QuoteAmendment__c FROM zqu__QuoteRatePlan__c WHERE zqu__QuoteAmendment__c IN : amendments];
        system.assertEquals(amendments.size(), ratePlans.size(), 'We expect one rate plan for each amendment');
        Map<Id, zqu__QuoteRatePlan__c> idToRatePlan = new Map<Id, zqu__QuoteRatePlan__c>(ratePlans);
        for(zqu__QuoteRatePlan__c ratePlan : ratePlans)
        {
            system.assert(idToAmendment.containsKey(ratePlan.zqu__QuoteAmendment__c), 'We expect the rate plan to be attached to the amendment');
        }
        
        
        //Query for the rate plan charges
        List<zqu__QuoteRatePlanCharge__c> ratePlanCharges = [SELECT Id, zqu__QuoteRatePlan__c, zqu__EffectivePrice__c, zqu__TCV__c FROM zqu__QuoteRatePlanCharge__c WHERE zqu__QuoteRatePlan__c IN : ratePlans];
        system.assertEquals(ratePlans.size(), ratePlanCharges.size(), 'We expect there to be one rate plan charge for each rate plan');

        for(zqu__QuoteRatePlanCharge__c ratePlanCharge : ratePlanCharges)
        {
            system.assert(idToRatePlan.containsKey(ratePlanCharge.zqu__QuoteRatePlan__c), 'We expect the charge to be attached to the rate plan.');
            system.assertEquals(expectedAmount, ratePlanCharge.zqu__TCV__c, 'We expect the overage amount to be the event usage less included units, multiplied by the price.');
            system.assertEquals(expectedAmount, ratePlanCharge.zqu__EffectivePrice__c, 'We expect the overage amount to be the event usage less included units, multiplied by the price.');
        }

        //Query for the charge summaries
        List<zqu__QuoteChargeSummary__c> chargeSummaries = [SELECT Id, zqu__QuoteRatePlan__c, zqu__EffectivePrice__c, zqu__TotalPrice__c FROM zqu__QuoteChargeSummary__c WHERE zqu__QuoteRatePlan__c IN : ratePlans];
        system.assertEquals(ratePlans.size(), chargeSummaries.size(), 'We expect one charge summary for each rate plan.');
        for(zqu__QuoteChargeSummary__c chargeSummary : chargeSummaries)
        {
            system.assert(idToRatePlan.containsKey(chargeSummary.zqu__QuoteRatePlan__c), 'We expect the summary to be attached to the rate plan.');
            system.assertEquals(expectedAmount, chargeSummary.zqu__EffectivePrice__c, 'We expect the overage amount to be the event usage less included units, multiplied by the price.');
            system.assertEquals(expectedAmount, chargeSummary.zqu__TotalPrice__c, 'We expect the overage amount to be the event usage less included units, multiplied by the price.');
        }
    }
    
    /*public static testMethod void testOverageFeeOppGenerationIPEventInsert()
    {
        setupObjects();
        insert ppc;
        
        PricebookEntry pbe = [SELECT Id, CurrencyISOCode, Name, Pricebook2.Name FROM PricebookEntry WHERE Name = :ppc.Product_Name__c and CurrencyISOCode = :account.CurrencyISOCode and Pricebook2.Name = 'Return Path Pricebook' LIMIT 1];
        
        test.startTest();
        insert ipEvent;
        test.stopTest();
        
        List<Opportunity> feeOpps = [SELECT Id, OwnerId, CurrencyISOCode, Type, AccountId FROM Opportunity WHERE AccountId = :account.Id and Id != :requiredOpp.Id];
        system.assertEquals(1, feeOpps.size());
        system.assertEquals('GBP', feeOpps[0].CurrencyISOCode);
        //system.assertEquals(account.OwnerId, feeOpps[0].OwnerId);
        
        List<OpportunityLineItem> feeOlis = [SELECT Id, PricebookEntryId, Product_Name__c, CurrencyISOCode FROM OpportunityLineItem WHERE OpportunityId IN :feeOpps];
        system.assertEquals(1, feeOlis.size());
        system.assertEquals(pbe.Id, feeOlis[0].PricebookEntryId);
    }
    
    public static testMethod void testOverageFeeOppGenerationIPEventUpdate()
    {
        setupObjects();
        insert ppc;
        insert ipEvent;
        
        PricebookEntry pbe = [SELECT Id, CurrencyISOCode, Name, Pricebook2.Name FROM PricebookEntry WHERE Name = :ppc.Product_Name__c and CurrencyISOCode = :account.CurrencyISOCode and Pricebook2.Name = 'Return Path Pricebook' LIMIT 1];
        
        test.startTest();
        ipEvent.Generate_Fee_Opportunity__c = true;
        update ipEvent;
        test.stopTest();
        
        List<Opportunity> feeOpps = [SELECT Id, OwnerId, CurrencyISOCode, Type, AccountId FROM Opportunity WHERE AccountId = :account.Id and Id != :requiredOpp.Id];
        system.assertEquals(2, feeOpps.size());
        system.assertEquals('GBP', feeOpps[0].CurrencyISOCode);
        //system.assertEquals(account.OwnerId, feeOpps[0].OwnerId);
        
        List<OpportunityLineItem> feeOlis = [SELECT Id, PricebookEntryId, Product_Name__c, CurrencyISOCode FROM OpportunityLineItem WHERE OpportunityId IN :feeOpps];
        system.assertEquals(2, feeOlis.size());
        system.assertEquals(pbe.Id, feeOlis[0].PricebookEntryId);
    }
    
    private static testMethod void testBulkInsertIPEvents()
    {
        Account acct = TestUtils.createAccount(TestUtils.MARKETER_RECORD_TYPE.Id, true);
        
        Opportunity opp = TestUtils.createOpportunity(TestUtils.RESEARCH_RECORD_TYPE.Id, acct.Id, true);
        
        Customer_Product_Detail__c ppct = TestUtils.createProductPortfolio(acct.Id, 'Email Intelligence Solutions - Platinum Service', false);
        ppct.Inbox_Preview_Events__c = 10;
        ppct.Inbox_Monitor_Events__c = 10;
        ppct.Inbox_Preview_Fee__c = 100;
        ppct.Inbox_Monitor_Fee__c = 100;
        ppct.Combined_Events__c = 'No';
        insert ppct;
        
        List<Campaign_Preview_Events__c> testIPEvents = TestUtils.createInboxPreviewEvents(900, acct.Id, opp.Id, '108900500', 15, false);
        
        test.startTest();
            insert testIPEvents;
        test.stopTest();        
    }*/
    
    /*private static testMethod void testBulkInsertWithDiffAccounts()
    {
        List<Account> testAccts = TestUtils.createAccounts(400, TestUtils.MARKETER_RECORD_TYPE.Id, true);
        List<Zuora__CustomerAccount__c> testBillingAccts = TestUtils.createBillingAccounts(testAccts, true);
        system.assert(testBillingAccts != NULL);
        List<Opportunity> requiredOpps = new List<Opportunity>();
        List<Mailbox_Monitor_Events__c> testIMEvents = new List<Mailbox_Monitor_Events__c>();
        List<Zuora__Subscription__c> testSubscriptions = new List<Zuora__Subscription__c>();
        List<Zuora__SubscriptionProductCharge__c> testSubProdCharges = new List<Zuora__SubscriptionProductCharge__c>();

        Zuora__Product__c testProd = TestUtils.createZuoraProduct('Email Optimization', true);
        system.assert(testProd != NULL);
        
        for(Integer i = 0; i < testBillingAccts.size(); i++)
        {
            Zuora__Subscription__c testSubs = TestUtils.createZ_Subscription(testBillingAccts[i].Zuora__Account__c, testBillingAccts[i].Id, testBillingAccts[i].Id, 'Test EIS Subscription', 'TERMED', false);
            system.assert(testSubs != NULL);
            testSubs.Zuora__Status__c = 'Active';
            testSubscriptions.add(testSubs);

            requiredOpps.add(TestUtils.createOpportunities(1, TestUtils.RESEARCH_RECORD_TYPE.Id, testBillingAccts[i].Zuora__Account__c, false )[0]);
        }
        insert requiredOpps;
        insert testSubscriptions;


        for(Integer i = 0; i < testSubscriptions.size(); i++)
        {
            Zuora__SubscriptionProductCharge__c testSubsProdCharge = TestUtils.createZ_SubscriptionCharge('Inbox Monitor Events', testSubscriptions[i].Id, testSubscriptions[i].Zuora__Account__c, testProd.Id, 'Email Intelligence Solutions - Gold - Monthly', 'Recurring', false);
            system.assert(testSubsProdCharge != NULL);
            testSubProdCharges.add(testSubsProdCharge);
            for(Integer j = 0; j < testSubProdCharges.size(); j++)
            {
                if(j < 100)
                {
                    testSubsProdCharge.IncludedUnits__c = 10;
                    testSubsProdCharge.Zuora__Price__c = 10.00;
                }
                else if(j >= 100 && j < 300)
                {
                    testSubsProdCharge.IncludedUnits__c = 15;
                    testSubsProdCharge.Zuora__Price__c = 15.00;
                }
                else if(j >= 300)
                {
                    testSubsProdCharge.IncludedUnits__c = 25;
                    testSubsProdCharge.Zuora__Price__c = 25.00;
                }
            }
        }
        insert testSubProdCharges;
        
        for(Integer i=0; i < requiredOpps.size(); i++)
        {
            testIMEvents.add(TestUtils.createInboxMonitorEvents(1, requiredOpps[i].AccountId, requiredOpps[i].Id, '99999999', 50, false)[0]);
        }

        List<AsyncApexJob> myJobs = [select id from AsyncApexJob where Status != 'Completed'];
        for(AsyncApexJob myJob : myJobs)
        {
            system.abortJob(myJob.Id);
        }

        test.startTest();
        insert testIMEvents;
        test.stopTest();
        
        //Get a set of AccountIds
        Set<Id> acctIds = pluck.Ids(testAccts);
        //Query for fee opps
        list<Opportunity> opportunitiesCreated = [SELECT Id FROM Opportunity WHERE AccountId IN : acctIds and Type = 'Fee'];
        system.assertEquals(400, opportunitiesCreated.size(), 'We expect there to be 400 new opportunities');
        Set<Id> oppIds = pluck.ids(opportunitiesCreated);
        //Query for fee quotes
        List<zqu__Quote__c> feeQuotes = [SELECT Id, zqu__Account__c, RealmId__c, Name, RecordTypeId, zqu__Amendment_Name__c, zqu__StartDate__c, zqu__SubscriptionType__c, zqu__ZuoraAccountID__c, zqu__ExistSubscriptionID__c
        FROM zqu__Quote__c WHERE zqu__Opportunity__c IN : oppIds];
        system.assertEquals(opportunitiesCreated.size(), feeQuotes.size(), 'We expect a quote for each opportunity');
        system.assertEquals(acctIds.size(), feeQuotes.size(), 'We expect each quote to have a different account.');
        for(zqu__Quote__c quote : feeQuotes)
        {
            //system.assertEquals(testAccount.Id, quote.zqu__Account__c, 'Each quote belongs to the same account');
            system.assertEquals(quote.RecordTypeId, OverageFeeGenerationServices.amendmentQuote.Id, 'Each quote should be an amendmentQuote');
            system.assertEquals((String) OverageFeeGenerationServices.subObject.getValue('Id'), quote.zqu__ExistSubscriptionID__c, 'Quote should be related to exisiting Subscription');
            system.assertEquals((String) OverageFeeGenerationServices.accObject.getValue('Id'), quote.zqu__ZuoraAccountID__c, 'Quote should be related to exisiting Billing Account');
        }
        
        //Query for quote amendments
        List<zqu__QuoteAmendment__c> amendments = [SELECT Id, zqu__TotalAmount__c, zqu__Quote__c, zqu__Quote__r.zqu__Account__c FROM zqu__QuoteAmendment__c WHERE zqu__Quote__c  IN :feeQuotes ];
        system.assertEquals(feeQuotes.size(), amendments.size(), 'We expect an amendment for each quote.');
        Map<Id, zqu__QuoteAmendment__c> idToAmendment = new Map<Id, zqu__QuoteAmendment__c>(amendments);

        //Query for all the existing charges, then map them to the quotes we generated, then validate that each amendment amount is correct
        List<Zuora__SubscriptionProductCharge__c> existingCharges = [SELECT Id, Name, Zuora__Account__c, Zuora__EffectiveEndDate__c, Zuora__EffectiveStartDate__c,
        Zuora__MonthlyRecurringRevenue__c, Product_Family__c, Zuora__Quantity__c, Zuora__Subscription__c, Zuora__Subscription__r.Zuora__Status__c, Zuora__Subscription__r.Zuora__Account__c, Zuora__Account__r.Name,
        Zuora__Subscription__r.Zuora__CustomerAccount__c, Zuora__Account__r.OwnerId, Zuora__Subscription__r.Zuora__Account__r.Owner.isActive, Zuora__Subscription__r.RealmId__c, Zuora__Price__c, IncludedUnits__c,
        Zuora__Subscription__r.CurrencyIsoCode, Zuora__Subscription__r.Zuora__Version__c, Zuora__Subscription__r.Zuora__Zuora_Id__c, Zuora__Subscription__r.Zuora__CustomerAccount__r.Zuora__Zuora_Id__c
        FROM Zuora__SubscriptionProductCharge__c WHERE Zuora__Account__c IN : testAccts AND Zuora__Subscription__r.Zuora__Status__c = : OverageFeeGenerationServices.ACTIVE];

        Map<Id, List<Zuora__SubscriptionProductCharge__c>> accountIdToExistingCharge = groupBy.Ids('Zuora__Account__c', existingCharges);

        //Query for all the inbox monitor events and then map them by account Id to record 
        List<Mailbox_Monitor_Events__c> insertedEvents = [SELECT Id, Account__c, Campaigns__c FROM Mailbox_Monitor_Events__c WHERE Id IN : testIMEvents];

        Map<Id, List<Mailbox_Monitor_Events__c>> accountIdToEvents = groupBy.Ids('Account__c', insertedEvents);

        for(zqu__QuoteAmendment__c amendment : amendments)
        {
            Decimal price = accountIdToExistingCharge.get(amendment.zqu__Quote__r.zqu__Account__c)[0].Zuora__Price__c;
            Integer includedUnits = Integer.valueOf(accountIdToExistingCharge.get(amendment.zqu__Quote__r.zqu__Account__c)[0].IncludedUnits__c);
            Integer eventLimit = Integer.valueOf(accountIdToEvents.get(amendment.zqu__Quote__r.zqu__Account__c)[0].Campaigns__c);

            Decimal overageAmount = ((eventLimit - includedUnits) * price).setScale(2);

            system.assertEquals(overageAmount, amendment.zqu__TotalAmount__c, 'We expect the overage amount to be the event usage less included units, multiplied by the price.');
        }
        
        //Query for the rate plans
        List<zqu__QuoteRatePlan__c> ratePlans = [SELECT Id, zqu__QuoteAmendment__c FROM zqu__QuoteRatePlan__c WHERE zqu__QuoteAmendment__c IN : amendments];
        system.assertEquals(amendments.size(), ratePlans.size(), 'We expect one rate plan for each amendment');
        Map<Id, zqu__QuoteRatePlan__c> idToRatePlan = new Map<Id, zqu__QuoteRatePlan__c>(ratePlans);
        for(zqu__QuoteRatePlan__c ratePlan : ratePlans)
        {
            system.assert(idToAmendment.containsKey(ratePlan.zqu__QuoteAmendment__c), 'We expect the rate plan to be attached to the amendment');
        }
        
        //Query for the rate plan charges
        List<zqu__QuoteRatePlanCharge__c> ratePlanCharges = [SELECT Id, zqu__QuoteRatePlan__c, zqu__QuoteRatePlan__r.zqu__QuoteAmendment__r.zqu__TotalAmount__c, zqu__EffectivePrice__c, zqu__TCV__c FROM zqu__QuoteRatePlanCharge__c WHERE zqu__QuoteRatePlan__c IN : ratePlans];
        system.assertEquals(ratePlans.size(), ratePlanCharges.size(), 'We expect there to be one rate plan charge for each rate plan');

        for(zqu__QuoteRatePlanCharge__c ratePlanCharge : ratePlanCharges)
        {
            system.assert(idToRatePlan.containsKey(ratePlanCharge.zqu__QuoteRatePlan__c), 'We expect the charge to be attached to the rate plan.');
            system.assertEquals(ratePlanCharge.zqu__QuoteRatePlan__r.zqu__QuoteAmendment__r.zqu__TotalAmount__c, ratePlanCharge.zqu__TCV__c, 'We expect the overage amount to be the event usage less included units, multiplied by the price.');
            system.assertEquals(ratePlanCharge.zqu__QuoteRatePlan__r.zqu__QuoteAmendment__r.zqu__TotalAmount__c, ratePlanCharge.zqu__EffectivePrice__c, 'We expect the overage amount to be the event usage less included units, multiplied by the price.');
        }

        //Query for the charge summaries
        List<zqu__QuoteChargeSummary__c> chargeSummaries = [SELECT Id, zqu__QuoteRatePlan__c, zqu__QuoteRatePlan__r.zqu__QuoteAmendment__r.zqu__TotalAmount__c, zqu__EffectivePrice__c, zqu__TotalPrice__c FROM zqu__QuoteChargeSummary__c WHERE zqu__QuoteRatePlan__c IN : ratePlans];
        system.assertEquals(ratePlans.size(), chargeSummaries.size(), 'We expect one charge summary for each rate plan.');
        for(zqu__QuoteChargeSummary__c chargeSummary : chargeSummaries)
        {
            system.assert(idToRatePlan.containsKey(chargeSummary.zqu__QuoteRatePlan__c), 'We expect the summary to be attached to the rate plan.');
            system.assertEquals(chargeSummary.zqu__QuoteRatePlan__r.zqu__QuoteAmendment__r.zqu__TotalAmount__c, chargeSummary.zqu__EffectivePrice__c, 'We expect the overage amount to be the event usage less included units, multiplied by the price.');
            system.assertEquals(chargeSummary.zqu__QuoteRatePlan__r.zqu__QuoteAmendment__r.zqu__TotalAmount__c, chargeSummary.zqu__TotalPrice__c, 'We expect the overage amount to be the event usage less included units, multiplied by the price.');
        }
    }*/
}