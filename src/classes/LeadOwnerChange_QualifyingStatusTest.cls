@isTest
private class LeadOwnerChange_QualifyingStatusTest {
    
    static Lead testLead;
    static User sdrUser;
    
   static void setupUser()
    {
       sdrUser = [SELECT Id FROM User WHERE IsActive = true AND Profile.Name = 'SDR' LIMIT 1];
    }
    static void setUpLead()
    {
        testLead = TestUtils.createLeads('TestCompany', 'Lead', 'Pending Assignment', true);
        system.debug('What is the Lead Status before Query'+ testLead.Status);
        testLead = [Select Id, Name, Status from lead where Id =: testLead.Id ][0];
       
        
    }
    
    
    static testMethod void testUpdateSingleLead_Accept()
    {
        setUpUser();
        system.runAs(sdrUser)
        {
            setUpLead();
        }
        
        
        //At this point we should have only 1 QHS sobject
        list<Lead> updatedLeads = [SELECT Id, Original_SDR__c, OwnerId, Status FROM Lead Where Id =:testLead.Id];
        list<Qualifying_Status_History__c> historyRecords = [SELECT Id, Previous_Qualifying_Status__c, Qualifying_Status__c FROM Qualifying_Status_History__c  ];
        system.assertEquals(1, historyRecords.size(), 'We expect there to be only one history record at this time');
        system.assertEquals(updatedLeads[0].Status, historyRecords[0].Qualifying_Status__c, 'We expect the history to capture the lead qualifying status');
        
        LeadOwnerChange_QualifyingStatus.hasTriggerRan = false;
        test.startTest();
        system.runAs(sdrUser)
        {
            testLead.Status = 'SDR Accept';
            
            update testLead;
            
        }
        test.stopTest();
        

        
        updatedLeads = [SELECT Id, Original_SDR__c, OwnerId, Status FROM Lead Where Id =: testLead.Id];
        system.assertEquals(sdrUser.Id, updatedLeads[0].Original_SDR__c, 'We expect the Original SDR field to hold the name of the current user');
        system.assertEquals(sdrUser.Id, updatedLeads[0].OwnerId, 'We expect the owner to match the Original SDR');
        
        historyRecords = [SELECT Id, Previous_Qualifying_Status__c, Qualifying_Status__c FROM Qualifying_Status_History__c Where Lead__c in :updatedLeads order by Name DESC];
        //Assert that we have two Qualifying Status history records at this point (1. Pending Assignment to Open (Open happens because lead is not from Eloqua so WFR fires)
        //2.) Open to SDR Accept which is executed in our test.)
        system.assertEquals(2, historyRecords.size(), 'We expect there to be two history records at this time');
        system.assertEquals(updatedLeads[0].Status, historyRecords[0].Qualifying_Status__c, 'We expect the history to capture the lead qualifying status');
    }
    
    static testMethod void testUpdateSingleLead_Disqualify()
    {
        setUpUser();
        system.runAs(AccountOwnerChange.Eloqua)
        {
            setUpLead();
        }
        
        list<Lead> updatedLeads = [SELECT Id, Original_SDR__c, OwnerId, Status FROM Lead Where Id =:testLead.Id];
        list<Qualifying_Status_History__c> historyRecords = [SELECT Id, Previous_Qualifying_Status__c, Qualifying_Status__c FROM Qualifying_Status_History__c ];
        //system.assertEquals(1, historyRecords.size(), 'We expect there to be only one history record at this time');
        //system.assertEquals(updatedLeads[0].Status, historyRecords[0].Qualifying_Status__c, 'We expect the history to capture the lead qualifying status');
        
        LeadOwnerChange_QualifyingStatus.hasTriggerRan = false;
        test.startTest();
        system.runAs(sdrUser)
        {
            testLead.Status = 'SDR DQ';
            
            update testLead;
            
        }
        test.stopTest();
        System.debug('Test Lead is :' + testLead);
        updatedLeads = [SELECT Id, Original_SDR__c, OwnerId, Status FROM Lead ];
        system.assertEquals(1, updatedLeads.size(), 'We expect there to be one lead');
        system.assertEquals(sdruser.Id,updatedLeads[0].Original_SDR__c,'We expect the user Id to be same as the SDR user');
        system.assertEquals(AccountOwnerChange.ELOQUA.Id, updatedLeads[0].OwnerId, 'We expect the owner to be Eloqua');
        historyRecords = [SELECT Id, Previous_Qualifying_Status__c, Qualifying_Status__c FROM Qualifying_Status_History__c Where Lead__c in :updatedLeads order by Name DESC];
        system.assertEquals(1, historyRecords.size(), 'We expect there to be two history records at this time');
        system.assertEquals(updatedLeads[0].Status, historyRecords[0].Qualifying_Status__c, 'We expect the history to capture the lead qualifying status');
    }
    
    static testMethod void testUpdateSingleLead_Qualified()
    {
        setUpUser();
        system.runAs(sdrUser)
        {
            setUpLead();
        }
        
        LeadOwnerChange_QualifyingStatus.hasTriggerRan = false;
        test.startTest();
        system.runAs(sdrUser)
        {
            testLead.For_RSE__c = TestUtils.SALES_USER.Id;
            testLead.Status = 'SDR Qualified';
            
            update testLead;
            
        }
        test.stopTest();
        
        list<Lead> updatedLeads = [SELECT Id, Original_SDR__c, OwnerId, Status FROM Lead where Id =:testLead.Id];
        system.assertEquals(1, updatedLeads.size(), 'We expect there to be one lead');
        system.assertEquals(TestUtils.SALES_USER.Id, updatedLeads[0].OwnerId, 'We expect the owner to be the new RSE');
        
        list<Qualifying_Status_History__c> historyRecords = [SELECT Id, Previous_Qualifying_Status__c, Qualifying_Status__c FROM Qualifying_Status_History__c Where Lead__c in :updatedLeads order by Name DESC  ];
        system.assertEquals(2, historyRecords.size(), 'We expect there to be two history records at this time');
        system.assertEquals(updatedLeads[0].Status, historyRecords[0].Qualifying_Status__c, 'We expect the history to capture the lead qualifying status');
    }
    
    static testMethod void testUpdateSingleLead_Negative()
    {
        setUpUser();
        system.runAs(sdrUser){
            setUpLead();
        }
        
        LeadOwnerChange_QualifyingStatus.hasTriggerRan = false;
        test.startTest();
        system.runAs(sdrUser)
        {
            testLead.Status = 'SDR Accept';
            testLead.LastName = null;
            
            try 
            {
                update testLead;
            }
            catch (DmlException e)
            {
                system.assert(e.getMessage().contains('REQUIRED_FIELD_MISSING'),
                                                      e.getMessage());
                
                system.assertEquals('REQUIRED_FIELD_MISSING' , e.getDmlStatusCode(0));
            }
            
        }
        test.stopTest();

    }
    
    static testMethod void testUpdateBulkLeads_Postive()
    {
        list<Lead> testLeads;
        setUpUser();
        /*system.runAs(sdrUser)
        {
            setUpLead();
        }
        LeadOwnerChange_QualifyingStatus.hasTriggerRan = false;*/
        
        //test.startTest();
        //system.runAs(sdrUser)
        //{
            testLeads = TestUtils.createLeadsMultiple(200, 'ACME', 'Lead', 'Pending Assignment', true);
            LeadOwnerChange_QualifyingStatus.hasTriggerRan = false;
            System.runAs(sdrUser){
                for(Lead aLead : testLeads)
                {
                    LeadOwnerChange_QualifyingStatus.hasTriggerRan = false;
                    aLead.Status = 'SDR Accept';
                }
                LeadOwnerChange_QualifyingStatus.hasTriggerRan = false;
                update testLeads;
            }
        //}
        //test.stopTest();
        
        list<Lead> updatedLeads = [SELECT Id, Status, OwnerId, Original_SDR__c FROM Lead where Id in: testLeads];
        system.assertEquals(200, updatedLeads.size(), 'We expect to have the correct number of leads (200)');
        for(Lead aLead : updatedLeads)
        {
            system.assertEquals(sdrUser.Id, aLead.Original_SDR__c, 'We expect the Original SDR field to hold the name of the current user');
           // system.assertEquals(sdruser.Id, aLead.OwnerId, 'We expect the owner to match the Original SDR');
        }
        
        list<Qualifying_Status_History__c> historyRecords = [SELECT Id, Previous_Qualifying_Status__c, Qualifying_Status__c FROM Qualifying_Status_History__c  where Lead__c in :testLeads];
        //2 QSH object will be created, One when the lead is created in Open status  and other when the status is changed.
        system.assertEquals(400, historyRecords.size(), 'We expect there to be the correct number of history records');

    }
        
    /* static testMethod void testLeadSourceValueChanges()
    {
        //set up methods
        User testUser = TestUtils.getTestUser('testUser', 'SDR');
        system.runAs(testUser)
        {
            setUpLead();
            system.debug('What is the Lead Status 2'+ testLead.Status);
        }
        
        LeadOwnerChange_QualifyingStatus.hasTriggerRan = false;
        test.startTest();
        system.runAs(testUser)
        {
            testLead.Campaign_Name__c = 'PPC';
            system.debug('What is the Lead Status 3'+ testLead.Status);
            update testLead;
            
        }
        test.stopTest();
        
        system.debug('What is the Lead Status 4'+ testLead.Status);
        list<Lead> updatedLeads = [SELECT Id, Most_Recent_Lead_Source__c, Lead_Source_Most_Recent_Detail__c,Lead_Source_Original_Detail__c , LeadSource 
                                   FROM Lead 
                                   WHERE Id =:testLead.Id];
        list<Qualifying_Status_history__c> historyRecords = [SELECT Id,Lead_Source_Original__c, Lead_Source_Original_Detail__c,Qualifying_Status__c,Lead_Source__c, Lead_SOurce_Most_Recent_Detail__c 
                                                             FROM Qualifying_Status_History__c  
                                                             WHERE Lead__c =:testLead.Id order by Name DESC  ];
        
        system.assertEquals(2, historyRecords.size(), 'We expect there to be two history records at this time');
        system.assertEquals(updatedLeads[0].Most_Recent_Lead_Source__c, historyRecords[0].Lead_Source__c, 'We expect the history to capture the lead Source Most Recent');
        system.assertEquals(updatedLeads[0].LeadSource, historyRecords[0].Lead_Source_Original__c, 'We expect the history to capture the lead Source Most Recent');
        system.assertEquals(updatedLeads[0].Lead_Source_Most_Recent_Detail__c, historyRecords[0].Lead_Source_Most_Recent_Detail__c, 'We expect the history to capture the lead Source Most Recent Detail');
        system.assertEquals(updatedLeads[0].Lead_Source_Original_Detail__c, historyRecords[0].Lead_Source_Original_Detail__c, 'We expect the history to capture the lead Source Original detail');
    }*/
    
  
}