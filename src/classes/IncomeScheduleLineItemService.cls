global with sharing class IncomeScheduleLineItemService {

    public static Boolean isTriggerRunning = false;

    public static void populateScheduleInvoiceLineItems(List<c2g__codaScheduleLineItem__c> newScheduleLineItems){

        Set<ID> invoiceIds = Pluck.ids('c2g__Invoice__c',newScheduleLineItems);
        Map<Id,c2g__codaInvoice__c> invoiceOpportunities = new Map<Id,c2g__codaInvoice__c>([Select id, c2g__Opportunity__c From c2g__codaInvoice__c where id in :invoiceIds ]);

        if (invoiceOpportunities != null && !invoiceOpportunities.isEmpty() ){
            for (c2g__codaScheduleLineItem__c newScheduleLineItem : newScheduleLineItems){
                if (invoiceOpportunities.get(newScheduleLineItem.c2g__Invoice__c)!= null){
                    newScheduleLineItem.Opportunity__c = invoiceOpportunities.get(newScheduleLineItem.c2g__Invoice__c).c2g__Opportunity__c;
                }
            }
        }
    }

    public static List<c2g__codaScheduleLineItem__c> processIncomeScheduleLineItemAmounts( List<c2g__codaScheduleLineItem__c> newIncomeScheduleLineItems )
    {
        List<c2g__codaScheduleLineItem__c > scheduleLineItems = new List<c2g__codaScheduleLineItem__c >(); 
        List<c2g__codaScheduleLineItem__c > scheduleLineItemsToDelete = new List<c2g__codaScheduleLineItem__c >(); 

        Map<Id, c2g__codaScheduleLineItem__c> incomeScheduleLineItems = new Map<Id, c2g__codaScheduleLineItem__c>( [ SELECT Id, Period_Month__c, c2g__Period__c, c2g__LineNumber__c,
                                                                                                                        c2g__Period__r.Name, SILI_End_Date_Month__c, Period_Change_Needed__c,
                                                                                                                        c2g__Amount__c, Sales_Invoice_Line_Item_End_Date__c, Sales_Invoice_Line_Item_Start_Date__c, SILI_Start_Date_Month__c,
                                                                                                                        c2g__SalesInvoiceLineItem__r.Price_Per_Day__c, c2g__Period__r.c2g__EndDate__c, c2g__Period__r.c2g__StartDate__c, 
                                                                                                                        c2g__Period__r.Days_in_Period__c, Opportunity__r.Billing_Type__c, Opportunity__r.Close_Date_Formula__c, 
                                                                                                                        Opportunity__r.Close_Date_Period__c, c2g__SalesInvoiceLineItem__r.c2g__UnitPrice__c, c2g__SalesInvoiceLineItem__r.c2g__Quantity__c,
                                                                                                                        c2g__SalesInvoiceLineItem__r.ffbilling__ScheduleNetTotal__c
                                                                                                                        FROM c2g__codaScheduleLineItem__c WHERE Id IN :newIncomeScheduleLineItems ] );

        
        Map<Id, c2g__codaPeriod__c> scheduleLineItemIdToNewPeriod = getNewPeriods( incomeScheduleLineItems.values() );

        for( c2g__codaScheduleLineItem__c aScheduleLineItem : incomeScheduleLineItems.values() )
        {
            //for invoices approved after contract start date. Need to update revenue schedule   
            String originalPeriod = aScheduleLineItem.c2g__Period__r.Name;
            if( aScheduleLineItem.Opportunity__r.Close_Date_Formula__c > 0 && aScheduleLineItem.Period_Change_Needed__c == 'true' )
            {
                aScheduleLineItem.c2g__Period__c = scheduleLineItemIdToNewPeriod.get( aScheduleLineItem.Id ).Id;
                aScheduleLineItem.Original_Contract_Period__c = originalPeriod;
            }
            
            String billingType = aScheduleLineItem.Opportunity__r.Billing_Type__c;
            Boolean isValidType = ( billingType == 'Annual' || billingType == 'Semi-Annual' || billingType == 'Quarterly' );
            
            if( !isValidType )
                continue;
                
            calculateAmount( aScheduleLineItem, aScheduleLineItem.Opportunity__r.Billing_Type__c );

            if( toDelete( aScheduleLineItem ) )
            {
                scheduleLineItemsToDelete.add( aScheduleLineItem );
            }
            else
            {
                scheduleLineItems.add( aScheduleLineItem );
            }
        }

        if( !scheduleLineItems.isEmpty() )
        {
            //TODO error handling
            update scheduleLineItems;
        }
        if( !scheduleLineItemsToDelete.isEmpty() )
        {
            //TODO : Error handling
            delete scheduleLineItemsToDelete;
        }
        return scheduleLineItems;
    }

    public static void calculateAmount( c2g__codaScheduleLineItem__c aScheduleLineItem, String billingType )
    {
        Boolean isAnnual = billingType == 'Annual';
        Boolean isOtherValidType = ( billingType == 'Monthly' || billingType == 'Semi-Annual' || billingType == 'Quarterly' );
        
        Date invoiceLineItem_StartDate = aScheduleLineItem.Sales_Invoice_Line_Item_Start_Date__c;
        Date invoiceLineItem_EndDate = aScheduleLineItem.Sales_Invoice_Line_Item_End_Date__c;
        Date period_StartDate = aScheduleLineItem.c2g__Period__r.c2g__StartDate__c;
        Date period_EndDate = aScheduleLineItem.c2g__Period__r.c2g__EndDate__c;
        
        Decimal period_Month = aScheduleLineItem.Period_Month__c;
        Decimal invoiceLineItem_StartMonth = aScheduleLineItem.SILI_Start_Date_Month__c;
        Decimal invoiceLineItem_EndMonth = aScheduleLineItem.SILI_End_Date_Month__c;
        
        Decimal pricePerDay = aScheduleLineItem.c2g__SalesInvoiceLineItem__r.Price_Per_Day__c;

        if( isAnnual )
        {
            if( ( aScheduleLineItem.c2g__LineNumber__c == 1 ) )
            {
                if( invoiceLineItem_EndDate >= period_EndDate )
                {
                    aScheduleLineItem.c2g__Amount__c = ( ( invoiceLineItem_StartDate.daysBetween( period_EndDate ) + 1 ) * pricePerDay ).setscale( 2 );
                }
                else
                {
                    aScheduleLineItem.c2g__Amount__c = ( ( invoiceLineItem_StartDate.daysbetween( invoiceLineItem_EndDate ) + 1 ) * pricePerDay ).setscale( 2 );
                }
            }

            else if( aScheduleLineItem.c2g__LineNumber__c > 1 )
            {
                if( invoiceLineItem_EndDate >= period_EndDate )
                {
                    aScheduleLineItem.c2g__Amount__c = ( aScheduleLineItem.c2g__Period__r.Days_in_Period__c * pricePerDay ).setscale( 2 );
                }
                else
                {
                    aScheduleLineItem.c2g__Amount__c = ( ( period_StartDate.daysbetween( invoiceLineItem_EndDate ) + 1 ) * pricePerDay ).setscale( 2 );
                }
            }
        }
        else if( isOtherValidType )
        {
            if( invoiceLineItem_StartMonth == period_Month )
            {
                if( invoiceLineItem_EndMonth != period_Month )
                {
                    aScheduleLineItem.c2g__Amount__c = ( ( invoiceLineItem_StartDate.daysBetween( period_EndDate ) + 1 ) * pricePerDay ).setscale( 2 );
                }
                else
                {
                    aScheduleLineItem.c2g__Amount__c = ( ( invoiceLineItem_StartDate.daysbetween( invoiceLineItem_EndDate ) + 1 ) * pricePerDay ).setscale( 2 );
                }
            }
            else
            {
                if( invoiceLineItem_EndMonth != period_Month )
                {
                    aScheduleLineItem.c2g__Amount__c = ( aScheduleLineItem.c2g__Period__r.Days_in_Period__c * pricePerDay ).setscale( 2 );
                }
                else
                {
                    aScheduleLineItem.c2g__Amount__c = ( ( period_StartDate.daysbetween( invoiceLineItem_EndDate ) + 1 ) * pricePerDay ).setscale( 2 );
                }
            }
        }
    }

    public static Boolean toDelete( c2g__codaScheduleLineItem__c aScheduleLineItem )
    {
        Date invoiceLineItem_EndDate = aScheduleLineItem.Sales_Invoice_Line_Item_End_Date__c;
        Date period_StartDate = aScheduleLineItem.c2g__Period__r.c2g__StartDate__c;
        Decimal period_Month = aScheduleLineItem.Period_Month__c;
        Decimal invoiceLineItem_StartMonth = aScheduleLineItem.SILI_Start_Date_Month__c;
        Decimal invoiceLineItem_EndMonth = aScheduleLineItem.SILI_End_Date_Month__c;

        return invoiceLineItem_StartMonth != period_Month && invoiceLineItem_EndMonth < period_Month && period_StartDate > invoiceLineItem_EndDate;
    }

    private static Set<String> pluckCloseDatePeriods( List<c2g__codaScheduleLineItem__c> incomeScheduleLineItems )
    {
        Set<String> closeDatePeriods = new Set<String>();
        for( c2g__codaScheduleLineItem__c aScheduleLineItem : incomeScheduleLineItems )
        {
            closeDatePeriods.add( aScheduleLineItem.Opportunity__r.Close_Date_Period__c );
        }
        return closeDatePeriods;
    }

    private static Map<Id, c2g__codaPeriod__c> getNewPeriods( List<c2g__codaScheduleLineItem__c> incomeScheduleLineItems )
    {
        Map<Id, c2g__codaPeriod__c> scheduleLineItemIdToPeriod = new Map<Id, c2g__codaPeriod__c>();

        Set<String> closeDatePeriodNames = pluckCloseDatePeriods( incomeScheduleLineItems );

        List<c2g__codaPeriod__c> periods = [ Select Id, Name from c2g__codaPeriod__c where Name IN :closeDatePeriodNames ];
        if( !periods.isEmpty() )
        {
            Map<String, List<c2g__codaPeriod__c>> periodNameToPeriods = GroupBy.strings( 'Name', periods );

            for( c2g__codaScheduleLineItem__c aScheduleLineItem : incomeScheduleLineItems )
            {
                scheduleLineItemIdToPeriod.put( aScheduleLineItem.Id, periodNameToPeriods.get( aScheduleLineItem.Opportunity__r.Close_Date_Period__c )[0] );
            }
        }
        
        return scheduleLineItemIdToPeriod;
    }

    public static void processAmountBalancing( List<c2g__codaScheduleLineItem__c> incomeScheduleLineItems )
    {
        Map<Id, List<c2g__codaScheduleLineItem__c>> scheduleLIsGroupedByInvoiceLI = GroupBy.Ids( 'c2g__SalesInvoiceLineItem__c', incomeScheduleLineItems );
        Map<Id, c2g__codaScheduleLineItem__c> invoiceLIToLastScheduleLI = new Map<Id, c2g__codaScheduleLineItem__c>();
        for( Id invoiceLI : scheduleLIsGroupedByInvoiceLI.keySet() )
        {
            List<ScheduleLineItemWrapper> scheduleLineItems =  new List<ScheduleLineItemWrapper>();
            for( c2g__codaScheduleLineItem__c aScheduleLineItem : scheduleLIsGroupedByInvoiceLI.get( invoiceLI ) )
            {
                scheduleLineItems.add( new ScheduleLineItemWrapper( aScheduleLineItem ) );
            }
            scheduleLineItems.sort();
            invoiceLIToLastScheduleLI.put( invoiceLI, scheduleLineItems[ scheduleLineItems.size() - 1 ].scheduleLineItem );
        }
        calculateBalanceAmounts( invoiceLIToLastScheduleLI );

        update invoiceLIToLastScheduleLI.values();
    }

    public static void calculateBalanceAmounts( Map<Id, c2g__codaScheduleLineItem__c> invoiceLIToLastScheduleLI )
    {
        Map<Id, Decimal> invoiceLIIdToTotalAmount = getTotalAmounts( invoiceLIToLastScheduleLI.keySet() );

        for( Id invoiceLI : invoiceLIToLastScheduleLI.keySet() )
        {
            c2g__codaScheduleLineItem__c lastScheduleLineItem = invoiceLIToLastScheduleLI.get( invoiceLI );
            
            Decimal totalPrice = lastScheduleLineItem.c2g__SalesInvoiceLineItem__r.c2g__UnitPrice__c * lastScheduleLineItem.c2g__SalesInvoiceLineItem__r.c2g__Quantity__c;
            Decimal totalAmount = invoiceLIIdToTotalAmount.get( invoiceLI );

            
            if( totalAmount != totalPrice )
            {
                populateBalanceAmount( lastScheduleLineItem, totalPrice, totalAmount );
            }
        }
    }

    private static void populateBalanceAmount( c2g__codaScheduleLineItem__c lastScheduleLineItem, Decimal totalPrice, Decimal totalAmount )
    {
        Decimal balance = totalPrice - totalAmount;
        lastScheduleLineItem.c2g__Amount__c += balance;
    }

    private static Map<Id, Decimal> getTotalAmounts( Set<Id> invoiceLIIds )
    {
        AggregateResult[] groupedResults  = [ SELECT SUM(c2g__Amount__c) amt, c2g__SalesInvoiceLineItem__c FROM c2g__codaScheduleLineItem__c 
                                                WHERE c2g__SalesInvoiceLineItem__c IN :invoiceLIIds
                                                GROUP BY c2g__SalesInvoiceLineItem__c ];
        Map<Id, Decimal> invoiceLIIdToTotalAmount = new Map<Id, Decimal>();
        for( AggregateResult aResult : groupedResults )
        {
            invoiceLIIdToTotalAmount.put( (Id)aResult.get( 'c2g__SalesInvoiceLineItem__c' ), (Decimal)aResult.get( 'amt' ) );
        }
        return invoiceLIIdToTotalAmount;
    }

    global class ScheduleLineItemWrapper implements Comparable 
    {
        public c2g__codaScheduleLineItem__c scheduleLineItem;

        // Constructor
        public ScheduleLineItemWrapper( c2g__codaScheduleLineItem__c scheduleLineItem ) 
        {
            this.scheduleLineItem = scheduleLineItem;
        }

        global Integer compareTo( Object compareTo ) 
        {
            ScheduleLineItemWrapper compareToOppy = (ScheduleLineItemWrapper)compareTo;

            // The return value of 0 indicates that both elements are equal.
            Integer returnValue = 0;
            if ( scheduleLineItem.c2g__LineNumber__c > compareToOppy.scheduleLineItem.c2g__LineNumber__c )
            {
                returnValue = 1;
            } 
            else if ( scheduleLineItem.c2g__LineNumber__c < compareToOppy.scheduleLineItem.c2g__LineNumber__c ) 
            {
                returnValue = -1;
            }

            return returnValue;       
        }
    }

}