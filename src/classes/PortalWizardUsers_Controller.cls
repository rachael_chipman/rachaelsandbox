public class PortalWizardUsers_Controller {

Public List<Contact> cons {get;set;}
Public Account theAcct {get;set;}
Public Account PAcct {get;set;}
Public Opportunity theOpp {get;set;}

Public PortalWizardUsers_Controller(){
      theAcct = [select Id, ParentId from Account where Id = :ApexPages.currentPage().getParameters().get('acct')];
      theOpp = [select Id from Opportunity where Id = :ApexPages.currentPage().getParameters().get('opp')];
      //PAcct = [select Id, CRM__r.Email, Owner.Email from Account where Id = :theAcct.ParentID];
      
      
      
    
    cons = new List<Contact>();
 cons.add(New Contact(
            RecordTypeId = '012000000004x7C',
            AccountId = ApexPages.currentPage().getParameters().get('acct')));
            
           
            
                     
        }
        
Public void addrow(){

if(cons.size() < 3){
     cons.add(New Contact(
         RecordTypeId = '012000000004x7C',
         AccountId = ApexPages.currentPage().getParameters().get('acct')));
     } 
     
    
}

 
public PageReference Next(){

if(test.isRunningTest() == true){
for(Contact c : cons){
c.LastName = 'test';}}

insert cons;

    
List<OpportunityContactRole> opptyroles = new List<OpportunityContactRole>();
for(Contact newcons : cons){
    opptyroles.add(new OpportunityContactRole(
        ContactId = newcons.Id,
        OpportunityId = ApexPages.currentPage().getParameters().get('opp'),
        Role = 'Partner Product User'));
        
    newcons.Contact_Role_Added__c = true;  
    update newcons;  
        }
       
insert opptyroles;    


 return new PageReference('/'+ApexPages.currentPage().getParameters().get('acct'));  
}
}