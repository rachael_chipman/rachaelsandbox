/*DESCRIPTION: This trigger looks for the related opportunity when an application 
is placed on hold or re-opened, so that the opportunity stage will correlate with the 
application*/

trigger FreezeUnfreezeOpp on Certification_Application__c (before update) {


Datetime myDT = Datetime.now();
String myDTstring = myDT.format('yyyy-MM-dd HH:mm', 'GMT'); 

    for(Certification_Application__c ca : trigger.new){
    
    //Get Application Previous Status
    Certification_Application__c prev = trigger.oldmap.get(ca.id);
    
    if(ca.Application_Status__c == 'On Hold' || (ca.Application_Status__c == 'Under Review' && prev.Application_Status__c == 'On Hold') && ca.License_Fee_Opportunity__c != null && ca.License_Fee_Opportunity__r.Trigger_Workflow_Time_Stamp__c != myDTstring){

    //Get related Opportunity
    Opportunity updateOpp = [SELECT Id, StageName, Awaiting_Approval__c, Approval_Type__c, Send_Monkey_Email__c, Submit_From_Trigger__c FROM Opportunity WHERE Id = :ca.License_Fee_Opportunity__c];
    
    //If the opp is pending approval - recall the request
    if(UpdateOpp.Awaiting_Approval__c == true){
    Approval.ProcessWorkItemRequest pwr = new Approval.ProcessWorkItemRequest();
    try{ProcessInstance procins = [SELECT Id from ProcessInstance where Status = 'Pending' and TargetObjectId = :UpdateOpp.Id];
            
            // Retrieve the process work instance id associated to the process instance
            List<ProcessInstanceWorkitem>  workitem = new list<ProcessInstanceWorkItem>([select Id from ProcessInstanceWorkitem where ProcessInstanceId = :procins.id]);
            
            if ((workitem != null) && (workitem.size() > 0))
            {
                pwr.SetComments('Recalled due to Cert App on hold.');

                pwr.setWorkItemId(workitem[0].id);
                pwr.setAction('Removed'); 
             
                
                // Execute process request
                Approval.ProcessResult pr = Approval.process(pwr);}
                
        }
            catch (Exception e) {
            ca.addError (e.getMessage());
            }
        }
        //If the Application Status is "On Hold" update the opportunity Stage to "On Hold"
        if(ca.Application_Status__c == 'On Hold' && prev.Application_Status__c != 'On Hold'){
        
            updateOpp.StageName = 'On Hold';
            update UpdateOpp;
            
            }
    
        //If the Application Status is "Under Review"  and just reached that from a previous Status of "On Hold" update the Opportunity Stage accordingly       
        if(ca.Application_Status__c == 'Under Review' && ca.Record_Locked__c == false && ca.Freeze_Application__c == false && updateOpp.StageName == 'On Hold'){
            
            //If it's not ever been approved (was not in Pending Cert) then move to OC and submit for approval (to Finance)                   
            if(updateOpp.Send_Monkey_Email__c != 'Monkey Sent'){updateOpp.StageName = 'Obtaining Commitment';
            updateOpp.Submit_From_Trigger__c = 'Yes';
            updateOpp.Trigger_Workflow_Time_Stamp__c = myDTstring;}
            //Otherwise just move the opp back to Pending Cert Stage
            else{
            updateOpp.StageName = 'Pending Certification Qualification';}
            update UpdateOpp;
            } 
            
            }

            }      
}