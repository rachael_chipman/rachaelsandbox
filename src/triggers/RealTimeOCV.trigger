trigger RealTimeOCV on Opportunity (before insert, before update) {
    system.debug('trigger RealTimeOCV start');
    
    for(Opportunity o : [select id, name, amount, stagename, opportunity_contract_value__c, Run_OCV__c, previous_value__c, (select id, quantity, unitprice, Previous_Value__c, product_status__c from opportunitylineitems) from opportunity where id in :trigger.new]) {
        
        if(o.stagename != 'Finance Approved' || o.Run_OCV__c == true) {
            system.debug('for opportunity ' + o.name + ' ' + o.id);
                            
            double ocv = 0;
            double prev = 0;
                                
            for(opportunitylineitem oli : o.opportunitylineitems) {
                system.debug('for line item ' + oli.id);
                if(oli.product_status__c == 'Current' || oli.product_status__c == 'Terminated') {
                    system.debug('product status is current or terminated');
                    ocv += (oli.quantity * oli.unitprice);
                    system.debug('ocv is now ' + ocv);
                    prev += (oli.quantity * (oli.Previous_Value__c != null ? oli.Previous_Value__c : 0));
                    system.debug('prev is now ' + prev);
                }
            }
                                
            trigger.newMap.get(o.id).opportunity_contract_value__c = ocv;
            system.debug('setting opportunity ' + o.name + ' ' + o.id + ' contract value to ' + ocv);
            trigger.newMap.get(o.id).previous_value__c = prev;
            system.debug('setting opportunity ' + o.name + ' ' + o.id + ' previous value to ' + prev);
                
            system.debug('trigger RealTimeOCV end');
        }
        
    }

}