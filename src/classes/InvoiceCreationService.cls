public with sharing class InvoiceCreationService 
{
	public static Map<Id, c2g__codaAccountingCurrency__c> getInvoiceCurrencies( List<Opportunity> filterOpportunities, Map<Id, Account> oppIdToPartner, Map<String, c2g__codaAccountingCurrency__c> currencyMap )
    {
        Map<Id, c2g__codaAccountingCurrency__c> oppIdToInvoiceCurrency = new Map<Id, c2g__codaAccountingCurrency__c>();
       
        for( Opportunity o : filterOpportunities )
        {
            c2g__codaAccountingCurrency__c opportunityCurrency = currencyMap.get( o.CurrencyIsoCode  );
            oppIdToInvoiceCurrency.put( o.Id, opportunityCurrency );

            if ( o.Who_is_Invoiced__c == OpportunityServices.PARTNER_INVOICED && o.PartnerLookup__c != NULL && o.CurrencyISOCode != oppIdToPartner.get( o.Id ).CurrencyISOCode )
            {
                c2g__codaAccountingCurrency__c partnerCurrency = currencyMap.get( oppIdToPartner.get( o.Id ).CurrencyISOCode  );
                oppIdToInvoiceCurrency.put( o.Id, partnerCurrency );
            }
        }
        return oppIdToInvoiceCurrency;
    }

    public static Map<Id, List<c2g__codaInvoice__c>> createOpportunityInvoices( List<Opportunity> filterOpportunities, Map<Id, c2g__codaAccountingCurrency__c> oppIdToCurrency, Map<Id, Account> oppIdToPartner )
    {
        Map<Id, List<c2g__codaInvoice__c>> invoicesToBeInserted = new Map<Id, List<c2g__codaInvoice__c>>();
        

        for(Opportunity filterOpp : filterOpportunities)
        {
            Id relatedAccountId = filterOpp.AccountId;
            if( oppIdToPartner.containsKey( filterOpp.Id ) )
            {
                relatedAccountId = oppIdToPartner.get( filterOpp.Id ).Id;
            }
            // get each opp's invoice dates
            invoicesToBeInserted.put( filterOpp.Id, getCodaInvoiceFromOpps( filterOpp, oppIdToCurrency.get( filterOpp.Id ), relatedAccountId ) );
        }
        return invoicesToBeInserted;
    }

    public static List<c2g__codaInvoice__c> getCodaInvoiceFromOpps(Opportunity opp, c2g__codaAccountingCurrency__c cur, Id relatedAccountId )
    {
        List<Date> invoiceDates = getInvoiceDates(opp);

        List<c2g__codaInvoice__c> codaInvoices = new List<c2g__codaInvoice__c>();
        String billingType = opp.Billing_Type__c;

        Date currentStartDate = opp.Minimum_Start_Date__c;
        Date currentEndDate = currentStartDate.addMonths( OpportunityServices.BILLING_INTERVAL_MAP.get( billingType ) ).addDays(-1);
        for(Integer i = 0; i < invoiceDates.size(); i++)//Date invoiceDate : invoiceDates)
            {
            c2g__codaInvoice__c invoice = new c2g__codaInvoice__c(
            c2g__OwnerCompany__c = OpportunityServices.RETURN_PATH_COMPANY.Id,
            c2g__Account__c = relatedAccountId,
            c2g__Opportunity__c = opp.Id,
            Opportunity_Account__c = opp.AccountId,
            ffbilling__CopyAccountValues__c = true,
            ffbilling__DeriveCurrency__c = false,
            c2g__InvoiceCurrency__c = cur.Id,
            c2g__InvoiceDate__c = invoiceDates[i],
            c2g__DueDate__c = invoiceDates[i].addDays( Integer.valueof(opp.Billing_Terms_Number_Calc__c) ),
            Contract_Service_Period_Start_Date__c = currentStartDate,
            Contract_Service_Period_End_Date__c = currentEndDate,
            ffbilling__DerivePeriod__c = true,
            ffbilling__DeriveDueDate__c = false,
            Invoicing_Contact__c = opp.Account.Invoicing_Contact__c,
            c2g__InvoiceStatus__c = 'In Progress',
            c2g__PrintStatus__c = 'Not Printed',
            PO_Number__c = opp.PO_Number__c );
                     

            if( i == invoiceDates.size()-1 && opp.Max_End_Date__c < currentEndDate )
            {
                //invoice.Contract_Service_Period_End_Date__c = opp.Max_End_Date__c >= invoiceDates[i+1] ? currentEndDate.addDays(-1) : opp.Max_End_Date__c;
                invoice.Contract_Service_Period_End_Date__c = opp.Max_End_Date__c;
            }

            if(billingType == OpportunityServices.MONTHLY)
            {
                invoice.Ready_For_Posting__c = true ;
            }
            // Increment date according to billingType
            currentStartDate = currentStartDate.addMonths( OpportunityServices.BILLING_INTERVAL_MAP.get( billingType ) );
            currentEndDate = currentEndDate.addMonths( OpportunityServices.BILLING_INTERVAL_MAP.get( billingType ) );

            if( opp.Who_is_Invoiced__c == OpportunityServices.PARTNER_INVOICED && opp.PartnerLookup__c != NULL )
            {
                invoice.Partner_is_Billing_Agent__c = true;
            }

            codaInvoices.add(invoice);
            }
        return codaInvoices;
    }

    public static List<Date> getInvoiceDates(Opportunity opp)
    {
        List<Date> invoiceDates = new List<Date>();
        String billingType = opp.Billing_Type__c;
        Integer monthInterval = OpportunityServices.BILLING_INTERVAL_MAP.get( billingType );
        Date startDate = opp.Minimum_Start_Date__c;

        Integer counter = 0;
        /*
                  To convert from makiCheckng a constant number of invoices simply change loop
                  to startDate < opp.Maximum_Date__c
         */
        while( counter < OpportunityServices.BILLING_TYPE_MULTIPLES.get( billingType ) )
        {
            if( ( billingType == OpportunityServices.QUARTERLY || billingType == OpportunityServices.SEMI_ANNUALLY  ) && (opp.Max_End_Date__c < startDate) )
                break;
            invoiceDates.add( System.today() < startDate ? startDate : System.today() );
            startDate = startDate.addMonths( monthInterval );
            counter++;
        }
        return invoiceDates;
    }
}