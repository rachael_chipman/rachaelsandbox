@isTest
public class testSetLeadAsPrimaryContactRoleTrigger {
    static testMethod void insertLead(){
                Lead leadToCreate = new Lead();
       leadToCreate.FirstName = 'Ali';
       leadToCreate.LastName  = 'Hafez';
       leadToCreate.Email     = 'ali@testtrigger.com';
       leadToCreate.Company = 'Test Company';

        insert leadToCreate;
    
        Database.LeadConvert lc = new Database.LeadConvert();
        lc.setLeadId(leadToCreate.id);

        LeadStatus convertStatus = [SELECT Id, MasterLabel FROM LeadStatus WHERE IsConverted=true LIMIT 1];
        lc.setConvertedStatus(convertStatus.MasterLabel);

        Database.LeadConvertResult lcr = Database.convertLead(lc);
        System.assert(lcr.isSuccess());   
    }           
    
    static testMethod void convertLeadNoOpp(){
                Lead leadToCreate = new Lead();
       leadToCreate.FirstName = 'Ali';
       leadToCreate.LastName  = 'Hafez';
       leadToCreate.Email     = 'ali@testrigger.com';
       leadToCreate.Company = 'Test Company';

        insert leadToCreate;
    
        Database.LeadConvert lc = new Database.LeadConvert();
        lc.setLeadId(leadToCreate.id);

        LeadStatus convertStatus = [SELECT Id, MasterLabel FROM LeadStatus WHERE IsConverted=true LIMIT 1];
        lc.setConvertedStatus(convertStatus.MasterLabel);
    lc.setDoNotCreateOpportunity(true);

        Database.LeadConvertResult lcr = Database.convertLead(lc);
        System.assert(lcr.isSuccess());   
    }

}