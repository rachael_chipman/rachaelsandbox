global class BatchToCreateJournals implements Database.Batchable<sObject>, Database.Stateful {
	
	String query;

	global Iterable<sObject> start(Database.BatchableContext BC) {
		query = 'SELECT Id, c2g__InvoiceCurrency__c, c2g__Opportunity__c, Contract_Service_Period_Start_Date__c, Contract_Service_Period_End_Date__c, Opportunity_Exchange_Rate__c, Partner_Exchange_Rate__c, Partner_Commission_Amount__c' +
				 ' FROM c2g__codaInvoice__c WHERE Ready_For_Journals__c = true';
		return Database.query(query);
	}

   	global void execute(Database.BatchableContext BC, List<c2g__codaInvoice__c> scope) 
   	{
   		c2g__codaInvoice__c invoice = scope[0];
   		invoice.Ready_For_Journals__c = false;
		try
		{
			update invoice;
		}
		catch( System.DmlException ex )
		{
			for ( Integer index = 0 ; index < ex.getNumDml() ; index++ )
            {
                JournalCreationService.errors += ( ' ' + ex.getDmlMessage( index ) );
            }
            return;
		}

   		List<Opportunity> relatedOpportunities =  [ SELECT Id, CurrencyISOCode, PartnerLookup__c, Who_is_Invoiced__c, 
													( SELECT Id, OpportunityId, UnitPrice, CurrencyISOCode FROM OpportunityLineItems WHERE PricebookEntry.Product2.ProductCode = :OpportunityServices.PRODUCT_CODE_REFERRAL LIMIT 1 )
												FROM Opportunity WHERE Id = :invoice.c2g__Opportunity__c ];
		if( relatedOpportunities.isEmpty() || relatedOpportunities[0].OpportunityLineItems.isEmpty() )
		{
			return;
		}
		JournalCreationService.createJournalsAndLineItems( relatedOpportunities[0], invoice );
	}
	
	global void finish(Database.BatchableContext BC) 
	{
		if ( String.isNotBlank( JournalCreationService.errors ) )
        {
            AsyncApexJob job = [ SELECT Id, Status, NumberOfErrors, JobItemsProcessed, TotalJobItems, CreatedBy.Email
                                 FROM AsyncApexJob
                                 WHERE Id =: bc.getJobId()];

            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();

            mail.setToAddresses( new String[] { /*job.CreatedBy.Email*/ 'miss.urvitanna@gmail.com' } );
            mail.setSubject( 'BatchToCreateJournals: ' + job.Status );
            mail.setPlainTextBody( '\n\n\nThe following errors occured:\n' + JournalCreationService.errors );

            List<Messaging.SingleEmailMessage> mailsToSend = new List<Messaging.SingleEmailMessage>{ mail };
            Messaging.sendEmail( mailsToSend );
        }
		
	}
	
}