/**
 * Sample code of splitting single Quote(MasterQuote) into two Quotes(SubQuote) and sending to Z-Billing.
 * 
 * User case
 *    a. The customers have two categories of Rate Plans (e.g. Regular/Power) that need to send to Z-Billing according to the categories but track using one MasterQuote. 
 *    b. Correspondingly to the two categories, there will be two SubQuotes. In the sample code, we send the two SubQuotes simultaneously to Z-Billing, and there will be two
 *       Subscriptions generated after sending to Z-Billing. 
 *    c. The Subscription will generated on single existing Billing Account;
 *    d. The Subscription Zuora IDs will be write back to the SubQuotes then roll up to the MasterQuote, among them, 
 *       the "Regular" Subscription's ID will be stored in MasterQuote's zqu__ZuoraSubscriptionID__c, and the "Power" Subscription's ID will be stored in the MasterQuotes' Extended_Zuora_Subscription_ID__c.
 *    e. The SubQuotes will be deleted after subscribing, and only MasterQuote will be stored.
 *
 * Pre-condition
 *    a. Set the value of the custom field Category__c in the zqu__ProductRatePlan__c, you can choose the value "Regular" or "Power", and the split rule will depend on this value, 
 *       please note that in the sample code we will handle the null value case as default one, which is "Regular";
 *    b. Create a "New Subscription" type Quote with multiple Rate Plans in upper Categories, and the Quote ID will be used as parameter during the API call;
 *    c. Create a Billing Account on Z-Billing side, and the Billing Account Zuora ID will be used as parameter during the API call;
 * 
 */
global class ExtendedZQuoteUtil {

  global static final String CATEGORY_REGULAR = 'Regular';

  global static final String CATEGORY_POWER = 'Power';
}