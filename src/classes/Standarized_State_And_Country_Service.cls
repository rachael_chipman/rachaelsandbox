public without sharing class Standarized_State_And_Country_Service {
//We want full country names and we want state abbreviations, confirmed with Marvin on 11/10/2015
	public static map<string,list<String>> Country_State_Map;
	public static list<String> StateOnly;
    public static List<Standardized_State_Country_Values__c>  Standard_State_Country;
    public static List<Standardized_State_Country_Values__c>  standardCountryAndStateRecords;
    public static FINAL String INVALID_COUNTRY = 'Invalid country';
    public static FINAL String INVALID_STATE = 'Invalid state';
    public static FINAL String NO_CHANGE = 'No Change';
    public static  String ERR_MSG = 'Invalid Country or State. Please enter a valid Country and State in the  ';
    public static boolean IsExemptUser = false;
    public static boolean isAccRec= false;
    public static boolean isValidated = false;
    public static Map<Id,User> usermap;
    public static String ACCT_PREFIX = '001';
    public static String CON_PREFIX = '003'; 
    
    public static boolean ExemptUser()
    {
        List<User> currentUser = [Select Id,Name,Bypass_Validation_Rules__c from User where Id = :UserInfo.getUserId()];
        usermap = new Map<Id,User>(currentUser);
    	System.debug('UserInfo is :' +UserInfo.getUserId());
        
       
    	List<Automation_User__c> exemptuserlist = Automation_User__c.getall().values();
    	for(Automation_User__c rec: exemptuserlist)
    	{
    		String UserId = UserInfo.getUserId();
    		String UserId15char = UserId.substring(0, UserId.length()-3);
            boolean bypassVA_rules = false; 
            if(usermap.get(UserInfo.getUserId())!= null)
            {
                   bypassVA_rules = usermap.get(UserInfo.getUserId()).Bypass_Validation_Rules__c;
            }
    		if(rec.Exempt_User_Id__c == UserId || rec.Exempt_User_Id__c == UserId15char || bypassVA_rules )
    		{
    			IsExemptUser = true;
    			
    			System.debug('Value of the exempt user is :' +UserId15char);
    			break;
    		}
    	
    		
    	}
        System.debug('Exempt user is:' +IsExemptUser);
    	return IsExemptUser;
    }
	/*public static map<String,String>  ValidateAddress(String Country, String State)
	{
		Map<String,String> Country_State_Abbrv = new Map<String,String>();
		//Map<String,Standard_State_Country> Country_State = Standardized_State_Country_Values__c.getAll();
		Standard_State_Country = Standardized_State_Country_Values__c.getall().values();
		//System.debug('Values of the custom setting is :' + Standard_State_Country);
		Set<String> countryNames = pluck.Strings('Country__c', Standard_State_Country);
		system.debug('Show country names: ' + countryNames);
		System.debug('Value of State is: ' +State);
		System.debug('Value of Country is :' +Country);
		for(Standardized_State_Country_Values__c  value : Standard_State_Country)
		{
				if(Country!= null )
				{
					//look for the exact match
					if(Country.equalsIgnoreCase(value.Country__c) && State!= null )
					{
						system.debug('Show me the value: ' + value);
						if(State.equalsIgnoreCase(value.State__c))
						{
							system.debug('Show me the value again: ' + value);
								system.debug('show me the values when I land here: ' + Country + ' - ' + State);
								//Country_State_Abbrv.put('CountryCode',value.Country__c);
								Country_State_Abbrv.put('CountryCode', NO_CHANGE);
								//Country_State_Abbrv.put('StateCode',value.State_Code__c);
								Country_State_Abbrv.put('StateCode', NO_CHANGE);
								break;
							
						}
						else if(State.equalsIgnoreCase(value.State_Code__c))
						{
							
								//Country_State_Abbrv.put('CountryCode',value.Country__c);
								Country_State_Abbrv.put('CountryCode',NO_CHANGE);
								//Country_State_Abbrv.put('StateCode',NO_CHANGE);
								Country_State_Abbrv.put('StateCode', value.State__c);
								break;
							
						}
						else if(Country.equalsIgnoreCase(value.Country__c) && !State.equalsIgnoreCase(value.State_Code__c) && !State.equalsIgnoreCase(value.State__c) )
						{
							Country_State_Abbrv.put('CountryCode',NO_CHANGE);
							Country_State_Abbrv.put('StateCode',NO_CHANGE);
							break;
						}
					}
					//if state is blank and country matches either country value, country code2 country cpde 3, do not valudate state	
					else if((Country.equalsIgnoreCase(value.Country_Code_2__c) || Country.equalsIgnoreCase(value.Country_Code_3__c) || Country.equalsIgnoreCase(value.Country__c)) && State == null )
					{
						Country_State_Abbrv.put('StateCode',NO_CHANGE);

						/*if(Country.equalsIgnoreCase(value.Country__c))
							Country_State_Abbrv.put('CountryCode',value.Country_Code_3__c)	;

						else
							Country_State_Abbrv.put('CountryCode',NO_CHANGE);*/

						/*Country_State_Abbrv.put('CountryCode',value.Country__c);

			    		
				        //break;
					}

					//if exact match for country code 2 or country code 3 and state cpde is found , just save the record
					else if((Country.equalsIgnoreCase(value.Country_Code_2__c) || Country.equalsIgnoreCase(value.Country_Code_3__c)) && (State.equalsIgnoreCase(value.State_Code__c)))
					{
							Country_State_Abbrv.put('CountryCode',value.Country__c)	;
							//Country_State_Abbrv.put('CountryCode',NO_CHANGE)	;
					    	//Country_State_Abbrv.put('StateCode',NO_CHANGE);
					    	Country_State_Abbrv.put('StateCode', value.State__c);
							break;
					}
					else if((Country.equalsIgnoreCase(value.Country_Code_2__c) || Country.equalsIgnoreCase(value.Country_Code_3__c)) && (State.equalsIgnoreCase(value.State__c)))
					{
							//Country_State_Abbrv.put('CountryCode',NO_CHANGE)	;
							system.debug('I am in this condition - right?' + Country + '-' + State);
							Country_State_Abbrv.put('CountryCode',value.Country__c);
					    	//Country_State_Abbrv.put('StateCode',value.State_Code__c);
					    	Country_State_Abbrv.put('StateCode',NO_CHANGE);
							break;
					}
					else if((Country.equalsIgnoreCase(value.Country_Code_2__c) || Country.equalsIgnoreCase(value.Country_Code_3__c)) && !(State.equalsIgnoreCase(value.State__c)))
					{
							//Country_State_Abbrv.put('CountryCode',NO_CHANGE)	;
							Country_State_Abbrv.put('CountryCode',value.Country__c);
					    	break;
					}

					
			    }
			    //if state is not blank and country is blank, do not validate Country	
			    else if(Country == null && State!= null)
			    {
			    	if(State.equalsIgnoreCase(value.State__c))
			    	{
			    		//Country_State_Abbrv.put('StateCode',value.State_Code__c);
			    		Country_State_Abbrv.put('StateCode', NO_CHANGE);
			    		Country_State_Abbrv.put('CountryCode',NO_CHANGE);
			    		break;
			    	}
			    	else if(State.equalsIgnoreCase(value.State_Code__c))
			    	{
			    		Country_State_Abbrv.put('CountryCode',NO_CHANGE);
			    		Country_State_Abbrv.put('StateCode',value.State__c);
			    		//Country_State_Abbrv.put('StateCode',NO_CHANGE);
			    		break;
			    	}

			    }
			   
		
		}

		if(Country_State_Abbrv.isEmpty())
		{
			if(Country!= null)
				Country_State_Abbrv.put('CountryCode', INVALID_COUNTRY);
			if(State!= null)
				Country_State_Abbrv.put('StateCode',INVALID_STATE);
		}
		
		isValidated = true;
		System.debug('Returnmimg ABBreviation is :' + Country_State_Abbrv);
		return Country_State_Abbrv;
	}*/

	public static Map<String,String> ValidateAddress(String country, String state)
	{
		Map<String,String> fieldToUpdateValue = new Map<String,String>();
		standardCountryAndStateRecords = Standardized_State_Country_Values__c.getAll().values();
		Set<String> countryNames = pluck.Strings('Country__c', StandardCountryAndStateRecords);
		Set<String> stateCodes = pluck.Strings('State_Code__c', StandardCountryAndStateRecords);

		Map<String, String> countryCode2ToCountry = new Map<String, String>();
		Map<String, String> countryCode3ToCountry = new Map<String, String>();
		Map<String, String> stateNameToStateCode =  new Map<String, String>();

		if(country != NULL)
		{
			//Check to see if the country value is in the set of countryNames
			if(countryNames.contains(country))
			{
				fieldToUpdateValue.put('CountryName', NO_CHANGE);
			}
			else
			{
				for(Standardized_State_Country_Values__c valueRecord : standardCountryAndStateRecords)
				{
					countryCode2ToCountry.put(valueRecord.Country_Code_2__c, valueRecord.Country__c);
					countryCode3ToCountry.put(valueRecord.Country_Code_3__c, valueRecord.Country__c);
				}
				if(!countryCode2ToCountry.isEmpty() && countryCode2ToCountry.containsKey(country))
				{
					//get the country name
					fieldToUpdateValue.put('CountryName', countryCode2ToCountry.get(country));
				}
				else if(!countryCode3ToCountry.isEmpty() && countryCode3ToCountry.containsKey(country))
				{
					//get the country name
					fieldToUpdateValue.put('CountryName', countryCode3ToCountry.get(country));
				}
				else
				{
					fieldToUpdateValue.put('CountryName', INVALID_COUNTRY);
				}
			}
		}
		else
		{
			fieldToUpdateValue.put('CountryName', NO_CHANGE);
		}
		if(state != NULL)
		{
			//Check to see if the country value is in the set of countryNames
			if(stateCodes.contains(state))
			{
				fieldToUpdateValue.put('StateCode', NO_CHANGE);
			}
			else //If its not then get a map of all the state names to state codes
			{
				for(Standardized_State_Country_Values__c valueRecord : standardCountryAndStateRecords)
				{
					stateNameToStateCode.put(valueRecord.State__c, valueRecord.State_Code__c);
				}
				//If the map is not empty then get the state code value that matches our state variable
				if(!stateNameToStateCode.isEmpty() && stateNameToStateCode.containsKey(state))
				{
					//get state code
					fieldToUpdateValue.put('StateCode', stateNameToStateCode.get(state));
				}
				else
				{
					fieldToUpdateValue.put('StateCode', INVALID_STATE);
				}
			}
		}
		else
		{
			fieldToUpdateValue.put('StateCode', NO_CHANGE);
		}
		return fieldToUpdateValue;
	}

	public static void ProcessContactTrigger( Map<Id,Contact> oldconmap,List<Contact> newconlist)
	{
		Map<String,String> resultmap ;
		String Countrycode;
    	String StateCode;
    	boolean isUpdated = false;
    	System.debug('Old Con Map is :' +oldconmap);
    	System.debug('value of isvalidated is '  +isValidated);
    	//call the exemptUser function
    	ExemptUser();
    	//isAccRec = false;
		List<Contact> ValidateList = new List<Contact>();
		if(newconlist!= null && !newconlist.isEmpty() && !IsExemptUser)
		{
			System.debug('value of isvalidated is '  +isValidated);
			for(Contact Tcon: newconlist)
			{
				if(oldconmap!= null && !oldconmap.isEmpty() && !TCon.Zuora_Information_Locked__c)
				{
					System.debug('value of isupadted is '  +isUpdated);
					Contact oldCon = oldconmap.get(Tcon.Id);
					if(oldCon!= null && (oldCon.MailingCountry!= Tcon.MailingCountry || oldCon.MailingState!= Tcon.MailingState || (oldCon.MailingCountry!= null && oldCon.MailingCountry.length() <= 3)) && (Tcon.Invoicing_Contact_Type__c!= 'Bill To' || Tcon.Invoicing_Contact_Type__c!= 'Sold To'))
					{
						System.debug('Calling the method');
						resultmap = ValidateAddress(Tcon.MailingCountry, Tcon.MailingState);
						updateContactValues(Tcon,resultmap);
					  
					}

				}
				else if(oldconmap == null)
				{
					System.debug('Country and State is :' +Tcon.MailingCountry +  '  ' +Tcon.MailingState);
					resultmap = ValidateAddress(Tcon.MailingCountry, Tcon.MailingState);
					updateContactValues(Tcon,resultmap);
				}
			}
				
		}
	}

	public static void ProcessAccountTrigger(Map<Id,Account> oldaccmap, List<Account> newacclist)
	{
		Map<String,String> resultmap ;
		//String Countrycode;
    	//String StateCode;
    	//boolean isUpdated = false;
    	isAccRec = true;
    	ExemptUser();
		system.debug('Show me newacclist ' + newacclist);
		system.debug('What is the value of the boolean: ' + stageTransitionController.doNotUpdateAddress);
		if(newacclist!= null && !newacclist.isEmpty() && !stageTransitionController.doNotUpdateAddress)
		{
			for(Account Tacc: newacclist)
		    {
				if(oldaccmap!= null && !oldaccmap.isEmpty() &&  !IsExemptUser && !Tacc.Zuora_Information_Locked__c)
				{
					Account oldAcc = oldaccmap.get(Tacc.Id);
					if(oldAcc!= null)
					{
						String Oldcountry = oldAcc.ShippingCountry;
						if(oldAcc.ShippingCountry!= Tacc.ShippingCountry || oldAcc.ShippingState!= TAcc.ShippingState || ( Oldcountry!= null && Oldcountry.length() <= 3 ))
						{
							System.debug('Calling the method');
							resultmap = ValidateAddress(Tacc.ShippingCountry, Tacc.ShippingState);
							updateAccountValues(Tacc, resultmap, 'Shipping');
						  
						}
						if(oldAcc.BillingCountry!= Tacc.BillingCountry || oldAcc.BillingState!= TAcc.BillingState ||  (oldAcc.BillingCountry!= null && oldAcc.BillingCountry.length() <= 3))
						{
							System.debug('Calling the method');
							resultmap = ValidateAddress(Tacc.BillingCountry, Tacc.BillingState);
							System.debug('RESULTMAP is :' +resultmap);
							updateAccountValues(Tacc, resultmap, 'Billing');

						}
					}
				}
				else if(oldaccmap == null)
				{	
					system.debug('What are my state and country values? ' + Tacc.BillingCountry + Tacc.BillingState);
					resultmap = ValidateAddress(Tacc.ShippingCountry, Tacc.ShippingState);
					//update the shipping address
					updateAccountValues(Tacc, resultmap,'Shipping');
		    		//update the Billing address
		    		resultmap = ValidateAddress(Tacc.BillingCountry, Tacc.BillingState);
		    		updateAccountValues(Tacc, resultmap, 'Billing');
				}
			}
		}
	}

	public static void updateAccountValues(Account TAcc, map<String,String> resultmap, String addType)
	{
		//String CountryCode;
		String countryName;
    	String StateCode;
    	//reset the error msg at the accont obje
    	//TAcc.addError('');
		if(addType == 'Billing')
		{
			if(resultmap!= null && !resultmap.isEmpty() && !TAcc.Zuora_Information_Locked__c)
	    	{

	    		//CountryCode = resultmap.get('CountryCode');
	    		countryName = resultmap.get('CountryName');
	    		StateCode = resultmap.get('StateCode');
	    		System.debug('Country Name is :' + countryName);
	    		System.debug('State Code is :' + StateCode);
	    		if((countryName == INVALID_COUNTRY || StateCode == INVALID_STATE) && IsExemptUser!= true)
	    		//if((Countrycode == INVALID_COUNTRY || StateCode == INVALID_STATE) && IsExemptUser!= true)
	    		{
	    			TAcc.addError(ERR_MSG + 'Billing address.');
	    		}
	    		if(countryName != null && countryName != NO_CHANGE && countryName != INVALID_COUNTRY)
	    		//if(CountryCode != null && CountryCode!= NO_CHANGE && CountryCode!= INVALID_COUNTRY)
    			{
    				//System.debug('BEFORE Setting the Billing Country to:' +CountryCode);
    				//TAcc.BillingCountry = CountryCode;
    				TAcc.BillingCountry = countryName;
    				//System.debug('AFTER Setting the Billing Country to:' +TAcc.BillingCountry);
    			}
	    		if(StateCode != null && StateCode != NO_CHANGE && StateCode!= INVALID_STATE)
	    		{
	    			TAcc.BillingState = StateCode;
	    		}
	    	}

		}
		else if(addType == 'Shipping')
		{
			if(resultmap!= null && !resultmap.isEmpty())
	    	{
	    		//CountryCode = resultmap.get('CountryCode');
	    		countryName = resultmap.get('CountryName');
	    		StateCode = resultmap.get('StateCode');
	    		System.debug('Country Name is :' + countryName);
	    		System.debug('State Code is :' + StateCode);
	    		if((countryName == INVALID_COUNTRY || StateCode == INVALID_STATE) && IsExemptUser!= true)
	    		//if((CountryCode == INVALID_COUNTRY || StateCode == INVALID_STATE) && IsExemptUser!= true)
	    		{
	    			TAcc.addError(ERR_MSG + 'Shipping address.');
	    		}
	    		if(countryName != null && countryName != NO_CHANGE && countryName != INVALID_COUNTRY)
	    		//if(CountryCode!= null && CountryCode!= NO_CHANGE && CountryCode!= INVALID_COUNTRY)
	    		{
	    			//TAcc.ShippingCountry = CountryCode;
	    			TAcc.ShippingCountry = countryName;
	    		}
	    		if(StateCode != null && StateCode != NO_CHANGE && StateCode != INVALID_STATE)
	    		{
	    			TAcc.ShippingState = StateCode;
	    		}
	    	}
		}
	}

	public static void updateContactValues(Contact TCon, map<String,String> resultmap)
	{
		//String CountryCode;
		String countryName;
    	String StateCode;

		if(resultmap!= null && !resultmap.isEmpty() && !TCon.Zuora_Information_Locked__c)
	    {
	    	
	    	//CountryCode = resultmap.get('CountryCode');
	    	countryName = resultmap.get('CountryName');
	    	StateCode = resultmap.get('StateCode');
	    	System.debug('Country Name is :' + countryName);
	    	System.debug('State Code is :' + StateCode);
	    	if((countryName == INVALID_COUNTRY || StateCode == INVALID_STATE) && !(isAccRec) && (IsExemptUser!= true))
	    	//if((CountryCode == INVALID_COUNTRY || StateCode == INVALID_STATE) && !(isAccRec) && (IsExemptUser!= true))
	    	{
	    		TCon.addError(ERR_MSG + 'Mailing address.');
	    	}
	    	if(countryName != null && countryName != NO_CHANGE && countryName != INVALID_COUNTRY)
	    	//if(CountryCode!= null && CountryCode!= NO_CHANGE && CountryCode!= INVALID_COUNTRY)
	    	{
	    		System.debug(' Before setting Country Name is :' + countryName);
	    		//TCon.MailingCountry = CountryCode;
	    		TCon.MailingCountry = countryName;

	    	}
	    	if(StateCode!= null && StateCode!= NO_CHANGE && StateCode!= INVALID_STATE)
	    	{
	    		System.debug('BEFORE Setting the Billing State to:' + StateCode);
	    		TCon.MailingState = StateCode;
	    		System.debug('AFTER Setting the Billing State to:' +TCon.MailingState);
	    	}
	    }
	}
}