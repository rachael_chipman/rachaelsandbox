@isTest
public class oppApprovalUpdateTest {
	
	static Account a;
	static Opportunity o;
	static Contract c;
	
	static testMethod void init() {
		a = new account(name='test account');
		insert a;
		
		o = new Opportunity(name='test opp',accountid=a.id,closedate=date.today(),stagename='investigating',type='New Business',approval_type__c = 'Legal Approval');
		insert o;
	}

	static testMethod void test1() {
		init();	
		
		c = new contract(accountid = a.id, opportunity__c = o.id, legal_stage__c = 'Submitted by rep');
		insert c;
		
		c.legal_stage__c = 'Approved by Legal';
		update c;
	}

}