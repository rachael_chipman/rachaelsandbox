@isTest
private class LGROwnerPush_test
{
    static User testUser;
    //static Id testUserId;

    static void setup()
    {
        testUser = TestUtils.SALES_USER;
        /*testUserId = [SELECT Id FROM USER WHERE Id = :testUser.Id LIMIT 1].Id;
        System.runAs ( TestUtils.ADMIN_USER )
        {
            insert testUser;
        }*/
    }

    private static testMethod void testPopulateOwnerCopy()
    {
        setup();
        test.startTest();
        system.runAs(testUser) 
        {
            Lead newLead = new Lead();
            newLead.Company = 'Some Company';
            newLead.FirstName = 'Scarlett';
            newLead.LastName = 'O\'Hara';
            insert newLead;
        }
        test.stopTest();

        //Query for the new lead
        List<Lead> leads = [SELECT Id, Owner_Copy__c FROM Lead];
        system.assert(1 == leads.size());
        system.assert(testUser.Id == leads[0].Owner_Copy__c);

    }

    /*static testMethod void runLGROwnerPush_test1()
    {
        //Test for Back to LGR Owner Update - push in the Previous Lead Owner's Name (into Previous LGR User ID field) if there isn't already a Prev LGR listed    
    
        setup();

        Lead l = new Lead(
        Company = 'Hummingbird Cakes',
        LastName = 'O Hara',
        FirstName = 'Scarlett',
        OwnerId = testUser.Id,
        Previous_ISR_User_ID__c = null);
        insert l;
        
        l.Back_to_ISR_Owner_Update__c = true;
        update l;
    }  
      
    static testMethod void runLGROwnerPush_test2()
    {    
        //Test for if there is a Prev LGR listed make them the Lead Owner and then if Qualifying for is blank then push the Original owner (FSR) into that field    
    
        setup();

        Lead l = new Lead(
        Company = 'Hummingbird Cakes',
        LastName = 'O Hara',
        FirstName = 'Scarlett',
        OwnerId = testUser.Id,
        Previous_ISR_User_ID__c = testUserId,
        Prospecting_For__c = null);    
        insert l;
        
        l.Back_to_ISR_Owner_Update__c = true;
        l.Previously_Sent_Back_to_Marketing__c = true;
        update l;
    }
    
    static testMethod void runLGROwnerPush_test3()
    {    
        //Test for Back to FSR Owner Update - If Qualifying for is blank and Status != Nurture then Owner becomes the Prev LGR, otherwise Owner for becomes Qualifying for    
        
        setup();

        Lead l = new Lead(
        Company = 'Hummingbird Cakes',
        LastName = 'O Hara',
        FirstName = 'Scarlett',
        OwnerId = testUser.Id,
        Status = 'Open',
        Previous_ISR_User_ID__c = testUserId,
        Prospecting_For__c = null);    
        insert l;
        
        l.Back_to_FSR_Owner_Update__c = true;
        update l;
    
    }
    
    static testMethod void runLGROwnerPush_test4()
    {    
        //Test for Back to FSR Owner Update - If Qualifying for is blank and Status != Nurture then Owner becomes the Prev LGR, otherwise Owner for becomes Qualifying for    
    
        setup();

        Lead l = new Lead(
        Company = 'Hummingbird Cakes',
        LastName = 'O Hara',
        FirstName = 'Scarlett',
        OwnerId = testUser.Id,
        Status = 'Nurture',
        Previous_ISR_User_ID__c = testUserId,
        Prospecting_For__c = null);    
        insert l;
        
        l.Back_to_FSR_Owner_Update__c = true;
        update l;
    }
    
    static testMethod void runLGROwnerPush_test5()
    {    
        //Test for If Qualifying for != blank then they become the Owner and Qualifying for becomes blank, and the Previous Owner becomes the Prev LGR User Id
    
        setup();

        Lead l = new Lead(
        Company = 'Hummingbird Cakes',
        LastName = 'O Hara',
        FirstName = 'Scarlett',
        OwnerId = testUser.Id,
        Status = 'Lead Gen Qualified',
        Previous_ISR_User_ID__c = testUserId,
        Prospecting_For__c = testUserId);    
        insert l;
        
        l.Back_to_FSR_Owner_Update__c = true;
        update l;
    }*/

}