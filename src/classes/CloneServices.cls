//The top class needs to be global as this needs to be accessible for API call
public class CloneServices { 
   
   //New Parent Record ID for API call
   public string newParentRecordID {get;private set;}
    
   public class CloneServiceException extends Exception {}
   
   public static Map<String, Schema.SObjectType> globalDescribe {
   		get{
   			if ( globalDescribe == null ){
   				globalDescribe = Schema.getglobalDescribe();
   			}
   			
   			return globalDescribe;
   		}
   		
   		private set;
   } 

   //excute clone and returns new parent record id
   public static List<Id> startsClone( Set<Id> parentRecordIds, String parentRecordAPIName, Map<String, String> childObjAPINameToParentField, Map<String, object> excludedParentsFieldToNewValue, Map<String, object> excludedChildFieldToNewValue ){

        Map<String, list<String>> childObjFields = New Map<String, list<String>>{};
        if( !childObjAPINameToParentField.isEmpty() ) {
            //getCreatableFields(objAPIName) returns all creatable fields name for a given child obj API name        
            for( String objAPIName : childObjAPINameToParentField.keySet() )
                childObjFields.put( objAPIName, getCreatableFields( objAPIName ) );
            
        }
		
        //Clone parent record*/
        Map<Id, Id> oldIdToNewId = cloneParentRecord( parentRecordAPIName, getCreatableFields( parentRecordAPIName ), parentRecordIds, excludedParentsFieldToNewValue );

        //Clone all selected child records
        	for( String childObjAPIName : childObjFields.keySet() )
            	cloneChildRecords( childObjAPIName, childObjAPINameToParentField.get ( childObjAPIName ), childObjFields.get(childObjAPIName), oldIdToNewId, excludedChildFieldToNewValue );

        return oldIdToNewId.values();    
    }


    //Clone parent record and return new parent record ID
    public static Map<Id, Id> cloneParentRecord(String objAPIName, List<String> createableFields, Set<Id> parentRecordIds, Map<String, object> excludedParentsFieldToNewValue ) {
       
       Map<Id, SObject> oldIdToClonedObject = new Map<Id, SObject>();
       Map<Id, Id> oldIdToNewId = new Map<Id, Id>(); 
        //Creates a string listing all of the parent object fields to exclude from the clone
        String fields = '';
        Set<String> excludedParentFields = excludedParentsFieldToNewValue.keySet();
        for( string s : createableFields ) {
            
            if ( !excludedParentFields.contains ( s ) ){
            	fields = fields + s + ',';
            }          
        }
        
        fields = fields.substring( 0, fields.lastIndexOf( ',' ) );
		
        String queryString = 'SELECT ' + fields + ' FROM ' + objAPIName + ' WHERE Id IN : parentRecordIds';
        
        for ( SObject result : Database.query(queryString) ){
        	SObject clonedObject = result.clone ( false, true );
    		
    		//Populate excluded parent fields with new specified value
        	for( String fieldName : excludedParentFields )
        	{
        		clonedObject.put( fieldName, excludedParentsFieldToNewValue.get( fieldName ) );
        	}
        	oldIdToClonedObject.put ( result.Id, clonedObject );
        }       
        
        try{   
	        insert oldIdToClonedObject.values();
 		}catch ( DMLException dmlEx ){
 			throw new CloneServiceException ( dmlEx.getMessage() );
 		}
       
       	for ( Id objId : oldIdToClonedObject.keySet() )
       		oldIdToNewId.put ( objId, oldIdToClonedObject.get ( objId ).Id );
       		
        return oldIdToNewId ;
    }
       
    
    //Clone all child records
    public static void cloneChildRecords( String childObjAPIName, String parentField, list<String> createableFields,  Map<Id, Id> oldIdToNewId, Map<String, object> excludedChildFieldToNewValue  ){
        String fields = '';
        String parentWhereClause = '(';
        
        //Creates a string listing all of the child object fields to exclude from the clone
        Set<String> excludedChildFields = excludedChildFieldToNewValue.keySet();
        for( string s : createableFields ) {
            
            if ( !excludedChildFields.contains ( s ) ){
            	fields = fields + s + ',';
            }          
        }

        fields = fields.substring(0, fields.lastIndexOf(','));
		
		for ( String key : oldIdToNewId.keySet() ){
			parentWhereClause += '\'' + key + '\',';
		}
		
		parentWhereClause = parentWhereClause.substring(0, parentWhereClause.lastIndexOf(','));
		parentWhereClause += ')';
			
        String queryString = 'SELECT ' + fields + ' FROM ' + childObjAPIName + ' WHERE ' + parentField + ' IN ' + parentWhereClause;
    	
    	list<SObject> childrenClones = new list<SObject>();
        for ( SObject result : Database.query( queryString ) ){
        	SObject clonedResult = result.clone( false, true );
        	clonedResult.put ( parentField, oldIdToNewId.get ( (Id)result.get( parentField ) ) );

    		//Populate excluded child fields with new specified value
        	for( String fieldName : excludedChildFields )
        	{
        		clonedResult.put( fieldName, excludedChildFieldToNewValue.get( fieldName ) );
        	}
			
			childrenClones.add ( clonedResult );
        }         
    
 		try{   
        	insert childrenClones;
 		}catch ( DMLException dmlEx ){
 			throw new CloneServiceException ( dmlEx.getMessage() );
 		}
    }
    
    //Get all creatable fields for a given object
    public static list<String> getCreatableFields( String objAPIName ){
        Map<string,string> childFieldsName = new Map<string,string>{};
        
        SObjectType sot = globalDescribe.get(objAPIName);
      
        //Get all non-creatable fields name except
        //Get the field tokens map
        Map<String, SObjectField> fields = new Map<String, SObjectField>{};
        if(sot.getDescribe().fields.getMap().keyset().size()>0)
            fields = sot.getDescribe().fields.getMap();
        
        //And drop those tokens in a List
        List<SObjectField> fieldtokens = fields.values();
        
        List<string> objectFields = new List<String>();
        
        for(SObjectField fieldtoken:fieldtokens) {
            DescribeFieldResult dfr = fieldtoken.getDescribe();
            
            if(dfr.isCreateable())
                objectFields.add(dfr.getLocalName());
        }
        
        return objectFields;
    }
    
}