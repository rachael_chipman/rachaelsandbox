@isTEST(SeeAllData=true)

public with sharing class ClonedPendingCertOpp_test{

	static Opportunity testOpp;
    static OpportunityLineItem CertOli;
    static OpportunityLineItem EISOli;
    
    static void SetupOppandOLIs()
    {
    	Account a = new Account (Name = 'Test Account');
	    insert a;
	    
	    testOpp = new Opportunity (
	    AccountId = a.Id, 
	    Name = 'TestOpp', 
	    CloseDate = system.today()+30, 
	    Type = 'New Business',
	    StageName = 'Establishing Value',
	    Pricebook2Id = '01s000000004NS6AAM',
	    CurrencyISOCode = 'USD',
	    //AM__c = '00500000007Em7P',
	    Cloned__c = false, 
	    Approval_Type__c = 'Legal Approval');

	    
	    CertOli = new OpportunityLineItem(
	    OpportunityId = testOpp.Id,
	    PricebookEntryId = '01u00000000FGwSAAW',
	    Product_Status__c = 'Current',
	    Description__c = 'RP Certification',
	    Start_Date__c = system.today() +30,
	    End_Date__c = system.today() +394,
	    UnitPrice = 1500.00,
	    Previous_Value__c = 0.00,
	    Product_Detail_Created__c = false,
	    Quantity = 1);

	    
	    EISOli = new OpportunityLineItem(
	    OpportunityId = testOpp.Id,
	    PricebookEntryId = '01u00000000FGw7AAG',
	    Product_Status__c = 'Current',
	    Start_Date__c = system.today() +30,
	    End_Date__c = system.today() +394,
	    UnitPrice = 1500.00,
	    Previous_Value__c = 0.00,
	    Product_Detail_Created__c = false, 
	    Quantity = 1);

    }
    
    static testMethod void CloneforPendingCert_Test1()
    {
    	SetupOppandOLIs();
    	insert testOpp;
    	testOpp.StageName = 'Signature';
    	update testOpp;
    	CertOli.OpportunityId = testOpp.Id;
    	EISOli.OpportunityId = testOpp.Id;
    	insert new List<OpportunityLineItem>{CertOli, EISOli};
    	
    	
    	//Query for testOpp
    	List<Opportunity> testOpps = [SELECT Id, Has_Cert_Products_Qty__c, Cert_Only_Opportunity__c,Type, Clone_for_Pending_Cert__c, Account.Certification_Client__c FROM Opportunity WHERE Id = : testOpp.Id ];
    	system.assertEquals(1, testOpps[0].Has_Cert_Products_Qty__c, 'There should be one Cert Product on the opp');
    	system.assertEquals('No', testOpps[0].Cert_Only_Opportunity__c, 'There should be multiple products on the opp');
    	system.assertEquals('New Business', testOpps[0].Type, 'The opp type should be New Business');
    	system.assertEquals(0, testOpps[0].Account.Certification_Client__c, 'The account should not be a Cert client');
    	system.assertEquals('Yes', testOpps[0].Clone_for_Pending_Cert__c, 'The opp should be marked for Pending Cert');
    	
    	//Query for all OLIs
    	List<OpportunityLineItem> testOppOLIs = [SELECT Id, OpportunityId, Product_Code__c FROM OpportunityLineItem WHERE OpportunityId = :testOpp.Id];
    	system.assertEquals(2, testOppOLIs.size(), 'There should be two Products on the opp');
    	
    	//Query for Cert OLI
    	List<OpportunityLineItem> CertOLIs = [SELECT Id, OpportunityId, Product_Code__c FROM OpportunityLineItem WHERE OpportunityId = :testOpp.Id and Product_Code__c = '004-CERT-FEE0'];
    	system.assertEquals(1, CertOLIs.size(), 'There should be one Cert Product on the opp with the correct Product code');	
    	
    	test.startTest();
    	testOpp.StageName = 'Finance Approved';
    	update testOpp;
    	test.stopTest();
    	
    	List<Opportunity> testOpps2 = [SELECT Id, Cloned__c, Has_Cert_Products_Qty__c, Cert_Only_Opportunity__c,Type, Account.Certification_Client__c FROM Opportunity WHERE Id = : testOpp.Id ];
    	system.assertEquals(true, testOpps2[0].Cloned__c, 'The opportunity should be marked that it is cloned');
    	
    	//Query for Line Items on Test Opp
    	List<OpportunityLineItem> OLIsOnTestOpp = [SELECT Id, OpportunityId, Product_Code__c, Previous_Value__c, Product_Detail_Created__c,PricebookEntryId, Quantity, UnitPrice, Product_Status__c, Description__c, Indirect__c, Active__c, Bundle__c, Recurring__c
    	FROM OpportunityLineItem WHERE OpportunityId = :testOpp.Id];
    	system.assertEquals(1, OLIsOnTestOpp.size(), 'There should only be one line item remaining on the testOpp');
    	
    	//Query for Pending Cert Opp
    	List<Opportunity> PendingCertOpps= [SELECT Id, StageName, Cloned_From_ID__c, AccountId FROM Opportunity WHERE StageName = 'Pending Certification Qualification' and Cloned_From_ID__c = :testOpp.Id];
    	system.AssertEquals(1, PendingCertOpps.size(), 'There should be one Pending Cert Opp with the correct Cloned From ID');
    }
    
}