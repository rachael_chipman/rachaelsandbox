trigger Opp_App_Fee_Create on Certification_Application__c (Before Insert, Before Update) {

Certification_Application__c ca = Trigger.New[0];

    if(ca.Promo_Type_Picklist__c == 'Walk-Up' && ca.Application_Fee_Opportunity__c == null){
    
        Opportunity op = new Opportunity (
            Name = 'Naming Convention WFR',
            AccountId = ca.Account__c,
            RecordTypeId = '012000000004wVU',
            StageName = 'Finance Approved',
            CloseDate = system.Today(),
            Type = 'Fee',
            OwnerId = '00500000007286zAAA',
            //CampaignId = '701000000004fLn',
            Product__c = 'Certification',
            SSC_Volume__c = ca.Certification_Volume__c,
            Pricebook2Id = '01s000000004NS6AAM',
            Product_Certification_App_Fee__c = true,
            Certification_Type__c = 'Application Fee',
            Billing_Type__c = 'Annually',
            Run_OCV__c = true);
            
            insert op;
            
           
            
        OpportunityLineItem opl = new OpportunityLineItem(
            OpportunityId = op.Id,
            Quantity = 1,
            UnitPrice = ca.Certification_App_Fee__c,
            Product_Status__c = 'Current',
            Description__c = 'Return Path Certification',
            Active__c = true,
            Recurring__c = false,
            PricebookentryId = '01u00000000FeX2AAK');
            
            insert opl;
                
           ca.Application_Fee_Opportunity__c = op.id;
            
            
            
            
             
            }
            
            
            
           


}