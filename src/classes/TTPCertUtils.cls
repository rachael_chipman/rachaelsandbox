public with sharing class TTPCertUtils {

	public Integer getDiffBusinessDays(Date startdt, Date enddt) 
	{
	 	Date tempdate = null;
	 	if (startdt > enddt) 
	 	{
	 	 	tempdate = enddt;
	 	 	enddt = startdt;
	 	 	startdt = tempdate;
	 	}
	 	
		Integer i = Math.mod((date.newinstance(1985, 6, 24)).daysBetween(startdt),7); // 24/6/85 was a monday
		Map<Integer, Map<Integer, Integer>> m = new Map<Integer, Map<Integer, Integer>> 
		{
			0 => new Map<Integer, Integer> { 1 => 2 , 2 => 3 , 3 => 4 , 4 => 5 , 5 => 5 , 6 => 5 },
			1 => new Map<Integer, Integer> { 1 => 2 , 2 => 3 , 3 => 4 , 4 => 4 , 5 => 4 , 6 => 5 },
			2 => new Map<Integer, Integer> { 1 => 2 , 2 => 3 , 3 => 3 , 4 => 3 , 5 => 4 , 6 => 5 },
			3 => new Map<Integer, Integer> { 1 => 2 , 2 => 2 , 3 => 2 , 4 => 3 , 5 => 4 , 6 => 5 },
			4 => new Map<Integer, Integer> { 1 => 1 , 2 => 1 , 3 => 2 , 4 => 3 , 5 => 4 , 6 => 5 },
			5 => new Map<Integer, Integer> { 1 => 0 , 2 => 1 , 3 => 2 , 4 => 3 , 5 => 4 , 6 => 5 },
			6 => new Map<Integer, Integer> { 1 => 1 , 2 => 2 , 3 => 3 , 4 => 4 , 5 => 5 , 6 => 5 }
		};
		
		Integer i2 = Math.mod((startdt.daysBetween(enddt)),7); // gives us the remainder of days between start date and end date divided by 7
		Integer i3 = (m.get(i)).get(i2);
		if (i2 == null || i2 < 1 || i2 > 6) 
		{
			if (i >= 0 && i <= 4) 
			{
				 i3 = 1;
		 	} 
			else  
			{
				 i3 = 0; 
			}
		}

		i3 = i3 + 5 * (Math.floor( ((Decimal) startdt.daysBetween(enddt)).divide(7,4))).intValue() -1;
		
		if (tempdate != null) i3 *= -1; // negative number of days
		return i3;
    }
}