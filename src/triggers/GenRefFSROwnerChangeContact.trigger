trigger GenRefFSROwnerChangeContact on Contact (before update){

/*DESCRIPTION: This trigger will reassign the Referring CRM as the Owner and then submit it for their approval, then once the CRM submits
the Contact for RD/FSR approval, the FSR who approves the Contact will become the Contact owner*/
    
    for(Contact c : trigger.new){ 

    //Set string variable for Most Recent Campaign
    string str = c.Campaign_Name__c;
    
    //Only pull Contacts that have a Gen Ref Most Recent Campaign and a Referring CRM
    if( SObjectTriggerServices.isValidCampaign( str ) && c.Referring_CRM__c != null){

   
     //If the CRM has approved the Lead and so has the FSR, then make the FSR the owner    
     if(c.Gen_Ref_Contact__c == true && c.FSR_Accepted_Gen_Ref__c == 'Accepted'){
    
        Contact prev = trigger.oldMap.get(c.Id);
    
        if(c.FSR_Accepted_Gen_Ref__c != prev.FSR_Accepted_Gen_Ref__c){
        
        c.OwnerId = UserInfo.getUserID();
       
           } 

        }
    }
    }
    }