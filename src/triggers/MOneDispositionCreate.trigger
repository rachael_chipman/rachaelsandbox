trigger MOneDispositionCreate on Lead (before update) {

list<MOne_Disposition__c> toInsert = new list<MOne_Disposition__c> ();

    for(Lead x : trigger.new){
    
    if(x.Disposition_Admin_Only__c == true){

     Lead prev = trigger.oldMap.get(x.Id);   
    
    x.Disposition_Admin_Only__c = false; 
     
          MOne_Disposition__c MOne = new MOne_Disposition__c();
              Mone.Date_of_Lead_Disposition__c = system.today();
              Mone.Lead__c = x.Id;
              if(x.Recycle_Counter__c > 0) Mone.Type_of_Disposition__c = 'Recycled';
              else Mone.Type_of_Disposition__c = 'Initial';
              if(x.Prospecting_for__c != null && x.Status == 'Lead Gen Qualified') Mone.New_Lead_Owner__c = x.Prospecting_for__c;
              else Mone.New_Lead_Owner__c = x.OwnerId;
              Mone.MOne_Rep__c = prev.OwnerId;
              Mone.Lead_Status_Time_of_Disposition__c = x.Status;
              toInsert.add(Mone);
       }
                insert toInsert;
}

}