//use this to schedule 
//PostInvoicesClass s = new PostInvoicesClass();
//string sch = '0 10 * * * *';
//system.schedule('Post Invoices',sch,s);

global class PostInvoicesClass implements Schedulable {
global void execute(SchedulableContext ctx) {

CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, NextFireTime
                FROM CronTrigger WHERE Id = :ctx.getTriggerId()];

      System.debug(ct.CronExpression);
      System.debug(ct.TimesTriggered);
      
     List<c2g__codaInvoice__c> invs = [select Id from c2g__codaInvoice__c where Ready_for_Posting__c = true and c2g__InvoiceStatus__c != 'Complete' and Ready_For_Posting__c = true order by CreatedDate limit 5];
     
   List<c2g.codaapicommon.reference> invoiceRefsbatch1 = new List<c2g.codaapicommon.reference>();

for(c2g__codaInvoice__c sis : invs){

invoiceRefsbatch1.add(c2g.CODAAPICommon.getRef(sis.Id, null));

}
c2g.CODAAPISalesInvoice_3_0.BulkPostInvoice(null, invoiceRefsbatch1); 
      
      
      
      }}