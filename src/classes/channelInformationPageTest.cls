@isTest
public class channelInformationPageTest {
    
    static User testUser;
    static Account a;
    static Account partner;
    static Contact partnerContact;
    static Opportunity o;
    static Promo_Code__c pc;
    static PageReference p;
    static ApexPages.standardController std;
    static channelInformationPageController extension;
    
    private static void init() {
        
        testUser = new User(alias = 'u1', email='u1@testorg.com',
            emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
            localesidkey='en_US', profileid = [select id from profile where id = '00e000000073QIv' limit 1].id, 
            country='United States', timezonesidkey='America/Los_Angeles', username='u1256457@testorg.com');
        
        insert testUser;
        
        a = new Account (name = 'test account');
        insert a;
        
        partner = new Account (name = 'test partner',crm__c = testUser.id);
        insert partner; 
        
        partnerContact = TestUtils.createContacts('Partner', 'Contact', partner.Id, true);
                
        pc = new Promo_Code__c (account__c = partner.Id, name = 'TEST0001', Promo_Type__c = 'Referral');
        insert pc;
        
        partner.Promo_Code__c = pc.Id;
        update partner;
        
        o = new Opportunity(name = 'test opp', closedate = date.today(), accountid = a.id, stagename = 'Pre-Pipeline', partnerlookup__c = partner.id);
        insert o;
        
        p = Page.ChannelInformationPage;
        p.getParameters().put('id',o.id);
        test.setCurrentPage(p);
        
        std = new ApexPages.StandardController(o);
        extension = new channelInformationPageController(std);      

    }
    
    public static testMethod void test1() {
        init();
        
        test.startTest();
        extension.makeChange();
        system.assertEquals(true,extension.editMode);
        
        extension.o.Nature_of_Involvement__c = 'We reached out to partner';
        extension.o.partnerlookup__c = partner.id;
        extension.o.Deal_Closing_Through__c = 'Partner';

        extension.makeChange();
        
        extension.getPartnerContacts();
        extension.getPartnerData();
        
        system.assertEquals(false,extension.editMode);
        test.stopTest();
        
        extension.newContact();
        //extension.apply();
        extension.WhoisInvoiced();
        extension.CheckPartner();
        
    }

    public static testMethod void test2() {
        init();
        
        test.startTest();
        extension.makeChange();
        system.assertEquals(true,extension.editMode);
        extension.o.deal_closing_through__c = 'Return Path';
        update extension.o;
        extension.makeChange();
        system.assertEquals(false,extension.editMode);
        extension.WhoisInvoiced(); 
        extension.CheckPartner();       
        test.stopTest();
    }
    
    public static testMethod void test3() {
        init();
        
        test.startTest();
        extension.makeChange();
        extension.CheckPartner();
        system.assertEquals(true,extension.editMode);
        
        extension.o.Nature_of_Involvement__c = 'Yes';
        extension.o.partnerlookup__c = null;

        extension.CheckPartner();
        extension.makeChange();
        
        extension.getPartnerContacts();
        extension.getPartnerData();
        
        system.assertEquals(false,extension.editMode);
        test.stopTest();
        
        extension.newContact();
        //extension.apply();
        extension.WhoisInvoiced();
        extension.CheckPartner();
    }
}