public class PortalWizardProducts_Controller2 {

    public Account theAcct {get;set;}
    public Opportunity theOpp {get;set;}
    public opportunityLineItem[] shoppingCart {get;set;}
    public opportunityLineItem Discount;
    public priceBookEntry[] AvailableProducts {get;set;}
    public priceBookEntry[] AddOns {get;set;}
    public Pricebook2 theBook {get;set;}      
    public String toSelect {get; set;}
    public String toUnselect {get; set;}
    private opportunityLineItem[] forDeletion = new opportunityLineItem[]{};
    private Boolean forcePricebookSelection = false;
    public Opportunitylineitem OppTotal;
    public Opportunitylineitem SubTotal;
    public Decimal newdis;
    public Pricebookentry dis {get; set;}
    public OpportunityLineItem discountoli;
    public Boolean displayAddOns {get;set;}
 

    public PortalWizardProducts_Controller2(ApexPages.StandardController controller) {
    
            theAcct = [select Id, CRM__c from Account where Id = :ApexPages.currentPage().getParameters().get('acct')];
   
            theOpp = [select Id, OwnerId, CRM_User__c, Account.CRM__c, Name, Type, Estimated_Start_Date__c, Pricebook2Id, Partner_Discount__c, ACV_Before_Partner_Comm__c, Pricebook2.Name, CurrencyIsoCode from Opportunity where Id = :ApexPages.currentPage().getParameters().get('opp')];
       
          Pricebook2[] activepbs = [select Id, Name from Pricebook2 where Name = 'SmartFocus Pricebook'];
         
             theBook = activepbs[0];
       
        shoppingCart = [select Id, Quantity, Discount__c, TotalPrice, UnitPrice, Description, PriceBookEntry.Product2.Description, PriceBookEntryId, PriceBookEntry.Name, PriceBookEntry.IsActive, Include_in_Package__c,
                        PriceBookEntry.Product2Id, PriceBookEntry.Product2.Name, PricebookEntry.UnitPrice, PriceBookEntry.PriceBook2Id, Start_Date__c, End_Date__c from opportunityLineItem where OpportunityId=:theOpp.Id and PriceBookEntry.Product2.Name != 'Discount'];
  
        
        dis = [select Id, Pricebook2Id from PricebookEntry where IsActive=true and 
                         Pricebook2Id = :theBook.Id and CurrencyIsoCode = :theOpp.CurrencyIsoCode and PriceBookEntry.Product2.Name = 'Discount' ];
                         


        if(theOpp.Type != 'Supplement')
        {
        	displayAddOns = false;
        }	
        else
        {
        	displayAddOns = true;
        }       
              if(!forcePricebookSelection)
            updateAvailableList();
            updateAddOnsList();
   
          
    }
    
  public Boolean getDisplayAddOns()
  {
  	displayAddOns = true;
  	return displayAddOns;
  }
    
    
  public Opportunitylineitem getOppTotal(){
   oppTotal = new Opportunitylineitem (UnitPrice = 0);
        
        for(Opportunitylineitem o : shoppingCart){
            if(theOpp.Partner_Discount__c > 0 && theOpp.Partner_Discount__c <= 10)
            {
            
            	oppTotal.UnitPrice += (o.UnitPrice * (1-(theOpp.Partner_Discount__c /100))).SetScale(2); 
            }
            
            else if(theOpp.Partner_Discount__c == 0 || theOpp.Partner_Discount__c == null)
            {
             	oppTotal.UnitPrice += o.UnitPrice.SetScale(2);
            }
            
            else if(theOpp.Partner_Discount__c > 10)
            {
            	oppTotal.UnitPrice += o.UnitPrice.SetScale(2);
            	ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, 'Discounts may not exceed 10%. Please contact your Channel Relationship Manager for assistance.' );
            	ApexPages.addMessage(myMsg);
				
            }  
        }
        
        return oppTotal; 
        
        }
        
        public Opportunitylineitem getSubTotal(){
        
         SubTotal = new Opportunitylineitem (UnitPrice = 0);
        
        for(Opportunitylineitem o : shoppingCart){
            
             SubTotal.UnitPrice += o.UnitPrice.SetScale(2);   
        }
        
        
        return SubTotal; 
    }

   
  public decimal updateDiscount(){
  
  
update theOpp;
  
return null;
  
  }

  public void updateAvailableList() {
    
    String qString = 'select Id, Pricebook2Id, IsActive, Product2.Name, Product2.Family, Product2.IsActive, Product2.Product_Type__c, Product2.Description, UnitPrice from PricebookEntry where IsActive=true and Product2.Product_Type__c = \'' + 'Core' + '\' and Pricebook2Id = \'' + theBook.Id + '\' and  CurrencyIsoCode = \'' + theOpp.CurrencyIsoCode + '\'';
    
     
    
 Set<Id> selectedEntries = new Set<Id>();
        for(opportunityLineItem d:shoppingCart){
            selectedEntries.add(d.PricebookEntryId);
        }      
      
        qString+= ' order by Product2.Name';
        qString+= ' limit 101';
        
        system.debug('qString:' +qString);        
        AvailableProducts = database.query(qString);

       
    }
    
    
     public void updateAddOnsList() {
    
    String qString = 'select Id, Pricebook2Id, IsActive, Product2.Name, Product2.Family, Product2.IsActive, Product2.Product_Type__c, Product2.Description, UnitPrice from PricebookEntry where IsActive=true and Product2.Product_Type__c = \'' + 'Add-On' + '\' and Pricebook2Id = \'' + theBook.Id + '\' and  CurrencyIsoCode = \'' + theOpp.CurrencyIsoCode + '\'';
    
    
     qString+= ' and (Product2.Name != \'' + 'Discount' + '\')';  

 Set<Id> selectedEntries = new Set<Id>();
        for(opportunityLineItem d:shoppingCart){
            selectedEntries.add(d.PricebookEntryId);
        }   
           
         
        qString+= ' order by Product2.Name';
        qString+= ' limit 101';
        
        system.debug('qString:' +qString);        
        AddOns = database.query(qString);

    } 
    
       public void addToShoppingCart()
       {
       		displayAddOns = true;
       			    
		        // This function runs when a user hits "select" button next to a product
		 for(PricebookEntry d :AvailableProducts)
		 {
             if((String)d.Id==toSelect)
             {
                 
                       if(ApexPages.currentPage().getParameters().get('sup') == 'false')
                       {                      
	                      shoppingCart.add(new opportunityLineItem(OpportunityId=theOpp.Id, PriceBookEntry=d, PriceBookEntryId=d.Id, UnitPrice=d.UnitPrice,
	                      Quantity=1, Start_Date__c=theOpp.Estimated_Start_Date__c, End_Date__c=theOpp.Estimated_Start_Date__c.AddMonths(12).AddDays(-1)));
	                    
	              
	                      
                      }
                      
                       if(ApexPages.currentPage().getParameters().get('sup') == 'true')
                       {                      
	                      shoppingCart.add(new opportunityLineItem(OpportunityId=theOpp.Id, PriceBookEntry=d, PriceBookEntryId=d.Id, UnitPrice=d.UnitPrice,
	                      Quantity=1, Start_Date__c=system.Today(), End_Date__c=system.today().AddMonths(12).AddDays(-1)));
	                      
	                  
                      }
                      
                           break;
             
                           
            }
		    }
		    for(PricebookEntry s :AddOns)
		    {
		             if((String)s.Id==toSelect)
		             {
		                 
		                      if(ApexPages.currentPage().getParameters().get('sup') == 'false')
		                      {                      
			                      shoppingCart.add(new opportunityLineItem(OpportunityId=theOpp.Id, PriceBookEntry=s, PriceBookEntryId=s.Id, UnitPrice=s.UnitPrice,
			                      Quantity=1, Start_Date__c=theOpp.Estimated_Start_Date__c, End_Date__c=theOpp.Estimated_Start_Date__c.AddMonths(12).AddDays(-1)));
			                      
			                      
		                      }
		                      
		                       if(ApexPages.currentPage().getParameters().get('sup') == 'true')
		                       {                      
			                      shoppingCart.add(new opportunityLineItem(OpportunityId=theOpp.Id, PriceBookEntry=s, PriceBookEntryId=s.Id, UnitPrice=s.UnitPrice,
			                      Quantity=1, Start_Date__c=system.Today(), End_Date__c=system.today().AddMonths(12).AddDays(-1)));
			                      
			                     
		                      }
		                           
		                           break;
		              
		                           
		            }            
		        }
		        
		              
         
    }
    
        public PageReference removeFromShoppingCart(){
    
        // This function runs when a user hits "remove" on an item in the "Selected Products" section
    
        Integer count = 0;
    
        for(opportunityLineItem d : shoppingCart){
            if((String)d.PriceBookEntryId==toUnselect){
            
                if(d.Id!=null)
                    forDeletion.add(d);
            
                shoppingCart.remove(count);
                break;
            }
            count++;
        }
        
        updateAvailableList();
        
        return null;
    }
    
    
 public PageReference onCancel(){
        
        if(ApexPages.currentPage().getParameters().get('sup')=='false'){      
        
        delete theOpp;
        delete theAcct;
        return new PageReference('/home/home.jsp');
        
        }
        else{
        delete theOpp;
        return new PageReference('/home/home.jsp'); 
        }
        
        }
        



    
    
    public PageReference onSave(){
    
    
   
    
     if(forDeletion.size()>0)
            Delete (forDeletion);
    
        // Previously selected products may have new quantities and amounts, and we may have new products listed, so we use upsert here
        try{
            if(shoppingCart.size()>0)
            
            
            
                upsert(shoppingCart);
        }
        catch(Exception e){
            ApexPages.addMessages(e);
            return null;
        }
        
         
       /* if(theOpp.Partner_Discount__c > 0){
        discountoli = new opportunityLineItem(
        OpportunityId=theOpp.Id, 
        PriceBookEntryId=dis.Id, 
        UnitPrice= ((SubTotal.UnitPrice*(theOpp.Partner_Discount__c/100))*(-1)),
        Quantity=1, 
        Start_Date__c=theOpp.Estimated_Start_Date__c);
        
        insert discountoli;}*/
        
         theOpp.Submit_From_Trigger__c = 'Yes';
         theOpp.CRM_User__c = theOpp.OwnerId;
         update theOpp;
         
        return new PageReference('/apex/PortalWizardUsers?acct='+ApexPages.currentPage().getParameters().get('acct')+'&opp='+ApexPages.currentPage().getParameters().get('opp')+'&sup='+ApexPages.currentPage().getParameters().get('sup')); 
        
        }
        
        public pageReference updateUnitPrice()
        {
        	for(OpportunityLineItem oli :shoppingCart)
        	{
        		if(oli.Include_in_Package__c == true)
        		{
        			oli.UnitPrice = 0.00;       			
        		}
        		else if(oli.Include_In_Package__c == false)
        		{
        			oli.UnitPrice = oli.PricebookEntry.UnitPrice;
        		}
        	}
        	update theOpp;
        	return null;
        }
        
}