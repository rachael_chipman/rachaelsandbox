public class dashboardViewLinksController {
    public string link {get;set;}
    
    public void assign() {
        link = this.link;
    }
    
    public list<selectOption> getOptions() {
        list<selectOption> options = new list<selectOption>();
        options.add(new SelectOption('','Select...'));
        options.add(new SelectOption('https://ssl.salesforce.com/apex/oppsHome?save_new=1&sfdc.override=1&j_id0%3Aj_id5%3Afcf=00B00000009AsGC','MyOpenOpps-A: Closing this Month'));
        options.add(new SelectOption('https://ssl.salesforce.com/apex/oppsHome?save_new=1&sfdc.override=1&j_id0%3Aj_id5%3Afcf=00B00000009AsGD','MyOpenOpps-B: All'));
        options.add(new SelectOption('https://ssl.salesforce.com/apex/oppsHome?save_new=1&sfdc.override=1&j_id0%3Aj_id5%3Afcf=00B00000009AsGE','MyOpenOpps-C: Investigating'));
        options.add(new SelectOption('https://ssl.salesforce.com/apex/oppsHome?save_new=1&sfdc.override=1&j_id0%3Aj_id5%3Afcf=00B00000009AsGF','MyOpenOpps-D: Demonstrating Capabilities'));
        options.add(new SelectOption('https://ssl.salesforce.com/apex/oppsHome?save_new=1&sfdc.override=1&j_id0%3Aj_id5%3Afcf=00B00000009AsGG','MyOpenOpps-E: Obtaining Commitment'));
        options.add(new SelectOption('https://ssl.salesforce.com/apex/oppsHome?save_new=1&sfdc.override=1&j_id0%3Aj_id5%3Afcf=00B00000009AsGH','MyOpenOpps-F: Renewal (Next 90 Days)'));
        options.add(new SelectOption('https://ssl.salesforce.com/apex/oppsHome?save_new=1&sfdc.override=1&j_id0%3Aj_id5%3Afcf=00B00000009B0HX','MyOpenOpps-G: Renewal (All)'));        
        options.add(new SelectOption('https://ssl.salesforce.com/apex/oppsHome?save_new=1&sfdc.override=1&j_id0%3Aj_id5%3Afcf=00B00000009AsGI','MyOpenOpps-H: Past Due'));
        
        return options;
    }
    
}