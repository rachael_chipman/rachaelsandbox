@isTest
public Class GenRefFSROwnerChangeContact_test{
    
   /* private static testmethod void testGenRefFSROwnerChangeContact(){

//Insert User
    User u = new user();
    u.Alias = 'tstuser';
    u.Email='testrc@rp.com';
    u.EmailEncodingKey='UTF-8'; 
    u.LastName='Testing';
    u.LanguageLocaleKey='en_US';
    u.LocaleSidKey='en_US';
    u.ProfileId = '00e0000000734LT'; 
    u.TimeZoneSidKey='America/Los_Angeles'; 
    u.UserName='testrc@rp.com.rp';
        
    insert u;    
    
//Insert a second User
    User u2 = new user();
    u2.Alias = 'tstuser2';
    u2.Email='testrc2@rp.com';
    u2.EmailEncodingKey='UTF-8'; 
    u2.LastName='Testing';
    u2.LanguageLocaleKey='en_US';
    u2.LocaleSidKey='en_US';
    u2.ProfileId = '00e0000000734LT'; 
    u2.TimeZoneSidKey='America/Los_Angeles'; 
    u2.UserName='testrc2@rp.com.rp';
        
    insert u2;
    
//Insert Account
    Account a = new Account();
    a.Name = 'Test Account';
    a.OwnerId = u.Id;
    
    insert a;
    
//Insert Contact
    Contact c = new Contact();
    c.LastName = 'Kat';
    c.FirstName = 'Kit';
    c.AccountId = a.Id;
    c.Campaign_Name__c = '2012_GL_PRO_WF_.NET Deliverability Channel Referral Form NEW';
    c.OwnerId = u.Id;
    c.Referring_CRM__c = u.Id;
    c.Gen_Ref_Contact__c = true;
    
    insert c;
    
        //Update Contact as  User2
        System.runAs(u2){
        
        c.FSR_Accepted_Gen_Ref__c = 'Accepted';
        
        update c;
        }
    }
    
    private static testMethod void testChangingOwnerOnContact(){
        
        List<User> testUsers = new List<User>();
        testUsers.add ( TestUtils.getTestUser( 'TestUser123', 'Exec Admin') );
        testUsers.add ( TestUtils.getTestUser( 'TestUser456', 'Exec Admin') );
         
        System.runAs( TestUtils.ADMIN_USER ){
            insert testUsers;
        } 
        
        Account testAccount = TestUtils.createAccount( null, false );
        testAccount.OwnerId = testUsers[0].Id;
        
        Contact testContact = TestUtils.createContacts( 'Test', 'Contact', testAccount.Id, false );
        testContact.Referring_CRM__c = testUsers[0].Id;
        testContact.OwnerId = testUsers[0].Id;
        testContact.Gen_Ref_Contact__c = true;
        testContact.Campaign_Name__c = '2012_GL_PRO_WF_.NET Deliverability Channel Referral Form NEW';
        insert testContact;
        
        Test.startTest();
            System.runAs ( testUsers[1] ){
                testContact.FSR_Accepted_Gen_Ref__c = 'Accepted';
                update testContact;
            }
        Test.stopTest();
        
        List<Contact> updatedContact = [SELECT Id, OwnerId FROM Contact];
        
        System.assertEquals ( 1, updatedContact.size(), 'There should only be one contact that was created in the test');
        System.assertEquals ( testUsers[1].Id, updatedContact[0].OwnerId, 'The owner should be set to the user updating the contact' );
        
    }*/
}