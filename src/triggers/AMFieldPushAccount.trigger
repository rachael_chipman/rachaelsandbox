trigger AMFieldPushAccount on Account(before insert, before update) {

    string amId = 'Test';
    string crmId = 'Test';
    string amName = 'Blank';
    string crmName = 'Blank';
    List<User> theActiveUsers = [Select Id, FirstName, LastName from User where isactive = true];
    
    for(Account acct1 : trigger.new) {

          if(acct1.accountmanager__c <> null && 
              acct1.accountmanager__c <> 'Unassigned'){
        
            for(User am1 : theActiveUsers){
                amName = am1.firstname + ' ' + am1.lastname;
                if(amName == acct1.accountmanager__c){
                    amId = am1.id;
                    break;
                }
            }
            if( amId <> 'Test') acct1.AM__c = amId;
          }  
          if(acct1.Customer_Relationship_Manager_Primary__c <> null && 
                acct1.Customer_Relationship_Manager_Primary__c <> 'Unassigned'){
        
            for(User crm1 : theActiveUsers){
                crmName = crm1.firstname + ' ' + crm1.lastname;
                if(crmName == acct1.Customer_Relationship_Manager_Primary__c){
                    crmId = crm1.id;
                    break;
                }
            }
            if (crmId <> 'Test') acct1.CRM__c = crmId;
          }    
    }            
}