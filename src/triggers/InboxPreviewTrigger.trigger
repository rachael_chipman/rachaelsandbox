trigger InboxPreviewTrigger on Campaign_Preview_Events__c (before insert) {

	
	if(trigger.isBefore && trigger.isInsert)
	{
		InboxPreviewTriggerHandler.processEvents(trigger.new);
	}
}