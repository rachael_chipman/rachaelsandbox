@isTest
public with sharing class TestSalesInvoiceOppService {
	/* Test commented out by urvi tanna - bluewolf. Functionlity does not exist anymore
	public static Account testAccount;
	public static product2 testProduct;
	public static c2g__codaGeneralLedgerAccount__c testGeneralLedgerAccount;
	public static integer NUM_OF_OPPORTUNITIES = 2;
	public static List<c2g__codaJournal__c> testJournals = new List<c2g__codaJournal__c>();
	public static integer NUM_OF_JOURNALS = 2;
	public static List<c2g__codaGroupingReference__c> testGroups = new List<c2g__codaGroupingReference__c>();
	public static PriceBookEntry testPBE;
	public static c2g__codaIncomeScheduleDefinition__c schedule;
		
	static void datasetup(){
		testGeneralLedgerAccount = TestUtils.createGeneralLedgerAccount(true);
		testAccount = TestUtils.createAccount_FinancialForce( TestUtils.PARTNER_RECORD_TYPE.Id, testGeneralLedgerAccount.Id, true );
		testProduct = TestUtils.createProduct_FinancialForce( testGeneralLedgerAccount.Id, false );
		testProduct.ProductCode = 'REF-CR';
		insert testProduct;
		testPBE = TestUtils.createPricebookEntry( testProduct.Id, TestUtils.STANDARD_PRICE_BOOK.Id, true );
		schedule = TestUtils.createIncomeSchedules(1,testGeneralLedgerAccount.id,true)[0];
	}
	
	@isTest(seeAllData=true) 	
	static void testSalesInvoiceInserts_CR_REF_lineItemBalance() 
	{
		datasetup();
		List<Opportunity> testOpportunities = TestUtils.createOpportunities( NUM_OF_OPPORTUNITIES, TestUtils.INVESTIGATING_RECORD_TYPE.Id, testAccount.Id, true );
		List<c2g__codaInvoice__c> testSalesInvoices = new List<c2g__codaInvoice__c>();
		List<c2g__codaInvoiceLineItem__c> testSalesInvoiceLineItems = new List<c2g__codaInvoiceLineItem__c>();
		
		List<OpportunityLineItem> CRopportunityLineItems = new List<OpportunityLineItem>();
		for( Opportunity anOpportunity : testOpportunities )
		{
			anOpportunity.Deal_Closing_Through__c = 'Return Path';
			anOpportunity.Who_is_Invoiced__c = 'Partner is Invoiced';
			List<PricebookEntry> pricebookEntries = new List<PricebookEntry>();
			priceBookEntries.add(testPBE);
			CROpportunityLineItems.addAll(TestUtils.createOpportunityLineItems( anOpportunity.id, pricebookEntries,1, false )); 

			TestUtils.createSalesInvoice( testAccount.Id, new Map<String,Object>{ 'c2g__Opportunity__c' => anOpportunity.Id }, false );
			testSalesInvoices.add( TestUtils.createSalesInvoice( testAccount.Id, new Map<String,Object>{ 'c2g__Opportunity__c' => anOpportunity.Id }, false ) );
		}
		update testOpportunities;	
		insert CRopportunityLineItems;

		test.startTest();
					insert testSalesInvoices;
		test.stopTest();

		List<c2g__codaJournal__c> insertedJournals = [ SELECT id, c2g__IncomeScheduleGroup__c, Opportunity__c FROM c2g__codaJournal__c WHERE Id IN :testJournals ];

		Map<ID,c2g__codaInvoiceLineItem__c> lineItemsMap = new Map<ID,c2g__codaInvoiceLineItem__c>([Select Id, c2g__UnitPrice__c,c2g__IncomeScheduleGroup__c,c2g__Invoice__r.c2g__Opportunity__c From  c2g__codaInvoiceLineItem__c where c2g__Invoice__c in : testSalesInvoices ]); 
		List<OpportunityLineItem> olis = [Select OpportunityId,TotalPrice, Product_Code__c, ListPrice From OpportunityLineItem where OpportunityId in: testOpportunities] ;
		Map<ID,OpportunityLineItem> oppCRLineItemMap = new Map<ID,OpportunityLineItem>();
		for (OpportunityLineItem oli:olis){
			oppCRLineItemMap.put(oli.OpportunityId,oli);
		}
		Map<ID,ID> invoiceLineItemGroupIdOppIdMap = new Map<ID,ID>();
		System.assertEquals(NUM_OF_OPPORTUNITIES,lineItemsMap.size(),'All sales invoice line items have been retrieved');
		System.assertEquals(NUM_OF_OPPORTUNITIES,olis.size(),'All opporutnity line items have been retrieved');
		for (c2g__codaInvoiceLineItem__c item :lineItemsMap.values()){
			System.assertNotEquals(0,item.c2g__UnitPrice__c,'The Unit price is not zero');
			System.assertEquals(item.c2g__UnitPrice__c,oppCRLineItemMap.get(item.c2g__Invoice__r.c2g__Opportunity__c).TotalPrice,'A REF-CR invoice line item has been populated for each invoice');
		}

	}	
	@isTest(seeAllData=true) 	
	static void testSalesInvoiceInserts_lineItemBalance_cr_negativeTest() 
	{
		datasetup();
		List<Opportunity> testOpportunities = TestUtils.createOpportunities( NUM_OF_OPPORTUNITIES, TestUtils.INVESTIGATING_RECORD_TYPE.Id, testAccount.Id, true );
		List<c2g__codaInvoice__c> testSalesInvoices = new List<c2g__codaInvoice__c>();
		List<c2g__codaInvoiceLineItem__c> testSalesInvoiceLineItems = new List<c2g__codaInvoiceLineItem__c>();
		
		List<OpportunityLineItem> CRopportunityLineItems = new List<OpportunityLineItem>();
		for( Opportunity anOpportunity : testOpportunities )
		{
			anOpportunity.Deal_Closing_Through__c = 'Return Path';
			anOpportunity.Who_is_Invoiced__c = 'Partner is Invoiced';
			List<PricebookEntry> pricebookEntries = new List<PricebookEntry>();
			priceBookEntries.add(testPBE);
			CROpportunityLineItems.addAll(TestUtils.createOpportunityLineItems( anOpportunity.id, pricebookEntries,1, false )); 

			TestUtils.createSalesInvoice( testAccount.Id, new Map<String,Object>{ 'c2g__Opportunity__c' => anOpportunity.Id }, false );
			// opportunity id not populated leading to causing a negative test
			testSalesInvoices.add( TestUtils.createSalesInvoice( testAccount.Id, new Map<String,Object>{ }, false ) );
		}
		update testOpportunities;	
		insert CRopportunityLineItems;

		test.startTest();	
			insert testSalesInvoices;
		test.stopTest();

		List<c2g__codaJournal__c> insertedJournals = [ SELECT id, c2g__IncomeScheduleGroup__c, Opportunity__c FROM c2g__codaJournal__c WHERE Id IN :testJournals ];

		Map<ID,c2g__codaInvoiceLineItem__c> lineItemsMap = new Map<ID,c2g__codaInvoiceLineItem__c>([Select Id, c2g__UnitPrice__c,c2g__IncomeScheduleGroup__c,c2g__Invoice__r.c2g__Opportunity__c From  c2g__codaInvoiceLineItem__c where c2g__Invoice__c in : testSalesInvoices ]); 
		List<OpportunityLineItem> olis = [Select OpportunityId,TotalPrice, Product_Code__c, ListPrice From OpportunityLineItem where OpportunityId in: testOpportunities] ;
		Map<ID,OpportunityLineItem> oppCRLineItemMap = new Map<ID,OpportunityLineItem>();
		for (OpportunityLineItem oli:olis){
			oppCRLineItemMap.put(oli.OpportunityId,oli);
		}
		Map<ID,ID> invoiceLineItemGroupIdOppIdMap = new Map<ID,ID>();
		System.assertEquals(0,lineItemsMap.size(),'All sales invoice line items have been retrieved');
		System.assertEquals(NUM_OF_OPPORTUNITIES,olis.size(),'All opporutnity line items have been retrieved');

	}

	@isTest(seeAllData=true) 	
	static void testSalesInvoiceInserts_REF_CR_deleteIncomeScheduleLineItems() 
	{
		datasetup();

		List<Opportunity> testOpportunities = TestUtils.createOpportunities( NUM_OF_OPPORTUNITIES, TestUtils.INVESTIGATING_RECORD_TYPE.Id, testAccount.Id, false );
		List<c2g__codaInvoice__c> testSalesInvoices = new List<c2g__codaInvoice__c>();
		List<c2g__codaInvoiceLineItem__c> testSalesInvoiceLineItems = new List<c2g__codaInvoiceLineItem__c>();
		List<c2g__codaScheduleLineItem__c> testScheduleLineItems = new List<c2g__codaScheduleLineItem__c>();
		
		List<OpportunityLineItem> CRopportunityLineItems = new List<OpportunityLineItem>();
		for( Opportunity anOpportunity : testOpportunities )
		{
			anOpportunity.Deal_Closing_Through__c = 'Return Path';
			anOpportunity.Who_is_Invoiced__c = 'Partner is Invoiced';			
			anOpportunity.StageName = 'Finance Approved';
			anOpportunity.Billing_Type__c = 'Annual';
			anOpportunity.Effective_Date__c =  Date.today();
			anOpportunity.Billing_Terms__c = 'Net 30';
		}
		insert testOpportunities;
		
		for( Opportunity anOpportunity : testOpportunities )
		{	
			List<PricebookEntry> pricebookEntries = new List<PricebookEntry>();
			priceBookEntries.add(testPBE);
			CROpportunityLineItems.addAll(TestUtils.createOpportunityLineItems( anOpportunity.id, pricebookEntries,1, false )); 

			TestUtils.createSalesInvoice( testAccount.Id, new Map<String,Object>{ 'c2g__Opportunity__c' => anOpportunity.Id }, false );
			testSalesInvoices.add( TestUtils.createSalesInvoice( testAccount.Id, new Map<String,Object>{ 'c2g__Opportunity__c' => anOpportunity.Id }, false ) );
		}	
		insert CRopportunityLineItems;
		insert testSalesInvoices;	
	
		for( c2g__codaInvoice__c testSalesInvoice: testSalesInvoices )
		{
			testSalesInvoiceLineItems.add( TestUtils.createSalesInvoiceLineItem( testSalesInvoice.Id, testProduct.Id, false ) );
		}
		insert testSalesInvoiceLineItems;     
        for (c2g__codaInvoice__c testSalesInvoice: testSalesInvoices){
            testScheduleLineItems.addAll( TestUtils.createScheduleLineItems(1, testSalesInvoice.Id,  false) );		
        }
        Integer i= 0;
        
		for (c2g__codaScheduleLineItem__c testScheduleLineItem : testScheduleLineItems){
			testScheduleLineItem.c2g__Amount__c = 10;
			testScheduleLineItem.c2g__UnitofWork__c = 1; 
			testScheduleLineItem.c2g__SalesInvoiceLineItem__c = testSalesInvoiceLineItems[i].id; 
			testScheduleLineItem.c2g__LineNumber__c  = i+ 1;
			i++;
		}
		test.startTest();
			insert testScheduleLineItems;	
		test.stopTest();

		List<c2g__codaJournal__c> insertedJournals = [ SELECT id, c2g__IncomeScheduleGroup__c, Opportunity__c FROM c2g__codaJournal__c WHERE Id IN :testJournals ];

		Map<ID,c2g__codaInvoiceLineItem__c> lineItemsMap = new Map<ID,c2g__codaInvoiceLineItem__c>([Select Id, c2g__UnitPrice__c,c2g__IncomeScheduleGroup__c,c2g__Invoice__r.c2g__Opportunity__c From  c2g__codaInvoiceLineItem__c where id in : testSalesInvoiceLineItems ]); 
		List<OpportunityLineItem> olis = [Select OpportunityId,TotalPrice, Product_Code__c, ListPrice From OpportunityLineItem where OpportunityId in: testOpportunities] ;
		List<c2g__codaScheduleLineItem__c> scheduleLineItems = [Select c2g__LineNumber__c, c2g__Invoice__c, c2g__Amount__c, Opportunity__c From c2g__codaScheduleLineItem__c where id in:testScheduleLineItems];
		
		System.assertEquals(NUM_OF_OPPORTUNITIES,lineItemsMap.size(),'All sales invoice line items have been retrieved');
		System.assertEquals(NUM_OF_OPPORTUNITIES,olis.size(),'All opporutnity line items have been retrieved');		
		System.assertEquals(0,scheduleLineItems.size(),'The schedule line items have been deleted');
		
	}
		
	@isTest(seeAllData=true)	
	static void testSalesInvoiceInserts_deleteIncomeScheduleLineItems_negativeTest() 
	{
		datasetup();
		// non deletion product code
		testProduct.ProductCode = 'CR-nonDelete';
		update testProduct;
		List<Opportunity> testOpportunities = TestUtils.createOpportunities( NUM_OF_OPPORTUNITIES, TestUtils.INVESTIGATING_RECORD_TYPE.Id, testAccount.Id, true );
		List<c2g__codaInvoice__c> testSalesInvoices = new List<c2g__codaInvoice__c>();
		List<c2g__codaInvoiceLineItem__c> testSalesInvoiceLineItems = new List<c2g__codaInvoiceLineItem__c>();
		List<c2g__codaScheduleLineItem__c> testScheduleLineItems = new List<c2g__codaScheduleLineItem__c>();
		
		List<OpportunityLineItem> CRopportunityLineItems = new List<OpportunityLineItem>();
		for( Opportunity anOpportunity : testOpportunities )
		{
			anOpportunity.Deal_Closing_Through__c = 'Return Path';
			anOpportunity.Who_is_Invoiced__c = 'Partner is Invoiced';			
			anOpportunity.StageName = 'Finance Approved';
			anOpportunity.Billing_Type__c = 'Annual';
			anOpportunity.Effective_Date__c =  Date.today();
			anOpportunity.Billing_Terms__c = 'Net 30';
			
			List<PricebookEntry> pricebookEntries = new List<PricebookEntry>();
			priceBookEntries.add(testPBE);
			CROpportunityLineItems.addAll(TestUtils.createOpportunityLineItems( anOpportunity.id, pricebookEntries,1, false )); 

			TestUtils.createSalesInvoice( testAccount.Id, new Map<String,Object>{ 'c2g__Opportunity__c' => anOpportunity.Id }, false );
			testSalesInvoices.add( TestUtils.createSalesInvoice( testAccount.Id, new Map<String,Object>{ 'c2g__Opportunity__c' => anOpportunity.Id }, false ) );
		}
		update testOpportunities;	
		insert CRopportunityLineItems;
		
		
		insert testSalesInvoices;	
		for( c2g__codaInvoice__c testSalesInvoice: testSalesInvoices )
		{
			testSalesInvoiceLineItems.add( TestUtils.createSalesInvoiceLineItem( testSalesInvoice.Id, testProduct.Id, false ) );
		}
		insert testSalesInvoiceLineItems;     
        for (c2g__codaInvoice__c testSalesInvoice: testSalesInvoices){
            testScheduleLineItems.addAll( TestUtils.createScheduleLineItems(1, testSalesInvoice.Id,  false) );		
        }
        Integer i= 0;
        
		for (c2g__codaScheduleLineItem__c testScheduleLineItem : testScheduleLineItems){
			testScheduleLineItem.c2g__Amount__c = 10;
			testScheduleLineItem.c2g__UnitofWork__c = 1; 
			testScheduleLineItem.c2g__SalesInvoiceLineItem__c = testSalesInvoiceLineItems[i].id; 
			testScheduleLineItem.c2g__LineNumber__c  = i+ 1;
			i++;
		}
		test.startTest();
					insert testScheduleLineItems;	
		test.stopTest();

		List<c2g__codaJournal__c> insertedJournals = [ SELECT id, c2g__IncomeScheduleGroup__c, Opportunity__c FROM c2g__codaJournal__c WHERE Id IN :testJournals ];

		Map<ID,c2g__codaInvoiceLineItem__c> lineItemsMap = new Map<ID,c2g__codaInvoiceLineItem__c>([Select Id, c2g__UnitPrice__c,c2g__IncomeScheduleGroup__c,c2g__Invoice__r.c2g__Opportunity__c From  c2g__codaInvoiceLineItem__c where id in : testSalesInvoiceLineItems ]); 
		List<OpportunityLineItem> olis = [Select OpportunityId,TotalPrice, Product_Code__c, ListPrice From OpportunityLineItem where OpportunityId in: testOpportunities] ;
		List<c2g__codaScheduleLineItem__c> scheduleLineItems = [Select c2g__LineNumber__c, c2g__Invoice__c, c2g__Amount__c, Opportunity__c From c2g__codaScheduleLineItem__c where id in:testScheduleLineItems];

		System.assertEquals(NUM_OF_OPPORTUNITIES,lineItemsMap.size(),'All sales invoice line items have been retrieved');
		System.assertEquals(NUM_OF_OPPORTUNITIES,olis.size(),'All opporutnity line items have been retrieved');
		System.assertEquals(NUM_OF_OPPORTUNITIES,scheduleLineItems.size(),'The schedule line items have been retained');
	}		


	@isTest(seeAllData=true) 	
	static void testSalesInvoiceLineItemInserts_REF_CR_removeScheduleFromLineItems() 
	{
		datasetup();

		List<Opportunity> testOpportunities = TestUtils.createOpportunities( NUM_OF_OPPORTUNITIES, TestUtils.INVESTIGATING_RECORD_TYPE.Id, testAccount.Id, false );
		List<c2g__codaInvoice__c> testSalesInvoices = new List<c2g__codaInvoice__c>();
		List<c2g__codaInvoiceLineItem__c> testSalesInvoiceLineItems = new List<c2g__codaInvoiceLineItem__c>();
		List<c2g__codaScheduleLineItem__c> testScheduleLineItems = new List<c2g__codaScheduleLineItem__c>();
		
		List<OpportunityLineItem> CRopportunityLineItems = new List<OpportunityLineItem>();
		for( Opportunity anOpportunity : testOpportunities )
		{
			anOpportunity.Deal_Closing_Through__c = 'Return Path';
			anOpportunity.Who_is_Invoiced__c = 'Partner is Invoiced';			
			anOpportunity.StageName = 'Finance Approved';
			anOpportunity.Billing_Type__c = 'Annual';
			anOpportunity.Effective_Date__c =  Date.today();
			anOpportunity.Billing_Terms__c = 'Net 30';
		}
		insert testOpportunities;
		
		for( Opportunity anOpportunity : testOpportunities )
		{	
			List<PricebookEntry> pricebookEntries = new List<PricebookEntry>();
			priceBookEntries.add(testPBE);
			CROpportunityLineItems.addAll(TestUtils.createOpportunityLineItems( anOpportunity.id, pricebookEntries,1, false )); 

			TestUtils.createSalesInvoice( testAccount.Id, new Map<String,Object>{ 'c2g__Opportunity__c' => anOpportunity.Id }, false );
			testSalesInvoices.add( TestUtils.createSalesInvoice( testAccount.Id, new Map<String,Object>{ 'c2g__Opportunity__c' => anOpportunity.Id }, false ) );
		}	
		insert CRopportunityLineItems;
		insert testSalesInvoices;	
	
		for( c2g__codaInvoice__c testSalesInvoice: testSalesInvoices )
		{
			testSalesInvoiceLineItems.add( TestUtils.createSalesInvoiceLineItem( testSalesInvoice.Id, testProduct.Id, false ) );
		}
		for (c2g__codaInvoiceLineItem__c testSalesInvoiceLineItem :testSalesInvoiceLineItems ){
			testSalesInvoiceLineItem.c2g__IncomeSchedule__c = schedule.id;
		} 
		test.startTest();
			insert testSalesInvoiceLineItems; 
		test.stopTest();

		List<c2g__codaInvoiceLineItem__c> invoicelineItems = [Select c2g__IncomeSchedule__c From  c2g__codaInvoiceLineItem__c where id in : testSalesInvoiceLineItems ]; 
		List<OpportunityLineItem> olis = [Select OpportunityId,TotalPrice, Product_Code__c, ListPrice From OpportunityLineItem where OpportunityId in: testOpportunities] ;
		
		System.assertEquals(NUM_OF_OPPORTUNITIES,invoicelineItems.size(),'All sales invoice line items have been retrieved');
		for(c2g__codaInvoiceLineItem__c invoiceLine : invoicelineItems ){
			System.assertEquals(null,invoiceLine.c2g__IncomeSchedule__c,'The sales invoice line item income schedule lookup has be set to null');
		}

		
	}
	
	@isTest(seeAllData=true) 	
	static void testSalesInvoiceLineItemInserts_REF_CR_removeScheduleFromLineItems_negativeTest() 
	{
		datasetup();
		// non schedule removal product code
		testProduct.ProductCode = 'CR-nonScheduleNull';
		update testProduct;
		List<Opportunity> testOpportunities = TestUtils.createOpportunities( NUM_OF_OPPORTUNITIES, TestUtils.INVESTIGATING_RECORD_TYPE.Id, testAccount.Id, false );
		List<c2g__codaInvoice__c> testSalesInvoices = new List<c2g__codaInvoice__c>();
		List<c2g__codaInvoiceLineItem__c> testSalesInvoiceLineItems = new List<c2g__codaInvoiceLineItem__c>();
		List<c2g__codaScheduleLineItem__c> testScheduleLineItems = new List<c2g__codaScheduleLineItem__c>();
		
		List<OpportunityLineItem> CRopportunityLineItems = new List<OpportunityLineItem>();
		for( Opportunity anOpportunity : testOpportunities )
		{
			anOpportunity.Deal_Closing_Through__c = 'Return Path';
			anOpportunity.Who_is_Invoiced__c = 'Partner is Invoiced';			
			anOpportunity.StageName = 'Finance Approved';
			anOpportunity.Billing_Type__c = 'Annual';
			anOpportunity.Effective_Date__c =  Date.today();
			anOpportunity.Billing_Terms__c = 'Net 30';
		}
		insert testOpportunities;
		
		for( Opportunity anOpportunity : testOpportunities )
		{	
			List<PricebookEntry> pricebookEntries = new List<PricebookEntry>();
			priceBookEntries.add(testPBE);
			CROpportunityLineItems.addAll(TestUtils.createOpportunityLineItems( anOpportunity.id, pricebookEntries,1, false )); 

			TestUtils.createSalesInvoice( testAccount.Id, new Map<String,Object>{ 'c2g__Opportunity__c' => anOpportunity.Id }, false );
			testSalesInvoices.add( TestUtils.createSalesInvoice( testAccount.Id, new Map<String,Object>{ 'c2g__Opportunity__c' => anOpportunity.Id }, false ) );
		}	
		insert CRopportunityLineItems;
		insert testSalesInvoices;	
	
		for( c2g__codaInvoice__c testSalesInvoice: testSalesInvoices )
		{
			testSalesInvoiceLineItems.add( TestUtils.createSalesInvoiceLineItem( testSalesInvoice.Id, testProduct.Id, false ) );
		}
		for (c2g__codaInvoiceLineItem__c testSalesInvoiceLineItem :testSalesInvoiceLineItems ){
			testSalesInvoiceLineItem.c2g__IncomeSchedule__c = schedule.id;
		}    

		test.startTest();
			insert testSalesInvoiceLineItems; 
		test.stopTest();

		List<c2g__codaInvoiceLineItem__c> invoicelineItems = [Select c2g__IncomeSchedule__c From  c2g__codaInvoiceLineItem__c where id in : testSalesInvoiceLineItems ]; 
		List<OpportunityLineItem> olis = [Select OpportunityId,TotalPrice, Product_Code__c, ListPrice From OpportunityLineItem where OpportunityId in: testOpportunities] ;
		
		System.assertEquals(NUM_OF_OPPORTUNITIES,invoicelineItems.size(),'All sales invoice line items have been retrieved');
		for(c2g__codaInvoiceLineItem__c invoiceLine : invoicelineItems ){
			System.assertEquals(schedule.id,invoiceLine.c2g__IncomeSchedule__c,'The sales invoice line item income schedule lookup is populated');
		}

		
	}*/
}