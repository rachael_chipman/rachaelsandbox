@isTest(SeeAllData=true)
public class TestUpdateProdFamilyOnSubCharge {
    
    private static Account Acc;
    private static RecordType AccRecType;
    private static Zuora__Subscription__c ZSub;
    private static Zuora__SubscriptionProductCharge__c subCharge;
    private static zqu__ZProduct__c ZProd;
    private static zqu__ProductRatePlan__c ZRatePlan;
    private static zqu__ProductRatePlanCharge__c ZRatePlanCharge;
    private static List<Zuora__CustomerAccount__c> BillingAccounts;
    private static String ZuoraId;
    private static Zuora__Product__c TestProd;
    
    private static void setUpTestData()
    {
        List<Account> Acclist = new List<Account>();
       AccRecType = TestUtils.MARKETER_RECORD_TYPE; 
       Acc  = TestUtils.createAccount(AccRecType.Id,true);
       Acclist.add(Acc);
       ZuoraId = TestUtils.generateZuoraId(32);
      // ZProd = testUtils.createZProduct('Email Optimization', true);
      ZProd = testUtils.createZProduct('Certification - License',true);
       TestProd = TestUtils.createZuoraProduct('Certification - License', true);
       ZRatePlan = TestUtils.createZ_ProductRatePlan(Zprod.Id, '00076', 'Cert','Certification - License - Annually' , true);
       ZRatePlanCharge = TestUtils.createZ_ProductRatePlanCharge('Certification - License - Annually' ,ZRatePlan.Id, 'Recurring', 'test', true);
        
       BillingAccounts = Testutils.createBillingAccounts(Acclist, true);
       ZSub = Testutils.createZ_Subscription(Acc.Id,BillingAccounts[0].Id, BillingAccounts[0].Id, 'Test Sub', 'TERMED', true);
       subCharge = TestUtils.createZ_SubscriptionCharge('Certification - License - Annually', ZSub.Id, Acc.Id,TestProd.Id, 'Certification - License - Annually', 'Recurring', false);
       subCharge.Zuora__Description__c = 'Test';
       subcharge.Zuora__Quantity__c = 5.0;
       insert subCharge;
    }
    
    private static TestMethod void  testUpdateSubBatch()
    {
        setUpTestData();
        //String query = 'Select Id, Name,Product_Family__c,Zuora__RatePlanName__c from Zuora__SubscriptionProductCharge__c where Id = ';
        UpdateProductFamilyOnSubscription UPS = new UpdateProductFamilyOnSubscription();
        Test.startTest();
        Database.executeBatch(UPS);
        Test.stopTest();

        List<Zuora__SubscriptionProductCharge__c> Tsubcharge = [Select Id,Name,Zuora__Description__c,Product_Family__c from  Zuora__SubscriptionProductCharge__c where Zuora__Description__c = 'Test'];
        System.assertNotEquals(Tsubcharge.isEmpty(),true);
        //System.assertEquals('Cert', Tsubcharge[0].Product_Family__c,'We expect the product Family to be cert');
        
    }

}