@isTest

private class GainsightCTATrigger_Test {

    static testMethod void GainsightCTATrigger_Test() {

        // Insert test Account and Contacts
        List<Account> listAccount = new List<Account>();

        Account acct1 = new Account(Name = 'Test Account 1');
        listAccount.add(acct1);

        Account acct2 = new Account(Name = 'Test Account 2');
        listAccount.add(acct2);

        Account acct3 = new Account(Name = 'Test Account 3');
        listAccount.add(acct3);

        insert listAccount;


        List<Contact> listContact = new List<Contact>();

        Contact cont1 = new Contact(FirstName = 'Test 1', LastName = 'Name', AccountId = acct1.Id, Email = 'test1@email.com', Contact_Status__c = 'Current');
        listContact.add(cont1);

        Contact cont2 = new Contact(FirstName = 'Test 2', LastName = 'Name', AccountId = acct1.Id, Email = 'test2@email.com', Contact_Status__c = 'Current');
        listContact.add(cont2);

        Contact cont3 = new Contact(FirstName = 'unused', LastName = 'unused', AccountId = acct1.Id, Email = 'no-match@wrong.com');
        listContact.add(cont3);

        insert listContact;


        // Insert test Picklist values
        List<JBCXM__Picklist__c> listPick = new List<JBCXM__Picklist__c>();

        JBCXM__Picklist__c pick1 = new JBCXM__Picklist__c(Name = 'Alert Reason 1 Name', JBCXM__SystemName__c = 'Alert Reason 1', JBCXM__Category__c = 'Alert Reason');
        listPick.add(pick1);

        JBCXM__Picklist__c pick2 = new JBCXM__Picklist__c(Name = 'Alert Reason 2 Name', JBCXM__SystemName__c = 'Alert Reason 2', JBCXM__Category__c = 'Alert Reason');
        listPick.add(pick2);

        JBCXM__Picklist__c pick3 = new JBCXM__Picklist__c(Name = 'Alert Reason 3 Name', JBCXM__SystemName__c = 'Alert Reason 3', JBCXM__Category__c = 'Alert Reason');
        listPick.add(pick3);

        JBCXM__Picklist__c pick4 = new JBCXM__Picklist__c(Name = 'Alert Reason 4 Name', JBCXM__SystemName__c = 'Alert Reason 4', JBCXM__Category__c = 'Alert Reason');
        listPick.add(pick4);

        JBCXM__Picklist__c pickOpen = new JBCXM__Picklist__c(Name = 'Alert Status Open', JBCXM__SystemName__c = 'Open', JBCXM__Category__c = 'Alert Status');
        listPick.add(pickOpen);

        JBCXM__Picklist__c pickClosed = new JBCXM__Picklist__c(Name = 'Alert Status Closed', JBCXM__SystemName__c = 'Closed', JBCXM__Category__c = 'Alert Status');
        listPick.add(pickClosed);

        insert listPick;


        // Insert test Custom Settings
        List<GS_Eloqua_Rules__c> listRules = new List<GS_Eloqua_Rules__c>();

        GS_Eloqua_Rules__c rule1 = new GS_Eloqua_Rules__c(Active__c = true, Name = 'cs 1', CTA_Reason__c = pick1.Id, Level__c = 'Account', Tag_Name__c = 'Tag 1 - Account');
        listRules.add(rule1);

        GS_Eloqua_Rules__c rule2 = new GS_Eloqua_Rules__c(Active__c = true, Name = 'cs 2', CTA_Reason__c = pick1.Id, Level__c = 'Contact', Tag_Name__c = 'Tag 1 - Contact');
        listRules.add(rule2);

        GS_Eloqua_Rules__c rule3 = new GS_Eloqua_Rules__c(Active__c = true, Name = 'cs 3', CTA_Reason__c = pick2.Id, Level__c = 'Account', Tag_Name__c = 'Tag 2 - Account');
        listRules.add(rule3);

        GS_Eloqua_Rules__c rule4 = new GS_Eloqua_Rules__c(Active__c = true, Name = 'cs 4', CTA_Reason__c = pick4.Id, Level__c = 'Contact', Tag_Name__c = 'Tag 4 - Contact');
        listRules.add(rule4);

        insert listRules;


        // Create test CTAs to trigger
        List<JBCXM__CTA__c> listCTA = new List<JBCXM__CTA__c>();

        JBCXM__CTA__c cta1 = new JBCXM__CTA__c(Name = 'CTA Test 1', JBCXM__Account__c = acct1.Id, JBCXM__Reason__c = pick1.Id, JBCXM__Comments__c = 'test1@email.com, test2@email.com;'); // Main test. Reason 1 has an Account rule and a Contact rule.
        listCTA.add(cta1);

        JBCXM__CTA__c cta2 = new JBCXM__CTA__c(Name = 'CTA Test 2', JBCXM__Account__c = acct2.Id, JBCXM__Reason__c = pick2.Id); // Reason 2 has only an Account rule.
        listCTA.add(cta2);

        JBCXM__CTA__c cta3 = new JBCXM__CTA__c(Name = 'CTA Test 3', JBCXM__Account__c = acct3.Id, JBCXM__Reason__c = pick3.Id); // Reason 3 has no rules at all.
        listCTA.add(cta3);

        JBCXM__CTA__c cta4 = new JBCXM__CTA__c(Name = 'CTA Test 4', JBCXM__Account__c = acct3.Id, JBCXM__Reason__c = pick4.Id);  // Reason 4 has only a Contact rule. This Account has no Contacts.
        listCTA.add(cta4);


        Test.startTest();
        insert listCTA;
        Test.stopTest();


        system.assert(4 == [SELECT Count() FROM Customer_Tag__c]);
        system.assert(1 == [SELECT Count() FROM Customer_Tag__c WHERE Name = 'Tag 1 - Account' AND Account__c = :acct1.Id]);
        system.assert(1 == [SELECT Count() FROM Customer_Tag__c WHERE Name = 'Tag 1 - Contact' AND Contact__c = :cont1.Id]);
        system.assert(1 == [SELECT Count() FROM Customer_Tag__c WHERE Name = 'Tag 1 - Contact' AND Contact__c = :cont2.Id]);
        system.assert(1 == [SELECT Count() FROM Customer_Tag__c WHERE Name = 'Tag 2 - Account' AND Account__c = :acct2.Id]);
    }
}