public with sharing class IPOverageOpportunityGenerationService {
    
    public static final String IP_EVENTS = 'Inbox Preview Events';
    public static final Set<String> EIS_PRODS = new Set<String>{IP_EVENTS};
    public static final String ACTIVE = 'Active';
    public static final String EIS_OVERAGE = 'Email Intelligence Solutions - Overage';

    public static zqu__ProductRatePlanCharge__c eventOverageRatePlanCharge
    {
        get
        {
            if(eventOverageRatePlanCharge == NULL)
            {
                eventOverageRatePlanCharge = [SELECT Id, Name, zqu__ZuoraId__c, zqu__ProductRatePlan__c, zqu__ProductRatePlan__r.zqu__ZuoraId__c, zqu__ProductRatePlan__r.zqu__ZProduct__r.Name, zqu__ProductRatePlan__r.Name FROM zqu__ProductRatePlanCharge__c WHERE Name = :EIS_OVERAGE AND zqu__Deleted__c = false AND (NOT zqu__ProductRatePlan__r.zqu__ZProduct__r.Name LIKE '%[DoNotUse]%') LIMIT 1];
            }
            return eventOverageRatePlanCharge;
        }
        private set;
    }

    public static void saveQuoteInformation(List<OverageFeeGenerationServices.QuoteAndAttachments> quoteAttachments)
    {
        List<zqu__QuoteAmendment__c> newAmendments = new List<zqu__QuoteAmendment__c>();
        List<zqu__QuoteRatePlan__c> newRatePlans = new List<zqu__QuoteRatePlan__c>();
        List<zqu__QuoteRatePlanCharge__c> newRatePlanCharges = new List<zqu__QuoteRatePlanCharge__c>();
        List<zqu__QuoteChargeSummary__c> newChargeSummaries = new List<zqu__QuoteChargeSummary__c> ();
        List<zqu__QuoteChargeDetail__c> newChargeDetails = new List<zqu__QuoteChargeDetail__c>();
        Map<Id, Decimal> quoteIdToOveragePrice = new Map<Id, Decimal>();
        List<zqu__Quote__c> quotesToSendForLineItems = new List<zqu__Quote__c>();

        //First we have to loop through the QuoteAndAttachment objects so that we can insert the amendments
        for(OverageFeeGenerationServices.QuoteAndAttachments qInfo : quoteAttachments)
        {
            Integer eventsUsed = qInfo.eventsUsed;
            Integer includedUnits = qInfo.includedUnits;
            Decimal price = qInfo.price;
            Decimal overageAmount = ((eventsUsed - includedUnits) * price).setScale(2);

            qInfo.amendment.zqu__Quote__c = qInfo.quote.Id;
            qInfo.amendment.zqu__Description__c += eventOverageRatePlanCharge.zqu__ProductRatePlan__r.zqu__ZuoraId__c + ', Rate Plan ' + EIS_OVERAGE;
            qInfo.amendment.zqu__TotalAmount__c = overageAmount;
            qInfo.amendment.zqu__DeltaTCV__c = overageAmount;

            quoteIdToOveragePrice.put(qInfo.amendment.zqu__Quote__c, overageAmount);
            system.debug('What is in the amount map? ' + quoteIdToOveragePrice);
            newAmendments.add(qInfo.amendment);
        }

        insert newAmendments;

        //Now that the amendments have been inserted we will need to loop through the QuoteAndAttachment objects again to update the rate plans and get them inserted
        for(OverageFeeGenerationServices.QuoteAndAttachments qInfo : quoteAttachments)
        {
            if(qInfo.amendment.Id != NULL && quoteIdToOveragePrice.containsKey(qInfo.amendment.zqu__Quote__c))
            {
                qInfo.ratePlan.Name = EIS_OVERAGE;
                qInfo.ratePlan.zqu__QuoteAmendment__c = qInfo.amendment.Id;
                qInfo.ratePlan.zqu__AmendmentType__c = qInfo.amendment.zqu__Type__c;
                qInfo.ratePlan.zqu__ProductRatePlanZuoraId__c = eventOverageRatePlanCharge.zqu__ProductRatePlan__r.zqu__ZuoraId__c;
                qInfo.ratePlan.zqu__QuoteProductName__c = eventOverageRatePlanCharge.zqu__ProductRatePlan__r.zqu__ZProduct__r.Name;
                newRatePlans.add(qInfo.ratePlan);
            }
        }

        insert newRatePlans;

        //Once we have the Quote Rate Plans inserted we need to insert the Quote Rate Plan Charges and Charge Summary records
        for(OverageFeeGenerationServices.QuoteAndAttachments qInfo : quoteAttachments)
        {
            if(qInfo.ratePlan.Id != NULL)
            {
                system.debug('What is the amount when I call the map? ' + quoteIdToOveragePrice.get(qInfo.amendment.zqu__Quote__c));
                qInfo.ratePlanCharge.zqu__QuoteRatePlan__c = qInfo.ratePlan.Id;
                qInfo.ratePlanCharge.Name = EIS_OVERAGE;
                qInfo.ratePlanCharge.zqu__ProductRatePlanChargeZuoraId__c = eventOverageRatePlanCharge.zqu__ZuoraId__c;
                qInfo.ratePlanCharge.zqu__EffectivePrice__c = quoteIdToOveragePrice.get(qInfo.amendment.zqu__Quote__c);
                qInfo.ratePlanCharge.zqu__TCV__c = quoteIdToOveragePrice.get(qInfo.amendment.zqu__Quote__c);
                qInfo.ratePlanCharge.zqu__Total__c = quoteIdToOveragePrice.get(qInfo.amendment.zqu__Quote__c);
                newRatePlanCharges.add(qInfo.ratePlanCharge);

                qInfo.chargeSummary.zqu__QuoteRatePlan__c = qInfo.ratePlan.Id;
                qInfo.chargeSummary.Name = EIS_OVERAGE;
                qInfo.chargeSummary.zqu__ChangeLog__c = 'New Rate Plan ' + '\"'+ EIS_OVERAGE + '\"';
                qInfo.chargeSummary.zqu__EffectivePrice__c = quoteIdToOveragePrice.get(qInfo.amendment.zqu__Quote__c);
                qInfo.chargeSummary.zqu__TCV__c = quoteIdToOveragePrice.get(qInfo.amendment.zqu__Quote__c);
                qInfo.chargeSummary.zqu__TotalPrice__c = quoteIdToOveragePrice.get(qInfo.amendment.zqu__Quote__c);
                newChargeSummaries.add(qInfo.chargeSummary);

                qInfo.chargeDetail.Name = EIS_OVERAGE;
                qInfo.chargeDetail.zqu__Quote__c = qInfo.quote.Id;
                qInfo.chargeDetail.zqu__ProductRatePlan__c = eventOverageRatePlanCharge.zqu__ProductRatePlan__c;
                qInfo.chargeDetail.zqu__ProductRatePlanCharge__c = eventOverageRatePlanCharge.Id;
                qInfo.chargeDetail.zqu__DeltaTCV__c = quoteIdToOveragePrice.get(qInfo.amendment.zqu__Quote__c);
                qInfo.chargeDetail.zqu__TCV__c = quoteIdToOveragePrice.get(qInfo.amendment.zqu__Quote__c);
                newChargeDetails.add(qInfo.chargeDetail);
            }
        }

        insert newRatePlanCharges;
        insert newChargeSummaries;
        insert newChargeDetails;

        ManageQuoteService.processQuotesForLineItems(quotesToSendForLineItems, NULL, false);
    }

    public static void processOverages(List<Campaign_Preview_Events__c> newEvents)
    {
        
        Set<Id> accountIds = pluck.Ids('Account__c', newEvents);
        Map<Id, Zuora__SubscriptionProductCharge__c> accountIdToSubCharge = OverageFeeGenerationServices.getExistingSubscriptionCharge(accountIds , EIS_PRODS);
        list<OverageFeeGenerationServices.OverageFeeOpportunity> feeOpps = new list<OverageFeeGenerationServices.OverageFeeOpportunity>();
        List<OverageFeeGenerationServices.QuoteAndAttachments> quoteAttachments = new List<OverageFeeGenerationServices.QuoteAndAttachments>();
        List<Campaign_Preview_Events__c> limitUpdates = new List<Campaign_Preview_Events__c>();


        for(Campaign_Preview_Events__c event : newEvents)
        {
            if(!accountIdToSubCharge.isEmpty() && accountIdToSubCharge.containsKey(event.Account__c))
            {
                Zuora__SubscriptionProductCharge__c charge = accountIdToSubCharge.get(event.Account__c);
                if(charge != NULL && charge.OveragePrice__c > 0 && charge.IncludedUnits__c > 0 && (((event.Messages__c - charge.IncludedUnits__c) * charge.OveragePrice__c) > 100.00))
                {
                    /*Campaign_Preview_Events__c limitUpdate = new Campaign_Preview_Events__c(Id = event.Id);
                    limitUpdate.Inbox_Preview_Limit__c = charge.IncludedUnits__c;
                    limitUpdates.add(limitUpdate);*/
                    Opportunity opp = OverageFeeGenerationServices.createOverageOpp(charge);
                    //system.debug('Do I have an opp after create method? ' + opp);
                    zqu__Quote__c quote = OverageFeeGenerationServices.createQuote(charge);
                    feeOpps.add(new OverageFeeGenerationServices.OverageFeeOpportunity( opp, charge, quote, IP_EVENTS));
                }
            }
        }
        //update limitUpdates;
        OverageFeeGenerationServices.saveOverageFeeOpportunities( feeOpps );
        Map<Id, zqu__Quote__c> accountIdToInsertedQuotes = OverageFeeGenerationServices.getInsertedQuotes(accountIds, IP_EVENTS);

        for(Campaign_Preview_Events__c event : newEvents)
        {
            if(!accountIdToInsertedQuotes.isEmpty() && accountIdToInsertedQuotes.containsKey(event.Account__c))
            {
                zqu__QuoteAmendment__c amendment = OverageFeeGenerationServices.createQuoteAmendment(accountIdToInsertedQuotes.get(event.Account__c));
                zqu__QuoteRatePlan__c ratePlan = OverageFeeGenerationServices.createQuoteRatePlan(accountIdToInsertedQuotes.get(event.Account__c));
                zqu__QuoteRatePlanCharge__c ratePlanCharge = OverageFeeGenerationServices.createQuoteRatePlanCharge(accountIdToInsertedQuotes.get(event.Account__c));
                zqu__QuoteChargeSummary__c chargeSummary = OverageFeeGenerationServices.createQuoteChargeSummary(accountIdToInsertedQuotes.get(event.Account__c));
                zqu__QuoteChargeDetail__c chargeDetail = OverageFeeGenerationServices.createQuoteChargeDetail(accountIdToInsertedQuotes.get(event.Account__c), rateplancharge, ratePlan);
                quoteAttachments.add( new OverageFeeGenerationServices.QuoteAndAttachments(accountIdToInsertedQuotes.get(event.Account__c), amendment, ratePlan, ratePlanCharge, chargeSummary, chargeDetail, 0, accountIdToSubCharge.get(event.Account__c).Zuora__MonthlyRecurringRevenue__c, accountIdToSubCharge.get(event.Account__c).Zuora__Quantity__c, Integer.valueOf(event.Messages__c), Integer.valueOf(accountIdToSubCharge.get(event.Account__c).IncludedUnits__c), accountIdToSubCharge.get(event.Account__c).OveragePrice__c));         
            }
        }
        saveQuoteInformation( quoteAttachments );
    }

    public static void processTrigger(List<Campaign_Preview_Events__c> newEvents)
    {
        processOverages(newEvents);
    }
}