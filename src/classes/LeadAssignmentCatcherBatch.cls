global with sharing class LeadAssignmentCatcherBatch implements Database.Batchable<sObject> {

	global Database.QueryLocator start( Database.BatchableContext BC ) {   	
   		User targetUserId = [SELECT Id FROM User WHERE Alias = 'EloqAuto'];
   		String userId = targetUserId.Id;
   		String query = 'SELECT Id FROM Lead WHERE OwnerId = \'' 
				+ userId
				+ '\' AND ( LastModifiedDate = TODAY OR LastModifiedDate = YESTERDAY )';
		System.debug( logginglevel.FINE, query );
		
		return Database.getQueryLocator( query );
	}

	global void execute( Database.BatchableContext BC, List<Lead> scope ) {
		System.debug( logginglevel.FINE, scope );
		for( Lead theLead : scope ) {
			Database.DMLOptions dmo = new Database.DMLOptions();
			dmo.assignmentRuleHeader.assignmentRuleId= '01Q00000000EmHc';
			theLead.setOptions(dmo);
		}
		try {
			update( scope );
		} catch ( Exception e ){
			
		}

	}
	
	global void finish( Database.BatchableContext BC ){
	}
	
	static testMethod void testLeadAssignmentCatcherBatch() {
		
		Account acct = [SELECT Id FROM Account LIMIT 1];
   		User targetUserId = [SELECT Id FROM User WHERE Alias = 'EloqAuto']; 
   		Lead[] leads = new List<Lead>();
      	for( Integer i = 0; i < 10; i++ ) {
         	Lead l = new Lead( 
         					FirstName = 'ApexTest20120116'
         					, LastName='testLead'+'i'
         					, OwnerId = targetUserId.Id
         					, Company = 'ApexTest20120116' 
         					); 
         	leads.add(l);
      	}
   		insert leads;
   
   		Test.StartTest();
   		LeadAssignmentCatcherBatch assigner = new LeadAssignmentCatcherBatch();
        ID batchprocessid = Database.executeBatch( assigner, 1 );
        system.AssertNotEquals( null, batchprocessid );
   		Test.StopTest();

   		system.AssertEquals( 
   			Database.countquery( 'SELECT COUNT()'
              +' FROM Lead WHERE FirstName = \'ApexTest20120116\' AND OwnerId=\'' + targetUserId.Id + '\''), 10);  
		
		leads = [SELECT Id, CreatedDate, LastModifiedDate FROM Lead WHERE FirstName = 'ApexTest20120116' ]; 
		//system.Assert( leads[0].CreatedDate < leads[0].LastModifiedDate );
		system.Assert( null != leads[0].LastModifiedDate );
	
	}
	
}