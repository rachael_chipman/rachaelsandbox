@isTest(SeeAllData=true)
private class TerminationQuoteToAccountInfoHelper_Test {
    
    private static Account testAccount;
    private static Opportunity testOpportunity;
    private static zqu__Quote__c testQuote;
    private static OpportunityLineItem testLineItem;
    private static PricebookEntry testPbe;
    private static PricebookEntry testPbe2;
    private static String RETURN_PATH = 'Return Path Pricebook';
    
    private static PricebookEntry productId (String testValue)
    {
        PricebookEntry product = [SELECT Id, Name FROM PricebookEntry WHERE isActive = true AND CurrencyIsoCode = 'USD' AND Pricebook2.Name = :RETURN_PATH AND Name = :testValue LIMIT 1];
        return product;
    }
    
    private static void setupObjects(String productName)
    {
        testAccount = TestUtils.createAccount(TestUtils.MARKETER_RECORD_TYPE.Id, true);
        testOpportunity = TestUtils.createOpportunity(TestUtils.RESEARCH_RECORD_TYPE.Id, testAccount.Id, true);
        testQuote = TestUtils.createZuoraQuote('Test Quote Name', testAccount.Id, testOpportunity.Id, TestUtils.ZUORA_QUOTE_CANCEL_RECORD_TYPE.Id, true, false);
        insert testQuote;
        testPbe = productId(productName);
        testLineItem = TestUtils.createOpportunityLineItem(testOpportunity.Id, testPbe.Id, true);
    }

    private static testMethod void testSingleQuoteUpdate()
    {
        String testValue = 'Email Optimization: Certification - License - Annually';
        Decimal quoteDelta = -5234.10;
        setupObjects(testValue);

        testLineItem.Previous_Value__c = (quoteDelta) * (-1);
        system.debug('Show previous value: ' + testLineItem.Previous_Value__c);
        update testLineItem;

        Opportunity o = [SELECT Id, Inc_ACV_Before_Partner_Comm__c FROM Opportunity WHERE Id = :testLineItem.OpportunityId];
        system.debug('Show inc acv: ' + o.Inc_ACV_Before_Partner_Comm__c);

        testQuote.zqu__Previewed_Delta_TCV__c = quoteDelta;
        testQuote.RecordTypeId = TestUtils.ZUORA_QUOTE_CANCEL_READ_ONLY.Id;
        testQuote.zqu__CancellationDate__c = system.today().addDays(15);
        testQuote.zqu__Status__c = TerminationQuoteToAccountInfoHelper.SENT_TO_ZBILL;

        test.startTest();
        update testQuote;
        test.stopTest();

        List<zqu__Quote__c> quotesToProcess = new List<zqu__Quote__c>{testQuote};

        Map<Id, String> abbreviationMap = TerminationQuoteToAccountInfoHelper.getProductNamesFromOlis(quotesToProcess);
        Map<Id, Decimal> acvMap = TerminationQuoteToAccountInfoHelper.getTerminationACV(quotesToProcess);
        Account testAccount_a = [SELECT Id, Most_Recent_Terminated_Product__c, Most_Recent_Termination_Date__c, Termination_ACV__c FROM Account WHERE Id = :testAccount.Id];
        system.assertEquals(testQuote.zqu__CancellationDate__c, testAccount_a.Most_Recent_Termination_Date__c, 'Termination Date should be cancel date');
        system.assertEquals(acvMap.get(testQuote.Id), testAccount_a.Termination_ACV__c, 'Term ACV should be Incremental ACV');
        system.assertEquals(abbreviationMap.get(testQuote.Id), testAccount_a.Most_Recent_Terminated_Product__c, 'Product should match abbreviation convention');
    }

    private static testMethod void testBulkQuoteUpdate()
    {
        String testValue1 = 'Email Optimization: Certification - License - Annually';
        String testValue2 = 'Email Fraud Protection: Domain Audit Service';

        testPbe = productId(testValue1);
        testPbe2 = productId(testValue2);
        List<Account> testAccountList = TestUtils.createAccounts(200, TestUtils.MARKETER_RECORD_TYPE.Id, true);
        List<Opportunity> testOpportunityList = new List<Opportunity>();
        List<OpportunityLineItem> testLineItems = new List<OpportunityLineItem>();
        List<zqu__Quote__c> testQuotes = new List<zqu__Quote__c>();
        

        for(Account acc : testAccountList)
        {
            Opportunity opp = TestUtils.createOpportunity(TestUtils.RESEARCH_RECORD_TYPE.Id, acc.Id, false);
            testOpportunityList.add(opp);
        }
        insert testOpportunityList;

        for(Integer i = 0; i < testOpportunityList.size(); i++)
        {
            OpportunityLineItem oli1 = TestUtils.createOpportunityLineItem(testOpportunityList[i].Id, testPbe.Id, false);
            oli1.Previous_Value__c = 7500.00;
            OpportunityLineItem oli2 = TestUtils.createOpportunityLineItem(testOpportunityList[i].Id, testPbe2.Id, false);
            oli2.Previous_Value__c = 2300.00;
            testLineItems.add(oli1);
            testLineItems.add(oli2);

            testQuotes.add(TestUtils.createZuoraQuote('Test Quote Name', testOpportunityList[i].AccountId, testOpportunityList[i].Id, TestUtils.ZUORA_QUOTE_CANCEL_RECORD_TYPE.Id, true, false));
        }
        insert testQuotes;
        insert testLineItems;


        //Send the quotes to z-billing
        for(zqu__Quote__c quote : testQuotes)
        {
            quote.RecordTypeId = TestUtils.ZUORA_QUOTE_CANCEL_READ_ONLY.Id;
            quote.zqu__CancellationDate__c = system.today().addDays(15);
            quote.zqu__Status__c = TerminationQuoteToAccountInfoHelper.SENT_TO_ZBILL;
        }

        test.startTest();
        update testQuotes;
        test.stopTest();

        //Get abbreviation map
        Map<Id, String> abbreviationMap = TerminationQuoteToAccountInfoHelper.getProductNamesFromOlis(testQuotes);
        system.debug('Show what is in the map: ' + abbreviationMap);
        Map<Id, Decimal> acvMap = TerminationQuoteToAccountInfoHelper.getTerminationACV(testQuotes);
        //Get account to quote map
        Map<Id, List<zqu__Quote__c>> accIdToQuotes = groupBy.Ids('zqu__Account__c', testQuotes);
        //Query the accounts
        List<Account> accountList = [SELECT Id, Most_Recent_Terminated_Product__c, Most_Recent_Termination_Date__c, Termination_ACV__c FROM Account WHERE Id IN : testAccountList];
        for(Account testAccount_a : accountList)
        {
            zqu__Quote__c testQuote_a;
            if(accIdToQuotes.containsKey(testAccount_a.Id))
            {
                testQuote_a = accIdToQuotes.get(testAccount_a.Id)[0];
            }
            system.assertEquals(testQuote_a.zqu__CancellationDate__c, testAccount_a.Most_Recent_Termination_Date__c, 'Termination Date should be cancel date');
            system.assertEquals(acvMap.get(testQuote_a.Id), testAccount_a.Termination_ACV__c, 'Term ACV should be Incremental ACV');
            system.assertEquals(abbreviationMap.get(testQuote_a.Id), testAccount_a.Most_Recent_Terminated_Product__c, 'Product should match abbreviation convention');
        }
    }
}