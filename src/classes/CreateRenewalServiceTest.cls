@isTest(SeeAllData=true)

public with sharing class CreateRenewalServiceTest 
{
    private static zqu__Quote__c newTestQuote;
    private static Pricebook2 testPricebook = TestUtils.STANDARD_PRICE_BOOK;
    private static Product2 testProduct;
    private static PricebookEntry testPbe;
    private static Opportunity origOpp;
    private static Id QuoteOppId;
    private static Account oppAccount;
    private static String NEW_BIZ = 'New Business';
    private static String EXT = 'Extension';
    private static String CLOSED_WON = 'Finance Approved';
    private static String RENEW = 'Renewal';
    private static String TRIAL = 'Trial';
    private static String CROSS_SELL = 'Cross-Sell';
    private static FINAL String CLIENT = 'Current Client/Partner';

    private static void setupReturnPathProducts(List<String> productNames)
    {
        List<Product2> testProducts = new List<Product2>();

        for(Integer i = 0; i < productNames.size(); i++)
        {
            testProduct = TestUtils.createProduct(false);
            testProduct.Name = productNames[i];
            testproduct.isActive = true;
            testProducts.add(testProduct);
        }
        insert testProducts;

        List<PricebookEntry> testPbes = new List<PricebookEntry>();

        for(Product2 testProd : testProducts)
        {
            testPbe = TestUtils.createPricebookEntry(testProd.Id, testPricebook.Id, false);
            testPbes.add(testPbe);
        }
        System.debug('PBE is:' + testPbes);
        insert testPbes;
    }
    private static void newSetup()
    {
        //oppAccount = TestUtils.createAccount(TestUtils.MARKETER_RECORD_TYPE.Id, true);
        /*oppAccount.EIS_Active_Subscriptions__c = 1;
        oppAccount.Total_Active_Subscriptions__c = 1;
        insert oppAccount;*/
        //origOpp = TestUtils.createOpportunity(TestUtils.PURCHASE_RECORD_TYPE.Id, oppAccount.Id, true);
         //origOpp.Pricebook2Id = testPricebook.Id;
        List<String> productNames = new List<String>{'Certification Testing: One-Time Per Unit Pricing', 'Certification Testing: Recurring Per Unit Pricing', 'Certification Testing: Usage Per Unit Pricing'};
        //List<String> productNames = new List<String>{ 'One-Time Per Unit Pricing', 'Recurring Per Unit Pricing', 'Usage Per Unit Pricing'};
        setupReturnPathProducts(productNames);
       
        newTestQuote = ExtendedTestDataSetup.getQuote('Plan for Per Unit charges', ManageQuoteService.MONTH);
        QuoteOppId = newTestQuote.zqu__Opportunity__c;
       /* List<zqu__QuoteCharge__c> testQuoteCharges = [SELECT Id, Name, zqu__ProductName__c, zqu__Total__c, zqu__EffectivePrice__c, zqu__Type__c, zqu__Period__c, zqu__Quote__r.zqu__Currency__c 
        FROM zqu__QuoteCharge__c 
        WHERE zqu__Quote__c = :newTestQuote.Id AND (zqu__Type__c = : ManageQuoteService.RECURRING OR zqu__Type__c = : ManageQuoteService.ONE_TIME)];
       */
        List<zqu__QuoteChargeDetail__c> testQuoteChargesdetails = [SELECT Id, Name ,Product_Name__c,zqu__TCV__c FROM zqu__QuoteChargeDetail__c 
        WHERE zqu__Quote__c = :newTestQuote.Id ];
        system.assert(!testQuoteChargesdetails.isEmpty(), 'We should have charges.');
        List<OpportunityLineItem> OppLineItems = [Select Id,OpportunityId from OpportunityLineItem where OpportunityId =:QuoteoppId];
        system.assert(!OppLineItems.isEmpty(),'We expect Opp line Items');
        Map<String, Decimal> prodKeyToPrice = new Map<String, Decimal>();

         
        //List<String> productNames = new List<String>();

        Decimal total = 0;
        for(zqu__QuoteChargeDetail__c charge : testQuoteChargesdetails)
        {
            /*String prodKey = charge.Product_Name__c + ': ' + charge.Name + charge.zqu__Quote__r.zqu__Currency__c;
            //productNames.add(prodKey);
            if(charge.zqu__Type__c == ManageQuoteService.RECURRING)
            {
                prodKeyToPrice.put(prodKey, ((charge.zqu__Total__c / 1) * 12));
            }
            else
            {
                prodKeyToPrice.put(prodKey, charge.zqu__Total__c);
            }
            system.debug('What is the type? ' + charge.zqu__Type__c);
            system.debug('What is the  total' + charge.zqu__Total__c);
            system.debug('Show the prod key: ' + charge.zqu__ProductName__c + ': ' + charge.Name);
            total += ((charge.zqu__Total__c / 1) * 12);*/
            String prodKey = charge.Product_Name__c + ': ' + charge.Name + 'USD';
            productNames.add(prodKey);
            prodKeyToPrice.put(prodKey,charge.zqu__TCV__c);
            total+= charge.zqu__TCV__c;
            
        }
        newTestQuote.zqu__Previewed_Total__c = total;
       // newTestQuote.zqu__Opportunity__c = origOpp.Id;
        update newTestQuote;
      	origOpp = [Select Id, Name, AccountId,StageName, Type,Certification_Tier_Sold__c from Opportunity where Id =:QuoteoppId]; 
        system.assert(origOpp!=null,'We expect Opp ');
        oppAccount = [Select Id,Name,EIS_Active_Subscriptions__c,Total_Active_Subscriptions__c ,Type from Account where Id =:origOpp.AccountId];
    }

    private static testMethod void testRenewalWithQuote()
    {
        newSetup();
	   
        origOpp.StageName = CLOSED_WON;
        origOpp.Type = NEW_BIZ;
        origOpp.Certification_Tier_Sold__c = 'Tier 1';
        test.startTest();
        update origOpp;
        test.stopTest();

        //Query for Renewal Opp
        //List<Opportunity> renewalOpps = [SELECT Id, Previous_Opportunity_Lookup__c, Type, HasOpportunityLineItem FROM Opportunity WHERE Previous_Opportunity_Lookup__c = : origOpp.Id];
        List<Opportunity> renewalOpps = [SELECT Id, Previous_Opportunity_Lookup__c, Type, HasOpportunityLineItem FROM Opportunity WHERE Previous_Opportunity_Lookup__c = :QuoteOppId];
        system.assertEquals(1, renewalOpps.size(), 'There should be one renewal opportunity');
        system.assertEquals(RENEW, renewalOpps[0].Type, 'New opp type should be renewal');
        system.assertEquals(true, renewalOpps[0].HasOpportunityLineItem, 'Renewal should have products');
    }

    private static testMethod void testRenewalWithQuote_Client_NotCert()
    {
        //Cert only client - Low Tier cert only opp
        newSetup();
       
        oppAccount.EIS_Active_Subscriptions__c = 1;
        oppAccount.Total_Active_Subscriptions__c = 1;
        update oppAccount;

        origOpp.StageName = CLOSED_WON;
        origOpp.Type = RENEW;
        origOpp.Certification_Tier_Sold__c = 'Tier 1';
        test.startTest();
        update origOpp;
        test.stopTest();

        //Query for Renewal Opp
        List<Opportunity> renewalOpps = [SELECT Id, Previous_Opportunity_Lookup__c, Type, HasOpportunityLineItem FROM Opportunity WHERE Previous_Opportunity_Lookup__c = : origOpp.Id];
        system.assertEquals(1, renewalOpps.size(), 'There should be one renewal opportunity');
        system.assertEquals(RENEW, renewalOpps[0].Type, 'New opp type should be renewal');
        system.assertEquals(true, renewalOpps[0].HasOpportunityLineItem, 'Renewal should have products');
    }

    private static testMethod void testRenewalWithQuote_Client_LOW()
    {
        //Cert only client - Low Tier cert only opp
        newSetup();
        
        oppAccount.Cert_Active_Subscriptions__c = 1;
        oppAccount.Total_Active_Subscriptions__c = 1;
        update oppAccount;
        
        

        origOpp.StageName = CLOSED_WON;
        origOpp.Type = RENEW;
        origOpp.Certification_Tier_Sold__c = 'Tier 1';
        test.startTest();
        update origOpp;
        test.stopTest();

        //Query for Renewal Opp
        List<Opportunity> renewalOpps = [SELECT Id, Previous_Opportunity_Lookup__c, Type, HasOpportunityLineItem FROM Opportunity WHERE Previous_Opportunity_Lookup__c = : origOpp.Id];
        system.assertEquals(1, renewalOpps.size(), 'There should be one renewal opportunity');
        system.assertEquals(RENEW, renewalOpps[0].Type, 'New opp type should be renewal');
        system.assertEquals(true, renewalOpps[0].HasOpportunityLineItem, 'Renewal should have products');
    }

    private static testMethod void testRenewalWithQuote_Client_HIGH()
    {
        //Cert only client - Low Tier cert only opp
        newSetup();
        
        oppAccount.Cert_Active_Subscriptions__c = 1;
        oppAccount.Total_Active_Subscriptions__c = 1;
        update oppAccount;

        origOpp.StageName = CLOSED_WON;
        origOpp.Type = RENEW;
        origOpp.Certification_Tier_Sold__c = 'Tier 7';
        test.startTest();
        update origOpp;
        test.stopTest();

        //Query for Renewal Opp
        List<Opportunity> renewalOpps = [SELECT Id, Previous_Opportunity_Lookup__c, Type, HasOpportunityLineItem FROM Opportunity WHERE Previous_Opportunity_Lookup__c = : origOpp.Id];
        system.assertEquals(1, renewalOpps.size(), 'There should be one renewal opportunity');
        system.assertEquals(RENEW, renewalOpps[0].Type, 'New opp type should be renewal');
        system.assertEquals(true, renewalOpps[0].HasOpportunityLineItem, 'Renewal should have products');
    }

    private static testMethod void testRenewalWithQuote_EXT()
    {
        newSetup();

        origOpp.StageName = CLOSED_WON;
        origOpp.Type = EXT;
        origOpp.Certification_Tier_Sold__c = 'Tier 7';
        test.startTest();
        update origOpp;
        test.stopTest();

        //Query for Renewal Opp
        List<Opportunity> renewalOpps = [SELECT Id, Previous_Opportunity_Lookup__c, Type, HasOpportunityLineItem FROM Opportunity WHERE Previous_Opportunity_Lookup__c = : origOpp.Id];
        system.assertEquals(1, renewalOpps.size(), 'There should be one renewal opportunity');
        system.assertEquals(RENEW, renewalOpps[0].Type, 'New opp type should be renewal');
        system.assertEquals(true, renewalOpps[0].HasOpportunityLineItem, 'Renewal should have products');
    }

    private static testMethod void testRenewalWithQuote_TRIAL()
    {
        newSetup();

        origOpp.StageName = CLOSED_WON;
        origOpp.Type = TRIAL;
        origOpp.Certification_Tier_Sold__c = 'Tier 1';
        test.startTest();
        update origOpp;
        test.stopTest();

        //Query for Renewal Opp
        List<Opportunity> renewalOpps = [SELECT Id, Previous_Opportunity_Lookup__c, Type, HasOpportunityLineItem FROM Opportunity WHERE Previous_Opportunity_Lookup__c = : origOpp.Id];
        system.assertEquals(1, renewalOpps.size(), 'There should be one renewal opportunity');
        if(oppAccount.Type != CLIENT)
        	system.assertEquals(New_BIZ, renewalOpps[0].Type, 'New opp type should be new business');
        else
            system.assertEquals(CROSS_SELL, renewalOpps[0].Type, 'New opp type should be cross sell');
        
        system.assertEquals(true, renewalOpps[0].HasOpportunityLineItem, 'Opp should have products');
    }

    private static testMethod void testInactiveUser_SendToFirstApprover()
    {
        newSetup();

        //create a user, make the user the owner of the account and the opportunity
        User userToDeactivate = TestUtils.getTestUser('CatWoman@dccomics.com', 'Exec Admin');
        userToDeactivate.First_Approver__c = TestUtils.CRM_USER.Id;
        User adminToDoDML = TestUtils.getTestUser('thor@marvel.com', 'Exec Admin');
        adminToDoDML.First_Approver__c = TestUtils.SALES_USER.Id;
        List<User> testUsers = new List<User>{userToDeactivate, adminToDoDML};
        insert testUsers;

        //run as new admin user since test runner did dml on user (setup object)
        system.runAs(testUsers[0])
        {
            oppAccount.OwnerId = testUsers[0].Id;
            update oppAccount;
            AccountTeamMember Teammemberad = new AccountTeamMember();
            Teammemberad.AccountId = oppAccount.Id;
            Teammemberad.UserId = testUsers[0].Id;
            Teammemberad.TeamMemberRole = CreateRenewalService.RM;
            insert Teammemberad;
        }
         
        //run as new admin user to deactivate the first new admin user
        system.runAs(testUsers[1])
        {
            //deactivate the user
            userToDeactivate.isActive = false;
            update userToDeactivate;
        }

        //then do the test, run as another admin since test runner did dml on user (setup object)
        origOpp.StageName = CLOSED_WON;
        origOpp.Type = NEW_BIZ;
        origOpp.Certification_Tier_Sold__c = 'Tier 9';
        
        system.runAs(TestUtils.SFORCE_AUTO)
        {
            test.startTest();
            update origOpp;
            test.stopTest();
        }
    }

    private static testMethod void testInactiveUser_SendToManager()
    {
        newSetup();

        //create a user, make the user the owner of the account and the opportunity
        User userToDeactivate = TestUtils.getTestUser('CatWoman@dccomics.com', 'Exec Admin');
        userToDeactivate.ManagerId = TestUtils.CRM_USER.Id;
        User adminToDoDML = TestUtils.getTestUser('thor@marvel.com', 'Exec Admin');
        adminToDoDML.ManagerId = TestUtils.SALES_USER.Id;
        List<User> testUsers = new List<User>{userToDeactivate, adminToDoDML};
        insert testUsers;

        //run as new admin user since test runner did dml on user (setup object)
        system.runAs(testUsers[0])
        {
            oppAccount.OwnerId = testUsers[0].Id;
            update oppAccount;
            AccountTeamMember Teammemberad = new AccountTeamMember();
            Teammemberad.AccountId = oppAccount.Id;
            Teammemberad.UserId = testUsers[0].Id;
            Teammemberad.TeamMemberRole = CreateRenewalService.RM;
            insert Teammemberad;
        }
         
        //run as new admin user to deactivate the first new admin user
        system.runAs(testUsers[1])
        {
            //deactivate the user
            userToDeactivate.isActive = false;
            update userToDeactivate;
        }

        //then do the test, run as another admin since test runner did dml on user (setup object)
        origOpp.StageName = CLOSED_WON;
        origOpp.Type = NEW_BIZ;
        origOpp.Certification_Tier_Sold__c = 'Tier 9';
        
        system.runAs(TestUtils.SFORCE_AUTO)
        {
            test.startTest();
            update origOpp;
            test.stopTest();
        }
    }

    private static testMethod void testInactiveUser_SendEmailNoActiveUser()
    {
        newSetup();

        //create a user, make the user the owner of the account and the opportunity
        User userToDeactivate = TestUtils.getTestUser('CatWoman@dccomics.com', 'Exec Admin');
        User adminToDoDML = TestUtils.getTestUser('thor@marvel.com', 'Exec Admin');
        List<User> testUsers = new List<User>{userToDeactivate, adminToDoDML};
        insert testUsers;

        //run as new admin user since test runner did dml on user (setup object)
        system.runAs(testUsers[0])
        {
            oppAccount.OwnerId = testUsers[0].Id;
            update oppAccount;
            AccountTeamMember Teammemberad = new AccountTeamMember();
            Teammemberad.AccountId = oppAccount.Id;
            Teammemberad.UserId = testUsers[0].Id;
            Teammemberad.TeamMemberRole = CreateRenewalService.RM;
            insert Teammemberad;
        }
         
        //run as new admin user to deactivate the first new admin user
        system.runAs(testUsers[1])
        {
            //deactivate the user
            userToDeactivate.isActive = false;
            update userToDeactivate;
        }

        //then do the test, run as another admin since test runner did dml on user (setup object)
        origOpp.StageName = CLOSED_WON;
        origOpp.Type = NEW_BIZ;
        origOpp.Certification_Tier_Sold__c = 'Tier 9';
        
        system.runAs(TestUtils.SFORCE_AUTO)
        {
            test.startTest();
            update origOpp;
            test.stopTest();
        }
    }
    
    
}