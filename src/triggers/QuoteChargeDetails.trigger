trigger QuoteChargeDetails on zqu__QuoteChargeDetail__c (after insert, after update, after delete) {
 
 if(trigger.isInsert || trigger.isUpdate)
    {
        System.debug('Calling the service function');
        LineItemGenerationService.processQuoteChargeDetails(trigger.new);
	}
}