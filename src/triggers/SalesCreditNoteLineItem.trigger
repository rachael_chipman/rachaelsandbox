trigger SalesCreditNoteLineItem on c2g__codaCreditNoteLineItem__c (before insert) {
	if( trigger.isBefore && trigger.isInsert){
		SalesCreditNoteService.populateCreditNoteLineItems(Trigger.new);
	}
}