public with sharing class GenRefCRMOwnerChangeContact {
	
	public static User genRefDefaultUser
	{
		get
		{
			if(genRefDefaultUser == NULL)
			{
				genRefDefaultUser = [SELECT Id, Gen_Ref_Default_User__c FROM User WHERE Gen_Ref_Default_User__c = true LIMIT 1];
			}
			return genRefDefaultUser;
		}
	}
	
	public static void processTrigger(List<Contact> updatedContacts, Map<Id, Contact> oldContacts)
	{
		List<Contact> filteredContacts = filterContactsOnUpdate(updatedContacts, oldContacts);
		if(!filteredContacts.isEmpty())
		{
			processContacts(filteredContacts);
		}
	}

	public static void processContacts(List<Contact> filteredContacts)
	{
		Map<Id, Promo_Code__c> contactIdToPromoCode = contactIdToPromoCodeMap(filteredContacts);
		for(Contact contact : filteredContacts)
		{
			if(contact.Promo_Code__c == NULL)
			{
				contact.OwnerId = genRefDefaultUser.Id;
				contact.Referring_CRM__c = genRefDefaultUser.Id;
				contact.Send_Gen_Ref_Notification__c = true;
			}
			else if(contact.Promo_Code__c != NULL && !contactIdToPromoCode.isEmpty() && contactIdtoPromoCode.containsKey(contact.Id))
			{
				contact.OwnerId = contactIdtoPromoCode.get(contact.Id).Account__r.CRM__c;
				contact.Referring_CRM__c = contactIdtoPromoCode.get(contact.Id).Account__r.CRM__c;
				contact.Referring_Partner__c = contactIdtoPromoCode.get(contact.Id).Account__c;
				contact.Send_Gen_Ref_Notification__c = true;
				contact.Gen_Ref_Contact__c = true;
			}
		}
	}
	
	public static List<Contact> filterContactsOnUpdate (List<Contact> updatedContacts, Map<Id, Contact> oldContacts)
	{
		List<Contact> filteredContacts = new List<Contact>();
		
		for(Contact updatedContact : updatedContacts)
		{
			Contact oldContact = oldContacts.get(updatedContact.Id);
			
			if(updatedContact.Campaign_Name__c != oldContact.Campaign_Name__c && SObjectTriggerServices.isValidCampaign( updatedContact.Campaign_Name__c ) && updatedContact.Gen_Ref_Contact__c == false)
			{
				filteredContacts.add(updatedContact);
			}
		}
		system.debug('What is in filteredContacts?'+filteredContacts);
		return filteredContacts;
	}
	
	private static Map<Id, Promo_Code__c> contactIdToPromoCodeMap (List<Contact> filteredContacts)
	{
		Set<String> promoCodeNames = new Set<String>();
		Map<Id, Promo_Code__c> contactIdToPromoCode = new Map<Id, Promo_Code__c>();
		Promo_Code__c promoCode = new Promo_Code__c();
		
		for(Contact contact : filteredContacts)
		{
			if(contact.Promo_Code__c != NULL || contactIdToPromoCode.isEmpty())
			{
				promoCodeNames.add(contact.Promo_Code__c);
			}
		}
		if(!promoCodeNames.isEmpty())
		{
			try
			{
				promoCode = [SELECT Id, Name, Account__c, Account__r.CRM__c FROM Promo_Code__c WHERE Name IN : promoCodeNames LIMIT 1];
			}
			catch (Exception e)
			{}
			
			for(Contact contact : filteredContacts)
			{
				if(contact.Promo_Code__c == promoCode.Name)
				{
					contactIdtoPromoCode.put(contact.Id, promoCode);
				}
			}
		}
		return contactIdtoPromoCode;
	}
}