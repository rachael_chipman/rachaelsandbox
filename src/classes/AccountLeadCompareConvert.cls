///////////////////This is how the batch class is called
//////////id batchinstanceid = database.executeBatch(new AccountLeadCompareConvert('select Id, Name from Matching__c where Convert_Lead__c = true'), 25);

global class AccountLeadCompareConvert implements Database.Batchable<sObject>{
global final String Query;
global AccountLeadCompareConvert(String q){Query=q;}

global Database.QueryLocator start(Database.BatchableContext BC){return Database.getQueryLocator(query);}

global void execute(Database.BatchableContext BC,List<Matching__c> scope){

List <Matching__c> m = New List<Matching__c>();
for(Matching__c mt : scope){

Lead l = [select Id, OwnerId, Owner.Isactive from Lead where Id = :mt.Lead__c];

 if(l.Owner.IsActive == false){
 l.OwnerId = mt.Account__r.OwnerId; 
 update l;}
 try{
 Database.LeadConvert lc = new Database.LeadConvert();
        lc.setLeadId(l.Id);
        lc.setDoNotCreateOpportunity(true);
        lc.setConvertedStatus('Qualified');
        lc.setAccountId(mt.Account__c);
        Database.LeadConvertResult lcr = Database.convertLead(lc);
        
        mt.Convert_Lead__c = false;
        mt.Converted__c = true;
        m.add(mt);
 }
 catch (Exception e){}
}
update m;
}

global void finish(Database.BatchableContext BC){
//Send an email to the User after your batch completes
Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
String[] toAddresses = new String[] {'rachael.chipman@returnpath.com'};
mail.setToAddresses(toAddresses);
mail.setSubject('Apex Batch Job is done');
mail.setPlainTextBody('The batch Apex job processed');
Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
}
}