global class CalculateTermACVLast12Months implements Schedulable {
	global void execute( SchedulableContext ctx )
	{
		CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, NextFireTime FROM CronTrigger WHERE Id = :ctx.getTriggerId()];
		
		system.debug( ct.CronExpression );
		system.debug( ct.TimesTriggered );
		
		list<Account> scope = [SELECT Id FROM Account WHERE Count_of_Termination_Ops__c > 0];
		
		set<Id> uniqueCustomObjectSet = new set<Id>();
		for( Account acc : scope )
		{
			uniqueCustomObjectSet.add( acc.Id );
		}
		
		list<AggregateResult> totalTermAmounts = [SELECT AccountId acctId, SUM(Prev_Recurring_ACV_Before_Partner_Comm__c)sum 
		FROM Opportunity 
		WHERE AccountId IN: uniqueCustomObjectSet AND CloseDate = LAST_N_DAYS:365 and StageName = 'Terminated Contract'
		GROUP BY AccountId ];
		
		list<Account> accountsToUpdate = new list<Account>();
		
		for( AggregateResult total : totalTermAmounts )
		{
			Id accountId = ( id ) total.get( 'acctId' ); 
			
			for( Account acc : scope )
			{
				if( acc.Id == accountId )
				{
					acc.Sum_of_Terminated_ACV_Last_12_months__c = ( decimal ) total.get( 'sum' );
					accountsToUpdate.add( acc );
				}
			}
		}
		update accountsToUpdate;
	}
	

}