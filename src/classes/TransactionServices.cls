public with sharing class TransactionServices
{
	private static final String TRANSACTION_LOOKUP = 'c2g__Transaction__c';
	private static final String COMPLETE_STATUS = 'Complete';

	// From BluewolfDevUtils
	public static String flattenListForQuery( List<Object> incoming )
	{

		String query = '';

		for(Object addToString : incoming)
		{
			query += '\'' + String.valueOf(addToString) + '\',';
		}

		if(query != '')
		{
			query = query.substring(0, query.length()-1);

			query = '(' + query + ') ';
		}

		return query;
	}



	public static List<sObject> filterPostedSObjects(List<sObject> newList, Map<id,sObject> oldMap, Schema.sObjectField stage, Schema.sObjectField opportunityLookup )
	{
		Select.Filter isPosted = Select.Field.hasChanged( TRANSACTION_LOOKUP ).andx( Select.Field.notNull( TRANSACTION_LOOKUP ) );
		Select.Filter isOpportunityPopulated = Select.Field.hasChanged( opportunityLookup );

		return ( isPosted.orx( isOpportunityPopulated ).andx( Select.Field.notNull( opportunityLookup ) ).andx( Select.Field.notNull( TRANSACTION_LOOKUP ) ) ).filter( newList, oldMap );
		
	}

	public static List<sObject> filterInvoiceTransactionLineItems( List<sObject> newList )
	{
		return Select.Field.isEqual( c2g__codaTransactionLineItem__c.Transaction_Type__c, 'Invoice' ).filter( newList);
	}

	public static List<c2g__codaTransaction__c> populateOpportunity(List<sObject> filteredSObjects, Schema.sObjectField opportunityLookup)
	{
		List<c2g__codaTransaction__c> transactionsToBeUpdated = new List<c2g__codaTransaction__c>();
		for(sObject filteredSObject: filteredSObjects)
		{
			if(filteredSObject.get(opportunityLookup) != null && filteredSObject.get( TRANSACTION_LOOKUP ) != null)
			{
				c2g__codaTransaction__c relatedTransaction = new c2g__codaTransaction__c(Id = (Id)filteredSObject.get( TRANSACTION_LOOKUP ));
				relatedTransaction.Opportunity__c = (Id) filteredSObject.get(opportunityLookup);
				transactionsToBeUpdated.add( relatedTransaction );
			}
		}
		return transactionsToBeUpdated;
	}

	public static void updateTransactions( List<c2g__codaTransaction__c> transactionsToBeUpdated, List<sObject> triggeredObjects )
	{
		try
		{
			update transactionsToBeUpdated;
		}
		catch( DmlException ex )
		{
			Map<Id,List<sObject>> transactionIdToSobjects = GroupBy.ids( TRANSACTION_LOOKUP, triggeredObjects );

			for ( Integer index = 0 ; index < ex.getNumDml() ; index++ )
			{
				Id errorId = transactionsToBeUpdated[ ex.getDmlIndex( index ) ].Id;
				transactionIdToSobjects.get( errorId )[0].addError( ex.getDmlMessage( index ) );
			}
		}
	}

	private static String buildTransactionLineItemQuery( List<SObject> sObjects )
	{
		String baseQuery = '';
		Schema.SObjectType type = sObjects[0].getSObjectType();
		Boolean isTransactionLineItem = ( type == c2g__codaTransactionLineItem__c.sObjectType ) ;
		Boolean isTransaction = ( type == c2g__codaTransaction__c.sObjectType ) ;

		if( !isTransactionLineItem && !isTransaction )
		{
			return baseQuery;
		}
		baseQuery += 'SELECT Id, Opportunity__c, c2g__Transaction__c, c2g__Transaction__r.Opportunity__c From c2g__codaTransactionLineItem__c WHERE ';
		if( sObjects[0].getSObjectType() == c2g__codaTransactionLineItem__c.sObjectType )
		{
			baseQuery += 'Id IN ';
		}
		else
		{
			baseQuery += 'c2g__Transaction__c IN ';
		}

		baseQuery +=  flattenListForQuery( new List<Id>( Pluck.ids( sObjects ) ) );
		return baseQuery;
	}

	public static List<c2g__codaTransactionLineItem__c> populateOpportunityOnLineItems( List<SObject> relatedSObjects )
	{
		List<c2g__codaTransactionLineItem__c> relatedTransactionLineItems = new List<c2g__codaTransactionLineItem__c>();
		String query = buildTransactionLineItemQuery( relatedSObjects );
		if( String.isBlank( query ) )
		{
			return relatedTransactionLineItems;
		}
		relatedTransactionLineItems = Database.query( query );
		for( c2g__codaTransactionLineItem__c aTransactionLineItem : relatedTransactionLineItems )
		{
			aTransactionLineItem.Opportunity__c = aTransactionLineItem.c2g__Transaction__r.Opportunity__c;
		}
		return relatedTransactionLineItems;
	}

	public static void updateTransactionLineItems( List<c2g__codaTransactionLineItem__c> transactionsLineItemsToBeUpdated, List<sObject> triggeredObjects )
	{
		try
		{
			update transactionsLineItemsToBeUpdated;
		}
		catch( DmlException ex )
		{
			Map<Id,List<sObject>> transactionIdToSobjects = GroupBy.ids( TRANSACTION_LOOKUP, triggeredObjects );

			for ( Integer index = 0 ; index < ex.getNumDml() ; index++ )
			{
				Id errorId = transactionsLineItemsToBeUpdated[ ex.getDmlIndex( index ) ].c2g__Transaction__c;
				transactionIdToSobjects.get( errorId )[0].addError( ex.getDmlMessage( index ) );
			}
		}
	}

	public static void processTransactionsAndLineItems( List<c2g__codaTransaction__c> relatedTransactions, List<sObject> triggeredObjects )
	{
		updateTransactions( relatedTransactions, trigger.new );

		List<c2g__codaTransactionLineItem__c> relatedTransactionLineItems =  populateOpportunityOnLineItems( relatedTransactions );
		if( !relatedTransactionLineItems.isEmpty() )
		{
			updateTransactionLineItems( relatedTransactionLineItems, trigger.new  );

		}
	}
}