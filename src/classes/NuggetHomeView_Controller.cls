public class NuggetHomeView_Controller {


Public List<Nugget__c> MRnugs;
Public List<Nugget__c> HSnugs;

Public string strID {get;set;}
Public String uId = UserInfo.getUserId();

   
    
     Public List<Nugget__c> getHSnugs(){  
    
    HSnugs = [select Id, Name, Company_Name__c, Assigned_to_Sales_Rep__c, Alchemy_Lead_Score_Alpha__c, Alchemy_Lead_Score_calc__c from Nugget__c where OwnerId = :uid and Status__c = 'Assigned' order by Alchemy_Lead_Score_calc__c DESC Limit 5];
    
    return HSnugs;
    
    }
    
    
    Public List<Nugget__c> getMRnugs(){  
    
    MRnugs = [select Id, Name, Company_Name__c, Alchemy_Lead_Score_calc__c, Alchemy_Lead_Score_Alpha__c, Assigned_to_Sales_Rep__c from Nugget__c where OwnerId = :uid and Status__c = 'Assigned' order by Assigned_to_Sales_Rep__c DESC Limit 5];
    
    return MRnugs;
    
    } 
    
    Public Pagereference goto(){
       
     pageReference ref = new PageReference('/'+strID);
    ref.setRedirect(true);
    return ref;
     
    }
    
    
   
}