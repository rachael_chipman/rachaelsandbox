//Batch Class
///////////////////This is how the batch class is called
//////////id batchinstanceid = database.executeBatch(new PartnerInvoiceCreateBatch('select Id, c2g__Account__c from c2g__codaInvoice__c where c2g__Account__c != null and Partner_is_Billing_Agent__c = true'), 25);

global class PartnerInvoiceCreateBatch implements Database.Batchable<sObject>
{
global final String Query;

global final List<Partner_Invoice__c> inpi;
global PartnerInvoiceCreateBatch(List<Partner_Invoice__c> a, String q)
{
Query = q;
inpi = a;
 
}

global Database.QueryLocator start(Database.BatchableContext BC)
{
return Database.getQueryLocator(Query);
}

global void execute(Database.BatchableContext BC,List<c2g__codaInvoice__c> scope){

    
List<c2g__codaInvoice__c> updatesi2 = new List<c2g__codaInvoice__c>();   
                      
for(c2g__codaInvoice__c si2: scope){
Partner_Invoice__c pi2 = [select Id, Invoice_Total__c from Partner_Invoice__c where Id in :inpi and Partner_Account__c = :si2.c2g__Account__c];
    si2.Partner_Invoice__c = pi2.Id;
    updatesi2.add(si2);
    
    
}

update updatesi2;

List<c2g__codaCreditNote__c> cns = new List<c2g__codaCreditNote__c>();

for(c2g__codaCreditNote__c cn : [Select Id, c2g__Account__c, Partner_Invoice__c, c2g__Invoice__c from c2g__codaCreditNote__c where c2g__Invoice__c in :scope and c2g__CreditNoteStatus__c = 'Complete']){

   c2g__codaInvoice__c inv = [select Id, Partner_Invoice__c from c2g__codaInvoice__c where Id = :cn.c2g__Invoice__c and Partner_Invoice__c != null];
   
   cn.Partner_Invoice__c = inv.Partner_Invoice__c;
   
   cns.add(cn);
   }
   
   update cns;


List<Partner_Invoice__c> updatepi = new List<Partner_Invoice__c>();

Decimal newprice;
Decimal newprice2;

for(Partner_Invoice__c p : inpi){

     List<AggregateResult> price = [select SUM(Invoice_Total__c) from c2g__codaInvoice__c where Partner_Invoice__c = :p.Id];
     for (AggregateResult ar : price)  {
     newprice = (Decimal) ar.get('expr0');}
     
     List<AggregateResult> price2 = [select SUM(c2g__CreditNoteTotal__c) from c2g__codaCreditNote__c where Partner_Invoice__c = :p.Id];
     for (AggregateResult ar : price2)  {
     newprice2 = (Decimal) ar.get('expr0');}
     
p.Invoices_Total__c = newprice;
p.Credits_Total__c = newprice2;
if(newprice != null && newprice2 != null){p.Outstanding_Value__c = newprice - newprice2;}
if(newprice != null && newprice2 == null){p.Outstanding_Value__c = newprice;}


    updatepi.add(p);
 

}

update updatepi;


}

global void finish(Database.BatchableContext BC){
//Send an email to the User after your batch completes
Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
String[] toAddresses = new String[] {'matt.starr@returnpath.com'};
mail.setToAddresses(toAddresses);
mail.setSubject('Partner Invoice Apex Batch Job is done');
mail.setPlainTextBody('Partner Invoices have been created.');
Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
}
}