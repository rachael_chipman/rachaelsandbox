trigger OpportunityLineItemTrigger on OpportunityLineItem (after insert, before update, before insert, after delete)
{
	

	/*if( Trigger.isUpdate )
	{
		if( Trigger.isBefore )
		{
			List<OpportunityLineItem> filteredOlis = Select.Field.hasChanged('End_Date__c').filter(Trigger.new, Trigger.oldMap);

			List<OpportunityLineItem> queriedOlis = [ SELECT Id, Previous_Value__c, Opportunity.Type, TotalPrice, Start_Date__c, End_Date__c,
															Opportunity.Previous_Opportunity_Lookup__c, PriceBookEntry.Product2Id
														FROM OpportunityLineItem
														WHERE OpportunityId IN :Pluck.Ids('OpportunityId', filteredOlis)
															AND (Previous_Value__c != 0 AND Previous_Value__c != NULL)
															AND Id IN :filteredOlis ];

			if(!queriedOlis.isEmpty())
			{
				ProductServices.populatePreviousValue(queriedOlis, trigger.newMap);
			}
		}
	}*/

	if(trigger.isInsert)
	{
		if(trigger.isAfter)
		{
			system.debug('what is trigger.new' +  trigger.new);

			List<OpportunityLineItem> queriedOlis = new List<OpportunityLineItem>();

			if(trigger.oldMap == NULL)
			{
				 queriedOlis = [ SELECT Id, Quantity, OpportunityId, Opportunity.isClosed, PricebookEntry.Name, Opportunity.Certification_Max_Volume__c
														FROM OpportunityLineItem 
														WHERE PriceBookEntry.Name LIKE : '%Certification - License%' AND Opportunity.isClosed = false and Id IN :trigger.new];
			}
			system.debug('What is queriedOlis ' + queriedOlis);
			ProductServices.populateCertificationTier(queriedOlis, trigger.newMap);
		}
	}
	if(trigger.isInsert && Trigger.isAfter)
	{
		System.debug('Calling from the trigger.');
		NamingConventionService.filterOpportunity(Trigger.new);
	}

	if(trigger.isDelete && Trigger.isAfter)
	{
		NamingConventionService.filterOpportunity(trigger.old);
	}
}