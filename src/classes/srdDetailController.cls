public class srdDetailController {

    public string vol {get;set;}
    public string ips {get;set;}
    public string delta {get;set;}
    public Opportunity o {get;set;}
    public Account a {get;set;}
    public User u {get;set;}
    
    public String nothing() {return null;}
    
    public void sendDetails() {
        if (o == null) o = [select amount, ownerid, description, accountid, srd__c from opportunity where id = :ApexPages.currentPage().getParameters().get('id')];
        if (a == null) a = [select name, ACV_TOTAL__c from account where id = :o.accountid];
        if (u == null) u = [select name from user where id = :o.ownerid];
        
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        
        mail.setSenderDisplayName('Salesforce Notifications');
        
        String[] toAddresses = new String[] {'margaret.farmakis@returnpath.net'};
        String[] bccAddresses = new String[] {'adam.bryant@returnpath.net'};
        mail.setToAddresses(toAddresses);
        mail.setBccAddresses(bccAddresses);
        mail.setSubject('New SRD Request');
        mail.setHTMLBody(
            u.name + ' has added SRD Consulting to an opportunity for ' + a.name + '.' +
            '<p>They have provided the following client variables:' + 
            '<p><b>Volume:</b> ' + vol + 
            '<br/><b>Percentage of IPs Affected:</b> ' + ips + 
            '<br/><b>SRD Delta:</b> ' + delta +
            '<br/><b>Opportunity Amount:</b> ' + o.amount +
            '<br/><b>' + a.name + ' Total ACV:</b> ' + a.acv_total__c +
			'<p>Click here to be taken to the opportunity: ' + 'http://ssl.salesforce.com/'+o.id);
		Messaging.sendEmail(new Messaging.SingleEmailMessage[] {mail});
        
        if(o.description == null) o.description = 'Volume: ' + vol + ' // IPs affected: ' + ips + ' // Change in SRD: ' + delta + ' // Account ACV: ' + a.acv_total__c + ' ]';
            else o.description = o.description + ' [ Volume: ' + vol + ' // IPs affected: ' + ips + ' // Change in SRD: ' + delta + ' // Account ACV: ' + a.acv_total__c + ' ]';
        o.srd__c = 'Sent';
        update o;
    } 
}