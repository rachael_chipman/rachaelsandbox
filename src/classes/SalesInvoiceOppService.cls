public with sharing class SalesInvoiceOppService {

	public static void createInvoiceLineItems(Map<ID,c2g__codaInvoice__c> newSalesInvoicesMap){

		List<c2g__codaInvoice__c> newSalesInvoices = newSalesInvoicesMap.values();
		Set<ID> opportunityIds = Pluck.ids('c2g__Opportunity__c',newSalesInvoices);
		List<c2g__codaInvoiceLineItem__c> newSalesInvoiceLineItems = new List<c2g__codaInvoiceLineItem__c>();

		List<OpportunityLineItem> olis = new List<OpportunityLineItem>();
		Map<ID,Opportunity> filteredOpportunities = new Map<iD,Opportunity>(
									[Select Id, Who_is_Invoiced__c, Deal_Closing_Through__c, CurrencyISOCode,
										(Select Product_Code__c,TotalPrice,UnitPrice,Quantity, PricebookEntry.Product2Id,Start_Date__c,End_Date__c,Description,Number_of_Journals_Calc__c From OpportunityLineItems where Product_Code__c = 'REF-CR') 
									From Opportunity  where Deal_Closing_Through__c = 'Return Path' 
									AND Who_is_Invoiced__c = 'Partner is Invoiced' 
									AND Id in: opportunityIds
									]);
															
		for (c2g__codaInvoice__c newsalesInvoice: newSalesInvoices){
			if (filteredOpportunities.get(newSalesInvoice.Opportunity_Id__c) != null
					&& filteredOpportunities.get(newSalesInvoice.Opportunity_Id__c).OpportunityLineItems != null 
					&& !filteredOpportunities.get(newSalesInvoice.Opportunity_Id__c).OpportunityLineItems.isEmpty()){
				for (OpportunityLineItem oli : filteredOpportunities.get(newSalesInvoice.Opportunity_Id__c).OpportunityLineItems){

					olis.add(oli);					
					newSalesInvoiceLineItems.add(					
						new c2g__codaInvoiceLineItem__c (
							c2g__Invoice__c = newsalesInvoice.id,
							c2g__UnitPrice__c = Math.abs(oli.UnitPrice),
							c2g__Quantity__c = oli.Quantity,
							c2g__UnitOfWork__c = 1,
							AR_Gross_Up_Item__c = true,
							c2g__Product__c = oli.PricebookEntry.Product2Id,
					        c2g__StartDate__c = oli.Start_Date__c,
					        End_Date__c = oli.End_Date__c,
					        c2g__LineDescription__c = oli.Description,
					        ffbilling__DeriveUnitPriceFromProduct__c = false,
					        c2g__IncomeSchedule__c = null,
							Opportunity__c = newsalesInvoice.c2g__Opportunity__c
						)
					);

				}
			}
		}

		try {
			
			insert newSalesInvoiceLineItems;
			
		} catch(DMLException exc){

			for( Integer index = 0; index < exc.getNumDml(); index++ ){
				newSalesInvoicesMap.get(newSalesInvoiceLineItems[exc.getDmlIndex(index)].c2g__Invoice__c).addError(exc.getDmlMessage(index));						
			}			
		}
		
								
	}
	public static void removeScheduleFromInvoiceLineItems(Map<ID,c2g__codaInvoiceLineItem__c> newInvoiceLineItemsMap){
		
		List<c2g__codaInvoiceLineItem__c> refCRSalesInvoiceLineItems = [Select c2g__Product__r.ProductCode,  c2g__IncomeSchedule__c, c2g__Invoice__c  
									From c2g__codaInvoiceLineItem__c 
									where id in : newInvoiceLineItemsMap.keySet()
									and c2g__invoice__r.c2g__Opportunity__r.Deal_Closing_Through__c = 'Return Path'
									and c2g__invoice__r.c2g__Opportunity__r.Who_is_Invoiced__c = 'Partner is Invoiced' 									
									AND c2g__Product__r.ProductCode = 'REF-CR'];
									
		for (c2g__codaInvoiceLineItem__c refCRSalesInvoiceLineItem : refCRSalesInvoiceLineItems){
			refCRSalesInvoiceLineItem.c2g__IncomeSchedule__c = null;
		}
		if (!refCRSalesInvoiceLineItems.isEmpty()){
			try {
								
				update refCRSalesInvoiceLineItems;
				
			} catch(DMLException exc){
	
				for( Integer index = 0; index < exc.getNumDml(); index++ ){
					newInvoiceLineItemsMap.get(refCRSalesInvoiceLineItems[exc.getDmlIndex(index)].id).addError(exc.getDmlMessage(index));						
				}			
			}	
		}					
	}
	
	public static void deleteIncomeScheduleLineItems(Map<ID,c2g__codaScheduleLineItem__c> newScheduleLineItemsMap){
		
		List<c2g__codaScheduleLineItem__c> scheduleLineItems = new List<c2g__codaScheduleLineItem__c>();
		scheduleLineItems = [Select id, c2g__invoice__c, c2g__invoice__r.c2g__Opportunity__c From c2g__codaScheduleLineItem__c 
															where c2g__SalesInvoiceLineItem__r.c2g__Product__r.ProductCode = 'REF-CR' 
															and c2g__invoice__r.c2g__Opportunity__r.Deal_Closing_Through__c = 'Return Path'
															and c2g__invoice__r.c2g__Opportunity__r.Who_is_Invoiced__c = 'Partner is Invoiced' 
															and id in: newScheduleLineItemsMap.keySet()];
		
		if (!schedulelineItems.isEmpty()){
			try {				
				delete scheduleLineItems;
				
			} catch(DMLException exc){
	
				for( Integer index = 0; index < exc.getNumDml(); index++ ){
					newScheduleLineItemsMap.get(scheduleLineItems[exc.getDmlIndex(index)].id).addError(exc.getDmlMessage(index));						
				}			
			}	
		}					
	}


}