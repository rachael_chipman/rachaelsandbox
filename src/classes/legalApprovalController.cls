/*  Written by Adam Bryant for Return Path, March 2012
*       
*       Legal Approval Process Controller: Facilitates the review of contracts submitted from reps to RP's legal team.
*       
*/

public class legalApprovalController {
        
        // There are the main variables for the entire process.

        public Opportunity o {get;set;}
        public Contract c {get;set;}
        public Attachment attachment {
                get {
                if (attachment == null)
                        attachment = new Attachment();
                return attachment;
        }
                set;
        }
        
        // These are state variables to control the workflow depending on user choices
        
        public String cid {get;set;}
        public String choice {get;set;}
        public String attachChoice {get;set;}
        public Boolean attachMoreChoice {get;set;}
        public Boolean finalStep {get;set;}
        public Boolean endStep {get;set;}
        public Boolean uploaded {get;set;}
        public String status {
                get { return o.Legal_Approval_Status__c; }
                set;
        }

        // Establish our opportunity identity and generate a contract
        
        public legalApprovalController() {
                if(c == null) c = new Contract();
                if (o == null) o = [select id, CloseDate, Legal_Approval_Status__c, accountid, name, Kick_Off_Date__c, TermofContractinmonths__c, contract__c from opportunity where id = :ApexPages.currentPage().getParameters().get('oid')];
        }
        
        //Check to see if there is already a contract for this opp. If so, go to the contract record. If not, start the workflow
        
        public PageReference checkStatus() 
        {
            if(o.CloseDate >= system.today())
            {
                if (o.Legal_Approval_Status__c <> 'Not yet started')
                {
                    PageReference needsApproval = new PageReference('/apex/contractOverride?id='+o.contract__c);
                    return needsApproval;
                } 
                else 
                { 
                    return null; 
                }
            }
            else
            {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,' The Close Date for open Opportunities may not be earlier than today. Please change the Close Date to today or a date in the future.'));
            }
            return null;
        }
        
        // Update contract fields and link it to the opportunity in both directions, then insert to the database
        
        public PageReference save() {
                c.name = 'Contract: ' + o.name;
                c.accountid = o.accountid;
                c.opportunity__c = o.id;
                c.startdate = o.kick_off_date__c;
                c.contractterm = 12;
                c.StartDate = date.today();
                PageReference next;
                
                // Decide the next page in the workflow depending on the user's response regarding material changes
                
                if (choice == 'Yes' && c.Type_of_review__c!= NULL && c.Type_of_Legal_Agreement__c != NULL) { 
                        c.legal_stage__c = 'Not yet submitted';
                        insert c;
                        o.contract__c = c.id;
                        update o;
                        next = new PageReference('/apex/legalApproval?oid='+o.id);
                        next.setRedirect(false);
                }
                
                if (choice == 'No') {
                        c.legal_stage__c = 'No review needed';
                        insert c;
                        o.contract__c = c.id;
                        update o;
                        next = next = new PageReference('/apex/legalskip?oid='+o.id);
                        next.setRedirect(false);
                }
                 else
                {
                    ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, 'Please ensure you have populated all the required fields'));
                }
                return next;
        }
        
        
        // These methods control the display of options and fields on their screen, and process the outcomes of those choices
        
        public void chooseAttach() {
                attachChoice = 'yes';
        }
        
        public void skipAttach() {
                attachChoice = 'no';
                finalStep = true;
                approvalProcess();
        }
                
        public void uploadDraft() {
                
                // Upload the contract to the related opportunity record
                
                attachment.OwnerId = UserInfo.getUserId();
                attachment.ParentId = o.id;
                attachment.name = 'ContractDraft.docx';
                attachment.IsPrivate = false;
        attachment.contentType = 'application/msword';
                
                // Print the outcome to the user, and set our state variables accordingly
    
                try { insert attachment; } 
                catch (DMLException e) { ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Error uploading attachment')); } 
                finally { attachment = new Attachment(); }
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.INFO,'Attachment uploaded successfully'));
                uploaded = true;
        }
    
    public void uploadFinal() {
                
                // Upload the contract to the related opportunity record
                
                attachment.OwnerId = UserInfo.getUserId();
                attachment.ParentId = o.id;
                attachment.name = 'ContractFinal.pdf';
                attachment.IsPrivate = false;
        attachment.contentType = 'application/pdf';
                
                // Print the outcome to the user, and set our state variables accordingly
    
                try { insert attachment; } 
                catch (DMLException e) { ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Error uploading attachment')); } 
                finally { attachment = new Attachment(); }
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.INFO,'Attachment uploaded successfully'));
                uploaded = true;
        }
        
        public void attachMore() {
                attachMoreChoice = true;
                uploaded = false;
                finalStep = false;
        }
        
        public void skipAttachMore() {
                attachMoreChoice = false;
                finalStep = true;
                approvalProcess();
        }
                
        public void approvalProcess() {
        
                // Create an approval request for the contract
    
                Approval.ProcessSubmitRequest req1 = new Approval.ProcessSubmitRequest();
                req1.setObjectId(c.id);
        
                // Submit the approval request for the contract
    
                Approval.ProcessResult result = Approval.process(req1);
        
                // Verify the result 
    
                System.assert(result.isSuccess());
                endStep = true;
        }
        
        public pageReference viewOpp() {
                PageReference OppPage = new PageReference('/'+o.id);
                OppPage.setRedirect(true);
                return OppPage;
        }
}