public with sharing class PortalWizardReferral_Controller {

    Public Lead l {get;set;}  
    Public Account UserAcct {get;set;}
    Public String acctname {get;set;}
    
    public PortalWizardReferral_Controller(ApexPages.StandardController controller) {

    User u = [Select Id, ContactId from User where Id = :UserInfo.getUserId()];
    
    Contact c = [Select Id, AccountId from Contact where Id = :u.ContactId];
    
    UserAcct = [select Id, Name, CRM__r.Id from Account where Id = :c.AccountId];
    
    acctname = UserAcct.Name.substring(0,7);
    
    l = new Lead(
    First_Campaign__c = 'Deliverability Channel Referral',
    Campaign_Name__c = 'Deliverability Channel Referral',
    Gen_Ref_Lead__c = false);
      

    }
    
      public List<selectOption> getPCs() {
        List<selectOption> options = new List<selectOption>(); //new list for holding all of the picklist options
        options.add(new selectOption('', '- None -')); //add the first option of '- None -' in case the user doesn't want to select a value or in case no values are returned from query below
        for (Promo_Code__c promos : [SELECT Id, Name, Account__r.Name FROM Promo_Code__c WHERE Account__r.Name like :acctname+'%' Order by Name]) { //query for User records with System Admin profile
            options.add(new selectOption(promos.Name, promos.Name+' - '+promos.Account__r.Name)); //for all records found - add them to the picklist options
        }
        return options; //return the picklist options
    }

  Public pagereference Next(){
  

  insert l;
  
  CampaignMember cm = new CampaignMember(
  LeadId = l.Id,
  CampaignId = '7010000000067J5');
  
  insert cm;
  
  return new PageReference('/'+l.Id);       
   }

    Public Pagereference Cancel(){
    return new PageReference('/home/home.jsp');
}
}