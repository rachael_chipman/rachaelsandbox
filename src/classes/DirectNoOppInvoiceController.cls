public with sharing class DirectNoOppInvoiceController {

    Public List<c2g__codaInvoiceLineItem__c> silis;
    Public c2g__codaInvoice__c si {get;set;}
    //Public Opportunity opp {get;set;}
    Public Date enddate {get;set;}
    Public String s {get;set;}
    Public Boolean hideprodstrue {get;set;}
    Public Boolean hideprodsfalse {get;set;}
    
    public DirectNoOppInvoiceController(ApexPages.StandardController controller) {
       si= [select Id, c2g__Opportunity__c, c2g__InvoiceDate__c, Invoice_Total__c from c2g__codaInvoice__c where Id = :ApexPages.currentPage().getParameters().get('id')];
      
       }
       
       
    
       
       Public List<c2g__codaInvoiceLineItem__c> getsilis(){
       
       silis = [select Id, c2g__Product__c, c2g__Quantity__c, c2g__UnitPrice__c, 
             c2g__Product__r.Description, c2g__Product__r.Name, Quantity_Text__c,
             Product_Line_Item_Description__c, c2g__NetValue__c, c2g__LineDescription__c from c2g__codaInvoiceLineItem__c where c2g__Invoice__c = :si.Id AND AR_Gross_Up_Item__c = false ORDER BY c2g__NetValue__c DESC];
             
       
      
       
       
       return silis;
    
    }

}