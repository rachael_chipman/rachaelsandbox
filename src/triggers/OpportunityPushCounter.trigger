trigger OpportunityPushCounter on Opportunity (before update) {
	
	Integer n = 1;
	
	try {
		
		for (opportunity oldo : trigger.old) {
			
			for (opportunity newo : trigger.new) {
				system.debug('comparison number: ' + n);
				
				if (oldo.id == newo.id && oldo.closedate < newo.closedate) { 
					
					system.debug('found a match. old opp is ' + oldo.id + ' with a close date of ' + oldo.closedate + ' and the new closedate is ' + newo.closedate); 
					system.debug('current pushcounter is ' + newo.push_counter__c);
					
					if (newo.push_counter__c == null) {
						newo.push_counter__c = 1; 
					} else { newo.push_counter__c++; }
					
					system.debug('the counter is now: ' + newo.push_counter__c);
					
				} else { system.debug('no match'); } 
				n++;
			}
			
		}
	
	} catch (Exception e) {}
		
}