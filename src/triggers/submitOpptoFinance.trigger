trigger submitOpptoFinance on Certification_Application__c (before update) {

    for(Certification_Application__c ca : trigger.new){
    
        if(ca.Application_Status__c == 'Under Review' && ca.Application_Result__c == 'In Progress' && 
        ca.License_Fee_Opportunity__c != null) {
        
        Opportunity SubmitOpp = [SELECT Id, Submit_From_Trigger__c, StageName, Awaiting_Approval__c FROM Opportunity WHERE Id = :ca.License_Fee_Opportunity__c];
        
        if(SubmitOpp.Awaiting_Approval__c == false && SubmitOpp.StageName == 'Signature'){
            SubmitOpp.Submit_From_Trigger__c = 'Yes';
            update SubmitOpp;
            }
        }
        }
}