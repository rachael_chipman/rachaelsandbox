public without sharing class ContactOwnerChange_QualifyingStatus {
    
    public static String RSE_ACCEPT = 'RSE Accept';
    public static String RSE_DQ = 'RSE DQ';
    public static String RSE_QUALIFIED = 'RSE Qualified';
    /*public static boolean IsLeadSourceOriginalChanged = false;
    public static boolean IsLeadSourceMostRecentChanged = false;
    public static boolean IsCampaignOriginalChanged = false;
    public static boolean IsCampaignMostRecentChanged = false;
    public static boolean IsQualifyingStatusChanged = false;*/
    
    
    public static list<Contact> filterContactsOnUpdate (list<Contact> newContacts, Map<Id, Contact> oldContacts)
    {
        list<Contact> filteredContacts = new list<Contact>();
        
        for(Contact aContact : newContacts)     
        {
            Contact oldContact;
            if(oldContacts != NULL)
            {
                oldContact = oldContacts.get(aContact.Id);
            }
            
            if((aContact.Qualifying_Status__c != oldContact.Qualifying_Status__c) /*||
              ((aContact.Campaign_Asset_Most_Recent__c != oldContact.Campaign_Asset_Most_Recent__c)  && aContact.Campaign_Asset_Most_Recent__c != NULL) ||
              ((aContact.Campaign_Source_Most_Recent__c != oldContact.Campaign_Source_Most_Recent__c) && aContact.Campaign_Source_Most_Recent__c != NULL)*/)
            {
                filteredContacts.add(aContact);
            }
        }
        return filteredContacts;
    }
    
    public static void processContacts (list<Contact> filteredContacts, Map<Id, Contact> oldContacts)
    {
        //list<Contact> contactsToUpdate = new list<Contact>();
        list<Qualifying_Status_History__c> historyRecords = new list<Qualifying_Status_History__c>();
        
        for(Contact aContact : filteredContacts)
        {
            //Contact updateContact = new Contact (Id = aContact.Id);
            Contact oldContact;
            if(oldContacts != NULL)
            {
                oldContact = oldContacts.get(aContact.Id);
            }
            
            if(aContact.Qualifying_Status__c == RSE_DQ && aContact.Original_Marketing_Router__c != NULL)
            {
                //updateContact.OwnerId = aContact.Original_Marketing_Router__c;
                aContact.OwnerId = aContact.Original_Marketing_Router__c;
            }
            else if(aContact.Qualifying_Status__c == RSE_ACCEPT && aContact.For_RSE__c != NULL)
            {
                //updateContact.OwnerId = aContact.For_RSE__c;
                aContact.OwnerId = aContact.For_RSE__c;
            }
            
            //Trying time stamp population differently - RC (Want to base on actual change of Source and Asset campaign Ids,
            // because I feel like we are missing the time stamps if there are two campaigns in a row of the same type, because then the source field wouldn't actually change)
            /*if(aContact.Campaign_Source_Original__c != oldContact.Campaign_Source_Original__c)
            {
                //updateContact.Lead_Source_Original_Time_Date_Stamp__c = system.now();
                //updateContact.Lead_Source_Most_Recent_TimeSTamp__c = system.now();
                aContact.Lead_Source_Original_Time_Date_Stamp__c = system.now();
                aContact.Lead_Source_Most_Recent_TimeSTamp__c = system.now();
            }
            else if((aContact.Campaign_Source_Most_Recent__c != oldContact.Campaign_Source_Most_Recent__c) && (aContact.Campaign_Source_Original__c == oldContact.Campaign_Source_Original__c))
            {
                //updateContact.Lead_Source_Most_Recent_TimeSTamp__c = system.now();
                aContact.Lead_Source_Most_Recent_TimeSTamp__c = system.now();
            }
            
            if(aContact.Campaign_Asset_Original__c != oldContact.Campaign_Asset_Original__c)
            {
                //updateContact.Lead_Asset_Original_Time_Date_Stamp__c = system.now();
                //updateContact.Lead_Asset_Most_Recent_Time_Date_Stamp__c = system.now();
                aContact.Lead_Asset_Original_Time_Date_Stamp__c = system.now();
                aContact.Lead_Asset_Most_Recent_Time_Date_Stamp__c = system.now();
            }
            else if((aContact.Campaign_Asset_Most_Recent__c != oldContact.Campaign_Asset_Most_Recent__c) && (aContact.Campaign_Asset_Original__c == oldContact.Campaign_Asset_Original__c))
            {
                //updateContact.Lead_Asset_Most_Recent_Time_Date_Stamp__c = system.now();
                aContact.Lead_Asset_Most_Recent_Time_Date_Stamp__c = system.now();
            }*/
            
            /*if(aContact.Most_Recent_Lead_Source__c!= oldContact.Most_Recent_Lead_Source__c)
                IsLeadSourceMostRecentChanged = true;
            if(!String.IsEmpty(aContact.LeadSource)  && ( oldContact.LeadSource == 'None' || String.IsEmpty(oldContact.LeadSource) || oldContact.LeadSource == null))
                IsLeadSourceOriginalChanged = true;
            if(!String.IsEmpty(aContact.First_Campaign__c) && String.IsEmpty(oldContact.First_Campaign__c))
               IsCampaignOriginalChanged  = true;
            if(aContact.Campaign_Name__c!= oldContact.Campaign_Name__c)
                IsCampaignMostRecentChanged = true;
            if(aContact.Qualifying_Status__c != oldContact.Qualifying_Status__c)
            {
                IsQualifyingStatusChanged = true;
            }
            
            System.debug('Value of the first lead source is : ' + oldContact.LeadSource);
            //populate/update the timestamp fields for LeadSource-Original and Lead Source Most Recent and Campaigns
            if(IsLeadSourceOriginalChanged)
                updateContact.Lead_Source_Original_Time_Date_Stamp__c = System.now();
            if(IsLeadSourceMostRecentChanged)
             updateContact.Lead_Source_Most_Recent_TimeSTamp__c = System.now(); 
			//if(IsCampaignOriginalChanged)
            //    updateContact.Lead_Asset_Original_Time_Date_Stamp__c = System.now();
            //if(IsCampaignMostRecentChanged)
            //    updateContact.Lead_Asset_Most_Recent_Time_Date_Stamp__c = System.now();
            //system.debug('The Lead Status is 2'+aLead.Status);*/
            
            system.debug('Who is the owner'+aContact.OwnerId);
            //contactsToUpdate.add(updateContact);
            
            Qualifying_Status_History__c historyRecord = new Qualifying_Status_History__c();
            historyRecord.Contact__c = aContact.Id;
            historyRecord.Campaign_Source_Original__c = aContact.Campaign_Source_Original__c;
            historyRecord.Campaign_Source_Most_Recent__c = aContact.Campaign_Source_Most_Recent__c;
            historyRecord.Campaign_Asset_Original__c = aContact.Campaign_Asset_Original__c;
            historyRecord.Campaign_Asset_Most_Recent__c = aContact.Campaign_Asset_Most_Recent__c;
            historyRecord.Lead_Source__c = aContact.Most_Recent_Lead_Source__c;
            historyRecord.Campaign_Name__c = aContact.Campaign_Name__c;
            historyRecord.Lead_Source_Original__c = aContact.LeadSource;
            historyRecord.Lead_Source_Original_Detail__c = aContact.Lead_Source_Original_Detail__c;
            historyRecord.Lead_Source_Most_Recent_Detail__c = aContact.Lead_Source_Most_Recent_Detail__c;
            historyRecord.Original_Campaign_Name__c = aContact.First_Campaign__c;
            if(aContact.Qualifying_Status__c != NULL)
            {
                historyRecord.Qualifying_Status__c = aContact.Qualifying_Status__c;
            }
            else
            {
                historyRecord.Qualifying_Status__c = 'Pre-Existing Contact';
            }
            historyRecord.Previous_Qualifying_Status__c = oldContact.Qualifying_Status__c;
            historyRecord.Unique_ID__c = aContact.Qualifying_Status__c + oldContact.Qualifying_Status__c + system.today() + aContact.Id;
            /*if(IsCampaignMostRecentChanged || IsQualifyingStatusChanged)
            {*/
                historyRecords.add(historyRecord);
            //}
             
        }
        
        try
        {
            //update contactsToUpdate;
            if(!CampaignFieldUpdate.updateContactMap.isEmpty())
            {
                update CampaignFieldUpdate.updateContactMap.values();
            }
        }
        catch (Exception e)
        {
            for(Integer i = 0; i < e.getNumDml(); i++)
            {
                system.debug('What was the error message?'+e.getDmlMessage(i));
                CampaignFieldUpdate.updateContactMap.values()[e.getDmlIndex(i)].addError(e.getDmlMessage(i));
            }
        }
        try
        {
            upsert historyRecords Unique_Id__c;
        }
        catch (DmlException e)
        {
            Map<Id, Contact> contactMap = new Map<Id, Contact>(filteredContacts);
            for(Integer i = 0; i < e.getNumDml(); i++)
            {
                system.debug('What is causing the error?' + e.getDmlMessage(i));
                Qualifying_Status_History__c errorHistory = historyRecords[e.getDmlIndex(i)];
                Contact errorContact = contactMap.get(errorHistory.Contact__c);
                errorContact.addError(e.getDmlMessage(i));
            } 
        }
    }
    
    public static void processTrigger(list<Contact> newContacts, Map<Id, Contact> oldContacts)
    {
        list<Contact> filterContacts = filterContactsOnUpdate(newContacts, oldContacts);
        if(!filterContacts.isEmpty())
        {
            processContacts(filterContacts, oldContacts);
        }
    }
}