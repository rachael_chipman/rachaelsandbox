public with sharing class JournalLineItemService {

	public static void populateJournalInvoiceLineItems(List<c2g__codaJournalLineItem__c> newJournalLineItems){

		Set<ID> journalIds = Pluck.ids('c2g__Journal__c',newJournalLineItems);
		Map<ID,c2g__codaJournal__c> opportunityJournals = new Map<Id,c2g__codaJournal__c>([Select Id , Opportunity__c From c2g__codaJournal__c  where Opportunity__c  != null and id in: journalIds]);
		if (opportunityJournals != null && !opportunityJournals.isEmpty() ){
			for (c2g__codaJournalLineItem__c newJournalLineItem : newJournalLineItems){
				if (opportunityJournals.get(newJournalLineItem.c2g__Journal__c)!= null){
					newJournalLineItem.Opportunity__c = opportunityJournals.get(newJournalLineItem.c2g__Journal__c).Opportunity__c;
				}
			}
		}
	}


}