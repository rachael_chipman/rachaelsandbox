public without sharing class OTMCommUpdateService {
	
	public static List<OpportunityTeamMember> filterOTMsForUpdate(List<Opportunity> newOpps, Map<ID, Opportunity> oldOppMap )
	{
		List<OpportunityTeamMember> filteredOtms = new List<OpportunityTeamMember>();
		Map<Id, List<OpportunityTeamMember>> oppIdtoTeamMembers = OppIdToTeamMembers(newOpps);
		system.debug('Whats in the Map'+ oppIdtoTeamMembers);
		for( Opportunity o : newOpps )
		{
			Opportunity OldOpp = OldOppMap.get(o.Id);
			if(OldOpp.OwnerId != o.OwnerId)
			{
				for(OpportunityTeamMember otm : oppIdtoTeamMembers.get(o.Id))
				{
					filteredOtms.add(otm);
				}
			}
			else
			{
				for(OpportunityTeamMember otm: oppIdtoTeamMembers.get(o.Id))
				{
					if(otm.Percent_Commissioned_On__c == null)
					{
						filteredOtms.add(otm);
					}
				}
			}
		}
		system.debug('What is in filteredOTMs?'+filteredOtms);
		return filteredOtms;
	}
	
	public static List<Opportunity> getOpportunityTeamMembers( List<Opportunity> filteredOpps )
	{
		return[SELECT Id, OwnerId, (SELECT Id, Percent_Commissioned_on__c from OpportunityTeamMembers where OpportunityId IN :filteredOpps)
		FROM Opportunity
		WHERE Id IN: filteredOpps];
	}
	
	private static Map<Id, List<OpportunityTeamMember>> OppIdToTeamMembers( List<Opportunity> filteredOpportunitiesWithOpportunityTeamMembers )
	{
		Map<Id, List<OpportunityTeamMember>> OpportunityIdtoTeamMembers = new Map<Id, List<OpportunityTeamMember>>();
		for( Opportunity o : filteredOpportunitiesWithOpportunityTeamMembers )
		{
			OpportunityIdtoTeamMembers.put( o.Id, new List<OpportunityTeamMember>() );
		
			for( OpportunityTeamMember otm : o.OpportunityTeamMembers )
			{
				OpportunityIdtoTeamMembers.get( o.Id ).add( otm );
			}
		}
		return OpportunityIdtoTeamMembers;
	}
	
	public static void processOTMs ( List<OpportunityTeamMember> filteredOtms, List<Opportunity> newOpps )
	{
		List<OpportunityTeamMember> UpdateOTMs = new List<OpportunityTeamMember>();
		
		for( OpportunityTeamMember oppTeamMember : filteredOtms )
		{
			oppTeamMember.Percent_Commissioned_On__c = 100;
			UpdateOTMs.add(oppTeamMember);
		}
		
		system.debug('What is in UpdateOTMs'+UpdateOTMs);
		update UpdateOTMs;
	}
	
	public void processTrigger( List<Opportunity> newOpps, Map<ID, Opportunity> oldOppMap )
	{
		List<Opportunity> oppsWithTeamMembers = getOpportunityTeamMembers (newOpps);
		List<OpportunityTeamMember> filteredOtms = filterOTMsForUpdate( oppsWithTeamMembers, oldOppMap);
		if(!filteredOtms.isEMpty())
		{
			processOTMs( filteredOtms, newOpps);	
		}
	}
	
}