trigger SalesCreditNote on c2g__codaCreditNote__c ( after insert,after update ) 
{
	
	if( trigger.isAfter && trigger.isUpdate )
	{
		List<c2g__codaCreditNote__c> postedCreditNotes = TransactionServices.filterPostedSObjects( trigger.new, trigger.oldMap, c2g__codaCreditNote__c.c2g__CreditNoteStatus__c, c2g__codaCreditNote__c.c2g__Opportunity__c );
		if( !postedCreditNotes.isEmpty() )
		{
			List<c2g__codaTransaction__c> relatedTransactions = TransactionServices.populateOpportunity( postedCreditNotes, c2g__codaCreditNote__c.c2g__Opportunity__c );
			if( !relatedTransactions.isEmpty() )
			{
				TransactionServices.processTransactionsAndLineItems( relatedTransactions, trigger.new );
			}
		}
	}

	
}