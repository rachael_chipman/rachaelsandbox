trigger LeadTrigger on Lead (before update, after update, before insert) {
    
    if(trigger.isBefore && trigger.isUpdate)
    {
        GenRefCRMOwnerChangeLead.processTrigger(trigger.new, trigger.oldMap);
	   //Check if original Lead OSurce and Lead Asset valued is changed, then ignore it.
       //(See CampaignFieldUpdate.cls this is not necessary as the 'Original' fields will only populate if they are null)
       //LeadOwnerChange_QualifyingStatus.CheckLead_Source_AssetOriginalValuesChanged(trigger.new, trigger.oldMap);

       system.debug('What is in trigger.new'+trigger.new);
       system.debug('What is the value of hasTriggerRan? ' + LeadOwnerChange_QualifyingStatus.hasTriggerRan);
       if(!LeadOwnerChange_QualifyingStatus.hasTriggerRan)
       {
           LeadOwnerChange_QualifyingStatus.processTrigger(trigger.new, trigger.oldMap);
           LeadOwnerChange_QualifyingStatus.hasTriggerRan = true;
       } 
    }
    
    if(trigger.isBefore && (trigger.isInsert || trigger.isUpdate))
    {
    	AssignPODToLead.ProcessPODAssignment(trigger.new);
    }
    
    /*if(trigger.isAfter && trigger.isUpdate)
    {
        system.debug('What is in trigger.new'+trigger.new);
        system.debug('What is the value of hasTriggerRan? ' + LeadOwnerChange_QualifyingStatus.hasTriggerRan);
        if(!LeadOwnerChange_QualifyingStatus.hasTriggerRan)
        {
            LeadOwnerChange_QualifyingStatus.processTrigger(trigger.new, trigger.oldMap);
            LeadOwnerChange_QualifyingStatus.hasTriggerRan = true;
        }
        
    }*/
      
}