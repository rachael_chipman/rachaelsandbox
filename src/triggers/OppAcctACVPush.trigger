trigger OppAcctACVPush on Opportunity (after update) {
    
   /* list<Opportunity> UpdatedOpportunities = new list<Opportunity>();
    list<Id> AcctIds = new list<Id>();
    list<Id> OppIds = new list<Id>();
    list<Opportunity> OppBatchUpdate = new list<Opportunity>();
    list<Account> AcctBatchUpdate = new list<Account>();
    list<ACV__c> ACVTableInsert = new list<ACV__c>();
    list<Opportunity> RenewalOppInsert = new list<Opportunity>();
    list<OpportunityLineItem> RenewalLineItemInsert = new list<OpportunityLineItem>();
    list<Partner> RenewalPartnerInsert = new list<Partner>();
    list<Opportunity> CloseDateRenewals = new list<Opportunity>();
    list<Opportunity> RenewalCreatedOpps = new list<Opportunity>();
    list<Opportunity> RenewalCreated1 = new list<Opportunity>();
    Datetime myDate = datetime.now();
    Integer currentYear = myDate.year();
    
    // Narrow to opportunities in Finance Approved with Run ACV != No
    for(Opportunity opploop1 : trigger.new){
        if(((opploop1.StageName == 'Finance Approved' && opploop1.Cloned_From_ID__c == null && (opploop1.Send_Monkey_Email__c == 'Monkey Sent' || opploop1.Send_Monkey_Email__c == 'No Monkey Needed')) || opploop1.StageName == 'Terminated Contract') && opploop1.Run_ACV__c != 'No') 
            {
            UpdatedOpportunities.add(opploop1);
            AcctIds.add(opploop1.accountid);
            OppIds.add(opploop1.id);
        }
    }
    
    if(UpdatedOpportunities.size() > 0){
    
        // Get all accounts associated with Opportunities
        map<Id,Account> AccountsInOpps = new map<Id,Account>([select id, Name, Ownerid, currencyisocode, Accred_Non_Recurring_Direct__c, Accred_Non_Recurring_Indirect__c, 
            Accred_Recurring_Direct__c, Accred_Recurring_Indirect__c, Gold_Recurring_Direct__c, Internal_Use_Recurring_Indirect__c, Platinum_Recurring_Direct__c, 
            ProServ_Non_Recurring_Direct__c, ProServ_Recurring_Direct__c, ProServ_Recurring_Indirect__c, ProServ_Non_Recurring_Indirect__c, 
            SSC_Referral_Child_Recurring_Indirect__c,  SSC_SME_Recurring_Indirect__c,  SSRA_Recurring_Indirect__c,  Tools_Child_Recurring_Indirect__c, 
            Tools_Child_Non_Recurring_Indirect__c, Tools_Reseller_API_Recurring_Indirect__c, ACV_Domain_Assurance_Direct_Recurring__c, ACV_Managed_Deliverability__c, 
            ACV_Pixel_Tracking_Recurring_Direct__c, ACV_Regional_ISP_Med_Non_Recurring_Indi__c, ACV_CorpDev_Research_Non_Recurring_Indi__c, 
            Tools_Only_Recurring_Direct__c, ACV_Inbox_Measurement_Recurring_Direct__c, Additional_AM_Services__c,Tools_Overage_Fees__c,Renewal_Date__c, Monthly_MM_Events__c, MM_Overage_Fee__c, 
            Monthly_CP_Events__c, CP_Overage_Fee__c, Reputation_Monitor_IPs__c, Blacklist_Alert_IPs__c, Renewed_to_Date__c, Domain_Assurance_Renewal_Date__c, 
            ProServ_End_date__c, Tools_Start_Date__c, Certification_Start_Date__c, DA_Start_Date__c, das_active_clients__c, 
            bonded_sender_customer__c,domain_assurance__c,ACV_Email_Brand_Monitor_Direct__c,ACV_Email_Brand_Monitor_Indirect__c,ACV_Domain_Protect_Direct__c,
      ACV_Domain_Protect_Indirect__c,ACV_Domain_Secure_Direct__c,ACV_Domain_Secure_Indirect__c from Account where id in :AcctIds]);
        
        // Get all Current Products associated with Opportunities
        list<OpportunityLineItem> ProductsInOpps = new list<OpportunityLineItem>([select product_status__c, id, quantity, unitprice, description__c, 
            Opportunityid, PricebookEntryId, product_code__c, Previous_Value__c, active__c, indirect__c, recurring__c,bundle__c, Opportunity_Type__c, 
            start_date__c, end_date__c, of_MM_Events_Allowed__c, of_CP_Events_Allowed__c, of_Rep_Mon_IPs__c, CP_Overage_Fee__c, MM_Overage_Fee__c, of_BLA_IPS__c 
            from opportunitylineitem where opportunityid in :OppIds and (Product_Status__c = 'Current' or Product_Status__c = 'Terminated')]);
            
        // Get active currencies with conversion rates
        list<DatedConversionRate> Currencies = new list<DatedConversionRate>([select IsoCode, ConversionRate from DatedConversionRate where NextStartDate = 9999-12-31]);
        
        // Get all Partner Records
        list<Partner> PartnerList = new list<Partner>([select AccounttoId, AccountfromId, OpportunityId, IsPrimary, ReversePartnerId, Role from Partner 
            where OpportunityId in :OppIds]);
        
        //Calculate individual Opp ACV
        for(Opportunity opploop2 : UpdatedOpportunities){
        
            Double Gold = 0;
            Double Platinum = 0;
            Double ToolsOnly = 0;
            Double CertDirectRecurring = 0;
            Double CertIndirectRecurring = 0;
            Double CertDirectNonRecurring = 0;
            Double CertIndirectNonRecurring = 0;
            Double InternalUse = 0;
            Double ToolsChild = 0;
            Double SSRA = 0;
            Double CertReferralChild = 0;
            Double SSCSME = 0;
            Double ToolsResellerAPI = 0;
            Double ProServRecurringDirect = 0;
            Double ProServRecurringIndirect = 0;
            Double ProServNonRecurringDirect = 0;
            Double ProServNonRecurringIndirect = 0;
      Double EmailBrandMonitor = 0;
      Double EmailBrandMonitorChild = 0;
      Double DomainAssurance = 0;
      Double DomainProtect = 0;
      Double DomainProtectChild = 0;
      Double DomainSecure = 0;
      Double DomainSecureChild = 0;
            Double InboxMeasurement = 0;
            Double PixelTracking = 0;
            Double CorpDevResearch = 0;
            Double RegionalISPMediation = 0;
            Double AdditionalAMServices = 0;
            Double ToolsOVerageFees = 0;
            Date ToolsRenewalDate = date.newinstance(1900,1,1);
            Date CertRenewalDate = date.newinstance(1900,1,1);
            Date DARenewalDate = date.newinstance(1900,1,1);
            Date ProServEndDate = date.newinstance(1900,1,1);
            Date ToolsStartDate = date.newinstance(1900,1,1);
            Date CertStartDate = date.newinstance(1900,1,1);
            Date DAStartDate = date.newinstance(1900,1,1); 
            Date ReportingDate = date.newinstance(1900,1,1);
            Double MMEvents = 0;
            Double CPEvents = 0;
            Double MMOverageFee = 0;
            Double CPOverageFee = 0;
            Double RepMonIPs = 0;
            Double BlacklistIPs = 0;
            Integer ToolsActive = 0;
            Integer CertActive = 0;
            String DAActive;
            
            // Get relevant Account
            Account loopAcct = AccountsInOpps.get(opploop2.accountid);
            Double ContractValue = 0;
            Double PreviousValue = 0;
            
            //Get Account conversion rate
            Double AcctConversionRate = 0;
            for(DatedConversionRate currency1 : Currencies){
                if(loopAcct.CurrencyISOCode == currency1.IsoCode){
                    AcctConversionRate = currency1.ConversionRate;
                    break;
                }
            }
            //Get Opportunity conversion rate
            Double OppConversionRate = 0;
            for(DatedConversionRate currency2 : Currencies){
                if(opploop2.CurrencyISOCode == currency2.IsoCode){
                    OppConversionRate = currency2.ConversionRate;
                    break;
                }
            }
            
            // Instantiate active checkbox variabes
            if(loopAcct.DAS_Active_Clients__c == true) ToolsActive = 1;
            if(loopAcct.Bonded_Sender_Customer__c == true) CertActive = 1;
            
            // Collect relevant line items in oppProducts
            list<OpportunityLineItem> oppProducts = new list<OpportunityLineItem>();
            for(OpportunityLineItem prodloop1 : ProductsInOpps){
                if(prodloop1.opportunityid == opploop2.id) oppProducts.add(prodloop1);
            }
            
            // Instantiate Account ACV variables, pull across former values if it's not being overwritten
            if(((opploop2.type == 'New Business' || opploop2.type == 'Cross-Sell' || opploop2.type == 'Renewal') && 
                     opploop2.Product_Gold__c) || loopAcct.Gold_Recurring_Direct__c == null) Gold = 0;
            else Gold = loopAcct.Gold_Recurring_Direct__c;
            if(((opploop2.type == 'New Business' || opploop2.type == 'Cross-Sell' || opploop2.type == 'Renewal') && 
                     opploop2.Product_Platinum__c) || loopAcct.Platinum_Recurring_Direct__c == null) Platinum = 0;
            else Platinum = loopAcct.Platinum_Recurring_Direct__c;
            if(((opploop2.type == 'New Business' || opploop2.type == 'Cross-Sell' || opploop2.type == 'Renewal') && 
                     opploop2.Product_Tools_Only__c) || loopAcct.Tools_Only_Recurring_Direct__c == null) ToolsOnly = 0;
            else ToolsOnly = loopAcct.Tools_Only_Recurring_Direct__c;
            if(((opploop2.type == 'New Business' || opploop2.type == 'Cross-Sell' || opploop2.type == 'Renewal') && 
                     opploop2.Product_Certification__c)|| loopAcct.Accred_Recurring_Direct__c == null) CertDirectRecurring = 0;
            else CertDirectRecurring = loopAcct.Accred_Recurring_Direct__c;
            if(loopAcct.accred_non_recurring_direct__c == null || loopAcct.accred_non_recurring_direct__c == 0) CertDirectNonRecurring = 0;
            else CertDirectNonRecurring = loopAcct.accred_non_recurring_direct__c;
            if(loopAcct.accred_non_recurring_indirect__c == null || loopAcct.accred_non_recurring_indirect__c == 0) CertIndirectNonRecurring = 0;
            else CertIndirectNonRecurring = loopAcct.accred_non_recurring_indirect__c;
            if(((opploop2.type == 'New Business' || opploop2.type == 'Cross-Sell' || opploop2.type == 'Renewal') && 
                     opploop2.Product_Internal_Use__c)|| loopAcct.Internal_Use_Recurring_Indirect__c== null) InternalUse = 0;
            else InternalUse = loopAcct.Internal_Use_Recurring_Indirect__c;
            if(((opploop2.type == 'New Business' || opploop2.type == 'Cross-Sell' || opploop2.type == 'Renewal') && 
                     opploop2.Product_Tools_Child__c)|| loopAcct.Tools_Child_Recurring_Indirect__c == null) ToolsChild = 0;
            else ToolsChild = loopAcct.Tools_Child_Recurring_Indirect__c;
            if(((opploop2.type == 'New Business' || opploop2.type == 'Cross-Sell' || opploop2.type == 'Renewal') && 
                     opploop2.Product_SSRA__c)|| loopAcct.SSRA_Recurring_Indirect__c == null) SSRA = 0;
            else SSRA = loopAcct.SSRA_Recurring_Indirect__c;
            if(((opploop2.type == 'New Business' || opploop2.type == 'Cross-Sell' || opploop2.type == 'Renewal') && 
                     opploop2.Product_SSC_Referral_Child__c)|| loopAcct.SSC_Referral_Child_Recurring_Indirect__c == null) CertReferralChild = 0;
            else CertReferralChild = loopAcct.SSC_Referral_Child_Recurring_Indirect__c;
            if(((opploop2.type == 'New Business' || opploop2.type == 'Cross-Sell' || opploop2.type == 'Renewal') && 
                     opploop2.Product_SSC_SME__c) || loopAcct.SSC_SME_Recurring_Indirect__c == null) SSCSME = 0;
            else SSCSME = loopAcct.SSC_SME_Recurring_Indirect__c;
            if(((opploop2.type == 'New Business' || opploop2.type == 'Cross-Sell' || opploop2.type == 'Renewal') && 
                     opploop2.Product_Tools_Reseller_API__c)|| loopAcct.Tools_Reseller_API_Recurring_Indirect__c == null) ToolsResellerAPI = 0;
            else ToolsResellerAPI = loopAcct.Tools_Reseller_API_Recurring_Indirect__c;
            if(((opploop2.type == 'New Business' || opploop2.type == 'Cross-Sell' || opploop2.type == 'Renewal') && 
                     (opploop2.Product_Deliverability_Consulting__c || opploop2.Product_ROI_and_Response_Consulting__c || 
                     opploop2.Product_Technology_Services__c) && opploop2.ssc_channel_child__c == 0) || loopAcct.ProServ_Recurring_Direct__c == null) ProServRecurringDirect = 0;
            else ProServRecurringDirect = loopAcct.ProServ_Recurring_Direct__c;
            if(((opploop2.type == 'New Business' || opploop2.type == 'Cross-Sell' || opploop2.type == 'Renewal') && 
                     (opploop2.Product_Deliverability_Consulting__c || opploop2.Product_ROI_and_Response_Consulting__c || 
                     opploop2.Product_Technology_Services__c) && opploop2.ssc_channel_child__c == 1)|| loopAcct.ProServ_Recurring_Indirect__c == null) ProServRecurringIndirect = 0;
            else ProServRecurringIndirect = loopAcct.ProServ_Recurring_Indirect__c;
            if(loopAcct.ProServ_Non_Recurring_Direct__c == null || loopAcct.ProServ_Non_Recurring_Direct__c == 0) ProServNonRecurringDirect = 0;
            else ProServNonRecurringDirect = loopAcct.ProServ_Non_Recurring_Direct__c;
            if(loopAcct.ProServ_Non_Recurring_Indirect__c == null || loopAcct.ProServ_Non_Recurring_Indirect__c == 0) ProServNonRecurringIndirect = 0;
            else ProServNonRecurringIndirect = loopAcct.ProServ_Non_Recurring_Indirect__c;
      
      /**/
            
      /*if(((opploop2.type == 'New Business' || opploop2.type == 'Cross-Sell' || opploop2.type == 'Renewal') && 
                     opploop2.Product_Domain_Assurance__c) ||loopAcct.ACV_Domain_Assurance_Direct_Recurring__c  == null) DomainAssurance = 0;
            else DomainAssurance = loopAcct.ACV_Domain_Assurance_Direct_Recurring__c;
      
      if(((opploop2.type == 'New Business' || opploop2.type == 'Cross-Sell' || opploop2.type == 'Renewal') && 
        opploop2.Product_Email_Brand_Monitor__c) || loopAcct.ACV_Email_Brand_Monitor_Direct__c  == null) EmailBrandMonitor = 0;
      else EmailBrandMonitor = loopAcct.ACV_Email_Brand_Monitor_Direct__c;
      
      if(((opploop2.type == 'New Business' || opploop2.type == 'Cross-Sell' || opploop2.type == 'Renewal') && 
        opploop2.Product_Email_Brand_Monitor_Child__c) || loopAcct.ACV_Email_Brand_Monitor_Indirect__c  == null) EmailBrandMonitorChild = 0;
      else EmailBrandMonitorChild = loopAcct.ACV_Email_Brand_Monitor_Indirect__c;
      
      if(((opploop2.type == 'New Business' || opploop2.type == 'Cross-Sell' || opploop2.type == 'Renewal') && 
        opploop2.Product_Domain_Protect__c) || loopAcct.ACV_Domain_Protect_Direct__c  == null) DomainProtect = 0;
      else DomainProtect = loopAcct.ACV_Domain_Protect_Direct__c;
      
      if(((opploop2.type == 'New Business' || opploop2.type == 'Cross-Sell' || opploop2.type == 'Renewal') && 
        opploop2.Product_Domain_Protect_Child__c) || loopAcct.ACV_Domain_Protect_Indirect__c  == null) DomainProtectChild = 0;
      else DomainProtectChild = loopAcct.ACV_Domain_Protect_Indirect__c;
      
      if(((opploop2.type == 'New Business' || opploop2.type == 'Cross-Sell' || opploop2.type == 'Renewal') && 
        opploop2.Product_Domain_Secure__c) || loopAcct.ACV_Domain_Secure_Direct__c  == null) DomainSecure = 0;
      else DomainSecure = loopAcct.ACV_Domain_Secure_Direct__c;
      
      if(((opploop2.type == 'New Business' || opploop2.type == 'Cross-Sell' || opploop2.type == 'Renewal') && 
        opploop2.Product_Domain_Secure_Child__c) || loopAcct.ACV_Domain_Secure_Indirect__c  == null) DomainSecureChild = 0;
      else DomainSecureChild = loopAcct.ACV_Domain_Secure_Indirect__c;
      
      /**/
      
            /*if(((opploop2.type == 'New Business' || opploop2.type == 'Cross-Sell' || opploop2.type == 'Renewal') && 
                     opploop2.Product_Inbox_Measurement__c) || loopAcct.ACV_Inbox_Measurement_Recurring_Direct__c == null) InboxMeasurement = 0;
            else InboxMeasurement = loopAcct.ACV_Inbox_Measurement_Recurring_Direct__c;
            if(((opploop2.type == 'New Business' || opploop2.type == 'Cross-Sell' || opploop2.type == 'Renewal') && 
                     opploop2.Product_Pixel_Tracking__c)|| loopAcct.ACV_Pixel_Tracking_Recurring_Direct__c == null) PixelTracking = 0;
            else PixelTracking = loopAcct.ACV_Pixel_Tracking_Recurring_Direct__c;
            if(loopAcct.ACV_CorpDev_Research_Non_Recurring_Indi__c == null || loopAcct.ACV_CorpDev_Research_Non_Recurring_Indi__c == 0) CorpDevResearch = 0;
            else CorpDevResearch = loopAcct.ACV_CorpDev_Research_Non_Recurring_Indi__c;
            if(loopAcct.ACV_Regional_ISP_Med_Non_Recurring_Indi__c == null || loopAcct.ACV_Regional_ISP_Med_Non_Recurring_Indi__c == 0) RegionalISPMediation = 0;
            else RegionalISPMediation = loopAcct.ACV_Regional_ISP_Med_Non_Recurring_Indi__c;
            if(loopAcct.Tools_Overage_Fees__c == null || loopAcct.Tools_Overage_Fees__c == 0) ToolsOverageFees = 0;
            else ToolsOverageFees = loopAcct.Tools_Overage_Fees__c;
            if(loopAcct.Additional_AM_Services__c == null || loopAcct.Additional_AM_Services__c == 0) AdditionalAMServices = 0;
            else AdditionalAMServices = loopAcct.Additional_AM_Services__c;
                 
            // Instantiate renewal date variables and events variables
            if(loopAcct.Renewal_Date__c == null) ToolsRenewalDate = date.newinstance(1900,1,1);
            else ToolsRenewalDate = loopAcct.Renewal_Date__c;
            if(loopAcct.Renewed_to_Date__c == null) CertRenewalDate = date.newinstance(1900,1,1);
            else CertRenewalDate = loopAcct.Renewed_to_Date__c;
            if(loopAcct.Domain_Assurance_Renewal_Date__c == null) DARenewalDate = date.newinstance(1900,1,1);
            else DARenewalDate = loopAcct.Domain_Assurance_Renewal_Date__c;
            if(loopAcct.ProServ_End_Date__c == null) ProServEndDate = date.newinstance(1900,1,1);
            else ProServEndDate = loopAcct.ProServ_End_Date__c;
            if(loopAcct.Tools_Start_Date__c == null) ToolsStartDate = date.newinstance(1900,1,1);
            else ToolsStartDate = loopAcct.Tools_Start_Date__c;
            if(loopAcct.Certification_Start_Date__c == null) CertStartDate = date.newinstance(1900,1,1);
            else CertStartDate = loopAcct.Certification_Start_Date__c;
            if(loopAcct.DA_Start_Date__c == null) DAStartDate = date.newinstance(1900,1,1);
            else DAStartDate = loopAcct.DA_Start_Date__c;
            if(((opploop2.type == 'New Business' || opploop2.type == 'Cross-Sell' || opploop2.type == 'Renewal') 
                     && (opploop2.Product_Gold__c || opploop2.Product_Platinum__c || opploop2.Product_Tools_Only__c 
                     || opploop2.Product_Internal_Use__c || opploop2.Product_Tools_Child__c)) || 
                     loopAcct.Monthly_MM_Events__c == null) MMEvents = 0;
            else MMEvents = loopAcct.Monthly_MM_Events__c;
            if(((opploop2.type == 'New Business' || opploop2.type == 'Cross-Sell' || opploop2.type == 'Renewal') 
                     && (opploop2.Product_Gold__c || opploop2.Product_Platinum__c || opploop2.Product_Tools_Only__c 
                     || opploop2.Product_Internal_Use__c || opploop2.Product_Tools_Child__c)) || 
                     loopAcct.Monthly_CP_Events__c == null) CPEvents = 0;
            else CPEvents = loopAcct.Monthly_CP_Events__c;
            if(((opploop2.type == 'New Business' || opploop2.type == 'Cross-Sell' || opploop2.type == 'Renewal')
                     && (opploop2.Product_Gold__c || opploop2.Product_Platinum__c || opploop2.Product_Tools_Only__c 
                     || opploop2.Product_Internal_Use__c || opploop2.Product_Tools_Child__c)) || 
                     loopAcct.MM_Overage_Fee__c == null) MMOverageFee = 0;
            else MMOverageFee = loopAcct.MM_Overage_Fee__c;
            if(((opploop2.type == 'New Business' || opploop2.type == 'Cross-Sell' || opploop2.type == 'Renewal')
                     && (opploop2.Product_Gold__c || opploop2.Product_Platinum__c || opploop2.Product_Tools_Only__c 
                     || opploop2.Product_Internal_Use__c || opploop2.Product_Tools_Child__c)) || 
                     loopAcct.CP_Overage_Fee__c == null) CPOverageFee = 0;
            else CPOverageFee = loopAcct.CP_Overage_Fee__c;
            if(((opploop2.type == 'New Business' || opploop2.type == 'Cross-Sell' || opploop2.type == 'Renewal')
                     && (opploop2.Product_Gold__c || opploop2.Product_Platinum__c || opploop2.Product_Tools_Only__c 
                     || opploop2.Product_Internal_Use__c || opploop2.Product_Tools_Child__c)) || 
                     loopAcct.Blacklist_Alert_IPs__c == null) BlacklistIPs = 0;
            else BlacklistIPs = loopAcct.Blacklist_Alert_IPs__c;
            if(((opploop2.type == 'New Business' || opploop2.type == 'Cross-Sell' || opploop2.type == 'Renewal')
                     && (opploop2.Product_Gold__c || opploop2.Product_Platinum__c || opploop2.Product_Tools_Only__c 
                     || opploop2.Product_Internal_Use__c || opploop2.Product_Tools_Child__c)) || 
                     loopAcct.Reputation_Monitor_IPs__c == null) RepMonIPs = 0;
            else RepMonIPs = loopAcct.Reputation_Monitor_IPs__c;
                 
            // ACV Mapping
            for(OpportunityLineItem prodloop2 : oppProducts){
                     
                // OCV Calculate
                ContractValue = ContractValue + (prodloop2.Quantity * prodloop2.UnitPrice);
                PreviousValue = PreviousValue + (prodloop2.Quantity * prodloop2.Previous_Value__c); 
                ReportingDate = opploop2.Reporting_Date__c;
                     
                //Currency Conversion
                // if(opploop2.CurrencyISOCode <> loopAcct.CurrencyISOCode){
                //    prodloop2.UnitPrice = (prodloop2.UnitPrice * AcctConversionRate) / OppConversionRate;
                //}
                     
                //Gold
                  if(prodloop2.Product_Code__c == '001-DMSU-GOLD' && prodloop2.Recurring__c){
                    if(prodloop2.product_status__c == 'Current'){
                        if(opploop2.type == 'Extension') {
                            Gold = Gold + (prodloop2.Quantity * (prodloop2.UnitPrice - prodloop2.Previous_Value__c));
                            ToolsRenewalDate = prodloop2.End_Date__c;
                        }
                        else{
                            Gold = Gold + (prodloop2.Quantity * prodloop2.UnitPrice);
                            MMEvents = MMEvents + prodloop2.of_MM_Events_Allowed__c;
                            CPEvents = CPEvents + prodloop2.of_CP_Events_Allowed__c;
                            MMOverageFee = MMOverageFee + prodloop2.MM_Overage_Fee__c;
                            CPOverageFee = CPOverageFee + prodloop2.CP_Overage_Fee__c;
                            RepMonIPs = RepMonIPs + prodloop2.of_Rep_Mon_IPs__c;
                            BlacklistIPs = BlacklistIPs + prodloop2.of_BLA_IPs__c;
                            if(prodloop2.End_Date__c > ToolsRenewalDate && 
                                (opploop2.type == 'New Business' || opploop2.type == 'Cross-Sell' 
                            || opploop2.type == 'Renewal' || opploop2.type == 'Extension')){
                                ToolsRenewalDate = prodloop2.End_Date__c;
                                ToolsStartDate =  prodloop2.Start_Date__c;
                            }
                        }    
                        ToolsActive = ToolsActive + 1;
                    }
                    else if(prodloop2.product_status__c == 'Terminated') ToolsActive = ToolsActive - 1;
                 }
                 //Gold Overage
                 else if(prodloop2.Product_Code__c == '001-DMSU-GOLD' && prodloop2.Recurring__c == false){
                         ToolsOVerageFees = ToolsOVerageFees + (prodloop2.Quantity * prodloop2.UnitPrice);
                 }
                 //Platinum
                 else if(prodloop2.Product_Code__c == '002-DMSU-PLAT' && prodloop2.Recurring__c){
                    if(prodloop2.product_status__c == 'Current'){
                        if(opploop2.type == 'Extension') {
                            Platinum = Platinum + (prodloop2.Quantity * (prodloop2.UnitPrice - prodloop2.Previous_Value__c));
                            ToolsRenewalDate = prodloop2.End_Date__c;
                        }
                        else{
                            Platinum = Platinum + (prodloop2.Quantity * prodloop2.UnitPrice);
                            MMEvents = MMEvents + prodloop2.of_MM_Events_Allowed__c;
                            CPEvents = CPEvents + prodloop2.of_CP_Events_Allowed__c;
                            MMOverageFee = MMOverageFee + prodloop2.MM_Overage_Fee__c;
                            CPOverageFee = CPOverageFee + prodloop2.CP_Overage_Fee__c;
                            RepMonIPs = RepMonIPs + prodloop2.of_Rep_Mon_IPs__c;
                            BlacklistIPs = BlacklistIPs + prodloop2.of_BLA_IPs__c;
                            if(prodloop2.End_Date__c > ToolsRenewalDate && 
                                (opploop2.type == 'New Business' || opploop2.type == 'Cross-Sell' 
                            || opploop2.type == 'Renewal' || opploop2.type == 'Extension')){
                                ToolsRenewalDate = prodloop2.End_Date__c;
                                ToolsStartDate =  prodloop2.Start_Date__c;
                            }
                        }
                        ToolsActive = ToolsActive + 1;
                    }
                    else if(prodloop2.product_status__c == 'Terminated') ToolsActive = ToolsActive - 1;
                 }
                 //Platinum Overage
                 else if(prodloop2.Product_Code__c == '002-DMSU-PLAT' && prodloop2.Recurring__c == false){
                         ToolsOVerageFees = ToolsOVerageFees + (prodloop2.Quantity * prodloop2.UnitPrice);
                 }
                 //Additional AM Services
                 else if(prodloop2.Product_Code__c == '028-DMSU-SERV'){
                         AdditionalAMServices = AdditionalAMServices + (prodloop2.Quantity * prodloop2.UnitPrice);
                 }
                 //Tools Only
                 else if(prodloop2.Product_Code__c == '025-DMSU-TOOL'){
                    if(prodloop2.product_status__c == 'Current'){
                        if(opploop2.type == 'Extension') {
                            ToolsOnly = ToolsOnly + (prodloop2.Quantity * (prodloop2.UnitPrice - prodloop2.Previous_Value__c));
                            ToolsRenewalDate = prodloop2.End_Date__c;
                        }
                        else{
                            ToolsOnly = ToolsOnly + (prodloop2.Quantity * prodloop2.UnitPrice);
                            MMEvents = MMEvents + prodloop2.of_MM_Events_Allowed__c;
                            CPEvents = CPEvents + prodloop2.of_CP_Events_Allowed__c;
                            MMOverageFee = MMOverageFee + prodloop2.MM_Overage_Fee__c;
                            CPOverageFee = CPOverageFee + prodloop2.CP_Overage_Fee__c;
                            RepMonIPs = RepMonIPs + prodloop2.of_Rep_Mon_IPs__c;
                            BlacklistIPs = BlacklistIPs + prodloop2.of_BLA_IPs__c;
                            if(prodloop2.End_Date__c > ToolsRenewalDate && 
                                (opploop2.type == 'New Business' || opploop2.type == 'Cross-Sell' 
                            || opploop2.type == 'Renewal' || opploop2.type == 'Extension')){
                                ToolsRenewalDate = prodloop2.End_Date__c;
                                ToolsStartDate =  prodloop2.Start_Date__c;
                            }
                        }
                        ToolsActive = ToolsActive + 1;
                    }
                    else if(prodloop2.product_status__c == 'Terminated') ToolsActive = ToolsActive - 1;
                 }
                 //Pixel Tracking
                 else if(prodloop2.Product_Code__c == '020-DMSU-PIXE'){
                         PixelTracking = PixelTracking + (prodloop2.Quantity * prodloop2.UnitPrice);
                 }
                 //Cert Recurring Direct
                else if(prodloop2.Product_Code__c ==  '004-CERT-FEE0' || 
                (prodloop2.Product_Code__c == '019-CERT-SAFE' && prodloop2.Indirect__c == false)){
                    if(prodloop2.Product_Status__c == 'Current'){
                        if(opploop2.type == 'Extension') {
                            CertDirectRecurring = CertDirectRecurring + (prodloop2.Quantity * (prodloop2.UnitPrice - prodloop2.Previous_Value__c));
                            CertRenewalDate = prodloop2.End_Date__c;
                        }
                        else{
                            CertDirectRecurring = CertDirectRecurring + (prodloop2.Quantity * prodloop2.UnitPrice);
                            if(opploop2.type == 'Renewal' || opploop2.type == 'Extension') CertRenewalDate = prodloop2.End_Date__c;
                        }
                    }
                    else if(prodloop2.Product_Status__c == 'Terminated') CertActive = 0;
                 }
                 //Cert Recurring Indirect
                 else if(prodloop2.Product_Code__c == '014-CERT-RESC' || 
                         (prodloop2.Product_Code__c == '019-CERT-SAFE' && prodloop2.Indirect__c)){
                    if(prodloop2.Product_Status__c == 'Current'){
                        if(opploop2.type == 'Extension') {
                            CertIndirectRecurring = CertIndirectRecurring + (prodloop2.Quantity * (prodloop2.UnitPrice - prodloop2.Previous_Value__c));
                            CertRenewalDate = prodloop2.End_Date__c;
                        }
                        else{
                            CertIndirectRecurring = CertIndirectRecurring + (prodloop2.Quantity * prodloop2.UnitPrice);
                            if(opploop2.type == 'Renewal' || opploop2.type == 'Extension') CertRenewalDate = prodloop2.End_Date__c;
                        }
                    }
                    else if(prodloop2.Product_Status__c == 'Terminated') CertActive = 0;
                 }
                 //Cert Non-Recurring Indirect
                 else if(prodloop2.Product_Code__c == '003-CERT-APP0' && prodloop2.Indirect__c){
                         CertIndirectNonRecurring = CertIndirectNonRecurring + (prodloop2.Quantity * prodloop2.UnitPrice);
                        if(opploop2.type == 'Renewal' || opploop2.type == 'Extension') CertRenewalDate = prodloop2.End_Date__c;
                 }
                 //Cert Non-Recurring Direct
                 else if(prodloop2.Product_Code__c == '003-CERT-APP0'){
                         CertDirectNonRecurring = CertDirectNonRecurring + (prodloop2.Quantity * prodloop2.UnitPrice);
                         if(opploop2.type == 'Renewal' || opploop2.type == 'Extension') CertRenewalDate = prodloop2.End_Date__c;
                     }
                     //Cert Referral Child Recurring
                 else if(prodloop2.Product_Code__c == '005-CERT-REFC'){
                    if(prodloop2.Product_Status__c == 'Current'){
                        if(opploop2.type == 'Extension') {
                            CertReferralChild = CertReferralChild + (prodloop2.Quantity * (prodloop2.UnitPrice - prodloop2.Previous_Value__c));
                            CertRenewalDate = prodloop2.End_Date__c;
                        }
                        else{
                            CertReferralChild = CertReferralChild + (prodloop2.Quantity * prodloop2.UnitPrice);
                            if(opploop2.type == 'Renewal' || opploop2.type == 'Extension') CertRenewalDate = prodloop2.End_Date__c;
                        }
                    }
                    else if(prodloop2.Product_Status__c == 'Terminated') CertActive = 0;
                 }
                 //Internal Use
                 else if(prodloop2.Product_Code__c == '012-ESPR-INTU'){
                    if(prodloop2.product_status__c == 'Current'){
                        if(opploop2.type == 'Extension') {
                            InternalUse = InternalUse + (prodloop2.Quantity * (prodloop2.UnitPrice - prodloop2.Previous_Value__c));
                            ToolsRenewalDate = prodloop2.End_Date__c;
                        }
                        else{
                            InternalUse = InternalUse + (prodloop2.Quantity * prodloop2.UnitPrice);
                            MMEvents = MMEvents + prodloop2.of_MM_Events_Allowed__c;
                            CPEvents = CPEvents + prodloop2.of_CP_Events_Allowed__c;
                            MMOverageFee = MMOverageFee + prodloop2.MM_Overage_Fee__c;
                            CPOverageFee = CPOverageFee + prodloop2.CP_Overage_Fee__c;
                            RepMonIPs = RepMonIPs + prodloop2.of_Rep_Mon_IPs__c;
                            BlacklistIPs = BlacklistIPs + prodloop2.of_BLA_IPs__c;
                            if(prodloop2.End_Date__c > ToolsRenewalDate && 
                                (opploop2.type == 'New Business' || opploop2.type == 'Cross-Sell' 
                            || opploop2.type == 'Renewal'|| opploop2.type == 'Extension')){
                                ToolsRenewalDate = prodloop2.End_Date__c;
                                ToolsStartDate =  prodloop2.Start_Date__c;
                            }
                        }
                        ToolsActive = ToolsActive + 1;
                    }
                    else if(prodloop2.product_status__c == 'Terminated') ToolsActive = ToolsActive - 1;
                 }
                 //Tools Child
                 else if(prodloop2.Product_Code__c == '015-ESPR-REFC'){
                    if(prodloop2.product_status__c == 'Current'){
                        if(opploop2.type == 'Extension') {
                            ToolsChild = ToolsChild + (prodloop2.Quantity * (prodloop2.UnitPrice - prodloop2.Previous_Value__c));
                            ToolsRenewalDate = prodloop2.End_Date__c;
                        }
                        else{
                            ToolsChild = ToolsChild + (prodloop2.Quantity * prodloop2.UnitPrice);
                            MMEvents = MMEvents + prodloop2.of_MM_Events_Allowed__c;
                            CPEvents = CPEvents + prodloop2.of_CP_Events_Allowed__c;
                            MMOverageFee = MMOverageFee + prodloop2.MM_Overage_Fee__c;
                            CPOverageFee = CPOverageFee + prodloop2.CP_Overage_Fee__c;
                            RepMonIPs = RepMonIPs + prodloop2.of_Rep_Mon_IPs__c;
                            BlacklistIPs = BlacklistIPs + prodloop2.of_BLA_IPs__c;
                            if(prodloop2.End_Date__c > ToolsRenewalDate && 
                                (opploop2.type == 'New Business' || opploop2.type == 'Cross-Sell' 
                            || opploop2.type == 'Renewal'|| opploop2.type == 'Extension')){
                                ToolsRenewalDate = prodloop2.End_Date__c;
                                ToolsStartDate =  prodloop2.Start_Date__c;
                            }
                        }
                        ToolsActive = ToolsActive + 1;
                    }
                    else if(prodloop2.product_status__c == 'Terminated') ToolsActive = ToolsActive - 1;
                 }
                 //SSRA
                 else if(prodloop2.Product_Code__c == '016-PROF-SSRA'){
                         SSRA = SSRA + (prodloop2.Quantity * prodloop2.UnitPrice);
                 }
                 //SSC SME
                 else if(prodloop2.Product_Code__c == '013-CERT-SMED'){
                         SSCSME = SSCSME + (prodloop2.Quantity * prodloop2.UnitPrice);
                 }
                 //Tools Reseller API
                 else if(prodloop2.Product_Code__c == '017-ESPR-API0'){
                         ToolsResellerAPI = ToolsResellerAPI + (prodloop2.Quantity * prodloop2.UnitPrice);
                 }
                 //Domain Assurance
                 else if(prodloop2.Product_Code__c == '021-DOMN-DOAS'){
                    if(prodloop2.Product_Status__c == 'Current'){
                        if(opploop2.type == 'Extension') {
                            DomainAssurance = DomainAssurance + (prodloop2.Quantity * (prodloop2.UnitPrice - prodloop2.Previous_Value__c));
                            DARenewalDate = prodloop2.End_Date__c;
                        }
                        else{
                            DomainAssurance = DomainAssurance + (prodloop2.Quantity * prodloop2.UnitPrice);
                            if(prodloop2.End_Date__c > DARenewalDate && 
                                (opploop2.type == 'New Business' || opploop2.type == 'Cross-Sell' 
                            || opploop2.type == 'Renewal'|| opploop2.type == 'Extension')){
                                DARenewalDate = prodloop2.End_Date__c;
                                DAStartDate = prodloop2.Start_Date__c;
                            }
                        }
                        DAActive = 'Legacy Domain Assurance';
                    }
                    else if(prodloop2.product_status__c == 'Terminated') DAActive = 'None';
                 }
          
        /**/
          
        // Email Brand Monitor
      /*  else if(prodloop2.Product_Code__c == '022-EMBR-MNTR'){
          if(prodloop2.Product_Status__c == 'Current'){
            if(opploop2.type == 'Extension') {
              EmailBrandMonitor = EmailBrandMonitor + (prodloop2.Quantity * (prodloop2.UnitPrice - prodloop2.Previous_Value__c));
              DARenewalDate = prodloop2.End_Date__c;
            }
            else{
              EmailBrandMonitor = EmailBrandMonitor + (prodloop2.Quantity * prodloop2.UnitPrice);
              if(prodloop2.End_Date__c > DARenewalDate && 
                (opploop2.type == 'New Business' || opploop2.type == 'Cross-Sell' 
              || opploop2.type == 'Renewal'|| opploop2.type == 'Extension')){
                DARenewalDate = prodloop2.End_Date__c;
                DAStartDate = prodloop2.Start_Date__c;
              }
            }
            DAActive = 'Email Brand Monitor';
          }
          else if(prodloop2.product_status__c == 'Terminated') DAActive = 'None';
        }
          
        // Email Brand Monitor Child
        else if(prodloop2.Product_Code__c == '023-EMBR-MTCH'){
          if(prodloop2.Product_Status__c == 'Current'){
            if(opploop2.type == 'Extension') {
              EmailBrandMonitorChild = EmailBrandMonitorChild + (prodloop2.Quantity * (prodloop2.UnitPrice - prodloop2.Previous_Value__c));
              DARenewalDate = prodloop2.End_Date__c;
            }
            else{
              EmailBrandMonitorChild = EmailBrandMonitorChild + (prodloop2.Quantity * prodloop2.UnitPrice);
              if(prodloop2.End_Date__c > DARenewalDate && 
                (opploop2.type == 'New Business' || opploop2.type == 'Cross-Sell' 
              || opploop2.type == 'Renewal'|| opploop2.type == 'Extension')){
                DARenewalDate = prodloop2.End_Date__c;
                DAStartDate = prodloop2.Start_Date__c;
              }
            }
            DAActive = 'Email Brand Monitor Child';
          }
          else if(prodloop2.product_status__c == 'Terminated') DAActive = 'None';
        }
          
        // Domain Protect
        else if(prodloop2.Product_Code__c == '024-DOMN-PROT'){
          if(prodloop2.Product_Status__c == 'Current'){
            if(opploop2.type == 'Extension') {
              DomainProtect = DomainProtect + (prodloop2.Quantity * (prodloop2.UnitPrice - prodloop2.Previous_Value__c));
              DARenewalDate = prodloop2.End_Date__c;
            }
            else{
              DomainProtect = DomainProtect + (prodloop2.Quantity * prodloop2.UnitPrice);
              if(prodloop2.End_Date__c > DARenewalDate && 
                (opploop2.type == 'New Business' || opploop2.type == 'Cross-Sell' 
              || opploop2.type == 'Renewal'|| opploop2.type == 'Extension')){
                DARenewalDate = prodloop2.End_Date__c;
                DAStartDate = prodloop2.Start_Date__c;
              }
            }
            DAActive = 'Domain Protect';
          }
          else if(prodloop2.product_status__c == 'Terminated') DAActive = 'None';
        }
          
        // Domain Protect Child
        else if(prodloop2.Product_Code__c == '027-DOMN-PRCH'){
          if(prodloop2.Product_Status__c == 'Current'){
            if(opploop2.type == 'Extension') {
              DomainProtectChild = DomainProtectChild + (prodloop2.Quantity * (prodloop2.UnitPrice - prodloop2.Previous_Value__c));
              DARenewalDate = prodloop2.End_Date__c;
            }
            else{
              DomainProtectChild = DomainProtectChild + (prodloop2.Quantity * prodloop2.UnitPrice);
              if(prodloop2.End_Date__c > DARenewalDate && 
                (opploop2.type == 'New Business' || opploop2.type == 'Cross-Sell' 
              || opploop2.type == 'Renewal'|| opploop2.type == 'Extension')){
                DARenewalDate = prodloop2.End_Date__c;
                DAStartDate = prodloop2.Start_Date__c;
              }
            }
            DAActive = 'Domain Protect Child';
          }
          else if(prodloop2.product_status__c == 'Terminated') DAActive = 'None';
        }
          
        // Domain Secure
        else if(prodloop2.Product_Code__c == '026-DOMN-SECR'){
          if(prodloop2.Product_Status__c == 'Current'){
            if(opploop2.type == 'Extension') {
              DomainSecure = DomainSecure + (prodloop2.Quantity * (prodloop2.UnitPrice - prodloop2.Previous_Value__c));
              DARenewalDate = prodloop2.End_Date__c;
            }
            else{
              DomainSecure = DomainSecure + (prodloop2.Quantity * prodloop2.UnitPrice);
              if(prodloop2.End_Date__c > DARenewalDate && 
                (opploop2.type == 'New Business' || opploop2.type == 'Cross-Sell' 
              || opploop2.type == 'Renewal'|| opploop2.type == 'Extension')){
                DARenewalDate = prodloop2.End_Date__c;
                DAStartDate = prodloop2.Start_Date__c;
              }
            }
            DAActive = 'Domain Secure';
          }
          else if(prodloop2.product_status__c == 'Terminated') DAActive = 'None';
        }
          
        // Domain Secure Child
        else if(prodloop2.Product_Code__c == '028-DOMN-SECH'){
          if(prodloop2.Product_Status__c == 'Current'){
            if(opploop2.type == 'Extension') {
              DomainSecureChild = DomainSecureChild + (prodloop2.Quantity * (prodloop2.UnitPrice - prodloop2.Previous_Value__c));
              DARenewalDate = prodloop2.End_Date__c;
            }
            else{
              DomainSecureChild = DomainSecureChild + (prodloop2.Quantity * prodloop2.UnitPrice);
              if(prodloop2.End_Date__c > DARenewalDate && 
                (opploop2.type == 'New Business' || opploop2.type == 'Cross-Sell' 
              || opploop2.type == 'Renewal'|| opploop2.type == 'Extension')){
                DARenewalDate = prodloop2.End_Date__c;
                DAStartDate = prodloop2.Start_Date__c;
              }
            }
            DAActive = 'Domain Secure Child';
          }
          else if(prodloop2.product_status__c == 'Terminated') DAActive = 'None';
        }
          
        /**/  
          
                 //ProServ Direct Recurring
          /*       else if(prodloop2.Product_Code__c == '008-PROF-GENC' && prodloop2.Recurring__c && prodloop2.Indirect__c == false){
                         ProServRecurringDirect = ProServRecurringDirect + (prodloop2.Quantity * prodloop2.UnitPrice);
                         if(prodloop2.End_Date__c > ProServEndDate && 
                             (opploop2.type == 'New Business' || opploop2.type == 'Cross-Sell' 
                             || opploop2.type == 'Renewal'|| opploop2.type == 'Extension')) ProServEndDate = prodloop2.End_Date__c;
                 }
                 //ProServ Indirect Recurring
                 else if(prodloop2.Product_Code__c == '008-PROF-GENC' && prodloop2.Recurring__c && prodloop2.Indirect__c){
                         ProServRecurringIndirect = ProServRecurringIndirect + (prodloop2.Quantity * prodloop2.UnitPrice);
                         if(prodloop2.End_Date__c > ProServEndDate && 
                             (opploop2.type == 'New Business' || opploop2.type == 'Cross-Sell' 
                             || opploop2.type == 'Renewal'|| opploop2.type == 'Extension')) ProServEndDate = prodloop2.End_Date__c;
                 }
                 //ProServ Direct Non-Recurring
                 else if(prodloop2.Product_Code__c == '008-PROF-GENC' && prodloop2.Recurring__c == false && prodloop2.Indirect__c == false){
                         ProServNonRecurringDirect = ProServNonRecurringDirect + (prodloop2.Quantity * prodloop2.UnitPrice);
                         if(prodloop2.End_Date__c > ProServEndDate && 
                             (opploop2.type == 'New Business' || opploop2.type == 'Cross-Sell' 
                             || opploop2.type == 'Renewal'|| opploop2.type == 'Extension')) ProServEndDate = prodloop2.End_Date__c;
                 }
                 //ProServ Indirect Non-Recurring
                 else if(prodloop2.Product_Code__c == '008-PROF-GENC' && prodloop2.Recurring__c == false && prodloop2.Indirect__c){
                         ProServNonRecurringIndirect = ProServNonRecurringIndirect + (prodloop2.Quantity * prodloop2.UnitPrice);
                         if(prodloop2.End_Date__c > ProServEndDate && 
                             (opploop2.type == 'New Business' || opploop2.type == 'Cross-Sell' 
                             || opploop2.type == 'Renewal'|| opploop2.type == 'Extension')) ProServEndDate = prodloop2.End_Date__c;
                 }
                 //Regional ISP Mediation
                 else if(prodloop2.Product_Code__c == '023-RGNL-MEDI'){
                         RegionalISPMediation = RegionalISPMediation + (prodloop2.Quantity * prodloop2.UnitPrice);
                 }
                 //CorpDev Research
                 else if(prodloop2.Product_Code__c == '024-CORP-RSCH'){
                         CorpDevResearch = CorpDevResearch + (prodloop2.Quantity * prodloop2.UnitPrice);
                 }
                 //Inbox Measurement
                 else if(prodloop2.Product_Code__c == '027-DMSU-IBM'){
                         InboxMeasurement = InboxMeasurement + (prodloop2.Quantity * prodloop2.UnitPrice);
                 }
             } //End of Product Loop
                 
             //Fill in Account to update
             Account acct = new Account(id = loopAcct.id);
       acct.ACV_Email_Brand_Monitor_Direct__c = EmailBrandMonitor;
       acct.ACV_Email_Brand_Monitor_Indirect__c = EmailBrandMonitorChild;
       acct.ACV_Domain_Protect_Direct__c = DomainProtect;
       acct.ACV_Domain_Protect_Indirect__c = DomainProtectChild;
       acct.ACV_Domain_Secure_Direct__c = DomainSecure;
       acct.ACV_Domain_Secure_Indirect__c = DomainSecureChild;
             acct.Gold_Recurring_Direct__c = Gold;
             acct.Platinum_Recurring_Direct__c = Platinum;
             acct.Tools_Only_Recurring_Direct__c = ToolsOnly;
             acct.Accred_Recurring_Direct__c = CertDirectRecurring;
             acct.Accred_Recurring_Indirect__c = CertIndirectRecurring;
             acct.accred_non_recurring_direct__c = CertDirectNonRecurring;
             acct.accred_non_recurring_indirect__c = CertIndirectNonRecurring;
             acct.Internal_Use_Recurring_Indirect__c = InternalUse;
             acct.Tools_Child_Recurring_Indirect__c = ToolsChild;
             acct.SSRA_Recurring_Indirect__c = SSRA;
             acct.SSC_Referral_Child_Recurring_Indirect__c = CertReferralChild;
             acct.SSC_SME_Recurring_Indirect__c = SSCSME = 0;
             acct.Tools_Reseller_API_Recurring_Indirect__c = ToolsResellerAPI;
             acct.ProServ_Recurring_Direct__c = ProServRecurringDirect;
             acct.ProServ_Recurring_Indirect__c = ProServRecurringIndirect;
             acct.ProServ_Non_Recurring_Direct__c = ProServNonRecurringDirect;
             acct.ProServ_Non_Recurring_Indirect__c = ProServNonRecurringIndirect;
             acct.ACV_Domain_Assurance_Direct_Recurring__c = DomainAssurance;
             acct.ACV_Inbox_Measurement_Recurring_Direct__c = InboxMeasurement;
             acct.ACV_Pixel_Tracking_Recurring_Direct__c = PixelTracking;
             acct.ACV_CorpDev_Research_Non_Recurring_Indi__c = CorpDevResearch;
             acct.ACV_Regional_ISP_Med_Non_Recurring_Indi__c = RegionalISPMediation;
             acct.Renewal_Date__c = ToolsRenewalDate;
             acct.Renewed_to_Date__c = CertRenewalDate;
             acct.Domain_Assurance_Renewal_Date__c = DARenewalDate;
             acct.ProServ_End_Date__c = ProServEndDate;
             acct.Tools_Start_Date__c = ToolsStartDate;
             acct.Certification_Start_Date__c = CertStartDate;
             acct.DA_Start_Date__c = DAStartDate; 
             acct.Monthly_MM_Events__c = MMEvents;
             acct.Monthly_CP_Events__c = CPEvents;
             acct.MM_Overage_Fee__c = MMOverageFee;
             acct.CP_Overage_Fee__c = CPOverageFee;
             acct.Reputation_Monitor_IPs__c = RepMonIPs;
             acct.Blacklist_Alert_IPs__c = BlacklistIPs;
             acct.Additional_AM_Services__c = AdditionalAMServices;
             acct.Tools_Overage_Fees__c = ToolsOverageFees;
             if(ToolsActive > 0) acct.DAS_Active_Clients__c = true;
             else acct.DAS_Active_Clients__c = false;
             if(CertActive == 1) acct.Bonded_Sender_Customer__c = true;
             else acct.Bonded_Sender_Customer__c = false;
             acct.Domain_Assurance__c = DAActive;
             AcctBatchUpdate.add(acct);
                 
             //Fill in Opp to Update
             Opportunity oppty = new Opportunity(id = opploop2.id);
             oppty.Run_ACV__c = 'No';
             oppty.Opportunity_Contract_Value__c = ContractValue;
             OppBatchUpdate.add(oppty);
             
             //ACV Table Row Creation
             ACV__c acvTable = new ACV__c(Account__c = loopAcct.Id);
             acvTable.Name = opploop2.Name;
             acvTable.Opportunity__c = opploop2.id;
             acvTable.Type__c = opploop2.type;
             acvTable.Product__c = opploop2.product__c;
             acvTable.Gold_Recurring_Direct__c = Gold;
             acvTable.Platinum_Recurring_Direct__c = Platinum;
             acvTable.Tools_Only_Recurring_Direct__c = ToolsOnly;
             acvTable.Accred_Recurring_Direct__c = CertDirectRecurring;
             acvTable.Accred_Recurring_Indirect__c = CertIndirectRecurring;
             acvTable.accred_non_recurring_direct__c = CertDirectNonRecurring;
             acvTable.accred_non_recurring_indirect__c = CertIndirectNonRecurring;
             acvTable.Internal_Use_Recurring_Indirect__c = InternalUse;
             acvTable.Tools_Child_Recurring_Indirect__c = ToolsChild;
             acvTable.SSRA_Recurring_Indirect__c = SSRA;
             acvTable.SSC_Referral_Child_Recurring_Indirect__c = CertReferralChild;
             acvTable.SSC_SME_Recurring_Indirect__c = SSCSME = 0;
             acvTable.Tools_Reseller_API_Recurring_Indirect__c = ToolsResellerAPI;
             acvTable.ProServ_Recurring_Direct__c = ProServRecurringDirect;
             acvTable.ProServ_Recurring_Indirect__c = ProServRecurringIndirect;
             acvTable.ProServ_Non_Recurring_Direct__c = ProServNonRecurringDirect;
             acvTable.ProServ_Non_Recurring_Indirect__c = ProServNonRecurringIndirect;
             acvTable.Domain_Assurance_Recurring_Direct__c = DomainAssurance;
             acvTable.Inbox_Measurement_Recurring_Direct__c = InboxMeasurement;
             acvTable.Pixel_Tracking__c = PixelTracking;
             acvTable.ACV_CorpDev_Research__c = CorpDevResearch;
             acvTable.ACV_Regional_ISP_Mediation__c = RegionalISPMediation;
             acvTable.Additional_AM_Services__c = AdditionalAMServices;
             acvTable.Tools_Overage_Fees__c = ToolsOverageFees;
             ACVTableInsert.add(acvTable);
             
              //Renewal Opportunity Creation
             if((opploop2.type == 'New Business' || opploop2.type == 'Cross-Sell' || opploop2.type == 'Renewal' || opploop2.type == 'Extension' || opploop2.type == 'Trial' || opploop2.type == 'Trial - Extension') && ((opploop2.amount > 0 && opploop2.product_tools_child__c) || opploop2.product_tools_child__c == false) &&
                 opploop2.of_Recurring_Product_Line_Items__c > 0 && opploop2.recordTypeId <> '012000000004wEC' && opploop2.Fast_Path_Initiative__c <> 'Yes' && opploop2.Renewal_Created_del__c == false){
                 
                 Opportunity RenewalOpp = new Opportunity();
                 RenewalOpp.AccountId = opploop2.accountid;
                 if(opploop2.Certification_Team_Owner__c <> null) RenewalOpp.OwnerId = opploop2.Certification_Team_Owner__c;
                 else if( opploop2.AM__c <> null) RenewalOpp.OwnerId = opploop2.AM__c;
                 else if( opploop2.CRM_User__c <> null) RenewalOpp.OwnerId = opploop2.CRM_User__c;
                 else RenewalOpp.OwnerId = loopAcct.OwnerId;
                 RenewalOpp.RecordTypeId = opploop2.recordtypeid;
                 RenewalOpp.CurrencyISOCode = opploop2.CurrencyISOCode;
                 RenewalOpp.Name = 'Renewal - ' + loopAcct.name;
                 if (opploop2.Certification_Team_Owner__c <> null || opploop2.Tools_Child_Only__c == true ) RenewalOpp.StageName = 'Obtaining Commitment';
                 else RenewalOpp.StageName = 'Demonstrating Capabilities';
                 if (RenewalOpp.StageName == 'Demonstrating Capabilities') RenewalOpp.RecordTypeId = '012000000004wVS'; 
                 else RenewalOpp.RecordTypeId = opploop2.RecordTypeId;
                 RenewalOpp.CloseDate = system.today();        
                 if(opploop2.Type == 'Trial' || opploop2.Type == 'Trial - Extension' && opploop2.Account.Type != 'Current Client/Partner') RenewalOpp.Type = 'New Business';
                 else if(opploop2.Type == 'Trial' || opploop2.Type == 'Trial - Extension' && opploop2.Account.Type == 'Current Client/Partner') RenewalOpp.Type = 'Cross-Sell';
                 else RenewalOpp.Type = 'Renewal';
                 if(opploop2.Type == 'Trial' || opploop2.Type == 'Trial - Extension') RenewalOpp.Generated_From_Free_Trial__c = true;
                 else RenewalOpp.Generated_From_Free_Trial__c = false;
                 RenewalOpp.Pricebook2Id = '01s000000004NS6AAM';
                 if (opploop2.Certification_Team_Owner__c <> null) RenewalOpp.Approval_Type__c = 'Finance Approval';
                 else RenewalOpp.Approval_Type__c = 'Pricing Approval';
                 RenewalOpp.Product__c = opploop2.Product__c;
                 RenewalOpp.Billing_Type__c = opploop2.Billing_Type__c;
                 RenewalOpp.Partner_Is_Invoiced__c = opploop2.Partner_Is_Invoiced__c;
                 RenewalOpp.Effective_Date__c = opploop2.Effective_Date__c;
                 RenewalOpp.End_User_Is_Invoiced__c = opploop2.End_User_Is_Invoiced__c;
                 RenewalOpp.eBill_Address__c = opploop2.eBill_Address__c;
                 RenewalOpp.Auto_Renew__c = opploop2.Auto_Renew__c;
                 RenewalOpp.Bond_Group_ID__c = opploop2.Bond_Group_ID__c;
                 RenewalOpp.SSC_Volume__c = opploop2.SSC_Volume__c;
                 RenewalOpp.Product_Gold__c = opploop2.Product_Gold__c; 
                 RenewalOpp.Product_Platinum__c = opploop2.Product_Platinum__c;
                 RenewalOpp.Product_Certification__c = opploop2.Product_Certification__c;
                 RenewalOpp.Product_Additional_AM_Services__c = opploop2.Product_Additional_AM_Services__c;
                 RenewalOpp.Product_Pixel_Tracking__c = opploop2.Product_Pixel_Tracking__c; 
                 RenewalOpp.Product_Deliverability_Consulting__c = opploop2.Product_Deliverability_Consulting__c;
                 RenewalOpp.Product_Internal_Use__c = opploop2.Product_Internal_Use__c;
                 RenewalOpp.Product_General_Referral__c = opploop2.Product_General_Referral__c;
                 RenewalOpp.Product_Tools_Child__c = opploop2.Product_Tools_Child__c;
                 RenewalOpp.Product_Tools_Reseller_API__c = opploop2.Product_Tools_Reseller_API__c;
                 RenewalOpp.Product_SSC_Referral_Framework__c = opploop2.Product_SSC_Referral_Framework__c;
                 RenewalOpp.Product_SSC_Referral_Child__c = opploop2.Product_SSC_Referral_Child__c;
                 RenewalOpp.Product_Tools_Reseller_Framework__c = opploop2.Product_Tools_Reseller_Framework__c;
                 RenewalOpp.Product_Inbox_Measurement__c = opploop2.Product_Inbox_Measurement__c;
                 RenewalOpp.Product_Email_Brand_Monitor__c = opploop2.Product_Email_Brand_Monitor__c;
                 RenewalOpp.Product_Email_Brand_Monitor_Child__c = opploop2.Product_Email_Brand_Monitor_Child__c;
                 RenewalOpp.Product_Domain_Protect__c = opploop2.Product_Domain_Protect__c;
                 RenewalOpp.Product_Domain_Protect_Child__c = opploop2.Product_Domain_Protect_Child__c;
                 RenewalOpp.Product_Domain_Secure__c = opploop2.Product_Domain_Secure__c;
                 RenewalOpp.Product_Domain_Secure_Child__c = opploop2.Product_Domain_Secure_Child__c;               
                 RenewalOpp.AccountManager__c = opploop2.AccountManager__c;
                 RenewalOpp.TermofContractinmonths__c = opploop2.TermofContractinmonths__c;
                 RenewalOpp.Previous_Opportunity_Lookup__c = opploop2.id;
                 RenewalOpp.Certification_Tier_Sold__c = opploop2.Certification_Tier_Sold__c;
                 RenewalOpp.Activation_Date__c = opploop2.Activation_Date__c; 
                 RenewalOpp.Nature_of_Involvement__c = opploop2.Nature_of_Involvement__c;
                 RenewalOpp.PartnerLookup__c = opploop2.PartnerLookup__c;
                 RenewalOpp.Partner_Contact__c = opploop2.Partner_Contact__c;
                 RenewalOpp.Type_of_InvolvementText__c = opploop2.Type_of_InvolvementText__c;
                 RenewalOpp.CRM_User__c = opploop2.CRM_User__c;
                 RenewalOpp.Deal_Closing_Through__c = opploop2.Deal_Closing_Through__c;
                 RenewalOpp.Partner_Commission__c = opploop2.Partner_Commission__c;
                 RenewalOpp.Who_is_Invoiced__c = opploop2.Who_is_Invoiced__c;
                 if(opploop2.Domain_Assurance_Volume_Tier__c <> null) RenewalOpp.Domain_Assurance_Volume_Tier__c = opploop2.Domain_Assurance_Volume_Tier__c;
                 RenewalOppInsert.add(RenewalOpp);
                 OppIds.add(opploop2.id);
                 RenewalCreated1.add(opploop2);
             }
            
         }//End of Opportunity Loop   
        
        Update AcctBatchUpdate;
        Update OppBatchUpdate;
        Insert ACVTableInsert;
        Insert RenewalOppInsert;
      
        //Renewal Line Item and Partner Create
        for(Opportunity opploop3 : RenewalOppInsert){
            for(OpportunityLineItem prodloop3 : ProductsInOpps){
                if(prodloop3.OpportunityId == opploop3.Previous_Opportunity_Lookup__c && prodloop3.Product_Code__c <> '008-PROF-GENC' && prodloop3.Product_Status__c == 'Current'){
                    OpportunityLIneItem ProdCreate = new OpportunityLineItem();
                    ProdCreate.OpportunityId = opploop3.id;
                    ProdCreate.PricebookEntryId = prodloop3.pricebookentryid;
                    ProdCreate.Quantity = prodloop3.Quantity;
                    if(prodloop3.Opportunity_Type__c == 'Extension'&& prodloop3.Start_Date__c != null && prodloop3.End_Date__c != null && (prodloop3.Start_Date__c.monthsBetween(prodloop3.End_Date__c) > 0))ProdCreate.UnitPrice = ((prodloop3.UnitPrice / prodloop3.Start_Date__c.monthsBetween(prodloop3.End_Date__c))*12).SetScale(2);
                    else ProdCreate.UnitPrice = prodloop3.UnitPrice;
                    if(prodloop3.Opportunity_Type__c == 'Extension') ProdCreate.Previous_Value__c = ProdCreate.UnitPrice;
                    else ProdCreate.Previous_Value__c = prodloop3.UnitPrice;
                    ProdCreate.of_MM_Events_Allowed__c = prodloop3.of_MM_Events_Allowed__c;
                    ProdCreate.of_CP_Events_Allowed__c = prodloop3.of_CP_Events_Allowed__c;
                    ProdCreate.MM_Overage_Fee__c = prodloop3.MM_Overage_Fee__c;
                    ProdCreate.CP_Overage_Fee__c = prodloop3.CP_Overage_Fee__c;
                    ProdCreate.of_BLA_IPs__c = prodloop3.of_BLA_IPs__c;
                    ProdCreate.of_Rep_Mon_IPs__c = prodloop3.of_Rep_Mon_IPs__c;
                    ProdCreate.Product_Status__c = prodloop3.Product_Status__c;
                    ProdCreate.Description__c = prodloop3.Description__c;
                    ProdCreate.Indirect__c = prodloop3.Indirect__c;
                    ProdCreate.Active__c = prodloop3.Active__c;
                    ProdCreate.Bundle__c = prodloop3.Bundle__c;
                    ProdCreate.Recurring__c = prodloop3.Recurring__c;
                    if(prodloop3.Start_Date__c <> null) ProdCreate.Start_Date__c = prodloop3.End_Date__c + 1;
                    if(ProdCreate.Start_Date__c <> null) ProdCreate.End_Date__c = ProdCreate.Start_Date__c + 364;
                    RenewalLineItemInsert.add(ProdCreate);
                }
            }
                
            for(Partner partnerloop1 : PartnerList){
                if(partnerloop1.opportunityid == opploop3.Previous_Opportunity_Lookup__c && partnerloop1.accounttoid <> opploop3.accountid){
                    Partner PartnerCreate = new Partner();
                    PartnerCreate.OpportunityId = opploop3.id;
                    PartnerCreate.AccounttoId = partnerloop1.AccounttoId;
                    PartnerCreate.IsPrimary = partnerloop1.IsPrimary;
                    PartnerCreate.Role = partnerloop1.Role;
                    RenewalPartnerInsert.add(PartnerCreate);
                    break;
                }
            }
                
        }
        
        Insert RenewalLineItemInsert;
        Insert RenewalPartnerInsert;

    
  list<Opportunity> Renewals = new list<Opportunity> ([SELECT Id, Minimum_Start_Date__c, CloseDate FROM Opportunity WHERE Id in : RenewalOppInsert]);
    
        for(Opportunity Renewal : Renewals){
            Opportunity Renew = new Opportunity(Id=Renewal.Id);
            Renew.CloseDate = Renewal.Minimum_Start_Date__c;
            CloseDateRenewals.add(Renew);
            }
            
            update CloseDateRenewals;
            
    list<Opportunity> PreviousOpps = new list<Opportunity> ([SELECT Id, Renewal_Created_del__c FROM Opportunity WHERE Id in :RenewalCreated1]);
    
        for(Opportunity po : PreviousOpps){
        Opportunity prev = new Opportunity(Id=po.Id);
        prev.Renewal_Created_del__c = true;
        RenewalCreatedOpps.add(prev);
        }
        
        update RenewalCreatedOpps;
    
        
        }*/
    
}