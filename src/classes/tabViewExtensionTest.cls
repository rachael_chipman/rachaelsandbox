@isTest
public class tabViewExtensionTest {
    
    public static list<Opportunity> oppList = new list<Opportunity>();
    public static Account testAccount;
    
    static testMethod void testTabViewExtension()
    {
        //setup test records
        testAccount = TestUtils.createAccount(TestUtils.MARKETER_RECORD_TYPE.Id, true);
        opplist = TestUtils.createOpportunities(10, TestUtils.PRE_PIPELINE_RECORD_TYPE.Id, testAccount.Id, true);
        if(opplist!= null)
        {
            Opportunity testOpp = opplist.get(0);
            PageReference pageref = Page.tabView;
            pageref.getParameters().put('id',testOpp.Id);
            Test.setCurrentPage(pageref);
            
            ApexPages.StandardController sc = new ApexPages.StandardController(testOpp);
            tabViewExtension testcontroller = new  tabViewExtension(sc);
                       
            system.debug('What is recType' +testController.recType);
            system.assertEquals(TestUtils.PRE_PIPELINE_RECORD_TYPE.Id, testController.recType.Id, 'We expect the extension to return a record type.');
            system.assertNotEquals(null, testController.profileName, 'We expect the extension to return a profileName');
        }
    }

}