//Matt Starr - 3/4/2013 - Created
//DESCRIPTION - This trigger populates the the First Campaign and Mopst Recent Campaign on the Lead, Contact or opportunity and
//continually updates accross the objects.

trigger CampaignTracker on CampaignMember (After Insert, Before Delete) {



if(trigger.isInsert || trigger.isUpdate){

for(CampaignMember cm : Trigger.New){


    if(cm.LeadId != null && cm.ContactId == null){
    

    Lead l = [select Id, First_Campaign__c, Campaign_Name__c, Name, Company, LeadSource, Most_Recent_Lead_Source__c, Owner.Email, 
              Status, isconverted from Lead where Id = :cm.LeadId];
    Campaign c = [select Id, Name, Type from Campaign where Id = :cm.CampaignId];


    if(l.First_Campaign__c != null && l.isconverted == false){
    l.Campaign_Name__c = c.Name;
    l.Most_Recent_Lead_Source__c = c.Type;
    

    update l;
    
     }
 

     if(l.First_Campaign__c == null && l.isconverted == false){
     l.First_Campaign__c = c.Name;
     l.Campaign_Name__c = c.Name;
     l.Most_Recent_Lead_Source__c = c.Type;
     l.LeadSource = c.Type;
     
    update l;
    
 }

   if(l.OwnerId == '00500000006olmF' && l.Status == 'Eloqua Lead Scoring'){
   //Fetching the assignment rules on case
AssignmentRule AR = new AssignmentRule();
AR = [select id from AssignmentRule where SobjectType = 'Lead' and Active = true limit 1];

//Creating the DMLOptions for "Assign using active assignment rules" checkbox
Database.DMLOptions dmlOpts = new Database.DMLOptions();
dmlOpts.assignmentRuleHeader.assignmentRuleId= AR.id;
l.setOptions(dmlOpts);
update l ;
}
   
    
     
}
    //if the CM is attached to a Contact
    if(cm.ContactId != null){
    
     Contact con = [Select Name, First_Campaign__c, Campaign_Name__c, Account.Customer_Relationship_Manager_Primary__c, 
     Account.Owner.Email, Account.CRM__r.Email, Account.AM__r.Email, Account.AccountManager__c,
     Account.Name, Account.Type from Contact where Id = :cm.ContactId];
     
     Campaign c = [select Id, Name from Campaign where Id = :cm.CampaignId];
    
     if(con.First_Campaign__c != null){
     con.Campaign_Name__c = c.Name; 
     update con;   
     // figure out who the email is going to and set it up
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                
            if (con.Account.AccountManager__c == null) {
                if (con.Account.Customer_Relationship_Manager_Primary__c == null) {
                    mail.setToAddresses(new String[] {con.Account.Owner.Email});
                } else {                 
                if(con.Account.CRM__c == null) {mail.setToAddresses(new String[] {con.Account.Owner.Email});
                } else { mail.setToAddresses(new String[] {con.Account.CRM__r.Email}); }}
                } else {
                if(con.Account.AM__c == null) {mail.setToAddresses(new String[] {con.Account.Owner.Email});
                } else { mail.setToAddresses(new String[] {con.Account.AM__r.Email}); }}
             
            
            mail.setSenderDisplayName('Salesforce Alerts');
            mail.setSubject('NOTICE: A contact or lead of yours has been added to an additional campaign');
            mail.setccAddresses(new String[] {'chris.kingdon@returnpath.com'});
            mail.setHTMLBody(
                '<p>A contact under an account related to you has been added to an additional campaign.' +
                '<p>Name: ' + con.Name +
                '<br/>Account: ' + con.Account.Name +
                '<br/>Account Type: ' + con.Account.Type +
                '<br/>New campaign: ' + c.Name +
                '<p>Please review the contact record and follow up as appropriate: ' + 'https://ssl.salesforce.com/' + cm.ContactId);
                            
                Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });   
                
                
    List<OpportunityContactRole> ocr = [select Id, OpportunityId from OpportunityContactRole where ContactId =:con.Id];
        if (ocr.isEmpty()) return;
            
    for(OpportunityContactRole ocrs : ocr){
                 
    List<Opportunity> o = [select Id, Name, Most_Recent_Campaign__c, Campaign_Tracker_Time_Stamp__c, isClosed from Opportunity where Id = :ocrs.OpportunityId];
                     if (o.isEmpty()) return;
     
     for(Opportunity ops : o){
     
    if(ops.isClosed == false){
        ops.Most_Recent_Campaign__c = c.Name;
        ops.Campaign_Tracker_Time_Stamp__c = system.Now();
        if(ops.First_Campaign__c == null){ops.First_Campaign__c = con.First_Campaign__c;}
        update ops;
        }
             
     
     } } }
    
        if(con.First_Campaign__c == null && c.Id != '7010000000068Tm'){
    con.First_Campaign__c = c.Name;
    con.Campaign_Name__c = c.Name;
    con.First_Campaign_Id__c = c.Id;
    update con;
    }
    
    if(con.First_Campaign__c == null && c.Id == '7010000000068Tm'){
    con.First_Campaign__c = c.Name;
    con.Campaign_Name__c = c.Name;
    update con;
    
    Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
           if (con.Account.AccountManager__c == null) {
                if (con.Account.Customer_Relationship_Manager_Primary__c == null) {
                    mail.setToAddresses(new String[] {con.Account.Owner.Email});
                } else {                 
                    if(con.Account.CRM__c == null) {mail.setToAddresses(new String[] {con.Account.Owner.Email});
                } else { mail.setToAddresses(new String[] {con.Account.CRM__r.Email}); }}
                } else {
                    if(con.Account.AM__c == null) {mail.setToAddresses(new String[] {con.Account.Owner.Email});
                } else { mail.setToAddresses(new String[] {con.Account.AM__r.Email}); }}
            
            mail.setSenderDisplayName('Salesforce Alerts');
            mail.setSubject('NOTICE: A contact or lead of yours has been added to an additional campaign');
            mail.setccAddresses(new String[] {'matt.starr@returnpath.com'});
            mail.setHTMLBody(
                '<p>A contact under an account related to you has been added to an additional campaign.' +
                '<p>Name: ' + con.Name +
                '<br/>Account: ' + con.Account.Name +
                '<br/>Account Type: ' + con.Account.Type +
                '<br/>New campaign: ' + c.Name +
                '<p>Please review the contact record and follow up as appropriate: ' + 'https://ssl.salesforce.com/' + cm.ContactId);
                            
                Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
    }
    
    } } }
    
    if(trigger.isDelete){
        //If the record is being deleted
        for(CampaignMember cm : Trigger.old){
            if(cm.LeadId != null && cm.ContactId == null){
            Lead l = [select Id, First_Campaign__c, Campaign_Name__c, Name, Company, Owner.Email, Status from Lead where Id = :cm.LeadId];
            List<CampaignMember> nextCM = [select Id, CreatedDate, CampaignId from CampaignMember where Id != :cm.Id and LeadId = :cm.LeadId order by CreatedDate DESC limit 1];
            
            if(nextCM.size() == 0){l.Campaign_Name__c = null; l.First_Campaign__c = null; update l; return;} 
            for(CampaignMember cm2 : nextCM){
            
            Campaign c = [select Id, Name from Campaign where Id = :cm2.CampaignId];
    
                if(l.Campaign_Name__c != null){
                l.Campaign_Name__c = c.Name; 
                update l; 
                } } }
                
                
            if(cm.ContactId != null){   
            Contact con = [Select Name, First_Campaign__c, Campaign_Name__c from Contact where Id = :cm.ContactId];
            List<CampaignMember> nextCM = [select Id, CreatedDate, CampaignId from CampaignMember where Id != :cm.Id and ContactId = :cm.ContactId order by CreatedDate DESC limit 1];
            
            if(nextCM.size() == 0){con.Campaign_Name__c = null; con.First_Campaign__c = null; update con; 
                        
             List<OpportunityContactRole> ocr = [select Id, OpportunityId from OpportunityContactRole where ContactId =:con.Id];
        if (ocr.isEmpty()) return;
            
    for(OpportunityContactRole ocrs : ocr){
                 
    List<Opportunity> o = [select Id, Name, Most_Recent_Campaign__c, First_Campaign__c, Campaign_Tracker_Time_Stamp__c, isClosed from Opportunity where Id = :ocrs.OpportunityId];
                     if (o.isEmpty()) return;
     
     for(Opportunity ops : o){
     
    if(ops.isClosed == false){ops.Most_Recent_Campaign__c = null; ops.First_Campaign__c = null; ops.Campaign_Tracker_Time_Stamp__c = system.Now();}
      update ops;      
            
            } } } 
            
            
            
            
            
            for(CampaignMember cm2 : nextCM){            
            Campaign c = [select Id, Name from Campaign where Id = :cm2.CampaignId];
            
            if(con.Campaign_Name__c != null){con.Campaign_Name__c = c.Name;
            
             List<OpportunityContactRole> ocr = [select Id, OpportunityId from OpportunityContactRole where ContactId =:con.Id];
        if (ocr.isEmpty()) return;
            
    for(OpportunityContactRole ocrs : ocr){
                 
    List<Opportunity> o = [select Id, Name, Most_Recent_Campaign__c, Campaign_Tracker_Time_Stamp__c, First_Campaign__c, isClosed from Opportunity where Id = :ocrs.OpportunityId];
                     if (o.isEmpty()) return;
     
     for(Opportunity ops : o){
     
    if(ops.isClosed == false){ops.Most_Recent_Campaign__c = c.Name; ops.Campaign_Tracker_Time_Stamp__c = system.Now();}
      update ops;               
            
            
            
            
            }
            update con;  
                } } }
             
    }
    }
    }
    }