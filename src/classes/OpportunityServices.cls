public without sharing class OpportunityServices {

    public static final String PENDING_CERTIFICATION = 'Pending Certification Qualification';
    public static final String FINANCE_APPROVED = 'Finance Approved';
    public static final String TERMINATION_STAGENAME = 'Terminated Contract';

    public static final Set<String> validStageNamesforCommissionCalulation = new Set<String> { PENDING_CERTIFICATION, FINANCE_APPROVED };
    public static final Set<String> validStageNamesforCommissionAndExchangeRates = new Set<String> { PENDING_CERTIFICATION, FINANCE_APPROVED, TERMINATION_STAGENAME };

    public static final String QUARTERLY_ROLE = 'Regional Sales Executive';
    //public static final String MEMBER_NOT_FOUND_TEMPLATE = Label.MEMBER_NOT_FOUND_IN_RATE_CARD;
    public static Boolean isRunning = false;

    public static final String ANNUAL = 'Annual';
    public static final String SEMI_ANNUALLY = 'Semi-Annual';
    public static final String MONTHLY = 'Monthly';
    public static final String QUARTERLY = 'Quarterly';
    public static final Set<String> BILLING_TYPES = new Set<String>{ANNUAL,QUARTERLY,MONTHLY,SEMI_ANNUALLY};
    public static final Set<String> TYPES = new Set<String>{'New Business', 'Renewal', 'Extension','Fee', 'Cross-Sell' };
    public static final String STAGENAME = 'Finance Approved';
    public static final String COMPANY_NAME_FORMULA = 'Return Path, Inc.';
    public static final String PARTNER_INVOICED = 'Partner is Invoiced';
    public static final String CUSTOMER_INVOICED = 'Customer is Invoiced';
    public static final String DEAL_CLOSING_THROUGH_RP = 'Return Path';

    public static final String PRODUCT_CODE_REFERRAL = 'REF-CR';
    public static final String PRODUCT_CODE_RESELLER = 'RES_CR';
    public static final Map<String, Integer> BILLING_INTERVAL_MAP = new Map<String, Integer>{ ANNUAL => 12, SEMI_ANNUALLY => 6, QUARTERLY => 3, MONTHLY => 1 };
    public static final Map<String, Integer> BILLING_TYPE_MULTIPLES = new Map<String, Integer>{ ANNUAL => 1, SEMI_ANNUALLY => 2, QUARTERLY => 4, MONTHLY => 12 };

    public static c2g__codaCompany__c RETURN_PATH_COMPANY
    {
        get
        {
            if( RETURN_PATH_COMPANY == NULL )
            {
                RETURN_PATH_COMPANY = [SELECT Id FROM c2g__codaCompany__c WHERE Name = :COMPANY_NAME_FORMULA LIMIT 1];
            }
            return RETURN_PATH_COMPANY;
        }
        private set;
    }

    public static c2g__codaIncomeScheduleDefinition__c INCOME_SCHEDULE_DEFINITION
    {
        get
        {
            if( INCOME_SCHEDULE_DEFINITION == NULL )
            {
                INCOME_SCHEDULE_DEFINITION = [SELECT Id, Name FROM c2g__codaIncomeScheduleDefinition__c WHERE Name = 'Income Schedule' LIMIT 1];
            }
            return INCOME_SCHEDULE_DEFINITION;
        }
        private set;
    }

      /* Commented out by urvi tanna - bluewolf
      public static Set<Id> filterforCommissionCalculation ( List<Opportunity> updatedOpps, Map<Id, Opportunity> oldOpps ){

        Set<Id> filteredOpportunities = new Set<Id>();
        for ( Opportunity opp : updatedOpps ){
            Opportunity oldOpp = oldOpps.get ( opp.Id );

            if ( opp.StageName != oldOpp.StageName && validStageNamesforCommissionCalulation.contains ( opp.StageName )
                    && !validStageNamesforCommissionCalulation.contains ( oldOpp.StageName ) &&
                    ( opp.Rate_Already_Calculated__c == null || opp.Rate_Already_Calculated__c < 2 ) ){
                filteredOpportunities.add ( opp.Id );
            }
        }

        return filteredOpportunities;
    }

    public static List<Opportunity> filterForSettingCommissionAndExchangeRates ( List<Opportunity> updatedOpps, Map<Id, Opportunity> oldOpps ){

        List<Opportunity> filteredOpportunities = new List<Opportunity>();
        for ( Opportunity opp : updatedOpps ){
            Opportunity oldOpp = oldOpps.get ( opp.Id );

            if ( opp.StageName != oldOpp.StageName && validStageNamesforCommissionAndExchangeRates.contains ( opp.StageName )
                    && !validStageNamesforCommissionAndExchangeRates.contains ( oldOpp.StageName ) ){
                filteredOpportunities.add ( opp );
            }
        }

        return filteredOpportunities;
    }

    public static OpportunityTeamMember setCommissionPodAndExchangeRate ( OpportunityTeamMember member,
            String podName,
            DatedConversionRate dcr,
            CurrencyType currType ){
        member.Team_Pods__c = podName;
        member.Exchange_Rate__c = dcr.ConversionRate;
        member.Company_Exchange_Rate__c = currType.ConversionRate;
        member.CurrencyIsoCode = 'USD';
        member.Percent_Commissioned_On__c = 100;

        return member;
    }

    public static Map<String, CurrencyType> getisoCodeToCurrencyType ( Set<String> isoCodes ){

        Map<String, CurrencyType> isoCodeToCurrencyType = new Map<String, CurrencyType>();
        for ( CurrencyType currType : [select ConversionRate, IsoCode from CurrencyType where IsoCode IN : isoCodes ] ){
            isoCodeToCurrencyType.put ( currType.IsoCode, currType );
        }

        return isoCodeToCurrencyType;
    }

    public static Map<Id, DatedConversionRate> getDatedConversionRate ( Set<String> isoCodes, List<Opportunity> opps ){

        Map<Id, DatedConversionRate> oppIdToConversionRate = new Map<Id, DatedConversionRate>();
        Date minimumCloseDate;
        Date maximumCloseDate;

        for ( Opportunity opp : opps ){
            if ( minimumCloseDate == null || opp.closeDate < minimumCloseDate ){
                minimumCloseDate = opp.closeDate;
            }

            if ( maximumCloseDate == null || opp.closeDate > maximumCloseDate ){
                maximumCloseDate = opp.closeDate;
            }
        }

        for ( Opportunity opp : opps ){

            for ( DatedConversionRate dcr : [SELECT ConversionRate, StartDate, NextStartDate, IsoCode
                                             FROM DatedConversionRate
                                             WHERE StartDate <= :minimumCloseDate
                                             AND NextStartDate > :maximumCloseDate
                                             AND IsoCode IN :isoCodes ] )
            {
                if ( dcr.StartDate <= opp.closeDate && dcr.NextStartDate > opp.closeDate && dcr.IsoCode == opp.CurrencyIsoCode ){
                    oppIdToConversionRate.put ( opp.Id, dcr );
                }
            }
        }

        return oppIdToConversionRate;

    }*/

   /* COMMENTED OUT 2/24/14 by JK/YC

    public static List<OpportunityTeamMember> calculateCommissionForTeamMembers ( List<Opportunity> oppWithTeamMembers ){

        Set<String> opportunityTypes = new Set<String>();
        Set<String> userRoles = new Set<String>();
        Set<Id> yearlyUserIds = new Set<Id>();
        Set<Id> quarterlyUserIds = new Set<Id>();
        Set<Id> teamMemberIds = new Set<Id>();
        String errorString = '';
        Set<Id> teamMemberProcessed = new Set<Id>();
        List<OpportunityTeamMember> teamMembersToUpdate = new List<OpportunityTeamMember>();

        for ( Opportunity oppWithTeamMember : oppWithTeamMembers ){

            opportunityTypes.add ( oppWithTeamMember.Type );
            for ( OpportunityTeamMember member : oppWithTeamMember.OpportunityTeamMembers ){
                userRoles.add ( member.TeamMemberRole );
                if ( member.TeamMemberRole == QUARTERLY_ROLE )
                    quarterlyUserIds.add ( member.UserId );
                else
                    yearlyUserIds.add ( member.UserId );
            }
        }

        Set<Id> userIds = quarterlyUserIds;
        userIds.addAll ( yearlyUserIds );
        Map<String, RateCardModel> managementNACVRates = RateCardServices.getManagementNACVRateModels ( userRoles, userIds );
        teamMembersToUpdate.addAll ( populateCommissionOnACVBeforePartner ( opportunityTypes, userRoles, oppWithTeamMembers, teamMemberProcessed ) );
        teamMembersToUpdate.addAll ( populateCommissionOnManagementNACV_Quarterly ( quarterlyUserIds, oppWithTeamMembers, managementNACVRates, teamMemberProcessed ) );
        teamMembersToUpdate.addAll ( populateCommissionOnManagementNACV_yearly ( yearlyUserIds, oppWithTeamMembers, managementNACVRates, teamMemberProcessed ) );

        for ( Opportunity oppWithTeamMember : oppWithTeamMembers ){
            for ( OpportunityTeamMember member : oppWithTeamMember.OpportunityTeamMembers ){
                if (  !teamMemberProcessed.contains ( member.Id ) ){
                    errorString +=  'Opportunity ' + URL.getSalesforceBaseUrl().toExternalForm() +'/' + oppWithTeamMember.id + '<br/>';
                    errorString += String.format ( MEMBER_NOT_FOUND_TEMPLATE, new List<String>{member.User.Name, member.TeamMemberRole} ) + '<br/>';
                }
            }
        }

        if ( !String.isBlank ( errorString ) ){
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();

            // Strings to hold the email addresses to which you are sending the email.
            String[] toAddresses = new String[] {'Bryan.Wilson@returnpath.com'};


            // Assign the addresses for the To and CC lists to the mail object.
            mail.setToAddresses(toAddresses);

            // Specify the name used as the display name.
            mail.setSenderDisplayName('Commission rate error');

            // Specify the subject line for your email address.
            mail.setSubject('There was an error while processing commission');

            // Specify the text content of the email.
            mail.setHtmlBody( errorString );

            // Send the email you have created.
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
        }

        return teamMembersToUpdate;
    }

    private static List<OpportunityTeamMember> populateCommissionOnACVBeforePartner ( Set<String> opportunityTypes, Set<String> userRoles,
                                                                                      List<Opportunity> oppWithTeamMembers, Set<Id> teamMemberProcessed ){

        List<OpportunityTeamMember> teamMembersToUpdate = new List<OpportunityTeamMember>();
        Map<String, Decimal> acvBeforPartners = RateCardServices.getACVBeforePartnerCommission ( opportunityTypes, userRoles );
        for ( Opportunity oppWithTeamMember : oppWithTeamMembers ){

            for ( OpportunityTeamMember member : oppWithTeamMember.OpportunityTeamMembers ){
                String key = oppWithTeamMember.Type + member.TeamMemberRole;

                if ( acvBeforPartners.containsKey( key ) ){
                    member.Commission_Percentage__c = acvBeforPartners.get( key );
                    teamMembersToUpdate.add ( member );
                    teamMemberProcessed.add ( member.Id );
                }
            }
        }

        return teamMembersToUpdate;
    }

    @TestVisible private static List<OpportunityTeamMember> populateCommissionOnManagementNACV_Quarterly ( Set<Id> quarterlyUserIds, List<Opportunity> oppWithTeamMembers,
                                                                                                           Map<String, RateCardModel> managementNACVRates, Set<Id> teamMemberProcessed ){

        List<OpportunityTeamMember> teamMembersToUpdate = new List<OpportunityTeamMember>();
        Map<Id, Decimal> userIdToTotalAmount = new Map<Id, Decimal>();

        List<Opportunity> opps = [SELECT Id FROM Opportunity WHERE Opportunity.StageName IN :validStageNamesforCommissionCalulation AND Opportunity.Reporting_Date__c = THIS_QUARTER];
        for ( AggregateResult agr : [SELECT UserId, sum(NA_Management_NACV__c)amount
                                     FROM OpportunityTeamMember
                                     WHERE OpportunityId IN :opps
                                     AND UserId IN :quarterlyUserIds

                                     Group By UserId]){

            Decimal amount = (Decimal)agr.get('amount');
            if ( amount != null )
                userIdToTotalAmount.put ( (String)agr.get( 'UserId' ), (Decimal) agr.get('amount') );

        }

        for ( Opportunity opp : oppWithTeamMembers ){

            for ( OpportunityTeamMember member : opp.OpportunityTeamMembers ){

                if ( !teamMemberProcessed.contains(member.Id ) ){
                    Decimal amount = userIdToTotalAmount.get ( member.UserId );

                    if ( managementNACVRates.containsKey ( member.TeamMemberRole ) ){
                        member.Commission_Percentage__c = managementNACVRates.get ( member.TeamMemberRole ).getRateBasedOnThreshold ( amount );
                        teamMembersToUpdate.add ( member );
                        teamMemberProcessed.add ( member.Id );
                    }
                }
            }
        }

        return teamMembersToUpdate;
    }

    @TestVisible private static List<OpportunityTeamMember> populateCommissionOnManagementNACV_yearly ( Set<Id> yearlyUserIds, List<Opportunity> oppWithTeamMembers,
                                                                                                        Map<String, RateCardModel> managementNACVRates, Set<Id> teamMemberProcessed ){

        List<OpportunityTeamMember> teamMembersToUpdate = new List<OpportunityTeamMember>();
        Map<Id, Decimal> userIdToTotalAmount = new Map<Id, Decimal>();
        List<Opportunity> opps = [SELECT Id FROM Opportunity WHERE Opportunity.StageName IN :validStageNamesforCommissionCalulation AND Opportunity.Reporting_Date__c = THIS_YEAR];
        for ( AggregateResult agr : [SELECT UserId, sum(NA_Management_NACV__c)amount
                                     FROM OpportunityTeamMember
                                     WHERE UserId IN :yearlyUserIds
                                     AND OpportunityId IN :opps
                                     Group By UserId]){

            Decimal amount = (Decimal)agr.get('amount');
            if ( amount != null )
                userIdToTotalAmount.put ( (String)agr.get( 'UserId' ), amount  );
        }

        for ( Opportunity opp : oppWithTeamMembers ){
            for ( OpportunityTeamMember member : opp.OpportunityTeamMembers ){

                if ( !teamMemberProcessed.contains(member.Id ) ){
                    Decimal amount = userIdToTotalAmount.get ( member.UserId );
                    String key = member.TeamMemberRole + member.UserId;
                    if ( managementNACVRates.containsKey ( key ) ){
                        member.Commission_Percentage__c = managementNACVRates.get ( key ).getRateBasedOnThreshold ( amount );
                        teamMembersToUpdate.add ( member );
                        teamMemberProcessed.add ( member.Id );
                    }
                }
            }
        }

        return teamMembersToUpdate;
    }
     */
    /******************************************************** Opp Post Invoice Process **************************************************************/

    public static Boolean isInvoiceTriggerRunning = false;
    public static void processToCreateInvoiceAndLineItems( List<Opportunity> newOpportunities, Map<Id, Opportunity> oldOpportunities  )
    {
        List<Opportunity> filteredOpps = filterOpportunitiesReadyForInvoice( newOpportunities, oldOpportunities );
        if( filteredOpps.isEmpty() )
        {
            return;
        }
        isInvoiceTriggerRunning = true;

        /****************************** INVOICE CREATION PROCESS *********************************/

        List<Opportunity> partnerOpportunities = getPartnerOpportunities( filteredOpps );
        Map<Id, Account> oppIdToPartner = getPartners( partnerOpportunities );
        Map<String, c2g__codaAccountingCurrency__c> currencyMap = CurrencyConversionServices.getAllCurrencies( filteredOpps, oppIdToPartner );
        Map<Id, c2g__codaAccountingCurrency__c> oppIdToCurrency = InvoiceCreationService.getInvoiceCurrencies( filteredOpps, oppIdToPartner, currencyMap );

        Map<Id, List<c2g__codaInvoice__c>> oppIdToInvoices = InvoiceCreationService.createOpportunityInvoices( filteredOpps, oppIdToCurrency, oppIdToPartner );

        /****************************** EXCHANGE RATE CALC *******************************************/

        //Currency coversions is to be done only for opps which are partner invoiced and which have currency different than the partner currency
        List<Opportunity> oppsNeedingExRateConversion = getPartnerInvoicedOpportunities( filteredOpps, oppIdToPartner ).values();

        Map<Id, Decimal> oppIdToRate = new Map<Id, Decimal>();
        Map<Id, Decimal> oppIdToPartnerRate = new Map<Id, Decimal>();

        if( !oppsNeedingExRateConversion.isEmpty() )
        {
            Set<Id> accountingCurrencyIds = new Set<Id>();
            for( String aCurrency : currencyMap.keySet() )
            {
                accountingCurrencyIds.add( currencyMap.get( aCurrency ).Id );
            }
            Map<Id, Date> oppIdToInvoiceDate = new Map<Id, Date>();
            for( Id oppId : oppIdToInvoices.keySet() )
            {
                oppIdToInvoiceDate.put( oppId, oppIdToInvoices.get( oppId )[0].c2g__InvoiceDate__c );
            }
            Map<String, Map<Date, c2g__codaExchangeRate__c>> currencyISOCodeToExRate = CurrencyConversionServices.getAllExchangeRates( oppIdToInvoiceDate, accountingCurrencyIds );
            oppIdToRate = CurrencyConversionServices.getOpportunityRate( filteredOpps, currencyISOCodeToExRate, oppIdToInvoiceDate );
            oppIdToPartnerRate = CurrencyConversionServices.getPartnerRates( filteredOpps, currencyISOCodeToExRate, oppIdToInvoiceDate, oppIdToPartner );
        }

        /****************************** EXCHANGE RATE CALC END ***************************************/


        List<c2g__codaInvoice__c> invoicesToBeInserted = new List<c2g__codaInvoice__c>();
        List<Opportunity> oppsNeedingJournals = filterOpportunitiesNeedingJournals( filteredOpps );
        Set<Id> oppIdsNeedingJournals = Pluck.ids( oppsNeedingJournals );
        Map<Id, OpportunityLineItem> oppIdToReferralOLI = getReferralOLI( oppIdsNeedingJournals );

        for( Id oppId : oppIdToInvoices.keySet() )
        {
            Opportunity filterOpp = new Map<Id, Opportunity>( filteredOpps ).get( oppId );
            /************** Exchange rate populating **************/

            Decimal oppExRate = 1.0;
            Decimal partnerExRate = 1.0;

            if( filterOpp.PartnerLookup__c != NULL && filterOpp.Who_is_Invoiced__c == PARTNER_INVOICED && filterOpp.CurrencyISOCode != oppIdToPartner.get( filterOpp.Id ).CurrencyISOCode )
            {
                oppExRate = oppIdToRate.get( filterOpp.Id );
                partnerExRate = oppIdToPartnerRate.get( filterOpp.Id );
            }
            List<c2g__codaInvoice__c> invoices = oppIdToInvoices.get( oppId );

            for( c2g__codaInvoice__c anInvoice : invoices )
            {
                anInvoice.Opportunity_Exchange_Rate__c = oppExRate;
                anInvoice.Partner_Exchange_Rate__c = partnerExRate;
            }
            /*********** Partner Commission price calc *************/

            List<Decimal> partnerCommissionAmounts = new List<Decimal>();
            if( oppIdToReferralOLI.containsKey( oppId ) )
            {
                String billingType = filterOpp.Billing_Type__c;
                partnerCommissionAmounts = InvoiceLineItemCreationService.getInvoiceLineItemPrices( oppIdToReferralOLI.get( oppId ), billingType, oppExRate, partnerExRate );
                for( Integer i = 0; i < partnerCommissionAmounts.size(); i++ )
                {
                    invoices[i].Partner_Commission_Amount__c = Math.abs( partnerCommissionAmounts[i].setscale(2) );
                    invoices[i].Ready_For_Journals__c = true;
                }
            }

            invoicesToBeInserted.addAll( oppIdToInvoices.get( oppId ) );
        }
        if( invoicesToBeInserted.isEmpty() )
        {
            return;
        }
        Map<Id, List<c2g__codaInvoice__c>> oppIdToInvoicesForJournals = new Map<Id, List<c2g__codaInvoice__c>>();

        Savepoint sp = Database.setSavepoint();
        try
        {
            insert invoicesToBeInserted;
        }
        catch ( DmlException ex )
        {
            for ( Integer index = 0 ; index < ex.getNumDml() ; index++ )
            {
                new Map<Id, Opportunity>( newOpportunities ).get ( invoicesToBeInserted[ ex.getDmlIndex( index ) ].c2g__Opportunity__c ).addError( ex.getDmlMessage( index ) );
            }
            system.assert( false, ex );

            return;
        }

        /****************************** INVOICE CREATION PROCESS END *********************************/


        /****************************** INVOICE LINE ITEM CREATION PROCESS ***************************/

        List<c2g__codaInvoiceLineItem__c> invoiceLineItemsToBeInserted = new List<c2g__codaInvoiceLineItem__c>();
        Map<Id, List<OpportunityLineItem>> oppIdToOLIs = getOpportunityLineItems( filteredOpps );

        for(Opportunity filterOpp : filteredOpps )
        {
            List<c2g__codaInvoice__c> invoices = oppIdToInvoices.get( filterOpp.Id );
            Decimal oppExRate = invoices[0].Opportunity_Exchange_Rate__c;
            Decimal partnerExRate = invoices[0].Partner_Exchange_Rate__c;
            invoiceLineItemsToBeInserted.addAll( InvoiceLineItemCreationService.getInvoiceLineItemFromOpp( filterOpp , oppIdToOLIs.get( filterOpp.Id ), invoices, oppExRate, partnerExRate ) );
        }
        try
        {
            insert invoiceLineItemsToBeInserted;
        }
        catch ( DmlException ex )
        {
            for ( Integer index = 0 ; index < ex.getNumDml() ; index++ )
            {
                c2g__codaInvoice__c relatedInvoice = new Map<Id, c2g__codaInvoice__c>( invoicesToBeInserted ).get( invoiceLineItemsToBeInserted[ ex.getDmlIndex( index ) ].c2g__Invoice__c );
                new Map<Id, Opportunity>( newOpportunities ).get ( relatedInvoice.c2g__Opportunity__c ).addError( ex.getDmlMessage( index ) );
            }
            Database.rollBack( sp );
            system.assert( false, ex );

            return;
        }
        /****************************** INVOICE LINE ITEM CREATION PROCESS END ***************************/

        /****************************** JOURNAL CREATION PROCESS *****************************************/

        for( Opportunity anOpp : filteredOpps )
        {
            anOpp.Invoice_Created__c = true;
        }

        if( !oppsNeedingJournals.isEmpty() )
            Database.executeBatch( new BatchToCreateJournals(), 1 );
    }

    public static List<Opportunity> filterOpportunitiesReadyForInvoice( List<Opportunity> newOpportunities, Map<Id, Opportunity> oldOpportunities )
    {
        List<Opportunity> filteredOpps = new List<Opportunity>();
        for(Opportunity newOpp : newOpportunities)
        {
            Boolean isStageChangedToFinanceApproved = ( newOpp.StageName != oldOpportunities.get(newOpp.Id).StageName && newOpp.StageName == FINANCE_APPROVED );
            Boolean isInvoiceCreatedChangedToUnChecked = ( newOpp.Invoice_Created__c != oldOpportunities.get(newOpp.Id).Invoice_Created__c && !newOpp.Invoice_Created__c );

            if( ( isStageChangedToFinanceApproved && newOpp.Invoice_Created__c == false ) || ( isInvoiceCreatedChangedToUnChecked && newOpp.StageName == FINANCE_APPROVED ) )
            {
                if( BILLING_TYPES.contains(newOpp.Billing_Type__c) &&
                    TYPES.contains(newOpp.type)  && newOpp.Minimum_Start_Date__c != null && newOpp.FF_Company_Formula__c == COMPANY_NAME_FORMULA &&
                    newOpp.Bypass_Financial_Force__c == false )
                {
                    filteredOpps.add(newOpp);
                }
            }
        }
        return filteredOpps;
    }

    private static List<Opportunity> getPartnerOpportunities( List<Opportunity> opportunities )
    {
        List<Opportunity> filteredOpps = new List<Opportunity>();
        for(Opportunity newOpp : opportunities )
        {
            if( newOpp.PartnerLookup__c != null && newOpp.Who_is_Invoiced__c == PARTNER_INVOICED )
            {
                filteredOpps.add( newOpp );
            }
        }
        return filteredOpps;
    }

    public static Map<Id, Account> getPartners( List<Opportunity> opportunities )
    {
        Map<Id, Account> oppIdToPartner = new Map<Id, Account>();
        Set<Id> partnerIds = Pluck.ids( 'PartnerLookup__c', opportunities );
        if( partnerIds == NULL || partnerIds.isEmpty() )
        {
            return oppIdToPartner;
        }
        Map<Id, Account> partners = new Map<Id, Account>( [ SELECT Id, CurrencyISOCode FROM Account WHERE Id IN :partnerIds  ] );
        for( Opportunity anOpp : opportunities )
        {
            oppIdToPartner.put( anOpp.Id, partners.get( anOpp.PartnerLookup__c ) );
        }
        return oppIdToPartner;
    }


    public static Map<Id, List<OpportunityLineItem>> getOpportunityLineItems( List<Opportunity> opportunities )
    {
        List<OpportunityLineItem> olis = new List<OpportunityLineItem>();
        LIST<Opportunity> oppsToBeGrouped = new List<Opportunity>();
        LIST<Opportunity> oppsNoWhoIsInvoiced = new List<Opportunity>();

        for( Opportunity anOpp : opportunities )
        {
            if( String.isBlank( anOpp.Who_is_Invoiced__c ) )
            {
                oppsNoWhoIsInvoiced.add( anOpp );
            }
            else
            {
                oppsToBeGrouped.add( anOpp );
            }
        }
        Map<String, List<Opportunity>> oppsGroupedByWhoInvoiced = GroupBy.Strings( 'Who_is_Invoiced__c', oppsToBeGrouped );

        List<Opportunity> nonCustomerInvoicedOpps = new List<Opportunity>();
        nonCustomerInvoicedOpps.addAll( oppsNoWhoIsInvoiced );
        for( String whoInvoiced : oppsGroupedByWhoInvoiced.keySEt() )
        {
            if( whoInvoiced != CUSTOMER_INVOICED )
            {
                nonCustomerInvoicedOpps.addAll( oppsGroupedByWhoInvoiced.get( whoInvoiced ) );
            }
        }
        if( !nonCustomerInvoicedOpps.isEmpty() )
        {
            List<OpportunityLineItem> olis_nonCustomerInvoiced = [ SELECT Id, OpportunityId, CurrencyIsoCode, Description, Description__c, Discount, Start_Date__c, End_Date__c,
                                                                Number_of_Journals_Calc__c, UnitPrice, Start_Date_on_the_First__c, PricebookEntry.Product2Id, Quantity, PricebookEntry.Product2.ProductCode
                                                                FROM OpportunityLineItem
                                                                WHERE OpportunityId IN :nonCustomerInvoicedOpps
                                                                AND Mark_for_Deletion__c = false ];
            olis.addAll( olis_nonCustomerInvoiced );

        }

        List<Opportunity> customerInvoicedOpps = oppsGroupedByWhoInvoiced.get( CUSTOMER_INVOICED );
        if( customerInvoicedOpps != NULL && !customerInvoicedOpps.isEmpty() )
        {
            List<OpportunityLineItem> olis_customerInvoiced = [ SELECT Id, OpportunityId, CurrencyIsoCode, Description, Description__c, Discount, Start_Date__c, End_Date__c,
                                                                Number_of_Journals_Calc__c, UnitPrice, Start_Date_on_the_First__c, PricebookEntry.Product2Id, Quantity, PricebookEntry.Product2.ProductCode
                                                                FROM OpportunityLineItem
                                                                WHERE PricebookEntry.Product2.ProductCode != :PRODUCT_CODE_REFERRAL
                                                                AND PricebookEntry.Product2.ProductCode != :PRODUCT_CODE_RESELLER
                                                                AND OpportunityId IN :customerInvoicedOpps
                                                                AND Mark_for_Deletion__c = false ];
            olis.addAll( olis_customerInvoiced );

        }

        Map<Id, List<OpportunityLineItem>> oppIdToOLIs = GroupBy.ids( 'OpportunityId', olis );
        return oppIdToOLIs;
    }

    private static Map<Id, Opportunity> getPartnerInvoicedOpportunities( List<Opportunity> opportunities, Map<Id, Account> oppIdToPartner )
    {
        Map<Id, Opportunity> partnerInvoicedOpportunities = new Map<Id, Opportunity>();
        for( Opportunity o : opportunities )
        {
            if(o.Who_is_Invoiced__c == PARTNER_INVOICED && o.PartnerLookup__c != NULL && o.CurrencyISOCode != oppIdToPartner.get( o.Id ).CurrencyISOCode )
            {
                partnerInvoicedOpportunities.put( o.Id, o );
            }
        }
        return partnerInvoicedOpportunities;
    }


    private static List<Opportunity> filterOpportunitiesNeedingJournals( List<Opportunity> opportunities )
    {
        List<Opportunity> filteredOpps = new List<Opportunity>();
        for( Opportunity anOpp : opportunities )
        {
            if( anOpp.Billing_Type__c != MONTHLY && ( anOpp.Who_is_Invoiced__c == CUSTOMER_INVOICED || anOpp.Who_is_Invoiced__c == PARTNER_INVOICED ) &&
                ( anOpp.Deal_Closing_Through__c == DEAL_CLOSING_THROUGH_RP ) )
            {
                filteredOpps.add( anOpp );
            }
        }
        return filteredOpps;
    }

    private static Map<Id, OpportunityLineItem> getReferralOLI( Set<Id> opportunityIds )
    {
        Map<Id, OpportunityLineItem> oppIdToReferralOLI = new Map<Id, OpportunityLineItem>();
        for( OpportunityLineItem anOLI : [ SELECT Id, OpportunityId, CurrencyISOCode, Description, Description__c, Discount, Start_Date__c, End_Date__c,
                                            Number_of_Journals_Calc__c, UnitPrice, Start_Date_on_the_First__c, PricebookEntry.Product2Id, Quantity, PricebookEntry.Product2.ProductCode
                                            FROM OpportunityLineItem WHERE OpportunityId IN :opportunityIds
                                            AND PricebookEntry.Product2.ProductCode = :PRODUCT_CODE_REFERRAL
                                            AND Mark_for_Deletion__c = false ] )

        {
            oppIdToReferralOLI.put( anOLI.OpportunityId, anOLI );
        }
        return oppIdToReferralOLI;
    }

    public static void sendEmail(String templateName, Id oId)
    {
        List<OpportunityLineItem> olis = [ SELECT Id, PricebookEntry.Product2.ProductCode FROM OpportunityLineItem WHERE PricebookEntry.Product2.ProductCode = '007-BLUE-WLF' AND OpportunityId = :oId];
    	if(olis.isEmpty()) return;

        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
		List<GroupMember> members = [ SELECT Id, UserOrGroupId FROM GroupMember WHERE Group.Name = 'Inbox Insight for Internal Use Notificat' ];

		if(members.isEmpty()) return;

		List<User> users = [ SELECT Id, Email FROM User WHERE Id IN :Pluck.Ids('UserorGroupId', members) ];

		if(users.isEmpty()) return;

        List<String> toAddresses = new List<String>(Pluck.Strings('Email', users));
        mail.setToAddresses(toAddresses);
		List<EmailTemplate> template = [Select e.DeveloperName From EmailTemplate e where DeveloperName = :templateName];

		if(template.isEmpty()) return;

        mail.setTemplateId(template[0].Id);
		mail.setTargetObjectId(users[0].Id);
		mail.setWhatId(oId);
		mail.setSaveAsActivity(false);
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
    }
    
    /********************************************Method Called in Schedule_AutoSubmitCertApps Class********************************************/
    
    public static void autoSubmitCertApps(){
        date d = system.today().addDays(30);
		list<Opportunity> scope = [SELECT Id FROM Opportunity WHERE Auto_Submit_Cert_Only__c ='Yes' AND Auto_Submit_WFR_Fired__c = FALSE AND CloseDate <=: d AND CloseDate>=: System.today()];
		system.debug(scope);				
		list<Opportunity> oppsToUpdate = new list<Opportunity>();
		
        for (Opportunity opp : scope){
            
            opp.Submit_From_Trigger__c = 'Yes';
            opp.Auto_Submit_WFR_Fired__c = True;
            
            oppsToUpdate.add(opp);
                    
        }
        
        update oppsToUpdate;
    }

}