trigger ownerCopy on Opportunity (before update, before insert) {

    //handle arbitrary number of opps
    List<Opportunity> ownerChangeOpps = new List<Opportunity>();
    for(Opportunity x : Trigger.new)
    {
        if(trigger.isUpdate)
        {
            Opportunity oldOpp = trigger.oldMap.get(x.Id);
            if((oldOpp.OwnerId != x.OwnerId || (oldOpp.Reset_1st_and_2nd_Approver_Fields__c == false && x.Reset_1st_and_2nd_Approver_Fields__c == true) || x.Owner_Copy__c == null) && x.isClosed == false)
            {
                ownerChangeOpps.add(x);
            }
            
        }
        if(trigger.isInsert)
        {
            ownerChangeOpps.add(x);
        }
    }
    if(!ownerChangeOpps.isEmpty())
    {
        
        Set<Id> OwnerIds = pluck.ids('OwnerId', ownerChangeOpps);
        Map<Id, User> OwnerMap = new Map<Id, User>([SELECT Id, First_Approver__c, Second_Approver__c, User_Pod_Number__c FROM User WHERE Id IN: OwnerIds]);
        
        
        for(Opportunity x : ownerChangeOpps) {
            
        

        // check that owner is a user (not a queue)
        if( ((String)x.ownerId).substring (0,3)=='005' ) {
            x.Owner_Copy__c = x.ownerId;
            x.First_Approver__c = OwnerMap.get(x.OwnerId).First_Approver__c;
            x.Second_Approver__c = OwnerMap.get(x.OwnerId).Second_Approver__c;
            x.POD__c = OwnerMap.get(x.OwnerId).User_Pod_Number__c;
            x.Reset_1st_and_2nd_Approver_Fields__c = false;
         }
         else{
             // in case of Queue we clear out our copy field
             x.Owner_Copy__c = null;
             }
       }
    }
 }