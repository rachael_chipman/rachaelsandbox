@ isTest (seealldata = true)
private class PartnerInvoiceCreate_Test {
private static testmethod void testPartnerInvoiceCreate(){

  String CRON_EXP = '0 0 * * * ?';   

 
// Schedule the test job

      String jobId = System.schedule('testBasicScheduledApex',
      CRON_EXP, 
         new PartnerInvoiceCreate());
   // Get the information from the CronTrigger API object

      CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, 
         NextFireTime
         FROM CronTrigger WHERE id = :jobId];

   // Verify the expressions are the same
      System.assertEquals(CRON_EXP, 
         ct.CronExpression);

   // Verify the job has not run
      System.assertEquals(0, ct.TimesTriggered);

   // Verify the next time the job will run
    



    
}}