public with sharing class JournalSalesLineItemService {

	public static void populateSalesInvoiceLineItems(List<c2g__codaJournal__c> newJournals){

		Map<Id,List<c2g__codaJournal__c>> groupIdJournalMap = new Map<Id,List<c2g__codaJournal__c>>(); 
		Set<Id> groupIds = new Set<ID>();
		for (c2g__codaJournal__c newJournal : newJournals ){
			groupIds.add(newJournal.c2g__IncomeScheduleGroup__c);
			if (newJournal.c2g__IncomeScheduleGroup__c != null ){
				if (!groupIdJournalMap.containsKey(newJournal.c2g__IncomeScheduleGroup__c)){
					groupIdJournalMap.put(newJournal.c2g__IncomeScheduleGroup__c,new List<c2g__codaJournal__c>());
				}
				groupIdJournalMap.get(newJournal.c2g__IncomeScheduleGroup__c).add(newJournal);
			}
		}

		List<c2g__codaInvoiceLineItem__c> lineItems = [Select Id, Name, c2g__IncomeScheduleGroup__c,c2g__Invoice__r.c2g__Opportunity__c From  c2g__codaInvoiceLineItem__c where c2g__IncomeScheduleGroup__c in : groupIds ]; 
		for(c2g__codaInvoiceLineItem__c lineItem :lineItems){
			if (lineItem.c2g__Invoice__r.c2g__Opportunity__c != null ){
				List<c2g__codaJournal__c> journals = groupIdJournalMap.get(lineitem.c2g__IncomeScheduleGroup__c);
				if (journals != null){
					for (c2g__codaJournal__c journal: journals){
						journal.Opportunity__c = lineItem.c2g__Invoice__r.c2g__Opportunity__c;
					}
				}
			}
		}
	}	

}