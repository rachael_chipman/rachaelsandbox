@isTest

public class AttachFinalController_Test
{

static Opportunity testOpp;
static Contract testCon;
static Attachment attachment;

    static void setupObjects()
    {

    Account a = new Account (Name = 'Test Account');
    insert a;
    
    testOpp = new Opportunity (
    AccountId = a.Id, 
    Name = 'TestOpp', 
    CloseDate = system.today()+30, 
    Type = 'New Business',
    StageName = 'Signature',
    Pricebook2Id = '01s000000004NS6AAM',
    CurrencyISOCode = 'USD',
    Approval_Type__c = 'Finance Approval',
    Final_Contract_Uploaded__c = false);
    
    testCon = new Contract (AccountId = a.Id);
    
    attachment = new Attachment();
    String myString = 'StringToBlob';
    Blob myBlob = Blob.valueof(myString);
    attachment.body = myBlob;

    }

    static testMethod void AttachFinalContract()
    {

    setupObjects();
    insert testOpp;
    testCon.Opportunity__c = testOpp.Id;
    insert testCon;
    
    attachment = new Attachment();
    String myString = 'StringToBlob';
    Blob myBlob = Blob.valueof(myString);
    attachment.body = myBlob;
    


    ApexPages.StandardController sc = new ApexPages.standardController(testCon);
    System.currentPagereference().getParameters().put('id',testCon.Id);
    AttachFinalController methods = new AttachFinalController(sc);
    
    
        
    test.startTest();
    methods.attachment.body = myBlob;
    methods.uploadFinal();
    test.stopTest();
    
     list<Attachment> attachments = [SELECT Id, Name, ParentId FROM Attachment WHERE Name = 'ContractFinal.pdf' and ParentId = :testOpp.Id];
     system.assert(attachments.size()== 1);
     list<Opportunity> opps = [SELECT Id, Final_Contract_Uploaded__c FROM Opportunity WHERE Id = :testOpp.Id and Final_Contract_Uploaded__c = true];
     system.assert(opps.size() == 1);
     
    }
}