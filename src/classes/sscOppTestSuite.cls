public class sscOppTestSuite {
  static testMethod void runPositiveTestCases() {
    //Setup Account
    account a = new Account();
    a.name = 'test insert';
    insert a;

    //Setup User
    User u1 = [select id from User where alias='Daniella'];
    //Run As U1
    //System.RunAs(u1){
      Opportunity o = new Opportunity();
      o.closedate = system.today();
      o.Type = 'New Business';
      o.Name = 'trigger test';
      o.DAS_Service_Type__c = 'Accreditation Only';
      o.RecordTypeId = '0120000000000o9AAA';
      o.AccountId = a.Id;
      o.StageName = 'Receptivity';
      o.OwnerId = u1.Id;
      
      system.debug('What is o?'+o);
      insert o; 
      List<Opportunity> insertedOpps = [SELECT Id FROM Opportunity WHERE Id = :o.Id];
      system.debug('Did the opp get inserted'+insertedOpps);
    //}
    /*integer theNewOpp = [select count() from Opportunity where Name = 'trigger test' limit 50];
        system.debug('What is theNewOpp'+theNewOpp);
        if (theNewOpp > 0)
        {
            System.assert(true);
        }
        else
        {
            System.assert(false);  //false means the opp has not been created
        } */
  }
}