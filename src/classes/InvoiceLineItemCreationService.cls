public with sharing class InvoiceLineItemCreationService 
{
    public static final Integer DAYS_IN_MONTH=30;

    public static List<c2g__codaInvoiceLineItem__c> getInvoiceLineItemFromOpp( Opportunity relatedOpp, List<OpportunityLineItem> olis, List<c2g__codaInvoice__c> invoices, Decimal oppExRate, Decimal partnerExRate )
    {
        List<c2g__codaInvoiceLineItem__c> invoiceLineItems = new List<c2g__codaInvoiceLineItem__c>();
        for( OpportunityLineItem anOLI : olis )
        {
            List<Decimal> prices = getInvoiceLineItemPrices( anOLI, relatedOpp.Billing_Type__c, oppExRate, partnerExRate );
            invoiceLineItems.addAll( createInvoiceLineItems( relatedOpp, invoices, prices, anOLI ) );
        }
        return invoiceLineItems;
    }

    public static List<Decimal> getInvoiceLineItemPrices( OpportunityLineItem oli, String billingType, Decimal oppExRate, Decimal partnerExRate )
    {
        List<Decimal> prices = new List<Decimal>();
        Integer monthInterval = OpportunityServices.BILLING_INTERVAL_MAP.get( billingType );// num of months in a period
        Integer totalInterval = OpportunityServices.BILLING_TYPE_MULTIPLES.get( billingType ); // num of  invoices in billing type
        Decimal unitPrice = ( oli.UnitPrice/oppExRate ) * partnerExRate;
        
        if( oli.Number_of_Journals_Calc__c == 12 )
        {
            for( Integer i = 0; i < totalInterval; i++ )
            {
                prices.add( ( unitPrice/totalInterval ).setscale( 2 ) );
            }
            return prices;
        }

        Date startDate = oli.Start_Date__c;
        Date endDate = oli.End_Date__c;
        Decimal pricePerMonth = ( unitPrice/(startDate.daysBetween(endDate)+1) ) * DAYS_IN_MONTH;

        if( endDate <= startDate.AddMonths( monthInterval ) )
        {
            prices.add( unitPrice );
            return prices;
        }

        Decimal curTotalPrice=0.0;
        for( Integer i = 0 ;  i < totalInterval - 1; i++ )
        { 
            Decimal price;
            Date invoiceDate = startDate.AddMonths( i * monthInterval ); 
            Integer nextInterval = ( i + 1 ) * monthInterval;

            if( endDate >  invoiceDate  && endDate > startDate.AddMonths( nextInterval ) )
            {
                price = monthInterval * pricePerMonth; 
                curTotalPrice += price;
                prices.add( price.setScale(2) );
            }
             else
             {
                break;
             } 
                
        }

        Decimal lastInvoiceLineItemPrice = ( unitPrice - curTotalPrice).setScale(2); 
        prices.add( lastInvoiceLineItemPrice );



        return prices;
    }

    public static List<c2g__codaInvoiceLineItem__c> createInvoiceLineItems( Opportunity relatedOpp, List<c2g__codaInvoice__c> invoices, List<Decimal> prices, OpportunityLineItem oli )
    {
        List<c2g__codaInvoiceLineItem__c> invoiceLineItems = new List<c2g__codaInvoiceLineItem__c>();
        Integer monthInterval = OpportunityServices.BILLING_INTERVAL_MAP.get( relatedOpp.Billing_Type__c );
        Integer numberOfJournals = monthInterval;
        for( Integer i = 0; i < prices.size(); i++ )
        {
            if( prices[i] == NULL )
                continue;

            c2g__codaInvoiceLineItem__c sili = new c2g__codaInvoiceLineItem__c
            (
                c2g__Invoice__c = invoices[i].Id,
                c2g__Product__c = oli.PricebookEntry.Product2Id,
                c2g__Quantity__c = oli.Quantity,
                c2g__UnitPrice__c = prices[i].setscale(2),
                Opportunity__c = oli.opportunityId,
                c2g__StartDate__c = oli.Start_Date__c.addMonths( i * monthInterval ),
                c2g__NumberofJournals__c = numberOfJournals,
                c2g__LineDescription__c = oli.Description,
                ffbilling__DeriveUnitPriceFromProduct__c = false,
                c2g__IncomeSchedule__c = OpportunityServices.INCOME_SCHEDULE_DEFINITION.Id ,
                ffbilling__CalculateIncomeSchedule__c = true
            );

            Integer nextInterval = ( i + 1 ) * monthInterval;
            if( oli.End_Date__c <= oli.start_Date__c.addmonths( nextInterval ) ) 
            { 
                sili.End_Date__c = oli.End_Date__c;
                if( relatedOpp.Billing_Type__c  != OpportunityServices.MONTHLY )
                    sili.c2g__NumberofJournals__c = oli.Number_of_Journals_Calc__c - ( i * monthInterval );

            }
            else
            { 
                sili.End_Date__c = oli.start_Date__c.addmonths( nextInterval ).addDays(-1);
            }

            if( relatedOpp.Who_is_Invoiced__c == OpportunityServices.PARTNER_INVOICED && relatedOpp.PartnerLookup__c != NULL && relatedOpp.Deal_Closing_Through__c == OpportunityServices.DEAL_CLOSING_THROUGH_RP && oli.PricebookEntry.Product2.ProductCode == OpportunityServices.PRODUCT_CODE_REFERRAL )
            {
                sili.c2g__IncomeSchedule__c = NULL;
                sili.ffbilling__CalculateIncomeSchedule__c = false;
                
                c2g__codaInvoiceLineItem__c sili_GrossUp = sili.clone( false, true );
                sili_GrossUp.c2g__UnitPrice__c = Math.abs( sili.c2g__UnitPrice__c );
                sili_GrossUp.AR_Gross_Up_Item__c = true;
                invoiceLineItems.add( sili_GrossUp );
            }
            if( relatedOpp.Billing_Type__c == OpportunityServices.MONTHLY )
            {
                sili.ffbilling__CalculateIncomeSchedule__c = false;
            }
            invoiceLineItems.add( sili );
        }
        return invoiceLineItems;
    }

}