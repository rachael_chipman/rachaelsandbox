public class Opportunity_Disqualified_Lost {
	
    public Opportunity Opp{get;set;}
    public Id ReadOnlyRecType {get;set;}
    private PageReference pageref;
    public Boolean displayPopup{get;set;}
    
    public Opportunity_Disqualified_Lost(ApexPages.StandardController stdController)
    {
        Id oppId = (Id) stdController.getId();
        Opp = [Select RecordTypeId, Name,Type, Disqualified_Sub_Status__c,Lost_Pass_Specifics__c,StageName,Reasons_for_Lost_Opportunities__c,Lost_Pass_Details__c from Opportunity where Id =:oppId];
        pageref = stdController.view();
        getRecordtype('Disqualified_Lost');
        
    }
    
    public PageReference OppLost()
    {
        //update the stagename to Lost
        Opp.StageName = 'Lost';
        Opp.RecordTypeId = ReadOnlyRecType;
        
        try
        {
            update Opp;
        }
        catch(Exception ex)
        {
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.FATAL, ex.getMessage());
            ApexPages.addMessage(myMsg);  
        }
        
        pageref = new pagereference('/'+Opp.id);
        return pageref;
    }
    
    public PageReference Submit()
    {
        
        List<Opportunity> Opplist = new List<Opportunity>();
        Opp.StageName = 'Disqualified';
        //get the Id for the disqualified record type
		getRecordtype('Disqualified_Lost');
        Opp.RecordTypeId = ReadOnlyRecType;
        Opplist.add(Opp);
        //update the Contact
       	updateContact();
       
        //update opportunity
        try
        { 
        	update Opplist;
        }
        catch(Exception e)
        {
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.FATAL, e.getMessage());
            ApexPages.addMessage(myMsg);  
        }
        return pageref;
    }
    
    public void getRecordType(string recordtypeName)
    {
      
        List<RecordType> rectype = [Select Id,Name from RecordType where Name =:recordtypeName AND SObjectType ='Opportunity'];
        if(rectype!= null)
           ReadOnlyRecType = rectype[0].Id;
       
    }
    public void updateContact()
    {
        List<Contact> contactstoUpdate = new List<Contact>();
        List<Id> ContactId = new List<Id>();
        List<OpportunityContactRole> conroles = [Select Id,ContactId from OpportunityContactRole where OpportunityId =:Opp.Id];
        if(conroles.size() > 0)
        {
            for(OpportunityContactRole Tconrole : conroles)
            {
                ContactId.add(Tconrole.ContactId);
            }
            List<Contact> contacts = [Select Id,name, Qualifying_Status__c from Contact where Id in:ContactId  ];
            if(contacts.size() > 0)
            {
                for(Contact tcon: contacts)
                {
                  // tcon.Qualifying_Status__c =  getPicklistValue(tcon, 'Qualifying_Status__c', 'RSE DQ');
                   tcon.Qualifying_Status__c = 'RSE DQ';
                   tcon.Sub_Status__c = Opp.Disqualified_Sub_Status__c;
                   contactstoUpdate.add(tcon);
                }
            }
            
        }
       
        try
        {
        	update contactstoUpdate;
        }
        catch(Exception e)
        {
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.FATAL, e.getMessage());
            ApexPages.addMessage(myMsg);  
        }
    }
    
   /* public string getPicklistValue(Sobject ObjName,String fieldName, string selectedValue)
   {
      list<SelectOption> options = new list<SelectOption>();
      string selectedStatus;
      // Get the object type of the SObject.
      Schema.sObjectType objType = ObjName.getSObjectType(); 
      // Describe the SObject using its object type.
      Schema.DescribeSObjectResult objDescribe = objType.getDescribe();       
      // Get a map of fields for the SObject
      map<String, Schema.SObjectField> fieldMap = objDescribe.fields.getMap(); 
      // Get the list of picklist values for this field.
      list<Schema.PicklistEntry> values =
         fieldMap.get(fieldName).getDescribe().getPickListValues();
      // Add these values to the selectoption list.
      for (Schema.PicklistEntry a : values)
      { 
          if(a.getLabel().equals(selectedValue))
          {
              selectedStatus = a.getValue();
              break;
          }
        
      }
      return selectedStatus;
   }*/
    
    
    
}