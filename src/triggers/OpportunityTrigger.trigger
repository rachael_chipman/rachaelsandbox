trigger OpportunityTrigger on Opportunity (after update, before update ,after insert) {
   
   system.debug('Show trigger.old Opp Trigger ' + trigger.old);
   system.debug('Show trigger.new Opp Trigger ' + trigger.new);
   List<Opportunity> oppsToPassToRenewalSvc = new List<Opportunity>();
   if(trigger.isAfter && trigger.isInsert)
    {
        PrimaryCampaignPush.processTrigger(trigger.new, trigger.oldMap);
        
       	AddCRMToOppTeam.processTrigger(trigger.new, trigger.oldMap);
       	
        OTMCommInsertService OTMSvc = new OTMCommInsertService();
        OTMSvc.processTrigger(trigger.new);
        
       	OTMExchangeRateService.processTeamMembers(trigger.new, trigger.oldMap, trigger.newMap);
        NamingConventionService.filterCloseDateOpp(trigger.new,trigger.oldmap);
    }
    
    if(trigger.isAfter && trigger.isUpdate)
    {      
        NamingConventionService.filterCloseDateOpp(trigger.new,trigger.oldmap);  
        PrimaryCampaignPush.processTrigger(trigger.new, trigger.oldMap);
        
        AddCRMToOppTeam.processTrigger(trigger.new, trigger.oldMap);
        
        OTMCommUpdateService OTMSvc = new OTMCommUpdateService();
        OTMSvc.processTrigger(trigger.new, trigger.oldMap);
        
       	OTMExchangeRateService.processTeamMembers(trigger.new, trigger.oldMap, trigger.newMap);
        
        PartnerCreateService PartnerSvc = new PartnerCreateService();
        PartnerSvc.processTrigger(trigger.new, trigger.oldMap); 
        
        CloneforPendingCertService PendingCertSvc = new CloneforPendingCertService();
        PendingCertSvc.processTrigger(trigger.new, trigger.oldMap);
        
        
        for(Opportunity opp : trigger.new)
        {
            Opportunity oldOpp = trigger.oldMap.get(opp.Id);
            system.debug('Show all criteria: oldOpp.Stage = ' + oldOpp.StageName + 'newOpp.Stage = ' + opp.StageName + 'line items: ' + opp.of_Recurring_Product_Line_Items__c + 'Renewal Created: ' + opp.Renewal_Created_del__c);
            if(opp.StageName == 'Finance Approved' && 
                opp.of_Recurring_Product_Line_Items__c > 0 &&
                ((!opp.Renewal_Created_del__c && oldOpp.StageName != 'Finance Approved') || 
                opp.Generate_Renewal_Opportunity__c))
            {
                oppsToPassToRenewalSvc.add(opp);
            }
        }
        if(!oppsToPassToRenewalSvc.isEmpty())
        {
            System.debug('Calling the renewal Service');
            CreateRenewalService.generateRenewalsWithOlis(oppsToPassToRenewalSvc);
        }


        /*CreateRenewalService RenewalSvc= new CreateRenewalService();
        RenewalSvc.processTrigger(trigger.new, trigger.oldMap);*/
         
        /*CreateCustomerProductDetailService CPDSvc= new CreateCustomerProductDetailService();
        CPDSvc.processTrigger(trigger.new, trigger.oldMap);
        
        ProServACVService PSSvc = new ProServACVService();
        PSSvc.processTrigger(trigger.new, trigger.oldMap);*/
        
        //UpdateRenewalCertOLI.processTrigger(trigger.new, trigger.oldMap);
        
        
    }
    
    if(trigger.isBefore && trigger.isUpdate)
    {  
        OwnerCopyPodApprovers.processTrigger(trigger.new, trigger.oldMap);
        
        GenRefOppOwnerChange.processTrigger(trigger.new, trigger.OldMap);
        
        AddCRMToOppTeam.processTrigger(trigger.new, trigger.oldMap);
        
        
        CreateRealmIdService RealmSvc = new CreateRealmIdService();
        RealmSvc.processTrigger(trigger.new, trigger.oldMap);
        
        CloneforPendingCertService.processOlisToBeDeleted(trigger.new, trigger.oldMap);
        
        //Call to Zuora to create subscription when a cert Opp is FA
        //ZuoraService.createZuoraSubscription(trigger.newmap);
        
        /*if( OpportunityServices.isInvoiceTriggerRunning == false )
            OpportunityServices.processToCreateInvoiceAndLineItems(trigger.new, trigger.oldmap);*/
    }
    
    if(trigger.isBefore && trigger.isInsert)
    { 
        OwnerCopyPodApprovers.processTrigger(trigger.new, trigger.oldMap);
    }
    
    
    
        
}