@isTest
public class SubmitFromTriggerCaseTest {
    static testMethod void runSubmitFromTrigCase() {

         //Run as BIO Automation
        User BIO = new User(id = '005000000074sVoAAI');
        System.RunAs(BIO){
        
        Account acct = new Account();
        acct.name = 'RPTestAcct';
        insert acct;
        
        Account acct1 = new Account();
        
        List<Account> acctId = [select Id from Account where Name = 'RPTestAcct' limit 1];
        for(Account newAcct : acctId){
            if(newAcct.Id <> null) acct1 = newAcct;
        }
        
        Contact contact1 = new Contact();
        contact1.AccountId = acct1.id; 
        contact1.FirstName = 'RP'; 
        contact1.LastName = 'Test'; 
        insert contact1;
        
        Contact contact2 = new Contact();
        
        List<Contact> contactId = [select Id from Contact where FirstName = 'RP' and LastName = 'Test' limit 1];
        for(Contact newContact : contactId){
            if(newContact.Id <> null) contact2 = newContact;
        }
        
        Case case1 = new Case();
        case1.contactid = contact2.id;
        case1.Subject = 'Testing';
        case1.Description = 'Test, test, 1, 2, 3';
        case1.Effective_Date__c = Date.valueOf('2009-01-01');
        case1.RecordTypeId = [SELECT Id FROM RecordType WHERE SObjectType = 'Case' and developerName = 'Client_Services'].Id;
        insert case1;
        
        Case case2 = new Case();
        
        List<Case> caseId = [select Id from Case where Subject = 'Testing' limit 1];
        for(Case newCase : caseId){
            if(newCase.Id <> null) case2 = newCase;
        }
       
        case2.submit_from_trigger__c = 'Yes';
        update case2;


        integer theNewCase = [select count() from Case where Subject = 'Testing' and Status = 'Client Services Approved' limit 1];
        if(theNewCase > 0){
            System.Assert(true);
        }
        else{
            System.Assert(false);
        }
    }    

    }
    
}