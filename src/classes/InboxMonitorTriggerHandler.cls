public class InboxMonitorTriggerHandler 
{
   public static void processEvents(List<Mailbox_Monitor_Events__c> eventList)
   {
        Set<Id> accountIds = pluck.Ids('Account__c', eventList);
        Map<Id, Zuora__SubscriptionProductCharge__c> chargeMap = OverageFeeGenerationServices.getExistingSubscriptionCharge(accountIds, IMOverageOpportunityGenerationService.EIS_PRODS); 
        if(!chargeMap.isEmpty())
        {
            for(Mailbox_Monitor_Events__c event : eventList)
            {
                if(chargeMap.containsKey(event.Account__c))
                {
                    event.Inbox_Monitor_Limit__c = chargeMap.get(event.Account__c).IncludedUnits__c;
                }
            }
        }
   }
}