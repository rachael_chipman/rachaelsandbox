@IsTest

public class legalApprovalControllerTest {

	public static testMethod void testChangesYesAttachNo() {
		
		Account a = new Account(name = 'testAccount');
		insert a;
		Opportunity o = new Opportunity(name = 'testopp',amount = 100,stagename = 'Negotiating',closeDate = date.today(),kick_off_date__c = date.today(),accountid = a.id);	
		insert o;
		
		PageReference oppPage = new PageReference('/'+o.id);
		Test.setCurrentPage(oppPage);
		ApexPages.currentPage().getParameters().put('oid',o.id);
		legalApprovalController con = new legalApprovalController();
		
		PageReference legalPage = Page.legal;
		Test.setCurrentPage(legalPage);
		ApexPages.currentPage().getParameters().put('oid',o.id);
		
		con.choice = 'yes';
		con.c.Type_of_review__c = 'Test Value';
		con.c.Type_of_Legal_Agreement__c = 'Test Value';
		con.save();
		con.skipAttach();
		con.viewOpp();
	
	}
	
	private static testMethod void testChangesNoAttachNo() {
		
		Account a = new Account(name = 'testAccount');
		insert a;
		Opportunity o = new Opportunity(name = 'testopp',amount = 100,stagename = 'Negotiating',closeDate = date.today(),kick_off_date__c = date.today(),accountid = a.id);	
		insert o;
		
		PageReference oppPage = new PageReference('/'+o.id);
		Test.setCurrentPage(oppPage);
		ApexPages.currentPage().getParameters().put('oid',o.id);
		legalApprovalController con = new legalApprovalController();
		
		PageReference legalPage = Page.legal;
		Test.setCurrentPage(legalPage);
		ApexPages.currentPage().getParameters().put('oid',o.id);
		
		con.choice = 'no';
		con.save();
		con.skipAttach();
		con.viewOpp();
		
	}
	
	private static testMethod void testChangesNoAttachYes() {
	
		Account a = new Account(name = 'testAccount');
		insert a;
		Opportunity o = new Opportunity(name = 'testopp',amount = 100,stagename = 'Negotiating',closeDate = date.today(),kick_off_date__c = date.today(),accountid = a.id);	
		insert o;
		
		PageReference oppPage = new PageReference('/'+o.id);
		Test.setCurrentPage(oppPage);
		ApexPages.currentPage().getParameters().put('oid',o.id);
		legalApprovalController con = new legalApprovalController();
		
		PageReference legalPage = Page.legal;
		Test.setCurrentPage(legalPage);
		ApexPages.currentPage().getParameters().put('oid',o.id);
		
		con.choice = 'no';
		con.save();
		con.chooseAttach();	
		con.skipAttachMore();
		con.viewOpp();

	}
	
	private static testMethod void testChangesYesAttachYes() {
	
		Account a = new Account(name = 'testAccount');
		insert a;
		Opportunity o = new Opportunity(name = 'testopp',amount = 100,stagename = 'Negotiating',closeDate = date.today(),kick_off_date__c = date.today(),accountid = a.id);	
		insert o;
		
		PageReference oppPage = new PageReference('/'+o.id);
		Test.setCurrentPage(oppPage);
		ApexPages.currentPage().getParameters().put('oid',o.id);
		legalApprovalController con = new legalApprovalController();
		
		PageReference legalPage = Page.legal;
		Test.setCurrentPage(legalPage);
		ApexPages.currentPage().getParameters().put('oid',o.id);
		
		con.choice = 'yes';
		con.c.Type_of_review__c = 'Test Value';
		con.c.Type_of_Legal_Agreement__c = 'Test Value';
		con.save();
		con.chooseAttach();	
		con.skipAttachMore();
		con.viewOpp();

	}
	
}