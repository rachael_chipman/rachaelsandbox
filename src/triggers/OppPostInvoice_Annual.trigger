trigger OppPostInvoice_Annual on Opportunity (before Update) {

/*Date inDate1;

for(Opportunity o : Trigger.New){
    /*if( o.StageName == OpportunityServices.FINANCE_APPROVED && Trigger.oldMap.get( o.Id ).StageName == 'Random' )
    {
        System.assert( false, 'Stage Name: '+ o.StageName+ ' Invoice created'+ o.Invoice_Created__c + ' Billing Type '+o.Billing_Type__c
                + ' Partner Lookup '+ o.PartnerLookup__c + ' Type: '+o.Type + 'Min. Start Date '+o.Minimum_Start_Date__c +' Company '+o.FF_Company_Formula__c 
                + ' Expected Revenue ' +o.ExpectedRevenue + 'Bypass FF '+o.Bypass_Financial_Force__c + ' Begin Automation '+ o.Begin_Automation__c  );
    }*/
	// **Move this to filter method
    /*if(o.StageName == 'Finance Approved' && o.Invoice_Created__c == false && o.Billing_Type__c == 'Annual' && o.PartnerLookup__c == null
       && (o.Type=='New Business' || o.type== 'Renewal' || o.type== 'Supplement' || o.type== 'Extension' || o.type== 'Fee' ) && o.Minimum_Start_Date__c != null && o.FF_Company_Formula__c == 'Return Path, Inc.'
       && o.ExpectedRevenue > 0 && o.Bypass_Financial_Force__c == false && o.Begin_Automation__c == true){
    /*System.assert( false, 'In the trigger' );*/
    // **Move out of for loop 
    /*c2g__codaAccountingCurrency__c cur = [select Id, Name from c2g__codaAccountingCurrency__c where Name = :o.CurrencyIsoCode and c2g__OwnerCompany__c = 'a2I00000000CaqE'];
    c2g__codaIncomeScheduleDefinition__c incsched = [SELECT Id, Name FROM c2g__codaIncomeScheduleDefinition__c WHERE Name = 'Income Schedule' LIMIT 1];


    //decide which date to use as an invoice date based on if the min. start date is in the past or future
    if(system.Today() <= o.Minimum_Start_Date__c) inDate1=o.Minimum_Start_Date__c;
    if(system.Today() > o.Minimum_Start_Date__c) inDate1=system.Today();

    //Create 1 annual invoice
    c2g__codaInvoice__c invoice = new c2g__codaInvoice__c(
        c2g__OwnerCompany__c = 'a2I00000000CaqE',
        c2g__Account__c = o.AccountId,
        c2g__Opportunity__c = o.Id,
        Opportunity_Account__c = o.AccountId,
        ffbilling__CopyAccountValues__c = true,
        ffbilling__DeriveCurrency__c = false,
        c2g__InvoiceCurrency__c = cur.Id,
        c2g__InvoiceDate__c = inDate1,
        c2g__DueDate__c = inDate1 +Integer.valueof(o.Billing_Terms_Number_Calc__c),
        Contract_Service_Period_Start_Date__c = o.Minimum_Start_Date__c,
        Contract_Service_Period_End_Date__c = o.Minimum_Start_Date__c.AddYears(1).addDays(-1),
        ffbilling__DerivePeriod__c = true,
        ffbilling__DeriveDueDate__c = false,
        Invoicing_Contact__c = o.Account.Invoicing_Contact__c,
        c2g__InvoiceStatus__c = 'In Progress',
        c2g__PrintStatus__c = 'Not Printed' );


    insert invoice;

    //variable for Invoice Line Item
    List<c2g__codaInvoiceLineItem__c> silis = new List<c2g__codaInvoiceLineItem__c>();

    //Create an invoice line item for each opp line item
    for(OpportunityLineItem olis : [select Id, Start_Date__c, End_Date__c, Start_Date_on_the_First__c, Number_of_Journals_Calc__c, CurrencyIsoCode, Description, Description__c, Discount,
        UnitPrice, PricebookEntry.Product2Id, Quantity from OpportunityLineItem where OpportunityId = :o.Id]){


    Decimal price = olis.UnitPrice;

    c2g__codaInvoiceLineItem__c sili = new c2g__codaInvoiceLineItem__c(
        c2g__Invoice__c = invoice.Id,
        c2g__Product__c = olis.PricebookEntry.Product2Id,
        c2g__Quantity__c = olis.Quantity,
        c2g__UnitPrice__c = price.setscale(2),
        Opportunity__c = o.Id,
        c2g__StartDate__c = olis.Start_Date__c,
        End_Date__c = olis.End_Date__c,
        c2g__LineDescription__c = olis.Description,
        c2g__NumberofJournals__c = olis.Number_of_Journals_Calc__c,
        ffbilling__DeriveUnitPriceFromProduct__c = false,
        c2g__IncomeSchedule__c = incsched.Id);



        silis.add(sili);

}

        //insert invoice line items
        insert silis;


        //list for updating income schedule line items
        list<c2g__codaScheduleLineItem__c > schlis = new list<c2g__codaScheduleLineItem__c >();
        list<c2g__codaScheduleLineItem__c > schlistodelete = new list<c2g__codaScheduleLineItem__c >();

         c2g__codaPeriod__c period = [select Id from c2g__codaPeriod__c where Name = :o.Close_Date_Period__c limit 1];

        //for invoices approved after contract start date. Need to update revenue schedule
         for( c2g__codaScheduleLineItem__c schli : [select Id, Period_Month__c, c2g__Period__c, c2g__Period__r.Name, SILI_End_Date_Month__c, Period_Change_Needed__c, SILI_End_Date_Year__c,
                                                    c2g__Amount__c, Sales_Invoice_Line_Item_End_Date__c, Sales_Invoice_Line_Item_Start_Date__c, SILI_Start_Date_Month__c, Start_Date_Year__c,
                                                    c2g__SalesInvoiceLineItem__r.Price_Per_Day__c, c2g__Period__r.c2g__EndDate__c, c2g__Period__r.c2g__StartDate__c, c2g__LineNumber__c,
                                                    c2g__Period__r.Days_in_Period__c
                                                    from c2g__codaScheduleLineItem__c where c2g__SalesInvoiceLineItem__c in :silis ]){


            //for invoices approved after contract start date. Need to update revenue schedule
            String operiod = schli.c2g__Period__r.Name;

            if(o.Close_Date_Formula__c > 0 && schli.Period_Change_Needed__c == 'true' ){
            schli.c2g__Period__c = period.Id;
            schli.Original_Contract_Period__c = operiod;
            }

            if(schli.c2g__LineNumber__c == 1 && schli.Sales_Invoice_Line_Item_End_Date__c > schli.c2g__Period__r.c2g__EndDate__c){
            schli.c2g__Amount__c = ((schli.Sales_Invoice_Line_Item_Start_Date__c.daysbetween(schli.c2g__Period__r.c2g__EndDate__c)+1)*schli.c2g__SalesInvoiceLineItem__r.Price_Per_Day__c).setscale(2);}

            if(schli.c2g__LineNumber__c == 1 && schli.Sales_Invoice_Line_Item_End_Date__c < schli.c2g__Period__r.c2g__EndDate__c){
            schli.c2g__Amount__c = ((schli.Sales_Invoice_Line_Item_Start_Date__c.daysbetween(schli.Sales_Invoice_Line_Item_End_Date__c)+1)*schli.c2g__SalesInvoiceLineItem__r.Price_Per_Day__c).setscale(2);}

            if(schli.c2g__LineNumber__c > 1 && schli.Sales_Invoice_Line_Item_End_Date__c > schli.c2g__Period__r.c2g__EndDate__c){
            schli.c2g__Amount__c = (schli.c2g__Period__r.Days_in_Period__c*schli.c2g__SalesInvoiceLineItem__r.Price_Per_Day__c).setscale(2);}

            if(schli.c2g__LineNumber__c > 1 && schli.Sales_Invoice_Line_Item_End_Date__c < schli.c2g__Period__r.c2g__EndDate__c){
            schli.c2g__Amount__c = ((schli.c2g__Period__r.c2g__StartDate__c.daysbetween(schli.Sales_Invoice_Line_Item_End_Date__c)+1)*schli.c2g__SalesInvoiceLineItem__r.Price_Per_Day__c).setscale(2);}

            if(schli.SILI_Start_Date_Month__c != schli.Period_Month__c && schli.SILI_End_Date_Month__c < schli.Period_Month__c && schli.c2g__Period__r.c2g__StartDate__c > schli.Sales_Invoice_Line_Item_End_Date__c){
            schli.c2g__Amount__c = 0;}



         if(schli.c2g__Amount__c != 0){schlis.add(schli);}
         if(schli.c2g__Amount__c == 0){schlistodelete.add(schli);}



            }

            update schlis;
            delete schlistodelete;

        list<c2g__codaScheduleLineItem__c > updatesli = new list<c2g__codaScheduleLineItem__c >();

           for(c2g__codaInvoiceLineItem__c ili : silis){
           if(ili.ffbilling__ScheduleNetTotal__c != (ili.c2g__UnitPrice__c * ili.c2g__Quantity__c)){
           Decimal UP = ili.c2g__UnitPrice__c * ili.c2g__Quantity__c;

           AggregateResult[] groupedResults  = [select SUM(c2g__Amount__c) amt from c2g__codaScheduleLineItem__c where c2g__SalesInvoiceLineItem__c = :ili.Id];
           Decimal SNT;

           for (AggregateResult ar : groupedResults)  {
           SNT = (Decimal)ar.get('amt');
           }
           if (UP == null) UP = 0;
           if (SNT == null) SNT = 0;
           Decimal balance = UP - SNT;
           List<c2g__codaScheduleLineItem__c> lastsli = [select Id, c2g__Amount__c from c2g__codaScheduleLineItem__c where c2g__SalesInvoiceLineItem__c = :ili.Id order by c2g__LineNumber__c DESC Limit 1];
           if( !lastsli.isEmpty() )
           {
               Decimal curamt = lastsli[0].c2g__Amount__c;
               lastsli[0].c2g__Amount__c = curamt + balance;
               updatesli.add(lastsli[0]);
           }


           }
            }

            update updatesli;

o.Invoice_Created__c = true;
    }


}*/}