Public with sharing class DirectInvoiceController {

    Public List<c2g__codaInvoiceLineItem__c> silis;
    Public c2g__codaInvoice__c si {get;set;}
    Public Opportunity opp {get;set;}
    Public Date enddate {get;set;}
    Public Date startdate {get;set;}
    Public String s {get;set;}
    Public Boolean hideprodstrue {get;set;}
    Public Boolean hideprodsfalse {get;set;}
    Public String billtype {get;set;}
    Public String billterms {get;set;}
    
    public DirectInvoiceController(ApexPages.StandardController controller) {
       si= [select Id, c2g__Opportunity__c, c2g__InvoiceDate__c, Invoice_Total__c, c2g__Opportunity__r.Account.Name from c2g__codaInvoice__c where Id = :ApexPages.currentPage().getParameters().get('id')];
      
      
       opp = [select Id, AccountId, Billing_Type__c, PO_Number__c, Billing_Terms__c, Minimum_Start_Date__c, End_Date_Calc__c,  Hide_Product_Details_on_Invoice__c, Max_End_Date__c
                from Opportunity where Id = :si.c2g__Opportunity__c];
       
       startdate = opp.Minimum_Start_Date__c;
       enddate = opp.Max_End_Date__c;
       billtype = opp.Billing_Type__c;
       billterms = opp.Billing_Terms__c;
       
       s = 'Testing String';
       
       if(opp.Hide_Product_Details_on_Invoice__c == 'Yes'){hideprodstrue = true; hideprodsfalse = false;}
       if(opp.Hide_Product_Details_on_Invoice__c == 'No' || opp.Hide_Product_Details_on_Invoice__c == null){hideprodstrue = false; hideprodsfalse = true;}
       }
       
       
    
       
       Public List<c2g__codaInvoiceLineItem__c> getsilis(){
       
       if(hideprodstrue == true){
       silis = [select Id, c2g__Product__c, c2g__Quantity__c, c2g__UnitPrice__c, 
             c2g__Product__r.Description, c2g__Product__r.Name, Quantity_Text__c,
             Product_Line_Item_Description__c, c2g__NetValue__c, c2g__LineDescription__c from c2g__codaInvoiceLineItem__c where c2g__Invoice__c = :si.Id limit 1];}
       
       else{silis = [select Id, c2g__Product__c, c2g__Quantity__c, c2g__UnitPrice__c, 
             c2g__Product__r.Description, c2g__Product__r.Name, Quantity_Text__c,
             Product_Line_Item_Description__c, c2g__NetValue__c, c2g__LineDescription__c from c2g__codaInvoiceLineItem__c where c2g__Invoice__c = :si.Id AND AR_Gross_Up_Item__c = false ORDER BY c2g__NetValue__c DESC];}
             
       
      
       
       
       return silis;
    
    }

}