public class NuggetConvert_Controller {

     Public Nugget__c n;
     Public List<Nugget_Match__c> nm; 
     Public List<Nugget_Match__c> anm;
     Public Boolean accts {get;set;}
     Public Boolean leads {get;set;}
     Public List<Lead> leadmerge {get; set;}
     Public List<Account> acctmerge {get; set;}
     Public String toaddacctid {get; set;}
     Public Boolean nomatches {get; set;}
     Public Boolean usedInSalesCycle {get; set;}
     public String searchInput {get;set;}
     public List<AccountWrapper> searchAccounts {get;set;}
     public List<LeadWrapper> searchLeads {get;set;}

    public NuggetConvert_Controller(ApexPages.StandardController controller) {  
 
        n = [select Id, OwnerId, Account_Matches__c, Lead_Matches__c, Promoted_Closed__c, Used_in_Sales_Cycle__c, Promoted_Account__c, First_Opened_by_Sales_Rep__c, Pod_Calc__c, 
            Returned_to_RD__c, Company_Name__c, D_U_N_S_Number__c, City__c, State__c, Country__c from Nugget__c where Id = :ApexPages.currentPage().getParameters().get('Id')];
        
        if(n.Lead_Matches__c > 0){
            leads = true;
        }
        else{
            leads = false;
        }
 
        if(n.Account_Matches__c > 0){
            accts = true;}
        else{
            accts = false;
        }
 
        if(n.Lead_Matches__c == 0 && n.Account_Matches__c == 0){
            nomatches = true;
        }
        
        usedInSalesCycle = n.Used_in_Sales_Cycle__c;
    }
 
    public List<Nugget_Match__c> getnm(){
 
        nm = [select Id, Matching_Lead__r.Name, Matching_Lead__r.Company, Matching_Lead__r.State, Matching_Lead__r.Country, 
        Matching_Lead__r.Owner.Name, Merge_with_Nugget_Account__c from Nugget_Match__c where Nugget__c = :n.Id and Matching_Lead__c != null];
        return nm;
    }

    public List<Nugget_Match__c> getanm(){
 
        anm = [select Id, Matching_Account__r.Name, Matching_Account__c, Matching_Account__r.BillingState, Matching_Account__r.BillingCountry, 
        Matching_Account__r.Owner.Name, Merge_with_Nugget_Account__c from Nugget_Match__c where Nugget__c = :n.Id and Matching_Account__c != null];
    
        return anm;
    }

      
    public PageReference convert(){
        Decimal checkacctmatch1 = 0;
        Decimal checkacctmatch2 = 0;
            
        if(accts == true){
            update anm;
            checkacctmatch1 = [select Count() from Nugget_Match__c where (Id in :anm) and Merge_with_Nugget_Account__c = true];
            if(leads == true) {
               update nm;
               checkacctmatch2 = [select Count() from Nugget_Match__c where (Id in :nm) and Merge_with_Nugget_Account__c = true];  
            }
        
            Decimal checkerror = checkacctmatch1 + checkacctmatch2;

            if(checkerror < 1){
               ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'You must select at least 1 Account or Lead to Convert. To create a new Account click "Create new Account"'));
               return null;
            }
    
            if(checkerror > 0){
    
                List<Nugget_Match__c> checkacctmatch = [select Id, Matching_Account__c from Nugget_Match__c where Id in:anm and Merge_with_Nugget_Account__c = true]; 
        
                if(checkacctmatch.size() > 1){
                    ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'You can only select one Account to merge to.'));
                    List<Nugget_Match__c> resetnm = new List<Nugget_Match__c>();
        
                    for(Nugget_Match__c acctmatch : checkacctmatch){
                        acctmatch.Merge_with_Nugget_Account__c = false;
                        resetnm.add(acctmatch);
                    }
                   update resetnm;
                   return null;
                }   
                if(checkacctmatch.size() == 1){
                    for(Nugget_Match__c acctmatch : checkacctmatch){
                        n.Promoted_Account__c = acctmatch.Matching_Account__c;
                        n.Status__c = 'Promoted to Account';
                        n.Promoted_Closed__c = system.now();
            
                        if(leads == true){
                            update nm;
                            List<Nugget_Match__c> checkleadmatch = [select Id, Matching_Lead__c from Nugget_Match__c where Id in:nm and Merge_with_Nugget_Account__c = true]; 
                            for(Nugget_Match__c slnm : checkleadmatch){
                                Database.LeadConvert lc = new Database.LeadConvert();
                                lc.setLeadId(slnm.Matching_Lead__c);
                                lc.setDoNotCreateOpportunity(true);
                                lc.setConvertedStatus('Qualified');
                                lc.setAccountId(acctmatch.Matching_Account__c);
                                Database.LeadConvertResult lcr = Database.convertLead(lc);
                            }
                        }
                        n.Used_in_Sales_Cycle__c = usedInSalesCycle;
                        update n;
                        pageReference ref = new PageReference('/'+acctmatch.Matching_Account__c);
                        ref.setRedirect(true);
                        return ref;
                    }
                }
    
    
                if(checkacctmatch.size() == 0){
            
                    Account acct = new Account(
                    Name = n.Company_Name__c,
                    DunsNumber = n.D_U_N_S_Number__c,
                    BillingCity = n.City__c,
                    BillingState = n.State__c,
                    BillingCountry = n.Country__c,
                    Salesperson__c = n.OwnerId);
            
                    insert acct;  
                    n.Promoted_Account__c = acct.Id;
                    n.Status__c = 'Promoted to Account';
                    n.Promoted_Closed__c = system.now();
                    n.Used_in_Sales_Cycle__c = usedInSalesCycle;
                    update n;
            
                    if(leads == true){
                        update nm;
                    }
                    List<Nugget_Match__c> checkleadmatch = [select Id, Matching_Lead__c from Nugget_Match__c where Id in:nm and Merge_with_Nugget_Account__c = true]; 
            
                    for(Nugget_Match__c slnm : checkleadmatch){
            
                        Database.LeadConvert lc = new Database.LeadConvert();
                        lc.setLeadId(slnm.Matching_Lead__c);
                        lc.setDoNotCreateOpportunity(true);
                        lc.setConvertedStatus('Qualified');
                        lc.setAccountId(acct.Id);
                        Database.LeadConvertResult lcr = Database.convertLead(lc);
                    }
                    pageReference ref = new PageReference('/'+acct.Id);
                    ref.setRedirect(true);
                    return ref;
                }
            }
        }
    
    
        
    if(accts == false){
        Account acct = new Account( Name = n.Company_Name__c,
                                    DunsNumber = n.D_U_N_S_Number__c,
                                    BillingCity = n.City__c,
                                    BillingState = n.State__c,
                                    BillingCountry = n.Country__c,
                                    Salesperson__c = n.OwnerId);
    
        insert acct;  
        n.Promoted_Account__c = acct.Id;
        n.Status__c = 'Promoted to Account';
        n.Promoted_Closed__c = system.now();
        n.Used_in_Sales_Cycle__c = usedInSalesCycle;
        update n;
    
        if(leads == true){
            update nm;
            List<Nugget_Match__c> checkleadmatch = [select Id, Matching_Lead__c from Nugget_Match__c where Id in:nm and Merge_with_Nugget_Account__c = true]; 
    
            for(Nugget_Match__c slnm : checkleadmatch){
    
                Database.LeadConvert lc = new Database.LeadConvert();
                 lc.setLeadId(slnm.Matching_Lead__c);
                  lc.setDoNotCreateOpportunity(true);
                  lc.setConvertedStatus('Qualified');
                   lc.setAccountId(acct.Id);
                   Database.LeadConvertResult lcr = Database.convertLead(lc);
               }
         }
    
          pageReference ref = new PageReference('/'+acct.Id);
          ref.setRedirect(true);
          return ref;
    
         }
        return null;
    }
    
    
    
    public PageReference createacct(){
        Decimal checkacctmatch1 = 0;
        Decimal checkacctmatch2 = 0;
    
        if(accts == true || leads == true){
            if(accts == true){
                update anm;
                checkacctmatch1 = [select Count() from Nugget_Match__c where (Id in :anm) and Merge_with_Nugget_Account__c = true];
            }
            if(leads == true) {
                update nm;
                checkacctmatch2 = [select Count() from Nugget_Match__c where (Id in :nm) and Merge_with_Nugget_Account__c = true];  
            }
       
            Decimal checkacctmatch = checkacctmatch1 + checkacctmatch2;
    
            if(checkacctmatch > 0){
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Account or Lead cannot be selected to when creating a new Account. To merge the records click "Convert"'));
                return null;
            }
    
            if(checkacctmatch == 0){
                
                System.debug('State and country is:' +n.State__c + ';' + n.Country__c + ''+
                              n.City__c + ':' + n.D_U_N_S_Number__c);
                Account acct = new Account(
                Name = n.Company_Name__c,
                DunsNumber = n.D_U_N_S_Number__c,
                BillingCity = n.City__c,
                BillingState = n.State__c,
                BillingCountry = n.Country__c,
                Salesperson__c = n.OwnerId);
                System.debug('Inserting Account :' +acct);
                insert acct;  
                n.Promoted_Account__c = acct.Id;
                n.Status__c = 'Promoted to Account';
                n.Promoted_Closed__c = system.now();
                n.Used_in_Sales_Cycle__c = usedInSalesCycle;
                update n;
    
                pageReference ref = new PageReference('/'+acct.Id);
                ref.setRedirect(true);
                return ref;
            }
        }
    
        if(accts == false && leads == false){
            Account acct = new Account(
                Name = n.Company_Name__c,
                DunsNumber = n.D_U_N_S_Number__c,
                BillingCity = n.City__c,
                BillingState = n.State__c,
                BillingCountry = n.Country__c,
                Salesperson__c = n.OwnerId);
    
            insert acct;  
            n.Promoted_Account__c = acct.Id;
            n.Status__c = 'Promoted to Account';
            n.Promoted_Closed__c = system.now();
            update n;
    
            pageReference ref = new PageReference('/'+acct.Id);
            ref.setRedirect(true);
            return ref;
        }
        return null;
    }
    
    
    
    public PageReference cancel(){
        pageReference ref = new PageReference('/'+n.Id);
        ref.setRedirect(true);
        return ref;
    }
    
    public void find()
    {
        //Nothing to search for
        if(searchInput == NULL || searchInput.length() <= 2)
        {
                searchAccounts = NULL;
                searchLeads = NULL;
        }
        else
        {
                searchInput = String.escapeSingleQuotes('%'+searchInput+'%');
                searchAccounts = new List<AccountWrapper>();
                searchLeads = new List<LeadWrapper>();
                for(Account acc : [SELECT Id, Name, Owner.Name FROM Account WHERE Name LIKE :searchInput LIMIT 100])
                {
                        accountWrapper acctWrapper = new accountWrapper(acc, false);
                        searchAccounts.add(acctWrapper);
                }
                for(Lead lead : [SELECT Id, Company, Owner.Name FROM Lead WHERE Company LIKE :searchInput LIMIT 100])
                {
                        leadWrapper leadWrapper = new leadWrapper(lead, false);
                        searchLeads.add(leadWrapper);
                }
        }
    }
    public class accountWrapper
    {
        public  Account anAcct {get;set;}
        public  Boolean isSelected {get;set;}
        
        public accountWrapper (Account account, Boolean isSelected)
        {
                this.anAcct = account;
                this.isSelected = isSelected;
        }
        
    }
    public class leadWrapper
    {
        public  Lead aLead {get;set;}
        public  Boolean isSelected {get;set;}
        
        public leadWrapper (Lead lead, Boolean isSelected)
        {
                this.aLead = lead;
                this.isSelected = isSelected;
        }
        
    }
    public pageReference convertSearchResults()
    {
        List<Account> selectedAccounts = new List<Account>();
        List<Lead> selectedLeads = new List<Lead>();
        for(AccountWrapper acctWrapper : searchAccounts)
        {
            if(acctWrapper.isSelected)
            {
                selectedAccounts.add(acctWrapper.anAcct);
            }
        }
        for(LeadWrapper leadWrapper : searchLeads)
        {
                if(leadWrapper.isSelected)
                {
                        selectedLeads.add(leadWrapper.aLead);
                }
        }
        if(selectedAccounts.size() == 0 && selectedLeads.size() == 0)
        {
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Please select an Account or Lead to merge to.'));
                return null;
        }
        else if(selectedAccounts.size() > 1 && selectedLeads.size() > 1)
        {
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'You can only merge to Leads or an Account, not both.'));
                return null;
        }
        else if(selectedAccounts.size() > 1)
        {
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'You can only select one Account to merge to.'));
                return null;
        }
        else if(selectedAccounts.size() == 1 && selectedLeads.size() == 0)
        {
                n.Promoted_Account__c = selectedAccounts[0].Id;
                n.Status__c = 'Promoted to Account';
                n.Promoted_Closed__c = system.now();
                n.Used_in_Sales_Cycle__c = usedInSalesCycle;
            update n;
            
            pageReference ref = new PageReference('/'+selectedAccounts[0].Id);
            ref.setRedirect(true);
            return ref;
        }
        else if(selectedLeads.size() > 0 && selectedAccounts.size() == 0)
        {
                Account account = new Account();
                {
                        account.Name = n.Company_Name__c;
                        account.DunsNumber = n.D_U_N_S_Number__c;
                        account.BillingCity = n.City__c;
                        account.BillingState = n.State__c;
                        account.BillingCountry = n.Country__c;
                        account.Salesperson__c = n.OwnerId;
                }
                insert account;
                
                n.Promoted_Account__c = account.Id;
                n.Status__c = 'Promoted to Account';
                n.Promoted_Closed__c = system.now();
                n.Used_in_Sales_Cycle__c = usedInSalesCycle;
            update n;
                
                for(Lead lead : selectedLeads)
                {
                        Database.LeadConvert leadConvert = new Database.LeadConvert();
                        leadConvert.setLeadId(lead.Id);
                        leadConvert.setDoNotCreateOpportunity(true);
                        leadConvert.setConvertedStatus('Qualified');
                        leadConvert.setAccountId(account.Id);
                        Database.LeadConvertResult convertResult = Database.convertLead(leadConvert);
                }
                pageReference ref = new PageReference('/'+account.Id);
            ref.setRedirect(true);
            return ref;
        }
        else
        {
                return null;
        }
    }
    
}