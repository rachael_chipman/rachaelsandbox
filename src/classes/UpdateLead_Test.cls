@isTest
class UpdateLead_Test {

   static testmethod void test() {
   Test.startTest();
   
   String CRON_EXP = '0 0 * * * ?';
 
 User user = new user(Alias = 'standt', Email='tes@rp.com', 
        EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
        LocaleSidKey='en_US', ProfileId = '00e0000000734LT', 
        TimeZoneSidKey='America/Los_Angeles', UserName='test@rp.com.rp');
            insert user;
    
    Lead L = new Lead();
        L.OwnerId = user.Id;
        L.FirstName = 'Test';
        L.LastName='Lead';
        L.Company = 'RPTEST';
        L.Street='123 S St';
        L.City='Denver';
        L.State='CO';
        L.PostalCode='80221';
        L.Fax='3033333333';
        L.Website='testrpath.com';
        L.SSC_Volume__c = '50,000 to 250,000';
        L.Assigned__c = false;
        L.Status = 'Eloqua Lead Scoring';
            insert L;
            
            // Schedule the test job

      String jobId = System.schedule('testBasicScheduledApex',
      CRON_EXP, 
         new UpdateLeads());
   // Get the information from the CronTrigger API object

      CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, 
         NextFireTime
         FROM CronTrigger WHERE id = :jobId];

   // Verify the expressions are the same
      System.assertEquals(CRON_EXP, 
         ct.CronExpression);

   // Verify the job has not run
      System.assertEquals(0, ct.TimesTriggered);

   // Verify the next time the job will run
    
      

   Test.stopTest();

   

   }
}