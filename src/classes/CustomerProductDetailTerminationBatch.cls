public class CustomerProductDetailTerminationBatch implements Database.Batchable<sObject>, Database.Stateful, Schedulable
{
	public static final String NAME_TO_EXCLUDE = 'Tools Child';
	public static final String PRODUCT_STATUS_TERMINATION = 'Terminated';
	
	public static final String BATCH_NAME = 'Terminate Product Portfolio Batch';
	public static final String BASE_QUERY = 'SELECT Id, Product2Id__c, Account__c, Renewal_Date__c FROM Customer_Product_Detail__c WHERE Name != \'' + NAME_TO_EXCLUDE + '\' AND Product_Status__c = \'Active\' AND Last_12_Months__c != null AND Renewal_Date__c < TODAY';
	public static final List<String> ERROR_EMAILS = new List<String>{ 'returnpath@bluewolfgroup.com', Label.CPD_Termination_Batch_Email };
	
	public static final List<String> VALID_OPTY_TYPE = new List<String>{ 'Renewal', 'Extension' };
	
	private String query;
	public String errors = '';
	
	public CustomerProductDetailTerminationBatch()
	{
		query = BASE_QUERY;
	}
	
	public void execute( SchedulableContext sc )
	{
		CustomerProductDetailTerminationBatch batch = new CustomerProductDetailTerminationBatch();
		Database.executeBatch( batch );
	}
	
	public Database.QueryLocator start( Database.BatchableContext BC )
	{
		return Database.getQueryLocator( query );
	}
	
	public void execute( Database.BatchableContext BC, List<sObject> scope)
	{
		try{
			List<Customer_Product_Detail__c> cpdList = (List<Customer_Product_Detail__c>)scope;
			List<Customer_Product_Detail__c> cpdsToUpdate = new List<Customer_Product_Detail__c>();
			
			System.debug( JSON.serialize( cpdList ) );
			Map<Id, List<OpportunityLineItem> > accountToOli = getGroupedOLIsByAccount( getValidatedOLIs ( Pluck.ids( 'Account__c', cpdList ) ) );	//This map contains Valid Olis Only
			System.debug( JSON.serialize( accountToOli ) );
			for( Customer_Product_Detail__c cpd : cpdList )
			{
				//Check if there are any valid oli's with this product, if there are do nothing to this cpd
				if( accountToOli.containsKey( cpd.Account__c ) && hasValidProduct( cpd, accountToOli.get( cpd.Account__c ) ) )
					continue;
				
				//There are no valid oli's therefore we terminate this 
				cpd.Product_Status__c = PRODUCT_STATUS_TERMINATION;
				cpd.Renewal_Date__c = null;
				cpd.Last_12_Months__c = 0;//Current ACV 
				cpdsToUpdate.add( cpd );
			}
			
			System.debug( JSON.serialize(cpdsToUpdate) );
		
			List<Database.SaveResult> resultList = new List<Database.SaveResult>();
			if( !cpdsToUpdate.isEmpty() )
				resultList = Database.update( cpdsToUpdate, false );
				
			for( Database.SaveResult result : resultList )
			{
				if( !result.isSuccess() )
				{
					errors += result.getId() + '; ' + JSON.serialize( result.getErrors() ) + ';\n';
				}
			}
		}
		catch( Exception e )
		{
			errors += String.valueOf( e ) + ';\n';
		}
	}
	
	/* The function checks if there are any OLI's that have the same ProductId as the Customer Product Detail
	*/
	public static Boolean hasValidProduct( Customer_Product_Detail__c cpd, List<OpportunityLineItem> validOlis )
	{
		for( OpportunityLineItem oli : validOlis )
		{
			Id cpdProdId = cpd.Product2Id__c;
			if(  oli.Product2Id == cpdProdId && oli.End_Date__c > cpd.Renewal_Date__c )
				return true;
		}
		
		return false;
	}
	
	@TestVisible
	public static Map<Id, List<OpportunityLineItem>> getGroupedOLIsByAccount( List<OpportunityLineItem> oliList )
	{
		Map<Id, List<OpportunityLineItem>> oliByAccountMap = new Map<Id, List<OpportunityLineItem>>();
		for( OpportunityLineItem oli : oliList )
		{
			Id actId = oli.Opportunity.AccountId;
			if( !oliByAccountMap.containsKey( actId ) )
				oliByAccountMap.put( actId, new List<OpportunityLineItem>() );
				
			oliByAccountMap.get( actId ).add( oli );
		}
		
		return oliByAccountMap;
	}
	
	/*
		This will return only Valid Oli's that belong to these account.
		A Valida OLI is defined as follows:
			OLI must belong to an Opportunity of the Type Renewal or Extension
			OLI must belong to an Opportunity with a Stage equal to: Value1, Value2 (in validOptyStage)

	*/
	@TestVisible
	private static List<OpportunityLineItem> getValidatedOLIs( Set<Id> accountIds )
	{
		//update function comment when changing these values
		
		return [ SELECT Id, Opportunity.AccountId, Product2Id, End_Date__c FROM OpportunityLineItem WHERE (Opportunity.isWon = true OR Opportunity.isClosed = false) AND Opportunity.Type IN :VALID_OPTY_TYPE AND Opportunity.AccountId IN :accountIds ];
	}
	
	public void finish( Database.BatchableContext BC )
	{
		AsyncApexJob job = [SELECT Id, Status, NumberOfErrors,
							JobItemsProcessed, TotalJobItems,
							CreatedBy.Email
							FROM AsyncApexJob
							WHERE Id =:bc.getJobId()];
	
		Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
	
		mail.setToAddresses( ERROR_EMAILS );
		mail.setSubject( 'Batch to ' + BATCH_NAME + ' ' + job.Status );
		
		String emailBody = 'Batch to ' + BATCH_NAME + ' processed '
						  + job.TotalJobItems
						  + ' batches with '
						  + job.NumberOfErrors
						  + ' failures.';
		if( errors != '' )
		{
			emailBody += '\n\n\nThe following errors occured:\n'+ errors;
		}
		
		mail.setPlainTextBody( emailBody );
		if( job.NumberOfErrors > 0 && String.isBlank( errors ) )
			Messaging.sendEmail( new Messaging.SingleEmailMessage[]{ mail } );
	}
}