//Will ensure Most Recent Campaign is populated correctly on opportunity

public class createOpportunityFromContactController{

    public Contact con {get;set;}
    public Opportunity o {get;set;}
    public Campaign mostrecent;
    public OpportunityContactRole contactrole {get;set;}
    public Profile currentUserProfile
    {
        get
        {
            if(currentUserProfile == NULL)
            {
                Id profileId = UserInfo.getProfileId();
                currentUserProfile = [SELECT Id, Name FROM Profile WHERE Id = :profileId];
            }
            return currentUserProfile;
        }
        private set;
    }
    public Boolean okToProceed{get;set;}
    

public createOpportunityFromContactController(ApexPages.StandardController std){

    con = [SELECT Id, Campaign_Name__c, Most_Recent_Campaign_Id__c, First_Campaign__c, First_Campaign_Id__c,
    AccountId, For_RSE__c, For_RSE__r.isActive, Qualifying_Status__c, REQ_Dedicated_IP_address_es__c,
    REQ_Email_Marketing_Challenges__c, REQ_Email_Volume_Month__c, REQ_My_Business_Primarily_Sells_to__c,
    REQ_Role_in_Purchasing__c, REQ_Willing_to_speak_to_Sales_Rep__c, REQ_What_specific_ISPs_are_problematic__c,
    Goals__c, SDR_Background__c, Business_Objectives_Pain_Points__c, Campaign_Asset_Most_Recent__c, Campaign_Source_Most_Recent__c
    FROM Contact WHERE Id = :ApexPages.currentPage().getParameters().get('id')];
    
    try{
        mostrecent=[SELECT Id FROM campaign WHERE Id=:con.Most_Recent_Campaign_Id__c];
        }
    catch(exception e){}
    
    okToProceed = false;
    
    }
public pageReference createOpp ()
    
    {
        if((currentUserProfile.Name == 'SDR' && con.For_RSE__c != NULL && con.For_RSE__r.isActive) || currentUserProfile.Name != 'SDR')
        {
            okToProceed = true;
            if(currentUserProfile.Name == 'SDR')
            {
                con.Qualifying_Status__c = 'SDR Qualified';
                if(con.Original_SDR__c == NULL)
                {
                    con.Original_SDR__c = UserInfo.getUserId();
                }
            }
            else
            {
                con.Qualifying_Status__c = 'RSE Qualified';
            }
            update con;

            o = new opportunity();
            if(currentUserProfile.Name == 'SDR')
            {
                o.OwnerId = con.For_RSE__c;
                o.Original_SDR__c = UserInfo.getUserId();
            }
            else
            {
                o.OwnerId = UserInfo.getUserId();
            }
            o.campaignId = con.Campaign_Source_Most_Recent__c;
            o.Primary_Campaign_Asset__c = con.Campaign_Asset_Most_Recent__c;
            o.First_Campaign__c = con.First_Campaign__c;
            o.Most_Recent_Campaign__c = con.Campaign_Name__c;
            o.AccountId = con.AccountID;
            o.RecordTypeId = TestUtils.PRE_PIPELINE_RECORD_TYPE.id;
            o.Name = 'New Opportunity';
            o.StageName = 'Pre-Pipeline';
            o.CloseDate = System.Today();
            o.Type = 'New Business';
            o.REQ_Has_dedicated_IP__c = con.REQ_Dedicated_IP_address_es__c;
            o.REQ_What_problems_have_you_experienced__c = con.REQ_Email_Marketing_Challenges__c;
            o.Volume_of_Emails_sent_per_month__c = con.REQ_Email_Volume_Month__c;
            o.Business_primarily_sells_to_B2B_B2C__c = con.REQ_My_Business_Primarily_Sells_to__c;
            o.Role_in_Purchasing__c = con.REQ_Role_in_Purchasing__c;
            o.Willing_to_speak_to_Sales_Rep__c = con.REQ_Willing_to_speak_to_Sales_Rep__c;
            o.REQ_What_specific_ISPs_are_problematic__c = con.REQ_What_specific_ISPs_are_problematic__c;
            o.Goals__c = con.Goals__c;
            o.SDR_Background__c = con.SDR_Background__c; 
            o.Business_Objectives_Pain_Points__c = con.Business_Objectives_Pain_Points__c;
            


            
            insert o;
            Set<Id> oppIds = new Set<Id>{o.Id};
            NamingConventionService.buildOppName(oppIds);
            contactrole= new OpportunityContactRole ();
            contactrole.OpportunityId=o.Id;
            contactrole.ContactId=con.Id;
            contactrole.IsPrimary=True;
           
            insert contactrole;
            /*pageReference ref= new pageReference('https://ssl.salesforce.com/'+o.Id+'/e?retURL=/apex/tabview?id='+o.Id);
            return ref;*/
            
            return null;
        }
        else
        {
            okToProceed = false;
            ApexPages.Message needOwner = new ApexPages.Message(ApexPages.Severity.ERROR, 'Please ensure that the For RSE field is populated with an active user. Use your browser\'s back button to return to the Contact record.');
            ApexPages.addMessage(needOwner);
            return null;
        }
        return null;
    }
    
public pageReference updateOpp()

    {
        if(o.Opportunity_Source__c=='Sales Outreach' && mostrecent==NULL)
        {
            o.campaignId='701000000006Oa1';
        }
        if(o.Opportunity_Source__c=='Contact Us' && mostrecent==NULL)
        {
            o.campaignId='701000000006Oa2';
        }
        if(o.Opportunity_Source__c=='Sales Insight Reports (SIRs)' && mostrecent==NULL)
        {
            o.campaignId='701000000006Oap';
        }
        if(o.Opportunity_Source__c=='Client Services Outreach' && mostrecent==NULL)
        {
            o.campaignId='701000000006Oe4';
        }
        if(o.Opportunity_Source__c=='Client Check-In' && mostrecent==NULL)
        {
            o.campaignId='701000000006Oe5';
        }
        if(o.Opportunity_Source__c=='Client Inbound Contact Us' && mostrecent==NULL)
        {
            o.campaignId='701000000006Oe8';
        }
        if(o.Opportunity_Source__c=='Tools Supplement' && mostrecent==NULL)
        {
            o.campaignId='701000000006OeA';
        }
        if(o.Opportunity_Source__c=='Channel Referral' && mostrecent==NULL)
        {
            o.campaignId='701000000006Oe3';
        }
        if(o.Opportunity_Source__c=='Incremental Solution Sell' && mostrecent==NULL)
        {
            o.campaignId='701000000006Q2f';
        }
        o.Record_Locked__c = true;
        update o;
        pageReference toOpp= new pageReference('/apex/tabview?id='+o.Id);
        return toOpp;
    }
public pageReference Cancel()

    {
        delete contactrole;
        delete o;
        pageReference toCon= new pageReference('/apex/contact_tab_view?id='+con.Id);
        return toCon;
    }
}