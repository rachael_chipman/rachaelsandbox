public with sharing class ProductServices
{
	/*public static void populatePreviousValue( List<OpportunityLineItem> olis, Map<Id, OpportunityLineItem> triggerNewMap)
	{
		Map<Id, List<OpportunityLineItem>> productsToOlis = new Map<Id, List<OpportunityLineItem>>();

		for ( OpportunityLineItem tmpOli : olis )
		{
			List<OpportunityLineItem> innerOLIList;
			if ( !productsToOlis.containsKey( tmpOli.PricebookEntry.Product2Id ) )
			{
				productsToOlis.put( tmpOli.PricebookEntry.Product2Id, new List<OpportunityLineItem>()  );
			}

			productsToOlis.get( tmpOli.PricebookEntry.Product2Id ).add( tmpOli );
		}

		Set<Id> previousOpps = new Set<Id>();

		for ( OpportunityLineItem aTmpOli : olis )
		{
			previousOpps.add( aTmpOli.Opportunity.Previous_Opportunity_Lookup__c ); 
		}

		Map<Id, List<OpportunityLineItem>> previousOppstoPreviousOlis = GroupBy.ids ( 'OpportunityId',
																									[ SELECT Id, Start_Date__c, TotalPrice, End_Date__c,
																												OpportunityId, Product2Id
																											FROM OpportunityLineItem
																											WHERE OpportunityId
																												IN :previousOpps
																									]
																			);
		for ( Id previousOppId : previousOppsToPreviousOlis.keySet() )
		{
			for ( OpportunityLineItem previousOli : previousOppsToPreviousOlis.get( previousOppId ) )
			{
				List<OpportunityLineItem> updatedOlis = productsToOlis.get( previousOli.Product2Id );
				if ( updatedOlis != NULL && !updatedOlis.isEmpty() )
				{
					for ( OpportunityLineItem currentOli : updatedOlis )
					{
						if ( currentOli.Opportunity.Previous_Opportunity_Lookup__c == previousOppId )
						{
							Integer monthsBetween = previousOli.Start_Date__c.monthsBetween( previousOli.End_Date__c );

							Double previousPPM = previousOli.TotalPrice / (monthsBetween == 0 ? 1 : monthsBetween);
							OpportunityLineItem oliFromTrigger = triggerNewMap.get( currentOli.Id );
							oliFromTrigger.Previous_Value__c = previousPPM * oliFromTrigger.Start_Date__c.monthsBetween( oliFromTrigger.End_Date__c );
							break;
						}
					}
				}
			}
		}

	}*/

	public static void populateCertificationTier(List<OpportunityLineItem> olis, Map<Id, OpportunityLineItem> oliMap)
	{
		Map<Id, Opportunity> oppsRequiringCertTier = new Map<Id, Opportunity>();
		List<OpportunityLineItem> filteredOlis = getCertProductNames(olis);
		if(!filteredOlis.isEmpty())
		{
			for(OpportunityLineItem oli : filteredOlis)
			{
				Opportunity opp = new Opportunity(Id = oli.OpportunityId);
				system.debug('what is my opp? ' + opp);
				system.debug('Am I getting a Tier? ' + CertificationOverageGeneration.roundTheTotalVolumeToNextTier(oli.Opportunity.Certification_Max_Volume__c));
				opp.Certification_Tier_Sold__c = 'Tier ' + CertificationOverageGeneration.roundTheTotalVolumeToNextTier(oli.Opportunity.Certification_Max_Volume__c);
				if(!oppsRequiringCertTier.containsKey(opp.Id))
				{
					oppsRequiringCertTier.put(opp.Id, opp);
				}
			}
		}
		try
		{
			update oppsRequiringCertTier.values();
		}
		catch (DmlException ex)
		{
			Map<Id, List<OpportunityLineItem>> oppIdToOlis = groupBy.Ids('OpportunityId', oliMap.values());
			for(Integer i = 0; i < ex.getNumDml(); i++)
			{
				Id errorId = ex.getDmlId(i);
				oppIdToOlis.get(errorId)[0].addError(ex.getDmlMessage(i));
			}
		}
	}

	public static List<OpportunityLineItem> getCertProductNames(List<OpportunityLineItem> olis)
	{
		List<OpportunityLineItem> filteredOlis = new List<OpportunityLineItem>();
		List<PricebookEntry> certProds = [SELECT Id, Name FROM PricebookEntry WHERE Name LIKE '%Certification - License%'];
		Set<String> certProdNames = pluck.Strings('Name', certProds);
		for(OpportunityLineItem oli : olis)
		{
			system.debug('Show me my oli: ' + oli);
			if(certProdNames.contains(oli.PricebookEntry.Name))
			{
				filteredOlis.add(oli);
			}
		}
		system.debug('Is there anything in my filter?' + filteredOlis);
		return filteredOlis;
	}
}