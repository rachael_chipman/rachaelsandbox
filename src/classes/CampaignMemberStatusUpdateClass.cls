public class CampaignMemberStatusUpdateClass {
    
    public static void processTrigger(list<Campaign> newCampaigns, map<id, campaign> oldCampaigns)
    {
        list<Campaign> filteredCamps = filterCampaigns (newCampaigns, oldCampaigns);
        if(!filteredCamps.isEmpty())
        {
            processCampaigns(filteredCamps);
        }
    }
    
    public static void processCampaigns(list<Campaign> filteredCampaigns)
    {
        list<CampaignMemberStatus> newCMStatus = new list<CampaignMemberStatus>();
    	list<CampaignMemberStatus> oldCMStatus = new list<CampaignMemberStatus>();
    	Set<Id> removeCampaignIDs = new Set<ID>();
    
    for (Campaign cItem : filteredCampaigns) {   
        removeCampaignIDs.add(cItem.id);
        
        list<CampaignMS__c> cmsList = new list<CampaignMS__c>();
        	cmsList = CampaignMS__c.getAll().Values();
        	for(CampaignMS__c cmsItem : cmsList){
           		newCMStatus.add (new CampaignMemberStatus(
           		CampaignId = cItem.Id,
           		HasResponded = cmsItem.HasResponded__c,
           		IsDefault = cmsItem.IsDefault__c,
           		Label = cmsItem.Status_Name__c,
           		SortOrder = integer.valueOf(cmsItem.SortOrder__c))); 
        }
    }
        oldCMStatus = [select cms.id
                    from CampaignMemberStatus cms
                    where cms.CampaignID in :removeCampaignIDs];
        try{
            system.debug('Whats in list'+newCMStatus);
            insert newCMStatus;
            list<CampaignMemberStatus> insertedCMS = [Select ID from CampaignMemberStatus Where Id In: newCMStatus];
            system.debug('What is inserted'+insertedCMS);
            delete oldCMStatus;}
        catch(exception e){
            for(CampaignMemberStatus CMS1 : newCMStatus)
            {
                CMS1.adderror('There was a problem inserting the Campaign Member Status');
            }
        }
    }
    
    public static List<Campaign> filterCampaigns (List<Campaign> newCampaigns, Map<Id, Campaign> oldCampaigns)
    {
        List<Campaign> filteredCampaigns = new List<Campaign>();
        for(Campaign camp : newCampaigns)
        {
            Campaign oldCamp;
            if(oldCampaigns != NULL)
            {
                oldCamp = oldCampaigns.get(camp.Id);
            }
            
            if(oldCamp == NULL || camp.Administrative_Edit__c == true && oldCamp.Administrative_Edit__c == FALSE)
            {
                filteredCampaigns.add(camp);
            }
        }
        system.debug('what is in filtered campaigns'+filteredCampaigns);
        
        return filteredCampaigns;
    }

}