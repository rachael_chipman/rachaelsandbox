public with sharing class createTermOppFromCase {
	
	public static Map<String, PriceBookEntry> isoToPBE {
		get
		{
			if(isoToPBE == null)
			{
				isoToPBE = new  Map<String, PriceBookEntry>();
				for(PriceBookEntry pbe : [Select ID, CurrencyIsoCode From PricebookEntry Where Product2.Name='Email Intelligence Tools Child' And PriceBook2ID=:RPPRICEBOOK.ID])
				{
					isoToPBE.put(pbe.CurrencyIsoCode, pbe);
				}
			}
			return isoToPBE;
		}
	}
	public static RecordType TERMINATION 
	{
		get
		{
			if(TERMINATION == NULL)
			{
				TERMINATION = [SELECT Id FROM RecordType WHERE developerName='Termination' AND sObjectType='Case'];
			}
			return TERMINATION;
		}
		set;
	}
	
    public static User SFAUTOMATION 
	{
		get
		{
			if(SFAUTOMATION == NULL)
			{
				SFAUTOMATION = [SELECT Id FROM User WHERE FirstName = 'Salesforce' AND LastName = 'Automation'];
			}
			return SFAUTOMATION;
		}
		set;
	}
    
	public static RecordType OPPTERMINATION  
	{
		get
		{
			if(OPPTERMINATION == NULL)
			{
				OPPTERMINATION = [SELECT Id FROM RecordType WHERE developerName='Termination' AND sObjectType='Opportunity'];
			}
			return OPPTERMINATION;
		}
		set;
	}
	
	public static Pricebook2 RPPRICEBOOK
	{
		get
		{
            
			if(RPPRICEBOOK == NULL) 
			{
				RPPRICEBOOK = [SELECT Id FROM Pricebook2 WHERE Name='Return Path Pricebook'];
			}
			return RPPRICEBOOK;
		}
		set;
	}
	
	public void processTrigger(List<Case> newCases)
	{
		List<Case> filteredCases = filterCasesOnInsert(newCases);
		if(!filteredCases.isEmpty())
		{
			processCasesAndOpps(filteredCases);
		}
	}
	
	public static void processCasesAndOpps(List<Case> filteredCases)
	{
		List<Opportunity> termOppsToInsert = new List<Opportunity>();
		List<OpportunityLineItem> termOlisToInsert = new List<OpportunityLineItem>();
		List<Case> casesToUpdate = new List<Case>();
		Map<Id, Opportunity> caseIdToOpportunity = new Map <Id, Opportunity>();
		Map<Id, Id> caseToCRM = caseIdtoCRMMap(filteredCases);
		
		system.debug('Whats in the case map?'+caseToCRM);
		for(Case aCase : filteredCases)
		{
			termOppstoInsert.add(createOpp(aCase));
		}
		system.debug('How many opps are in termOppstoInsert?'+termOppstoInsert.size());
		for(Opportunity opp : termOppstoInsert)
		{
			if(!caseToCRM.isEmpty() && caseToCRM.containsKey(opp.Termination_Case__c))
			{
				if(caseToCRM.get(opp.Termination_Case__c) != NULL)
				{
					opp.OwnerId = caseToCRM.get(opp.Termination_Case__c);
				}
			}
		}
		insert termOppstoInsert;
		
		Map<Id, Case> caseIdToCase = new Map <Id, Case>(filteredCases);
		for(Opportunity opp : termOppsToInsert)
		{
			caseIdToOpportunity.put(opp.Termination_Case__c, opp);
			OpportunityLineItem oli = new OpportunityLineItem();
			oli.OpportunityId = opp.Id;
			oli.PriceBookentryID = isoToPbe.get(opp.CurrencyISOCode).ID;			
			oli.UnitPrice = 0.00;
			oli.Quantity = 1;
			oli.Previous_Value__c = caseIdToCase.get(opp.Termination_Case__c).Terminated_Annual_Value__c;
			oli.Description__c = 'Product Deactivation';
			oli.Product_Status__c = 'Terminated';
			termOlisToInsert.add(oli);
		}
			
		for(Case aCase : filteredCases)
		{
			system.debug('How many cases are in filteredCases'+filteredCases.size());
			
			if(caseIdToOpportunity.containsKey(aCase.Id) && !caseIdToOpportunity.isEmpty())
			{
				Case updateCase = new Case (id = aCase.Id);
				updateCase.Opportunity__c = caseIdToOpportunity.get(aCase.Id).Id;
				updateCase.Term_Opp_Created__c = true;
				casesToUpdate.add(updateCase);
			}
		}
			
		update casesToUpdate;
		insert termOlisToInsert;
		
	}
	
	public static List<Case> filterCasesOnInsert (List<Case> newCases)
	{
		List<Case> filteredCases = new List<Case>();
		for(Case aCase : newCases)
		{
			if(aCase.RecordTypeId == TERMINATION.Id && aCase.Term_Opp_Created__c == false && aCase.Opportunity__c == NULL)
			{
				filteredCases.add(aCase);
			}
		}
		return filteredCases;
	}
	
	private static Map <Id, Id> caseIdtoCRMMap (List<Case> filteredCases)
	{
		List<Id> caseAccountIds = new List<Id>(); //Maybe this should be a set?
		Map<Id, Id> caseIdtoCRMUser = new Map<Id, Id>();
		
		for(Case aCase : filteredCases)
		{
			caseAccountIds.add(aCase.AccountId); 
			
		}
		
		List<Account> caseAccounts = [SELECT Id, CRM__c FROM Account WHERE Id IN : caseAccountIds];
		
		for(Case aCase : filteredCases)
		{
			for(Account acct : caseAccounts)
			{
				if(acct.Id == aCase.AccountId)
				{
					caseIdtoCRMUser.put(aCase.Id, acct.CRM__c);
				}
			}
		}
		system.debug( 'Whats in the map?'+caseIdtoCRMUser);
		return caseIdtoCRMUser;
	}

	public static Opportunity createOpp(Case aCase)
	{
		Opportunity opp = new Opportunity();
		opp.Termination_Case__c = aCase.Id;
		opp.AccountId = aCase.AccountId;
		opp.StageName = 'Approval Required';
		opp.RecordTypeId = OPPTERMINATION.Id;
		opp.CloseDate = system.today();
		opp.Name = aCase.Subject;
		opp.Type = 'Renewal';
		opp.CurrencyIsoCode = aCase.CurrencyIsoCode;
		opp.Pricebook2Id = RPPRICEBOOK.Id;
		opp.OwnerId = SFAUTOMATION.id;
		opp.Detailed_Termination_Reasons__c = aCase.Cancellation_Reasons__c;
		opp.Termination_Timing__c = aCase.Termination_Timing__c;
		opp.Product__c = 'Tools Child'; //Check that they should all be Tools Child Opps
		opp.Reasons_for_Lost_Opportunities__c = aCase.Type_Sub_Category__c;
		opp.Lost_Pass_Details__c = aCase.Reasons_for_Cancellation__c;
		return opp;
	}
}