@isTest(SeeAllData = True)
Public class finalApprovalErrorController_Test{

    /*public static testMethod void testfinalApprovalErrorController(){
    PageReference pageRef = Page.finalApprovalError;
Test.setCurrentPageReference(pageRef);

   Account acct = new Account();
        acct.name = 'RPTestAcct1';
        acct.accountmanager__c = 'BIO Automation';
        acct.Customer_Relationship_Manager_Primary__c = 'BIO Automation';
        Insert acct;

    Account acct2 = new Account();
        acct2.name = 'RPTestAcct1';
        acct2.accountmanager__c = 'BIO Automation';
        acct2.Customer_Relationship_Manager_Primary__c = 'BIO Automation';
        Insert acct2;

    Promo_Code__c pc = new Promo_Code__c();
        pc.Account__c = acct2.Id;
        pc.Name = 'TEST001';
        pc.Promo_Type__c = 'Referral';
        insert pc;

            acct2.Promo_Code__c = pc.Id;
            update acct2;


   Opportunity opp = new Opportunity();
       opp.AccountId = acct.Id;
       opp.Name = 'Test Opp';
       opp.CloseDate = system.today();
       opp.Type = 'New Business';
       opp.StageName = 'Obtaining Commitment';
       opp.Pricebook2Id = '01s000000004NS6AAM';
       opp.PartnerLookup__c = acct2.Id;
       opp.Partner_Commission__c = .25;
       opp.CurrencyISOCode = 'USD';
       opp.Deal_Closing_Through__c = 'Partner';
       opp.Who_Is_Invoiced__c = 'Partner';
       opp.CurrencyISOCode = 'USD';
       opp.Nature_of_Involvement__c = 'Yes';
       insert opp;

   OpportunityLineItem oli = new OpportunityLineItem();
       oli.OpportunityId = opp.Id;
       oli.UnitPrice = 100.00;
       oli.Quantity = 1;
       oli.PricebookEntryId = '01u00000000FeWfAAK';
       insert oli;

    Opportunity opp2 = new Opportunity();
       opp2.AccountId = acct.Id;
       opp2.Name = 'Test Opp';
       opp2.CloseDate = system.today();
       opp2.Type = 'New Business';
       opp2.StageName = 'Obtaining Commitment';
       opp2.Pricebook2Id = '01s000000004NS6AAM';
       opp2.PartnerLookup__c = acct2.Id;
       opp2.Partner_Commission__c = .25;
       opp2.Billing_Type__c = 'Annual';
       opp2.Who_is_Invoiced__c = 'Partner is Invoiced';
       opp2.Deal_Closing_Through__c = 'Partner';
       opp2.CurrencyISOCode = 'USD';
       opp2.Previous_Opportunity_Lookup__c = opp.Id;
       insert opp2;

   OpportunityLineItem oli2 = new OpportunityLineItem();
       oli2.OpportunityId = opp.Id;
       oli2.UnitPrice = 100.00;
       oli2.Quantity = 1;
       oli2.PricebookEntryId = '01u00000000FeWfAAK';
       oli2.Start_Date__c = system.today();
       oli2.End_Date__c = system.today() +365;
       oli.Description = 'Commission';
       oli2.Product_Status__c = 'Current';
       oli2.Previous_Value__c = 50.00;
       insert oli2;

ApexPages.StandardController sc = new ApexPages.standardController(opp);
System.currentPagereference().getParameters().put('id',opp.Id);
FinalApprovalErrorController oPEE = new FinalApprovalErrorController(sc);

oPEE.checkPartner();
oPEE.addCom();
oPEE.GoBack();
oPEE.SaveChanges();
oPEE.WhoisInvoiced();
oPEE.UpdateOpp();


}

    public static testMethod void testfinalApprovalErrorController2(){
    PageReference pageRef = Page.finalApprovalError;
Test.setCurrentPageReference(pageRef);

   Account acct = new Account();
        acct.name = 'RPTestAcct1';
        acct.accountmanager__c = 'BIO Automation';
        acct.Customer_Relationship_Manager_Primary__c = 'BIO Automation';
        Insert acct;

    Account acct2 = new Account();
        acct2.name = 'RPTestAcct1';
        acct2.accountmanager__c = 'BIO Automation';
        acct2.Customer_Relationship_Manager_Primary__c = 'BIO Automation';
        Insert acct2;

    Promo_Code__c pc = new Promo_Code__c();
        pc.Account__c = acct2.Id;
        pc.Name = 'TEST001';
        pc.Promo_Type__c = 'Referral';
        insert pc;

            acct2.Promo_Code__c = pc.Id;
            update acct2;


   Opportunity opp = new Opportunity();
       opp.AccountId = acct.Id;
       opp.Name = 'Test Opp';
       opp.CloseDate = system.today();
       opp.Type = 'New Business';
       opp.StageName = 'Obtaining Commitment';
       opp.Pricebook2Id = '01s000000004NS6AAM';
       opp.PartnerLookup__c = acct2.Id;
       opp.Partner_Commission__c = .25;
       opp.Deal_Closing_Through__c = 'Return Path';
       opp.Who_Is_Invoiced__c = 'Client is Invoiced';
       opp.CurrencyISOCode = 'USD';
       opp.Nature_of_Involvement__c = 'Yes';
       insert opp;

   OpportunityLineItem oli = new OpportunityLineItem();
       oli.OpportunityId = opp.Id;
       oli.UnitPrice = 100.00;
       oli.Quantity = 1;
       oli.PricebookEntryId = '01u00000000FeWfAAK';
       insert oli;

Opportunity opp2 = new Opportunity();
       opp2.AccountId = acct.Id;
       opp2.Name = 'Test Opp';
       opp2.CloseDate = system.today();
       opp2.Type = 'New Business';
       opp2.StageName = 'Obtaining Commitment';
       opp.Pricebook2Id = '01s000000004NS6AAM';
       opp2.PartnerLookup__c = acct2.Id;
       opp2.Partner_Commission__c = .30;
       opp2.Billing_Type__c = 'Annual';
       opp2.Who_is_Invoiced__c = 'Client is Invoiced';
       opp2.Deal_Closing_Through__c = 'Return Path';
       opp2.CurrencyISOCode = 'USD';
       opp2.Previous_Opportunity_Lookup__c = opp.Id;
       insert opp2;

   OpportunityLineItem oli2 = new OpportunityLineItem();
       oli2.OpportunityId = opp.Id;
       oli2.UnitPrice = 100.00;
       oli2.Quantity = 1;
       oli2.PricebookEntryId = '01u00000000FeWfAAK';
       oli2.Start_Date__c = system.today();
       oli2.End_Date__c = system.today() +365;
       oli.Description = 'Commission';
       oli2.Product_Status__c = 'Current';
       oli2.Previous_Value__c = 50.00;
       insert oli2;


ApexPages.StandardController sc = new ApexPages.standardController(opp);
System.currentPagereference().getParameters().put('id',opp.Id);
FinalApprovalErrorController oPEE = new FinalApprovalErrorController(sc);

oPEE.checkPartner();
oPEE.addCom();
oPEE.GoBack();
oPEE.SaveChanges();
oPEE.WhoisInvoiced();
oPEE.UpdateOpp();
oPEE.goToProducts();


}
    public static testMethod void testfinalApprovalErrorController3(){
    PageReference pageRef = Page.finalApprovalError;
Test.setCurrentPageReference(pageRef);

   Account acct = new Account();
        acct.name = 'RPTestAcct1';
        acct.accountmanager__c = 'BIO Automation';
        acct.Customer_Relationship_Manager_Primary__c = 'BIO Automation';
        Insert acct;

    Account acct2 = new Account();
        acct2.name = 'RPTestAcct1';
        acct2.accountmanager__c = 'BIO Automation';
        acct2.Customer_Relationship_Manager_Primary__c = 'BIO Automation';
        Insert acct2;

    Promo_Code__c pc = new Promo_Code__c();
        pc.Account__c = acct2.Id;
        pc.Name = 'TEST001';
        pc.Promo_Type__c = 'Referral';
        insert pc;

            acct2.Promo_Code__c = pc.Id;
            update acct2;


   Opportunity opp = new Opportunity();
       opp.AccountId = acct.Id;
       opp.Name = 'Test Opp';
       opp.CloseDate = system.today();
       opp.Type = 'New Business';
       opp.StageName = 'Obtaining Commitment';
       opp.Pricebook2Id = '01s000000004NS6AAM';
       opp.PartnerLookup__c = acct2.Id;
       opp.Partner_Commission__c = .25;
       opp.CurrencyISOCode = 'USD';
       opp.Deal_Closing_Through__c = 'Partner';
       opp.Who_Is_Invoiced__c = 'Partner';
       opp.CurrencyISOCode = 'USD';
       opp.Nature_of_Involvement__c = 'Yes';
       insert opp;

   OpportunityLineItem oli = new OpportunityLineItem();
       oli.OpportunityId = opp.Id;
       oli.UnitPrice = 100.00;
       oli.Quantity = 1;
       oli.PricebookEntryId = '01u00000000FeWfAAK';
       insert oli;

    Opportunity opp2 = new Opportunity();
       opp2.AccountId = acct.Id;
       opp2.Name = 'Test Opp';
       opp2.CloseDate = system.today();
       opp2.Type = 'New Business';
       opp2.StageName = 'Obtaining Commitment';
       opp2.Pricebook2Id = '01s000000004NS6AAM';
       opp2.PartnerLookup__c = acct2.Id;
       opp2.Partner_Commission__c = .25;
       opp2.Billing_Type__c = 'Annual';
       opp2.Who_is_Invoiced__c = 'Partner is Invoiced';
       opp2.Deal_Closing_Through__c = 'Partner';
       opp2.CurrencyISOCode = 'USD';
       opp2.Previous_Opportunity_Lookup__c = opp.Id;
       insert opp2;

   OpportunityLineItem oli2 = new OpportunityLineItem();
       oli2.OpportunityId = opp.Id;
       oli2.UnitPrice = 100.00;
       oli2.Quantity = 1;
       oli2.PricebookEntryId = '01u00000000FeWfAAK';
       oli2.Start_Date__c = system.today();
       oli2.End_Date__c = system.today() +365;
       oli.Description = 'Commission';
       oli2.Product_Status__c = 'Current';
       oli2.Previous_Value__c = 50.00;
       insert oli2;

ApexPages.StandardController sc = new ApexPages.standardController(opp);
System.currentPagereference().getParameters().put('id',opp.Id);
FinalApprovalErrorController oPEE = new FinalApprovalErrorController(sc);

oPEE.checkPartner();
oPEE.recalc();
oPEE.GoBack();
oPEE.SaveChanges();
oPEE.WhoisInvoiced();
oPEE.UpdateOpp();


}

    public static testMethod void testfinalApprovalErrorController4(){
    PageReference pageRef = Page.finalApprovalError;
Test.setCurrentPageReference(pageRef);

   Account acct = new Account();
        acct.name = 'RPTestAcct1';
        acct.accountmanager__c = 'BIO Automation';
        acct.Customer_Relationship_Manager_Primary__c = 'BIO Automation';
        Insert acct;

    Account acct2 = new Account();
        acct2.name = 'RPTestAcct1';
        acct2.accountmanager__c = 'BIO Automation';
        acct2.Customer_Relationship_Manager_Primary__c = 'BIO Automation';
        Insert acct2;

    Promo_Code__c pc = new Promo_Code__c();
        pc.Account__c = acct2.Id;
        pc.Name = 'TEST001';
        pc.Promo_Type__c = 'Referral';
        insert pc;

            acct2.Promo_Code__c = pc.Id;
            update acct2;


   Opportunity opp = new Opportunity();
       opp.AccountId = acct.Id;
       opp.Name = 'Test Opp';
       opp.CloseDate = system.today();
       opp.Type = 'New Business';
       opp.StageName = 'Obtaining Commitment';
       opp.Pricebook2Id = '01s000000004NS6AAM';
       opp.PartnerLookup__c = acct2.Id;
       opp.Partner_Commission__c = .25;
       opp.Deal_Closing_Through__c = 'Return Path';
       opp.Who_Is_Invoiced__c = 'Client is Invoiced';
       opp.CurrencyISOCode = 'USD';
       opp.Nature_of_Involvement__c = 'Yes';
       insert opp;

   OpportunityLineItem oli = new OpportunityLineItem();
       oli.OpportunityId = opp.Id;
       oli.UnitPrice = 100.00;
       oli.Quantity = 1;
       oli.PricebookEntryId = '01u00000000FeWfAAK';
       insert oli;

Opportunity opp2 = new Opportunity();
       opp2.AccountId = acct.Id;
       opp2.Name = 'Test Opp';
       opp2.CloseDate = system.today();
       opp2.Type = 'New Business';
       opp2.StageName = 'Obtaining Commitment';
       opp.Pricebook2Id = '01s000000004NS6AAM';
       opp2.PartnerLookup__c = acct2.Id;
       opp2.Partner_Commission__c = .30;
       opp2.Billing_Type__c = 'Annual';
       opp2.Who_is_Invoiced__c = 'Client is Invoiced';
       opp2.Deal_Closing_Through__c = 'Return Path';
       opp2.CurrencyISOCode = 'USD';
       opp2.Previous_Opportunity_Lookup__c = opp.Id;
       insert opp2;

   OpportunityLineItem oli2 = new OpportunityLineItem();
       oli2.OpportunityId = opp.Id;
       oli2.UnitPrice = 100.00;
       oli2.Quantity = 1;
       oli2.PricebookEntryId = '01u00000000FeWfAAK';
       oli2.Start_Date__c = system.today();
       oli2.End_Date__c = system.today() +365;
       oli.Description = 'Commission';
       oli2.Product_Status__c = 'Current';
       oli2.Previous_Value__c = 50.00;
       insert oli2;


ApexPages.StandardController sc = new ApexPages.standardController(opp);
System.currentPagereference().getParameters().put('id',opp.Id);
FinalApprovalErrorController oPEE = new FinalApprovalErrorController(sc);

oPEE.checkPartner();
oPEE.recalc();
oPEE.GoBack();
oPEE.SaveChanges();
oPEE.WhoisInvoiced();
oPEE.UpdateOpp();
oPEE.goToProducts();


}

  private static Opportunity opp;
  private static Account acct1;
  private static Account acct2;
  private static ApexPages.StandardController sc;
  private static final String AR_TRADE_ACCOUNT = '1100 - AR-TRADE';

  private static void setup( Id generalLedgerAccountId, Boolean partnerLookupRequired, Id generalLedgerAccountIdPartnerLookup )
  {
    /*RecordType testAccountRecordType = [SELECT Id FROM RecordType WHERE sObjectType = 'Account'][0];
    RecordType testOpportunityRecordType = [SELECT Id FROM RecordType WHERE sObjectType = 'Opportunity'][0];*/

    /*PageReference pageRef = Page.finalApprovalError;
    Test.setCurrentPageReference(pageRef);

    acct1 = TestUtils.createAccount(TestUtils.MARKETER_RECORD_TYPE.Id,false);
    acct2 = TestUtils.createAccount(TestUtils.MARKETER_RECORD_TYPE.Id,false);

    if(generalLedgerAccountId!=NULL)
      acct1.c2g__CODAAccountsReceivableControl__c = generalLedgerAccountId;

    if(generalLedgerAccountIdPartnerLookup!=NULL)
      acct2.c2g__CODAAccountsReceivableControl__c = generalLedgerAccountIdPartnerLookup;

    insert (new List<Account>{acct1, acct2});

    opp = TestUtils.createOpportunity(TestUtils.DEMONSTRATING_CAPABILITIES_RECORD_TYPE.Id, acct1.Id, false);
    opp.StageName = 'Obtaining Commitment';
    if(partnerLookupRequired)
      opp.PartnerLookup__c = acct2.Id;

    insert opp;

    sc = new ApexPages.standardController(opp);
    System.currentPagereference().getParameters().put('id',opp.Id);

  }

  // This is for testing the situation when the opportunity has an account, but the
  // account's c2g__CODAAccountsReceivableControl__c is NULL. So after the test
  // the account's c2g__CODAAccountsReceivableControl__c Field should be upadted to
  // '1100 - AR-TRADE'.
  private static testMethod void testUpdateAccountField_NoPartnerLookup()
  {
    setup( null, false, null );

    Test.startTest();
      finalApprovalErrorController testController = new finalApprovalErrorController( sc );
      Boolean result = testController.updateAccountField();
    Test.stopTest();

    Account actualGeneralLedgerAccount = [ SELECT c2g__CODAAccountsReceivableControl__c FROM Account WHERE Id =: acct1.Id ];
    System.assertEquals( finalApprovalErrorController.generalLedgerAccount.Id, actualGeneralLedgerAccount.c2g__CODAAccountsReceivableControl__c, 'The account\'s(lookup as Account from Opportunity) AR control field should be updated to 1100 - AR-TRADE' );
  }

  // This is for testing the situation when the opportunity has an account and the
  // account's c2g__CODAAccountsReceivableControl__c is set to something already.
  // So after the test the account's c2g__CODAAccountsReceivableControl__c Field should
  // NOT be upadted to '1100 - AR-TRADE'.
  private static testMethod void testUpdateAccountField_NoPartnerLookup_AccountAlreadyAssigned()
  {
    //c2g__codaGeneralLedgerAccount__c someOtherAccount = [ SELECT Id FROM c2g__codaGeneralLedgerAccount__c WHERE Name != :AR_TRADE_ACCOUNT ][0];
    c2g__codaGeneralLedgerAccount__c someOtherAccount = TestUtils.createGeneralLedgerAccount(true);

    setup( someOtherAccount.Id, false, null );

    Test.startTest();
      finalApprovalErrorController testController = new finalApprovalErrorController( sc );
      Boolean result = testController.updateAccountField();
    Test.stopTest();

    Account actualGeneralLedgerAccount = [ SELECT c2g__CODAAccountsReceivableControl__c FROM Account WHERE Id =: acct1.Id ];
    System.assertEquals( someOtherAccount.Id, actualGeneralLedgerAccount.c2g__CODAAccountsReceivableControl__c, 'The account\'s(lookup as Account from Opportunity) AR control field should not be updated to 1100 - AR-TRADE' );
  }

  // This is for testing the situation when the opportunity has an account and also a PartnerLookup.
  // The PartnerLooup's c2g__CODAAccountsReceivableControl__c is set to NULL.
  // So after the test the PartnerLooup's c2g__CODAAccountsReceivableControl__c Field should
  // be upadted to '1100 - AR-TRADE'.
    private static testMethod void testUpdateAccountField_WithPartnerLookup()
    {
      setup(null, true, null);

      Test.startTest();
      finalApprovalErrorController testController = new finalApprovalErrorController( sc );
        Boolean result = testController.updateAccountField();
      Test.stopTest();

      Account actualGeneralLedgerAccountPartnerLookup = [ SELECT c2g__CODAAccountsReceivableControl__c FROM Account WHERE Id =: acct2.Id ];
      System.assertEquals( finalApprovalErrorController.generalLedgerAccount.Id, actualGeneralLedgerAccountPartnerLookup.c2g__CODAAccountsReceivableControl__c, 'The account\'s(lookup as PartnerLookup from Opportunity) AR control field should be updated to 1100 - AR-TRADE' );
    }

  // This is for testing the situation when the opportunity has an account and also a PartnerLookup.
  // The PartnerLooup's c2g__CODAAccountsReceivableControl__c is already assigned something.
  // So after the test the PartnerLooup's c2g__CODAAccountsReceivableControl__c Field should
  // NOT be upadated to '1100 - AR-TRADE'.
    private static testMethod void testUpdateAccountField_WithPartnerLookupAlreadyAssigned()
    {
    //c2g__codaGeneralLedgerAccount__c someOtherAccount = [ SELECT Id FROM c2g__codaGeneralLedgerAccount__c WHERE Name != :AR_TRADE_ACCOUNT ][0];
      c2g__codaGeneralLedgerAccount__c someOtherAccount = TestUtils.createGeneralLedgerAccount(true);
      setup(null, true,someOtherAccount.Id);

      Test.startTest();
        finalApprovalErrorController testController = new finalApprovalErrorController( sc );
        Boolean result = testController.updateAccountField();
      Test.stopTest();

      Account actualGeneralLedgerAccount = [ SELECT c2g__CODAAccountsReceivableControl__c FROM Account WHERE Id =: acct2.Id ];
      System.assertEquals( someOtherAccount.Id, actualGeneralLedgerAccount.c2g__CODAAccountsReceivableControl__c, 'The account\'s(lookup as PartnerLookup from Opportunity) AR control field should not be updated to 1100 - AR-TRADE' );
    }

    // This is for testing the situation when the opportunity has an account but no PartnerLookup.
    // So after the test the PartnerLooup's c2g__CODAAccountsReceivableControl__c should remain NULL.
    private static testMethod void testUpdateAccountFieldPartnerLookup_NoPartnerLookup()
    {
      setup(null, false, null);

      Test.startTest();
        finalApprovalErrorController testController = new finalApprovalErrorController( sc );
        Boolean result = testController.updateAccountField();
      Test.stopTest();

      Account actualGeneralLedgerAccountPartnerLookup = [ SELECT c2g__CODAAccountsReceivableControl__c FROM Account WHERE Id =: acct2.Id ];
      System.assertEquals( NULL, actualGeneralLedgerAccountPartnerLookup.c2g__CODAAccountsReceivableControl__c, 'The account\'s(lookup as PartnerLookup from Opportunity) AR control field should be NULL as the PartnerLookup is NULL' );
    }*/
}