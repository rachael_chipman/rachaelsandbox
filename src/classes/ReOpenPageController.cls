public with sharing class ReOpenPageController 
{
    public Certification_Application__c certApp {get;set;}
    
    
    public ReOpenPageController(ApexPages.StandardController controller) 
    {
        certApp = [SELECT Id, Application_Status__c, Application_Result__c, Record_Locked__c, Date_App_Reopened__c, Application_Fail_Details__c, Application_Fail_Reason__c, License_Fee_Opp_Stage__c, License_Fee_Opportunity__c, License_Fee_Opportunity__r.IsClosed 
        FROM Certification_Application__c 
        WHERE Id = :ApexPages.currentPage().getParameters().get('id')];
        
        
    }

    public pageReference ReOpen()
    {
        if(certApp.Application_Status__c == 'On Hold')
        {       
            certApp.Application_Status__c = 'Under Review';
            certApp.Application_Result__c = 'In Progress';
            certApp.Application_Fail_Details__c = null;
            certApp.Application_Fail_Reason__c = null;
            certApp.Record_Locked__c = false;
            certApp.Date_App_Reopened__c = system.today();
            update certApp;
            pageReference toApp = new pageReference('/'+certApp.Id);
            return toApp;
        }
        pageReference toApp = new pageReference('/'+certApp.Id);
        return toApp;       
    }
}