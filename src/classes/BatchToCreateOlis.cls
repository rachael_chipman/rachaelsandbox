global class BatchToCreateOlis implements Database.Batchable<sObject> {
	
	public static final String BATCH_NAME = 'BatchToCreateOlis';
    public static final String[] ERROR_EMAILS = new String[]{ 'rachael.chipman@returnpath.com' };
	String errors = '';
	global final String query;

	global BatchToCreateOlis(String queryString) 
	{
		query = queryString;
	}
	
	global Database.QueryLocator start(Database.BatchableContext BC) {
		return Database.getQueryLocator(query);
	}

   	global void execute(Database.BatchableContext BC, List<sObject> scope) 
   	{
		try
		{
			//ManageQuoteService.processQuotesForLineItemsBatch(scope, false);
			LineItemGenerationService.processQuoteChargeDetails(scope);
		}
		catch (DmlException dmx)
		{
			for(Integer i = 0; i < dmx.getNumDml(); i++)
			{
				errors += dmx.getDmlMessage(i);
			}
		}
	}
	
	global void finish(Database.BatchableContext BC) 
	{
		AsyncApexJob job = [SELECT Id, Status, NumberOfErrors,
	                         JobItemsProcessed, TotalJobItems,
	                         CreatedBy.Email
	                         FROM AsyncApexJob
	                         WHERE Id =:bc.getJobId()];

	     Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();

	     mail.setToAddresses( ERROR_EMAILS );
	     mail.setSubject( 'Batch to ' + BATCH_NAME + ' ' + job.Status );

	     String emailBody = 'Batch to ' + BATCH_NAME + ' processed '
	                      + job.TotalJobItems
	                      + ' batches with '
	                      + job.NumberOfErrors
	                      + ' failures.';
	     if( errors != '' )
	     {
		     emailBody += '\n\n\nThe following errors occured:\n'+ errors;
		     mail.setPlainTextBody( emailBody );
		     Messaging.sendEmail( new Messaging.SingleEmailMessage[]{ mail } );
	     }			
	}
	
}