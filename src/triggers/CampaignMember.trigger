trigger CampaignMember on CampaignMember (before insert, after insert, after update) {
    
    if(Trigger.isBefore)
    {
        if(Trigger.isInsert)
        {
            //CampaignMemberServices.populateAutoNumbers(Trigger.new);
        }
    }

    system.debug('what is in trigger.new'+ trigger.new);
    if( trigger.isAfter && ( trigger.isUpdate || trigger.isInsert ) )
    {
        CampaignFieldUpdate.processTrigger( trigger.new, trigger.oldMap );
    }
}