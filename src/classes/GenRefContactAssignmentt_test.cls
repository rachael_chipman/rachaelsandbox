@isTEST

Public Class GenRefContactAssignmentt_test{

   /* private static testmethod void testGenRefContactAssignment(){
    
    
    //Insert User
    User u = new user();
    u.Alias = 'tstuser';
    u.Email='testrc@rp.com';
    u.EmailEncodingKey='UTF-8'; 
    u.LastName='Testing';
    u.LanguageLocaleKey='en_US';
    u.LocaleSidKey='en_US';
    u.ProfileId = '00e0000000734LT'; 
    u.TimeZoneSidKey='America/Los_Angeles'; 
    u.UserName='testrc@rp.com.rp';
        
    insert u;
   
    //Insert Account with CRM
    Account a = new account();
    a.Name = 'Test Account';
    a.OwnerId = u.id;
    a.CRM__c = '00500000002Qd97';
    
    insert a;
    
    //Insert Promo Code related to above Account
    Promo_Code__c pc = new Promo_Code__c();
    pc.Name = 'TEST003';
    pc.Promo_Type__c = 'Referral';
    pc.Account__c = a.Id;
    
    insert pc;
    
    //Insert 2nd Account for Contact
    Account a2 = new account();
    a2.Name = 'Test Account 2';
    a2.OwnerId = u.id;
    
    insert a2;
    
    //Insert Contact
    Contact c = new contact();
    c.LastName = 'Kat';
    c.FirstName = 'Kitty';
    c.AccountId = a2.Id;
    c.Promo_Code__c = 'TEST001';
    
    insert c;
    
        //Update Campaign on Contact to fire trigger
        c.Campaign_Name__c = '2012_GL_PRO_WF_.NET Deliverability Channel Referral Form NEW';
        
        update c;
}*/

 /*   private static testmethod void testGenRefContactAssignment2(){
    
    
    //Insert User
    User u = new user();
    u.Alias = 'tstuser';
    u.Email='testrc@rp.com';
    u.EmailEncodingKey='UTF-8'; 
    u.LastName='Testing';
    u.LanguageLocaleKey='en_US';
    u.LocaleSidKey='en_US';
    u.ProfileId = '00e0000000734LT'; 
    u.TimeZoneSidKey='America/Los_Angeles'; 
    u.UserName='testrc@rp.com.rp';
        
    insert u;
   
    //Insert Account with CRM
    Account a = new account();
    a.Name = 'Test Account';
    a.OwnerId = u.id;
    a.CRM__c = '00500000006olmF';
    
    insert a;
    
    //Insert Promo Code related to above Account
    Promo_Code__c pc = new Promo_Code__c();
    pc.Name = 'TEST001';
    pc.Promo_Type__c = 'Referral';
    pc.Account__c = a.Id;
    
    insert pc;
    
    //Insert 2nd Account for Contact
    Account a2 = new account();
    a2.Name = 'Test Account 2';
    a2.OwnerId = u.id;
    
    insert a2;
    
    //Insert Contact
    Contact c = new contact();
    c.LastName = 'Kat';
    c.FirstName = 'Kitty';
    c.AccountId = a2.Id;
    c.Promo_Code__c = 'TEST001';
    
    insert c;
    
        //Update Campaign on Contact to fire trigger
        c.Campaign_Name__c = '2012_GL_PRO_WF_.NET Deliverability Channel Referral Form NEW';  
        
        update c;    
        
}

/*private static testmethod void testGenRefContactAssignment3(){

    //Insert User
    User u = new user();
    u.Alias = 'tstuser';
    u.Email='testrc@rp.com';
    u.EmailEncodingKey='UTF-8'; 
    u.LastName='Testing';
    u.LanguageLocaleKey='en_US';
    u.LocaleSidKey='en_US';
    u.ProfileId = '00e0000000734LT'; 
    u.TimeZoneSidKey='America/Los_Angeles'; 
    u.UserName='testrc@rp.com.rp';
        
    insert u;

    //Insert Account for Contact
    Account a2 = new account();
    a2.Name = 'Test Account 2';
    a2.OwnerId = u.id;
    
    insert a2;
    
    //Insert Contact
    Contact c = new contact();
    c.LastName = 'Kat';
    c.FirstName = 'Kitty';
    c.AccountId = a2.Id;
    
    
    insert c;
    
        //Update Campaign on Contact to fire trigger
        c.Campaign_Name__c = '2012_GL_PRO_WF_.NET Deliverability Channel Referral Form NEW';    
    
        update c;
        
}*/
  /*  private static testMethod void testContactAssignment(){
        
        Id marketerRecordTypeId = [SELECT Id From RecordType Where Name = 'Marketer' AND sObjectType = 'Account'].Id;
        User testUser = TestUtils.getTestUser( 'TestUser123', 'Exec Admin');
        
        System.runAs( TestUtils.ADMIN_USER ){
           insert testUser;
        }
        
        Account testAccount = TestUtils.createAccount( marketerRecordTypeId, false );
        testAccount.OwnerId = testUser.Id;
        testAccount.Customer_Relationship_Manager_Primary__c = 'Test';
        testAccount.CRM__c = testUser.Id;
        insert testAccount;
        
        Promo_Code__c testPromo = TestUtils.createPromoCode( 'TestCode', 'Referral', testAccount.Id, false );
        testPromo.Account__c = testAccount.Id;
        insert testPromo;
        
        Contact testContact = TestUtils.createContacts( 'Test', 'Contact', testAccount.Id, false );
        testContact.Promo_Code__c = 'TestCode';
        insert testContact;
        
        Test.startTest();
            testContact.Gen_Ref_Contact__c = false;
            testContact.Campaign_Name__c = '2012_GL_PRO_WF_.NET Deliverability Channel Referral Form NEW';
            update testContact;
        Test.stopTest();
        
        List<Contact> updatedContact = [SELECT Id, Referring_Partner__c, Referring_CRM__c, OwnerId FROM Contact];
        
        System.assertEquals ( 1, updatedContact.size(), 'There should be one contact created in the test');
        System.assertEquals ( testAccount.Id, updatedContact[0].Referring_Partner__c, 'Referring_Partner__c should be populated with the accountId');
        System.assertEquals ( testUser.Id, updatedContact[0].Referring_CRM__c, 'Referring_CRM__c should be populated from the account');
        System.assertEquals ( testUser.Id, updatedContact[0].OwnerId, 'There should be one contact created in the test');
        
        
    } */
}