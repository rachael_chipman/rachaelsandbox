/** This class updates the fields on Peoples section on the Account Object
 * from the Account teams list. 
 * This job runs nightly 
 * @Author: Sangita ghosh
 * @Date: 3/17/15
 */

global class UpdatePeoplesSectionBatch  implements Database.Batchable<sObject>, schedulable{

    global final String Query;
    Map<Id,String> failedaccountMap;
    string errormessage;
    
    public UpdatePeoplesSectionBatch()
    {
        failedaccountmap = new map<Id,String>();
        Query = 'Select Id, Name,AM__c,CRM__c, OwnerId,Commerical_Sales_Rep__c, ' +
                'APS_Client_Services__c,Inbox_Insight_Client_Services__c,Original_Salesperson__c,Salesperson__c,'+
                'Regional_Director__c,CorpDev_Other__c, Technical_Account_Manager__c,RecordTypeId,' +
                'Collections_Team_Member__c,Channel_Partner_Rep__c ,(Select AccountId, UserId, User.IsActive,User.Name, ' +
                'IsDeleted, TeamMemberRole, CreatedDate, AccountAccessLevel from AccountTeamMembers order by CreatedDate DESC) ' +
                 'from Account';
        
         System.debug ('Query is :' +Query);
       
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC)
    {
       
        return Database.getQueryLocator(Query);
    }
    
    global void execute(Database.BatchableContext BC, List<sObject> scope)
    {
        System.debug('Scope is :' + scope);
       // errormessage = '';
        failedaccountMap = AccountService.updatePeoplesTeamSection(scope);
    
    }
    
    global void finish(Database.BatchableContext BC)
    {
        //send email to the Salesforce splat team
       AccountService.sendErrorEmail(failedaccountmap);

    }
    
  global void execute(SchedulableContext sc) {
      UpdatePeoplesSectionBatch UPB = new UpdatePeoplesSectionBatch(); 
      database.executebatch(UPB);
   }

    
    

}