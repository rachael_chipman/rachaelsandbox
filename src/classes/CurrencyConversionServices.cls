public with sharing class CurrencyConversionServices {
    
    public static c2g__codaCompany__c RETURN_PATH
    {
        get
        {
            if (RETURN_PATH == null)
            {
                RETURN_PATH = [SELECT Id, Name FROM c2g__codaCompany__c WHERE Name = 'Return Path, Inc.' LIMIT 1];
            }
            return RETURN_PATH;
        }
        private set;
    }

    public static Map<String, c2g__codaAccountingCurrency__c> getAllCurrencies( List<Opportunity> filterOpportunities, Map<Id, Account> oppIdToPartner )
    {
        Set<String> curIsoCodes = Pluck.strings( 'CurrencyIsoCode',filterOpportunities );
        
        if( oppIdToPartner != NULL && !oppIdToPartner.isEmpty() )
        {
            curIsoCodes.addAll( Pluck.strings( 'CurrencyIsoCode',oppIdToPartner.values() ) );

        }
        Map<String, c2g__codaAccountingCurrency__c> currencyMap = new Map<String, c2g__codaAccountingCurrency__c>();
        for( c2g__codaAccountingCurrency__c anAccountingCurrency : [select Id, Name, CurrencyIsoCode from c2g__codaAccountingCurrency__c where Name IN :curIsoCodes and c2g__OwnerCompany__c = :RETURN_PATH.Id ] )
        {
            currencyMap.put( anAccountingCurrency.Name, anAccountingCurrency );
        }
        return currencyMap;
    }

    public static Map<String, Map<Date, c2g__codaExchangeRate__c>> getAllExchangeRates( Map<Id, Date> oppIdToInvoiceDate, Set<Id> currencyIds )
    {
        Date maximumInvoiceDate = getMaximumInvoiceDate( oppIdToInvoiceDate );

        //Get all exchange rates before the maximum invoice dates for all the currencies
        List<c2g__codaExchangeRate__c> exRates = [SELECT Id, c2g__Rate__c, c2g__ExchangeRateCurrency__r.Name, c2g__Startdate__c from c2g__codaExchangeRate__c WHERE c2g__ExchangeRateCurrency__r.Id IN :currencyIds 
        AND c2g__ExchangeRateCurrency__r.c2g__OwnerCompany__c = :RETURN_PATH.Id 
        AND c2g__Startdate__c <= :maximumInvoiceDate order by c2g__Startdate__c desc ];

        return groupExchangeRateByCurrencyISOCode( exRates );

    }

    private static Date getMaximumInvoiceDate( Map<Id, Date> oppIdToInvoiceDate )
    {
        //Sort all dates and get the last one
        List<Date> invoiceDates = oppIdToInvoiceDate.values();
        invoiceDates.sort();
        return invoiceDates[ invoiceDates.size() - 1 ];
    }

    private static Map<String, Map<Date, c2g__codaExchangeRate__c>> groupExchangeRateByCurrencyISOCode( List<c2g__codaExchangeRate__c> exRates )
    {
        Map<String, Map<Date, c2g__codaExchangeRate__c>> currencyISOCodeToExRate = new Map<String, Map<Date, c2g__codaExchangeRate__c>>();
        for( c2g__codaExchangeRate__c anExRate : exRates )
        {
            String key = anExRate.c2g__ExchangeRateCurrency__r.Name ;
            if( !currencyISOCodeToExRate.containsKey( key ) )
            {
                currencyISOCodeToExRate.put( key, new Map<Date, c2g__codaExchangeRate__c>() );
            }
            currencyISOCodeToExRate.get( key ).put( anExRate.c2g__Startdate__c, anExRate );
        }
        return currencyISOCodeToExRate;
    }

    //Assumes incoming opportunities are partner invoiced and currency iso does not match with partner's currencyisocode
    public static Map<Id, Decimal> getOpportunityRate( List<Opportunity> opportunities, Map<String, Map<Date, c2g__codaExchangeRate__c>> currencyISOCodeToExRate, Map<Id, Date> oppIdToInvoiceDate )
    {
        Map<Id, Decimal> oppIdToRate = new Map<Id, Decimal>();

        for( Opportunity o : opportunities )
        {
            if(o.CurrencyISOCode == 'USD')
            { 
                oppIdToRate.put( o.Id, 1 );
            }
            else
            {
                Date invoiceDate = oppIdToInvoiceDate.get( o.Id );
                //Sort the exchange rate dates and iterate starting from maximum to get the latest exrate with date <= invoice date
                List<Date> relatedExRateDates = new List<Date> ( currencyISOCodeToExRate.get( o.CurrencyISOCode ).keySet() );
                relatedExRateDates.sort();
                for( Integer i = relatedExRateDates.size()-1; i >= 0; i-- )
                {
                    Date anExRateDate = relatedExRateDates[i];
                    if( anExRateDate <= invoiceDate )
                    {
                        oppIdToRate.put( o.Id, currencyISOCodeToExRate.get( o.CurrencyISOCode ).get( anExRateDate ).c2g__Rate__c );
                        break;
                    }
                }
            }
        }
        return oppIdToRate;
    }

    public static Map<Id, Decimal> getPartnerRates( List<Opportunity> opportunities, Map<String, Map<Date, c2g__codaExchangeRate__c>> currencyISOCodeToExRate, Map<Id, Date> oppIdToInvoiceDate, Map<Id, Account> oppIdToPartner )
    {
        Map<Id, Decimal> oppIdToPartnerRate = new Map<Id, Decimal>();

        for( Opportunity o : opportunities )
        {
            Account relatedPartner = oppIdToPartner.get( o.Id );
            Date invoiceDate = oppIdToInvoiceDate.get( o.Id );

            oppIdToPartnerRate.put( o.Id, getRate( relatedPartner.CurrencyISOCode, invoiceDate, currencyISOCodeToExRate ) );
        }
        return oppIdToPartnerRate;
    }

    public static Decimal getRate( String currencyISOCode, Date invoiceDate, Map<String, Map<Date, c2g__codaExchangeRate__c>> currencyISOCodeToExRate )
    {
        if( currencyISOCode != 'USD')
        {
            //Sort the exchange rate dates and iterate starting from maximum to get the latest exrate with date <= invoice date
            List<Date> relatedExRateDates = new List<Date> ( currencyISOCodeToExRate.get( currencyISOCode ).keySet() );
            relatedExRateDates.sort();
            for( Integer i = relatedExRateDates.size()-1; i >= 0; i-- )
            {
                Date anExRateDate = relatedExRateDates[i];
                if( anExRateDate <= invoiceDate )
                {
                    return currencyISOCodeToExRate.get( currencyISOCode ).get( anExRateDate ).c2g__Rate__c;
                }
            }
        }
        return 1.0; // USD
    }
}