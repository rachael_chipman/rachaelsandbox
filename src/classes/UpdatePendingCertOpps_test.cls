@isTest

Public Class UpdatePendingCertOpps_test{
    
    private static Account acc;
    private static Campaign certCampaign;

    private static RecordType CERT_APP
    {
        get
        {
            if(CERT_APP == NULL)
            {
                CERT_APP = [SELECT Id FROM RecordType WHERE DeveloperName = 'Cert_App'];
            }
            return CERT_APP;
        }
        private set;
    }
    
    private static void setup()
    {
         /*User user = new user(Alias = 'standt', Email='tes@rp.com', 
                              EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                              LocaleSidKey='en_US', ProfileId = '00e0000000734LT', 
                              TimeZoneSidKey='America/Los_Angeles', UserName='test@rp.com.rp');
                              insert user;  
                        
                         acc = new Account(
                                OwnerId = user.Id,
                                Name = 'Test Account',
                                phone = '(303) 555-5555');
                                insert acc;*/
        acc = TestUtils.createAccount(TestUtils.MARKETER_RECORD_TYPE.Id, true);

        certCampaign = TestUtils.createCampaign(false);
        certCampaign.Name = 'WEB: SSC Application Form';
        certCampaign.Type = 'SS.ORG';
        certCampaign.IsActive = true;
        insert certCampaign;
    }

    /*private static testmethod void testUpdatePendingCertOpps1(){


                       setup();
                            
                        Certification_Application__c ca = new Certification_Application__c();
        				ca.RecordTypeId = '012000000004xGx';
                        ca.Account__c = acc.id;
                        ca.Certification_Volume__c = 'Tier 2';
                        ca.Application_Status__c = 'Under Review';
                        ca.Application_Result__c = 'In Progress';
                        ca.Promo_Type_Picklist__c = 'Walk-Up';
                        insert ca;
        				
        				test.starttest();
                        List<Certification_Application__c> NewCA = [Select Id, Name,Opportunity_Created__c,Application_Fee_Opportunity__c,
                                                              License_Fee_Opportunity__c,License_Fee_Opp_Stage__c from Certification_Application__c
                                                              where Id =:ca.Id];
                        System.debug('New CA is :' + NewCA[0]);
        				System.debug('Ca value is :' + ca);
                        System.debug('New CA is :'+NewCA[0].Opportunity_Created__c  );
                        System.assertNotEquals(NewCA[0].License_Fee_Opportunity__c,null,'We expect the opportunity to be created');
                        System.assertEquals(NewCA[0].Opportunity_Created__c,'Yes','We expect the opportunity to be created');
                        System.assertEquals(NewCA[0].License_Fee_Opp_Stage__c,'Signature','We expect the Opportunity to be in Signature Stage');
                        test.stoptest();
                        
                       /* ca.Application_Status__c = 'Closed';
                        ca.Application_Result__c = 'Pass';
                        ca.Activation_Date__c = system.today();
                        update ca;
}*/
    
    private static testMethod void testUpdatePendingCertOpps2(){

           				setup();
                        
                        Certification_Application__c ca2 = new Certification_Application__c();
                        ca2.Account__c = acc.id;
        				ca2.RecordTypeId = CERT_APP.Id;
                        ca2.Certification_Volume__c = 'Tier 2';
                        ca2.Application_Status__c = 'Under Review';
                        ca2.Application_Result__c = 'In Progress';
                        ca2.Promo_Type_Picklist__c = 'Walk-Up';
                        insert ca2;
                        
                        Certification_Application__c NewCA = [Select Id, Name,Opportunity_Created__c,Application_Fee_Opportunity__c,
                                                              License_Fee_Opportunity__c,License_Fee_Opp_Stage__c from Certification_Application__c where Id =:ca2.Id limit 1];
                        
                        System.assertNotEquals(NewCA.License_Fee_Opportunity__c,null,'We expect the opportunity to be created');
                        System.assertEquals(NewCA.Opportunity_Created__c,'Yes','We expect the opportunity to be created');
                        System.assertEquals(NewCA.License_Fee_Opp_Stage__c,'Signature','We expect the Opportunity to be in Signature Stage');
                        
                        ca2.Application_Status__c = 'Closed';
                        ca2.Application_Result__c = 'Fail';
                        ca2.Application_Fail_Reason__c = 'Administrative';
                        ca2.Application_Fail_Details__c = 'No Response';
                        ca2.Date_Failed__c = system.today();
                        
                        test.startTest();
                        update ca2;
    				    test.stopTest();
                        NewCA = [Select Id, Name,Opportunity_Created__c,Application_Fee_Opportunity__c,
                                                              License_Fee_Opportunity__c,License_Fee_Opp_Stage__c from Certification_Application__c where Id =:ca2.Id limit 1];
                        System.assertEquals(NewCA.License_Fee_Opp_Stage__c,'Lost','We expect the Opportunity to be in Signature Stage');
}

    private static testMethod void testUpdatePendingCertOpps3(){


						setup();

                        Opportunity pendingCertOpp = TestUtils.createOpportunity(TestUtils.PURCHASE_RECORD_TYPE.Id, acc.Id, false);
                        pendingCertOpp.StageName = 'Pending Certification Qualification';
                        pendingCertOpp.Approval_Type__c = 'Finance Approval';
                        insert pendingCertOpp;

                        Certification_Application__c ca2 = new Certification_Application__c();
                        ca2.Account__c = acc.id;
				        ca2.RecordTypeId = CERT_APP.Id;
                        ca2.Certification_Volume__c = 'Tier 2';
                        ca2.Application_Status__c = 'Audit Complete';
                        ca2.Application_Result__c = 'Fixes Needed';
                        ca2.Promo_Type_Picklist__c = 'Walk-Up';
                        ca2.License_Fee_Opportunity__c = pendingCertOpp.Id;
                        insert ca2;
                        
                        /*Certification_Application__c NewCA = [Select Id, Name,Opportunity_Created__c,Application_Fee_Opportunity__c,
                                                              License_Fee_Opportunity__c,License_Fee_Opp_Stage__c,License_Fee_Opportunity__r.Awaiting_Approval__c from Certification_Application__c where Id =:ca2.Id limit 1];
                        //approve the Opp
                        List<ProcessInstanceWorkItem> workItem = [Select Id, ProcessInstance.TargetObjectId from ProcessInstanceWorkitem where 
                                                                      ProcessInstance.TargetObjectId = :NewCA.License_Fee_Opportunity__c  limit 1];
                            
                            System.debug('WorkItem is :' + workItem);
                            System.debug('AP is :'+    NewCA.License_Fee_Opportunity__r.Awaiting_Approval__c);
                            if(workItem!= null && !workItem.isEmpty())
                            {
                                ProcessInstanceWorkItem pwrItem = workItem[0];
                                Approval.ProcessWorkitemRequest pwr = new Approval.ProcessWorkitemRequest();
                                pwr.setWorkitemId(pwrItem.Id);
                                pwr.setAction('Approve');
                                Approval.ProcessResult res = Approval.process(pwr);
                            }
                            
                        List<Opportunity> updatedOpp = [SELECT Id, Awaiting_Approval__c, StageName FROM Opportunity WHERE Id = : ca2.License_Fee_Opportunity__c];
                        if(updatedOpp[0].StageName != 'Pending Certification Qualification')
                        {
                            updatedOpp[0].StageName = 'Pending Certification Qualification';
                            update updatedOpp[0];
                        }
                        system.assertEquals(updatedOpp[0].StageName,'Pending Certification Qualification','We expect the Opportunity to be in Pending Certification Qualification');*/

                        ca2.Activation_Date__c = system.today();
                        
                        test.startTest();
                        update ca2;
                        test.stopTest();

                        Opportunity pendingCert = [SELECT Id, Awaiting_Approval__c FROM Opportunity WHERE Id = : ca2.License_Fee_Opportunity__c];
                        system.assertEquals(true, pendingCert.Awaiting_Approval__c, 'Opp should be pending approval');

    }
 
}