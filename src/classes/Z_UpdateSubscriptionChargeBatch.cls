/** Batch class that pulls in Included Units and Overage Price from Zuora and sets the on the Subscription Product & Charge
 *
 * @author Marin Zaimov (marin.zaimov@zuora.com)
 * @version 1.0 June 11, 2015
 */
 global class Z_UpdateSubscriptionChargeBatch implements Database.Batchable<sObject>, Database.AllowsCallouts, Database.Stateful {
    
    String query;
    List<Zuora__SubscriptionProductCharge__c> newSubChargeList;

  /**
   * Passing in objects instead of querying, since we already have them from a trigger execution
   */
    global Z_UpdateSubscriptionChargeBatch(List<Zuora__SubscriptionProductCharge__c> subChargeList) {
        newSubChargeList = subChargeList;
    }
    
  /**
   * Passing in objects instead of querying, since we already have them from a trigger execution
   */
    global Iterable<sObject> start(Database.BatchableContext BC) {
        return newSubChargeList;
    }

  /**
   * Runs all the logical steps to pull in values from Zuora and set them on the Subscription Product & Charge
   */
  global void execute(Database.BatchableContext BC, List<Zuora__SubscriptionProductCharge__c> scope) {
    Map<String, Zuora__SubscriptionProductCharge__c> zuoraIdToSubChargeMap = createZuoraIdToSubChargeMap(scope);
    

    /*
     * NOTE(Marin): Created 2 queries because of 2 reasons:
     *      1. the following error:
     *            Zuora.zRemoteException: Can only use one of the following in a single query: OveragePrice, Price, IncludedUnits, DiscountAmount or DiscountPercentage.
     *      2. The two fields are actually on different objects. Included Units is on RatePlanCharge,
     *            while Overage Price is on RatePlanChargeTier.Price when RatePlanChargeTier.IsOveragePrice = true
     */

    String zuoraIncludedUnitsQuery = createIncludedUnitsQuery(scope);
    String zuoraOveragePriceQuery = createOveragePriceQuery(scope);

    Zuora.zApi zuoraApi = zuoraApiAccess();
    
    List<Zuora.zObject> zuoraSubChargesIncludedUnits = queryZuoraRatePlanCharges(zuoraApi, zuoraIncludedUnitsQuery);
    List<Zuora.zObject> zuoraSubChargeTiersOveragePrice = queryZuoraRatePlanChargeTiers(zuoraApi, zuoraOveragePriceQuery);

    List<Zuora__SubscriptionProductCharge__c> chargesToUpdate = getChargesToUpdate(zuoraIdToSubChargeMap, zuoraSubChargesIncludedUnits, zuoraSubChargeTiersOveragePrice);

        update chargesToUpdate;
    }
    

  /**
   * Nothing to do here for now
   */
    global void finish(Database.BatchableContext BC) {
        
    }

    /**
     * Assigns the included units and overage price to each charge in the map (zuoraIdToSubChargeMap), where applicable, and returns a list to update
     */
    private List<Zuora__SubscriptionProductCharge__c> getChargesToUpdate(
    Map<String, Zuora__SubscriptionProductCharge__c> zuoraIdToSubChargeMap, 
    List<Zuora.zObject> zuoraSubChargesIncludedUnits,
    List<Zuora.zObject> zuoraSubChargeTierssOveragePrice)
    {
        
    for (Zuora.zObject zuoraCharge : zuoraSubChargesIncludedUnits) {
            
            Zuora__SubscriptionProductCharge__c charge = zuoraIdToSubChargeMap.get((String)zuoraCharge.getValue('Id'));
      if (charge != null) {
          if (zuoraCharge.getValue('IncludedUnits') != null){
            charge.IncludedUnits__c = (Decimal)zuoraCharge.getValue('IncludedUnits');
            zuoraIdToSubChargeMap.put((String)zuoraCharge.getValue('Id'), charge);
          }
      }

        }
    for (Zuora.zObject zuoraChargeTier : zuoraSubChargeTierssOveragePrice) {

      Zuora__SubscriptionProductCharge__c charge = zuoraIdToSubChargeMap.get((String)zuoraChargeTier.getValue('RatePlanChargeId'));
      if (charge != null) {
          if (zuoraChargeTier.getValue('Price') != null){
             charge.OveragePrice__c = (Decimal)zuoraChargeTier.getValue('Price');
             zuoraIdToSubChargeMap.put((String)zuoraChargeTier.getValue('RatePlanChargeId'), charge);   
          }
      }

    }

        return zuoraIdToSubChargeMap.values();
    }

    /**
     * Creates a query to pull in RatePlanCharges from Zuora
     * Zuora query language has no IN clause and needs to have multiple OR clauses to pull in objects by Id
     */
    private String createIncludedUnitsQuery(List<Zuora__SubscriptionProductCharge__c> subChargeListScope)
  {
    String query = 'SELECT Id, IncludedUnits FROM RatePlanCharge WHERE ';
    for (Integer i=0;i<subChargeListScope.size();i++) {
        query += 'Id = \'' + subChargeListScope.get(i).Zuora__Zuora_Id__c + '\'';

      if (i!=subChargeListScope.size()-1) {
        query += ' OR ';
      }
    }
   // for (Zuora__SubscriptionProductCharge__c charge : newSubChargeList) {
   //   query += 'Id = \'' + charge.Zuora__Zuora_Id__c + '\'';

//      if (subChargeList.get(subChargeList.size() - 1).Id != charge.Id) {
  //      query += ' OR ';
    //  }
    //}

    return query;
  }

  /**
   * Creates a query to pull in RatePlanCharges from Zuora
   * Zuora query language has no IN clause and needs to have multiple OR clauses to pull in objects by Id
   */
  private String createOveragePriceQuery(List<Zuora__SubscriptionProductCharge__c> subChargeListScope)
  {
    String query = 'SELECT RatePlanChargeId, Price FROM RatePlanChargeTier WHERE ';
    for (Integer i=0;i<subChargeListScope.size();i++) {
    //for (Zuora__SubscriptionProductCharge__c charge : newSubChargeList) {
      query += 'IsOveragePrice = true AND RatePlanChargeId = \'' + subChargeListScope.get(i).Zuora__Zuora_Id__c + '\'';

      if (i != subChargeListScope.size()-1) {
        query += ' OR ';
      }
   }

    return query;
  }

  /**
     * Used for assigning the included Units values from Zuora to the values in SFDC charges. Bring complexity down from O(n^2) to O(n)
     */
  private Map<String, Zuora__SubscriptionProductCharge__c> createZuoraIdToSubChargeMap(List<Zuora__SubscriptionProductCharge__c> newSubChargeList)
  {
    Map<String, Zuora__SubscriptionProductCharge__c> zuoraIdToSubChargeMap = new Map<String, Zuora__SubscriptionProductCharge__c>();
    for (Zuora__SubscriptionProductCharge__c charge : newSubChargeList) {
      zuoraIdToSubChargeMap.put(charge.Zuora__Zuora_Id__c, charge);
    }

    return zuoraIdToSubChargeMap;
  }

    /**
     * Performs the query to Zuora including the login call
     */
  private List<Zuora.zObject> queryZuoraRatePlanCharges(Zuora.zApi zuoraApi, String zuoraQuery)
  {

    List<Zuora.zObject> queryResultsList = new List<Zuora.zObject>();
    if (!Test.isRunningTest()) {
      queryResultsList = zuoraApi.zQuery(zuoraQuery);
    } else {
      Zuora.zObject rpc = new Zuora.zObject('RatePlanCharge');
      rpc.setValue('Id','12345678901234567890123456789012');
      rpc.setValue('IncludedUnits',100);
      Zuora.zObject rpc2 = new Zuora.zObject('RatePlanCharge');
      rpc2.setValue('Id','12345678901234567890123456789022');
      rpc2.setValue('IncludedUnits',200);
      queryResultsList = new List<Zuora.zObject>{ rpc, rpc2 };
    }
    return queryResultsList;
  }

  /**
   * Performs the query to Zuora including the login call
   */
  private List<Zuora.zObject> queryZuoraRatePlanChargeTiers(Zuora.zApi zuoraApi, String zuoraQuery)
  {

    List<Zuora.zObject> queryResultsList = new List<Zuora.zObject>();
    if (!Test.isRunningTest()) {
      queryResultsList = zuoraApi.zQuery(zuoraQuery);
    } else {
      Zuora.zObject rpct = new Zuora.zObject('RatePlanChargeTier');
      rpct.setValue('RatePlanChargeId', '12345678901234567890123456789012');
      rpct.setValue('Price', 10);
      Zuora.zObject rpct2 = new Zuora.zObject('RatePlanChargeTier');
      rpct2.setValue('RatePlanChargeId', '12345678901234567890123456789022');
      rpct2.setValue('Price', 20);
      queryResultsList = new List<Zuora.zObject>{ rpct, rpct2 };
    }
    
    return queryResultsList;
  }

  /**
   * To get access the Zuora Api and login
   * CAUTION: consumes 1 callout so it is better to reuse the same zApi object when possible
   */
  public Zuora.zApi zuoraApiAccess() {
    Zuora.zApi zuoraApi = new Zuora.zApi();
    if (!Test.isRunningTest()) {
      zuoraApi.zlogin();
    } else {
      // nothing
    }
    return zuoraApi;
  }
    
}