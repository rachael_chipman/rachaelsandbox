public class AttachFinalController
{

    public Opportunity o {get;set;}
    public Contract c {get;set;}
    public Attachment attachment {
    get {if (attachment == null)
        attachment = new Attachment();
      return attachment;
    } 
    set; 
    }
    public Boolean uploaded {get;set;}



public AttachFinalController(ApexPages.StandardController std)
    {
        c = [SELECT Id, Opportunity__c FROM Contract WHERE Id = :ApexPages.currentPage().getParameters().get('id')];
    
        o = [SELECT Id, Final_Contract_Uploaded__c FROM Opportunity 
        WHERE Id = :c.Opportunity__c];

    }



public pageReference uploadFinal() 
    {
        // Upload the contract to the related opportunity record
        
        attachment.OwnerId = UserInfo.getUserId();
        attachment.ParentId = o.id;
        attachment.name = 'ContractFinal.pdf';
        attachment.IsPrivate = false;
        attachment.contentType = 'application/pdf';
        insert attachment;

        uploaded = true;
        o.Final_Contract_Uploaded__c = true;
        update o;
        
        pageReference returntoOpp = new pageReference('/apex/tabView?id='+o.Id);
        
        return returntoOpp;
    }
    
}