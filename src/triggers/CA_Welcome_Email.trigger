trigger CA_Welcome_Email on Certification_Application__c (after update) {

	CertAppService CertAppSvc = new CertAppService();
	CertAppSvc.processTrigger(trigger.new, trigger.oldMap);
}