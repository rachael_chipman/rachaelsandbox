public with sharing class MarkasTerminationController
{
    public Opportunity opp {get;set;}
    public list <OpportunityLineItem> olis;
    public Boolean Convert {get;set;}
    public Boolean ReqOtherOpp {get;set;}
    public static String PREVIOUS_STAGE{get;set;}
    /*public Boolean ReqProductDeactivationDate {get;set;}*/

    
    public MarkasTerminationController(ApexPages.StandardController std)
    {
        opp = [SELECT Id, Type, StageName, RecordTypeId, Approval_Type__c, Reasons_for_Lost_Opportunities__c, Other_Opportunity__c, Type_of_InvolvementText__c, Lost_Pass_Details__c, Product_Certification_App_Fee__c, Lost_Pass_Specifics__c, Special_Billing_Notes__c, Product_Deactivation_Date__c, Outstanding_Billing_Directive__c, Termination_Timing__c FROM Opportunity WHERE Id = :ApexPages.currentPage().getParameters().get('id')];
        
        olis = [SELECT Id, Start_Date__c, End_Date__c, Product_Status__c, Description__c, UnitPrice, Previous_Value__c FROM OpportunityLineItem WHERE OpportunityId = :opp.Id]; 
        
        PREVIOUS_STAGE = opp.StageName;

        ReqOtherOpp = false;
        
        /*ReqProductDeactivationDate = false;*/
    }
    
    public void CheckOpp()
    {
        if(((opp.Type=='Renewal' || opp.Type == 'Extension') && (opp.StageName=='Defining Needs' || opp.StageName=='Verbal Commitment' || opp.StageName == 'Establishing Value' ||
                                                                opp.StageName == 'Negotiating Terms' || opp.StageName == 'Signature'))|| opp.Product_Certification_App_Fee__c == true)
        {
                Convert = true;
                opp.StageName = 'Approval Required';
                opp.RecordTypeId = [SELECT Id, DeveloperName FROM RecordType WHERE DeveloperName = 'Termination' and SObjectType = 'Opportunity'].Id;
                if(opp.Type_of_InvolvementText__c != 'Reseller')
                {
                	opp.Send_Termination_Notification__c = true;
                }
                update opp;
        }
        else
        {
            Convert = false;
        }
    }
    
    public void CheckReason()
    {
        if(opp.Reasons_for_Lost_Opportunities__c == 'Closing Elsewhere')
        {
            ReqOtherOpp = true;
        }
        else
        {
            ReqOtherOpp = false;
        }
        /*if(opp.Reasons_for_Lost_Opportunities__c == 'RP Initiated' || opp.Reasons_for_Lost_Opportunities__c == 'Client Intiated')
        {
                ReqProductDeactivationDate = true;              
        }
        else
        {
                ReqProductDeactivationDate = false;
        }*/
    }
    
    public pageReference Cancel()
    {
        if(PREVIOUS_STAGE =='Defining Needs' || PREVIOUS_STAGE == 'Establishing Value')
        {
           // opp.RecordTypeId = opp.RecordTypeId = [SELECT Id, DeveloperName FROM RecordType WHERE DeveloperName = 'Consideration' and SObjectType = 'Opportunity'].Id;
           opp.RecordTypeId = opp.RecordTypeId = [SELECT Id, DeveloperName FROM RecordType WHERE DeveloperName = 'Select' and SObjectType = 'Opportunity'].Id;
        }
        else 
        {
            //opp.RecordTypeId = opp.RecordTypeId = [SELECT Id, DeveloperName FROM RecordType WHERE DeveloperName = 'Decision' and SObjectType = 'Opportunity'].Id;
             opp.RecordTypeId = opp.RecordTypeId = [SELECT Id, DeveloperName FROM RecordType WHERE DeveloperName = 'Purchase' and SObjectType = 'Opportunity'].Id;
        }
        
        opp.StageName = PREVIOUS_STAGE;
        update opp;
        pageReference toOpp = new pageReference('/apex/tabView?Id=' + opp.Id);
        return toOpp;
    }
    
    public pageReference GoBack()
    {
        pageReference toOpp = new pageReference('/apex/tabView?id='+ opp.Id);
        return toOpp; 
    }
    
    public pageReference UpdateObjects()
    {    
        if((opp.Lost_Pass_Details__c != null && opp.Lost_Pass_Specifics__c != null) && (opp.Lost_Pass_Details__c != 'Non-responsive: Non-Payment' && opp.Lost_Pass_Details__c != 'Non-responsive: Zero Volume' && opp.Lost_Pass_Details__c != 'Billing Admin (Accounting Only)') ||
        (opp.Lost_Pass_Details__c == 'Non-responsive: Non-Payment' && opp.Lost_Pass_Details__c == 'Non-responsive: Zero Volume' && opp.Lost_Pass_Details__c == 'Billing Admin (Accounting Only)' && opp.Lost_Pass_Specifics__c == null) ||
        (opp.Lost_Pass_Details__c == 'Merged into another opp' && opp.Other_Opportunity__c != null && opp.Lost_Pass_Specifics__c != null)) 
        {
            opp.StageName = 'Approval Required';
            opp.Approval_Type__c = 'Finance Approval';
            update opp;
            
            if(opp.Type == 'Renewal' || opp.Product_Certification_App_Fee__c == true)
            {
            
                for(OpportunityLineItem oli : olis)
                {
                    //oli.Start_Date__c = system.today();
                    oli.End_Date__c = system.today();
                    oli.Product_Status__c = 'Terminated';
                    oli.Description__c = 'Product Deactivation';
                    oli.Previous_Value__c = oli.UnitPrice;
                    oli.UnitPrice = 0.00;
                    update oli;
                }
            }
            
            if(opp.Type == 'Extension')
            {
                opp.Type = 'Renewal';
                update opp;
            
                for(OpportunityLineItem oli : olis) 
                { 
                    oli.Previous_Value__c = ((oli.UnitPrice / (oli.Start_Date__c.monthsBetween(oli.End_Date__c)+1)*12).SetScale(2));
                    oli.Product_Status__c = 'Terminated';
                    oli.Description__c = 'Product Deactivation';                   
                    //oli.Start_Date__c = system.today();
                    oli.End_Date__c = system.today();
                    oli.UnitPrice = 0.00;
                    update oli;
                }
                
            }
            
            PageReference oppPageRef = new pageReference('/apex/tabView?id=' + opp.id );
            oppPageRef.setRedirect( true );
            return oppPageRef;
        }
        else if((opp.Reasons_for_Lost_Opportunities__c == 'Closing Elsewhere' && opp.Other_Opportunity__c == null) || (opp.Lost_Pass_Details__c == null && opp.Reasons_for_Lost_Opportunities__c != 'Other') || opp.Lost_Pass_Specifics__c == null)
        {
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, 'Please be sure that all fields have been populated.' );
            ApexPages.addMessage(myMsg);
            return null;    
        }
        else
        {
            return null;
        } 
    }
    
}