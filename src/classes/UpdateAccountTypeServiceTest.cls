@isTest(SeeAllData=true)
public with sharing class UpdateAccountTypeServiceTest {
	
	public static Account account;
	//public static Customer_Product_Detail__c ToolsPPC;
	public static Opportunity ClosedLostOpp;
	public static Opportunity OpenOpp;
	/*public static Zuora__Subscription__c activeSub;
	public static Zuora__SubscriptionProductCharge__c charge;
	public static Zuora__Product__c zProd;
	public static zqu__ProductRatePlan__c ratePlan;
	public static zqu__ProductRatePlan__c toolsChildRatePlan;
	public static Zuora__CustomerAccount__c billingAccount;
	public static zqu__ZProduct__c zProduct;*/
	
	static void setupObjects()
	{
		account = new Account();
		account.Name = 'Test Type Field';
        //account.Type = 'Lead Level Prospect';
		insert account;

		//update account;
		
		List<Account> InsertedAccounts = [SELECT Id, Type, Name, EFP_Active_Subscriptions__c, Cert_Active_Subscriptions__c, EIS_Active_Subscriptions__c, 
		Tools_Child_Active_Subscriptions__c, Partner_Framework_Active_Subscriptions__c, Insight_Active_Subscriptions__c, ProServ_Active_Subscriptions__c,
		Count_of_Open_Opportunities__c 
		FROM Account 
		WHERE Id = :account.Id];
		system.debug('Show me inserted accounts: ' + InsertedAccounts);
		system.debug('What is the count of opps?' + InsertedAccounts[0].Count_of_Open_Opportunities__c);
		system.assertEquals('Lead Level Prospect', InsertedAccounts[0].Type, 'The account should have the correct type after insert');
		
		//billingAccount = TestUtils.createBillingAccounts(InsertedAccounts, true)[0];

		/*ToolsPPc = new Customer_Product_Detail__c();
		ToolsPPC.Product_Family__c = 'Email Intelligence for Marketers';
		ToolsPPC.Product_Name__c = 'Email Intelligence Tools Child';
		ToolsPPC.Product_Status__c = 'Active';
		ToolsPPC.Account__c = account.Id;*/
		
		OpenOpp = new Opportunity();
		OpenOpp.Name = 'Open Opportunity';
		OpenOpp.StageName = 'Defining Needs';
		OpenOpp.CloseDate = system.today().addDays(30);
		OpenOpp.AccountId = account.Id;
		
		ClosedLostOpp = new Opportunity();
		ClosedLostOpp.Name = 'Lost Opportunity';
		ClosedLostOpp.StageName = 'Lost';
		ClosedLostOpp.CloseDate = system.today();
		ClosedLostOpp.AccountId = account.Id;
		/*zProduct = TestUtils.createZProduct('Email Optimization', true);
		zProd = TestUtils.createZuoraProduct('Email Optimization', true);
		ratePlan = TestUtils.createZ_ProductRatePlan(zProduct.Id, '000-TEST-000', 'Email Intelligence Solutions', 'Email Intelligence Solutions - Gold', true);
		toolsChildRatePlan = TestUtils.createZ_ProductRatePlan(zProduct.Id, '000-TEST-000', 'Email Intelligence Solutions', 'Email Intelligence Solutions - Tools Child', true);
		activeSub = TestUtils.createZ_Subscription(account.Id, billingAccount.Id, billingAccount.Id, 'Test Subscription', 'Recurring', false);*/

	}

	private static Account testAccount_S;
	private static List<Account> testAccounts_S;
	private static Opportunity testOpportunity;
	private static Zuora__CustomerAccount__c testBillingAccount_S;
	private static Zuora__Subscription__c testSub_S;
	private static Zuora__SubscriptionProductCharge__c testSubProdCharge_S;
	private static Zuora__SubscriptionProductCharge__c testSubProdCharge2_S;
	private static Zuora__Product__c testProduct_S;
	private static Map<String, Decimal> prodKeyToPrice = new Map<String, Decimal>();

	private static Zuora__Subscription__c testSub_S1;
	private static Zuora__SubscriptionProductCharge__c testSubProdCharge_S1;

	private static void setupExistingSubscriptions()
	{
		testAccounts_S = new List<Account>();

		testAccounts_S.add(testAccount_S = TestUtils.createAccount(TestUtils.MARKETER_RECORD_TYPE.Id, true));
		system.assert(testAccount_S != NULL);

		testOpportunity = TestUtils.createOpportunity(TestUtils.RESEARCH_RECORD_TYPE.Id, testAccount_S.Id, true);
		system.assert(testOpportunity != NULL);

		/*testBillingAccount_S = TestUtils.createBillingAccounts(testAccounts_S, true)[0];
		system.assert(testBillingAccount_S != NULL);

		testProduct_S = TestUtils.createZuoraProduct('Email Optimization', true);
		system.assert(testProduct_S != NULL);

		testSub_S = TestUtils.createZ_Subscription(testAccount_S.Id, testBillingAccount_S.Id, testBillingAccount_S.Id, 'Test Subscription', 'TERMED', true);
		system.assert(testSub_S != NULL);
		testSub_S.Zuora__Status__c = 'Active';
		testSub_S.Zuora__Zuora_Id__c = TestUtils.generateZuoraId(32);
		testSub_S.Zuora__TermStartDate__c = system.today().addDays(-1);
		update testSub_S;

		testSubProdCharge_S = TestUtils.createZ_SubscriptionCharge('Certification - License - Annually', testSub_S.Id, testAccount_S.Id, testProduct_S.Id, 'Certification - License - Annually', 'Recurring', false);
		system.assert(testSubProdCharge_S != NULL);
		testSubProdCharge_S.Zuora__Product__c = testProduct_S.Id;
		testSubProdCharge_S.Zuora__ProductName__c = testProduct_S.Name;
		testSubProdCharge_S.Product_Family__c = 'Certification';
		testSubProdCharge_S.Zuora__Quantity__c = 50000;
		testSubProdCharge_S.Zuora__Type__c = ManageQuoteService.RECURRING;
		testSubProdCharge_S.Zuora__Model__c = 'Some Model';
		testSubProdCharge_S.Zuora__MonthlyRecurringRevenue__c = 36.67;
		testSubProdCharge_S.Zuora__TotalContractValue__c = testSubProdCharge_S.Zuora__MonthlyRecurringRevenue__c * 12;
		testSubProdCharge_S.Zuora__RatePlanId__c = TestUtils.generateZuoraId(32);
		insert testSubProdCharge_S;
		prodKeyToPrice.put(testSubProdCharge_S.Zuora__ProductName__c + ': ' + testSubProdCharge_S.Name, testSubProdCharge_S.Zuora__TotalContractValue__c);

		testSubProdCharge2_S = TestUtils.createZ_SubscriptionCharge('Email Intelligence Solutions - Gold - Annually', testSub_S.Id, testAccount_S.Id, testProduct_S.Id, 'Email Intelligence Solutions - Gold - Annually', 'Recurring', false);
        system.assert(testSubProdCharge2_S != NULL);
        testSubProdCharge2_S.Zuora__Product__c = testProduct_S.Id;
        testSubProdCharge2_S.Zuora__ProductName__c = testProduct_S.Name;
        testSubProdCharge2_S.Product_Family__c = 'Email Intelligence Solutions';
        testSubProdCharge2_S.Zuora__Quantity__c = 1;
        testSubProdCharge2_S.Zuora__Type__c = ManageQuoteService.RECURRING;
        testSubProdCharge2_S.Zuora__Model__c = 'Some Model';
		testSubProdCharge2_S.Zuora__MonthlyRecurringRevenue__c = 36.67;
		testSubProdCharge2_S.Zuora__TotalContractValue__c = testSubProdCharge_S.Zuora__MonthlyRecurringRevenue__c * 12;
		testSubProdCharge2_S.Zuora__RatePlanId__c = TestUtils.generateZuoraId(32);
        insert testSubProdCharge2_S;
        prodKeyToPrice.put(testSubProdCharge2_S.Zuora__ProductName__c + ': ' + testSubProdCharge2_S.Name, testSubProdCharge2_S.Zuora__TotalContractValue__c);*/
    }
	
	private static testMethod void testActiveProspect()
	{
		setupObjects();
		
		test.startTest();
		insert OpenOpp;
		test.stopTest();
		
		List<Account> InsertedAccounts = [SELECT Id, Type FROM Account WHERE Id = :account.Id];
		system.assertEquals('Active Prospect', InsertedAccounts[0].Type, 'The account should have the correct type with only Open opportunities');
	}
	
	private static testMethod void testCurrentClient()
	{
		/*setUpObjects();
		//ToolsPPC.Last_12_Months__c = 1500.00;
		activeSub.Zuora__Status__c = 'Active';
		insert activeSub;

		charge = TestUtils.createZ_SubscriptionCharge('Email Intelligence - Gold', activeSub.Id, account.Id, zProd.Id, ratePlan.Name, 'Recurring', false);
		insert charge;
		
		account.EIS_Active_Subscriptions__c = 1;
		update account;

		test.startTest();
		insert OpenOpp;
		test.stopTest();
		
		system.debug('EIS' + account.EIS_Active_Subscriptions__c);

		List<Account> InsertedAccounts = [SELECT Id, Type, EFP_Active_Subscriptions__c, Cert_Active_Subscriptions__c, EIS_Active_Subscriptions__c, Tools_Child_Active_Subscriptions__c, Partner_Framework_Active_Subscriptions__c, Insight_Active_Subscriptions__c, ProServ_Active_Subscriptions__c FROM Account WHERE Id = :account.Id];
		system.debug('Show me the account ' + InsertedAccounts);
		system.assertEquals('Current Client/Partner', InsertedAccounts[0].Type, 'The account should have the correct type with Active PPC');*/

		setupExistingSubscriptions();
		testAccount_S.EIS_Active_Subscriptions__c = 1;
		testAccount_S.Total_Active_Subscriptions__c = 3;
		testAccount_S.Tools_Child_Active_Subscriptions__c = 1;

		test.startTest();
		update testAccount_S;
		test.stopTest();

		//Query for my account and the value of the rollup
		List<Account> myAccount = [SELECT Id, Type, EIS_Active_Subscriptions__c, Count_of_Open_Opportunities__c FROM Account WHERE Id = : testAccount_S.Id];
		system.debug('Show me the account: ' + myAccount[0]);
		system.debug('what is the EIS value? ' + myAccount[0].EIS_Active_Subscriptions__c);
		system.debug('what is count of open opps?' + myAccount[0].Count_of_Open_Opportunities__c);
		//system.debug('What is the product family? ' + testSubProdCharge2_S.Product_Family__c);

		system.assert('Current Client/Partner' == myAccount[0].Type);
		
	}
    
	
	private static testMethod void testPassiveClient()
	{
		/*setUpObjects();
		activeSub.Zuora__Status__c = 'Active';
		insert activeSub;

		charge = TestUtils.createZ_SubscriptionCharge('Email Intelligence Solutions - Tools Child', activeSub.Id, account.Id, zProd.Id, toolsChildRatePlan.Name, 'Recurring', false);
		insert charge;
		
		account.Tools_Child_Active_Subscriptions__c = 1;
		update account;

		test.startTest();
		insert OpenOpp;
		test.stopTest();
		
		List<Account> InsertedAccounts = [SELECT Id, Type FROM Account WHERE Id = :account.Id];
		system.assertEquals('Passive Client', InsertedAccounts[0].Type, 'The account should have the correct type with Active 0.00 Tools Child PPC');*/
		setupExistingSubscriptions();

		/*testSub_S1 = TestUtils.createZ_Subscription(testAccount_S.Id, testBillingAccount_S.Id, testBillingAccount_S.Id, 'Test Subscription', 'TERMED', true);
		system.assert(testSub_S != NULL);
		testSub_S1.Zuora__Status__c = 'Active';
		testSub_S1.Zuora__Zuora_Id__c = TestUtils.generateZuoraId(32);
		testSub_S1.Zuora__TermStartDate__c = system.today().addDays(-1);
		update testSub_S;

		testSubProdCharge_S1 = TestUtils.createZ_SubscriptionCharge('Certification - License - Annually', testSub_S1.Id, testAccount_S.Id, testProduct_S.Id, 'Email Intelligence Solutions - Tools Child - Annually', 'Recurring', false);
		system.assert(testSubProdCharge_S != NULL);
		testSubProdCharge_S1.Zuora__Product__c = testProduct_S.Id;
		testSubProdCharge_S1.Zuora__ProductName__c = testProduct_S.Name;
		testSubProdCharge_S1.Product_Family__c = 'Email Intelligence Solutions';
		testSubProdCharge_S1.Zuora__Quantity__c = 50000;
		testSubProdCharge_S1.Zuora__Type__c = ManageQuoteService.RECURRING;
		testSubProdCharge_S1.Zuora__Model__c = 'Some Model';
		testSubProdCharge_S1.Zuora__MonthlyRecurringRevenue__c = 36.67;
		testSubProdCharge_S1.Zuora__TotalContractValue__c = testSubProdCharge_S1.Zuora__MonthlyRecurringRevenue__c * 12;
		testSubProdCharge_S1.Zuora__RatePlanId__c = TestUtils.generateZuoraId(32);
		insert testSubProdCharge_S1;
		prodKeyToPrice.put(testSubProdCharge_S1.Zuora__ProductName__c + ': ' + testSubProdCharge_S1.Name, testSubProdCharge_S1.Zuora__TotalContractValue__c);*/

		testAccount_S.Tools_Child_Active_Subscriptions__c = 1;
		testAccount_S.EIS_Active_Subscriptions__c= 1;
		testAccount_S.Total_Active_Subscriptions__c = 2;

		test.startTest();
		update testAccount_S;
		test.stopTest();

		List<Account> myAccount = [SELECT Id, Type, EIS_Active_Subscriptions__c, Total_Active_Subscriptions__c, Tools_Child_Active_Subscriptions__c, Count_of_Open_Opportunities__c FROM Account WHERE Id = : testAccount_S.Id];
		system.debug('Show me the account: ' + myAccount[0]);
		system.debug('what is the EIS value? ' + myAccount[0].Tools_Child_Active_Subscriptions__c);
		system.debug('what is the Total value? ' + myAccount[0].Total_Active_Subscriptions__c);
		system.debug('what is count of open opps?' + myAccount[0].Count_of_Open_Opportunities__c);
		//system.debug('What is the product family? ' + testSubProdCharge_S1.Product_Family__c);

		system.assert('Passive Client' == myAccount[0].Type);

		
	}
	
	private static testMethod void testInactiveProspect()
	{
		setupObjects();
		
		test.startTest();
		insert ClosedLostOpp;
		test.stopTest();
		
		List<Account> InsertedAccounts = [SELECT Id, Type FROM Account WHERE Id = :account.Id];
		system.assertEquals('Inactive Prospect', InsertedAccounts[0].Type, 'The account should have the correct type with only Lost opportunities');
	}
    
    private static Account CreateContact( string contype,string testtype)
    {
    	account = new Account();
		account.Name = 'Test Type Field';
		

		insert account;

        Contact newcon = new contact();
        newcon.LastName = 'testcon';
        newcon.FirstName = 'testcon';
        newcon.AccountId = account.Id;
        newcon.Invoicing_Contact_Type__c = contype;
        insert newcon;
        
        if(contype == 'Bill To')
		{
			account.BillingCity = 'Denver';
			account.BillingStreet = '299';
			account.BillingState = 'CO';
			account.BillingCountry = 'United State';
			account.BillingPostalCode = '80023';
		}
		else if(contype == 'Sold To')
		{
			account.ShippingCity = 'Dublin';
			account.ShippingStreet = '5678';
			account.ShippingState = 'CA';
			account.ShippingCountry = 'United State';
			account.ShippingPostalCode = '94034';
		}
        
        if(testtype == 'Negative')
        {
        	account.Name = null;
        }


        update account;

        return account;
    }
    
    private testMethod static void testBillingAddress()
    {
        
        Account acc = CreateContact('Bill TO',null);
        List<Contact> conlist = [Select Id, Name, AccountId, MailingStreet,MailingCity,MailingState,MailingPostalCode,MailingCountry,Invoicing_Contact_Type__c, Account.BillingCity,Account.BillingState, Account.BillingStreet,
			                             Account.BillingPostalCode, Account.BillingCountry, Account.ShippingCity,Account.ShippingState,Account.ShippingPostalCode,
			                             Account.ShippingCountry,Account.ShippingStreet from Contact where AccountId =:acc.Id];
	    if(!conlist.isEmpty())
	    {
	    	for(Contact Tcon: conlist)
	    	{
	    		System.assertEquals(Tcon.MailingCity,Tcon.Account.BillingCity, ' We expect Account Billing City to match the Contact Mailing City');
	    		System.assertEquals(Tcon.MailingState,Tcon.Account.BillingState, ' We expect Account Billing State to match the Contact Mailing State');
	    		System.assertEquals(Tcon.MailingStreet,Tcon.Account.BillingStreet, ' We expect Account Billing Street to match the Contact Mailing Street');
	    		System.assertEquals(Tcon.MailingCountry,Tcon.Account.BillingCountry, ' We expect Account Billing Country to match the Contact Mailing Country');
	    		System.assertEquals(Tcon.MailingPostalCode,Tcon.Account.BillingPostalCode, ' We expect Account Billing zipcode to match the Contact Mailing Zipcode');
	    	}
	    }

    }

    private testMethod static void testShippingAddress()
    {
        
        Account acc = CreateContact('Sold TO',null);
        List<Contact> conlist = [Select Id, Name, AccountId, MailingStreet,MailingCity,MailingState,MailingPostalCode,MailingCountry,Invoicing_Contact_Type__c, Account.BillingCity,Account.BillingState, Account.BillingStreet,
			                             Account.BillingPostalCode, Account.BillingCountry, Account.ShippingCity,Account.ShippingState,Account.ShippingPostalCode,
			                             Account.ShippingCountry,Account.ShippingStreet from Contact where AccountId =:acc.Id];
	    if(!conlist.isEmpty())
	    {
	    	for(Contact Tcon: conlist)
	    	{
	    		System.assertEquals(Tcon.MailingCity,Tcon.Account.ShippingCity, ' We expect Account Shipping City to match the Contact Mailing City');
	    		System.assertEquals(Tcon.MailingState,Tcon.Account.ShippingState, ' We expect Account Shipping State to match the Contact Mailing State');
	    		System.assertEquals(Tcon.MailingStreet,Tcon.Account.ShippingStreet, ' We expect Account Shipping Street to match the Contact Mailing Street');
	    		System.assertEquals(Tcon.MailingCountry,Tcon.Account.ShippingCountry, ' We expect Account Shipping Country to match the Contact Mailing Country');
	    		System.assertEquals(Tcon.MailingPostalCode,Tcon.Account.ShippingPostalCode, ' We expect Account Shipping zipcode to match the Contact Mailing Zipcode');
	    	}
	    }
    }

  /*  private testMethod static void testNegativeShippingAddress()
    {
        
        Account acc = CreateContact('Sold TO','Negative');
        List<Contact> conlist = [Select Id, Name, AccountId, MailingStreet,MailingCity,MailingState,MailingPostalCode,MailingCountry,Invoicing_Contact_Type__c, Account.BillingCity,Account.BillingState, Account.BillingStreet,
			                             Account.BillingPostalCode, Account.BillingCountry, Account.ShippingCity,Account.ShippingState,Account.ShippingPostalCode,
			                             Account.ShippingCountry,Account.ShippingStreet from Contact where AccountId =:acc.Id];
	    if(!conlist.isEmpty())
	    {
	    	for(Contact Tcon: conlist)
	    	{
	    		System.assertnotEquals(Tcon.MailingCity,Tcon.Account.ShippingCity, ' We expect Account Shipping City to match the Contact Mailing City');
	    		System.assertnotEquals(Tcon.MailingState,Tcon.Account.ShippingState, ' We expect Account Shipping State to match the Contact Mailing State');
	    		System.assertnotEquals(Tcon.MailingStreet,Tcon.Account.ShippingStreet, ' We expect Account Shipping Street to match the Contact Mailing Street');
	    		System.assertnotEquals(Tcon.MailingCountry,Tcon.Account.ShippingCountry, ' We expect Account Shipping Country to match the Contact Mailing Country');
	    		System.assertnotEquals(Tcon.MailingPostalCode,Tcon.Account.ShippingPostalCode, ' We expect Account Shipping zipcode to match the Contact Mailing Zipcode');
	    	}
	    }
    }*/
}