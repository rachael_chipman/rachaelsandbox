@isTest
private class productEntryControllerTest {
	
	/*public static testMethod void test1() {
		Account a = new Account (name='test account');
		insert a;
		
		opportunity o = new Opportunity(name='test opp', closedate = date.today(), accountid = a.id, stagename = 'Pre-Pipeline');
		insert o;
		
		PageReference p = Page.productEntry;
		p.getParameters().put('addProduct','true');
		p.getParameters().put('id',o.id);
		test.setCurrentPage(p);
		
		ApexPages.standardController std = new ApexPages.StandardController(o);
		productEntryController extension = new productEntryController(std);
		
		o.Number_of_Domains__c = 1;
		o.Monthly_Send_Volume__c = 5000000;
		o.Monthly_Campaigns__c = 25;
		update o;
		
		extension.family = 'Certification';
		extension.getTable();
	}
	
	/*public static testMethod void test2() {
		Account a = new Account (name='test account');
		insert a;
		
		opportunity o = new Opportunity(name='test opp', closedate = date.today(), accountid = a.id, stagename = 'Pre-Pipeline');
		insert o;
		
		PageReference p = Page.productEntry;
		p.getParameters().put('addProduct','true');
		p.getParameters().put('id',o.id);
		test.setCurrentPage(p);
		
		ApexPages.standardController std = new ApexPages.StandardController(o);
		productEntryController extension = new productEntryController(std);
		
		o.Number_of_Domains__c = 1;
		o.Monthly_Send_Volume__c = 5000000;
		o.Monthly_Campaigns__c = 25;
		update o;
		
		extension.family = 'Inbox Measurement';
		extension.getTable();
		
	}*/

	final static Integer NUM_OF_OLIS = 3;
	static Account testAccount;
	static Opportunity testOpportunity;
	static List<OpportunityLineItem> testOpportunityLineItems;
	static ProductEntryController testController;

	static void setup()
	{
		testAccount = TestUtils.createAccount( TestUtils.MARKETER_RECORD_TYPE.Id, true );
		//testOpportunity = TestUtils.createOpportunity( TestUtils.CONSIDERATION_RECORD_TYPE.Id, testAccount.Id, false );
		testOpportunity = TestUtils.createOpportunity( TestUtils.SELECT_RECORD_TYPE.Id, testAccount.Id, false );
		insert testOpportunity;
		
		Product2 testProduct = TestUtils.createProduct( true );
		PricebookEntry testPricebookEntry = TestUtils.createPricebookEntry( testProduct.Id, TestUtils.STANDARD_PRICE_BOOK.Id, true );
		
		testOpportunityLineItems = new List<OpportunityLineItem>();
		for ( Integer i = 0 ; i < NUM_OF_OLIS ; i++ )
		{
			OpportunityLineItem testOLI = TestUtils.createOpportunityLineItem( testOpportunity.Id, testPricebookEntry.Id, false );
			testOpportunityLineItems.add( testOLI );
		}
		insert testOpportunityLineItems;
		
	}

	private static testMethod void testProductEntryController_Constructor()
	{
		setup();
		PageReference p = Page.productEntry;
		p.getParameters().put( 'addProduct','true' );
		p.getParameters().put( 'id', testOpportunity.Id );
		Test.setCurrentPage( p );
		
		Test.startTest();
		
			ApexPages.StandardController stdController = new ApexPages.StandardController( testOpportunity );
			testController = new productEntryController( stdController );
		
		Test.stopTest();
		
		System.assertEquals( NUM_OF_OLIS, testController.shoppingCart.size(), 'The number of cart items in our shopping cart should match the number of opportunity line items we created' );
	}
	
	private static testMethod void testSetOli()
	{
		setup();
		PageReference p = Page.productEntry;
		p.getParameters().put( 'addProduct','true' );
		p.getParameters().put( 'id', testOpportunity.Id );
		Test.setCurrentPage( p );
		ApexPages.StandardController stdController = new ApexPages.StandardController( testOpportunity );
		testController = new productEntryController( stdController );
			
		Test.startTest();
		
			testController.toEdit = testOpportunityLineItems[0].Id;
			OpportunityLineItem actualOLI = testController.getOli();
			
		Test.stopTest();
		
		System.assertEquals( testOpportunityLineItems[0].Id, actualOLI.Id, 'The Id of our OLI returned for editing should match the first of our list of test OLIs' );
	}
	
}