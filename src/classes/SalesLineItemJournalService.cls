public without sharing class SalesLineItemJournalService {
	
	public static List<c2g__codaInvoiceLineItem__c> filterOnscheduleGroupAdded(Map<ID,c2g__codaInvoiceLineItem__c> oldLineItemsMap,List<c2g__codaInvoiceLineItem__c> newLineItems){		
		return Select.Field.isNew( c2g__codaInvoiceLineItem__c.c2g__IncomeScheduleGroup__c).filter( newLineItems, oldLineItemsMap );		
	}

	public static void populateJournalOpportunity(List<c2g__codaInvoiceLineItem__c> oldLineItems){
		
		Map<ID,List<c2g__codaInvoiceLineItem__c>> groupIdLineItemsMap = new Map<ID,List<c2g__codaInvoiceLineItem__c>>();
		for (c2g__codaInvoiceLineItem__c lineItem : oldLineItems){
			if (!groupIdLineItemsMap.containsKey(lineItem.c2g__IncomeScheduleGroup__c)){
				groupIdLineItemsMap.put(lineItem.c2g__IncomeScheduleGroup__c,new List<c2g__codaInvoiceLineItem__c>());
			}
			groupIdLineItemsMap.get(lineItem.c2g__IncomeScheduleGroup__c).add(lineItem);
		}

		Map<ID,c2g__codaGroupingReference__c> groupJournalsMap = new Map<ID,c2g__codaGroupingReference__c>([Select Name, Id,
																			(Select id, c2g__IncomeScheduleGroup__c, Opportunity__c From c2g__Journals__r) 
																		From c2g__codaGroupingReference__c where id in : groupIdLineItemsMap.keySet()]);

		Map<ID,List<c2g__codaJournal__c >> groupIdwithJournalsMap = new Map<ID,List<c2g__codaJournal__c >> ();
		Set<id> journalIds = new Set<id>();
		for (c2g__codaGroupingReference__c grouping: groupJournalsMap.values()){
			if (grouping.c2g__Journals__r != null && !grouping.c2g__Journals__r.isEmpty()){				
				for (c2g__codaJournal__c journal: grouping.c2g__Journals__r){
					if (!groupIdwithJournalsMap.containsKey(grouping.id)){
						groupIdwithJournalsMap.put(grouping.id,new List<c2g__codaJournal__c>());
					}
					groupIdwithJournalsMap.get(grouping.id).add(journal);
					journalIds.add(journal.id);
				}
			}
		}
		
		List<c2g__codaInvoiceLineItem__c> lineItems = [Select Id, Name, c2g__IncomeScheduleGroup__c,c2g__Invoice__c,c2g__Invoice__r.c2g__Opportunity__c From  c2g__codaInvoiceLineItem__c 
														where c2g__IncomeScheduleGroup__c in : groupIdwithJournalsMap.keySet() ];
		List<c2g__codaJournalLineItem__c> journalLineItems = [Select c2g__Journal__c, Opportunity__c From c2g__codaJournalLineItem__c  where c2g__Journal__c in : journalIds];

		List<c2g__codaJournal__c> journalsWithGroups = new List<c2g__codaJournal__c>();												
		for (c2g__codaInvoiceLineItem__c lineItem : lineItems){
			if (groupIdwithJournalsMap.containsKey(lineItem.c2g__IncomeScheduleGroup__c)){
				for (c2g__codaJournal__c journal : groupIdwithJournalsMap.get(lineItem.c2g__IncomeScheduleGroup__c)){
					Id oppId = lineItem.c2g__Invoice__r.c2g__Opportunity__c;
					c2g__codaJournal__c codaJournal  = new c2g__codaJournal__c( id = journal.id, Opportunity__c = oppId);
					journalsWithGroups.add(new c2g__codaJournal__c( id = journal.id, Opportunity__c = oppId, c2g__IncomeScheduleGroup__c = journal.c2g__IncomeScheduleGroup__c));
				}
			}
		}
 
		try {
			
			update journalsWithGroups;
			
		} catch (DMLException exc){
			for( Integer index = 0; index < exc.getNumDml(); index++ ){
				List<c2g__codaInvoiceLineItem__c> triggerLineItems = groupIdLineItemsMap.get(journalsWithGroups[exc.getDmlIndex(index)].c2g__IncomeScheduleGroup__c);
				for (c2g__codaInvoiceLineItem__c triggerLineItem : triggerLineItems ){
					triggerLineItem.addError(exc.getDmlMessage(index));
				}						
			}
		}	                   
	}

}