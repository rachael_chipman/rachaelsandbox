@isTest
private class Opp_Team_Update_Test 
{
     static testMethod void Opp_Team_Update_Test1() 
     {
        User testUser = TestUtils.getTestUser( 'testUser', 'Standard User' );
        System.runAs ( TestUtils.ADMIN_USER )
        {
            insert testUser;
        }

        Account acct = new Account();
        acct.name = 'RPTestAcct1';
        acct.accountmanager__c = 'BIO Automation';
        insert acct;   
        
        Opportunity op = new Opportunity (
        Name = 'Naming Convention WFR',
        AccountId = acct.Id,
        RecordTypeId = '012000000004wVU',
        StageName = 'Finance Approved',
        CloseDate = system.Today(),
        Type = 'Fee',
        OwnerId = testUser.Id,
        CampaignId = '701000000004fLn',
        Product__c = 'Certification',
        SSC_Volume__c = 'Tier 4',
        Pricebook2Id = '01s000000004NS6AAM',
        Certification_Type__c = 'Application Fee');
        insert op;
              
        OpportunityTeamMember otm = new OpportunityTeamMember (
        OpportunityId = op.Id,
        UserId = testUser.Id,
        TeamMemberRole = 'Sales Rep');
         
        insert otm;
             
        op.OwnerId = testUser.Id;
        update op; 
    }
}