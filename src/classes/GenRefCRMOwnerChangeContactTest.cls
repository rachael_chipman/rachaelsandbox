@isTest
private with sharing class GenRefCRMOwnerChangeContactTest {
	
	private static testMethod void testContactAssignmentWithPromoCode()
	{
        
        Id marketerRecordTypeId = [SELECT Id From RecordType Where Name = 'Marketer' AND sObjectType = 'Account'].Id;
        User testUser = TestUtils.getTestUser( 'TestUser123', 'Exec Admin');
        
        System.runAs( TestUtils.ADMIN_USER ){
           insert testUser;
        }
        
        Account testAccount = TestUtils.createAccount( marketerRecordTypeId, false );
        testAccount.OwnerId = testUser.Id;
        testAccount.Customer_Relationship_Manager_Primary__c = 'Test';
        testAccount.CRM__c = testUser.Id;
        insert testAccount;
        
        Promo_Code__c testPromo = TestUtils.createPromoCode( 'TestCode', 'Referral', testAccount.Id, false );
        testPromo.Account__c = testAccount.Id;
        insert testPromo;
        
        Contact testContact = TestUtils.createContacts( 'Test', 'Contact', testAccount.Id, false );
        testContact.Promo_Code__c = 'TestCode';
        testContact.Gen_Ref_Contact__c = false;
        insert testContact;
        
        Test.startTest();
        testContact.Campaign_Name__c = '2012_GL_PRO_WF_.NET Deliverability Channel Referral Form NEW';
        update testContact;
        Test.stopTest();
        
        List<Contact> updatedContact = [SELECT Id, Referring_Partner__c, Referring_CRM__c, OwnerId FROM Contact];
        
        System.assertEquals ( 1, updatedContact.size(), 'There should be one contact created in the test');
        System.assertEquals ( testAccount.Id, updatedContact[0].Referring_Partner__c, 'Referring_Partner__c should be populated with the accountId');
        System.assertEquals ( testUser.Id, updatedContact[0].Referring_CRM__c, 'Referring_CRM__c should be populated from the account');
        System.assertEquals ( testUser.Id, updatedContact[0].OwnerId, 'There should be one contact created in the test');
	}
	
	private static testMethod void testContactAssignmentWithoutPromoCode()
	{
        
        Id marketerRecordTypeId = [SELECT Id From RecordType Where Name = 'Marketer' AND sObjectType = 'Account'].Id;
        User testUser = TestUtils.getTestUser( 'TestUser123', 'Exec Admin');
        
        System.runAs( TestUtils.ADMIN_USER ){
           insert testUser;
        }
        
        Account testAccount = TestUtils.createAccount( marketerRecordTypeId, false );
        testAccount.OwnerId = testUser.Id;
        testAccount.Customer_Relationship_Manager_Primary__c = 'Test';
        testAccount.CRM__c = testUser.Id;
        insert testAccount;
        
        Contact testContact = TestUtils.createContacts( 'Test', 'Contact', testAccount.Id, false );
        testContact.Gen_Ref_Contact__c = false;
        insert testContact;
        
        Test.startTest();
        testContact.Campaign_Name__c = '2012_GL_PRO_WF_.NET Deliverability Channel Referral Form NEW';
        update testContact;
        Test.stopTest();
        
        List<Contact> updatedContact = [SELECT Id, Referring_Partner__c, Referring_CRM__c, OwnerId FROM Contact];
        
        System.assertEquals ( 1, updatedContact.size(), 'There should be one contact created in the test');
        System.assertEquals ( GenRefCRMOwnerChangeContact.genRefDefaultUser.Id, updatedContact[0].Referring_CRM__c, 'Referring_CRM__c should be the default Gen Ref User');
        System.assertEquals ( GenRefCRMOwnerChangeContact.genRefDefaultUser.Id, updatedContact[0].OwnerId, 'The owner should be the default Gen Ref User');
	}

}