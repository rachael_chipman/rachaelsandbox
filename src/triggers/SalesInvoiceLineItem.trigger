trigger SalesInvoiceLineItem on c2g__codaInvoiceLineItem__c (before update, after update) {

	if (Trigger.isAfter && Trigger.isUpdate){
		List<c2g__codaInvoiceLineItem__c> newGroupJournals = SalesLineItemJournalService.filterOnscheduleGroupAdded(Trigger.oldMap,Trigger.new);
		if (newGroupJournals != null && !newGroupJournals.isEmpty()){
			SalesLineItemJournalService.populateJournalOpportunity(newGroupJournals);
		}
	}

	/*if( Trigger.isBefore && Trigger.isUpdate )
	{
		if( trigger.new[0].ffbilling__CalculateIncomeSchedule__c && !trigger.old[0].ffbilling__CalculateIncomeSchedule__c )
		{
			Map<Id, c2g__codaScheduleLineItem__c> incomeScheduleLineItems = new Map<Id, c2g__codaScheduleLineItem__c>( [ SELECT Id, Period_Month__c, c2g__Period__c, c2g__LineNumber__c,
        																												c2g__Period__r.Name, SILI_End_Date_Month__c, Period_Change_Needed__c,
																														c2g__Amount__c, Sales_Invoice_Line_Item_End_Date__c, Sales_Invoice_Line_Item_Start_Date__c, SILI_Start_Date_Month__c,
																														c2g__SalesInvoiceLineItem__r.Price_Per_Day__c, c2g__Period__r.c2g__EndDate__c, c2g__Period__r.c2g__StartDate__c, 
																														c2g__Period__r.Days_in_Period__c, Opportunity__r.Billing_Type__c, Opportunity__r.Close_Date_Formula__c, 
																														Opportunity__r.Close_Date_Period__c, c2g__SalesInvoiceLineItem__r.c2g__UnitPrice__c, c2g__SalesInvoiceLineItem__r.c2g__Quantity__c,
																														c2g__SalesInvoiceLineItem__r.ffbilling__ScheduleNetTotal__c
																														FROM c2g__codaScheduleLineItem__c WHERE c2g__SalesInvoiceLineItem__c = :trigger.new[0].Id ] );
			System.assert( false, incomeScheduleLineItems.size() );
		}
	}*/	
}