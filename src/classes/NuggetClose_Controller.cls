public class NuggetClose_Controller {

Public Nugget__c n; 

    public NuggetClose_Controller(ApexPages.StandardController controller) {
    
    n = [select Id, OwnerId, Account_Matches__c, Lead_Matches__c, Promoted_Closed__c, Promoted_Account__c, First_Opened_by_Sales_Rep__c, Pod_Calc__c, Close_Date_Time__c,
      Returned_to_RD__c, Company_Name__c, D_U_N_S_Number__c, City__c, State__c, Country__c from Nugget__c where Id = :ApexPages.currentPage().getParameters().get('Id')];

    }
    
    Public PageReference closenugget(){
    
    
    n.Status__c = 'Closed';
    update n;
    
    pageReference ref = new PageReference('/'+n.Id);
    ref.setRedirect(true);
    return ref;
    
    }
    

}