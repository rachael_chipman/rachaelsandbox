public class CloneOpportunityContExt
{
    public final static String CLONED_STAGE = 'Defining Needs';
    public final static String CLONED_APPROVAL_TYPE = 'Pricing Approval';
    public final static String RET_URL = 'retURL';
     
    static RecordType SELECT_RECORD_TYPE
    { 
        get{
            if( SELECT_RECORD_TYPE == NULL )
            {
                SELECT_RECORD_TYPE = [SELECT Id FROM RecordType WHERE sObjectType = 'Opportunity' AND DeveloperName = 'Select' ];
            }
            return SELECT_RECORD_TYPE;
        } private set;
    }
    
    //Map of fields on the parent object ( opportunity in this case ) that should not be cloned and the values that they should be to
    static Map<String, object> excludedFieldToNewValue = new Map<String, object>{  'StageName' => CLONED_STAGE,
                                                                                   'RecordTypeId' => TestUtils.SELECT_RECORD_TYPE.Id,
                                                                                   'Run_ACV__c' => NULL,
                                                                                   'Send_Monkey_Email__c' => NULL,
                                                                                   'Approval_Type__c' => CLONED_APPROVAL_TYPE,
                                                                                   'RD_Pricing_Review__c' => NULL,
                                                                                   'Reporting_Date__c' => NULL,
                                                                                   'Certification_Team_Owner__c' => NULL,
                                                                                   'Contract__c' => NULL,
                                                                                   'Previous_Opportunity_Lookup__c' => NULL,
                                                                                   'Invoice_Created__c' => false,
                                                                                   'CloseDate' => SYSTEM.TODAY()}; 
     

    public Opportunity opportunity {get; set;}
    
    public CloneOpportunityContExt( ApexPages.StandardController controller )
    {
        opportunity = (Opportunity)controller.getRecord();
    }
    
    //Called by the page cloneOpportunityWithProducts.Clones opportunity with child records of a specified object type and returns the url for the edit page.
    public PageReference cloneOpportunityWithProducts()
    { 
        PageReference clonedOpportunityPage;
        Savepoint sp = Database.setSavepoint();
        
        //Map of the child object name to the name of the field the child object uses to reference the parent object
        Map<String, String> childObjAPINameToParentField = new Map<String, String> {'OpportunityLineItem' => 'OpportunityId'};

        //Map of what fields on the child records that should not be cloned to what they should be set to in the clone 
        Map<String, object> excludedChildFieldToNewValue = new Map<String, object>{ 'TotalPrice' => NULL, 
                                                                                    'Start_Date__c' => NULL,
                                                                                    'End_Date__c' => NULL};

        Id clonedOpportunityId;
        
        try
        {   
            //Params are in this order: Set of Ids for which records to clone, the Name of the object to clone, child record maps to clone, excluded parent fields and the excluded child fields
            clonedOpportunityId = CloneServices.startsClone( new Set<Id> {opportunity.Id}, 'Opportunity', childObjAPINameToParentField, excludedFieldToNewValue, excludedChildFieldToNewValue)[0];
        }
        catch (CloneServices.CloneServiceException ex)
        {
            Database.rollback( sp );
            ApexPages.addMessages( ex );
            return null;
        }
        
        //creates the URL for the edit page of the cloned opportunity
        clonedOpportunityPage = new PageReference('/' + clonedOpportunityId + '/e' );
        clonedOpportunityPage.getParameters().put(RET_URL, clonedOpportunityId);
        return clonedOpportunityPage;
    }
    
    //Called by the page cloneOpportunityWithoutProduct. Clones opportunity but none of the child records
    public PageReference cloneOpportunityWithoutProducts()
    {
        PageReference clonedOpportunityPage;
        Savepoint sp = Database.setSavepoint();
        Id clonedOpportunityId;
        
        try
        {   
            //Params are in this order: Set of Ids for which records to clone, the Name of the object to clone, child record maps to clone, excluded parent fields and the excluded child fields
            clonedOpportunityId = CloneServices.startsClone( new Set<Id> {opportunity.Id}, 'Opportunity', new Map<String,String>(), excludedFieldToNewValue, new Map<String, object>())[0];
        }
        catch (CloneServices.CloneServiceException ex)
        {
            Database.rollback( sp );
            ApexPages.addMessages( ex );
            return null;
        }
        //creates the URL for the edit page of the cloned opportunity
        clonedOpportunityPage = new PageReference('/' + clonedOpportunityId + '/e' );
        clonedOpportunityPage.getParameters().put(RET_URL, clonedOpportunityId);
        
        return clonedOpportunityPage;
    }
}