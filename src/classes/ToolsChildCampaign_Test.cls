@ isTest (SeeAllData = true)
public class ToolsChildCampaign_Test {
    static testMethod void TestToolsChildCampaign() {
    Campaign toolsChildCampaign = new Campaign (Name = '2013_GL_ALL_IT_Internal Tools Child Accounts', isActive = true);
        insert toolsChildCampaign;
    c2g__codaGeneralLedgerAccount__c gLAcct = TestUtils.createGeneralLedgerAccount(true);
        
    Account acct = new Account();
        acct.name = 'RPTestacct1';
        acct.BillingCountry = 'USA';
        acct.c2g__CODAAccountsReceivableControl__c = gLAcct.Id;
        Insert acct;
        
        Opportunity opp1 = new Opportunity(
        RecordtypeId = TestUtils.TOOLS_CHILD_RECORD_TYPE.Id,
        AccountId = acct.id, 
        Name = 'RPTestOpp1', 
        Pricebook2Id = '01s000000004NS6AAM',
        StageName = 'Signature', 
        CloseDate = system.today(), 
        Type = 'New Business',
        Billing_Terms__c = 'Net 30',        
        Invoice_Created__c = false,
        Billing_Type__c = 'Annual',
        currencyISOcode = 'USD'
                
        );
        
        Insert opp1;
        
        }
        }