trigger Account on Account (before insert, before update, after update, after insert) {

   /*if (Trigger.isBefore && Trigger.isInsert) 
    {
        /* AccountService.populateAccountsReceivableControl(Trigger.new); 
       /* UpdateAccountTypeService.populateAccountTypeonInsert(Trigger.new);
        //updateAccountOwnerfromRM.processTrigger(Trigger.new);
	    AccountOwnerChange.processTrigger( trigger.new, trigger.oldMap); 
        
    }

 /*  if(Trigger.isBefore && Trigger.isUpdate)
    {
        UpdateAccountTypeService.processTrigger(Trigger.new, Trigger.oldMap);
        //updateAccountOwnerfromRM.processTrigger(Trigger.new);
	    AccountOwnerChange.processTrigger( trigger.new, trigger.oldMap);
	    Standarized_State_And_Country_Service.ProcessAccountTrigger(trigger.oldmap,trigger.new); 
    }*/
    
    //update the address for all the contacts if the shipping address/mailing address is changed.
    if(Trigger.isBefore && ( Trigger.isInsert || Trigger.isUpdate))
    {
    	if(trigger.isInsert)
    	{
    		Standarized_State_And_Country_Service.ProcessAccountTrigger(null,trigger.new);
    		UpdateAccountTypeService.populateAccountTypeonInsert(Trigger.new);
    		AccountOwnerChange.processTrigger( trigger.new, trigger.oldMap); 
        }
        else if(trigger.isUpdate )
        {
            Standarized_State_And_Country_Service.ProcessAccountTrigger(trigger.oldmap,trigger.new); 
            UpdateAccountTypeService.processTrigger(Trigger.new, Trigger.oldMap);
            AccountOwnerChange.processTrigger( trigger.new, trigger.oldMap);
            //UpdateAccountTypeService.processContactAddressUpdates(trigger.new, trigger.oldMap);
        }
    }



    if(Trigger.isAfter && (Trigger.isUpdate || Trigger.isInsert))
    {
        
		UpdateAccountTypeService.processContactAddressUpdates(trigger.new, trigger.oldMap);
	    //List of Shares to Insert
	    List<AccountShare> sharesToInsert = new List<AccountShare>();
	    
	    for(Account account : trigger.new)
	    {	
	    	
			if(account.Technical_Account_Manager__c != null)
			{
				if( Trigger.isInsert || ( Trigger.isUpdate && trigger.oldMap.get(account.Id).Technical_Account_Manager__c != account.Technical_Account_Manager__c ))
				{
					AccountShare acctShare = new AccountShare();
					acctShare.AccountId = account.Id;
					acctShare.AccountAccessLevel = 'Edit';
					acctShare.ContactAccessLevel = 'Edit';
					acctShare.OpportunityAccessLevel = 'Edit';
					acctShare.UserOrGroupId = account.Technical_Account_Manager__c;
					sharesToInsert.add(acctShare);
				}
			}
	    }
	    try
	    {
	    	database.insert(sharesToInsert);
	    }
	    catch (System.DMLException ex)
	    {
	    	for( Integer index = 0; index < ex.getNumDml(); index++ )
           {
               Id errorId = sharesToInsert[ ex.getDmlIndex( index ) ].AccountId;
               trigger.newMap.get(errorId).addError( ex.getDmlMessage( index ) );
           }
         return;  	
	    }
    }

    if(Trigger.isUpdate && Trigger.isAfter)
    {
        NamingConventionService.filterAccountName(Trigger.new,Trigger.oldmap);
    }

}