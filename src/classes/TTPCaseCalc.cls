public class TTPCaseCalc {
    
    public static datetime open;
    public static datetime created;
    public static datetime hold;
    public static datetime closed;
    
    public static List<Case> filteredCasesOnUpdate(List<Case> newCases, Map<Id, Case> oldCases){
        List<Case> filteredCases = new List<Case>();
        for(Case newCase: newCases){
            Case oldCase;
            if(oldCases!=Null){
                oldCase = oldCases.get(newCase.Id);
                
            if(oldCase.Open_Date_Time__c != newCase.Open_Date_Time__c || 
               oldCase.On_Hold_Date_Time__c!=newCase.On_Hold_Date_Time__c || 
               oldCase.Closed_Date_Time__c!= newCase.Closed_Date_Time__c)
            {
                   filteredCases.add(newCase);
            }
            }
        }
            
        system.debug('whats in filtered cases'+ filteredCases);
        return filteredCases;
        
    }
    
    public static void processCalculation(List<Case> filteredCases, Map<Id, Case> oldCases)
    {
        BusinessHours stdBusinessHours = [SELECT Id FROM businesshours WHERE isDefault = true];
        List<Case> casesToAdd = new List<Case>();
        
        try{ 
            for(Case currentCase: filteredCases){
            
            Case oldCase = oldCases.get(currentCase.Id);
                          
            
            //Calculate TTP New to Assigned
                if(currentCase.Open_Date_Time__c!=Null && currentCase.Created_Date_Time__c!=Null)
                	{	

                        created = currentCase.Created_Date_Time__c;
                        created.format('h:mm a', 'GMT'); 
                        
                        open = currentCase.Open_Date_Time__c;
                        open.format('h:mm a', 'GMT');
                    
                        currentCase.TTP_New_to_Assigned__c = BusinessHours.diff(stdBusinessHours.id, created , open)/1000/60/60;
                        
                        casesToAdd.add(currentCase);
        		
                	}
        
                
            
            //Calculate TTP Assigned to First Response
            if(currentCase.On_Hold_Date_Time__c!=Null && currentCase.Open_Date_Time__c!=Null)
            		{
                
                        open = currentCase.Open_Date_Time__c;
                        open.format('h:mm a', 'GMT');
                        
                        hold = currentCase.On_Hold_Date_Time__c;
                        hold.format('h:mm a', 'GMT');
                        
                        currentCase.TTP_Assigned_to_First_Response__c = BusinessHours.diff(stdBusinessHours.id, open, hold)/1000/60/60;
                        
                        casesToAdd.add(currentCase);
                	}	
                      
            //Calculate TTP New to Closed
            if(currentCase.Closed_Date_Time__c!=Null && currentCase.Created_Date_Time__c !=Null)
            	{
                                
                        closed = currentCase.Closed_Date_Time__c;
                        closed.format('h:mm a', 'GMT');
                        
                        created = currentCase.Created_Date_Time__c;
                        created.format('h:mm a', 'GMT');
                        
                        currentCase.TTP_New_to_Closed__c = BusinessHours.diff(stdBusinessHours.id, created, closed)/1000/60/60;
                
                if(currentCase.On_Hold_Date_Time__c!=Null)
                	{
                    
                            hold = currentCase.On_Hold_Date_Time__c;
                            hold.format('h:mm a', 'GMT');
                            
                            currentCase.TTP_First_Response_to_Closed__c = BusinessHours.diff(stdBusinesshours.id, hold, closed)/1000/60/60;
                    
                	}	
                
                casesToAdd.add(currentCase);

            	}	
            
            if(currentCase.Closed_Date_Time__c==null && oldCase.Closed_Date_Time__c!= currentCase.Closed_Date_Time__c){
                
                currentCase.TTP_New_to_Closed__c = Null;
                currentCase.TTP_First_Response_to_Closed__c = Null;
                
            }
            
        }          
        }
        
        catch(exception e){
            for(Case ca : casesToAdd){
                ca.addError('There was a problem calculating the TTP fields, please contact salesforcerequests@returnpath.com');
            }
        }
    }
    
    public void processTrigger(List<Case> newCases, Map<Id, Case> oldCases){
        List<Case> casesToProcess = filteredCasesOnUpdate(newCases, oldCases);
        if(!casesToProcess.isEmpty()){
            processCalculation(casesToProcess, oldCases);
        }
    }

}