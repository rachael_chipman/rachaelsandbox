@isTest (SeeAllData=true)
public with sharing class OTMTriggerTest {
	
	static Opportunity testOpp;
	static OpportunityTeamMember testMember;
	
	static void setupOppAndOtm()
	{
		Account a = new Account (Name = 'Test Account');
		insert a;
		
		testOpp = new Opportunity(
		AccountId = a.Id, 
		Name = 'Test Opp',
		StageName = 'Pre-Pipeline',
		Type = 'New Business',
		CloseDate = system.today()+30);
		
		insert testOpp;
		
		testMember = new OpportunityTeamMember(
		OpportunityId = testOpp.Id,
		UserId = [SELECT Id, ProfileId, isActive FROM User WHERE ProfileId = '00e000000073QIq' and isActive = true LIMIT 1].Id,
		TeamMemberRole = 'Sales');
	}
	
	private static testMethod void OtmPercentCommissionedOnInsertTest()
	{
		setupOppAndOtm();
		
		test.startTest();
		insert testMember;
		test.stopTest();
		
		List<OpportunityTeamMember> teamMembers = [SELECT Id, OpportunityId, Percent_Commissioned_On__c FROM OpportunityTeamMember WHERE OpportunityId = :testOpp.Id];
		system.assertEquals(100, teamMembers[0].Percent_Commissioned_On__c, 'Percent Commissioned On should be 100%');
		
	}
	
	private static testMethod void OtmPercentCommissionedOnUpdateTest()
	{
		setupOppAndOtm();
		insert testMember;
		test.startTest();
		testMember.Percent_Commissioned_On__c = null;
		update testMember;
		test.stopTest();
		
		List<OpportunityTeamMember> teamMembers = [SELECT Id, OpportunityId, Percent_Commissioned_On__c FROM OpportunityTeamMember WHERE OpportunityId = :testOpp.Id];
		system.assertEquals(100, teamMembers[0].Percent_Commissioned_On__c, 'Percent Commissioned On should be 100%');
		
	}

}