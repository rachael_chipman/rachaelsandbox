@isTest
private class TestTransactionServices
{
	static Opportunity testCreditNoteOpportunity;
	static Opportunity testInvoiceOpportunity;
	static Opportunity testJournalOpportunity;

	static Account testAccount;

	static List<c2g__codaCreditNote__c> testCreditNotes;
	static List<c2g__codaInvoice__c> testInvoices;
	static List<c2g__codaJournal__c> testJournals;

	static c2g__codaCreditNote__c testCreditNote;
	static c2g__codaInvoice__c testInvoice;
	static c2g__codaJournal__c testJournal;

	static product2 testProduct;
	static c2g__codaGeneralLedgerAccount__c testGeneralLedgerAccount;

	static void setup()
	{
		testGeneralLedgerAccount = TestUtils.createGeneralLedgerAccount(true);
		testAccount = TestUtils.createAccount_FinancialForce( TestUtils.PARTNER_RECORD_TYPE.Id, testGeneralLedgerAccount.Id, true );
		testCreditNoteOpportunity = TestUtils.createOpportunity( TestUtils.RESEARCH_RECORD_TYPE.Id, testAccount.Id, false );
		testInvoiceOpportunity = TestUtils.createOpportunity( TestUtils.RESEARCH_RECORD_TYPE.Id, testAccount.Id, false );
		testJournalOpportunity = TestUtils.createOpportunity( TestUtils.RESEARCH_RECORD_TYPE.Id, testAccount.Id, false );
		insert new List<Opportunity>{ testCreditNoteOpportunity, testInvoiceOpportunity, testJournalOpportunity };

		testProduct = TestUtils.createProduct_FinancialForce( testGeneralLedgerAccount.Id, true );
		PriceBookEntry testPBE = TestUtils.createPricebookEntry( testProduct.Id, TestUtils.STANDARD_PRICE_BOOK.Id, true );
	}
	
	static void setupTransactionParentObjects()
	{
		testCreditNote = TestUtils.createSalesCreditNote( testAccount.Id, new Map<String,Object>{ 'c2g__Opportunity__c' => testCreditNoteOpportunity.Id }, false );
		testInvoice = TestUtils.createSalesInvoice( testAccount.Id, new Map<String,Object>{ 'c2g__Opportunity__c' => testInvoiceOpportunity.Id }, false );
		testJournal = TestUtils.createJournal( new Map<String,Object>{ 'Opportunity__c' => testJournalOpportunity.Id }, false );
	}

	static void post( Id sObjectId )
	{
		c2g.CODAAPICommon.Reference creditNoteRef = new c2g.CODAAPICommon.Reference();
		creditNoteRef.Id = sObjectId;
		c2g.CODAAPISalesCreditNote_7_0.PostCreditNote( NULL, creditNoteRef );
	}

	@isTest(seeAllData=true)
	static void testPopulateOpportunity()
	{
		setup();
		setupTransactionParentObjects();
		insert testCreditNote;
		
		TestUtils.createSalesCreditNoteLineItems( 1,testCreditNote.Id, testProduct.Id, true );

		post( testCreditNote.Id );

		testCreditNote = [ SELECT c2g__Opportunity__c, c2g__Transaction__c FROM c2g__codaCreditNote__c WHERE ID = :testCreditNote.Id ];
		test.startTest();
			List<c2g__codaTransaction__c> actualTransactions = TransactionServices.populateOpportunity( new List<Sobject>{ testCreditNote }, c2g__codaCreditNote__c.c2g__Opportunity__c );
		test.stopTest();
		System.assertEquals( 1, actualTransactions.size(), 'There should be 1 transaction returned when there is a related opportunity' );
		System.assertEquals(  testCreditNoteOpportunity.Id, actualTransactions[0].Opportunity__c, 'The returned transaction should have its opportunity lookup populated');
	}

	@isTest(seeAllData=true)
	static void testFilterPostedSObjects_negative()
	{
		setup();
		setupTransactionParentObjects();
		insert testCreditNote;
		
		TestUtils.createSalesCreditNoteLineItems( 1,testCreditNote.Id, testProduct.Id, true );

		post( testCreditNote.Id );

		test.startTest();
			List<c2g__codaCreditNote__c> filteredSObjects = TransactionServices.filterPostedSObjects( new List<Sobject>{ testCreditNote }, new Map<Id, Sobject>{ testCreditNote.Id => testCreditNote }, c2g__codaCreditNote__c.c2g__CreditNoteStatus__c, c2g__codaCreditNote__c.c2g__Opportunity__c );
		test.stopTest();

		System.assert( filteredSObjects.isEmpty(), 'No sobjects should get returned if transaction lookup or opportunity lookup did not get changed' );

	}

	@isTest(seeAllData=true)
	static void testFilterPostedSObjects_positive()
	{
		final Id DUMMY_TRANSACTION_ID = 'a3uR00000001234';
		setup();
		setupTransactionParentObjects();
		insert testCreditNote;
		
		c2g__codaCreditNote__c newCreditNote = testCreditNote.clone( true ,true );
		newCreditNote.c2g__Transaction__c = DUMMY_TRANSACTION_ID;
		test.startTest();
			List<c2g__codaCreditNote__c> filteredSObjects = TransactionServices.filterPostedSObjects( new List<Sobject>{ newCreditNote }, new Map<Id, Sobject>{ testCreditNote.Id => testCreditNote }, c2g__codaCreditNote__c.c2g__CreditNoteStatus__c, c2g__codaCreditNote__c.c2g__Opportunity__c );
		test.stopTest();

		System.assert( !filteredSObjects.isEmpty(), 'A sobject should get returned if transaction lookup got changed' );

	}

	@isTest(seeAllData=true)
	static void testFilterPostedSObjects_negative_opportunityLookup()
	{
		final Id DUMMY_OPPORTUNITY_ID = 'a3uR00000001234';
		setup();
		setupTransactionParentObjects();
		insert testCreditNote;
		
		c2g__codaCreditNote__c newCreditNote = testCreditNote.clone( true ,true );
		newCreditNote.c2g__Opportunity__c = DUMMY_OPPORTUNITY_ID;
		test.startTest();
			List<c2g__codaCreditNote__c> filteredSObjects = TransactionServices.filterPostedSObjects( new List<Sobject>{ newCreditNote }, new Map<Id, Sobject>{ testCreditNote.Id => testCreditNote }, c2g__codaCreditNote__c.c2g__CreditNoteStatus__c, c2g__codaCreditNote__c.c2g__Opportunity__c );
		test.stopTest();

		System.assert( filteredSObjects.isEmpty(), 'No sobject should get returned if opportunity lookup got changed but transaction lookup is not populated' );

	}

}