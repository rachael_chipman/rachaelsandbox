public class stageTransitionController {

    public ApexPages.StandardController stdCtrl {get;set;}
    public Opportunity o {get;set;}
    //public Boolean req {get;set;}
    public Boolean lost {get;set;}
    public String buttonText {get;set;}
    public static Boolean doNotUpdateAddress = false;

    public stageTransitionController(ApexPages.StandardController std) {
        stdCtrl = std;
        doNotUpdateAddress = false;
        // get the opp we're dealing with and the fields we want to review
        if(o == null) o = [
            SELECT Id, OwnerId, Name, StageName, CurrencyIsoCode, Reasons_for_Lost_Opportunities__c, Lost_Pass_Details__c, Lost_Pass_Specifics__c
            FROM Opportunity
            WHERE Id = : ApexPages.currentPage().getParameters().get('id')
        ];

        if(ApexPages.currentPage().getParameters().get('lost') == 'true') lost = true;

        buttonText = (
            lost == true ? 'Submit Loss Reasons' : '');

        /*//see the if this opp owner needs to fill out the review fields (determined by RD and indicated by custom field on user object)
        user u = [select Required_Review_Fields__c from user where id = :o.ownerid];
        if(u.Required_Review_Fields__c == true) req = true;*/

    }

    // conditional actions and redirection depending on where they are in the process.
    public PageReference submit() {
        pagereference p;

        // going from 1 to 2? user enters products
        /*if(o.stagename == 'Investigating' && lost != true) {

            // give the user an error message if the checkboxes are required for them and any of them are not checked
            if(req == true &&
                (o.Looking_for_validation_from_ESP__c == false ||
                o.Options_other_than_Return_Path__c == false ||
                o.How_has_the_Buyer_Prioritized__c == false ||
                o.Other_stakeholders_involved__c == false ||
                o.Buyer_selling_internally__c == false)
            ) {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Please ensure all Investigating Actions have checkmarks.'));
            } else {
                // move to next stage and return to opp
                o.stagename = 'Demonstrating Capabilities';
                p = new pagereference('/apex/productEntry?id='+o.id);
                p.getParameters().put('addProduct','true');
            }

        // going from 2 to 3? user submits for approval
        } else if (o.stagename == 'Demonstrating Capabilities' && lost != true) {

            // give the user an error message if the checkboxes are required for them and any of them are not checked
            if(req == true &&
                (o.Presentation_Demo__c == false ||
                o.Change_Management__c == false ||
                o.Implementation_Timeframe__c == false ||
                o.Pricing_Funding__c == false ||
                o.Buyer_s_technical_requirements__c == false)
            ) {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Please ensure all Demonstrating Capabilities Actions have checkmarks.'));
            } else {
                // move to next stage, submit for pricing approval, and return to opp
                OpportunityServices.sendEmail('II_Internal_Use_Pricing_Approval', o.Id);
                Approval.ProcessSubmitRequest req1 = new Approval.ProcessSubmitRequest();
                req1.setObjectId(o.id);
                Approval.ProcessResult result = Approval.process(req1);
                p = new pagereference('/'+o.id);
            }

        // opp is lost? just redirect
        } else*/
        if (lost == true) 
        {
            o.stagename = 'Lost';
            o.CloseDate = system.today();
            o.Record_Locked__c = true;
            doNotUpdateAddress = true;
            p = new pagereference('/'+o.id);
        }

        // commit any changes
        try 
        {
            update o;
        } 
        catch (Exception e) 
        {
            ApexPages.addMessages(e);
        }

        // redirect them according to where we specified earlier in the code
        return p;
    }

    // show the user what line items they have (what they're submitting for pricing approval). this is what populates the "Current Products" table on the stageTransition page
    /*public List<OpportunityLineItem> getProds() {
        list<opportunitylineitem> prodList = [select id,product_name__c,product_status__c,unitprice,quantity,totalprice from opportunitylineitem where opportunityid = :o.id];
        return prodList;
    }

    // if the user wants to change their pricing and/or products, give them a button to click and do it at the actual product screen
    public PageReference editProds() {
        return new pagereference('/apex/productEntry?id='+o.id);
    }*/
}