trigger AccreditationVolumeTrigger on Accreditation_Volume__c (after insert, after update) {
    
    Map<Id, Accreditation_Volume__c> accVolRecords = new Map<Id, Accreditation_Volume__c>();

    if(trigger.isAfter)
    {
        for(Integer i = 0; i < trigger.new.size(); i++)
			{
				if((trigger.isInsert && (trigger.new[i].Overage__c > 0 && trigger.new[i].Tier__c != 'Non-Profit') ||
				(trigger.new[i].Tier__c == 'Non-Profit' && trigger.new[i].Total_Volume__c > 250000)) ||
				(trigger.isUpdate && (trigger.new[i].Generate_Overage_Opportunity__c && !trigger.oldMap.get(trigger.new[i].Id).Generate_Overage_Opportunity__c)))
				{
					accVolRecords.put(trigger.new[i].Id, trigger.new[i]);
				}
			}
			if(accVolRecords.size() > 0)
			{
				Database.executeBatch(new CertOverageGenerationBatch(accVolRecords), 200);
			}
    }

}