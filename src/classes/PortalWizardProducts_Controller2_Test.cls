@isTest (seeAllData=true)
Public class PortalWizardProducts_Controller2_Test{
    public static testMethod void testPortalWizardProducts_Controller2(){
    PageReference pageRef = Page.PortalWizardProducts;
Test.setCurrentPageReference(pageRef);

           
     Account acct = new Account();
        acct.name = 'RPTestAcct1';
        acct.accountmanager__c = 'BIO Automation';
        acct.Customer_Relationship_Manager_Primary__c = 'BIO Automation';
        Insert acct;
        
             
        Pricebook2 standardPB = [select id from Pricebook2 where isStandard=true];

        Pricebook2 pb = new Pricebook2(Name = 'Standard Price Book 2009', Description = 'Price Book 2009 Products', IsActive = true);
        insert pb;
        
        Product2 prod = new Product2(Name = 'Anti-infectives 2007', Family = 'Best Practices', IsActive = true);
        insert prod;
        
        Product2 prod2 = new Product2(Name = 'Partner Commissions - Reseller', Family = 'Partner Commissions', IsActive = true);
        insert prod2;

        PricebookEntry standardPrice = new PricebookEntry(Pricebook2Id = standardPB.Id, Product2Id = prod.Id, UnitPrice = 10000, IsActive = true, UseStandardPrice = false);
        insert standardPrice;
        
        PricebookEntry standardPrice2 = new PricebookEntry(Pricebook2Id = standardPB.Id, Product2Id = prod2.Id, UnitPrice = 10000, IsActive = true, UseStandardPrice = false);
        insert standardPrice2;

        PricebookEntry pbe = new PricebookEntry(Pricebook2Id = pb.Id, Product2Id = prod.Id, UnitPrice = 10000, IsActive = true, UseStandardPrice = false);
        insert pbe;
        PricebookEntry pbe2 = new PricebookEntry(Pricebook2Id = pb.Id, Product2Id = prod2.Id, UnitPrice = 10000, IsActive = true, UseStandardPrice = false);
        insert pbe2;

        
   Opportunity opp = new Opportunity();
       opp.AccountId = acct.Id;
       opp.Name = 'Test Opp';
       opp.CloseDate = system.today();
       opp.Type = 'New Business';
       opp.StageName = 'Signature';
       opp.Pricebook2Id = pb.Id;
       opp.Partner_Commission__c = .25;
       opp.CurrencyISOCode = 'USD';
       insert opp;            
       
        OpportunityLineItem oli = new OpportunityLineItem();
       oli.OpportunityId = opp.Id;
       oli.UnitPrice = 100.00;
       oli.Quantity = 1;
       oli.PricebookEntryId = pbe.Id;
       insert oli;
       
       
      ApexPages.StandardController sc = new ApexPages.standardController(oli);
      System.currentPagereference().getParameters().put('acct',acct.Id);
      System.currentPagereference().getParameters().put('opp',opp.Id);
PortalWizardProducts_Controller2 oPEE = new PortalWizardProducts_Controller2(sc);  

oPEE.getOppTotal();
oPEE.getSubTotal();
oPEE.updateDiscount();
oPEE.updateAvailableList();
oPEE.updateAddOnsList();
oPEE.removeFromShoppingCart();
oPEE.addToShoppingCart();
oPEE.updateUnitPrice();
oPEE.onSave();
//oPEE.onCancel();


}}