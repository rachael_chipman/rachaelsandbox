@isTEST

Public Class GenRefLeadAssignment_test{

  /* private static testmethod void testGenRefLeadAssignment(){
    
    
    //Insert User
    User u = new user();
    u.Alias = 'tstuser';
    u.Email='testrc@rp.com';
    u.EmailEncodingKey='UTF-8'; 
    u.LastName='Testing';
    u.LanguageLocaleKey='en_US';
    u.LocaleSidKey='en_US';
    u.ProfileId = '00e0000000734LT'; 
    u.TimeZoneSidKey='America/Los_Angeles'; 
    u.UserName='testrc@rp.com.rp';
        
    insert u;
    
  
    //Insert Account
    Account a = new Account(
    OwnerId = u.Id,
    Name = 'Test Account',
    phone = '(303) 555-5555',
    CRM__c = '005000000071xQv');
    
    insert a;     
    
    //Insert Promo Code
    Promo_Code__c pc = new Promo_Code__c();
    pc.Account__c = a.id;
    pc.Name = 'TEST003';
    pc.Promo_Type__c = 'Referral';
    pc.CurrencyISOCode = 'USD';
    
    insert pc;
    
        //Insert Lead
        Lead l = new Lead ();
        l.Company = 'Test Company';
        l.LastName = 'Thecat';
        l.SSC_Promo_Code__c = 'TEST003';
        l.OwnerId = u.Id;
        
        insert l;
        
            //Update Lead
            l.Campaign_Name__c = '2012_GL_PRO_WF_.NET Deliverability Channel Referral Form NEW';
            update l;
            
        }

   /* private static testmethod void testGenRefLeadAssignment2(){
    
    
    //Insert User
    User u = new user();
    u.Alias = 'tstuser';
    u.Email='testrc@rp.com';
    u.EmailEncodingKey='UTF-8'; 
    u.LastName='Testing';
    u.LanguageLocaleKey='en_US';
    u.LocaleSidKey='en_US';
    u.ProfileId = '00e0000000734LT'; 
    u.TimeZoneSidKey='America/Los_Angeles'; 
    u.UserName='testrc@rp.com.rp';
        
    insert u;
   
    
        //Insert Lead
        Lead l = new Lead ();
        l.Company = 'Test Company';
        l.LastName = 'Thecat';
        
        insert l;
        
            //Update Lead
            l.Campaign_Name__c = '2012_GL_PRO_WF_.NET Deliverability Channel Referral Form NEW';
            
            update l;
            
        }*/
        
/* private static testmethod void testGenRefLeadAssignment3(){
    
    
    //Insert User
    User u = new user();
    u.Alias = 'tstuser';
    u.Email='testrc@rp.com';
    u.EmailEncodingKey='UTF-8'; 
    u.LastName='Testing';
    u.LanguageLocaleKey='en_US';
    u.LocaleSidKey='en_US';
    u.ProfileId = '00e0000000734LT'; 
    u.TimeZoneSidKey='America/Los_Angeles'; 
    u.UserName='testrc@rp.com.rp';
        
    insert u;
    
        User u2 = new user();
    u2.Alias = 'tstuser2';
    u2.Email='testrc2@rp.com';
    u2.EmailEncodingKey='UTF-8'; 
    u2.LastName='Testing';
    u2.LanguageLocaleKey='en_US';
    u2.LocaleSidKey='en_US';
    u2.ProfileId = '00e0000000734LT'; 
    u2.TimeZoneSidKey='America/Los_Angeles'; 
    u2.UserName='testrc2@rp.com.rp';
        
    insert u2;

        System.runAs(u) {
        
        System.debug('Current User: ' + UserInfo.getUserID());
        System.debug('Current Profile: ' + UserInfo.getProfileId());     
    
        //Insert Lead
        Lead l = new Lead ();
        l.OwnerId = u2.Id;
        l.Company = 'Test Company';
        l.LastName = 'Thecat';
        l.Gen_Ref_Lead__c = true;
        l.Status = 'Qualified';
         l.Campaign_Name__c = '2012_GL_PRO_WF_.NET Deliverability Channel Referral Form NEW';
        
        insert l;
        update l;
               
        
            //Update Lead
           
            l.Status = 'Accepted';
            l.Gen_Ref_Lead__c = true;
            
            update l;
            
            }
            
        }
        
        private static testMethod void testLeadAssignment(){
        
        Id marketerRecordTypeId = [SELECT Id From RecordType Where Name = 'Marketer' AND sObjectType = 'Account'].Id;
        User testUser = TestUtils.getTestUser( 'TestUser123', 'Exec Admin');
        
        System.runAs( TestUtils.ADMIN_USER ){
            insert testUser;
        }
        
        Account testAccount = TestUtils.createAccount( marketerRecordTypeId, false );
        testAccount.OwnerId = testUser.Id;
        testAccount.Customer_Relationship_Manager_Primary__c = 'Test';
        testAccount.CRM__c = testUser.Id;
        insert testAccount;
        
        Promo_Code__c testPromo = TestUtils.createPromoCode( 'TestCode', 'Referral', testAccount.Id, false );
        testPromo.Account__c = testAccount.Id;
        insert testPromo;
        
        Lead testLead = TestUtils.createLeads( 'TestLead', 'TestLead', 'Qualified', false );
        testLead.Campaign_Name__c = '2012_GL_PRO_WF_.NET Deliverability Channel Referral Form NEW';
        testLead.Gen_Ref_Lead__c = false;
        testLead.OwnerId = testUser.Id;
        testLead.SSC_Promo_Code__c = 'TestCode';
        insert testLead;
        
        Test.startTest();
            update testLead;
        Test.stopTest();
        
        List<Lead> updatedLead = [SELECT Id, Referring_Partner__c, Referring_CRM__c, OwnerId FROM Lead];
        
        System.assertEquals ( 1, updatedLead.size(), 'There should be one contact created in the test');
        System.assertEquals ( testAccount.Id, updatedLead[0].Referring_Partner__c, 'Referring_Partner__c should be populated with the accountId');
        
    } */
        

    }