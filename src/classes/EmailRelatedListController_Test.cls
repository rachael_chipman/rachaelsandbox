@isTest (seeAllData=true)
Public class EmailRelatedListController_Test{
    public static testMethod void testEmailRelatedListController(){
    PageReference pageRef = Page.CertAppCaseEmails;
Test.setCurrentPageReference(pageRef);

   Account acct = new Account();
        acct.name = 'RPTestAcct1';
        acct.accountmanager__c = 'BIO Automation';
        Insert acct;   
        
          
           
            
     Certification_Application__c ca = new Certification_Application__c();
         ca.Application_Result__c = 'closed';
         ca.Account__c = acct.Id;
         ca.Certification_App_Fee__c = 123456;
         ca.Certification_Volume__c = 'Tier 4';
         ca.Has_Cases__c = true;
         insert ca;
         
     Case cas = new Case();
         cas.Certification_Application__c = ca.Id;
         cas.status = 'New';
         cas.AccountId = acct.Id;
         cas.Origin = 'Cert App Email';
         cas.Subject = 'Test Case';
         insert cas;
         
     EmailMessage mail = new EmailMessage();
         mail.ParentId = cas.Id;
         mail.textBody = 'Test';
         mail.FromAddress = 'test@testrp.com';
         mail.ToAddress = 'TestTo!testrp.com';
         mail.Subject = 'Test Subject';
         mail.MessageDate = system.Today();
         insert mail;
             
         
         
         
      ApexPages.StandardController sc = new ApexPages.standardController(ca);
System.currentPagereference().getParameters().put('id',ca.Id);
EmailRelatedListController oPEE = new EmailRelatedListController(sc);   

oPEE.strID = mail.Id;

oPEE.deletecase();
oPEE.gethasmail();
oPEE.getdisplaymail();
oPEE.reply();
oPEE.replyall();
oPEE.read();
oPEE.create();
}}