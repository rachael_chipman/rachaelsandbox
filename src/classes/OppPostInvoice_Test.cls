@ isTest (SeeAllData = true)
public class OppPostInvoice_Test {
    /*static testMethod void TestOppPostAnnual1() {

        Account acct = new Account();
        acct.name = 'RPTestacct1';
        acct.BillingCountry = 'USA';
        acct.c2g__CODAAccountsReceivableControl__c = 'a2Z00000000H8ou';
        Insert acct;
        
        Opportunity opp1 = new Opportunity(
        AccountId = acct.id, 
        Name = 'RPTestOpp1', 
        Pricebook2Id = '01s000000004NS6AAM',
        StageName = 'Signature', 
        CloseDate = Date.Today(), 
        Type = 'New Business',
        Billing_Terms__c = 'Net 30',
        
        Invoice_Created__c = false,
        Billing_Type__c = 'Annual',
        currencyISOcode = 'USD'
                
        );
        
        Insert opp1;
        
        OpportunityLineItem prod1 = new OpportunityLineItem();
        prod1.OpportunityId = opp1.id;
        prod1.PricebookEntryId = '01u00000000FGwcAAG';
        prod1.Quantity = 12;
        prod1.UnitPrice = 1200;
        prod1.of_MM_Events_Allowed__c = 20;
        prod1.of_CP_Events_Allowed__c = 20;
        prod1.MM_Overage_Fee__c = 25;
        prod1.CP_Overage_Fee__c = 25;
        prod1.of_BLA_IPs__c = 10;
        prod1.of_Rep_Mon_IPs__c = 10;
        prod1.Product_Status__c = 'Current';
        prod1.Description__c = 'Deliverability Tools';
        prod1.Indirect__c = false;
        prod1.Active__c = true;
        prod1.Previous_Value__c = 0;
        prod1.Bundle__c = false;
        prod1.Recurring__c = true;
        prod1.Start_Date__c = Date.Today();
        prod1.End_Date__c = prod1.Start_Date__c.addYears(1);
        Insert prod1;
        
        
       opp1.StageName = 'Finance Approved';
       opp1.Bypass_Product_Portfolio__c = true;
       opp1.Begin_Automation__c = true;
       update opp1; 

}


    static testMethod void TestOppPostAnnual2() {
        
        Account acct = new Account();
        acct.name = 'RPTestacct1';
        acct.BillingCountry = 'USA';
        acct.c2g__CODAAccountsReceivableControl__c = 'a2Z00000000H8ou';
        Insert acct;
        
        Opportunity opp1 = new Opportunity(
        AccountId = acct.id, 
        Name = 'RPTestOpp1', 
        Pricebook2Id = '01s000000004NS6AAM',
        StageName = 'Signature', 
        CloseDate = Date.Today(), 
        Type = 'New Business',
        Billing_Terms__c = 'Net 30',
        
        Invoice_Created__c = false,
        Billing_Type__c = 'Annual',
        currencyISOcode = 'USD'
                
        );
        
        Insert opp1;
        
        OpportunityLineItem prod1 = new OpportunityLineItem();
        prod1.OpportunityId = opp1.id;
        prod1.PricebookEntryId = '01u00000000FGwcAAG';
        prod1.Quantity = 12;
        prod1.UnitPrice = 1200;
        prod1.of_MM_Events_Allowed__c = 20;
        prod1.of_CP_Events_Allowed__c = 20;
        prod1.MM_Overage_Fee__c = 25;
        prod1.CP_Overage_Fee__c = 25;
        prod1.of_BLA_IPs__c = 10;
        prod1.of_Rep_Mon_IPs__c = 10;
        prod1.Product_Status__c = 'Current';
        prod1.Description__c = 'Deliverability Tools';
        prod1.Indirect__c = false;
        prod1.Active__c = true;
        prod1.Previous_Value__c = 0;
        prod1.Bundle__c = false;
        prod1.Recurring__c = true;
        prod1.Start_Date__c = System.today();
        prod1.End_Date__c = System.today().addYears(1);
        Insert prod1;
        
          OpportunityLineItem prod2 = new OpportunityLineItem();
        prod2.OpportunityId = opp1.id;
        prod2.PricebookEntryId = '01u00000000FGwcAAG';
        prod2.Quantity = 12;
        prod2.UnitPrice = 1200;
        prod2.of_MM_Events_Allowed__c = 20;
        prod2.of_CP_Events_Allowed__c = 20;
        prod2.MM_Overage_Fee__c = 25;
        prod2.CP_Overage_Fee__c = 25;
        prod2.of_BLA_IPs__c = 10;
        prod2.of_Rep_Mon_IPs__c = 10;
        prod2.Product_Status__c = 'Current';
        prod2.Description__c = 'Deliverability Tools';
        prod2.Indirect__c = false;
        prod2.Active__c = true;
        prod2.Previous_Value__c = 0;
        prod2.Bundle__c = false;
        prod2.Recurring__c = true;
        prod2.Start_Date__c = System.today();
        prod2.End_Date__c = System.today().addMonths(6);
        Insert prod2;
        
        
       opp1.StageName = 'Finance Approved';
       opp1.Begin_Automation__c = true;
       opp1.Bypass_Product_Portfolio__c = true;
       update opp1; 
}

  
 static testMethod void TestOppPostQuarterly1() {
        
        Account acct = new Account();
        acct.name = 'RPTestacct1';
        acct.BillingCountry = 'USA';
        acct.c2g__CODAAccountsReceivableControl__c = 'a2Z00000000H8ou';
        Insert acct;
        
        Opportunity opp1 = new Opportunity(
        AccountId = acct.id, 
        Name = 'RPTestOpp1', 
        Pricebook2Id = '01s000000004NS6AAM',
        StageName = 'Signature', 
        CloseDate = Date.Today(), 
        Type = 'New Business',
        Billing_Terms__c = 'Net 30',
        
        Invoice_Created__c = false,
        Billing_Type__c = 'Quarterly',
        currencyISOcode = 'USD'
                
        );
        
        Insert opp1;
        
        OpportunityLineItem prod1 = new OpportunityLineItem();
        prod1.OpportunityId = opp1.id;
        prod1.PricebookEntryId = '01u00000000FGwcAAG';
        prod1.Quantity = 12;
        prod1.UnitPrice = 1200;
        prod1.of_MM_Events_Allowed__c = 20;
        prod1.of_CP_Events_Allowed__c = 20;
        prod1.MM_Overage_Fee__c = 25;
        prod1.CP_Overage_Fee__c = 25;
        prod1.of_BLA_IPs__c = 10;
        prod1.of_Rep_Mon_IPs__c = 10;
        prod1.Product_Status__c = 'Current';
        prod1.Description__c = 'Deliverability Tools';
        prod1.Indirect__c = false;
        prod1.Active__c = true;
        prod1.Previous_Value__c = 0;
        prod1.Bundle__c = false;
        prod1.Recurring__c = true;
        prod1.Start_Date__c = System.today();
        prod1.End_Date__c = System.today().addYears(1);
        Insert prod1;
        
        
       opp1.StageName = 'Finance Approved';
       opp1.Begin_Automation__c = true;
       opp1.Bypass_Product_Portfolio__c = true;
       update opp1; 

}

    static testMethod void TestOppPostQuarterly2() {
        
        Account acct = new Account();
        acct.name = 'RPTestacct1';
        acct.BillingCountry = 'USA';
        acct.c2g__CODAAccountsReceivableControl__c = 'a2Z00000000H8ou';
        Insert acct;
        
        Opportunity opp1 = new Opportunity(
        AccountId = acct.id, 
        Name = 'RPTestOpp1', 
        Pricebook2Id = '01s000000004NS6AAM',
        StageName = 'Signature', 
        CloseDate = Date.Today(), 
        Type = 'New Business',
        Billing_Terms__c = 'Net 30',
        
        Invoice_Created__c = false,
        Billing_Type__c = 'Quarterly',
        currencyISOcode = 'USD'
                
        );
        
        Insert opp1;
        
        OpportunityLineItem prod1 = new OpportunityLineItem();
        prod1.OpportunityId = opp1.id;
        prod1.PricebookEntryId = '01u00000000FGwcAAG';
        prod1.Quantity = 12;
        prod1.UnitPrice = 1200;
        prod1.of_MM_Events_Allowed__c = 20;
        prod1.of_CP_Events_Allowed__c = 20;
        prod1.MM_Overage_Fee__c = 25;
        prod1.CP_Overage_Fee__c = 25;
        prod1.of_BLA_IPs__c = 10;
        prod1.of_Rep_Mon_IPs__c = 10;
        prod1.Product_Status__c = 'Current';
        prod1.Description__c = 'Deliverability Tools';
        prod1.Indirect__c = false;
        prod1.Active__c = true;
        prod1.Previous_Value__c = 0;
        prod1.Bundle__c = false;
        prod1.Recurring__c = true;
        prod1.Start_Date__c = System.today();
        prod1.End_Date__c = System.today().addYears(1);
        Insert prod1;
        
          OpportunityLineItem prod2 = new OpportunityLineItem();
        prod2.OpportunityId = opp1.id;
        prod2.PricebookEntryId = '01u00000000FGwcAAG';
        prod2.Quantity = 12;
        prod2.UnitPrice = 1200;
        prod2.of_MM_Events_Allowed__c = 20;
        prod2.of_CP_Events_Allowed__c = 20;
        prod2.MM_Overage_Fee__c = 25;
        prod2.CP_Overage_Fee__c = 25;
        prod2.of_BLA_IPs__c = 10;
        prod2.of_Rep_Mon_IPs__c = 10;
        prod2.Product_Status__c = 'Current';
        prod2.Description__c = 'Deliverability Tools';
        prod2.Indirect__c = false;
        prod2.Active__c = true;
        prod2.Previous_Value__c = 0;
        prod2.Bundle__c = false;
        prod2.Recurring__c = true;
        prod2.Start_Date__c = System.today();
        prod2.End_Date__c = System.today().addMonths(6);
        Insert prod2;
        
        
       opp1.StageName = 'Finance Approved';
       opp1.Begin_Automation__c = true;
       opp1.Bypass_Product_Portfolio__c = true;
       update opp1; 
}

 static testMethod void TestOppPostMonthly1() {

        Account acct = new Account();
        acct.name = 'RPTestacct1';
        acct.BillingCountry = 'USA';
        acct.c2g__CODAAccountsReceivableControl__c = 'a2Z00000000H8ou';
        Insert acct;
        
        Opportunity opp1 = new Opportunity(
        AccountId = acct.id, 
        Name = 'RPTestOpp1', 
        Pricebook2Id = '01s000000004NS6AAM',
        StageName = 'Signature', 
        CloseDate = Date.Today(), 
        Type = 'New Business',
        Billing_Terms__c = 'Net 30',
        
        Invoice_Created__c = false,
        Billing_Type__c = 'Monthly',
        currencyISOcode = 'USD'
                
        );
        
        Insert opp1;
        
        OpportunityLineItem prod1 = new OpportunityLineItem();
        prod1.OpportunityId = opp1.id;
        prod1.PricebookEntryId = '01u00000000FGwcAAG';
        prod1.Quantity = 12;
        prod1.UnitPrice = 1200;
        prod1.of_MM_Events_Allowed__c = 20;
        prod1.of_CP_Events_Allowed__c = 20;
        prod1.MM_Overage_Fee__c = 25;
        prod1.CP_Overage_Fee__c = 25;
        prod1.of_BLA_IPs__c = 10;
        prod1.of_Rep_Mon_IPs__c = 10;
        prod1.Product_Status__c = 'Current';
        prod1.Description__c = 'Deliverability Tools';
        prod1.Indirect__c = false;
        prod1.Active__c = true;
        prod1.Previous_Value__c = 0;
        prod1.Bundle__c = false;
        prod1.Recurring__c = true;
        prod1.Start_Date__c = System.today();
        prod1.End_Date__c = System.today().addYears(1);
        Insert prod1;
        
        
       opp1.StageName = 'Finance Approved';
       opp1.Begin_Automation__c = true;
       opp1.Bypass_Product_Portfolio__c = true;
       update opp1; 

}


    static testMethod void TestOppPostMonthly2() {
        
        Account acct = new Account();
        acct.name = 'RPTestacct1';
        acct.BillingCountry = 'USA';
        acct.c2g__CODAAccountsReceivableControl__c = 'a2Z00000000H8ou';
        Insert acct;
        
        Opportunity opp1 = new Opportunity(
        AccountId = acct.id, 
        Name = 'RPTestOpp1', 
        Pricebook2Id = '01s000000004NS6AAM',
        StageName = 'Signature', 
        CloseDate = Date.Today(), 
        Type = 'New Business',
        Billing_Terms__c = 'Net 30',
        
        Invoice_Created__c = false,
        Billing_Type__c = 'Monthly',
        currencyISOcode = 'USD'
                
        );
        
        Insert opp1;
        
        OpportunityLineItem prod1 = new OpportunityLineItem();
        prod1.OpportunityId = opp1.id;
        prod1.PricebookEntryId = '01u00000000FGwcAAG';
        prod1.Quantity = 12;
        prod1.UnitPrice = 1200;
        prod1.of_MM_Events_Allowed__c = 20;
        prod1.of_CP_Events_Allowed__c = 20;
        prod1.MM_Overage_Fee__c = 25;
        prod1.CP_Overage_Fee__c = 25;
        prod1.of_BLA_IPs__c = 10;
        prod1.of_Rep_Mon_IPs__c = 10;
        prod1.Product_Status__c = 'Current';
        prod1.Description__c = 'Deliverability Tools';
        prod1.Indirect__c = false;
        prod1.Active__c = true;
        prod1.Previous_Value__c = 0;
        prod1.Bundle__c = false;
        prod1.Recurring__c = true;
        prod1.Start_Date__c = System.today();
        prod1.End_Date__c = System.today().addYears(1);
        Insert prod1;
        
          OpportunityLineItem prod2 = new OpportunityLineItem();
        prod2.OpportunityId = opp1.id;
        prod2.PricebookEntryId = '01u00000000FGwcAAG';
        prod2.Quantity = 12;
        prod2.UnitPrice = 1200;
        prod2.of_MM_Events_Allowed__c = 20;
        prod2.of_CP_Events_Allowed__c = 20;
        prod2.MM_Overage_Fee__c = 25;
        prod2.CP_Overage_Fee__c = 25;
        prod2.of_BLA_IPs__c = 10;
        prod2.of_Rep_Mon_IPs__c = 10;
        prod2.Product_Status__c = 'Current';
        prod2.Description__c = 'Deliverability Tools';
        prod2.Indirect__c = false;
        prod2.Active__c = true;
        prod2.Previous_Value__c = 0;
        prod2.Bundle__c = false;
        prod2.Recurring__c = true;
        prod2.Start_Date__c = System.today();
        prod2.End_Date__c = System.today().addMonths(6);
        Insert prod2;
        
        
       opp1.StageName = 'Finance Approved';
       opp1.Begin_Automation__c = true;
       opp1.Bypass_Product_Portfolio__c = true;
       Test.startTest();
       update opp1; 
       Test.stopTest();
}*/


}