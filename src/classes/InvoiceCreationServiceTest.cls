@isTest(SeeAllData = true) 
private class InvoiceCreationServiceTest 
{
	private static Opportunity testOpportunity;

	private static void setupWithOLI( Date startDate, Date endDate ){
		RecordType accountRecordtype = [ SELECT Id FROM RecordType WHERE SObjectType = 'Account' AND IsActive = TRUE LIMIT 1 ];
		Account opportunityAccount = TestUtils.createAccount( accountRecordtype.Id, true );

		RecordType opportunityRecordType = [ SELECT Id FROM RecordType WHERE SObjectType = 'Opportunity' AND IsActive = TRUE LIMIT 1 ];
		testOpportunity = TestUtils.createOpportunity( opportunityRecordType.Id, opportunityAccount.Id, true);

		OpportunityLineItem testOLI = TestUtils.createOpportunityLineItems(testOpportunity.Id, 1, false)[0];
		testOLI.Start_Date__c = startDate;
		testOLI.End_Date__c = endDate;
		insert testOLI;

		testOpportunity = [Select Id, Billing_Type__c, Minimum_Start_Date__c, Max_End_Date__c, StageName, AccountId, Billing_Terms_Number_Calc__c,
		Account.Invoicing_Contact__c, FF_Company_Formula__c, ExpectedRevenue, Invoice_Created__c, Bypass_Financial_Force__c, Type
		From Opportunity Where Id = :testOpportunity.Id ];
	}

	
	private static testMethod void getInvoiceDates_Monthly()
	{
		setupWithOLI( Date.today(), Date.today().addMonths(12) );
		testOpportunity.Billing_Type__c = OpportunityServices.MONTHLY;

		Test.startTest();
		List<Date> invoiceDates = InvoiceCreationService.getInvoiceDates( testOpportunity );
		Test.stopTest();

		System.assertEquals( 12, invoiceDates.size(), 'We expect 12 invoice dates to be created for Month billing type.');

		Date todaysDate = Date.today();
		for(Date curDate : invoiceDates)
		{
			System.assertEquals(curDate, todaysDate, 'We expect the the two dates should be equal for billing type Monthly.');
			todaysDate = todaysDate = todaysDate.addMonths(1);
		}
	}

	private static testMethod void getInvoiceDates_Annual()
	{
		setupWithOLI( Date.today(), Date.today().addYears(1) );
		testOpportunity.Billing_Type__c = OpportunityServices.ANNUAL;

		Test.startTest();
		List<Date> invoiceDates = InvoiceCreationService.getInvoiceDates( testOpportunity );
		Test.stopTest();

		System.assertEquals( 1, invoiceDates.size(), 'We expect 1 invoice dates to be created for Annual billing type.');

		System.assertEquals( Date.today(), invoiceDates[0], 'We expect todays date to be returned on billing type Annual.');
	}

	private static testMethod void getInvoiceDates_Quarter_Case1()
	{
		setupWithOLI( Date.today(), Date.today().addYears(1) );
		testOpportunity.Billing_Type__c = OpportunityServices.QUARTERLY;

		Test.startTest();
		List<Date> invoiceDates = InvoiceCreationService.getInvoiceDates( testOpportunity );
		Test.stopTest();
		System.assertEquals( 4, invoiceDates.size(), 'We expect 4 invoice dates to be created for Quater billing type with full year span between min and max dates.');

		Date todaysDate = Date.today();
		for(Date curDate : invoiceDates)
		{
			System.assertEquals(curDate, todaysDate, 'We expect the dates should be equal on billing type Quarterly');
			todaysDate = todaysDate.addMonths(3);
		}
	}

	private static testMethod void getInvoiceDates_Quarter_Case2()
	{
		setupWithOLI( Date.today(), Date.today().addMonths(5) );
		testOpportunity.Billing_Type__c = OpportunityServices.QUARTERLY;

		Test.startTest();
		List<Date> invoiceDates = InvoiceCreationService.getInvoiceDates( testOpportunity );
		Test.stopTest();

		System.assertEquals( 2, invoiceDates.size(), 'We expect 2 invoice dates to be created for Quater billing type with minimum and maximum date span of 2 quarters.');

		Date todaysDate = Date.today();
		for(Date curDate : invoiceDates)
		{
			System.assertEquals(curDate, todaysDate, 'We expect the dates should be equal on billing type Quarterly');
			todaysDate = todaysDate.addMonths(3);
		}
	}

	private static testMethod void getInvoiceDates_SemiAnnually_Case1()
	{
		setupWithOLI( Date.today(), Date.today().addYears(1) );
		testOpportunity.Billing_Type__c = OpportunityServices.SEMI_ANNUALLY;

		Test.startTest();
		List<Date> invoiceDates = InvoiceCreationService.getInvoiceDates( testOpportunity );
		Test.stopTest();

		System.assertEquals( 2, invoiceDates.size(), 'We expect 2 invoice date to be created for a opportunity with a date span of more then 6 months when the billing type is Semi-Annually.' );


		Date todaysDate = Date.today();
		for(Date curDate : invoiceDates)
		{
			System.assertEquals(curDate, todaysDate, 'We expect the dates should be equal on billing type Semi-Annually');
			todaysDate = todaysDate.addMonths(6);
		}
	}

	private static testMethod void getInvoiceDates_SemiAnnually_Case2()
	{
		setupWithOLI( Date.today(), Date.today().addMonths(5) );
		testOpportunity.Billing_Type__c = OpportunityServices.SEMI_ANNUALLY;

		Test.startTest();
		List<Date> invoiceDates = InvoiceCreationService.getInvoiceDates( testOpportunity );
		Test.stopTest();

		System.assertEquals( 1, invoiceDates.size(), 'We expect 1 invoice date to be created for a opportunity with a date span of less then 6 months when the billing type is Semi-Annually.');

		Date todaysDate = Date.today();
		for(Date curDate : invoiceDates)
		{
			System.assertEquals(curDate, todaysDate, 'We expect the dates should be equal on billing type Semi-Annually');
			todaysDate = todaysDate.addMonths(6);
		}
	}

	/****************************************** Testing getCodaInvoiceFromOpps() method ****************************************/
	static void assertOnCommonCodaInvoiceFields( List<c2g__codaInvoice__c> newInvoices )
	{
		Integer monthInterval = OpportunityServices.BILLING_INTERVAL_MAP.get( testOpportunity.Billing_Type__c );
		Date invoiceDate = testOpportunity.Minimum_Start_Date__c;
		Date dueDate;

		Date startDate = testOpportunity.Minimum_Start_Date__c;
		Date endDate = testOpportunity.Minimum_Start_Date__c.addMonths( monthInterval ).addDays( -1 );
		Integer totalInvoices = newInvoices.size();
		for( c2g__codaInvoice__c invoice : newInvoices )
		{
			Date currentInvoiceDate = invoiceDate < Date.today() ? Date.today() : invoiceDate;

			dueDate = currentInvoiceDate.addDays( Integer.valueof(testOpportunity.Billing_Terms_Number_Calc__c) );

			//This case only happens on the last invoice
			if( endDate > testOpportunity.Max_End_Date__c && totalInvoices == 1 )
				endDate = testOpportunity.Max_End_Date__c;

			//System.assertEquals( OpportunityServices.RETURN_PATH_COMPANY.Id, invoice.c2g__OwnerCompany__c, 'We expect the proper Company to be populated as the Owner Company' );
			System.assertEquals( testOpportunity.AccountId, invoice.c2g__Account__c, 'We expect the invoice to have the same financial force account as our opportunity account' );
			System.assertEquals( testOpportunity.AccountId, invoice.Opportunity_Account__c, 'We expect the invoice to have a lookup to our opportunity account' );
			System.assertEquals( testOpportunity.Id, invoice.c2g__Opportunity__c, 'We expect the invoice to look up to our opportunity' );
			System.assertEquals( TestUtils.ACCOUNTING_CURRENCY_USD.Id, invoice.c2g__InvoiceCurrency__c, 'We expect the invoice to look up to our test currency' );
			System.assertEquals( testOpportunity.Account.Invoicing_Contact__c, invoice.Invoicing_Contact__c, 'We expect the invoicing contact to look up to our account invoicing contact' );

			System.assertEquals( currentInvoiceDate, invoice.c2g__InvoiceDate__c, 'We expect the invoice date to be equal to our Opportunity Minimum Start Date because it is greater then today.');
			System.assertEquals( dueDate, invoice.c2g__DueDate__c, 'We expect a due date which is (billing terms number) ' + testOpportunity.Billing_Terms_Number_Calc__c + ' away from our invoice date' );
			System.assertEquals( startDate, invoice.Contract_Service_Period_Start_Date__c, 'We expect the start date to be equal to our minimum date' );
			System.assertEquals( endDate, invoice.Contract_Service_Period_End_Date__c, 'We expect the end date to be 1 year apart from the start date.' + totalInvoices);

			invoiceDate = invoiceDate.addMonths( monthInterval );
			dueDate = currentInvoiceDate.addDays( Integer.valueof(testOpportunity.Billing_Terms_Number_Calc__c) );

			startDate = startDate.addMonths( monthInterval );
			endDate = endDate.addMonths( monthInterval );
			totalInvoices--;
		}
		for( Integer i = 1; i < newInvoices.size(); i++ )
		{
			if( newInvoices[i].c2g__InvoiceDate__c != newInvoices[i-1].c2g__InvoiceDate__c )
			System.assert( newInvoices[i].c2g__InvoiceDate__c > newInvoices[i-1].c2g__InvoiceDate__c, 'The invoice list returned by the method should be ordered by invoice date ' + i + 'Current Invoice Dates ' + newInvoices[i].c2g__InvoiceDate__c + ' previous ' + newInvoices[i-1].c2g__InvoiceDate__c);
		}
	}

	static testMethod void testAnnualInvoice_Case1() 
	{
		Date startDate = Date.today().addMonths(1);
		Date endDate = startDate.addYears(1);
		setupWithOLI( startDate, endDate );

		Test.startTest();
		testOpportunity.Billing_Type__c = OpportunityServices.ANNUAL;
		List<c2g__codaInvoice__c> newInvoices = InvoiceCreationService.getCodaInvoiceFromOpps(testOpportunity, TestUtils.ACCOUNTING_CURRENCY_USD, testOpportunity.AccountId );
		Test.stopTest();

		System.assertEquals( 1, newInvoices.size(), 'We expect only 1 invoice to be created when billing type is \'Anually\'');
		assertOnCommonCodaInvoiceFields( newInvoices );
	}

	static testMethod void testAnnualInvoice_Case2()
	{
		Date startDate = Date.today().addMonths(-1);
		Date endDate = startDate.addYears(1);
		setupWithOLI( startDate, endDate );

		Test.startTest();
		testOpportunity.Billing_Type__c = OpportunityServices.ANNUAL;
		List<c2g__codaInvoice__c> newInvoices = InvoiceCreationService.getCodaInvoiceFromOpps(testOpportunity, TestUtils.ACCOUNTING_CURRENCY_USD, testOpportunity.AccountId );
		Test.stopTest();

		System.assertEquals( 1, newInvoices.size(), 'We expect only 1 invoice to be created when billing type is \'Anually\'');
		assertOnCommonCodaInvoiceFields( newInvoices );
	}

	static testMethod void testAnnualInvoice_Case_2YearSpan() 
	{
		Date startDate = Date.today().addMonths(1);
		Date endDate = startDate.addYears(2);
		setupWithOLI( startDate, endDate );

		Test.startTest();
		testOpportunity.Billing_Type__c = OpportunityServices.ANNUAL;
		List<c2g__codaInvoice__c> newInvoices = InvoiceCreationService.getCodaInvoiceFromOpps(testOpportunity, TestUtils.ACCOUNTING_CURRENCY_USD, testOpportunity.AccountId);
		Test.stopTest();

		System.assertEquals( 1, newInvoices.size(), 'We expect only 1 invoice to be created when billing type is \'Anually\'');
		assertOnCommonCodaInvoiceFields( newInvoices );
	}

	static testMethod void testSemiAnnualInvoice_Case1() 
	{
		Date startDate = Date.today().addMonths(1);
		Date endDate = startDate.addMonths(13);
		setupWithOLI( startDate, endDate );

		Test.startTest();
		testOpportunity.Billing_Type__c = OpportunityServices.SEMI_ANNUALLY;
		List<c2g__codaInvoice__c> newInvoices =  InvoiceCreationService.getCodaInvoiceFromOpps( testOpportunity, TestUtils.ACCOUNTING_CURRENCY_USD, testOpportunity.AccountId );
		Test.stopTest();

		System.assertEquals( 2, newInvoices.size(), 'We expect 2 invoices to be created when the date span is greater than 1 year and the billing type is semi annually');
		assertOnCommonCodaInvoiceFields( newInvoices );
	}

	static testMethod void testSemiAnnualInvoice_Case2() 
	{
		Date startDate = Date.today().addMonths(1);
		Date endDate = startDate.addMonths(7);
		setupWithOLI( startDate, endDate );

		Test.startTest();
		testOpportunity.Billing_Type__c = OpportunityServices.SEMI_ANNUALLY;
		List<c2g__codaInvoice__c> newInvoices =  InvoiceCreationService.getCodaInvoiceFromOpps( testOpportunity, TestUtils.ACCOUNTING_CURRENCY_USD, testOpportunity.AccountId );
		Test.stopTest();

		System.assertEquals( 2, newInvoices.size(), 'We expect 2 invoices to be created when the date span is greater then 6 months but less then 1 year and the billing type is semi annually');
		assertOnCommonCodaInvoiceFields( newInvoices );
	}

	static testMethod void testSemiAnnualInvoice_Case3() 
	{
		Date startDate = Date.today().addMonths(1);
		Date endDate = startDate.addMonths(3);
		setupWithOLI( startDate, endDate );

		Test.startTest();
		testOpportunity.Billing_Type__c = OpportunityServices.SEMI_ANNUALLY;
		List<c2g__codaInvoice__c> newInvoices =  InvoiceCreationService.getCodaInvoiceFromOpps( testOpportunity, TestUtils.ACCOUNTING_CURRENCY_USD, testOpportunity.AccountId );
		Test.stopTest();

		System.assertEquals( 1, newInvoices.size(), 'We expect 1 invoices to be created when the date span is less then 6 months and the billing type is semi annually');
		assertOnCommonCodaInvoiceFields( newInvoices );
	}

	static testMethod void testSemiAnnualInvoice_Case4() 
	{
		Date startDate = Date.today().addMonths(-1);
		Date endDate = startDate.addMonths(13);
		setupWithOLI( startDate, endDate );

		Test.startTest();
		testOpportunity.Billing_Type__c = OpportunityServices.SEMI_ANNUALLY;
		List<c2g__codaInvoice__c> newInvoices =  InvoiceCreationService.getCodaInvoiceFromOpps( testOpportunity, TestUtils.ACCOUNTING_CURRENCY_USD, testOpportunity.AccountId );
		Test.stopTest();

		System.assertEquals( 2, newInvoices.size(), 'We expect 2 invoices to be created when the date span is greater than year and the billing type is semi annually');
		assertOnCommonCodaInvoiceFields( newInvoices );    
	}

	static testMethod void testSemiAnnualInvoice_Case5() 
	{
		Date startDate = Date.today().addMonths(-1);
		Date endDate = startDate.addMonths(7);
		setupWithOLI( startDate, endDate );

		Test.startTest();
		testOpportunity.Billing_Type__c = OpportunityServices.SEMI_ANNUALLY;
		List<c2g__codaInvoice__c> newInvoices =  InvoiceCreationService.getCodaInvoiceFromOpps( testOpportunity, TestUtils.ACCOUNTING_CURRENCY_USD, testOpportunity.AccountId );
		Test.stopTest();

		System.assertEquals( 2, newInvoices.size(), 'We expect 2 invoices to be created when the date span is greater than 6 months and the billing type is semi annually');
		assertOnCommonCodaInvoiceFields( newInvoices );
	}


	static testMethod void testSemiAnnualInvoice_Case6() 
	{
		Date startDate = Date.today().addMonths(-1);
		Date endDate = startDate.addMonths(3);
		setupWithOLI( startDate, endDate );

		Test.startTest();
		testOpportunity.Billing_Type__c = OpportunityServices.SEMI_ANNUALLY;
		List<c2g__codaInvoice__c> newInvoices =  InvoiceCreationService.getCodaInvoiceFromOpps( testOpportunity, TestUtils.ACCOUNTING_CURRENCY_USD, testOpportunity.AccountId );
		Test.stopTest();

		System.assertEquals( 1, newInvoices.size(), 'We expect 1 invoices to be created when the date span is less than 6 months and the billing type is semi annually');
		assertOnCommonCodaInvoiceFields( newInvoices );
	}
	static testMethod void testSemiAnnualInvoice_Case_2YearSpan() 
	{

		Date startDate = Date.today().addMonths(1);
		Date endDate = startDate.addYears(2);
		setupWithOLI( startDate, endDate );

		Test.startTest();
		testOpportunity.Billing_Type__c = OpportunityServices.SEMI_ANNUALLY;
		List<c2g__codaInvoice__c> newInvoices =  InvoiceCreationService.getCodaInvoiceFromOpps( testOpportunity, TestUtils.ACCOUNTING_CURRENCY_USD, testOpportunity.AccountId );
		Test.stopTest();

		System.assertEquals( 2, newInvoices.size(), 'We expect 2 invoices to be created when the date span is more than 1 year and the billing type is semi annually');
		assertOnCommonCodaInvoiceFields( newInvoices );
	}

	static testMethod void testMonthInvoice_Case1() 
	{

		Date startDate = Date.today().addMonths(1);
		Date endDate = startDate.addMonths(12);
		setupWithOLI( startDate, endDate );

		Test.startTest();
		testOpportunity.Billing_Type__c = OpportunityServices.MONTHLY;
		List<c2g__codaInvoice__c> newInvoices =  InvoiceCreationService.getCodaInvoiceFromOpps( testOpportunity, TestUtils.ACCOUNTING_CURRENCY_USD, testOpportunity.AccountId );
		Test.stopTest();

		System.assertEquals( 12, newInvoices.size(), 'We expect 12 invoices to be created when the date span is 1 year (12 months) and the billing type is MOnthly');
		assertOnCommonCodaInvoiceFields( newInvoices );
	}

	static testMethod void testMonthInvoice_Case2() 
	{

		Date startDate = Date.today().addMonths(1);
		Date endDate = startDate.addMonths(6);
		setupWithOLI( startDate, endDate );

		Test.startTest();
		testOpportunity.Billing_Type__c = OpportunityServices.MONTHLY;
		List<c2g__codaInvoice__c> newInvoices =  InvoiceCreationService.getCodaInvoiceFromOpps( testOpportunity, TestUtils.ACCOUNTING_CURRENCY_USD, testOpportunity.AccountId );
		Test.stopTest();

		System.assertEquals( 12, newInvoices.size(), 'We expect 12 invoices to be created when the date span is 6 months and the billing type is Monthly');
		assertOnCommonCodaInvoiceFields( newInvoices );
	}

	static testMethod void testMonthInvoice_Case3() 
	{

		Date startDate = Date.today().addMonths(-1);
		Date endDate = startDate.addMonths(12);
		setupWithOLI( startDate, endDate );       

		Test.startTest();
		testOpportunity.Billing_Type__c = OpportunityServices.MONTHLY;
		List<c2g__codaInvoice__c> newInvoices =  InvoiceCreationService.getCodaInvoiceFromOpps( testOpportunity, TestUtils.ACCOUNTING_CURRENCY_USD, testOpportunity.AccountId );
		Test.stopTest();

		System.assertEquals( 12, newInvoices.size(), 'We expect 12 invoices to be created when the start date is 1 month back and the date span is 13 months and the billing type is Monthly');
		assertOnCommonCodaInvoiceFields( newInvoices );
	}

	static testMethod void testMonthInvoice_Case4() 
	{

		Date startDate = Date.today().addMonths(-1);
		Date endDate = startDate.addMonths(6);
		setupWithOLI( startDate, endDate );

		Test.startTest();
		testOpportunity.Billing_Type__c = OpportunityServices.MONTHLY;
		List<c2g__codaInvoice__c> newInvoices =  InvoiceCreationService.getCodaInvoiceFromOpps( testOpportunity, TestUtils.ACCOUNTING_CURRENCY_USD, testOpportunity.AccountId );
		Test.stopTest();

		System.assertEquals( 12, newInvoices.size(), 'We expect 12 invoices to be created when the start date is 1 month back and the date span is 6 months and the billing type is Monthly');
		assertOnCommonCodaInvoiceFields( newInvoices );
	}

	static testMethod void testMonthInvoice_Case_2YearSpan() 
	{

		Date startDate = Date.today().addMonths(1);
		Date endDate = startDate.addYears(2);
		setupWithOLI( startDate, endDate );

		Test.startTest();
		testOpportunity.Billing_Type__c = OpportunityServices.MONTHLY;
		List<c2g__codaInvoice__c> newInvoices =  InvoiceCreationService.getCodaInvoiceFromOpps( testOpportunity, TestUtils.ACCOUNTING_CURRENCY_USD, testOpportunity.AccountId );
		Test.stopTest();

		System.assertEquals( 12, newInvoices.size(), 'We expect 12 invoices to be created when the start date is 1 month forward and the date span is 2 years and the billing type is Monthly');
		assertOnCommonCodaInvoiceFields( newInvoices );

	}

	static testMethod void testQuaterlyInvoice_Case1() 
	{

		Date startDate = Date.today().addMonths(1);
		Date endDate = startDate.addMonths(13);
		setupWithOLI( startDate, endDate );

		Test.startTest();
		testOpportunity.Billing_Type__c = OpportunityServices.QUARTERLY;
		List<c2g__codaInvoice__c> newInvoices =  InvoiceCreationService.getCodaInvoiceFromOpps( testOpportunity, TestUtils.ACCOUNTING_CURRENCY_USD, testOpportunity.AccountId );
		Test.stopTest();

		System.assertEquals( 4, newInvoices.size(), 'We expect 4 invoices to be created when the date span is 13 months and the billing type is Quartely');
		assertOnCommonCodaInvoiceFields( newInvoices );

	}

	static testMethod void testQuaterlyInvoice_Case2() 
	{

		Date startDate = Date.today().addMonths(1);
		Date endDate = startDate.addMonths(5);
		setupWithOLI( startDate, endDate );

		Test.startTest();
		testOpportunity.Billing_Type__c = OpportunityServices.QUARTERLY;
		List<c2g__codaInvoice__c> newInvoices =  InvoiceCreationService.getCodaInvoiceFromOpps( testOpportunity, TestUtils.ACCOUNTING_CURRENCY_USD, testOpportunity.AccountId );
		Test.stopTest();

		System.assertEquals( 2, newInvoices.size(), 'We expect 5 invoices to be created with a date span is 5 months and the billing type is Quartely');
		assertOnCommonCodaInvoiceFields( newInvoices );
	}

	static testMethod void testQuaterlyInvoice_Case3() 
	{

		Date startDate = Date.today().addMonths(-1);
		Date endDate = startDate.addMonths(13);
		setupWithOLI( startDate, endDate );

		Test.startTest();
		testOpportunity.Billing_Type__c = OpportunityServices.QUARTERLY;
		List<c2g__codaInvoice__c> newInvoices =  InvoiceCreationService.getCodaInvoiceFromOpps( testOpportunity, TestUtils.ACCOUNTING_CURRENCY_USD, testOpportunity.AccountId );
		Test.stopTest();

		System.assertEquals( 4, newInvoices.size(), 'We expect 4 invoices to be created when start date is 1 month back and the date span is 13 months and the billing type is Quartely');
		assertOnCommonCodaInvoiceFields( newInvoices );
	}

	static testMethod void testQuaterlyInvoice_Case4() 
	{
		Date startDate= Date.today().addMonths(-1);
		Date endDate = startDate.addMonths(2);
		setupWithOLI( startDate, endDate );

		Test.startTest();
		testOpportunity.Billing_Type__c = OpportunityServices.QUARTERLY;
		List<c2g__codaInvoice__c> newInvoices =  InvoiceCreationService.getCodaInvoiceFromOpps( testOpportunity, TestUtils.ACCOUNTING_CURRENCY_USD, testOpportunity.AccountId );
		Test.stopTest();

		System.assertEquals( 1, newInvoices.size(), 'We expect 1 invoices to be created when start date is 1 month back and the date span is 2 months and the billing type is Quartely');
		assertOnCommonCodaInvoiceFields( newInvoices );
	}



	static testMethod void testQuaterlyInvoice_Case_2YearSpan(){

		Date startDate = Date.today().addMonths(1);
		Date endDate  = startDate.addYears(2);
		setupWithOLI( startDate, endDate );

		Test.startTest();
		testOpportunity.Billing_Type__c = OpportunityServices.QUARTERLY;
		List<c2g__codaInvoice__c> newInvoices =  InvoiceCreationService.getCodaInvoiceFromOpps( testOpportunity, TestUtils.ACCOUNTING_CURRENCY_USD, testOpportunity.AccountId );
		Test.stopTest();

		System.assertEquals( 4, newInvoices.size(), 'We expect 4 invoices to be created when the date span is 2 years and the billing type is Quartely');
		assertOnCommonCodaInvoiceFields( newInvoices );
	}

}