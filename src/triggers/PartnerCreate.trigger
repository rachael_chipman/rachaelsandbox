trigger PartnerCreate on Opportunity (after update) {
    
    list<Partner> PartnerstoInsert = new list<Partner>();
    list<Partner> PartnerstoDelete = new list<Partner>();
    
    for(opportunity o : trigger.new) {       
        
        if(o.recordtypeid == '012000000004wVS' || o.recordtypeid == '012000000004wVT' || o.recordtypeid == '012000000004wVU') {
        
            if(o.partnerlookup__c != null) {            
            List<Partner> p1 = [select id,accounttoid,accountfromid,opportunityid from Partner where Opportunityid = :o.Id];
                if(p1.size() > 0) {
                    for(partner p : p1) {
                        if(p.accountToId != o.partnerlookup__c && p.accountFromId != o.partnerlookup__c) {
                            partnerstodelete.add(p);
                        }
                    }
                }
                
                if(p1.size() == 0) {
                    partnerstoinsert.add(new partner(opportunityid = o.id, accounttoid = o.partnerlookup__c));
                }
            }
            
            if(o.partnerlookup__c == null) {
            List<Partner> p2 = [select id,accounttoid,accountfromid,opportunityid from Partner where Opportunityid = :o.Id];
                if(p2.size() > 0) {
                    for(partner p : p2) {
                        partnerstodelete.add(p);
                    }
                }
            }

        }
    }
    
    if(PartnerstoDelete.size() > 0) delete PartnerstoDelete;
    if(PartnerstoInsert.size() > 0) insert PartnerstoInsert;              
}