public class SubscriptionChargeService 
{
   public static void UpdateProductFamily(List<Zuora__SubscriptionProductCharge__c> filteredCharges)
    {
        if(filteredCharges!= null && !filteredCharges.isEmpty())
        {
            Map<Id, String> chargeIdToProductFamily = ProductFamilyToSubscriptionCharge.getFamily(filteredCharges);
            System.debug('ChargeIdToProductFamily is :' + chargeIdToProductFamily);
            Map<Id, Zuora__SubscriptionProductCharge__c> chargesToUpdate = new Map<Id, Zuora__SubscriptionProductCharge__c>();
            Map<Id,Zuora__Subscription__c> updateSubscriptions = new Map<Id,Zuora__Subscription__c>();
            for(Zuora__SubscriptionProductCharge__c charge : filteredCharges)
            {
                if(!chargeIdToProductFamily.isEmpty() && chargeIdToProductFamily.containsKey(charge.Id))
                {
                    system.debug('In product charge update');
                    Zuora__SubscriptionProductCharge__c placeHolder = new Zuora__SubscriptionProductCharge__c(Id=charge.Id);
                    placeHolder.Product_Family__c = chargeIdToProductFamily.get(charge.Id);
                    if(!chargesToUpdate.containsKey(placeHolder.Id))
                    {
                        system.debug('Adding charge to map');
                        chargesToUpdate.put(placeHolder.Id, placeHolder);
                    }
                }
            }
            if(!chargestoUpdate.isEmpty())
            {
                update chargestoUpdate.values();
                
            }
            
        }
    }
}