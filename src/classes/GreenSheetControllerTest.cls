@isTest
private class GreenSheetControllerTest {

    private static Account testAccount;
    private static Account testPartnerAccount;
    private static Opportunity testOpp;
    private static Contact testContact;
    private static Contact testPartnerContact;
    private static Milestone__c testMilestone;
    private static Risk_To_Close__c testRisk;
    private static ApexPages.Standardcontroller sc;
    private static final String SEARCH_INPUT = 'Contact';

    private static void setup()
    {
        testAccount = TestUtils.createAccount( TestUtils.MARKETER_RECORD_TYPE.Id, false );
        testPartnerAccount = TestUtils.createAccount( TestUtils.MARKETER_RECORD_TYPE.Id, false );
        insert ( new list<Account>{testAccount, testPartnerAccount} );

        testOpp = TestUtils.createOpportunity( TestUtils.RESEARCH_RECORD_TYPE.Id, testAccount.Id, true );
        testOpp.PartnerLookup__c = testPartnerAccount.Id;
        update testOpp;

        testContact = TestUtils.createContacts( 'Account', 'Contact', testAccount.Id, false );
        testContact.ContactsRole__c = 'Test Role';
        testPartnerContact = TestUtils.createContacts( 'Partner', 'Contact', testPartnerAccount.Id, false );
        testPartnerContact.ContactsRole__c = 'Test Partner Role';
        insert ( new list<Contact>{testContact, testPartnerContact} );

        testMilestone = new Milestone__c ( Opportunity__c = testOpp.Id, Due_Date__c = system.today() );
        insert testMilestone;
        testRisk = new Risk_To_Close__c ( Opportunity__c = testOpp.Id );
        insert testRisk;

        PageReference pageRef = Page.GreenSheet;
        Test.setCurrentPageReference( pageRef );

        sc = new ApexPages.Standardcontroller( testOpp );
        system.currentPageReference().getParameters().put('id', testOpp.Id);

    }

    private static testMethod void testSearchContacts()
    {
        setup();

        test.startTest();
        GreenSheetController testController = new GreenSheetController( sc );
        testController.searchText = SEARCH_INPUT;
        testController.searchContacts();
        test.stopTest();

        system.AssertEquals(2, testController.accountContacts.size(), 'We expect to return the two contacts we inserted');

    }

    private static testMethod void testEditGreenSheet()
    {
        setup();

        test.startTest();
        GreenSheetController testController = new GreenSheetController( sc );
        testController.editGreenSheet();
        test.stopTest();

        system.assertEquals(true, testController.editMode, 'We expect edit mode to be true');

        for( GreenSheetController.MilestoneWrapper milestone : testController.milestones )
        {
            system.AssertEquals( true, milestone.isEdit, 'We expect isEdit to be true');
        }
        for( GreenSheetController.RiskWrapper risk : testController.risks )
        {
            system.assertEquals( true, risk.isEdit, 'We expect isEdit to be true (risk)');
        }
    }
    
    private static testMethod void testCancelGreenSheet()
    {
        setup();

        test.startTest();
        GreenSheetController testController = new GreenSheetController( sc );
        testController.cancelGreenSheet();
        test.stopTest();

        system.assertEquals(false, testController.editMode, 'We expect edit mode to be false');

        for( GreenSheetController.MilestoneWrapper milestone : testController.milestones )
        {
            system.AssertEquals( false, milestone.isEdit, 'We expect isEdit to be false');
        }
        for( GreenSheetController.RiskWrapper risk : testController.risks )
        {
            system.assertEquals( false, risk.isEdit, 'We expect isEdit to be false(risk)');
        }
    }
    
    

    private static testMethod void testSaveGreenSheet()
    {
        setup();

        test.startTest();
        GreenSheetController testController = new GreenSheetController( sc );
        testController.saveGreenSheet();
        test.stopTest();

        system.assertEquals(false, testController.editMode, 'We expect edit mode to be false');

        for( GreenSheetController.MilestoneWrapper milestone : testController.milestones )
        {
            system.AssertEquals( false, milestone.isEdit, 'We expect isEdit to be false');
        }
        for( GreenSheetController.RiskWrapper risk : testController.risks )
        {
            system.assertEquals( false, risk.isEdit, 'We expect isEdit to be false (risk)');
        }
    }

    private static testMethod void testAddNewMilestone()
    {
        setup();

        test.startTest();
        GreenSheetController testController = new GreenSheetController( sc );
        testController.addNewMilestone();
        test.stopTest();

        list<Milestone__c> insertedMilestones = [SELECT Id FROM Milestone__c WHERE Opportunity__c = :testOpp.Id];
        system.AssertEquals(4, testController.milestones.size(), 'We expect there to be one inserted milestone wrapper');
    }

    private static testMethod void testEditMilestone()
    {
        setup();

        test.startTest();
        GreenSheetController testController = new GreenSheetController( sc );
        ApexPages.currentPage().getParameters().put('edit', '0');
        for( GreenSheetController.MilestoneWrapper milestoneWrapper : testController.milestones )
        {
            if( milestoneWrapper.milestone.Id == testMilestone.Id )
            {
                system.AssertEquals( false, milestoneWrapper.isEdit, 'We expect isEdit to be false');
            }
        }
        testController.editMilestone();
        test.stopTest();

        for( GreenSheetController.MilestoneWrapper milestoneWrapper : testController.milestones )
        {
            if( milestoneWrapper.milestone.Id == testMilestone.Id )
            {
                system.AssertEquals( true, milestoneWrapper.isEdit, 'We expect isEdit to be true');
            }
        }

    }

    private static testMethod void testSaveMilestone()
    {
        setup();

        test.startTest();
        GreenSheetController testController = new GreenSheetController( sc );
        ApexPages.currentPage().getParameters().put('save', '0');
        testMilestone.Step_to_Close__c = 'Test Step To Close';
        testController.saveMilestone();
        test.stopTest();

        for( GreenSheetController.MilestoneWrapper milestoneWrapper : testController.milestones )
        {
            if( milestoneWrapper.milestone.Id == testMilestone.Id )
            {
                system.AssertEquals( false, milestoneWrapper.isEdit, 'We expect isEdit to be false');
            }
        }
        system.assertEquals('Test Step To Close', testMilestone.Step_to_Close__c, 'We expect the milestone to be updated');

    }

    private static testMethod void testDeleteMilestone()
    {
        setup();

        test.startTest();
        GreenSheetController testController = new GreenSheetController( sc );
        ApexPages.currentPage().getParameters().put('remove', '0');
        testController.deleteMilestone();
        test.stopTest();
        
        system.AssertEquals(2, testController.milestones.size(), 'We expect there to be one less milestone wrapper');
    }

    private static testMethod void testAddNewRisk()
    {
        setup();

        test.startTest();
        GreenSheetController testController = new GreenSheetController( sc );
        testController.addNewRisk();
        test.stopTest();

        system.assertEquals( 4, testController.risks.size(), 'We expect to have inserted one RiskWrapper');
    }

    private static testMethod void testEditRisk()
    {
        setup();

        test.startTest();
        GreenSheetController testController = new GreenSheetController( sc );
        ApexPages.currentPage().getParameters().put('editRisk', '0');
        for( GreenSheetController.RiskWrapper riskWrapper : testController.risks )
        {
          if( riskWrapper.risk.Id == testRisk.Id )
          {
            system.AssertEquals( false, riskWrapper.isEdit, 'We expect isEdit to be false');
          }
        }
        testController.editRisk();
        test.stopTest();

        for( GreenSheetController.RiskWrapper riskWrapper : testController.risks )
        {
          if( riskWrapper.risk.Id == testRisk.Id )
          {
            system.AssertEquals( true, riskWrapper.isEdit, 'We expect isEdit to be true');
          }
        }
    }

    private static testMethod void testSaveRisk()
    {
        setup();

        test.startTest();
        GreenSheetController testController = new GreenSheetController( sc );
        ApexPages.currentPage().getParameters().put('saveRisk', '0');
        testRisk.Mitigation_Plan__c = 'Test Mitigation Plan';
        testController.saveRisk();
        test.stopTest();

        for( GreenSheetController.RiskWrapper riskWrapper : testController.risks )
        {
          if( riskWrapper.risk.Id == testRisk.Id )
          {
            system.AssertEquals( false, riskWrapper.isEdit, 'We expect isEdit to be false');
          }
        }
        system.assertEquals('Test Mitigation Plan', testRisk.Mitigation_Plan__c, 'We expect the risk to be updated');
    }

    private static testMethod void testDeleteRisk()
    {
        setup();

        test.startTest();
        GreenSheetController testController = new GreenSheetController( sc );
        ApexPages.currentPage().getParameters().put('removeRisk', '0');
        testController.deleteRisk();
        test.stopTest();
        
        system.assertEquals(2, testController.risks.size(), 'We expect to have one less RiskWrapper');
    }

    private static testMethod void testAddContact()
    {
        setup();

        GreenSheetController testController = new GreenSheetController( sc );
        ApexPages.currentPage().getParameters().put('contactId', testContact.Id);
        testController.searchText = SEARCH_INPUT;
        testController.searchContacts();

        test.startTest();
        testController.addContact();
        test.stopTest();

        system.assertEquals(1, testController.selectedContacts.size(), 'There should be one selected contact');
        system.assertEquals('Account Contact - Test Role;', testController.opp.Green_Sheet_Contacts__c, 'We expect the Green Sheet Contacts field to be updated');
    }

    private static testMethod void testRemoveContact()
    {
        setup();

        GreenSheetController testController = new GreenSheetController( sc );
        ApexPages.currentPage().getParameters().put('selection', testContact.Id);
        testController.searchText = SEARCH_INPUT;
        testController.searchContacts();
        testController.addContact();

        test.startTest();
        testController.removeContact();
        test.stopTest();

        system.assertEquals(0, testController.selectedContacts.size(), 'There should be no selected contacts');
        system.assertEquals('', testController.opp.Green_Sheet_Contacts__c, 'We expect the Green Sheet Contacts field to be empty');


    }


}