public class PortalWizardSelectAcct_Controller {

Public String cuser = UserInfo.getUserId();
Public Contact c {get;set;}
public List <Account> acct;
Public string strID {get;set;}
Public Opportunity opp {get;set;}
Public Account UserAcct {get;set;}
Public User u {get;set;}
String usracctname {get;set;}


    public PortalWizardSelectAcct_Controller() {
    
    u = [select Id, ContactId from User where id = :cuser];
    c = [select Id, Account.ParentId from Contact where Id = :u.ContactId];
    UserAcct = [select Id, Name, CRM__r.Id from Account where Id = :c.AccountId];
    usracctname = UserAcct.Name.substring(0,7);
  
    }

    public pagereference getopp(){
     if(ApexPages.currentPage().getParameters().get('bypass') == 'true'){
     
     Account ac = [select Id, CurrencyISOCode, Parent.CRM__c from Account where Id = :ApexPages.currentPage().getParameters().get('acct')];
     opp = New Opportunity(
        
        Name = 'Portal Name',
        AccountId = ac.Id,
        CurrencyISOCode = ac.CurrencyISOCode,
        OwnerId = ac.Parent.CRM__c,
        RecordTypeId = TestUtils.PURCHASE_RECORD_TYPE.Id,
        CloseDate = system.Today(),
        TermofContractinmonths__c = '12',
        StageName = 'Verbal Commitment',
        Type = 'Supplement',
        Approval_Type__c = 'Finance Approval',
        Nature_of_Involvement__c = 'Yes',
        Type_of_InvolvementText__c = 'Reseller',
        PartnerLookup__c = UserAcct.Id,
        Partner_Discount__c = 0,
        Partner_Contact__c = c.Id,
        Deal_Closing_Through__c = 'Partner',
        Who_is_Invoiced__c = 'Partner is Invoiced',
        Created_in_Portal__c = true,
        Bypass_Product_Portfolio__c = true,
        CRM_User__c = ac.Parent.CRM__c,
        Did_not_Fill_Pre_Pipeline_Exit_Criteria__c = true,
        Did_not_Fill_Defining_Need_Exit_Criteria__c = true,
        Did_not_Fill_Est_Value_Exit_Criteria__c = true
        );
        
        insert opp;
        
        return new PageReference('/apex/PortalWizardProducts2?acct='+ac.Id+'&opp='+opp.Id+'&sup=true'); 
    }
else{ return null;}
}
    
    
    public List <Account> getacct(){
    
    if(ApexPages.currentPage().getParameters().get('type') == 'Termination'){
    Opportunity[] opps = [select Id, AccountId from Opportunity where PartnerLookup__r.Name like :usracctname+'%' and type = 'renewal' and isclosed = false];
    
    List<String> accts2 = new List<String>();
    
    for(Opportunity o : opps){
    accts2.add(o.AccountId);
    }
    
    
    acct = [select Id, Name, BillingCity, BillingCountry, BillingState from Account where Id in :accts2 order by Name Limit 1000];
    }
    else{
    
    Opportunity[] opps = [select Id, AccountId from Opportunity where PartnerLookup__r.Name like :usracctname+'%'];
    
    List<String> accts2 = new List<String>();
    
    for(Opportunity o : opps){
    accts2.add(o.AccountId);
    }
    acct = [select Id, Name, BillingCity, BillingCountry, BillingState from Account where Id in :accts2 order by Name Limit 1000];
    }
    return acct;
    
    }
    
    
        public PageReference Next(){
        
       
         
         if(ApexPages.currentPage().getParameters().get('type') == 'Service'){
     Account ac = [select Id, OwnerId, CRM__c, CurrencyISOCode, Parent.CRM__c from Account where Id = :strId]; 
       opp = New Opportunity(
        
        Name = 'Portal Name',
        AccountId = ac.Id,
        CurrencyISOCode = ac.CurrencyISOCode,
        OwnerId = UserAcct.CRM__r.Id,
        RecordTypeId = TestUtils.PURCHASE_RECORD_TYPE.Id,
        CloseDate = system.Today(),
        TermofContractinmonths__c = '12',
        StageName = 'Verbal Commitment',
        Type = 'Supplement', 
        Approval_Type__c = 'Finance Approval',
        Nature_of_Involvement__c = 'Yes',
        Type_of_InvolvementText__c = 'Reseller',
        PartnerLookup__c = UserAcct.Id,
        Partner_Discount__c = 0,
        Partner_Commission__c = 40,
        Partner_Contact__c = c.Id,
        Deal_Closing_Through__c = 'Partner',
        Who_is_Invoiced__c = 'Partner is Invoiced',
        Created_in_Portal__c = true,
        Bypass_Product_Portfolio__c = true,
        CRM_User__c = ac.Parent.CRM__c,
        Did_not_Fill_Pre_Pipeline_Exit_Criteria__c = true,
        Did_not_Fill_Defining_Need_Exit_Criteria__c = true,
        Did_not_Fill_Est_Value_Exit_Criteria__c = true);
        
         if(test.isrunningtest() == true){opp.OwnerId = '00500000006olmF';}
        
        insert opp;
        
pageReference ref = new PageReference('/apex/PortalWizardProducts?acct='+strId+'&opp='+opp.Id+'&sup=true'); 
ref.setRedirect(true);
    return ref; 
}

if(ApexPages.currentPage().getParameters().get('type') == 'Termination'){

return new PageReference('/apex/PortalWizardTermination?acct='+strId);  
}
else{
return null;
}
}
}