public class PortalWizardNewAcct_Controller {

Public Account acct {get;set;}
Public Opportunity opp {get;set;}
Public Account UserAcct {get;set;}
Public String Pcode;
Public String acctname {get;set;}
 
 public PortalWizardNewAcct_Controller() {
    
    User u = [Select Id, ContactId from User where Id = :UserInfo.getUserId()];
    
    Contact c = [Select Id, AccountId from Contact where Id = :u.ContactId];
    
    UserAcct = [select Id, Name, Promo_Code__r.Name, CRM__r.Id from Account where Id = :c.AccountId];
    
    acctname = UserAcct.Name.substring(0,7);
    
    acct = New Account(
        RecordtypeId = TestUtils.MARKETER_RECORD_TYPE.Id,
        Type = 'Current Client/Partner',
        Partner_Promo_Code__c = UserAcct.Promo_Code__r.Name,
        OwnerId = u.Id );
   
    opp = New Opportunity(
        
        Name = 'Portal Name',
        OwnerId = UserAcct.CRM__r.Id,
        RecordTypeId = TestUtils.PURCHASE_RECORD_TYPE.Id,
        CloseDate = system.Today(),
        TermofContractinmonths__c = '12',
        StageName = 'Verbal Commitment',
        Type = 'New Business',
        Approval_Type__c = 'Finance Approval',
        Nature_of_Involvement__c = 'Yes',
        Type_of_InvolvementText__c = 'Reseller',
        Partner_Contact__c = c.Id,
        Deal_Closing_Through__c = 'Partner',
        Who_is_Invoiced__c = 'Partner is Invoiced',
        Partner_Commission__c = 40,
        CRM_User__c = UserAcct.CRM__c,
        Bypass_Product_Portfolio__c = true,
        Created_in_Portal__c = true,
        Did_not_Fill_Pre_Pipeline_Exit_Criteria__c = true,
        Did_not_Fill_Defining_Need_Exit_Criteria__c = true,
        Did_not_Fill_Est_Value_Exit_Criteria__c = true);

    }

    public List<selectOption> getPCs() {
        List<selectOption> options = new List<selectOption>(); //new list for holding all of the picklist options
        options.add(new selectOption('', '- None -')); //add the first option of '- None -' in case the user doesn't want to select a value or in case no values are returned from query below
        for (Promo_Code__c promos : [SELECT Id, Name, Account__r.Name FROM Promo_Code__c WHERE Account__r.Name like :acctname+'%' Order by Name]) { //query for User records with System Admin profile
            options.add(new selectOption(promos.Name, promos.Name+' - '+promos.Account__r.Name)); //for all records found - add them to the picklist options
        }
        return options; //return the picklist options
    }

    public pagereference Next(){
    //insert acct;
    
    Promo_Code__c parentacct = [select Id, CRM_ID__c, Account__c from Promo_Code__c where Name = :acct.Partner_Promo_Code__c LIMIT 1];   
    
    acct.ParentId = parentacct.Account__c;
    acct.CRM__c = parentacct.CRM_ID__c;
    //update acct;
    insert acct;
    
    opp.AccountId = acct.Id;
    opp.CRM_User__c = parentacct.CRM_ID__c;
    opp.OwnerId = parentacct.CRM_ID__c;
    opp.PartnerLookup__c = parentacct.Account__c;
   
    insert opp;
   
   
    
    return new PageReference('/apex/PortalWizardProducts?acct='+acct.Id+'&opp='+opp.Id+'&sup=false');  
    }
    
    public pagereference Cancel(){

    return new PageReference('/home/home.jsp');  
    }

}