public with sharing class OnHoldPageController 
{
    public Certification_Application__c certApp {get;set;}
    public Boolean ChangeStatus = false;
    
    public OnHoldPageController(ApexPages.StandardController controller) 
    {
        certApp = [SELECT Id, Application_Status__c, Application_Result__c, Record_Locked__c, Application_Fail_Details__c, Application_Fail_Reason__c, License_Fee_Opp_Stage__c, License_Fee_Opportunity__c, License_Fee_Opportunity__r.IsClosed 
        FROM Certification_Application__c 
        WHERE Id = :ApexPages.currentPage().getParameters().get('id')];
        
        
        ChangeStatus = false;
    }

    public pageReference checkStatus()
    {    
        certApp.Application_Status__c = 'On Hold';
        certApp.Application_Result__c = 'On Hold';
        update certApp;
        ChangeStatus = true;
        return null;
        
    }
    
    public pageReference SaveandBacktoApp()
    {
        certApp.Record_Locked__c = true;
        certApp.Date_to_On_Hold__c = system.today();
        update certApp;
        pageReference toApp = new pageReference('/'+certApp.Id);
        return toApp;
    }
    public pageReference BacktoApp()
    {
        pageReference toApp = new pageReference('/'+certApp.Id);
        return toApp;
    }
}