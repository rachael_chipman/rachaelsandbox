public with sharing class SObjectTriggerServices 
{
	public static final String DELIVERABILITY_CHANNEL_REFERRAL = 'Deliverability Channel Referral';
	public static final String CHANNEL_PROJECT_RUN_REFERRAL = 'Channel Project Run Referral';
	public static final String SS_WIDGET = 'GL_CH_SSO_Widget';
	
	public static Boolean isValidCampaign( String campaignName )
	{
		return( !String.isBlank( campaignName ) && 
					( campaignName.contains( DELIVERABILITY_CHANNEL_REFERRAL ) 
					|| campaignName.contains( CHANNEL_PROJECT_RUN_REFERRAL ) 
					|| campaignName.contains( SS_WIDGET ) ) 
				);
	}

	// Returns a dynamic SOQL statement for the whole object, includes only creatable fields since we will be inserting a cloned result of this query
    public static string getCreatableFieldsSOQLOpp(String objectName, String whereClause){
         
        String selects = '';
         
        if (whereClause == null || whereClause == '')
        { 
        	return null; 
        }
         
        // Get a map of field name and field token
        Map<String, Schema.SObjectField> fMap = Schema.getGlobalDescribe().get(objectName.toLowerCase()).getDescribe().Fields.getMap();
        list<string> selectFields = new list<string>();
         
        if (fMap != null)
        {
            for (Schema.SObjectField ft : fMap.values()){ // loop through all field tokens (ft)
                Schema.DescribeFieldResult fd = ft.getDescribe(); // describe each field (fd)
                if (fd.isCreateable()){ // field is creatable
                    selectFields.add(fd.getName());
                }
            }
        }
         
        if (!selectFields.isEmpty()){
            for (string s:selectFields){
                selects += s + ',';
            }
            if (selects.endsWith(',')){selects = selects.substring(0,selects.lastIndexOf(','));}
             
        }
         
        return 'SELECT ' + selects + ', Cert_Tier_High_or_Low__c, Minimum_Start_Date__c, Max_End_Date__c, Cert_Only_Opportunity__c FROM ' + objectName + ' WHERE ' + whereClause;
         
    }

    public static string getCreatableFieldsSOQL(String objectName, String whereClause){
         
        String selects = '';
         
        if (whereClause == null || whereClause == '')
        { 
        	return null; 
        }
         
        // Get a map of field name and field token
        Map<String, Schema.SObjectField> fMap = Schema.getGlobalDescribe().get(objectName.toLowerCase()).getDescribe().Fields.getMap();
        list<string> selectFields = new list<string>();
         
        if (fMap != null)
        {
            for (Schema.SObjectField ft : fMap.values()){ // loop through all field tokens (ft)
                Schema.DescribeFieldResult fd = ft.getDescribe(); // describe each field (fd)
                if (fd.isCreateable()){ // field is creatable
                    selectFields.add(fd.getName());
                }
            }
        }
         
        if (!selectFields.isEmpty()){
            for (string s:selectFields){
                selects += s + ',';
            }
            if (selects.endsWith(',')){selects = selects.substring(0,selects.lastIndexOf(','));}
             
        }
         
        return 'SELECT ' + selects + ', Opportunity_Type__c, Opportunity.Previous_Opportunity_Lookup__c FROM ' + objectName + ' WHERE ' + whereClause;
         
    }

}