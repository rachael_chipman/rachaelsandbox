@isTest
public class PartnerCreateTest {
    static testMethod void runPartnerCreateTest() {       
        account a = new account(name = 'test account');
        insert a;
             
        account partner = new account(name = 'partner account');
        insert partner;
        
        Promo_Code__c pc = new Promo_Code__c(Name = 'TEST001', Account__c = partner.Id);
        insert pc;
        update pc;

        opportunity o = new opportunity(name='test opp',closedate=date.today(),stagename='Investigating',recordtypeid='012000000004wVT',accountid=a.id);
        insert o;
        
        test.startTest();
        o.partnerlookup__c = partner.id;
        update o;
        test.stopTest();
    }
}