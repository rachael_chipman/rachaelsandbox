@isTest(SeeAllData = true) 
private class TestOpportunityServices {
    
    //private static List<Rate_Card__c> testRateCards;
    private static List<Decimal> thresholds;
    private static List<Decimal> rates;
    private static Opportunity testOpportunity;

    private static void setup( Integer numOfRateCards ){
        final Decimal THRESHOLD1 = 5200000.00;
        final Decimal THRESHOLD2 = 10400000.00;
        final Decimal THRESHOLD3 = 50400000.00;
        thresholds = new List<Decimal> { THRESHOLD1, THRESHOLD2, THRESHOLD3 };

        final Decimal RATE1 = 0.25;
        final Decimal RATE2 = 0.5;
        final Decimal RATE3 = 0.6;
        final Decimal RATE4 = 0.65;
        rates = new List<Decimal> { RATE1, RATE2, RATE3, RATE4 };

        //testRateCards = TestUtils.createRateCards ( numOfRateCards, thresholds, rates, false );

        RecordType accountRecordtype = [ SELECT Id FROM RecordType WHERE SObjectType = 'Account' AND IsActive = TRUE LIMIT 1 ];
        Account opportunityAccount = TestUtils.createAccount( accountRecordtype.Id, true );
        
        RecordType opportunityRecordType = [ SELECT Id FROM RecordType WHERE SObjectType = 'Opportunity' AND IsActive = TRUE LIMIT 1 ];
        testOpportunity = TestUtils.createOpportunity( opportunityRecordType.Id, opportunityAccount.Id, true);
    }
    
    private static void setupWithOLI( Date startDate, Date endDate ){
        setup( 1);
        
        OpportunityLineItem testOLI = TestUtils.createOpportunityLineItems(testOpportunity.Id, 1, false)[0];
        testOLI.Start_Date__c = startDate;
        testOLI.End_Date__c = endDate;
        insert testOLI;
        
        testOpportunity = [Select Id, Billing_Type__c, Minimum_Start_Date__c, Max_End_Date__c, StageName, AccountId, Billing_Terms_Number_Calc__c,
                                Account.Invoicing_Contact__c, FF_Company_Formula__c, ExpectedRevenue, Invoice_Created__c, Bypass_Financial_Force__c, Type
                           From Opportunity Where Id = :testOpportunity.Id ];
    }
    /* COMMENTED OUT 2/24/14 by JK/YC 
    @isTest static void testFilterforCommissionCalculation (){

        Account testAccount = TestUtils.createAccount( TestUtils.PARTNER_RECORD_TYPE.Id, true );
        List<Opportunity> testOpps = TestUtils.createOpportunities( 2, TestUtils.DEMONSTRATING_CAPABILITIES_RECORD_TYPE.Id, testAccount.Id, true );
        
        Map<Id, Opportunity> oldOpps = new Map<Id, Opportunity> ( testOpps.deepClone(true, false, false ) );    
        testOpps[0].StageName = 'Finance Approved';
        testOpps[0].Rate_Already_Calculated__c = 0;
        update testOpps[0];

        Test.startTest();
            Set<Id> filteredOpp = OpportunityServices.filterforCommissionCalculation ( testOpps, oldOpps );
        Test.stopTest();

        System.assertEquals ( 1, filteredOpp.size(), 'Only one opportunity got updated' );
        for ( Id opportunityId : filteredOpp )
            System.assertEquals ( testOpps[0].Id, opportunityId, 'Making sure the opportunity that was updated is returned');
    }
    @isTest static void testcalculateCommissionForTeamMembers() {
        
        final Integer NUM_OF_CARDS = 1;
        final String TEST_COMMISSION_RATE = '0.12';
        final String TEST_OPPORTUNITY_TYPE = 'testoppType';
        final String TEST_USERROLE = 'testUserRole';

        setup ( NUM_OF_CARDS );

        Set<String> userRoles = new Set<String>();
        Rate_Card__c rateCard = testRateCards[0];
        rateCard.Opportunity_Type__c = TEST_OPPORTUNITY_TYPE;
        rateCard.User_Role__c = TEST_USERROLE;
        rateCard.Commission_Rate__c = TEST_COMMISSION_RATE;

        insert testRateCards;

        Account testAccount = TestUtils.createAccount( TestUtils.PARTNER_RECORD_TYPE.Id, true );
        Opportunity testOpp = TestUtils.createOpportunity( TestUtils.DEMONSTRATING_CAPABILITIES_RECORD_TYPE.Id, testAccount.Id, false );
        testOpp.Type = TEST_OPPORTUNITY_TYPE;
        insert testOpp;

        User testUser = TestUtils.getTestUser('testUsersXX123', 'Standard User');
        System.runAs ( TestUtils.ADMIN_USER ){
            insert testUser;
        }

        OpportunityTeamMember teamMember = TestUtils.createOpportunityTeamMember ( testOpp.Id, testUser.Id, TEST_USERROLE, true );

        List<Opportunity> oppWithMembers = [SELECT Id, Type, (SELECT User.Name, TeamMemberRole, UserId FROM OpportunityTeamMembers )
                                                FROM Opportunity 
                                                WHERE Id = :testOpp.Id ];
        Test.startTest();
            List<OpportunityTeamMember> memberToUpdate = OpportunityServices.calculateCommissionForTeamMembers( oppWithMembers );
        Test.stopTest();

        System.assertEquals ( 1, memberToUpdate.size(), 'The opportunity has only one team member');
        System.assertEquals ( Decimal.valueOf ( TEST_COMMISSION_RATE ), memberToUpdate[0].Commission_Percentage__c, 'The commission should be set from the rate card object');
    }

    @isTest static void testcalculateCommissionForTeamMembers_WithoutRateCard() {

        Account testAccount = TestUtils.createAccount( TestUtils.PARTNER_RECORD_TYPE.Id, true );
        Opportunity testOpp = TestUtils.createOpportunity( TestUtils.DEMONSTRATING_CAPABILITIES_RECORD_TYPE.Id, testAccount.Id, true );
       
        User testUser = TestUtils.getTestUser('testUsersXX123', 'Standard User');
        System.runAs ( TestUtils.ADMIN_USER ){
            insert testUser;
        }

        OpportunityTeamMember teamMember = TestUtils.createOpportunityTeamMember ( testOpp.Id, testUser.Id, '', true );

        List<Opportunity> oppWithMembers = [SELECT Id, Type, (SELECT User.Name, TeamMemberRole, UserId FROM OpportunityTeamMembers )
                                                FROM Opportunity 
                                                WHERE Id = :testOpp.Id ];
        Test.startTest();
            List<OpportunityTeamMember> memberToUpdate = OpportunityServices.calculateCommissionForTeamMembers( oppWithMembers );
        Test.stopTest();

        System.assertEquals ( 0, memberToUpdate.size(), 'Team member should not be updated without rate card');
    }

    @istest(SeeAllData=true) static void testPopulateCommissionOnManagementNACV_Quarterly(){
        
        final Integer NUM_OF_OLIS = 2;
        final String TEST_USERROLE = 'testUserRole';
        final Integer NUM_OF_CARDS = 1;

        User testUser = TestUtils.getTestUser('testUsersXX123', 'Standard User');
        System.runAs ( TestUtils.ADMIN_USER ){
            insert testUser;
        }

        setup ( NUM_OF_CARDS );
        testRateCards[0].User_Role__c = TEST_USERROLE;
        insert testRateCards;

        Account testAccount = TestUtils.createAccount( TestUtils.PARTNER_RECORD_TYPE.Id, true );
        List<Opportunity> testOpps = TestUtils.createOpportunities( 2, TestUtils.DEMONSTRATING_CAPABILITIES_RECORD_TYPE.Id, testAccount.Id, false );
        testOpps[0].Reporting_Date__c = Date.today();
        testOpps[0].StageName = 'Finance Approved';
        testOpps[1].Reporting_Date__c = Date.today().addMonths ( -5 );
        testOpps[1].StageName = 'Finance Approved';
        insert testOpps;

        Product2 testProduct = TestUtils.createProduct( true );
        PricebookEntry testPricebookEntry = TestUtils.createPricebookEntry( testProduct.Id, TestUtils.STANDARD_PRICE_BOOK.Id, true );
        
        List<OpportunityLineItem> testOpportunityLineItems = new List<OpportunityLineItem>();
        for ( Integer i = 0 ; i < NUM_OF_OLIS ; i++ )
        {
            OpportunityLineItem testOLI = TestUtils.createOpportunityLineItem( testOpps[i].Id, testPricebookEntry.Id, false );
            testOpportunityLineItems.add( testOLI );
        }
        testOpportunityLineItems[1].UnitPrice = 50400000.00;
        insert testOpportunityLineItems;


        List<OpportunityTeamMember> teamMembers = new List<OpportunityTeamMember>();
        for ( Integer i = 0; i< 2; i++ ) {
            OpportunityTeamMember member = TestUtils.createOpportunityTeamMember ( testOpps[i].Id, testUser.Id, TEST_USERROLE, false );
            member.Company_Exchange_Rate__c = 1;
            member.Exchange_Rate__c = 1;
            teamMembers.add ( member );
        }
        insert teamMembers; 

        Set<Id> userIds = new Set<Id> {teamMembers[0].UserId};
        userIds.add ( teamMembers[1].UserId );
        Map<String, RateCardModel> managementNACVRates = RateCardServices.getManagementNACVRateModels ( new Set<String>{TEST_USERROLE}, new Set<Id>{null} );    
        List<Opportunity> oppWithTeamMembers = [SELECT Id, Type, (SELECT TeamMemberRole, UserId FROM OpportunityTeamMembers )
                                                FROM Opportunity 
                                                WHERE Id IN :testOpps ];

        Test.startTest();
            List<OpportunityTeamMember> teamMembersToUpdate = OpportunityServices.populateCommissionOnManagementNACV_Quarterly ( userIds, oppWithTeamMembers, managementNACVRates, new Set<Id>() );
        Test.stopTest();

        System.assertEquals ( 2, teamMembersToUpdate.size(), 'There should only be 2 team member for quarter to update');
        System.assertEquals ( rates[0], teamMembersToUpdate[0].Commission_Percentage__c, 'the total sum is less the the first threshold');
        System.assertEquals ( rates[0], teamMembersToUpdate[1].Commission_Percentage__c, 'the total sum is less the the first threshold');

    }

    @istest(SeeAllData=true) static void testPopulateCommissionOnManagementNACV_yearly(){
        
        final Integer NUM_OF_OLIS = 2;
        final String TEST_USERROLE1 = 'testUserRole';
        final String TEST_USERROLE2 = 'testUserRole2';
        final Integer NUM_OF_CARDS = 2;

        final List<String> testUserRoles = new List<String> {TEST_USERROLE1, TEST_USERROLE2};
        User testUser = TestUtils.getTestUser('testUsersXX123', 'Standard User');
        System.runAs ( TestUtils.ADMIN_USER ){
            insert testUser;
            testRateCards[i].User__c = testUser.Id;
        }

        setup ( NUM_OF_CARDS );
        for ( Integer i = 0; i < NUM_OF_CARDS; i++ ){
            testRateCards[i].User_Role__c = testUserRoles[i];
        }

        insert testRateCards;

        Account testAccount = TestUtils.createAccount( TestUtils.PARTNER_RECORD_TYPE.Id, true );
        List<Opportunity> testOpps = TestUtils.createOpportunities( 2, TestUtils.DEMONSTRATING_CAPABILITIES_RECORD_TYPE.Id, testAccount.Id, false );
        testOpps[0].Reporting_Date__c = Date.today();
        testOpps[0].StageName = 'Finance Approved';
        testOpps[1].Reporting_Date__c = Date.today().addYears ( -5 );
        testOpps[1].StageName = 'Finance Approved';
        insert testOpps;

        Product2 testProduct = TestUtils.createProduct( true );
        PricebookEntry testPricebookEntry = TestUtils.createPricebookEntry( testProduct.Id, TestUtils.STANDARD_PRICE_BOOK.Id, true );
        
        List<OpportunityLineItem> testOpportunityLineItems = new List<OpportunityLineItem>();
        for ( Integer i = 0 ; i < NUM_OF_OLIS ; i++ )
        {
            OpportunityLineItem testOLI = TestUtils.createOpportunityLineItem( testOpps[i].Id, testPricebookEntry.Id, false );
            testOpportunityLineItems.add( testOLI );
        }
        testOpportunityLineItems[1].UnitPrice = 50400000.00;
        insert testOpportunityLineItems;


        List<OpportunityTeamMember> teamMembers = new List<OpportunityTeamMember>();
        for ( Integer i = 0; i < NUM_OF_CARDS ; i++ ) {
            OpportunityTeamMember member = TestUtils.createOpportunityTeamMember ( testOpps[i].Id, testUser.Id, testUserRoles[i], false );
            member.Company_Exchange_Rate__c = 1;
            member.Exchange_Rate__c = 1;
            teamMembers.add ( member );
        }
        insert teamMembers; 

        Set<Id> userIds = new Set<Id> {teamMembers[0].UserId};
        userIds.add ( teamMembers[1].UserId );
        Map<String, RateCardModel> managementNACVRates = RateCardServices.getManagementNACVRateModels ( new Set<String>{TEST_USERROLE1, TEST_USERROLE2}, userIds ); 
        List<Opportunity> oppWithTeamMembers = [SELECT Id, Type, (SELECT TeamMemberRole, UserId FROM OpportunityTeamMembers )
                                                FROM Opportunity 
                                                WHERE Id IN :testOpps ];

        Test.startTest();
            List<OpportunityTeamMember> teamMembersToUpdate = OpportunityServices.populateCommissionOnManagementNACV_Yearly ( userIds, oppWithTeamMembers, managementNACVRates,  new Set<Id>() );
        Test.stopTest();

        System.assertEquals ( 2, teamMembersToUpdate.size(), 'There should only be 2 team member for quarter to update');
        System.assertEquals ( rates[0], teamMembersToUpdate[0].Commission_Percentage__c, 'the total sum is less the the first threshold');
        System.assertEquals ( rates[0], teamMembersToUpdate[1].Commission_Percentage__c, 'the total sum is less the the first threshold');

    }
    */

    /**********************************  Tests for Filtering Opportunities ************************************/

    
    public static testMethod void test_forFilterOpportunitiesForInvoice_withCorrectFields(){
        setupWithOLI( Date.today(), Date.today().addMonths(12) );
        
        testOpportunity.StageName = 'Random';
        testOpportunity.Billing_Type__c = OpportunityServices.MONTHLY;
        testOpportunity.Type = 'New Business';
        testOpportunity.Bypass_Financial_Force__c = false;
        testOpportunity.Invoice_Created__c = false;
        
        // Old Map contents
        Map<Id, Opportunity> oldOppMap = new Map<Id, Opportunity>{ testOpportunity.Id => testOpportunity };
        //oldOppMap.put( testOpportunity.Id, testOpportunity );
        
        //New list Contents
        Opportunity copyTestOpp = testOpportunity.Clone(true, true);
        copyTestOpp.StageName = OpportunityServices.FINANCE_APPROVED;
        List< Opportunity> newOppList = new List<Opportunity>{copyTestOpp};

        Test.startTest();
            List<Opportunity> filterOpps = OpportunityServices.filterOpportunitiesReadyForInvoice( newOppList, oldOppMap );
        Test.stopTest();
        
        System.assertEquals(filterOpps.size(), 1, 'There should be atleast one opportunity created because of correct fields has been set.');
    }

    public static testMethod void test_forFilterOpportunitiesForInvoice_InvoiceCreatedUnChecked(){
        setupWithOLI( Date.today(), Date.today().addMonths(12) );
        
        testOpportunity.Billing_Type__c = OpportunityServices.MONTHLY;
        testOpportunity.Type = 'New Business';
        testOpportunity.Bypass_Financial_Force__c = false;
        testOpportunity.StageName = OpportunityServices.FINANCE_APPROVED;
        testOpportunity.Invoice_Created__c = true;
        
        // Old Map contents
        Map<Id, Opportunity> oldOppMap = new Map<Id, Opportunity>{ testOpportunity.Id => testOpportunity };
        //oldOppMap.put( testOpportunity.Id, testOpportunity );
        
        //New list Contents
        Opportunity copyTestOpp = testOpportunity.Clone(true, true);
        copyTestOpp.Invoice_Created__c = false;
        List< Opportunity> newOppList = new List<Opportunity>{copyTestOpp};

        Test.startTest();
            List<Opportunity> filterOpps = OpportunityServices.filterOpportunitiesReadyForInvoice( newOppList, oldOppMap );
        Test.stopTest();
        
        System.assertEquals( 1, filterOpps.size(), 'There should be atleast one opportunity created because of correct fields has been set.');
    }

    public static testMethod void test_forFilterOpportunitiesForInvoice_InvoiceCreatedUnChecked_Negative(){
        setupWithOLI( Date.today(), Date.today().addMonths(12) );
        
        testOpportunity.StageName = 'Random';
        testOpportunity.Billing_Type__c = OpportunityServices.MONTHLY;
        testOpportunity.Type = 'New Business';
        testOpportunity.Bypass_Financial_Force__c = false;
        testOpportunity.Invoice_Created__c = true;
        
        // Old Map contents
        Map<Id, Opportunity> oldOppMap = new Map<Id, Opportunity>{ testOpportunity.Id => testOpportunity };
        //oldOppMap.put( testOpportunity.Id, testOpportunity );
        
        //New list Contents
        Opportunity copyTestOpp = testOpportunity.Clone(true, true);
        testOpportunity.Invoice_Created__c = false;
        List< Opportunity> newOppList = new List<Opportunity>{copyTestOpp};

        Test.startTest();
            List<Opportunity> filterOpps = OpportunityServices.filterOpportunitiesReadyForInvoice( newOppList, oldOppMap );
        Test.stopTest();
        
        System.assertEquals( 0, filterOpps.size(), 'There should be atleast no opportunity returned');
    }
    
    public static testMethod void test_forFilterOpportunitiesForInvoice_withInCorrectFields(){
        setupWithOLI( Date.today(), Date.today().addMonths(12) );
        testOpportunity.StageName = OpportunityServices.FINANCE_APPROVED; 
        
        Map<Id, Opportunity> oldOppMap = new Map<Id, Opportunity>();
        oldOppMap.put( testOpportunity.Id, testOpportunity );
        
        List< Opportunity> newOppList = new List <Opportunity>();
        Opportunity opp = new Opportunity( Id = testOpportunity.Id, Billing_Type__c = OpportunityServices.MONTHLY, StageName = OpportunityServices.FINANCE_APPROVED );
        newOppList.add(opp);
        
        Test.startTest();
            List<Opportunity> filterOpps = OpportunityServices.filterOpportunitiesReadyForInvoice( newOppList, oldOppMap );
        Test.stopTest();
        
        System.assertEquals(filterOpps.size(), 0, 'There should be no opportunites returning because the Stage Name has not changed.');
    }
}