public class PortalWizardTermination_Controller {

Public Account acct {get;set;}
Public Opportunity opp {get;set;}

    public PortalWizardTermination_Controller() {
    
    acct = [select Id from Account where Id = :ApexPages.currentPage().getParameters().get('acct')];
    opp = [select Id, type, Reasons_for_Lost_Opportunities__c, Lost_Pass_Details__c, Lost_Pass_Specifics__c from Opportunity where type = 'renewal' and AccountId = :acct.Id and IsClosed = false limit 1];
    
    
    
   

    }
    
     public List<selectOption> getreasons() {
        List<selectOption> options = new List<selectOption>();
        options.add(new selectOption('', '- None -'));
        options.add(new selectOption('Bankruptcy / Going out of Business', 'Bankruptcy / Going out of Business'));
        options.add(new selectOption('Budget owner does not see value', 'Budget owner does not see value'));
        options.add(new selectOption('Customer unwilling to make changes', 'Customer unwilling to make changes'));
        options.add(new selectOption('Loss to Competition', 'Loss to Competition'));
        options.add(new selectOption('Not logging into tool / Lack of engagement by daily contact', 'Not logging into tool / Lack of engagement by daily contact'));
        options.add(new selectOption('Perception ESP providing', 'Perception ESP providing'));
        options.add(new selectOption('Switch from Channel Reseller to Direct', 'Switch from Channel Reseller to Direct'));
        options.add(new selectOption('Specific Reason (details required)', 'Other (Please Provide Details)'));      
        
        return options; //return the picklist options
    }
    
     public pagereference Cancel(){
    
   
    return new PageReference('/home/home.jsp');  
    }


public pagereference Next(){
    opp.Reasons_for_Lost_Opportunities__c = 'Client Intiated';
    opp.StageName = 'Approval Required';
    opp.Submit_From_Trigger__c = 'Yes';
    update opp;
    
    return new PageReference('/home/home.jsp');  
    }

}