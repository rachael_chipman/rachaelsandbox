@isTest
private class CampaignMemberTriggerTest 
{
    static final Integer NUM_RECORDS = 10;
    static Campaign testCampaign;
    static List<CampaignMember> testCMs;
    static List<Lead> testLeads;
    static Set<Integer> expectedAutoNumbers; 
/*
    static testMethod void test_autoUpdateCMs_noPrevCampaign_Members()
    {
        setup();

        Test.startTest();
            insert testCMs;
        Test.stopTest();

        List<Lead> resultLeads = [SELECT Campaign_Lead_Number__c FROM Lead WHERE ID IN: testLeads];

        Set<Integer> autoNumResults = new Set<Integer>();
        for(Lead ld: resultLeads)
        {
            autoNumResults.add((Integer)ld.Campaign_Lead_Number__c);
        }

        System.assert(expectedAutoNumbers.containsAll(autoNumResults), 'All of the expected autonumbers to have the autonumber value set on the leads of the campaign members.');
        System.assert(autoNumResults.containsAll(expectedAutoNumbers), 'All of the autonumber results should have the expected autonumber values set on the leads of the campaign members.');
    }

    static testMethod void test_autoUpdateLead_previousLeadsExist()
    {
        setup();

        Lead prevLead =  TestUtils.createLeads('random', 'random', 'random', true);
        CampaignMember previousCM =  TestUtils.createCampaignMember(testCampaign.Id, prevLead.Id , null, false);
        insert previousCM; 

        Lead nextLead =  TestUtils.createLeads('random', 'random', 'random', true);
        CampaignMember testCampaignMember = TestUtils.createCampaignMember(testCampaign.Id, nextLead.Id , null, false);

        Test.startTest();
            insert testCampaignMember;
        Test.stopTest();

        CampaignMember resultCM = [SELECT Campaign_Member_Number__c FROM CampaignMember WHERE Id =: testCampaignMember.Id];
        Lead resultLead = [SELECT Campaign_Lead_Number__c FROM Lead WHERE Id =: nextLead.Id];

        System.assertEquals(2, resultCM.Campaign_Member_Number__c , 'We expect the 2nd campaign member to have have a campaign member number of 2');
        System.assertEquals(2, resultLead.Campaign_Lead_Number__c, 'We the 2nd campaign members lead to have its campaign lead number equal to the campaign member number ');
    }

    static void setup()
    {   
        testCampaign = TestUtils.createCampaign(false);
        testCampaign.Name = 'Request a Demo';
        insert testCampaign;

        testLeads = TestUtils.createLeadsMultiple( NUM_RECORDS, 'random' , 'random' ,'random' , true);
        
        testCMs = new List<CampaignMember>();
        expectedAutoNumbers = new Set<Integer>();

        for(Integer i = 0; i <  NUM_RECORDS ; i++)
        {
            CampaignMember newCM = TestUtils.createCampaignMember ( testCampaign.Id, testLeads[i].Id , NULL, false);
            testCMs.add(newCM);
            expectedAutoNumbers.add( i + 1);
        }   
    }
    */
}