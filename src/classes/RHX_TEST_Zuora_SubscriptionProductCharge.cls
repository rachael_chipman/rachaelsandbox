@isTest(SeeAllData=true)
public class RHX_TEST_Zuora_SubscriptionProductCharge {
	static testMethod void RHX_Testmethod() {
        List<sObject> sourceList = [SELECT Id 
			FROM Zuora__SubscriptionProductCharge__c LIMIT 1];
        if(sourceList.size() == 0) {
            sourceList.add(
                    new Zuora__SubscriptionProductCharge__c()
            );
        }
    	Database.upsert(sourceList);
    }
}