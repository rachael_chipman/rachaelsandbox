/*** Controller ***/
public class custParams {
         
  private String year   = null;
  private String month  = null;
  private String sortby = null;
         
  // define lists elements
           
  public List<SelectOption> getYears() {
          List<SelectOption> options = new List<SelectOption>(); 
          options.add(new SelectOption('2009','2009')); 
          options.add(new SelectOption('2010','2010')); 
          options.add(new SelectOption('2011','2011')); 
          options.add(new SelectOption('2012','2012')); 
          return options; 
  }

  public List<SelectOption> getMonths() {
          List<SelectOption> options = new List<SelectOption>(); 
          options.add(new SelectOption('01','01')); 
          options.add(new SelectOption('02','02')); 
          options.add(new SelectOption('03','03')); 
          options.add(new SelectOption('04','04')); 
          options.add(new SelectOption('05','05')); 
          options.add(new SelectOption('06','06')); 
          options.add(new SelectOption('07','07')); 
          options.add(new SelectOption('08','08')); 
          options.add(new SelectOption('09','09')); 
          options.add(new SelectOption('10','10')); 
          options.add(new SelectOption('11','11')); 
          options.add(new SelectOption('12','12')); 
          return options; 
  }

  public List<SelectOption> getSortbys() {
          List<SelectOption> options = new List<SelectOption>(); 
          options.add(new SelectOption('acct_mgr','Account Manager')); 
          options.add(new SelectOption('products','Product')); 
          options.add(new SelectOption('channel_svc_type','Direct/Channel')); 
          return options; 
  }
   
  // get methods

  public String getYear() {
      if(year==null){year='2009';}
      return year;
  }
                    
  public String getMonth() {
      if(month==null){month='01';}
      return month;
  }

  public String getSortby() {
      if(sortby==null){sortby='acct_mgr';}
      return sortby;
  }

  
  // set methods

  public void setYear(String Year) { 
      this.year = year; 
  }

  public void setMonth(String Month) { 
      this.month = month; 
  }

  public void setSortby(String Sortby) { 
      this.sortby = sortby; 
  }
  
  public PageReference popFields()
  {
      //system.debug('This is the selected YYYYMM: ' + year + month);
      //system.debug('This is the selected Sort by: ' + sortby);
      return null;
  } 
  
static testmethod void testCode() {
  
       PageReference pageRef = Page.active_customers;
        Test.setCurrentPage(pageRef);
        
        // Create an instance of the controller
        custParams custParamsController = new custParams();

         // Get the year from the controller
        List<SelectOption> years = custParamsController.getYears();
        // Verify that there is at least one year to edit
        system.assertEquals(years.size() > 0, true);

         // Get the months from the controller
        List<SelectOption> months = custParamsController.getMonths();
        // Verify that there is at least one month
        system.assertEquals(months.size() > 0, true);

         // Get the sortbys from the controller
        List<SelectOption> sortbys = custParamsController.getSortbys();
        // Verify that there is at least one sortby
        system.assertEquals(sortbys.size() > 0, true);

        // Get the sortby from the controller
        String sortby = custParamsController.getSortby();
        system.assertEquals(sortby > '', true);

        // Get the month from the controller
        String month = custParamsController.getMonth();
        system.assertEquals(month > '', true);

        // Get the year from the controller
        String year = custParamsController.getYear();
        system.assertEquals(year > '', true);
        
 }
 }