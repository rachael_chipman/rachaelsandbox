global with sharing class LeadAssignmentCatcherSchedule implements Schedulable {
	
	global void execute(SchedulableContext sc){
		
		LeadAssignmentCatcherBatch assigner = new LeadAssignmentCatcherBatch();
        
        // Must use batch size of 1 for assignment trigger consistancy
        ID batchprocessid = Database.executeBatch( assigner, 1 );
		System.debug( logginglevel.FINE, batchprocessid );
	}
	
	static testmethod void testLeadAssignmentCatcherSchedule() {
		
		Account acct = [SELECT Id FROM Account LIMIT 1];
   		User targetUserId = [SELECT Id FROM User WHERE Alias = 'EloqAuto']; 
   		List <Lead> leads = new List<Lead>();
      	for( Integer i = 0; i < 10; i++ ) {
         	Lead l = new Lead( 
         					FirstName = 'ApexTest20120116'
         					, LastName='testLead'+'i'
         					, OwnerId = targetUserId.Id
         					, Company = 'ApexTest20120116' 
         					); 
         	leads.add(l);
      	}
   		insert leads;
		
		Test.startTest();
			
		// Schedule the test job 		
		String sch = '0 30 19 * * ? ' + String.valueOf(Date.today().year()); //Class runs every day at 2:30 PM during the current year.
		String jobId = System.schedule(
						'LeadAssignmentCatcherTest'
						, sch
						, new LeadAssignmentCatcherSchedule()
						);
		    
		// Get the information from the CronTrigger API object 
		CronTrigger ct = [ SELECT Id, CronExpression, TimesTriggered, NextFireTime
		         		   FROM CronTrigger WHERE id = :jobId ];

		// Verify the job has not run 
		System.assertEquals(0, ct.TimesTriggered);
		
		// Verify the next time the job will run 
		String nextrun = String.valueOf(Date.today().year()) + '-'
		   					+ String.valueOf(Date.today().month()) + '-'
		   					+ String.valueOf(Date.today().day())
		   					+ ' 19:30:00';
		//System.assertEquals( nextrun, String.valueOf( ct.NextFireTime ) );
		System.assert( null != ct.NextFireTime );
		
		Test.stopTest();

	}
}