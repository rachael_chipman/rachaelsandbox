@isTest(SeeAllData=True)
public class producEntryTest {

	/*static Account a;
	static Account partner;
	static Opportunity o;
	static PageReference p;
	static ApexPages.standardController std;
	static productEntryController ext;
	static string selected;

	static void init() {
		a = new Account (name = 'test account');
		insert a;

		o = new Opportunity(name = 'test opp', closedate = date.today(), accountid = a.id, stagename = 'Defining Needs', currencyisocode = 'USD');
		insert o;

		p = Page.productEntry;
		p.getParameters().put('id',o.id);
		test.setCurrentPage(p);

		std = new ApexPages.StandardController(o);
		ext = new productEntryController(std);
	}

	static testMethod void test1() {
		init();

		test.startTest();

		ext.addProd();
		ext.editAccess = true;
		ext.family = 'Certification';
		ext.getTable();

		for (productEntryController.prodChoice pc : ext.prodChoices) {
			if(pc.prodCode == '004-CERT-FEE0') ext.toSelect = pc.pb.id;
		}

		ext.addtoShoppingCart();

		test.stopTest();

	}

	static testMethod void test2() {

		init();

		test.startTest();

		ext.family = 'Professional Services';
		ext.getTable();

		for(productEntryController.prodChoice pc : ext.prodChoices) {
			if(pc.prodCode == '008-PROF-GENC') {
				pc.descrip = 'Deliverability & Technology Consulting: Deliverability Report Card';
				break;
			}
		}

		ext.priceProServ();

		for (productEntryController.prodChoice pc : ext.prodChoices) {
			if(pc.prodCode == '008-PROF-GENC') ext.toSelect = pc.pb.id;
		}

		ext.addToShoppingCart();

		test.stopTest();

		//system.assertEquals(1,ext.shoppingCart.size());

	}

	static testMethod void test3() {
		init();

		ext.getProdFamilies();
		ext.getAMServices();

		ext.quickUpdateOpp();

		ext.editAccess = true;
		ext.addProd();
		ext.family = 'Inbox Measurement';
		ext.getTable();

		for(productEntryController.prodChoice pc : ext.prodChoices) {
			if(pc.prodCode == '027-DMSU-IBM') {
				ext.toSelect = pc.pb.id;
			}
		}

		ext.addToShoppingCart();

		test.startTest();

		for(productEntryController.cartItem item : ext.shoppingCart) {
			if(item.oli.product_code__c == '027-DMSU-IBM') {
				ext.toEdit = item.oli.id;
				ext.getOli();
				ext.editOli();
				ext.setOli();
				ext.toUnSelect = ext.toEdit;
				ext.removeFromShoppingCart();
				break;
			}
		}

		ext.onSave();

		ext.onCancel();

		test.stopTest();
	}

	static testMethod void test4() {
		init();
		ext.editAccess = true;

		test.startTest();

		ext.family = 'Deliverability Tools';
		ext.o.Number_of_Platinum_Contacts__c = 2;
		ext.getTable();

		for (productEntryController.prodChoice pc : ext.prodChoices) {
			if(pc.prodCode == '002-DMSU-PLAT') {
				ext.toSelect = pc.pb.id;
				ext.addToShoppingCart();
				break;
			}
		}

		test.stopTest();
	}

	static testMethod void test5() {
		init();

		test.startTest();
		ext.getQuickServ();
		ext.seeMore();
		ext.getServices();
		ext.onSave();
		test.stopTest();
	}
     static testMethod void createCustomSetting()
    {
    	init();
    	User otherUser = TestUtils.getTestUser('Joo','Standard User');
    	String productFamilyAvailableToCurrentUser = 'testProductFamily3';
    	System.runAs(TestUtils.ADMIN_USER)
        {
    		insert otherUser;
        }
    	
        List<Product_Families__c> allProductFamilies = new List<Product_Families__c>();
        allProductFamilies.add(TestUtils.createProductyFamiliesCustomSetting('testProductFamily1',false));
        allProductFamilies.add(TestUtils.createProductyFamiliesCustomSetting('testProductFamily2',false));
        
        List<Special_Product_Family__c> allSpecialProductFamilies = new List<Special_Product_Family__c>();
        allSpecialProductFamilies.add(TestUtils.createSpecialProductFamilyCustomSetting(UserInfo.getUserID(), productFamilyAvailableToCurrentUser,false));
        allSpecialProductFamilies.add(TestUtils.createSpecialProductFamilyCustomSetting(otherUser.ID, 'testProductFamily4',false));
        
        Set<String> expectedProductFamilies = new Set<String>();
        expectedProductFamilies.addAll(Pluck.strings('Name', allProductFamilies));
		expectedProductFamilies.add(productFamilyAvailableToCurrentUser);
		
		testCustomSetting(allProductFamilies,allSpecialProductFamilies,expectedProductFamilies,'We expect to get every option in the product family custom setting and then the options in the special product custom setting for the specific user');
    }
    
    static testMethod void createCustomSetting_NoListCustomSettings()
    {
    	init();
    	User otherUser = TestUtils.getTestUser('Joo','Standard User');
    	String productFamilyAvailableToCurrentUser = 'testProductFamily3';
    	System.runAs(TestUtils.ADMIN_USER)
        {
    		insert otherUser;
        }
  
        List<Special_Product_Family__c> allSpecialProductFamilies = new List<Special_Product_Family__c>();
        allSpecialProductFamilies.add(TestUtils.createSpecialProductFamilyCustomSetting(UserInfo.getUserID(), productFamilyAvailableToCurrentUser,false));
        allSpecialProductFamilies.add(TestUtils.createSpecialProductFamilyCustomSetting(otherUser.ID, 'testProductFamily4',false));
        
        Set<String> expectedProductFamilies = new Set<String>();
		expectedProductFamilies.add(productFamilyAvailableToCurrentUser);
		
		testCustomSetting(null,allSpecialProductFamilies,expectedProductFamilies,'We expect to get the option in the special product custom setting for the specific user.');
    }
    
    static testMethod void createCustomSetting_NoSpecialProductFamilyForUser()
    {
    	init();
    	User otherUser = TestUtils.getTestUser('Joo','Standard User');
    	
    	System.runAs(TestUtils.ADMIN_USER)
        {
    		insert otherUser;
        }
    	
        List<Product_Families__c> allProductFamilies = new List<Product_Families__c>();
        allProductFamilies.add(TestUtils.createProductyFamiliesCustomSetting('testProductFamily1',false));
        
        List<Special_Product_Family__c> allSpecialProductFamilies = new List<Special_Product_Family__c>();
        allSpecialProductFamilies.add(TestUtils.createSpecialProductFamilyCustomSetting(otherUser.ID, 'testProductFamily4',false));
        
        Set<String> expectedProductFamilies = new Set<String>();
        expectedProductFamilies.addAll(Pluck.strings('Name', allProductFamilies));
		testCustomSetting(allProductFamilies,allSpecialProductFamilies,expectedProductFamilies,'We expect to get every option in the product family list custom setting, and none from the special product family heirarchy custom setting as there is not a value defined for the current user / profile');
    }
    
    static testMethod void createCustomSetting_MultipleValuesInHeirarchy_TrailingSemicolon()
    {
    	init();
    	User otherUser = TestUtils.getTestUser('Joo','Standard User');
    	Set<String> productFamilyAvailableSet = new Set<String> {'testProductFamily2', 'testProductFamily3','testProductFamily5', 'testProductFamily7'};
    	String productFamilyAvailable  = String.join(new List<String>(productFamilyAvailableSet),';')+';';
    	
    	System.runAs(TestUtils.ADMIN_USER)
        {
    		insert otherUser;
        }
    	
        List<Product_Families__c> allProductFamilies = new List<Product_Families__c>();
        allProductFamilies.add(TestUtils.createProductyFamiliesCustomSetting('testProductFamily1',false));
        allProductFamilies.add(TestUtils.createProductyFamiliesCustomSetting('testProductFamily2',false));
        
        List<Special_Product_Family__c> allSpecialProductFamilies = new List<Special_Product_Family__c>();
        allSpecialProductFamilies.add(TestUtils.createSpecialProductFamilyCustomSetting(UserInfo.getUserID(), productFamilyAvailable,false));
        allSpecialProductFamilies.add(TestUtils.createSpecialProductFamilyCustomSetting(otherUser.ID, 'testProductFamily4;testProductFamily7',false));
        
        Set<String> expectedProductFamilies = new Set<String>();
        expectedProductFamilies.addAll(Pluck.strings('Name', allProductFamilies));
		expectedProductFamilies.addAll(productFamilyAvailableSet);

		testCustomSetting(allProductFamilies,allSpecialProductFamilies,expectedProductFamilies,'We expect to get every option in the product family custom setting and then the options in the special product custom setting for the specific user');
    }
    
    static void testCustomSetting(List<Product_Families__c> productFamilies, List<Special_Product_Family__c> specialProductFamilies, Set<String> expectedProductFamilies, String message)
    {
    	System.runAs(TestUtils.ADMIN_USER)
        {
            delete Product_Families__c.getAll().values();
            if(productFamilies!=null) insert productFamilies;
            
            delete [SELECT ID FROM Special_Product_Family__c];
            if(specialProductFamilies!=null) insert specialProductFamilies;
        }

        Test.startTest();
			List<String> actualProductFamilies = ext.productFamilies;
		Test.stopTest();
		    
		System.assertEquals(expectedProductFamilies.size(),actualProductFamilies.size(),message);		
		System.assertEquals(expectedProductFamilies, new Set<String> (actualProductFamilies), message);
    }
    
    static testMethod void createCustomSetting_MultipleValuesInHeirarchy()
    {
    	init();
    	User otherUser = TestUtils.getTestUser('Joo','Standard User');
    	Set<String> productFamilyAvailableSet = new Set<String> {'testProductFamily3','testProductFamily5', 'testProductFamily7'};
    	String productFamilyAvailable = String.join(new List<String>(productFamilyAvailableSet),';');
    	
    	System.runAs(TestUtils.ADMIN_USER)
        {
    		insert otherUser;
        }
    	
        List<Product_Families__c> allProductFamilies = new List<Product_Families__c>();
        allProductFamilies.add(TestUtils.createProductyFamiliesCustomSetting('testProductFamily1',false));
        
        List<Special_Product_Family__c> allSpecialProductFamilies = new List<Special_Product_Family__c>();
        allSpecialProductFamilies.add(TestUtils.createSpecialProductFamilyCustomSetting(UserInfo.getUserID(), productFamilyAvailable,false));
        allSpecialProductFamilies.add(TestUtils.createSpecialProductFamilyCustomSetting(otherUser.ID, 'testProductFamily4;testProductFamily7',false));
        
        Set<String> expectedProductFamilies = new Set<String>();
        expectedProductFamilies.addAll(Pluck.strings('Name', allProductFamilies));
		expectedProductFamilies.addAll(productFamilyAvailableSet);
		
		testCustomSetting(allProductFamilies,allSpecialProductFamilies,expectedProductFamilies,'We expect to get every option in the product family custom setting and then the options in the special product custom setting for the specific user');
    }*/
}