@ isTest (SeeAllData = true)
Public class Campaign_Member_to_Cert_App_Test{

    public static testMethod void testCampaign_Member_to_Cert_App1(){
    
    User user = new user(Alias = 'standt', Email='testCM2CA@rp.com', 
        EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
        LocaleSidKey='en_US', ProfileId = '00e0000000734LT', 
        TimeZoneSidKey='America/Los_Angeles', UserName='testCM2CA@rp.com.rp');
            insert user;
            
    Lead L = new Lead();
    L.OwnerId = user.Id;
    L.FirstName = 'Test';
    L.LastName='Lead';
    L.Company = 'RPTEST';
    L.Street='123 S St';
    L.City='Denver';
    L.State='CO';
    L.PostalCode='80221';
    L.Fax='3033333333';
    L.Website='testrpath.com';
    L.SSC_Volume__c = '50,000 to 250,000';
    L.Billing_Address__c = '123 Fake St.';
    L.Billing_Address_2__c = 'Apt 1';
    L.Billing_City__c = 'New York';
    L.Billing_Country__c = 'USA';
    L.Billing_Email_Address__c = 'fake@fake.com';
    L.Billing_First_Name__c = 'John';
    L.Billing_Last_Name__c = 'Doe';
    L.Billing_State__c = 'NY';
    L.Billing_Zip__c = '10001';
        
    insert L;
    
    CampaignMember cm = new CampaignMember();
    cm.CampaignId = '701000000004fLn';
    cm.Certification_Application_Created__c = false;
    cm.LeadId = L.Id;
    
    insert cm;

    
    }
    
 public static testMethod void testCampaign_Member_to_Cert_App2(){
    
    User user = new user(Alias = 'standt', Email='tes@rp.com', 
      EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
      LocaleSidKey='en_US', ProfileId = '00e0000000734LT', 
      TimeZoneSidKey='America/Los_Angeles', UserName='testCMtCAT@rp.com.rp');
insert user;


Account a = new Account(
        OwnerId = user.Id,
        Name = 'Test Account',
        phone = '(303) 555-5555',
        CRM__c = user.Id);
        insert a;        
    
    
    Promo_Code__c pc = New Promo_Code__c();
    pc.Name = 'PC1234';
    pc.Account__c = a.Id;
    pc.Promo_Type__c = 'Referral';
    insert pc;
    
    
    
    Lead L = new Lead();
    L.OwnerId = user.Id;
    L.FirstName = 'Test';
    L.LastName='Lead';
    L.Company = 'RPTEST';
    L.Street='123 S St';
    L.City='Denver';
    L.State='CO';
    L.PostalCode='80221';
    L.Fax='3033333333';
    L.Website='testrpath.com';
    L.SSC_Volume__c = '50,000 to 250,000';
    L.SSC_Promo_Code__c = 'PC1234';
    L.Billing_Address__c = '123 Fake St.';
    L.Billing_Address_2__c = 'Apt 1';
    L.Billing_City__c = 'New York';
    L.Billing_Country__c = 'USA';
    L.Billing_Email_Address__c = 'fake@fake.com';
    L.Billing_First_Name__c = 'John';
    L.Billing_Last_Name__c = 'Doe';
    L.Billing_State__c = 'NY';
    L.Billing_Zip__c = '10001';
    insert L;

       
  Campaign c = new Campaign();
    c.Name = 'Test Campaign';
    c.StartDate = system.today() - 30;
    c.EndDate = system.today() + 30;
    insert c;
    
    CampaignMember cm = new CampaignMember();
    cm.CampaignId = '701000000004fLn';
    cm.Certification_Application_Created__c = false;
    cm.LeadId = L.Id;
    insert cm;

    
    }    
    
    
    
    }