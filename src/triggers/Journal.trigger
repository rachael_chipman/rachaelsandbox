trigger Journal on c2g__codaJournal__c ( before insert ,after update, after insert ) 
{
	
	if (Trigger.isBefore && Trigger.isInsert){
		JournalSalesLineItemService.populateSalesInvoiceLineItems(Trigger.new);
	}
	
	if( trigger.isAfter && trigger.isUpdate )
	{
		List<c2g__codaJournal__c> postedJournals = TransactionServices.filterPostedSObjects( trigger.new, trigger.oldMap, c2g__codaJournal__c.c2g__JournalStatus__c, c2g__codaJournal__c.Opportunity__c );
		if( !postedJournals.isEmpty() )
		{
			List<c2g__codaTransaction__c> relatedTransactions = TransactionServices.populateOpportunity( postedJournals, c2g__codaJournal__c.Opportunity__c );
			if( !relatedTransactions.isEmpty() )
			{
				TransactionServices.processTransactionsAndLineItems( relatedTransactions, trigger.new );
			}
		}
	}

	if( trigger.isAfter && trigger.isInsert )
	{
		List<c2g__codaJournal__c> filteredJournals = JournalServices.filterBasedOnOpportunity( trigger.new, trigger.oldMap );
		if( !filteredJournals.isEmpty() )
		{
			List<c2g__codaTransaction__c> relatedTransactions = TransactionServices.populateOpportunity( filteredJournals, c2g__codaJournal__c.Opportunity__c );
			if( !relatedTransactions.isEmpty() )
			{
				TransactionServices.processTransactionsAndLineItems( relatedTransactions, trigger.new );
			}
		}
	}

}