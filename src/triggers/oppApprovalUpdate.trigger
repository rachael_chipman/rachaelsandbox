trigger oppApprovalUpdate on Contract (after update, after insert) {

	List<opportunity> oppUpdate = new list<opportunity>();

	for(contract c : [Select Legal_Stage__c, Opportunity__c, Opportunity__r.Approval_Type__c from Contract where id IN :trigger.new]) {
		if(c.legal_stage__c == 'No review needed' || c.legal_stage__c == 'Approved by Legal') {
			c.opportunity__r.approval_type__c = 'Finance Approval';
			oppUpdate.add(c.opportunity__r);
		}
	}

	try { update oppUpdate; }
	catch(DMLexception e) 
	{ 
		system.debug('error! ' + e); 
		for(Opportunity opp : oppUpdate)
		{
			opp.addError('The approval type was not updated:'+ e.getMessage());
		}
	}
	
}