trigger RefCRMAssignmentLead on Lead (before update) {

/*DESCRIPTION: This trigger will assign the Referring CRM and the Referring Partner to a lead
based on the Promo Code that gets updated on the lead by Eloqua via the General Referral web form*/

  for(Lead l : trigger.new){
  
  if(l.Campaign_Name__c != null){
  
  //Set string variable for Most Recent Campaign
  String str = l.Campaign_Name__c;
  //Set variable for owner change and auto submit
  Lead prev = trigger.oldMap.get(l.Id);
  
        //Only pull Leads that have a Gen Ref Most Recent Campaign 
        if(SObjectTriggerServices.isValidCampaign( str ) && l.Gen_Ref_Lead__c == false){
        
        
        
            //If the Promo Code field is not blank get the CRM and Account from the Promo Code --> Otherwise assign Peter M. as the Referring CRM
            if(l.SSC_Promo_Code__c != null){
            
            
            List<Promo_Code__c> pc = [select Id, Name, Promo_Type__c, Account__c from Promo_Code__c where Name = :l.SSC_Promo_Code__c Limit 1];
       
           if(pc.size() < 1){
               l.Referring_CRM__c = '005000000074QZs';
               l.OwnerId = '005000000074QZs';
               

}
           else{
                for(Promo_Code__c pc2 : pc){
                    if(pc2.Account__c == null){l.Referring_CRM2__c = '005000000074QZs'; l.OwnerId = '005000000074QZs';                       
                       }
                    if(pc2.Account__c != null){
        
                try{Account pcacct = [select Id, CRM__c from Account where Id = :pc2.Account__c and CRM__c != null];
            
                l.Referring_Partner__c = pc2.Account__c;             
                l.Referring_CRM2__c = pcacct.CRM__c;
                l.OwnerId = pcacct.CRM__c;

    
                }
                catch(Exception e){}
                }}}}
    
            if(l.SSC_Promo_Code__c == null){
    
            l.Referring_CRM2__c = '005000000074QZs';
            l.OwnerId = '005000000074QZs';
      }      
    }
    
    

}
}
}