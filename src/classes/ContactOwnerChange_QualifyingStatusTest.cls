@isTest
private class ContactOwnerChange_QualifyingStatusTest {
    
    static Account testAccount;
    static Contact testContact;
    
    static void setUpMethod()
    {
        testAccount = TestUtils.createAccount(TestUtils.MARKETER_RECORD_TYPE.Id, true);
        testContact = TestUtils.createContacts('Rachael', 'Chipman', testAccount.Id, false);
        testContact.Qualifying_Status__c =  'SDR Qualified';
        testContact.Original_Marketing_Router__c = TestUtils.ADMIN_USER.Id;
        testContact.For_RSE__c = TestUtils.SALES_USER.Id;
        insert testContact;
    }
    
    static testMethod void testUpdateSingleContact_Accept()
    {
        setUpMethod();
        
        testContact.Qualifying_Status__c = 'RSE Accept';
        
        test.startTest();
        update testContact;
        test.stopTest();
        
        list<Qualifying_Status_History__c> historyRecords = [SELECT Id, Qualifying_Status__c, Previous_Qualifying_Status__c FROM Qualifying_Status_History__c WHERE Qualifying_Status__c = :testContact.Qualifying_Status__c];
        system.assertEquals(1, historyRecords.size(), 'We only expect 1 record to be created');
        system.assertEquals('SDR Qualified', historyRecords[0].Previous_Qualifying_Status__c, 'We expect the previous qual status to be recorded');
        system.assertEquals('RSE Accept', historyRecords[0].Qualifying_Status__c, 'We expect the currect qual status to be recorded');
        list<Contact> updatedContacts = [SELECT Id, OwnerId FROM Contact];
        system.assertEquals(TestUtils.SALES_USER.Id, updatedContacts[0].OwnerId, 'We expect the RSE to be the owner of the contact');
    }
    
    static testmethod void testNegativeSingleContact_Disqualify()
    {
        setUpMethod();
        testContact.Qualifying_Status__c = 'RSE Accept';
        //testContact.OwnerId = null;
        test.startTest();
        update testContact;
        test.stopTest();
    }
    
    
    static testMethod void testUpdateSingleContact_Disqualify()
    {
        setUpMethod();
        
        testContact.Qualifying_Status__c = 'RSE DQ';
        
        test.startTest();
        update testContact;
        test.stopTest();
        
        list<Qualifying_Status_History__c> historyRecords = [SELECT Id, Qualifying_Status__c, Previous_Qualifying_Status__c FROM Qualifying_Status_History__c WHERE Qualifying_Status__c = :testContact.Qualifying_Status__c];
        system.assertEquals(1, historyRecords.size(), 'We only expect 1 record to be created');
        system.assertEquals('SDR Qualified', historyRecords[0].Previous_Qualifying_Status__c, 'We expect the previous qual status to be recorded');
        system.assertEquals('RSE DQ', historyRecords[0].Qualifying_Status__c, 'We expect the currect qual status to be recorded');
        list<Contact> updatedContacts = [SELECT Id, OwnerId FROM Contact];
        system.assertEquals(TestUtils.ADMIN_USER.Id, updatedContacts[0].OwnerId, 'We expect the Original Marketing Router to be the owner of the contact');
    }
    
    static testMethod void testBulkContact_Update()
    {
        Account testAccount2 = TestUtils.createAccount(TestUtils.MARKETER_RECORD_TYPE.Id, true);
        list<Contact> testContacts = TestUtils.createContactsMultiple(201, 'Rachael', 'Chipman', testAccount2.Id, false);
        for(Contact testContact2 : testContacts)
        {
            testContact2.Qualifying_Status__c =  'SDR Qualified';
        	testContact2.Original_Marketing_Router__c = TestUtils.ADMIN_USER.Id;
        	testContact2.For_RSE__c = TestUtils.SALES_USER.Id;
        }
        insert testContacts;
        
        for(Contact testContact2 : testContacts)
        {
            testContact2.Qualifying_Status__c = 'RSE Accept';
        }
        test.startTest();
        update testContacts;
        test.stopTest();
        
        list<Qualifying_Status_History__c> historyRecords = [SELECT Id, Qualifying_Status__c, Previous_Qualifying_Status__c FROM Qualifying_Status_History__c WHERE Qualifying_Status__c = :testContacts[0].Qualifying_Status__c];
        system.assertEquals(201, historyRecords.size(), 'We only expect 201 records to be created');
        for(Qualifying_Status_History__c history : historyRecords)
        {
            system.assertEquals('SDR Qualified', history.Previous_Qualifying_Status__c, 'We expect the previous qual status to be recorded');
        	system.assertEquals('RSE Accept', history.Qualifying_Status__c, 'We expect the currect qual status to be recorded');
        }
        list<Contact> updatedContacts = [SELECT Id, OwnerId FROM Contact];
        for(Contact updatedContact: updatedContacts)
        {
            system.assertEquals(TestUtils.SALES_USER.Id, updatedContact.OwnerId, 'We expect the RSE to be the owner of the contact');
        }
    }
    

}