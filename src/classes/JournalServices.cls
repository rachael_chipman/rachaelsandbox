public with sharing class JournalServices 
{
	public static List<c2g__codaJournal__c> filterBasedOnOpportunity( List<c2g__codaJournal__c> newList, Map<Id, c2g__codaJournal__c> oldMap )
	{
		return Select.Field.notNull( c2g__codaJournal__c.Opportunity__c  ).andx( Select.Field.notNull( c2g__codaJournal__c.c2g__Transaction__c ) ).filter( newList, oldMap );
	}
}