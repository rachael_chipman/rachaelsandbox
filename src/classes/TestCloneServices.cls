@istest
private class TestCloneServices {
        
    static final String ACCOUNT_NAME = 'Test Account';
    static final String ACCOUNT_INDUSTRY = 'Transportation';
    static final String SHIPPING_CITY = 'Walla Walla';
    static final String NEW_SHIPPING_CITY = 'Seattle';
    static final String STAGE_NAME = 'Old Stage';
    static final String NEW_STAGE_NAME = 'New Stage';
    static final String TYPE_NAME = 'Old Type';
    static final String NEW_TYPE_NAME = 'New Type';
    
    static testMethod void testClone() {
    	
        final Integer NUM_OF_OPPORTUNITIES = 2;
        
        Map<String, object> excludedParentsFieldToNewValue = new Map<String, object>{ 'ShippingCity' => NEW_SHIPPING_CITY, 'RecordTypeId' => TestUtils.PARTNER_RECORD_TYPE.Id };
        Map<String, object> excludedChildFieldToNewValue = new Map<String, object>{ 'StageName' => NEW_STAGE_NAME, 'Type' => NEW_TYPE_NAME };
                
        //Create account test data
        Account testAccount = TestUtils.createAccount(TestUtils.MARKETER_RECORD_TYPE.Id, false);
        testAccount.Name = ACCOUNT_NAME;
        testAccount.Industry = ACCOUNT_INDUSTRY;
        testAccount.ShippingCity = SHIPPING_CITY;
        insert testAccount;
        
        //Create opportunity test data
        List<Opportunity> testOpportunities = TestUtils.createOpportunities( NUM_OF_OPPORTUNITIES, TestUtils.RESEARCH_RECORD_TYPE.Id, testAccount.Id, false );
        for( Integer i = 0; i < testOpportunities.size(); i++ )
        {
        	//Using same naming convention as account
        	Integer append = i + 1;
        	testOpportunities[i].Name = ACCOUNT_NAME + append;
        	testOpportunities[i].StageName = STAGE_NAME;
        	testOpportunities[i].Type = TYPE_NAME;
        	
        }
        
        insert testOpportunities;
        
        Test.startTest();
        	CloneServices.startsClone( new Set<Id> {testAccount.Id}, 'Account', new Map<String, String> {'Opportunity' => 'AccountId'}, excludedParentsFieldToNewValue, excludedChildFieldToNewValue );
        Test.stopTest();
		
		//Since we cannot see all data, the only other test account in the system should be the cloned one
        Account clonedAccount = [ SELECT Id, Name, Industry, RecordtypeId, ShippingCity,
        								(SELECT Name, Type, StageName FROM Opportunities ) 
        					      FROM Account 
        						  WHERE Id <> : testAccount.Id ];
        
        //Asserting that the cloned account has the correct values
        System.assertEquals( ACCOUNT_NAME, clonedAccount.Name, 'The Name of original and clone object should be the same');
        System.assertEquals( NEW_SHIPPING_CITY, clonedAccount.ShippingCity, 'The shipping city should be set to the new value specified by the cloning service' );
        System.assertEquals( TestUtils.PARTNER_RECORD_TYPE.Id, clonedAccount.RecordTypeId, 'The record type id should be set to the new value specified by the cloning service' );
        System.assertEquals( ACCOUNT_INDUSTRY, clonedAccount.Industry, 'The Industry of original and clone object should be the same');
        
        System.assertEquals( NUM_OF_OPPORTUNITIES, clonedAccount.Opportunities.size(), 'The method should clone all original opportunities');
        //Checking that all of the cloned opportunities have the correct value
        for ( Opportunity cloneOpp : clonedAccount.Opportunities ){
        	System.assert( cloneOpp.Name.contains ( clonedAccount.Name ), 'The clone and original opportunities name should be the same' );
        	System.assertEquals( NEW_TYPE_NAME, cloneOpp.Type, 'The type should have been set to the new value specified by the cloning service' );
        	System.assertEquals( NEW_STAGE_NAME, cloneOpp.StageName, 'The type should have been set to the new value specified by the cloning service' );
        }
      	
    }
    
    static testMethod void testClone_Bulk(){
        
        final Integer NUM_OF_OPPORTUNITIES = 4;
        final Integer NUM_OF_ACCOUNT = 20;
        
        Map<String, object> excludedParentsFieldToNewValue = new Map<String, object>{ 'ShippingCity' => NEW_SHIPPING_CITY, 'RecordTypeId' => TestUtils.PARTNER_RECORD_TYPE.Id };
        Map<String, object> excludedChildFieldToNewValue = new Map<String, object>{ 'StageName' => NEW_STAGE_NAME, 'Type' => NEW_TYPE_NAME };
                
        //Create account test data
        List<Account> testAccounts = TestUtils.createAccounts(NUM_OF_ACCOUNT, TestUtils.MARKETER_RECORD_TYPE.Id, false);
        List<Opportunity> testOpportunities = new List<Opportunity>();
        
        for ( Account testAccount : testAccounts ){
        	testAccount.Name = ACCOUNT_NAME;
        	testAccount.Industry = ACCOUNT_INDUSTRY;
        	testAccount.ShippingCity = SHIPPING_CITY;
        }
        
        insert testAccounts;
      	
      	Set<Id> accIds = new Set<Id>();
      	
      	for ( Account testAccount : testAccounts ){
      		List<Opportunity> newOpps = ( TestUtils.createOpportunities(NUM_OF_OPPORTUNITIES, TestUtils.RESEARCH_RECORD_TYPE.Id, testAccount.Id, false) );
             for( Integer i = 0; i < testOpportunities.size(); i++ )
        	{
	        	//Using same naming convention as account
	        	Integer append = i + 1;
	        	testOpportunities[i].Name = ACCOUNT_NAME + append;
        	}
            testOpportunities.addAll( newOpps );
            accIds.add( testAccount.Id );
      	}
      	 
      	insert testOpportunities; 
      	 
        Test.startTest();
        	CloneServices.startsClone( accIds, 'Account', new Map<String, String> {'Opportunity' => 'AccountId'}, excludedParentsFieldToNewValue, excludedChildFieldToNewValue );
        Test.stopTest();

		//Since we cannot see all data, the only other test account in the system should be the cloned one
        List<Account> clonedAccounts = [ SELECT Id, Name, Industry, RecordtypeId, ShippingCity,
        										(SELECT Name, Type, StageName FROM Opportunities )
        								 FROM Account 
        								 WHERE Id NOT IN :accIds ];
        
        System.assertEquals ( NUM_OF_ACCOUNT, clonedAccounts.size(), 'Cloned account should be queried back' );
        
        //Asserting that the cloned accounts have the correct values	
        for ( Account acc : clonedAccounts ){
        	System.assertEquals( ACCOUNT_NAME, acc.Name, 'The Name of original and clone object should be the same');
        	System.assertEquals( ACCOUNT_INDUSTRY, acc.Industry, 'The Industry of original and clone object should be the same');
			System.assertEquals( NEW_SHIPPING_CITY, acc.ShippingCity, 'The shipping city should be set to the new value specified by the cloning service' );
        	System.assertEquals( TestUtils.PARTNER_RECORD_TYPE.Id, acc.RecordTypeId, 'The record type id should be set to the new value specified by the cloning service' );
	        
	        System.assertEquals( NUM_OF_OPPORTUNITIES, acc.Opportunities.size(), 'The method should clone all original opportunities');
	        //Checking that all of the cloned opportunities have the correct value
	        for ( Opportunity cloneOpp : acc.Opportunities ) {
	        	System.assert( cloneOpp.Name.contains ( acc.Name ), 'The clone and original opportunities name should be the same' );
	        	System.assertEquals( NEW_TYPE_NAME, cloneOpp.Type, 'The type should have been set to the new value specified by the cloning service' );
        		System.assertEquals( NEW_STAGE_NAME, cloneOpp.StageName, 'The type should have been set to the new value specified by the cloning service' );
	        }
        }
      
    }
}