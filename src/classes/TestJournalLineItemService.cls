@isTest
public with sharing class TestJournalLineItemService {
    public static Account testAccount;
    public static product2 testProduct;
    public static c2g__codaGeneralLedgerAccount__c testGeneralLedgerAccount;
    public static integer NUM_OF_OPPORTUNITIES = 10;
    public static List<c2g__codaJournal__c> testJournals = new List<c2g__codaJournal__c>();
    public static integer NUM_OF_JOURNALS = 10;
    public static List<c2g__codaJournalLineItem__c> testJournalLineItems;
        
    static void datasetup(){
        testGeneralLedgerAccount = TestUtils.createGeneralLedgerAccount(true);
        testAccount = TestUtils.createAccount_FinancialForce( TestUtils.PARTNER_RECORD_TYPE.Id, testGeneralLedgerAccount.Id, true );
        testProduct = TestUtils.createProduct_FinancialForce( testGeneralLedgerAccount.Id, true );
        PriceBookEntry testPBE = TestUtils.createPricebookEntry( testProduct.Id, TestUtils.STANDARD_PRICE_BOOK.Id, true );
    }
    
    @isTest(seeAllData=true)    
    static void testJournalLineItemInserts() 
    {
        datasetup();
        //List<Opportunity> testOpportunities = TestUtils.createOpportunities( NUM_OF_OPPORTUNITIES, TestUtils.AWARENESS_RECORD_TYPE.Id, testAccount.Id, true );
        List<Opportunity> testOpportunities = TestUtils.createOpportunities( NUM_OF_OPPORTUNITIES, TestUtils.RESEARCH_RECORD_TYPE.Id, testAccount.Id, true );
        testJournals = new List<c2g__codaJournal__c>();
        testJournalLineItems = new List<c2g__codaJournalLineItem__c>();
        
        for( Opportunity anOpportunity : testOpportunities )
        {

           testJournals.add( TestUtils.createJournal(new Map<String,Object>{ 'Opportunity__c' => anOpportunity.Id }, false));
        }
        insert testJournals;
        String lineType = 'General Ledger Account';
        Decimal value = 10;
        for (c2g__codaJournal__c testJournal: testJournals){
            testJournalLineItems.add( TestUtils.createJournalLineItem(testjournal.Id, lineType, testGeneralLedgerAccount.id, value, false) );		
        }

        test.startTest();
            insert testJournalLineItems;
        test.stopTest();

        List<c2g__codaJournal__c> insertedJournals = [ SELECT id, c2g__IncomeScheduleGroup__c, Opportunity__c FROM c2g__codaJournal__c WHERE Id IN :testJournals ];
        List<c2g__codaJournalLineItem__c> insertedJournalLineItems = [ SELECT id, Opportunity__c,c2g__Journal__c FROM c2g__codaJournalLineItem__c WHERE c2g__Journal__c IN :testJournals ]; 
        Map<ID,List<c2g__codaJournalLineItem__c>> journalJLIMap = new Map<ID,List<c2g__codaJournalLineItem__c>>();
        for (c2g__codaJournalLineItem__c jli :insertedJournalLineItems){
            if(!journalJLIMap.containsKey(jli.c2g__Journal__c)){
                journalJLIMap.put(jli.c2g__Journal__c,new List<c2g__codaJournalLineItem__c>());
            }
			journalJLIMap.get(jli.c2g__Journal__c).add(jli);
        }
        System.assertEquals(NUM_OF_JOURNALS,insertedJournals.size(),'All journals have been retrieved');
        System.assertEquals(NUM_OF_JOURNALS,insertedJournalLineItems.size(),'All journal line items have been retrieved');
        
        for( c2g__codaJournal__c insertedJournal: insertedJournals )
        {
            for (c2g__codaJournalLineItem__c jli : journalJLIMap.get(insertedJournal.id)){
            	System.assertNotEquals( null,jli.Opportunity__c, 'The journal opportunity is populated' );           	
            	System.assertEquals( insertedJournal.Opportunity__c, jli.Opportunity__c, 'The journal line item is populated with opportunity lookup from the journal' );
            }
        }

    }
    
    @isTest(seeAllData=true)    
    static void testJournalLineItemInserts_negativeTest() 
    {
        datasetup();
        //List<Opportunity> testOpportunities = TestUtils.createOpportunities( NUM_OF_OPPORTUNITIES, TestUtils.AWARENESS_RECORD_TYPE.Id, testAccount.Id, true );
        List<Opportunity> testOpportunities = TestUtils.createOpportunities( NUM_OF_OPPORTUNITIES, TestUtils.RESEARCH_RECORD_TYPE.Id, testAccount.Id, true );
        testJournals = new List<c2g__codaJournal__c>();
        testJournalLineItems = new List<c2g__codaJournalLineItem__c>();
        
        for( Opportunity anOpportunity : testOpportunities )
        {
           testJournals.add( TestUtils.createJournal(new Map<String,Object>{ }, false));
        }
        insert testJournals;
        String lineType = 'General Ledger Account';
        Decimal value = 10;
        for (c2g__codaJournal__c testJournal: testJournals){
            testJournalLineItems.add( TestUtils.createJournalLineItem(testjournal.Id, lineType, testGeneralLedgerAccount.id, value, false) );		
        }

        test.startTest();
            insert testJournalLineItems;
        test.stopTest();

        List<c2g__codaJournal__c> insertedJournals = [ SELECT id, c2g__IncomeScheduleGroup__c, Opportunity__c FROM c2g__codaJournal__c WHERE Id IN :testJournals ];
        List<c2g__codaJournalLineItem__c> insertedJournalLineItems = [ SELECT id, Opportunity__c,c2g__Journal__c FROM c2g__codaJournalLineItem__c WHERE c2g__Journal__c IN :testJournals ]; 
        System.assertEquals(NUM_OF_JOURNALS,insertedJournals.size(),'All journals have been retrieved');
        System.assertEquals(NUM_OF_JOURNALS,insertedJournalLineItems.size(),'All journal line items have been retrieved');
        
        for( c2g__codaJournalLineItem__c insertedJournalLineItem: insertedJournalLineItems )
        {
			System.assertEquals( null,insertedJournalLineItem.Opportunity__c, 'The journal opportunity is not populated' );           	
        }

    }
}