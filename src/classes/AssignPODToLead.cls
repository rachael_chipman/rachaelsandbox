public class AssignPODToLead {
   
    public static void ProcessPODAssignment(List<Lead> newleads)
    {
        if(!newleads.isEmpty())
        {
            for(Lead tlead: newleads)
            {
                if(tlead.Country == null && tlead.State == null)
                    tlead.Pod_Number__c = 'None';
                else
                  assignPODNumber(tlead);
            }
        }
    }
    
    private static void assignPODNumber(Lead qlead)
    {
        string podnumber = 'None';
        Map<String,POD_Values_By_Region__c> PodMapValues = POD_Values_By_Region__c.getAll();
        List<POD_Values_By_Region__c> PodValues = PodMapValues.values();
        boolean isPodassigned = false;
        
                
        if(PodValues!= null)
        {
           if(qlead.Country != null  && qlead.Identity__c == 'Channel' && !isPodassigned) 
           {
               String leadCountry = qlead.Country.tolowercase();
        		
               for(POD_Values_By_Region__c  tpod : PodValues)
               {
                   if(tpod.Country__c == null)
                       continue;
                   if(tpod.Country__c.tolowercase().contains(leadCountry) && tpod.Channel__c == true)
                   {
                       qlead.Pod_Number__c = tpod.POD__c;
                       isPodassigned = true;
                       return ;
                   }
                   
               }
           }
          if(qlead.Country!= null && qlead.State!= null && !isPodassigned)
            {
                String leadCountry = qlead.Country.tolowercase();
        		String leadState = qlead.State.tolowercase();
                
               for(POD_Values_By_Region__c  tpod : PodValues)
               { 
                   
                  if(tpod.State_Name__c == null || tpod.Country__c == null)
                    continue;
                   if(tpod.Country__c.tolowercase().contains(leadCountry) && tpod.State_Name__c.tolowercase().contains(leadState) && tpod.Channel__c!= true  &&  qlead.Identity__c != 'Channel' )
                    {
                          qlead.Pod_Number__c = tpod.POD__c;
                          isPodassigned = true;
                          return ;
                    }
                   else if(tpod.Country__c.tolowercase().contains(leadCountry) && tpod.State_Name__c.tolowercase().contains(leadState) && tpod.Channel__c == true  &&  qlead.Identity__c == 'Channel')
                   {
                        qlead.Pod_Number__c = tpod.POD__c;
                        isPodassigned = true;
                         return ;
                   }
                   
                }
             }
           if(qlead.Country!= null && !isPodassigned)
            {
                String leadCountry = qlead.Country.tolowercase();
               for(POD_Values_By_Region__c  tpod : PodValues)
               {
                   if(tpod.Country__c == null)
                       continue;
                  if(tpod.Country__c.tolowercase().contains(leadCountry) && (tpod.State_Name__c == null || tpod.State_Name__c == '') && tpod.Channel__c!= true  &&  qlead.Identity__c != 'Channel')
                  {
                      qlead.Pod_Number__c  = tpod.POD__c;
                      isPodassigned = true;
                      return ;
                  }
                  else if(tpod.Country__c.tolowercase().contains(leadCountry)  && (tpod.State_Name__c == null || tpod.State_Name__c == '') && tpod.Channel__c == true  &&  qlead.Identity__c == 'Channel' )
                  {
                      qlead.Pod_Number__c  = tpod.POD__c;
                      isPodassigned = true;
                      return ;
                  }
                  else
                  {
                     qlead.Pod_Number__c = podnumber;
                      isPodassigned = true;
                  }
               }
            }
            
        }
   }
  
}