trigger CertificationApplication on Certification_Application__c (before insert, before update) {
	
	if( trigger.isBefore && ( trigger.isInsert || trigger.isUpdate ) )
	{
		FindCreateCertOpp.processTrigger(trigger.new, trigger.oldMap);
	}

}