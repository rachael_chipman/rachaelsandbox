/* Written by Adam Bryant, April 2012 for Return Path
*
* This trigger identifies any leads and/or contacts that have been added to a 2nd (or 3rd, 4th etc) campaign.
* It then sends an email to the appropriate party to notify them of such.
*
* Note that this trigger makes use of a utility class, campEmailUtil, to facilitate the sending of emails and
* avoid Salesforce's governor limit of 10 email invocations.
*
* [CHANGELOG]
*
* [AJB] May 15 2012: added functionality to remove any pending approval request for a lead if it is added to WEB:SSC campaign 
* [AJB] April 25 2012: removed adam.bryant@returnpath.net from bcc address
* 
*
*/

trigger campaign_name_push on CampaignMember (after insert) {
    
    // these will collect leads/accounts to later push campaign names into
    
    List<Lead> Leads = new List<Lead>();
    List<Contact> Contacts = new List<Contact>();

    // loop through the inserted record(s)
    
    for (CampaignMember cm : [SELECT Id, LeadId, ContactId, CampaignId, Campaign.Name, Campaign.Type, Lead.Name, Lead.Company, 
    Lead.Status, Lead.LeadSource, Lead.Campaign_Name__c, Lead.Owner.Email, Lead.Owner.Id, Contact.Name, Contact.Account.Name, 
    Contact.Account.Type, Contact.Campaign_Name__c, Contact.Account.AccountManager__c, Contact.Account.AM__r.Email, 
    Contact.Account.Customer_Relationship_Manager_Primary__c, Contact.Account.CRM__r.Email, Contact.Account.Owner.Email 
        FROM CampaignMember WHERE Id IN :Trigger.newMap.keySet()]) {            
        
    // find leads whose campaign membership is 2+
    if (cm.leadid != null && cm.Lead.Campaign_Name__c != cm.Campaign.Name) {
                
        if (cm.Lead.Campaign_Name__c != null) {
            
            system.debug('Updating an additional campaign membership for ' + cm.Lead.Id + '- ' + cm.Lead.Name);

            // set up our email notification, unless eloqua owns this lead, then update the most recent campaign field
            if (cm.Lead.Owner.Id != '00500000006olmF') { 
                Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                mail.setSenderDisplayName('Salesforce Alerts');
                mail.setSubject('NOTICE: A contact or lead of yours has been added to an additional campaign');
                mail.setToAddresses(new String[] {cm.Lead.Owner.Email});
                mail.setccAddresses(new String[] {'chris.kingdon@returnpath.net'});
                mail.setHTMLBody(
                    '<p>A lead which you own has been added to an additional campaign.' +
                    '<p>Name: ' + cm.Lead.Name +
                    '<br/>Company: ' + cm.Lead.Company +
                    '<br/>Status: ' + cm.Lead.Status +
                    '<br/>New Campaign: ' + cm.Campaign.Name +
                    '<p>Please review the lead and follow up as appropriate: ' + 'https://ssl.salesforce.com/' + cm.LeadId
                );
                                
                // add our message to a list. we'll send them all later in a batch
                campEmailUtil.mailArray.add(mail);
                system.debug('Notification queued up for lead ' + cm.Lead.Id);
            }
            
            system.debug('Setting ' + cm.Lead.Id + ' - ' + cm.Lead.Name + ' most recent campaign to ' + cm.Campaign.Name);
            cm.Lead.Campaign_Name__c = cm.Campaign.Name;
            
            system.debug('Setting ' + cm.Lead.Id + ' - ' + cm.Lead.Name + ' most recent lead source to ' + cm.Campaign.Type);
            cm.Lead.Most_Recent_Lead_Source__c = cm.Campaign.Type;
            
            
            // if the new campaign is cert app form, and it's waiting for leadgen approval, recall that request so that it can be converted by castiron (happens within 1 minute of update)
            if (cm.CampaignId == '701000000004fLn' && cm.Lead.Status == 'Pending LG Approval') {
                
                List<ProcessInstanceWorkitem> workitem = [Select p.Id, p.ProcessInstanceId, p.ProcessInstance.TargetObjectId from ProcessInstanceWorkitem p where p.ProcessInstance.TargetObjectId = :cm.Lead.Id];
                Approval.ProcessWorkitemRequest req = new Approval.ProcessWorkitemRequest();
                req.setComments('Request removed by BIO Automation.');
                req.setAction('Removed'); 
                req.setWorkitemId(workitem[0].Id);
                Approval.ProcessResult result =  Approval.process(req);
            }
                        
        }
            
        // for brand new leads, just push the campaign name and lead source 
        if (cm.Lead.Campaign_Name__c == null) {
            system.debug(cm.Lead.Id + ' - ' + cm.Lead.Name + ' is a brand new lead. Pushing in campaign: ' + cm.Campaign.Name);
            cm.Lead.First_Campaign__c = cm.Campaign.Name;
            
            system.debug('Setting ' + cm.Lead.Id + ' - ' + cm.Lead.Name + ' most recent campaign to ' + cm.Campaign.Name);
            cm.Lead.Campaign_Name__c = cm.Campaign.Name; 
            
            system.debug('Setting ' + cm.Lead.Id + ' - ' + cm.Lead.Name + ' lead source to ' + cm.Campaign.Name);
            cm.Lead.LeadSource = cm.Campaign.Type;
            
            system.debug('Setting ' + cm.Lead.Id + ' - ' + cm.Lead.Name + ' most recent lead source to ' + cm.Campaign.Name);
            cm.Lead.Most_Recent_Lead_Source__c = cm.Campaign.Type;
        }
            
        // get our leads onto the update list
        Leads.add(cm.Lead);
                
        // find contacts whose campaign membership is 2+ and email the account's AM and/or CRM, or the owner if neither of those exist      
    } else if (cm.contactid != null && cm.Contact.Campaign_Name__c != cm.Campaign.Name) {
        
        if (cm.Contact.Campaign_Name__c != null) {
                            
            cm.Contact.Campaign_Name__c = cm.Campaign.Name;
            
            system.debug('Updating an additional campaign membership for ' + cm.Contact.Id + '- ' + cm.Contact.FirstName + cm.Contact.LastName);
                
            // figure out who the email is going to and set it up
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                
            if (cm.Contact.Account.AccountManager__c == null) {
                if (cm.Contact.Account.Customer_Relationship_Manager_Primary__c == null) {
                    mail.setToAddresses(new String[] {cm.Contact.Account.Owner.Email});
                } else { mail.setToAddresses(new String[] {cm.Contact.Account.CRM__r.Email}); }
            } else { mail.setToAddresses(new String[] {cm.Contact.Account.AM__r.Email}); }
            
            mail.setSenderDisplayName('Salesforce Alerts');
            mail.setSubject('NOTICE: A contact or lead of yours has been added to an additional campaign');
            mail.setccAddresses(new String[] {'chris.kingdon@returnpath.net'});
            mail.setHTMLBody(
                '<p>A contact under an account related to you has been added to an additional campaign.' +
                '<p>Name: ' + cm.Contact.Name +
                '<br/>Account: ' + cm.Contact.Account.Name +
                '<br/>Account Type: ' + cm.Contact.Account.Type +
                '<br/>New campaign: ' + cm.Campaign.Name +
                '<p>Please review the contact record and follow up as appropriate: ' + 'https://ssl.salesforce.com/' + cm.ContactId
                );
                            
            system.debug('Notification queued up for contact' + cm.Contact.Id);
            campEmailUtil.mailArray.add(mail);

            // just push the campaign name for contacts with first-time campaign memberships. note that this isn't the same as leads (which require campaigns on insert)
        } else { 
            system.debug('Setting contact ' + cm.Contact.Id + ' most recent campaign to ' + cm.Campaign.Name);
            cm.Contact.Campaign_Name__c = cm.Campaign.Name; }
        
        // get our contacts onto the update list
        Contacts.add(cm.Contact);

        }
    }
    
    //send our email notifications
    
    system.debug('Sending email notifications');
    try { campEmailUtil.sendCampEmail(); } 
    catch (Exception E) { system.debug('Error sending email notifications'); }

    // finally, update the records that we pushed campaign names to 
    if (Leads.size() > 0) {
        system.debug('Updating leads');
        try { update Leads; } 
        catch (Exception E) { system.debug('Campaign Name Push Trigger Error on one or more Leads'); } 
    } else {}
    
    if (Contacts.size() > 0) {
        system.debug('Updating contacts');
        try { update Contacts; } 
        catch (Exception E) { system.debug('Campaign Name Push Trigger Error on one or more Contacts'); }
    } else {}
    
}