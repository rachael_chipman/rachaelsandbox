public without sharing class OTMCommInsertService {
	
	
	public static List<Opportunity> filterNewOpps(List<Opportunity> newOpps)
	{
		List<Opportunity> filteredOpps = new List<Opportunity>();
	
		for( Opportunity o : newOpps )
		{
			filteredOpps.add(o);
		}
		return filteredOpps;
	}
	
	public static List<Opportunity> getOpportunityTeamMembers( List<Opportunity> filteredOpps )
	{
		return[SELECT Id, OwnerId, (SELECT Id, Percent_Commissioned_on__c from OpportunityTeamMembers where OpportunityId IN :filteredOpps)
		FROM Opportunity
		WHERE Id IN: filteredOpps];
	}
	
	private static Map<Id, List<OpportunityTeamMember>> OppIdToTeamMembers( List<Opportunity> filteredOpportunitiesWithOpportunityTeamMembers )
	{
		Map<Id, List<OpportunityTeamMember>> OpportunityIdtoTeamMembers = new Map<Id, List<OpportunityTeamMember>>();
		for( Opportunity o : filteredOpportunitiesWithOpportunityTeamMembers )
		{
			OpportunityIdtoTeamMembers.put( o.Id, new List<OpportunityTeamMember>() );
		
			for( OpportunityTeamMember otm : o.OpportunityTeamMembers )
			{
				OpportunityIdtoTeamMembers.get( o.Id ).add( otm );
			}
		}
		return OpportunityIdtoTeamMembers;
	}
	
	public static void processOTMs ( List<Opportunity> filteredOpportunitiesWithOpportunityTeamMembers, List<Opportunity> newOpps )
	{
		List<OpportunityTeamMember> UpdateOTMs = new List<OpportunityTeamMember>();
		Map<Id, List<OpportunityTeamMember>> OpportunityIdToTeamMembers = OppIdToTeamMembers(filteredOpportunitiesWithOpportunityTeamMembers);
		
		for( Opportunity o : filteredOpportunitiesWithOpportunityTeamMembers )
		{
			if(!OpportunityIdToTeamMembers.isEmpty() && OpportunityIdToTeamMembers.containsKey( o.Id ))
			{
				for( OpportunityTeamMember oppTeamMember : o.OpportunityTeamMembers )
				{
					oppTeamMember.Percent_Commissioned_On__c = 100;
					UpdateOTMs.add(oppTeamMember);
				}
			}
		}
		update UpdateOTMs;
	}
	
	public void processTrigger( List<Opportunity> newOpps)
	{
		List<Opportunity> filteredOpps = filterNewOpps( newOpps);
		List<Opportunity> OpportunityWithTeamMembers = getOpportunityTeamMembers( filteredOpps);
		if(!filteredOpps.isEmpty())
		{
			processOTMs( OpportunityWithTeamMembers, newOpps);	
		}
	}
	
}