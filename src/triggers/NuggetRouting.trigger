trigger NuggetRouting on Nugget__c (Before Insert, Before Update) {

for(Nugget__c n : trigger.New){

if(n.Sales_User_ID__c != null && Trigger.isInsert){
n.OwnerId = n.Sales_User_ID__c;
n.Assigned_to_Sales_Rep__c = system.Now();
n.Status__c = 'Assigned';
n.Routed_Using_ID__c = true;}

if(n.Sales_User_ID__c == null && n.Assigned_to_Pod_RD__c == null && n.Pod_Calc__c != 'none' && n.Pod_Calc__c != null && Trigger.isInsert){
Group g = [select Id from Group where Name = :n.Pod_Calc__c and Type = 'Queue'];
n.OwnerId = g.Id;
n.Assigned_to_Pod_RD__c = system.now();
n.Assigned_Pod__c = n.Pod_Calc__c;}

if(n.Sales_User_ID__c != null && n.Routed_Using_ID__c == false && Trigger.isUpdate){
n.OwnerId = n.Sales_User_ID__c;
n.Assigned_to_Sales_Rep__c = system.Now();
n.Status__c = 'Assigned';
n.Routed_Using_ID__c = true;}

if(n.Assigned_to_Sales_Rep__c == null && n.Owner_Role__c.contains('Sales Pod') && Trigger.isUpdate){
n.Assigned_to_Sales_Rep__c = system.Now();
n.Status__c = 'Assigned';
}

if(n.Returned_to_RD__c == true && n.Owner_Role__c.contains('Sales Pod') && n.Assigned_to_Sales_Rep__c != null && Trigger.isUpdate){
n.Assigned_to_Sales_Rep__c = system.Now();
n.Status__c = 'Assigned';
n.Returned_to_RD__c = false;}

if(n.Status__c == 'Closed' && n.Close_Date_Time__c == null && Trigger.isUpdate){
n.Close_Date_Time__c = system.now();
}

}
}