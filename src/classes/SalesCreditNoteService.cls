public with sharing class SalesCreditNoteService {

	public static void populateCreditNoteLineItems (List<c2g__codaCreditNoteLineItem__c> newCreditNoteLineItems){
		
		Set<id> creditNoteIds = Pluck.ids('c2g__CreditNote__c',newCreditNoteLineItems);
		Map<Id,c2g__codaCreditNote__c> creditNotesMap = new Map<ID,c2g__codaCreditNote__c>([Select c2g__Opportunity__c From c2g__codaCreditNote__c 
																	where id in :creditNoteIds ]);
											
		For(c2g__codaCreditNoteLineItem__c lineItem: newCreditNoteLineItems){
			if (lineItem.Opportunity__c == null) {
				lineItem.Opportunity__c = creditNotesMap.get(lineItem.c2g__CreditNote__c).c2g__Opportunity__c;
			}
		}
		
	}
}