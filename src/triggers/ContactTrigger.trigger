trigger ContactTrigger on Contact (before insert, before update, after insert, after update) {
    
    if(trigger.isInsert && trigger.isBefore)
    {
        Standarized_State_And_Country_Service.ProcessContactTrigger(null,trigger.new);
        System.debug('Before updating the state and country');
        //Standarized_State_And_Country_Service.ProcessContactTrigger(trigger.oldmap,trigger.new);
    }

    if(trigger.isBefore && trigger.isUpdate)
    {
        GenRefCRMOwnerChangeContact.processTrigger(trigger.new, trigger.oldMap);
        ContactOwnerChange_QualifyingStatus.processTrigger(trigger.new, trigger.oldMap);
         Standarized_State_And_Country_Service.ProcessContactTrigger(trigger.oldmap,trigger.new);
    }
    
    if(trigger.isAfter)
    {
        if(trigger.isUpdate)
        {
            //ContactOwnerChange_QualifyingStatus.processTrigger(trigger.new, trigger.oldMap);
            CreateUpdateCommunityUser.createUpdateCommunityUsers(trigger.new, trigger.oldMap);
        }
        else if(trigger.isInsert)
        {
            CreateUpdateCommunityUser.createUpdateCommunityUsers(trigger.new, trigger.oldMap);
        }
    }

}