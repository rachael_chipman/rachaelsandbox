@isTest(SeeAllData=true) 
private class AccountOwnerChangeTest {
	
	static User RM = [SELECT Id FROM User WHERE Profile.Name = 'Client Services' and isActive = true LIMIT 1];
	//005000000073rBMAAY
	static User CRM = [SELECT Id FROM User WHERE Profile.Name = 'Client Services' and isActive = true and Id != :RM.Id LIMIT 1];
	//005000000075CRsAAM
	static User RD = [SELECT Id FROM User WHERE Profile.Name = 'Onboarding & Client Support' and isActive = true LIMIT 1]; //updated from SSC Qual Team to Onboarding & Client Support
	//005000000075CTjAAM
	static User SALES = TestUtils.SALES_USER;
	//005000000073qaQAAQ
	//static Opportunity testOpportunity;
	//static Pricebook2 pb;
	//static Product2 testProduct;
	//static PricebookEntry toolsProduct;
	//static OpportunityLineItem testOli;

	private static Account testAccount_S;
	private static List<Account> testAccounts_S;
	private static Opportunity testOpportunity;
	private static Zuora__CustomerAccount__c testBillingAccount_S;
	private static Zuora__Subscription__c testSub_S;
	private static Zuora__SubscriptionProductCharge__c testSubProdCharge_S;
	private static Zuora__SubscriptionProductCharge__c testSubProdCharge2_S;
	private static Zuora__Product__c testProduct_S;
	private static zqu__Quote__c amendTestQuote;
	private static Map<String, Decimal> prodKeyToPrice = new Map<String, Decimal>();

	private static void setupExistingSubscriptions()
	{
		testAccounts_S = new List<Account>();

		testAccounts_S.add(testAccount_S = TestUtils.createAccount(TestUtils.MARKETER_RECORD_TYPE.Id, true));
		system.assert(testAccount_S != NULL);

		testOpportunity = TestUtils.createOpportunity(TestUtils.RESEARCH_RECORD_TYPE.Id, testAccount_S.Id, true);
		system.assert(testOpportunity != NULL);

		testBillingAccount_S = TestUtils.createBillingAccounts(testAccounts_S, true)[0];
		system.assert(testBillingAccount_S != NULL);

		testProduct_S = TestUtils.createZuoraProduct('Email Optimization', true);
		system.assert(testProduct_S != NULL);

		testSub_S = TestUtils.createZ_Subscription(testAccount_S.Id, testBillingAccount_S.Id, testBillingAccount_S.Id, 'Test Subscription', 'TERMED', true);
		system.assert(testSub_S != NULL);
		testSub_S.Zuora__Status__c = 'Active';
		testSub_S.Zuora__Zuora_Id__c = TestUtils.generateZuoraId(32);
		testSub_S.Zuora__TermStartDate__c = system.today().addDays(-1);
		update testSub_S;

		testSubProdCharge_S = TestUtils.createZ_SubscriptionCharge('Certification - License - Annually', testSub_S.Id, testAccount_S.Id, testProduct_S.Id, 'Certification - License - Annually', 'Recurring', true);
		system.assert(testSubProdCharge_S != NULL);
		testSubProdCharge_S.Zuora__Product__c = testProduct_S.Id;
		testSubProdCharge_S.Zuora__ProductName__c = testProduct_S.Name;
		testSubProdCharge_S.Zuora__Quantity__c = 50000;
		testSubProdCharge_S.Zuora__Type__c = ManageQuoteService.RECURRING;
		testSubProdCharge_S.Zuora__Model__c = 'Some Model';
		testSubProdCharge_S.Zuora__MonthlyRecurringRevenue__c = 36.67;
		testSubProdCharge_S.Zuora__TotalContractValue__c = testSubProdCharge_S.Zuora__MonthlyRecurringRevenue__c * 12;
		testSubProdCharge_S.Zuora__RatePlanId__c = TestUtils.generateZuoraId(32);
		update testSubProdCharge_S;
		prodKeyToPrice.put(testSubProdCharge_S.Zuora__ProductName__c + ': ' + testSubProdCharge_S.Name, testSubProdCharge_S.Zuora__TotalContractValue__c);

		testSubProdCharge2_S = TestUtils.createZ_SubscriptionCharge('Email Intelligence Solutions - Gold - Annually', testSub_S.Id, testAccount_S.Id, testProduct_S.Id, 'Email Intelligence Solutions - Gold - Annually', 'Recurring', true);
        system.assert(testSubProdCharge2_S != NULL);
        testSubProdCharge2_S.Zuora__Product__c = testProduct_S.Id;
        testSubProdCharge2_S.Zuora__ProductName__c = testProduct_S.Name;
        testSubProdCharge2_S.Zuora__Quantity__c = 1;
        testSubProdCharge2_S.Zuora__Type__c = ManageQuoteService.RECURRING;
        testSubProdCharge2_S.Zuora__Model__c = 'Some Model';
		testSubProdCharge2_S.Zuora__MonthlyRecurringRevenue__c = 36.67;
		testSubProdCharge2_S.Zuora__TotalContractValue__c = testSubProdCharge_S.Zuora__MonthlyRecurringRevenue__c * 12;
		testSubProdCharge2_S.Zuora__RatePlanId__c = TestUtils.generateZuoraId(32);
        update testSubProdCharge2_S;
        prodKeyToPrice.put(testSubProdCharge2_S.Zuora__ProductName__c + ': ' + testSubProdCharge2_S.Name, testSubProdCharge2_S.Zuora__TotalContractValue__c);

        /*amendTestQuote = createZQuote(true, false, testOpportunity);
        amendTestQuote.RecordTypeId = TestUtils.ZUORA_QUOTE_AMEND_RECORD_TYPE.Id;
        amendTestQuote.zqu__ExistSubscriptionID__c = testSub_S.Zuora__Zuora_Id__c;
        amendTestQuote.zqu__StartDate__c = system.today();
        amendTestQuote.zqu__InitialTerm__c = 12;
        amendTestQuote.zqu__RenewalTerm__c = 12;
        amendTestQuote.zqu__AutoRenew__c = false;
        amendTestQuote.zqu__SubscriptionTermStartDate__c = amendTestQuote.zqu__StartDate__c;*/
	}
	
	private static testMethod void testAccountInsert_Eloqua()
	{
		system.debug('What is the Id of the RM'+RM.Id); 
		system.debug('What is the Id of the CRM'+CRM.Id);
		system.debug('What is the Id of the CertManager'+RD.Id);
		system.debug('What is the Id of the Salesperson'+SALES.Id);
		
	
		Account testAccount = TestUtils.createAccount(TestUtils.MARKETER_RECORD_TYPE.Id, false);
		
		test.startTest();
		insert testAccount;
		test.stopTest();
		
		List<Account> insertedAccounts = [SELECT Id, OwnerId FROM Account WHERE Id = :testAccount.Id];
		system.assertEquals(1,insertedAccounts.size(), 'We should have one account');
		system.assertEquals(AccountOwnerChange.ELOQUA.Id, insertedAccounts[0].OwnerId, 'We expect the owner to be Eloqua');
	}
	/*private static void setUpMethod(Account testAccount)
	{	
		//testOpportunity = TestUtils.createOpportunity(TestUtils.INVESTIGATING_RECORD_TYPE.Id, testAccount.Id, true);
		testOpportunity = TestUtils.createOpportunity(TestUtils.RESEARCH_RECORD_TYPE.Id, testAccount.Id, true);
		pb = TestUtils.STANDARD_PRICE_BOOK;
		
		testProduct = TestUtils.createProduct(false);
		testProduct.Family = 'Email Intelligence for Marketers';
		insert testProduct;
		system.assertEquals('Email Intelligence for Marketers', testProduct.Family, 'Family should be tools');
		
		toolsProduct = TestUtils.createPricebookEntry(testProduct.Id, pb.Id, true);
		
		testOli = TestUtils.createOpportunityLineItem(testOpportunity.Id, toolsProduct.Id, true);
		OpportunityLineItem insertedOli = [SELECT Id, Product_Family__c FROM OpportunityLineItem WHERE Id = :testOli.Id];
		system.assertEquals('Email Intelligence for Marketers', insertedOli.Product_Family__c, 'We expect the product family to be tools');
	}*/
	private static testMethod void testAccountUpdate_RM()
	{
		//Account testAccount = TestUtils.createAccount(TestUtils.MARKETER_RECORD_TYPE.Id, false);
		setupExistingSubscriptions();

		testAccount_S.AM__c = RM.Id;
		testAccount_S.CRM__c = CRM.Id;
		testAccount_S.Salesperson__c = SALES.Id;
		testAccount_S.Regional_Director__c = RD.Id;
		//insert testAccount;
		//setUpMethod(testAccount);
		testAccount_S.Total_Active_Subscriptions__c = 1;
		testAccount_S.Cert_Active_Subscriptions__c = 1;

		
		test.startTest();
		update testAccount_S;
		//testOpportunity.StageName = 'Finance Approved';
		//update testOpportunity;
		test.stopTest();
		
		List<Account> insertedAccounts = [SELECT Id, Type, AM__c, OwnerId FROM Account WHERE Id = :testAccount_S.Id];
		system.assertEquals('Current Client/Partner', insertedAccounts[0].Type, 'We expect the account to be a client');
		system.assertEquals(RM.Id, insertedAccounts[0].AM__c, 'The AM field should be populated');
		system.assertEquals(1,insertedAccounts.size(), 'We should have one account');
		system.assertEquals(RM.Id, insertedAccounts[0].OwnerId, 'We expect the owner to be the RM');
	}
	
	private static testMethod void testAccountUpdate_CRM()
	{
		//Account testAccount = TestUtils.createAccount(TestUtils.MARKETER_RECORD_TYPE.Id, false);
		setupExistingSubscriptions();
		testAccount_S.CRM__c = CRM.Id;
		testAccount_S.Salesperson__c = SALES.Id;
		testAccount_S.Regional_Director__c = RD.Id;
		//insert testAccount_S;
		//setUpMethod(testAccount);
		testAccount_S.Total_Active_Subscriptions__c = 1;
		testAccount_S.Cert_Active_Subscriptions__c = 1;
		
		test.startTest();
		update testAccount_S;
		//testOpportunity.StageName = 'Finance Approved';
		//update testOpportunity;
		test.stopTest();
		
		List<Account> insertedAccounts = [SELECT Id, OwnerId, Type, CRM__c FROM Account WHERE Id = :testAccount_S.Id];
		system.assertEquals('Current Client/Partner', insertedAccounts[0].Type, 'We expect the account to be a client');
		system.assertEquals(CRM.Id, insertedAccounts[0].CRM__c, 'The CRM field should be populated');
		system.assertEquals(1,insertedAccounts.size(), 'We should have one account');
		system.assertEquals(CRM.Id, insertedAccounts[0].OwnerId, 'We expect the owner to be the CRM');
	}
	
	private static testMethod void testAccountUpdate_SALES_CLIENT()
	{
		//Account testAccount = TestUtils.createAccount(TestUtils.MARKETER_RECORD_TYPE.Id, false);
		setupExistingSubscriptions();
		testAccount_S.Salesperson__c = SALES.Id;
		testAccount_S.Regional_Director__c = RD.Id;
		//insert testAccount;
		//setUpMethod(testAccount);
		//testAccount_S.CRM__c = CRM.Id;
		//testAccount_S.Salesperson__c = SALES.Id;
		//testAccount_S.Regional_Director__c = RD.Id;
		testAccount_S.Total_Active_Subscriptions__c = 1;
		testAccount_S.Cert_Active_Subscriptions__c = 1;
		
		test.startTest();
		//testOpportunity.StageName = 'Finance Approved';
		//update testOpportunity;
		update testAccount_S;
		test.stopTest();
		
		List<Account> insertedAccounts = [SELECT Id, OwnerId, Type, Salesperson__c FROM Account WHERE Id = :testAccount_S.Id];
		system.assertEquals('Current Client/Partner', insertedAccounts[0].Type, 'We expect the account to be a client');
		system.assertEquals(SALES.Id, insertedAccounts[0].Salesperson__c, 'The Salesperson field should be populated');
		system.assertEquals(1,insertedAccounts.size(), 'We should have one account');
		system.assertEquals(SALES.Id, insertedAccounts[0].OwnerId, 'We expect the owner to be the Salesperson');
	}
	
	private static testMethod void testAccountUpdate_RD_CLIENT()
	{
		//Account testAccount = TestUtils.createAccount(TestUtils.MARKETER_RECORD_TYPE.Id, false);
		setupExistingSubscriptions();
		testAccount_S.Regional_Director__c = RD.Id;
		//insert testAccount;
		//setUpMethod(testAccount);
		testAccount_S.Total_Active_Subscriptions__c = 1;
		testAccount_S.Cert_Active_Subscriptions__c = 1;
		
		test.startTest();
		//testOpportunity.StageName = 'Finance Approved';
		//update testOpportunity;
		update testAccount_S;
		test.stopTest();
		
		List<Account> insertedAccounts = [SELECT Id, OwnerId, Type, Regional_Director__c FROM Account WHERE Id = :testAccount_S.Id];
		system.assertEquals('Current Client/Partner', insertedAccounts[0].Type, 'We expect the account to be a client');
		system.assertEquals(RD.Id, insertedAccounts[0].Regional_Director__c, 'The RD field should be populated');
		system.assertEquals(1,insertedAccounts.size(), 'We should have one account');
		system.assertEquals(RD.Id, insertedAccounts[0].OwnerId, 'We expect the owner to be the RD');
	}
	
	private static testMethod void testAccountUpdate_SF_AUTO()
	{
		//Account testAccount = TestUtils.createAccount(TestUtils.MARKETER_RECORD_TYPE.Id, false);
		setupExistingSubscriptions();
		testAccount_S.BillingState = 'California';
		//insert testAccount;
		//setUpMethod(testAccount);
		testAccount_S.Total_Active_Subscriptions__c = 1;
		testAccount_S.Cert_Active_Subscriptions__c = 1;
		
		test.startTest();
		//testOpportunity.StageName = 'Finance Approved';
		//update testOpportunity;
		update testAccount_S;
		test.stopTest();
		
		
		List<Account> insertedAccounts = [SELECT Id, Internal_Pod_Number__c, OwnerId FROM Account WHERE Id = :testAccount_S.Id];
		system.assertEquals(1,insertedAccounts.size(), 'We should have one account');
		system.assertEquals(TestUtils.SFORCE_AUTO.Id, insertedAccounts[0].OwnerId, 'We expect the owner to be SF Automation');
	}
	
	private static testMethod void testAccountInsert_SALES()
	{
		//Account testAccount = TestUtils.createAccount(TestUtils.MARKETER_RECORD_TYPE.Id, false);
		setupExistingSubscriptions();
		testAccount_S.Salesperson__c = SALES.Id;
		testAccount_S.CRM__c = CRM.Id;

		/*testAccount_S.EIS_Active_Subscriptions__c = 1;
		testAccount_S.Cert_Active_Subscriptions__c = 1;*/
		
		test.startTest();
		//insert testAccount;
		update testAccount_S;
		test.stopTest();
		
		List<Account> insertedAccounts = [SELECT Id, OwnerId, Type, Salesperson__c FROM Account WHERE Id = :testAccount_S.Id];
		system.assertEquals('Active Prospect', insertedAccounts[0].Type, 'We expect the account to be a prospect');
		system.assertEquals(SALES.Id, insertedAccounts[0].Salesperson__c, 'The Salesperson field should be populated');
		system.assertEquals(1,insertedAccounts.size(), 'We should have one account');
		system.assertEquals(SALES.Id, insertedAccounts[0].OwnerId, 'We expect the owner to be the Salesperson');
	}
	
	private static testMethod void testAccountInsert_CRM()
	{
		//Account testAccount = TestUtils.createAccount(TestUtils.MARKETER_RECORD_TYPE.Id, false);
		setupExistingSubscriptions();
		testAccount_S.CRM__c = CRM.Id;
		
		test.startTest();
		update testAccount_S;
		test.stopTest();
		
		List<Account> insertedAccounts = [SELECT Id, OwnerId, Type, CRM__c FROM Account WHERE Id = :testAccount_S.Id];
		system.assertEquals('Active Prospect', insertedAccounts[0].Type, 'We expect the account to be a prospect');
		system.assertEquals(CRM.Id, insertedAccounts[0].CRM__c, 'The CRM field should be populated');
		system.assertEquals(1,insertedAccounts.size(), 'We should have one account');
		system.assertEquals(CRM.Id, insertedAccounts[0].OwnerId, 'We expect the owner to be the CRM');
	}
	
	private static testMethod void testAccountInsert_CHANNEL()
	{
		//Account testAccount = TestUtils.createAccount(TestUtils.MARKETER_RECORD_TYPE.Id, false);
		//insert testAccount;

		setupExistingSubscriptions();
		
		Account espAccount = TestUtils.createAccount(TestUtils.MARKETER_RECORD_TYPE.Id, false);
		espAccount.CRM__c = CRM.Id;
		insert espAccount;
		
		ESP_Information__c esp = new ESP_Information__c();
		esp.Account__c = testAccount_S.Id;
		esp.ESP__c = espAccount.Id;
		
		test.startTest();
		insert esp;
		test.stopTest();
		
		List<Account> insertedAccounts = [SELECT Id, OwnerId, Type FROM Account WHERE Id = :testAccount_S.Id];
		system.assertEquals('Active Prospect', insertedAccounts[0].Type, 'We expect the account to be a prospect');
		system.assertEquals(1,insertedAccounts.size(), 'We should have one account');
		system.assertEquals(espAccount.CRM__c, insertedAccounts[0].OwnerId, 'We expect the owner to be the Channel Manager from the ESP Account');
	}

}