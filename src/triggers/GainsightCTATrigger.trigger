/****************************************************************************************
**  File:   Gainsight CTA Trigger.cls
**  Desc:   Trigger to create Customer_Tag__c records when a CTA is inserted.
**          Built for Return Path
**  Auth:   Josh White
**  Date:   12.01.14
*****************************************************************************************
**  Change History
**  PR  Date        Author          Description 
**  --  --------    ------------    ------------------------------------
****************************************************************************************/

trigger GainsightCTATrigger on JBCXM__CTA__c (before insert) {

    // Get a map of CTA Reason to lists of Eloqua Rules.
    Map<Id, List<GS_Eloqua_Rules__c>> mapCTAReasonIdToListRules = new Map<Id, List<GS_Eloqua_Rules__c>>();

    for (GS_Eloqua_Rules__c rule : [SELECT Id, CTA_Reason__c, Level__c, Tag_Name__c FROM GS_Eloqua_Rules__c WHERE Active__c = true]) {

        List<GS_Eloqua_Rules__c> listRules = new List<GS_Eloqua_Rules__c>();

        if (mapCTAReasonIdToListRules.containsKey(rule.CTA_Reason__c)) { listRules = mapCTAReasonIdToListRules.get(rule.CTA_Reason__c); }

        listRules.add(rule);

        mapCTAReasonIdToListRules.put(rule.CTA_Reason__c, listRules);
    }


    // Get CTA Status for Open and Closed.
    Id ctaStatusOpen;
    Id ctaStatusClosed;

    for (JBCXM__Picklist__c pick : [SELECT Id, JBCXM__SystemName__c FROM JBCXM__Picklist__c WHERE JBCXM__Category__c = 'Alert Status']) {

        if (pick.JBCXM__SystemName__c == 'Open') {

            ctaStatusOpen = pick.Id;
        }

        else if (pick.JBCXM__SystemName__c == 'Closed') {

            ctaStatusClosed = pick.Id;
        }
    }



    // Get Customer_Tag__c record type values
    Map<String, Id> mapRecordTypeNameToId = new Map<String, Id>();

    for (RecordType rt : [SELECT Id, Name FROM RecordType WHERE SObjectType = 'Customer_Tag__c']) { // Values are 'Account Tag' and 'Contact Tag'.

        mapRecordTypeNameToId.put(rt.Name, rt.Id);
    }



    // Get a map of Account Ids to a map of emails to a list of Contacts for parsing out emails later.
    Map<Id, Map<String, List<Contact>>> mapAccountIdToMapEmailToListContact = new Map<Id, Map<String, List<Contact>>>();

    for (JBCXM__CTA__c cta : Trigger.New) {

        mapAccountIdToMapEmailToListContact.put(cta.JBCXM__Account__c, new Map<String, List<Contact>>());
    }


    for (Contact cont : [SELECT Id, Email, AccountId FROM Contact WHERE AccountId IN :mapAccountIdToMapEmailToListContact.keySet() AND Contact_Status__c = 'Current']) {

        Map<String, List<Contact>> mapEmailToListContact = new Map<String, List<Contact>>();
        List<Contact> listContact = new List<Contact>();

        if (mapAccountIdToMapEmailToListContact.containsKey(cont.AccountId)) { mapEmailToListContact = mapAccountIdToMapEmailToListContact.get(cont.AccountId); }

        if (mapEmailToListContact.containsKey(cont.Email)) { listContact = mapEmailToListContact.get(cont.Email); }

        listContact.add(cont);

        mapEmailToListContact.put(cont.Email, listContact);

        mapAccountIdToMapEmailToListContact.put(cont.AccountId, mapEmailToListContact);
    }



    // For each CTA inserted, create all appropriate Customer_Tag__c records based on custom settings
    List<Customer_Tag__c> listCustomerTag = new List<Customer_Tag__c>();

    for (JBCXM__CTA__c cta : Trigger.New) {

        if (mapCTAReasonIdToListRules.containsKey(cta.JBCXM__Reason__c)) {

            for (GS_Eloqua_Rules__c rule : mapCTAReasonIdToListRules.get(cta.JBCXM__Reason__c)) {

                // CTA Stage is set to Closed by default. 
                cta.JBCXM__Stage__c = ctaStatusClosed;

                // If the custom setting creates an Account Tag, assign the tag to the Account and create it.
                if (rule.Level__c == 'Account') {

                    Customer_Tag__c custTag = new Customer_Tag__c();

                    custTag.Name = rule.Tag_Name__c;
                    custTag.Account__c = cta.JBCXM__Account__c;
                    custTag.RecordTypeId = mapRecordTypeNameToId.get('Account Tag');

                    listCustomerTag.add(custTag);
                }

                // If the custom setting creates a Contact Tag, parse all emails from the CTA comments and loop through all Contacts until a match is found.
                else if (rule.Level__c == 'Contact') {

                    // Loop through all emails in the CTA Comments and create a Customer Tag for each matching Contact.
                    boolean matchFound = false;

                    if (cta.JBCXM__Comments__c != null) {

                        // Get the list of Contacts at the given Account.
                        Map<String, List<Contact>> mapEmailToListContact = mapAccountIdToMapEmailToListContact.get(cta.JBCXM__Account__c);

                        // Parse body for email
                        Pattern p = Pattern.compile('[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,4}');

                        Matcher pm = p.matcher(cta.JBCXM__Comments__c);

                        while (pm.find()) {
                            
                            if (mapEmailToListContact.containsKey(pm.group())) {

                                List<Contact> listContact = mapEmailToListContact.get(pm.group());

                                // Create a Customer Tag
                                for (Contact cont : listContact) {

                                    Customer_Tag__c custTag = new Customer_Tag__c();

                                    custTag.Name = rule.Tag_Name__c;
                                    custTag.Account__c = cta.JBCXM__Account__c;
                                    custTag.Contact__c = cont.Id;
                                    custTag.RecordTypeId = mapRecordTypeNameToId.get('Contact Tag');

                                    listCustomerTag.add(custTag);

                                    if (!matchFound) { matchFound = true; }
                                }
                            }
                        }
                    }

                    // If no single matching Contact was found, make the CTA Status set to 'Open'.
                    if (!matchFound) { cta.JBCXM__Stage__c = ctaStatusOpen; }
                }
            }
        }
    }

    if (!listCustomerTag.isEmpty()) { insert listCustomerTag; }
}