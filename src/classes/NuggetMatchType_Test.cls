@IsTest 
private class NuggetMatchType_Test {
private static testmethod void testNuggetMatchType(){


Nugget__c n = new Nugget__c(
Company_Name__c = 'test comp',
State__c = 'CA',
Country__c = 'United States',
Status__c = 'Unassigned');

insert n;

Account a = New Account(
Name = 'test Account');

insert a;

Lead l = new Lead(
LastName = 'testguy',
company = 'rptest');

insert l;

Nugget_Match__c nm1 = New Nugget_Match__c(
Nugget__c = n.Id,
Matching_Account__c = a.Id);

insert nm1;

Nugget_Match__c nm2 = New Nugget_Match__c(
Nugget__c = n.Id,
Matching_Lead__c = l.Id);

insert nm2;

}}