@isTest
private class InvoiceLineItemCreationServiceTest 
{
	static final Decimal OPP_EX_RATE=1; 
	static final Decimal PARTNR_EX_RATE =1;
	static final Integer DAYS_IN_MONTH = 30;
	static final Integer MONTHS_IN_QUARTER = 3;
	static final Integer MONTHS_IN_SEMI = 6;


	static OpportunityLineItem testOLI;

	static testMethod void test_getInvoiceLineItemPrices_wholeMonths_MonthlyBillingType()
	{	
		String billingType=  OpportunityServices.MONTHLY;
		Date startDate = Date.newInstance(2014, 1, 1);
		Date endDate = startDate.addMonths(2);
		Decimal unitPrice = 1000;
		testOli = new OpportunityLineItem(Start_Date__c = startDate, End_Date__c = endDate, UnitPrice = unitPrice);

		Integer expectedNumOfILI  = startDate.monthsBetween(endDate) ;
		Decimal expectedILIPrice = unitPrice/expectedNumOfILI;

		Test.startTest();
			List<Decimal> resultPrices = InvoiceLineItemCreationService.getInvoiceLineItemPrices(testOLI, billingType,  OPP_EX_RATE, PARTNR_EX_RATE);	
		Test.stopTest();

		System.assertEquals(expectedNumOfILI, resultPrices.size(), 'We expect the number of ILI to be '+ expectedNumOfILI+' for billing type monthly for a datespan of 2 months');
		for(Decimal resultPrice : resultPrices)
		{
			System.assertEquals( expectedILIPrice, resultPrice.setScale(2), 'The result price should be equal to the unit price divided by the expected num of ILIs');
		}
	}

	static testMethod void test_getInvoiceLineItemPrices_halfMonthRemaining_MonthlyBillingType()
	{
		String billingType=  OpportunityServices.MONTHLY;
		Date startDate = Date.newInstance(2014, 1, 1);
		Date endDate = startDate.addMonths(3).addDays(15); 
		Decimal unitPrice = 1000;
		testOli = new OpportunityLineItem(Start_Date__c = startDate, End_Date__c = endDate, UnitPrice = unitPrice);

		Integer expectedNumOfILI  = startDate.monthsBetween(endDate) +1 ;
		Decimal expectedPricePerMonth = ( unitPrice/(startDate.daysBetween(endDate)+1) ) * DAYS_IN_MONTH;
		Decimal expectedPriceRemaining = (unitPrice - (expectedPricePerMonth * 3)).setScale(2);

		Test.startTest();
			List<Decimal> resultPrices = InvoiceLineItemCreationService.getInvoiceLineItemPrices(testOLI, billingType,  OPP_EX_RATE, PARTNR_EX_RATE);	
		Test.stopTest();

		System.assertEquals(expectedNumOfILI, resultPrices.size(), 'We expect the number of ILI to be '+ expectedNumOfILI+' for billing type monthly for a datespan of 3 and half months');
		for(Integer i = 0 ; i < resultPrices.size() - 1; i++)
		{
			System.assertEquals( expectedPricePerMonth.setScale(2), resultPrices[i].setScale(2), 'The result price for each month should be equal to our expected price per month');
		}
		System.assertEquals(expectedPriceRemaining, resultPrices[resultPrices.size() -1], 'The last invoice line item price should have the remainder  of the unit price after the amounts have been set for the months');
	}


	static testMethod void test_getInvoiceLineItemPrices_halfQuarterRemaining_QuarterlyBillingType()
	{
		String billingType=  OpportunityServices.QUARTERLY;
		Date startDate = Date.newInstance(2014, 1, 15);
		Date endDate = startDate.addMonths(5); 
		Decimal unitPrice = 12000;
		testOli = new OpportunityLineItem(Start_Date__c = startDate, End_Date__c = endDate, UnitPrice = unitPrice);

		Integer expectedNumOfILI  = getNumOfQuarters(startDate.monthsBetween(endDate) +1) ;
		Decimal expectedPricePerQuarter =  (( ( unitPrice/(startDate.daysBetween(endDate)+1) ) * DAYS_IN_MONTH) * MONTHS_IN_QUARTER).setScale(2);
		Decimal expectedPriceRemaining = (unitPrice - (expectedPricePerQuarter)).setScale(2);

		Test.startTest();
			List<Decimal> resultPrices = InvoiceLineItemCreationService.getInvoiceLineItemPrices(testOLI, billingType,  OPP_EX_RATE, PARTNR_EX_RATE);
		Test.stopTest();

		System.assertEquals(expectedNumOfILI, resultPrices.size(), 'We two prices to be returned, 1 for the Quarter, and one for the remaining quarter');
		for(Integer i = 0; i < resultPrices.size() - 1; i++)
		{
			System.assertEquals(expectedPricePerQuarter, resultPrices[i], 'We expect the result prices for the quarter to be the same as the expected price per quarter');	
		}
		System.assertEquals(expectedPriceRemaining, resultPrices[resultPrices.size() -1], 'The price for the last quarter should have the remainder  of the unit price after the amounts have been set for the previous quarter');
	}

	static testMethod void test_getInvoiceLineItemPrices_WholeQuarter_QuarterlyBillingType()
	{
		String billingType=  OpportunityServices.QUARTERLY;
		Date startDate = Date.newInstance(2014, 1, 15);
		Date endDate = startDate.addMonths(6); 
		Decimal unitPrice = 12000;
		testOli = new OpportunityLineItem(Start_Date__c = startDate, End_Date__c = endDate, UnitPrice = unitPrice);

		Integer expectedNumOfILI  = getNumOfQuarters(startDate.monthsBetween(endDate)) ;
		Decimal expectedPricePerQuarter =  ( ( ( unitPrice/(startDate.daysBetween(endDate)+1) ) * DAYS_IN_MONTH) * MONTHS_IN_QUARTER).setScale(2);
		Decimal expectedPriceRemaining = (unitPrice - (expectedPricePerQuarter) ).setScale(2);

		Test.startTest();
			List<Decimal> resultPrices = InvoiceLineItemCreationService.getInvoiceLineItemPrices(testOLI, billingType,  OPP_EX_RATE, PARTNR_EX_RATE);
		Test.stopTest();

		System.assertEquals(expectedNumOfILI, resultPrices.size(), 'We two prices to be returned, 1 for the Quarter, and one for the remaining quarter');
		for(Integer i = 0; i < resultPrices.size() - 1; i++)
		{
			System.assertEquals(expectedPricePerQuarter, resultPrices[i], 'We expect the result prices for the quarter to be the same as the expected price per quarter');	
		}
		System.assertEquals(expectedPriceRemaining, resultPrices[resultPrices.size() -1], 'The price for the last quarter should have the remainder  of the unit price after the amounts have been set for the previous quarter');
	}

	static testMethod void test_getInvoiceLineItemPrices_halfYear_SemiAnnualBillingType()
	{
		String billingType=  OpportunityServices.SEMI_ANNUALLY;
		Date startDate = Date.newInstance(2014, 3, 15);
		Date endDate = startDate.addMonths(5); 
		Decimal unitPrice = 32000;
		testOli = new OpportunityLineItem(Start_Date__c = startDate, End_Date__c = endDate, UnitPrice = unitPrice);

		Integer expectedNumOfILI  = startDate.monthsBetween(endDate) > MONTHS_IN_SEMI ? 2 : 1;

		Test.startTest();
			List<Decimal> resultPrices = InvoiceLineItemCreationService.getInvoiceLineItemPrices(testOLI, billingType,  OPP_EX_RATE, PARTNR_EX_RATE);
		Test.stopTest();
	
		System.assertEquals(expectedNumOfILI, resultPrices.size(), 'We expect the number of ILI to be '+expectedNumOfILI+' for billingType '+ billingType+' when the datespan is '+startDate.monthsBetween(endDate) );
		System.assertEquals(unitPrice, resultPrices[resultPrices.size() -1],  'Since the datespan is less than a half year for billing type semi annual we expect that the whole Unit price should be in one Invoice Line item');
	}

	static testMethod void test_getInvoiceLineItemPrices_fullYear_SemiAnnualBillingType()
	{
		String billingType=  OpportunityServices.SEMI_ANNUALLY;
		Date startDate = Date.newInstance(2014, 3, 15);
		Date endDate = startDate.addMonths(11); 
		Decimal unitPrice = 32000;
		testOli = new OpportunityLineItem(Start_Date__c = startDate, End_Date__c = endDate, UnitPrice = unitPrice);

		Integer expectedNumOfILI  = startDate.monthsBetween(endDate) > MONTHS_IN_SEMI ? 2 : 1;
		Decimal expectedPricePerSemi =  ( ( ( unitPrice/(startDate.daysBetween(endDate)+1) ) * DAYS_IN_MONTH) * MONTHS_IN_SEMI).setScale(2);
		Decimal expectedPriceRemaining = (unitPrice - (expectedPricePerSemi)).setScale(2);

		Test.startTest();
			List<Decimal> resultPrices = InvoiceLineItemCreationService.getInvoiceLineItemPrices(testOLI, billingType,  OPP_EX_RATE, PARTNR_EX_RATE);
		Test.stopTest();
	
		System.assertEquals(expectedNumOfILI, resultPrices.size(), 
			'We expect the number of ILI to be '+expectedNumOfILI+' for billingType '+ billingType+' when the datespan is '+startDate.monthsBetween(endDate) );
		for(Integer i = 0; i < resultPrices.size()-1; i++)
		{
			System.assertEquals(expectedPricePerSemi, resultPrices[i], 'We expect the result prices for the half year to be the same as the expected price per half year');	
		}
		System.assertEquals(expectedPriceRemaining, resultPrices[resultPrices.size()-1], 'The remaining amount of unit price should be inserted into the 2nd Invoice line item'+
			' for billingType Semi Annual for datespan of more than half a year' );
	}

	static Integer getNumOfQuarters(Integer numOfMonths )
	{
		//Takes in num of months between oli start and end date.
		if(numOfMonths  <= 3)
			return 1;
		else if(numOfMonths >=4 && numOfMonths <=6)
			return 2;
		else if(numOfMonths >=7 && numOfMonths <=9)
			return 3;
		else if(numOfMonths >=10 && numOfMonths <=12)
			return 4;
		return 0;
	}
}