public class CampaignMemberServices 
{


	public static void populateAutoNumbers(List<CampaignMember> campaignMembers)
	{
		List<CampaignMember> filteredCMs =  filterCampaignMembers(campaignMembers);
		if(filteredCMs.isEmpty()) return;

		Map<Id, Lead> relatedLeads = new Map<Id, Lead>([SELECT Campaign_Lead_Number__c,  Id FROM Lead WHERE Id IN: Pluck.Ids('LeadId', filteredCMs)]);
		Map<Id, List<CampaignMember>>  campaignToMembers = GroupBy.Ids('CampaignId', campaignMembers);
		Map<Id, Integer> campaignToMaxMemberNum = new Map<Id, Integer>();
		Map<Id,Campaign> campaignMap = new Map<Id,Campaign>([SELECT Id, (SELECT Id,Campaign_Member_Number__c FROM CampaignMembers ORDER BY Campaign_Member_Number__c DESC NULLS LAST LIMIT 1) FROM Campaign WHERE ID IN: campaignToMembers.keySet()]);

		for(Id campaignId : campaignToMembers.keySet())
		{
			List<CampaignMember> newCampaignMembers = campaignToMembers.get(campaignId);
			List<CampaignMember> lastCampaignMember = campaignMap.get(campaignId).CampaignMembers;
			Integer prevMax = getCMPreviousMaximum(campaignId, campaignMap,lastCampaignMember);
			Integer nextNum = prevMax + 1;
			incrementCMsAndLeads(nextNum,newCampaignMembers, relatedLeads);	
		}


		updateLeads(relatedLeads.values(),filteredCMs);

	}

	public static void updateLeads(List<Lead> relatedLeads, List<CampaignMember> filteredCMs)
	{

		try
		{
			update relatedLeads;
		}

		catch(DMLException dmx)
		{
			Map<Id, CampaignMember> leadToCM = new Map<Id, CampaignMember>();
			for(CampaignMember cm: filteredCMs)
			{
				leadToCm.put(cm.LeadId, cm);
			}

		  	for(Integer i = 0; i< dmx.getNumDml();i++)
            {
                Integer failedIndex = dmx.getDmlindex(i);
                String failedMessage = dmx.getDmlMessage(i);
                leadToCM.get(relatedLeads[failedIndex].Id).addError(failedMessage);
            }
		}
	}

	public static void incrementCMsAndLeads(Integer nextNum, List<CampaignMember> newCampaignMembers, Map<Id,Lead> relatedLeads)
	{	
		if(newCampaignMembers.isEmpty() || relatedLeads.isEmpty()) return;

		for(Integer i = 0; i < newCampaignMembers.size(); i++)
		{

			if(newCampaignMembers[i].Campaign_Member_Number__c == null)
			{
				newCampaignMembers[i].Campaign_Member_Number__c = 0;
			}

			newCampaignMembers[i].Campaign_Member_Number__c = nextNum;

			if(relatedLeads.get(newCampaignMembers[i].LeadId).Campaign_Lead_Number__c == null)
			{
				relatedLeads.get(newCampaignMembers[i].LeadId).Campaign_Lead_Number__c = 0;
			}  

			relatedLeads.get(newCampaignMembers[i].LeadId).Campaign_Lead_Number__c = nextNum;
			nextNum +=1;
		}
	}

	public static Integer getCMPreviousMaximum(Id campaignId, Map<Id,Campaign> campaignMap, List<CampaignMember> lastCampaignMember)
	{
		Integer prevMax;
		if(lastCampaignMember.isEmpty())
		{
			prevMax = 0;
		}
		
		else
		{

			prevMax  =  campaignMap.get(campaignId).CampaignMembers[0].Campaign_Member_Number__c == NULL ? 0 : (Integer)campaignMap.get(campaignId).CampaignMembers[0].Campaign_Member_Number__c;
		}
		return prevMax;
	}



	public static List<CampaignMember> filterCampaignMembers(List<CampaignMember> campaignMembers)
	{
		List<CampaignMember> filteredCampaigns = new List<CampaignMember>();
		for(CampaignMember cm: campaignMembers)
		{
			if(cm.LeadId != NULL)
			{
				filteredCampaigns.add(cm);
			}
		}
		return filteredCampaigns;
	}

}