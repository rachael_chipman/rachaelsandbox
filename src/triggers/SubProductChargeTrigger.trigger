trigger SubProductChargeTrigger on Zuora__SubscriptionProductCharge__c (after insert) {

	if(trigger.isAfter && trigger.isInsert )
	{
		ProductFamilyToSubscriptionCharge.updateChargesWithFamily(trigger.new);
	}

}