@isTest
class CampaignTrackerSchedClass_Test {

   /*static testmethod void test() {
   
   String CRON_EXP = '0 0 * * * ?';
   
    
    c2g__codaGeneralLedgerAccount__c GenLedge = new c2g__codaGeneralLedgerAccount__c (Name = 'Test', c2g__ReportingCode__c = '1234', c2g__Type__c = 'Balance Sheet');
    c2g__codaGeneralLedgerAccount__c GenLedge2 = new c2g__codaGeneralLedgerAccount__c (Name = 'Test', c2g__ReportingCode__c = '1350', c2g__Type__c = 'Balance Sheet');
    c2g__codaGeneralLedgerAccount__c GenLedge3 = new c2g__codaGeneralLedgerAccount__c (Name = 'Test', c2g__ReportingCode__c = '5000', c2g__Type__c = 'Balance Sheet');
    insert new List<c2g__codaGeneralLedgerAccount__c>{GenLedge, GenLedge2, GenLedge3};

    c2g__codaIncomeScheduleDefinition__c incsched = new c2g__codaIncomeScheduleDefinition__c (Name = 'Income Schedule', c2g__GeneralLedgerAccount__c = GenLedge.Id);
    insert incsched;
    
    c2g__codaCompany__c financeAccount = new c2g__codaCompany__c( c2g__TaxIdentificationNumber__c = '06-1566593', Name = 'TestAccount' );
    insert financeAccount;
 
 User user = new user(Alias = 'standt', Email='tes@rp.com', 
        EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
        LocaleSidKey='en_US', ProfileId = '00e0000000734LT', 
        TimeZoneSidKey='America/Los_Angeles', UserName='test@rp.com.rp');
            insert user;
    
    Lead L = new Lead();
        L.OwnerId = user.Id;
        L.FirstName = 'Test';
        L.LastName='Lead';
        L.Company = 'RPTEST';
        L.Street='123 S St';
        L.City='Denver';
        L.State='CO';
        L.PostalCode='80221';
        L.Fax='3033333333';
        L.Website='testrpath.com';
        L.SSC_Volume__c = '50,000 to 250,000';
            insert L;
    
    Campaign c = new Campaign();
        c.Name = 'Test Campaign';
        c.StartDate = system.today() - 30;
        c.EndDate = system.today() + 30;
            insert c;
    
    CampaignMember cm = new CampaignMember();
        cm.CampaignId = c.Id;
        cm.Certification_Application_Created__c = false;
        cm.LeadId = L.Id;
        cm.Run_First_Most_Recent_Campaign_Sync__c = true;
            insert cm;
            
  /*          Lead L2 = new Lead();
        L2.OwnerId = '00500000006olmF';
        L2.Status = 'Eloqua Lead Scoring';
        L2.FirstName = 'Test';
        L2.LastName='Lead';
        L2.Company = 'RPTEST';
        L2.Street='123 S St';
        L2.City='Denver';
        L2.State='CO';
        L2.PostalCode='80221';
        L2.Fax='3033333333';
        L2.Website='testrpath.com';
        L2.SSC_Volume__c = '50,000 to 250,000';
        L2.First_Campaign__c = 'Test Camapaign';
            insert L2;
      
    CampaignMember cm1 = new CampaignMember();
        cm1.CampaignId = c.Id;
        cm1.Certification_Application_Created__c = false;
        cm1.LeadId = L2.Id;
        cm1.Run_First_Most_Recent_Campaign_Sync__c = true;
            insert cm1;
            
             Account acct = new Account();
        acct.OwnerId = user.Id;
        acct.name = 'RPTestAcct1';
            Insert acct;
        
    Contact con = new Contact();
        con.OwnerId = user.Id;
        con.AccountId = acct.Id;
        con.LastName = 'Test';
        con.FirstName = 'Trigger';
        con.Email = 'test@returnpath.com';
            Insert Con;
    
    Opportunity opp = new Opportunity();
        opp.AccountId = acct.id; 
        opp.Name = 'RPTestOpp1'; 
        opp.StageName = 'Negotiating'; 
        opp.CloseDate = Date.valueOf('2009-01-01'); 
        opp.OwnerId = user.Id; 
        opp.RecordTypeId = '012000000004u5b';
            insert opp;
            
    OpportunityContactRole ocr = new OpportunityContactRole();
        ocr.ContactId = con.Id;
        ocr.IsPrimary = true;
        ocr.OpportunityId = opp.Id;
            insert ocr; 
    
    CampaignMember cm2 = new CampaignMember();
        cm2.CampaignId = c.Id;
        cm2.Certification_Application_Created__c = false;
        cm2.ContactId = con.Id; 
        cm2.Run_First_Most_Recent_Campaign_Sync__c = true;
            insert cm2;
            
                      Account acct2 = new Account();
        acct2.OwnerId = user.Id;
        acct2.name = 'RPTestAcct1';
            Insert acct2;
        
    Contact con2 = new Contact();
        con2.OwnerId = user.Id;
        con2.AccountId = acct2.Id;
        con2.LastName = 'Test';
        con2.FirstName = 'Trigger';
        con2.Email = 'test@returnpath.com';
        con2.First_Campaign__c = 'Test Campaign';
            Insert Con2;
    
    Opportunity opp2 = new Opportunity();
        opp2.AccountId = acct2.id; 
        opp2.Name = 'RPTestOpp1'; 
        opp2.StageName = 'Negotiating'; 
        opp2.CloseDate = Date.valueOf('2009-01-01'); 
        opp2.OwnerId = user.Id; 
        opp2.RecordTypeId = '012000000004u5b';
            insert opp2;
            
    OpportunityContactRole ocr2 = new OpportunityContactRole();
        ocr2.ContactId = con2.Id;
        ocr2.IsPrimary = true;
        ocr2.OpportunityId = opp.Id;
            insert ocr2; 
    
    CampaignMember cm3 = new CampaignMember();
        cm3.CampaignId = c.Id;
        cm3.Certification_Application_Created__c = false;
        cm3.ContactId = con2.Id; 
        cm3.Run_First_Most_Recent_Campaign_Sync__c = true;
            insert cm3;*/
            
// Schedule the test job
   /*Test.startTest();

      String jobId = System.schedule('testBasicScheduledApex',
      CRON_EXP, 
         new CampaignTrackerSchedClass());
   // Get the information from the CronTrigger API object

      CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, 
         NextFireTime
         FROM CronTrigger WHERE id = :jobId];

   // Verify the expressions are the same
      System.assertEquals(CRON_EXP, 
         ct.CronExpression);

   // Verify the job has not run
      System.assertEquals(0, ct.TimesTriggered);

   // Verify the next time the job will run
    
      

   Test.stopTest();

   

   }*/
}