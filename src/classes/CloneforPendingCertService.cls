public with sharing class CloneforPendingCertService {
    
    public void processTrigger( List<Opportunity> newOpportunities, Map<Id, Opportunity> OldOpps )
    {
        List<Opportunity> filteredOpportunities = filterOpportunitiesOnUpdate( newOpportunities , oldOpps ); 
        if(!filteredOpportunities.isEmpty())
        {
            List<Opportunity> opportunityWithLineItems = getCertOLIs( filteredOpportunities );
            processPendingCertOpps( opportunityWithLineItems, newOpportunities );
        }
    }
    
    public static void processPendingCertOpps( List<Opportunity> filteredOpportunitiesWithLineItems, List<Opportunity>newOpportunities )
    {   
        List<OpportunityTeamMember> InsertOTMs = new List<OpportunityTeamMember>();
        List<Attachment> InsertAttachments = new List<Attachment>();
        List<Opportunity> UpdateOpps = new List<Opportunity>();
        List<Opportunity> InsertPendingCertOpps = new List<Opportunity>();
        List<OpportunityLineItem> InsertPendingCertOLIs = new List<OpportunityLineItem>();
        Map<Id, List<OpportunityTeamMember>> TeamMembers = getOppTeamMembers(filteredOpportunitiesWithLineItems);
        Map<Id, List<Attachment>> Attachments = getAttachments(filteredOpportunitiesWithLineItems);
        Map<Id, Opportunity> filteredOpportunitiesWithLineItemsMap = new Map<Id, Opportunity>(filteredOpportunitiesWithLineItems);
        List<OpportunityLineItem> DeleteOldCertOLIs = new List<OpportunityLineItem>();
        List<CommissionLineItem> commOlis = new list<CommissionLineItem>();
        //Map<Id, Certification_Application__c> applications = getCertApp(filteredOpportunitiesWithLineItems);
        List<Certification_Application__c> applicationsToUpdate = new List<Certification_Application__c>();
        
        for(Opportunity o : filteredOpportunitiesWithLineItems)
        {
            o.Cloned__c = true;
            o.Product_Certification__c = false;
            o.Product_SSC_Referral_Child__c = false;
            UpdateOpps.add(o);
            InsertPendingCertOpps.add(createClonedOpp(o));
        }
        update UpdateOpps;
        insert InsertPendingCertOpps;
        
        
        for(Opportunity pendingCert : InsertPendingCertOpps)
        {
            if(!Attachments.isEmpty() && Attachments.containsKey( pendingCert.Cloned_From_Id__c ))
            {
                for( Attachment att : Attachments.get(pendingCert.Cloned_From_Id__c))
                {
                    Attachment newAttach = new Attachment();
                    newAttach.Body = att.Body;
                    newAttach.Description = att.Description;
                    newAttach.Name = att.Name;
                    newAttach.OwnerId = att.OwnerId;
                    newAttach.ParentId = pendingCert.Id;
                    InsertAttachments.add(newAttach);
                }
            }
            
            if(!TeamMembers.isEmpty() && TeamMembers.containsKey( pendingCert.Cloned_From_Id__c ))
            {
                for(OpportunityTeamMember member : TeamMembers.get(pendingCert.Cloned_From_Id__c))
                {
                    OpportunityTeamMember otm = new OpportunityTeamMember();
                    otm.OpportunityId = pendingCert.Id;
                    otm.UserId = member.UserId;
                    otm.TeamMemberRole = member.TeamMemberRole;
                    otm.Percent_Commissioned_On__c = member.Percent_Commissioned_On__c;
                    otm.Commission_Percentage__c = member.Commission_Percentage__c;
                    InsertOTMs.add(otm);
                }
            }
            
            /*if(!applications.isEmpty() && applications.containsKey(pendingCert.Cloned_From_Id__c))
            {
                for(Certification_Application__c app : applications.values())
                {
                    app.License_Fee_Opportunity__c = pendingCert.Id;
                    applicationsToUpdate.add(app);
                }
            }*/
            
            Opportunity previousOpp = filteredOpportunitiesWithLineItemsMap.get(pendingCert.Cloned_From_Id__c);
            for(OpportunityLineItem oli : previousOpp.OpportunityLineItems)
            {
                OpportunityLineItem pendingCertOli = new OpportunityLineItem();
                pendingCertOli.OpportunityId = PendingCert.id;
                pendingCertOli.PricebookEntryId = oli.pricebookentryid;
                pendingCertOli.Quantity = oli.Quantity;
                pendingCertOli.UnitPrice = oli.UnitPrice;
                pendingCertOli.Product_Status__c = oli.Product_Status__c;
                pendingCertOli.Description__c = oli.Description__c;
                pendingCertOli.Indirect__c = oli.Indirect__c;
                pendingCertOli.Active__c = oli.Active__c;
                pendingCertOli.Bundle__c = oli.Bundle__c;
                pendingCertOli.Recurring__c = oli.Recurring__c;
                InsertPendingCertOLIs.add(pendingCertOli);
                DeleteOldCertOLIs.add(oli);
                commOlis.add( new CommissionLineItem( pendingCert, pendingCertOli, pendingCert.CurrencyIsoCode ) );
                
				OpportunityLineItem testOli = pendingCertOli.clone();

            }
            
        }
        system.debug('What OTMs are there for insert?'+InsertOTMs);
        system.debug('What Attachments are there for insert?'+InsertAttachments);
        update applicationsToUpdate;
        insert InsertOTMs; 
        insert InsertAttachments;
        insert InsertPendingCertOLIs;
        delete DeleteOldCertOLIs;
        insertCommissionLineItem( commOlis, newOpportunities );
    }
    
    public class CommissionLineItem
    {
    	public Opportunity pendingCertOpp{get; private set;}
    	public OpportunityLineItem pendingCertOli{get; private set;}
    	public String isoCode{get; private set;}
    	
    	public CommissionLineItem( Opportunity pendingCertOpp, OpportunityLineItem pendingCertOli, String isoCode )
    	{
    		this.pendingCertOpp = pendingCertOpp;
    		this.pendingCertOli = pendingCertOli;
    		this.isoCode = isoCode;
    	}
    }
    
    public static void insertCommissionLineItem( List<CommissionLineItem> commOlis, list<Opportunity> newOpps )
    {
    	list<String> currencyCodes = new list<String>();
    	for( CommissionLineItem commOli : commOlis )
    	{
    		currencyCodes.add( commOli.isoCode );
    	}
    	
    	list<PricebookEntry> referralPbes = [SELECT Id, CurrencyIsoCode FROM PricebookEntry WHERE Name = 'Partner Commissions - Referral' and CurrencyIsoCode IN : currencyCodes and Pricebook2.Name = 'Return Path Pricebook'];
    	list<PricebookEntry> resellerPbes = [SELECT Id, CurrencyIsoCode FROM PricebookEntry WHERE Name = 'Partner Commissions - Reseller' and CurrencyIsoCode IN : currencyCodes and Pricebook2.Name = 'Return Path Pricebook'];
    	map<String, PricebookEntry> currencyIsoCodeToReferral = new map<String, PricebookEntry>();
    	map<String, PricebookEntry> currencyIsoCodeToReseller = new map<String, PricebookEntry>();
    	
    	for( PricebookEntry referralPbe : referralPbes )
    	{
    		currencyIsoCodeToReferral.put( referralPbe.CurrencyIsoCode, referralPbe );
    	}
    	
    	for( PricebookEntry resellerPbe : resellerPbes )
    	{
    		currencyIsoCodeToReseller.put( resellerPbe.CurrencyIsoCode, resellerPbe );
    	}
    	
    	list<OpportunityLineItem> commOlisToInsert = new list<OpportunityLineItem>();
    	for( CommissionLineItem commOli : commOlis )
    	{
    		if( commOli.pendingCertOpp.Partner_Commission__c != NULL && commOli.pendingCertOpp.Partner_Commission__c > 0 )
    		{
	    		OpportunityLineItem commission = commOli.pendingCertOli.clone(false, true, false, false);
	    		commission.UnitPrice = (commOli.pendingCertOli.UnitPrice * (commOli.pendingCertOpp.Partner_Commission__c / 100) )*-1;
	    		if( commOli.pendingCertOpp.Deal_Closing_Through__c == 'Return Path' )
	    		{
	    			commission.PricebookEntryId = currencyIsoCodeToReferral.get( commOli.isoCode ).Id;
	    		}
	    		else if( commOli.pendingCertOpp.Deal_Closing_Through__c == 'Partner')
	    		{
	    			commission.PricebookEntryId = currencyIsoCodeToReseller.get( commOli.isoCode ).Id;
	    		}
	    		commOlisToInsert.add( commission );
    		} 
    	}
    	try
    	{
    		insert commOlisToInsert;
    	}
    	catch (Dmlexception e)
    	{
    		for( Integer i = 0; i < e.getNumDml(); i++ )
    		{
    			system.debug( e.getDmlMessage( i ) );
    			for( Opportunity newOpp : newOpps)
    			{
    				newOpp.addError( e.getDmlMessage ( i ));
    			}
    		}
    	}
    }
      
    
    public static List<Opportunity> filterOpportunitiesOnUpdate( List<Opportunity> newOpportunities, Map<Id, Opportunity> OldOpps )
    {
        List<Opportunity> filteredOpportunities = new List<Opportunity>();
        
        for ( Opportunity o : newOpportunities)
        {
            Opportunity OldOpp = OldOpps.get(o.Id);
            if(o.StageName == 'Finance Approved' && o.Clone_for_Pending_Cert__c == 'Yes' && oldOpp.StageName != 'Finance Approved' )
            {
                filteredOpportunities.add(o);
            }
        }
        return filteredOpportunities;       
    }
    
    public static List<Opportunity> getCertOLIs( List<Opportunity> filteredOpportunities)
    {
        return[SELECT Id, OwnerId, StageName, Type, Account.Certification_Client__c, CurrencyISOCode, PartnerLookup__c, Type_of_InvolvementText__c, Partner_Contact__c, CRM_User__c, Deal_Closing_Through__c, Partner_Commission__c, 
        		Who_is_Invoiced__c, POD__c, First_Approver__c, Second_Approver__c,
                (SELECT Id, Product_Code__c, Previous_Value__c, Product_Detail_Created__c, Start_Date__c, End_Date__c,
                PricebookEntryId, Quantity, UnitPrice, Product_Status__c, Description__c, Indirect__c, Active__c, Bundle__c, Recurring__c
                FROM OpportunityLineItems 
                WHERE OpportunityId IN: filteredOpportunities and ( Product_Code__c = '004-CERT-FEE0' or Product_Code__c = '005-CERT-REFC' ) and Previous_Value__c = 0.00 and Product_Detail_Created__c = false )
                FROM Opportunity WHERE Id IN: filteredOpportunities ];
    }
    
    private static Map<Id, List<OpportunityTeamMember>> getOppTeamMembers( List<Opportunity> filteredOpportunitiesWithLineItems )
    {
        Map<Id, List<OpportunityTeamMember>> OpportunityIdtoTeamMembers = new Map<Id, List<OpportunityTeamMember>>();
        List<OpportunityTeamMember> TeamMembersInOpps = [ SELECT Id, OpportunityId, OpportunityAccessLevel, TeamMemberRole, UserId, Percent_Commissioned_On__c, Commission_Percentage__c
             FROM OpportunityTeamMember
             WHERE OpportunityId IN: filteredOpportunitiesWithLineItems ];
             
        for( Opportunity o : filteredOpportunitiesWithLineItems )
        {
            OpportunityIdtoTeamMembers.put(o.Id, new List<OpportunityTeamMember>() );
            
            for( OpportunityTeamMember otm : TeamMembersInOpps)
            {
                OpportunityIdtoTeamMembers.get(o.Id).add( otm );
            }
        }
        system.debug('What Opp Team Members are in the map?'+OpportunityIdtoTeamMembers);
        return OpportunityIdtoTeamMembers; 
    }
    
    private static Map< Id, List<Attachment>> getAttachments( List<Opportunity> filteredOpportunitiesWithLineItems )
    {
        Map<Id, List<Attachment>> OpportunityIdtoAttachments = new Map<Id, List<Attachment>>();
        List<Attachment> AttachmentsInOpps = [SELECT Id, Body, Description, Name, OwnerId, ParentId FROM Attachment WHERE ParentId IN : filteredOpportunitiesWithLineItems];
        
        for( Opportunity o : filteredOpportunitiesWithLineItems )
        {
            OpportunityIdtoAttachments.put( o.Id, new List<Attachment>() );
                
            for( Attachment attach : AttachmentsInOpps )
            {
                OpportunityIdtoAttachments.get( o.Id ).add( attach );
            }
        }
        system.debug('What attachments are in the map?'+OpportunityIdtoAttachments);
        return OpportunityIdtoAttachments;
    }
    
    /*private static Map<Id, Certification_Application__c> getCertApp (List<Opportunity> filteredOpportunitiesWithLineItems)
    {
        Map<Id, Certification_Application__c> oppIdToCertApp = new Map<Id, Certification_Application__c>();
        List<Certification_Application__c> appsInOpps = [SELECT Id, License_Fee_Opportunity__c 
                                                         FROM Certification_Application__c 
                                                         WHERE License_Fee_Opportunity__c IN : filteredOpportunitiesWithLineItems];
        for(Certification_Application__c app : appsInOpps)
        {
            oppIdToCertApp.put(app.License_Fee_Opportunity__c, app);
        }
        return oppIdToCertApp;
    }*/
    
    
    public static Opportunity createClonedOpp ( Opportunity o )
    {
        Opportunity PendingCertOpp = o.clone(false, true, false, false);
         PendingCertOpp.StageName = 'Pending Certification Qualification';
         PendingCertOpp.Approval_Type__c = 'Certification Approval';
         PendingCertOpp.CloseDate = system.today() + 30;
         if(PendingCertOpp.Type == 'Renewal') PendingCertOpp.Type = 'Cross-Sell';
         PendingCertOpp.Reporting_Date__c = system.today();
         PendingCertOpp.Name = 'WFR Name';
         PendingCertOpp.Cloned__c = false;
         PendingCertOpp.OwnerId = o.OwnerId;
         PendingCertOpp.Cloned_From_ID__c = o.Id;
         PendingCertOpp.Original_Opportunity__c = 0;
         PendingCertOpp.Begin_Automation__c = false;
         PendingCertOpp.Record_Locked__c = false;
         PendingCertOpp.Product_Additional_AM_Services__c = false;
         PendingCertOpp.Product_Campaign_Preview_API__c = false;
         PendingCertOpp.Product_CorpDev_Research__c = false;
         PendingCertOpp.Product_Domain_Assurance__c = false;
         PendingCertOpp.Product_Domain_Audit_Service__c = false;
         PendingCertOpp.Product_Domain_Protect__c = false;
         PendingCertOpp.Product_Domain_Protect_Child__c = false;
         PendingCertOpp.Product_Domain_Secure__c = false;
         PendingCertOpp.Product_Domain_Secure_Child__c = false;
         PendingCertOpp.Product_Tools_Child__c = false;
         PendingCertOpp.Product_Gold__c = false;
         PendingCertOpp.Product_Internal_Use__c = false;
         PendingCertOpp.Product_Platinum__c = false;
         PendingCertOpp.Product_Email_Brand_Monitor__c = false;
         PendingCertOpp.Product_Email_Brand_Monitor_Child__c = false;
         PendingCertOpp.Product_Pixel_Tracking__c = false;
         PendingCertOpp.Product_General_Referral__c = false;
         PendingCertOpp.Product_Inbox_Measurement__c = false;
         PendingCertOpp.Product_Managed_Delivarbility__c = false;
         PendingCertOpp.Product_Preferred_Partner_Framework__c = false;
         PendingCertOpp.Product_Deliverability_Consulting__c = false;
         PendingCertOpp.Product_Regional_ISP_Mediation__c = false;
         PendingCertOpp.Product_ROI_and_Response_Consulting__c = false;
         PendingCertOpp.Product_RPost__c = false;
         PendingCertOpp.Product_Safe__c = false;
         PendingCertOpp.Product_Sender_Score_Premium__c = false;
         PendingCertOpp.Product_SSRA__c = false;
         PendingCertOpp.Product_Strategic_Media_Services__c = false;
         PendingCertOpp.Product_Technology_Services__c = false;
         PendingCertOpp.Product_Tools_Only__c = false;
         PendingCertOpp.Product_Tools_Reseller_API__c = false;
         PendingCertOpp.Product_Tools_Reseller_Framework__c = false;
         return PendingCertOpp;
    }
    
    public static void processOlisToBeDeleted(List<Opportunity> newOpportunities, Map<Id, Opportunity> oldOpps)
    {
    	List<OpportunityLineItem> certOlisToUpdate = new List<OpportunityLineItem>();
    	List<Opportunity> filteredOpps = filterOpportunitiesOnUpdate(newOpportunities, oldOpps);
    	
    	if(!filteredOpps.isEmpty())
    	{
	    	List<Opportunity> filteredOppsWithLineItems = getCertOLIs(filteredOpps);
	    	for(Opportunity opp : filteredOppsWithLineItems)
	    	{
	    		for(OpportunityLineItem certOli : opp.OpportunityLineItems)
	    		{
	    			certOli.Mark_for_Deletion__c = true;
	    			certOlisToUpdate.add(certOli);
	    		}
	    	}
	    	update certOlisToUpdate;
    	}
    	
    }
    
    
}