@ isTest
Public class CampaignTracker_Test{

    /*public static testMethod void testCampaignTracker1(){
    
    User user = new user(Alias = 'standt', Email='tes@rp.com', 
        EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
        LocaleSidKey='en_US', ProfileId = '00e0000000734LT', 
        TimeZoneSidKey='America/Los_Angeles', UserName='test@rp.com.rp');
            insert user;
    
    Lead L = new Lead();
        L.OwnerId = user.Id;
        L.FirstName = 'Test';
        L.LastName='Lead';
        L.Company = 'RPTEST';
        L.Street='123 S St';
        L.City='Denver';
        L.State='CO';
        L.PostalCode='80221';
        L.Fax='3033333333';
        L.Website='testrpath.com';
        L.SSC_Volume__c = '50,000 to 250,000';
            insert L;
    
    Campaign c = new Campaign();
        c.Name = 'Test Campaign';
        c.StartDate = system.today() - 30;
        c.EndDate = system.today() + 30;
            insert c;
    
    CampaignMember cm = new CampaignMember();
        cm.CampaignId = c.Id;
        cm.Certification_Application_Created__c = false;
        cm.LeadId = L.Id;
            insert cm;
            update cm;

    Campaign c2 = new Campaign();
        c2.Name = 'Test Campaign';
        c2.StartDate = system.today() - 30;
        c2.EndDate = system.today() + 30;
            insert c2;  
    
    CampaignMember cm2 = new CampaignMember();
        cm2.CampaignId = c2.Id;
        cm2.Certification_Application_Created__c = false;
        cm2.LeadId = L.Id;    
            insert cm2;
            update cm2;
            delete cm2;
    
    }
    
    public static testMethod void testCampaignTracker2(){
    
    User user = new user(Alias = 'standt', Email='tes@rp.com', 
        EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
        LocaleSidKey='en_US', ProfileId = '00e0000000734LT', 
        TimeZoneSidKey='America/Los_Angeles', UserName='test@rp.com.rp');
            insert user;
    
    Account acct = new Account();
        acct.OwnerId = user.Id;
        acct.name = 'RPTestAcct1';
            Insert acct;
        
    Contact con = new Contact();
        con.OwnerId = user.Id;
        con.AccountId = acct.Id;
        con.LastName = 'Test';
        con.FirstName = 'Trigger';
        con.Email = 'test@returnpath.com';
            Insert Con;
    
    Opportunity opp = new Opportunity();
        opp.AccountId = acct.id; 
        opp.Name = 'RPTestOpp1'; 
        opp.StageName = 'Negotiating'; 
        opp.CloseDate = system.today(); 
        opp.OwnerId = user.Id; 
        opp.RecordTypeId = '012000000004u5b';
            insert opp;
            
    OpportunityContactRole ocr = new OpportunityContactRole();
        ocr.ContactId = con.Id;
        ocr.IsPrimary = true;
        ocr.OpportunityId = opp.Id;
            insert ocr; 
    
    Campaign c = new Campaign();
        c.Name = 'Test Campaign';
        c.StartDate = system.today() - 30;
        c.EndDate = system.today() + 30;
            insert c;
    
    CampaignMember cm = new CampaignMember();
        cm.CampaignId = c.Id;
        cm.Certification_Application_Created__c = false;
        cm.ContactId = con.Id;   
            insert cm;
            update cm;

            Opportunity opp2 = new Opportunity();
        opp2.AccountId = acct.id; 
        opp2.Name = 'RPTestOpp1'; 
        opp2.StageName = 'Negotiating'; 
        opp2.CloseDate = system.today(); 
        opp2.OwnerId = user.Id; 
        opp2.RecordTypeId = '012000000004u5b';
            insert opp2;
            
    OpportunityContactRole ocr2 = new OpportunityContactRole();
        ocr2.ContactId = con.Id;
        ocr2.IsPrimary = true;
        ocr2.OpportunityId = opp.Id;
            insert ocr2;        
            
    Campaign c2 = new Campaign();
        c2.Name = 'Test Campaign';
        c2.StartDate = system.today() - 30;
        c2.EndDate = system.today() + 30;
            insert c2;  
    
    CampaignMember cm2 = new CampaignMember();
        cm2.CampaignId = c2.Id;
        cm2.Certification_Application_Created__c = false;
        cm2.ContactId = con.Id;     
            insert cm2;
            update cm2;
            delete cm2;
    }
    
    public static testMethod void testCampaignTracker3(){
    
    User user = new user(Alias = 'standt', Email='tes@rp.com', 
        EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
        LocaleSidKey='en_US', ProfileId = '00e0000000734LT', 
        TimeZoneSidKey='America/Los_Angeles', UserName='test@rp.com.rp');
            insert user;
    
    Account acct = new Account();
        acct.OwnerId = user.Id;
        acct.name = 'RPTestAcct1';
        acct.Customer_Relationship_Manager_Primary__c = 'BIO Automation';
        acct.AccountManager__c = 'BIO Automation';
            Insert acct;
        
    Contact con = new Contact();
        con.OwnerId = user.Id;
        con.AccountId = acct.Id;
        con.LastName = 'Test';
        con.FirstName = 'Trigger';
        con.Email = 'test@returnpath.com';
            Insert Con;
    
    Opportunity opp = new Opportunity();
        opp.AccountId = acct.id; 
        opp.Name = 'RPTestOpp1'; 
        opp.StageName = 'Negotiating'; 
        opp.CloseDate = system.today(); 
        opp.OwnerId = user.Id; 
        opp.RecordTypeId = '012000000004u5b';
            insert opp;
            
    OpportunityContactRole ocr = new OpportunityContactRole();
        ocr.ContactId = con.Id;
        ocr.IsPrimary = true;
        ocr.OpportunityId = opp.Id;
            insert ocr;

    Campaign c = new Campaign();
        c.Name = 'Test Campaign';
        c.StartDate = system.today() - 30;
        c.EndDate = system.today() + 30;
            insert c;
                
    CampaignMember cm = new CampaignMember();
        cm.CampaignId = c.Id;
        cm.Certification_Application_Created__c = false;
        cm.ContactId = con.Id;   
            insert cm;
            update cm;
            delete cm;
    }*/
    }