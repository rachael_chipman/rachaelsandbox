@isTest (seeAllData=true)
Public class PartnerInvoiceController_Test{
    public static testMethod void testPartnerInvoiceController(){
    PageReference pageRef = Page.DirectInvoice;
Test.setCurrentPageReference(pageRef);
        
 Account acct = new Account();
        acct.name = 'RPTestacct1';
        acct.BillingCountry = 'USA';
        acct.c2g__CODAAccountsReceivableControl__c = 'a2Z00000000H8ou';
        acct.currencyISOcode = 'EUR';
        Insert acct;


 Opportunity opp1 = new Opportunity(
        AccountId = acct.id, 
        Name = 'RPTestOpp1', 
        Pricebook2Id = '01s000000004NS6AAM',
        StageName = 'Signature', 
        CloseDate = system.today(), 
        Type = 'New Business',
        Billing_Terms__c = 'Net 30',
        
        Invoice_Created__c = false,
        Billing_Type__c = 'Annual',
        currencyISOcode = 'USD'
                
        );
        
        Insert opp1;
        
        OpportunityLineItem prod1 = new OpportunityLineItem();
        prod1.OpportunityId = opp1.id;
        prod1.PricebookEntryId = '01u00000000FGwcAAG';
        prod1.Quantity = 12;
        prod1.UnitPrice = 1200;
        prod1.of_MM_Events_Allowed__c = 20;
        prod1.of_CP_Events_Allowed__c = 20;
        prod1.MM_Overage_Fee__c = 25;
        prod1.CP_Overage_Fee__c = 25;
        prod1.of_BLA_IPs__c = 10;
        prod1.of_Rep_Mon_IPs__c = 10;
        prod1.Product_Status__c = 'Current';
        prod1.Description__c = 'Deliverability Tools';
        prod1.Indirect__c = false;
        prod1.Active__c = true;
        prod1.Previous_Value__c = 0;
        prod1.Bundle__c = false;
        prod1.Recurring__c = true;
        prod1.Start_Date__c = Date.valueOf('2013-01-01');
        prod1.End_Date__c = Date.valueOf('2013-12-31');
        Insert prod1;
        
        Partner_Invoice__c pi = new Partner_Invoice__c(Partner_account__c = acct.Id);
        insert pi;

c2g__codaInvoice__c si = new c2g__codaInvoice__c(
c2g__Account__c = acct.Id,
c2g__InvoiceDate__c = system.Today(), 
c2g__DueDate__c = system.Today().addMonths(1), 
c2g__Opportunity__c = opp1.Id,
Opportunity_Account__c = acct.Id,
c2g__InvoiceCurrency__c = 'a10000000005AZ5',
Partner_Invoice__c = pi.Id,
Ready_for_Posting__c = true);

insert si;

c2g__codaInvoiceLineItem__c sili = new c2g__codaInvoiceLineItem__c(
        c2g__Invoice__c = si.Id,
        c2g__Product__c = '01t00000000AE4q',
        c2g__Quantity__c = 1,
        c2g__UnitPrice__c = 25,
        c2g__NumberofJournals__c = 1,
        Opportunity__c = opp1.Id,
        c2g__StartDate__c = system.Today(),
        End_Date__c = Date.Today().addMonths(1),
        c2g__LineDescription__c = 'test product',
        ffbilling__DeriveUnitPriceFromProduct__c = false,
        c2g__IncomeSchedule__c = [SELECT Id, Name FROM c2g__codaIncomeScheduleDefinition__c WHERE Name = 'Income Schedule' LIMIT 1].Id);

        
        insert sili;
        
       
 
        
        
        System.currentPagereference().getParameters().put('id',pi.Id);
        ApexPages.StandardController sc = new ApexPages.standardController(pi);
        PartnerInvoiceController oPEE = new PartnerInvoiceController(sc);   
        
        oPEE.getinvoices();
        }
        }