@isTest(SeeAllData=true)
public class AssignPODToLeadTest {

	testMethod static private void  TestSingleLead()
	{
		Lead newlead = new Lead();
		newlead.LastName = 'Test';
		newlead.Company = 'RP';
		newlead.Country = 'United States';
		newlead.Identity__c = 'Channel';
		Test.startTest();
		insert newlead;
		Test.stopTest();
        Lead tlead = [Select Id, Pod_Number__c, Identity__c from Lead where Id =:newlead.Id limit 1][0];
        System.debug('Lead is :' +tlead);
		System.assertequals(tlead.Pod_Number__c, '15', 'Pod value should be 15');
		
	}
    testMethod static private void  TestbulkLead()
	{
        List<Lead> newleads = new List<Lead>();
        for(Integer x =0;x<100;x++)
        {
            Lead newlead = new Lead();
            newlead.LastName = 'Test';
            newlead.Company = 'RP';
            newlead.Country = 'United States';
            newlead.Identity__c = 'Channel';
            newleads.add(newlead);
        }
        Test.startTest();    
		insert newleads;
		Test.stopTest();
        List<Lead> tlead = [Select Id, Pod_Number__c, Identity__c from Lead where Id in:newleads ];
        for(Lead templead:tlead)
        {
			System.assertequals(templead.Pod_Number__c, '15', 'Pod value should be 15');
        }
		
	}
    
    
    testMethod static private void  TestbulkLeadWithCountryAndState()
	{
        List<Lead> newleads = new List<Lead>();
        for(Integer x =0;x<100;x++)
        {
            Lead newlead = new Lead();
            newlead.LastName = 'Test';
            newlead.Company = 'RP';
            newlead.Country = 'United States';
            newlead.State = 'Colorado';
            newleads.add(newlead);
        }
        Test.startTest();    
		insert newleads;
		Test.stopTest();
        List<Lead> tlead = [Select Id, Pod_Number__c, Identity__c from Lead where Id in:newleads ];
        for(Lead templead:tlead)
        {
			System.assertequals(templead.Pod_Number__c, '11', 'Pod value should be 11');
        }
		
	}
    
    testMethod static private void  TestbulkLeadWithCountryAndState2()
	{
        List<Lead> newleads = new List<Lead>();
        for(Integer x =0;x<100;x++)
        {
            Lead newlead = new Lead();
            newlead.LastName = 'Test';
            newlead.Company = 'RP';
            newlead.Country = 'United States';
            newlead.State = 'Georgia';
            newleads.add(newlead);
        }
        Test.startTest();    
		insert newleads;
		Test.stopTest();
        List<Lead> tlead = [Select Id, Pod_Number__c, Identity__c from Lead where Id in:newleads ];
        for(Lead templead:tlead)
        {
			System.assertequals(templead.Pod_Number__c, '14', 'Pod value should be 14');
        }
		
	}
    
    testMethod static private void  TestbulkLeadWithCountryOnly()
	{
        List<Lead> newleads = new List<Lead>();
        for(Integer x =0;x<100;x++)
        {
            Lead newlead = new Lead();
            newlead.LastName = 'Test';
            newlead.Company = 'RP';
            newlead.Country = 'Canada';
            newleads.add(newlead);
        }
        Test.startTest();    
		insert newleads;
		Test.stopTest();
        List<Lead> tlead = [Select Id, Pod_Number__c, Identity__c from Lead where Id in:newleads ];
        for(Lead templead:tlead)
        {
			System.assertequals(templead.Pod_Number__c, '16', 'Pod value should be 16');
        }
		
	}
    
    testMethod static private void  TestbulkLeadWithCountry2()
	{
        List<Lead> newleads = new List<Lead>();
        for(Integer x =0;x<100;x++)
        {
            Lead newlead = new Lead();
            newlead.LastName = 'Test';
            newlead.Company = 'RP';
            newlead.Country = 'Korea';
            newleads.add(newlead);
        }
        Test.startTest();    
		insert newleads;
		Test.stopTest();
        List<Lead> tlead = [Select Id, Pod_Number__c, Identity__c from Lead where Id in:newleads ];
        for(Lead templead:tlead)
        {
			System.assertequals(templead.Pod_Number__c, '50', 'Pod value should be 50');
        }
		
	}
    
    testMethod static private void  TestbulkLeadWithCountry3()
	{
        List<Lead> newleads = new List<Lead>();
        for(Integer x =0;x<100;x++)
        {
            Lead newlead = new Lead();
            newlead.LastName = 'Test';
            newlead.Company = 'RP';
            newlead.Country = 'Macedonia';
            newleads.add(newlead);
        }
        Test.startTest();    
		insert newleads;
		Test.stopTest();
        List<Lead> tlead = [Select Id, Pod_Number__c, Identity__c from Lead where Id in:newleads ];
        for(Lead templead:tlead)
        {
			System.assertequals(templead.Pod_Number__c, '31', 'Pod value should be 31');
        }
		
	}

}