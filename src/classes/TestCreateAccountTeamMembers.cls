@isTest
public class TestCreateAccountTeamMembers {
    
    
    static Account acc;
    static List<User> userslist;
    static Id AlexUserId = '00500000006pFZL';
    static Id InActiveUserId = '0050000000740FI';
    
   public static void getUser()
   {
       userslist = new list<User>();
       userslist = [SELECT Id FROM User WHERE Profile.Name = 'Sales' AND isActive = true LIMIT 10];
   }
  
   private void createTestUser(string TFirstName,string TLastName)
   {
       User Tuser = new User();
       Tuser.FirstName = TFirstName;
       Tuser.LastName = TLastName;
       Tuser.Alias = TLastName;
       
   }
    
    
   private static void setup()
    {
        getUser();
        acc = new Account();
        acc.Name = 'test acc';
        acc.CRm__c = userslist[0].id;
        acc.AM__c = userslist[1].id;
        //acc.CorpDev_Other__c = userslist[2].id;
        acc.CorpDev_Other__c = AlexUserId;
        acc.Salesperson__c = userslist[3].id;
        acc.Inbox_Insight_Client_Services__c = userslist[4].id;
        acc.APS_Client_Services__c = userslist[5].id;
        acc.Technical_Account_Manager__c = userslist[6].id;
        insert acc;
    }
    
    private static void setupforInactiveUser()
    {
      
        getUser();
        acc = new Account();
        acc.Name = 'test acc';
        acc.AM__c = InActiveUserId;
        //acc.CorpDev_Other__c = userslist[2].id;
        acc.CorpDev_Other__c = AlexUserId;
        acc.Salesperson__c = InActiveUserId;
        acc.Inbox_Insight_Client_Services__c = userslist[4].id;
        acc.APS_Client_Services__c = InActiveUserId;
        acc.Technical_Account_Manager__c = userslist[6].id;
        insert acc;
    }
    
    
    static testmethod void  testAccountteam()
    {
        setup();                                                                                                                                                       
        Test.startTest();
        CreateAccountTeamMembers CM = new CreateAccountTeamMembers();
        Database.executeBatch(CM);
        
         Test.stopTest();
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       
        Account tacc = [Select Id, Name, (Select Id, UserId, TeammemberRole,isDeleted from AccountTeamMembers where isDeleted!= true) from account where Id =:acc.Id ];
        List<AccountTeamMember> accteams = new List<AccountTeamMember>();
        accteams = tacc.AccountTeamMembers;
        System.assert(accteams.size() > 0);
        System.debug('Acc team size is : ' +accteams.size()) ;
        System.assertEquals(accteams.size(),7);  // as all the time Salesperson field is not converted
        
        for(AccountTeamMember TM: accteams)
        {
            if(TM.TeamMemberRole == 'Channel Manager')
            {
                System.assertEquals(acc.CRM__c, TM.UserId);                          
            }
            else if(TM.TeamMemberRole == 'Relationship Manager')                                                                                                                                                      
            {
                System.assertEquals(acc.AM__c, TM.UserId);                                                                           
            }
            else if(TM.TeamMemberRole == 'Relationship Manager')                                                                                                                                                      
            {
                System.assertEquals(acc.AM__c, TM.UserId);                                                                           
            }
            
            else if(TM.TeamMemberRole == 'Technical Account Manager')
            {
               System.assertEquals(acc.Technical_Account_Manager__c , TM.UserId);                          
            }
            else if(TM.TeamMemberRole == 'EFP Client Services')                                                                                                                                                      
            {
                System.assertEquals(acc.APS_Client_Services__c, TM.UserId);                                                                           
            }
            else if(TM.TeamMemberRole == 'Inbox Insight Client Services')                                                                                                                                                      
            {
                System.assertEquals(acc.Inbox_Insight_Client_Services__c, TM.UserId);                                                                           
            }
            else if(TM.TeamMemberRole == 'Provider BD')                                                                                                                                                      
            {
                System.assertEquals(acc.CorpDev_Other__c, TM.UserId);                                                                           
            }
        }
        
    }
    
    
    
    static testmethod void  testAccountteamInActiveUser()
    {
        setupforInactiveUser();  
        
        Test.startTest();
        CreateAccountTeamMembers CM = new CreateAccountTeamMembers();
        Database.executeBatch(CM);
        //CM.execute(CM);
         Test.stopTest();
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       
        Account tacc = [Select Id, Name, (Select Id, UserId, TeammemberRole,isDeleted from AccountTeamMembers where isDeleted!= true) from account where Id =:acc.Id ];
        List<AccountTeamMember> accteams = new List<AccountTeamMember>();
        accteams = tacc.AccountTeamMembers;
        System.assert(accteams.size() > 0);
        System.debug('Acc team size is : ' +accteams.size()) ;
        System.assertEquals(accteams.size(),3);  // as all the time Salesperson field is not converted
        
        for(AccountTeamMember TM: accteams)
        {
            if(TM.TeamMemberRole == 'Channel Manager')
            {
                System.assertEquals(acc.CRM__c, TM.UserId);                          
            }
          /*  else if(TM.TeamMemberRole == 'Relationship Manager')                                                                                                                                                      
            {
                System.assertEquals(acc.AM__c, TM.UserId);                                                                           
            }*/
            else if(TM.TeamMemberRole == 'Relationship Manager')                                                                                                                                                      
            {
                System.assertEquals(acc.AM__c, TM.UserId);                                                                           
            }
            
            else if(TM.TeamMemberRole == 'Technical Account Manager')
            {
               System.assertEquals(acc.Technical_Account_Manager__c , TM.UserId);                          
            }
          /*  else if(TM.TeamMemberRole == 'EFP Client Services')                                                                                                                                                      
            {
                System.assertEquals(acc.APS_Client_Services__c, TM.UserId);                                                                           
            }*/
            else if(TM.TeamMemberRole == 'Inbox Insight Client Services')                                                                                                                                                      
            {
                System.assertEquals(acc.Inbox_Insight_Client_Services__c, TM.UserId);                                                                           
            }
          /*  else if(TM.TeamMemberRole == 'Provider BD')                                                                                                                                                      
            {
                System.assertEquals(acc.CorpDev_Other__c, TM.UserId);                                                                           
            }*/
        }
        
    }

    
    static testmethod void testschedulemethod()
    {
        createAccountTeamMembers CM = new createAccountTeamMembers();
        string CRON_EXP = '11 11 11 28 11 ? 2015';
        Test.startTest();
        //schedule the test job
        String JobId = System.Schedule('CreateAccountMemberTest', CRON_EXP, CM);
         // Get the information from the CronTrigger API object
        CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, 
         NextFireTime
         FROM CronTrigger WHERE id = :jobId];

       // Verify the expressions are the same
        System.assertEquals(CRON_EXP, 
         ct.CronExpression);

       // Verify the job has not run
      System.assertEquals(0, ct.TimesTriggered);
        Test.stopTest();
    }
}