/** Handler class for the Z_SubscriptionChargeTrigger trigger
 *
 * @author Marin Zaimov (marin.zaimov@zuora.com)
 * @version 1.0 June 11, 2015
 */
public with sharing class Z_SubscriptionChargeTriggerHandler {

    public Z_SubscriptionChargeTriggerHandler() {
        
    }

  /**
   * Runs after insert. Pulls fields from Zuora into SFDC
   */
  public void onAfterInsert(List<Zuora__SubscriptionProductCharge__c> newSubChargeList, Map<Id,Zuora__SubscriptionProductCharge__c> newSubChargeMap)
  {
    Set<Id> subChargeIds = newSubChargeMap.keySet();
    System.debug('# of charges: ' + newSubChargeList.size());
    if (subChargeIds.size() > 0) {
      updateIncludedUnitsAndOveragePriceFromZuora(newSubChargeList, newSubChargeMap);
    }
  }

  /**
   * Calls a batch class to pull in fields from Zuora
   * Running a batch class since a full sync of all Zuora data may run over execution time limits
   */
  private static void updateIncludedUnitsAndOveragePriceFromZuora(List<Zuora__SubscriptionProductCharge__c> newSubChargeList, Map<Id,Zuora__SubscriptionProductCharge__c> newSubChargeMap)
  {

    /* NOTE(Marin): Re-querying objects here so we can update them in the batch job. After trigger records are read only by default. */
    Set<Id> chargeIds = newSubChargeMap.keySet();
    newSubChargeList = [SELECT Id, IncludedUnits__c, OveragePrice__c, Zuora__Zuora_Id__c FROM Zuora__SubscriptionProductCharge__c WHERE Id IN :chargeIds];
    
    /* NOTE(Marin): Running a batch class since a full sync of all Zuora data may run over execution time limits */
    /* WARNING(Marin): Zuora Query Language can only have 200 filter statements in a query. THe language also has no parenthesis,
     * therefore 1 of our queries can only pull in 100 objects, due to extra filter statements. It is important we keep the batch scope below 100. I've set it to 90. */
//    Z_UpdateSubscriptionChargeBatch subChargeBatch = new Z_UpdateSubscriptionChargeBatch(newSubChargeList);
    Database.executeBatch(new Z_UpdateSubscriptionChargeBatch(newSubChargeList), 90);
  }

}