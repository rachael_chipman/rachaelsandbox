public with sharing class PromoCodeService {
	
	public void processTrigger(List<Promo_Code__c> newPromos)
	{
		List<Promo_Code__c> filterPromoCodes = filterPromoCodesonUpdate(newPromos);
		if(!filterPromoCodes.isEmpty())
		{
			processPromoCodes(filterPromoCodes, newPromos);
		}
	}
	
	public void processPromoCodes(List<Promo_Code__c> filteredPromos, List<Promo_Code__c> newPromos )
	{
		Map<Id, Account> PromoCodeIdtoAccountMap = PCIdtoAccountMap(filteredPromos);
		List<Account> AccountsToUpdate = new List<Account>();
		
		for(Promo_Code__c pc : filteredPromos)
		{
			if(!PromoCodeIdtoAccountMap.isEmpty() && PromoCodeIdtoAccountMap.containsKey(pc.Id))
			{
				Account acct = PromoCodeIdtoAccountMap.get(pc.Id);				
				acct.Promo_Code__c = pc.Id;
				AccountsToUpdate.add(acct);				
			}
		}
		try
		{
			update AccountsToUpdate;
		}
		catch(DMLException e)
		{
			for(Account account : AccountsToUpdate)
			{
				account.addError('There was a problem updating the accounts');
			}
		}
	}
	
	public static list<Promo_Code__c> filterPromoCodesonUpdate(List<Promo_Code__c> newPromos)
	{
		List<Promo_Code__c> filteredPromos = new List<Promo_Code__c> ();
		
		for(Promo_Code__c pc : newPromos)
		{
			
			if(pc.Account__c != null)
			{
				filteredPromos.add(pc);
			}
		}
		return filteredPromos;
	}	
	
	private static Map<Id, Account> PCIdtoAccountMap(List<Promo_Code__c> filteredPromos)
	{
		
		List<Id> AccountIds = new List<Id>();
		
		for(Promo_Code__c pc : filteredPromos)
		{
			if(pc.Account__r.Promo_Code__c == null || pc.Account__r.Promo_Code__c != pc.Id)
			{
				AccountIds.add(pc.Account__c);
			}
		}
		
		List<Account> accountsToUpdate = [SELECT Id, Promo_Code__c FROM Account WHERE Id IN : accountIds];
		Map<Id, Account> PromoCodeIdtoAccount = new Map <Id, Account>();
		
		for(Promo_Code__c pc : filteredPromos)
		{
			if(!accountsToUpdate.isEmpty())
			{				
				for(Account acct : accountsToUpdate)
				{
					if(pc.Account__c == acct.Id)
					{
						PromoCodeIdtoAccount.put(pc.Id, acct);
					}
				}
			}
		}
		return PromoCodeIdtoAccount;
	}

}