@isTest

public class App_Pass_Fail_Tests {
     static /*testMethod*/ void App_Pass_Fail_Test1() {
    
     Account acct = new Account();
        acct.name = 'RPTestAcct1';
        acct.accountmanager__c = 'BIO Automation';
        Insert acct;   
        
     Opportunity op = new Opportunity (
            Name = 'Naming Convention WFR',
            AccountId = acct.Id,
            RecordTypeId = '012000000004wVU',
            StageName = 'Finance Approved',
            CloseDate = system.Today(),
            Type = 'Fee',
            OwnerId = '00500000007286zAAA',
            CampaignId = '701000000004fLn',
            Product__c = 'Certification',
            SSC_Volume__c = 'Tier 4',
            Pricebook2Id = '01s000000004NS6AAM',
            Certification_Type__c = 'Application Fee');
            
            insert op;
            
           
            
        OpportunityLineItem opl = new OpportunityLineItem(
            OpportunityId = op.Id,
            Quantity = 1,
            UnitPrice = 100,
            Product_Status__c = 'Current',
            Description__c = 'Return Path Certification',
            Active__c = true,
            Recurring__c = false,
            PricebookentryId = '01u00000000FeX2AAK');
            
            insert opl;      
    
     
     Certification_Application__c ca = new Certification_Application__c();
         ca.Application_Result__c = 'Fail';
         ca.Account__c = acct.Id;
         ca.Application_Fee_Opportunity__c = op.Id;
         ca.Certification_App_Fee__c = 123456;
         ca.Certification_Volume__c = 'Tier 4';
         insert ca;
         }

    static /*testMethod*/ void App_Pass_Fail_Test2() {
     Account acct = new Account();
        acct.name = 'RPTestAcct1';
        acct.accountmanager__c = 'BIO Automation';
        Insert acct;   
        
     Opportunity op = new Opportunity (
            Name = 'Naming Convention WFR',
            AccountId = acct.Id,
            RecordTypeId = '012000000004wVU',
            StageName = 'Finance Approved',
            CloseDate = system.Today(),
            Type = 'Fee',
            OwnerId = '00500000007286zAAA',
            CampaignId = '701000000004fLn',
            Product__c = 'Certification',
            SSC_Volume__c = 'Tier 4',
            Pricebook2Id = '01s000000004NS6AAM',
            Certification_Type__c = 'Application Fee');
            
            insert op;
            
           
            
        OpportunityLineItem opl = new OpportunityLineItem(
            OpportunityId = op.Id,
            Quantity = 1,
            UnitPrice = 100,
            Product_Status__c = 'Current',
            Description__c = 'Return Path Certification',
            Active__c = true,
            Recurring__c = false,
            PricebookentryId = '01u00000000FeX2AAK');
            
            insert opl;      
    
     
     Certification_Application__c ca = new Certification_Application__c();
         ca.Application_Result__c = 'Pass';
         ca.Application_Status__c = 'Audit Complete';
         ca.Account__c = acct.Id;
         ca.Application_Fee_Opportunity__c = op.Id;
         ca.Certification_App_Fee__c = 123456;
         ca.Certification_Volume__c = 'Tier 4';
         ca.Promo_Type_Picklist__c = 'Walk-Up';
         ca.Locked__c = 'Yes';
         ca.Early_Activation__c = False;
         insert ca;
         }
    
    
      static /*testMethod*/ void App_Pass_Fail_Test3() {
     
     User user = new user(Alias = 'standt', Email='tes@rp.com', 
      EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
      LocaleSidKey='en_US', ProfileId = '00e0000000734LT', 
      TimeZoneSidKey='America/Los_Angeles', UserName='testCMtCAT@rp.com.rp');
insert user;
     
     Account acct = new Account(
        Name = 'RPTestAcct1',
        OwnerId = user.Id,
        accountmanager__c = 'BIO Automation');
        Insert acct; 
        
      Account Pacct = new Account(
        Name = 'RPTestPartner',
        OwnerId = user.Id,
        CRM__c = user.Id,
        accountmanager__c = 'BIO Automation');
        Insert Pacct;
        
    Promo_Code__c pc = new Promo_Code__c();
        pc.Name = 'TEST001';
        pc.Account__c = pacct.Id;
        pc.Promo_Type__c = 'Referral';
        insert pc;
        
        pacct.Promo_Code__c = pc.Id;
        update pacct;        
        
     Opportunity op = new Opportunity (
            Name = 'Naming Convention WFR',
            AccountId = acct.Id,
            RecordTypeId = '012000000004wVU',
            StageName = 'Finance Approved',
            CloseDate = system.Today(),
            Type = 'Fee',
            OwnerId = user.Id,
            CampaignId = '701000000004fLn',
            Product__c = 'Certification',
            SSC_Volume__c = 'Tier 4',
            Pricebook2Id = '01s000000004NS6AAM',
            Certification_Type__c = 'Application Fee');
            
            insert op;
            
           
            
        OpportunityLineItem opl = new OpportunityLineItem(
            OpportunityId = op.Id,
            Quantity = 1,
            UnitPrice = 100,
            Product_Status__c = 'Current',
            Description__c = 'Return Path Certification',
            Active__c = true,
            Recurring__c = false,
            PricebookentryId = '01u00000000FeX2AAK');
            
            insert opl;      
    
     
     Certification_Application__c ca = new Certification_Application__c();
         ca.Application_Result__c = 'In Progress';
         ca.Application_Status__c = 'Under Review';
         ca.OwnerId = user.Id;
         ca.Activation_Date__c = system.today();
         ca.Account__c = acct.Id;
         ca.Application_Fee_Opportunity__c = op.Id;
         ca.Certification_App_Fee__c = 123;
         ca.Certification_Volume__c = 'Tier 4';
         ca.Promo_Type_Picklist__c = 'Referral';
         ca.Locked__c = 'Early Activation';
         ca.Early_Activation__c = False;
         ca.Partner_Account__c = Pacct.Id;
         ca.Partner_CRM__c = Pacct.CRM__c;
         insert ca;

         }  
    
    
     static /*testMethod*/ void App_Pass_Fail_Test4() {
     
     User user = new user(Alias = 'standt', Email='tes@rp.com', 
      EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
      LocaleSidKey='en_US', ProfileId = '00e0000000734LT', 
      TimeZoneSidKey='America/Los_Angeles', UserName='testCMtCAT@rp.com.rp');
insert user;
     
     Account acct = new Account(
        Name = 'RPTestAcct1',
        OwnerId = user.Id,
        accountmanager__c = 'BIO Automation');
        Insert acct; 
        
      Account Pacct = new Account(
        Name = 'RPTestPartner',
        OwnerId = user.Id,
        accountmanager__c = 'BIO Automation');
        Insert Pacct;    
        
     Opportunity op = new Opportunity (
            Name = 'Naming Convention WFR',
            AccountId = acct.Id,
            RecordTypeId = '012000000004wVU',
            StageName = 'Finance Approved',
            CloseDate = system.Today(),
            Type = 'Fee',
            OwnerId = user.Id,
            CampaignId = '701000000004fLn',
            Product__c = 'Certification',
            SSC_Volume__c = 'Tier 4',
            Pricebook2Id = '01s000000004NS6AAM',
            Certification_Type__c = 'Application Fee');
            
            insert op;
            
           
            
        OpportunityLineItem opl = new OpportunityLineItem(
            OpportunityId = op.Id,
            Quantity = 1,
            UnitPrice = 100,
            Product_Status__c = 'Current',
            Description__c = 'Return Path Certification',
            Active__c = true,
            Recurring__c = false,
            PricebookentryId = '01u00000000FeX2AAK');
            
            insert opl;      
    
     
     Certification_Application__c ca = new Certification_Application__c();
         ca.Application_Result__c = 'Pass';
         ca.Application_Status__c = 'Audit Complete';
         ca.OwnerId = user.Id;
         ca.Account__c = acct.Id;
         ca.Application_Fee_Opportunity__c = op.Id;
         ca.Certification_App_Fee__c = 123;
         ca.Certification_Volume__c = 'Tier 4';
         ca.Promo_Type_Picklist__c = 'Bundle';
         ca.Locked__c = 'Yes';
         ca.Early_Activation__c = False;
         ca.Partner_Account__c = Pacct.Id;
         insert ca;

         }  
        
    


}