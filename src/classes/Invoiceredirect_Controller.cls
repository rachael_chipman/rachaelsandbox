public class Invoiceredirect_Controller {
 
 private pageReference p;

Public Pagereference direct(){
 c2g__codaInvoice__c si = [select Id, c2g__Opportunity__c, c2g__InvoiceDate__c from c2g__codaInvoice__c where Id = :ApexPages.currentPage().getParameters().get('id')];
 
 if(si.c2g__Opportunity__c != null){p = new PageReference('/apex/DirectInvoice?id='+si.Id);}   
 else{p = new PageReference('/apex/DirectInvoiceNoOpp?id='+si.Id);}

    p.setRedirect(true);
        return p;
}

}