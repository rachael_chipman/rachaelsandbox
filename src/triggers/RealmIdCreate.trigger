trigger RealmIdCreate on Opportunity (after update) {
    list<Opportunity> UpdateOpps = new list<Opportunity>();
    list<RealmId__c> Realms = new list<RealmId__c>();
    
    for(Opportunity o1 : [select id, realmid__c, accountid, realm_id_created__c from opportunity where id in :trigger.new]){
        if(o1.realmid__c <> null && o1.Realm_Id_Created__c <> true){
            RealmId__c R = new RealmId__c();
            R.Name = o1.realmid__c;
            R.Account__c = o1.accountid;
            R.Opportunity__c = o1.id;
            Realms.add(R);
            Opportunity newopp = new Opportunity(id = o1.id,Realm_Id_Created__c = true);
            UpdateOpps.add(newopp);
        }
    }
    if(Realms.size() > 0) insert Realms;
    if(UpdateOpps.size() > 0) update UpdateOpps;
}