public with sharing class ExLogServices {
	public static final String CONFIG_SETTINGS_MISSING = 'Could not find Exception Logger configuration setting record named {0}';

	public static ExLog.ExLogConfig retrieveConfig( String customSettingName ) {
		
		Map<String, Ex_Log_Config__c> allExLogConfigs = Ex_Log_Config__c.getAll();
		
		if ( !allExLogConfigs.containsKey( customSettingName ) )
			throw new ExLog.ExLogException( String.format( CONFIG_SETTINGS_MISSING, new List<String>{ customSettingName } ) );
		
		ExLog.ExLogConfig soughtConfig = new ExLog.ExLogConfig();

		Ex_Log_Config__c selectedExLogConfig = allExLogConfigs.get( customSettingName );
		
		if ( selectedExLogConfig.Logging_Level__c != null && selectedExLogConfig.Logging_Level__c.trim() != '' ) {
			
			String upperLevel = selectedExLogConfig.Logging_Level__c.toUpperCase();
			
			for ( ExLog.Level aLogLevel : ExLog.Level.values() ) {
				
				if ( aLogLevel.name().toUpperCase() == upperLevel ) {
					
					soughtConfig.level = aLogLevel;
					
					break;
			
				}
				
			}
		
		}
		
		if ( selectedExLogConfig.Notification_Recips__c != null && selectedExLogConfig.Notification_Recips__c.trim() != '' )
			soughtConfig.peopleToEmail = selectedExLogConfig.Notification_Recips__c.split( '\\n' );
		
		soughtConfig.flushable = selectedExLogConfig.Flushable__c;

		return soughtConfig;
		
	}
}