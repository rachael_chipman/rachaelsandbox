trigger ChannelChildPush on Opportunity (after update) {
    list<Opportunity> DirectOppstoUpdate = new list<Opportunity>();
    list<Opportunity> ChildOppstoUpdate = new list<Opportunity>();
    list<Id> ChildIds = new list<Id>{};
    list<Opportunity> DirectOpps = new list<Opportunity>();

    for(Opportunity o : trigger.new){
        if(o.Channel_Child_Opp__c <> null && o.Direct_Child_Link_Approved__c == 'Link Approved'){
           ChildIds.add(o.Channel_Child_Opp__c);
           DirectOpps.add(o);
        }    
    }

    map<Id,Opportunity> ChildOpps = new map<Id,Opportunity>([select id, CRM_User__c, CloseDate,
        parent_realm_id__c, DAS_Account_Status__c, realmid__c, RealmID_Display_Name__c
        from Opportunity where id in :ChildIds]);

    for(Opportunity o2 : DirectOpps){

        Opportunity placeholderopp = new Opportunity(id=o2.id);
        placeholderopp.realmid__c = ChildOpps.get(o2.Channel_Child_Opp__c).realmid__c;
        placeholderopp.parent_realm_id__c = ChildOpps.get(o2.Channel_Child_Opp__c).parent_realm_id__c;
        placeholderopp.DAS_Account_Status__c = ChildOpps.get(o2.Channel_Child_Opp__c).DAS_Account_Status__c;
        placeholderopp.RealmID_Display_Name__c = ChildOpps.get(o2.Channel_Child_Opp__c).RealmID_Display_Name__c;
        placeholderopp.CloseDate = ChildOpps.get(o2.Channel_Child_Opp__c).CloseDate;
        placeholderopp.Direct_Child_Link_Approved__c = 'Push Completed';
        DirectOppstoUpdate.add(placeholderopp);

        Opportunity childupdate = new Opportunity(id = o2.Channel_Child_Opp__c);
        childupdate.stagename = 'Lost';
        childupdate.Reasons_for_Lost_Opportunities__c = 'Direct Opp Populated';
        ChildOppstoUpdate.add(childupdate);
    }

    update DirectOppstoUpdate;
    update ChildOppstoUpdate;
}