public class tabViewExtension {
    
    private final Opportunity opp;
    public RecordType recType
    {
        get
        {
            if(recType == NULL)
            {
                recType = [SELECT Id FROM RecordType WHERE SObjectType = 'Opportunity' AND DeveloperName = 'Pre_Pipeline'];
            }
            return recType;
        }
        private set;
    }

    public String profileName
    {
        get
        {
            Id profileId = UserInfo.getProfileId();
            if(profileName == NULL)
            {
                profileName = [SELECT Id, Name FROM Profile WHERE Id = :profileId].Name;
            }
            return profileName;
        }
        private set;
    }
        
    public tabViewExtension(ApexPages.StandardController stdController) {
        this.opp = (Opportunity)stdController.getRecord();
    }


}