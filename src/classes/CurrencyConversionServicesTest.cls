@isTest
private class CurrencyConversionServicesTest 
{
	static List<Opportunity> testOpportunities;
	static final String USD = 'USD';
	static final String EUR = 'EUR';

	@isTest( seeAllData = true )
	static void testGetAllCurrencies() 
	{
		c2g__codaGeneralLedgerAccount__c testGeneralLedgerAccount = TestUtils.createGeneralLedgerAccount(true);
        Account testAccount = TestUtils.createAccount_FinancialForce( TestUtils.MARKETER_RECORD_TYPE.Id, testGeneralLedgerAccount.Id, false );
        testAccount.CurrencyISOCode = USD;
        Account testPartner = TestUtils.createAccount_FinancialForce( TestUtils.PARTNER_RECORD_TYPE.Id, testGeneralLedgerAccount.Id, false );
        testPartner.CurrencyISOCode = EUR;
        insert new List<Account>{ testAccount, testPartner };

        RecordType opportunityRecordType = [ SELECT Id FROM RecordType WHERE SObjectType = 'Opportunity' AND IsActive = TRUE LIMIT 1 ];
        Opportunity testOpportunity = TestUtils.createOpportunity( opportunityRecordType.Id, testAccount.Id, false);
        testOpportunity.PartnerLookup__c = testPartner.Id;
        testOpportunity.CurrencyISOCode = USD;
        insert testOpportunity;

		test.startTest();
			Map<String, c2g__codaAccountingCurrency__c> currencyISOCodeToAccountingCurrency = CurrencyConversionServices.getAllCurrencies( new List<Opportunity>{ testOpportunity }, new Map<Id, Account>{ testOpportunity.Id => testPartner } );
		test.stopTest();

		System.assertEquals( 2, currencyISOCodeToAccountingCurrency.size(), 'Accounting currency for opp and partner currency iso codes should be returned' );
		System.assert( currencyISOCodeToAccountingCurrency.keySet().containsAll( new Set<String>{ USD, EUR } ), 'Accounting currency for opp and partner currency iso codes should be returned' );
		System.assertEquals( USD, currencyISOCodeToAccountingCurrency.get( USD ).Name, 'The accounting currency should be mapped to ' );
		System.assertEquals( EUR, currencyISOCodeToAccountingCurrency.get( EUR ).Name, 'The accounting currency should be mapped to ' );

	}

	@isTest( seeAllData = true )
	public static void testGetOpportunityRate()
    {
        final String AUD = 'AUD';
        final Date invoiceDate1 = Date.newInstance( Date.today().year()-1, 06, 05 );
        final Date invoiceDate2 = invoiceDate1.addMonths( 2 );
        final Date invoiceDate3 = invoiceDate1.addMonths( -2 );

        final Integer numToCreate = 3;
        RecordType accountRecordtype = [ SELECT Id FROM RecordType WHERE SObjectType = 'Account' AND IsActive = TRUE LIMIT 1 ];
        Account opportunityAccount = TestUtils.createAccount( accountRecordtype.Id, true );

        RecordType opportunityRecordType = [ SELECT Id FROM RecordType WHERE SObjectType = 'Opportunity' AND IsActive = TRUE LIMIT 1 ];
        testOpportunities = TestUtils.createOpportunities( numToCreate, opportunityRecordType.Id, opportunityAccount.Id, false );
        Opportunity testOpp_USD = testOpportunities[0];
        testOpp_USD.CurrencyISOCode = USD;
        Opportunity testOpp_EUR = testOpportunities[1];
        testOpp_EUR.CurrencyISOCode = EUR;
        Opportunity testOpp_AUD = testOpportunities[2];
        testOpp_AUD.CurrencyISOCode = AUD;
        insert testOpportunities;
        Set<String> curIsoCodes = new Set<String>{ USD, EUR, AUD };

        List<c2g__codaAccountingCurrency__c> accountingCurrencies = [SELECT Id, Name, CurrencyIsoCode from c2g__codaAccountingCurrency__c where Name IN :curIsoCodes and c2g__OwnerCompany__c = :OpportunityServices.RETURN_PATH_COMPANY.Id];
        System.assertEquals( 3, accountingCurrencies.size(), 'All the expected accounting currencies should be returned' + accountingCurrencies );
        Map<Id, c2g__codaAccountingCurrency__c> oppIdToCurrency = new Map<Id, c2g__codaAccountingCurrency__c>();
        Map<Id, Date> oppIdToInvoiceDate = new Map<Id, Date>();

        for( c2g__codaAccountingCurrency__c acctCurrency : accountingCurrencies )
        {
            if( acctCurrency.CurrencyISOCode == USD )
            {
                oppIdToCurrency.put( testOpp_USD.Id, acctCurrency );
                oppIdToInvoiceDate.put( testOpp_USD.Id, invoiceDate1 );
            }
            else if( acctCurrency.CurrencyISOCode == EUR )
            {
                oppIdToCurrency.put( testOpp_EUR.Id, acctCurrency );
                oppIdToInvoiceDate.put( testOpp_EUR.Id, invoiceDate2 );

            }
            else if( acctCurrency.CurrencyISOCode == AUD )
            {
                oppIdToCurrency.put( testOpp_AUD.Id, acctCurrency );
                oppIdToInvoiceDate.put( testOpp_AUD.Id, invoiceDate3 );
            }
        }

        Decimal expectedRate_USD = 1;
        c2g__codaExchangeRate__c expectedRate_EUR = [SELECT Id, c2g__Rate__c, c2g__Startdate__c FROM c2g__codaExchangeRate__c WHERE c2g__ExchangeRateCurrency__r.Name = :EUR 
                                    AND c2g__ExchangeRateCurrency__r.c2g__OwnerCompany__c = :OpportunityServices.RETURN_PATH_COMPANY.Id 
                                    AND c2g__Startdate__c <= :invoiceDate2 ORDER BY c2g__Startdate__c desc LIMIT 1][0];
        c2g__codaExchangeRate__c expectedRate_AUD = [SELECT Id, c2g__Rate__c, c2g__Startdate__c FROM c2g__codaExchangeRate__c WHERE c2g__ExchangeRateCurrency__r.Name = :AUD 
                                    AND c2g__ExchangeRateCurrency__r.c2g__OwnerCompany__c = :OpportunityServices.RETURN_PATH_COMPANY.Id
                                    AND c2g__Startdate__c <= :invoiceDate3 ORDER BY c2g__Startdate__c desc LIMIT 1][0];
        test.startTest();
            Map<String, Map<Date, c2g__codaExchangeRate__c>> currencyISOCodeToExRate = CurrencyConversionServices.getAllExchangeRates( oppIdToInvoiceDate, Pluck.ids( accountingCurrencies ) );
            Map<Id, Decimal> oppIdToExRate = CurrencyConversionServices.getOpportunityRate( testOpportunities, currencyISOCodeToExRate, oppIdToInvoiceDate );
        test.stopTest();

        System.assertEquals( 3, oppIdToExRate.size(), 'All the 3 opportunity should be returned with their exchange rates' );
        System.assertEquals( expectedRate_USD, oppIdToExRate.get( testOpp_USD.Id ), 'The opportunity should have the expected exchange rate' );
        System.assertEquals( expectedRate_AUD.c2g__Rate__c, oppIdToExRate.get( testOpp_AUD.Id ), 'The opportunity should have the expected exchange rate' + expectedRate_AUD.c2g__Startdate__c);
        System.assertEquals( expectedRate_EUR.c2g__Rate__c, oppIdToExRate.get( testOpp_EUR.Id ), 'The opportunity should have the expected exchange rate' + expectedRate_EUR.c2g__Startdate__c);

    }
	
}