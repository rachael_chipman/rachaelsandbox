global class BatchToProcessClonedPPCs implements Database.Batchable<SObject>, Database.Stateful, Schedulable
{

	public static final String BATCH_NAME = 'BatchToProcessClonedPPCs';
    public static final String[] ERROR_EMAILS = new String[]{ 'salesforceplatform@returnpath.com' };
	

    global String errors = '';
	
	
    global void execute( SchedulableContext sc )
    {
        BatchToProcessClonedPPCs batch = new BatchToProcessClonedPPCs();
        Database.executeBatch( batch );
    }

    global Database.QueryLocator start( Database.BatchableContext BC )
    {
        return Database.getQueryLocator(CustomerProductDetailCloneServices.buildQuery());
    }

    global void execute( Database.BatchableContext BC, List<sObject> scope )
    {
    	try
    	{
    		CustomerProductDetailCloneServices.processOpportunities(scope);
    	}
		
		catch(DMLException dmx)
		{  
		   for( Integer index = 0; index < dmx.getNumDml(); index++ )
           {
              errors += dmx.getDmlMessage(index);
           }
		}
    }

    global void finish( Database.BatchableContext BC )
    {

	     AsyncApexJob job = [SELECT Id, Status, NumberOfErrors,
	                         JobItemsProcessed, TotalJobItems,
	                         CreatedBy.Email
	                         FROM AsyncApexJob
	                         WHERE Id =:bc.getJobId()];

	     Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();

	     mail.setToAddresses( ERROR_EMAILS );
	     mail.setSubject( 'Batch to ' + BATCH_NAME + ' ' + job.Status );

	     String emailBody = 'Batch to ' + BATCH_NAME + ' processed '
	                      + job.TotalJobItems
	                      + ' batches with '
	                      + job.NumberOfErrors
	                      + ' failures.';
	     if( errors != '' )
	     {
		     emailBody += '\n\n\nThe following errors occured:\n'+ errors;
		     mail.setPlainTextBody( emailBody );
		     Messaging.sendEmail( new Messaging.SingleEmailMessage[]{ mail } );
	     }

    }
}