public class GreenSheetController {

        public  Opportunity opp{get;set;}
        public Boolean editMode{get;set;}
        public list<MilestoneWrapper> milestones{get;set;}
        public Milestone__c milestone{get;set;}
        public list<RiskWrapper> risks{get;set;}
        public Risk_To_Close__c risk{get;set;}
        public String searchText{get;set;}
        public list<Contact> accountContacts{get;set;}
        public list<String> selectedContacts{get;set;}
        public list<Milestone__c> milestonesToInsert{get;set;}
        public list<Risk_To_Close__c> risksToInsert{get;set;}
        public Boolean recordLocked{get;set;}
    	public list<Milestone__c> milestonesToDelete{get;set;}
    	public list<Risk_To_Close__c> risksToDelete{get;set;}
        public list<Milestone__c> newMilestones{get;set;}
        public list<Risk_To_Close__c> newRisks{get;set;}

        public GreenSheetController( ApexPages.Standardcontroller std )
        {
            editMode = false;
            
            selectedContacts = new list<String>();


            opp = [SELECT Id, Name, Owner.Name, CloseDate, AccountId, Contract_Paper__c, Will_the_Contract_need_Legal_Review__c, Are_you_engaged_with_the_Decision_Maker__c, Customer_Decision_Process__c, Customer_Approval_Process__c,
                    Customer_Signature_Process__c, Partner_3rd_Party_Signature_Process__c, Green_Sheet_Contacts__c, Help_Needed_To_Close__c, PartnerLookup__c, Compelling_Event_to_Close__c
                    FROM Opportunity
                    WHERE Id = :ApexPages.currentPage().getParameters().get('id')];

            recordLocked = doesOpportunityHavePendingApproval();

            milestones = new list<MilestoneWrapper>();
            milestonesToDelete = new list<Milestone__c>();
            newMilestones = new list<Milestone__c>();

            risks = new list<RiskWrapper>();
            risksToDelete = new list<Risk_To_Close__c>();
            newRisks = new list<Risk_To_Close__c>();
            
            queryRecords();

            if(opp.Green_Sheet_Contacts__c != NULL)
            {
                selectedContacts = opp.Green_Sheet_Contacts__c.split(';');
            }
            else
            {
                opp.Green_Sheet_Contacts__c = '';
            }

            insertBlankMilestonesAndRisks( milestones, risks);
        }

        public Boolean doesOpportunityHavePendingApproval()
        {
            return ![ Select Id From ProcessInstance WHERE TargetObjectId =: opp.Id AND Status = 'Pending' ].isEmpty();
        }

        public void insertBlankMilestonesAndRisks(list<MilestoneWrapper> queriedMilestones, list<RiskWrapper> queriedRisksToClose)
        {
            if( queriedMilestones.size() < 4 || queriedRisksToClose.size() < 4)
            {
                for( Integer i = queriedMilestones.size(); i < 3; i++ )
                {
                    Milestone__c milestone = new Milestone__c();
                    milestone.Opportunity__c = opp.Id;
                    milestone.Due_Date__c = system.today();
                    milestones.add( new MilestoneWrapper(milestone, false ));
                }

                for( Integer i = queriedRisksToClose.size(); i < 3; i++ )
                {
                    Risk_To_Close__c risk = new Risk_To_Close__c();
                    risk.Opportunity__c = opp.Id;
                    risks.add( new RiskWrapper( risk, false ));
                }
            }
        }
    
    	public void queryRecords()
        {
            list<Milestone__c> queriedMilestones = [SELECT Id, Step_To_Close__c, Due_Date__c, Opportunity__c, Completed__c FROM Milestone__c WHERE Opportunity__c = :opp.Id];
            for(Milestone__c milestone : queriedMilestones)
            {
                milestones.add(new MilestoneWrapper(milestone, false));
            }
            
            list<Risk_To_Close__c> queriedRisksToClose = [SELECT Id, Type_of_Risk__c, Mitigation_Plan__c, Risk_To_Close__c, Opportunity__c FROM Risk_To_Close__c WHERE Opportunity__c = :opp.Id];
            for(Risk_To_Close__c risk : queriedRisksToClose)
            {
                risks.add(new RiskWrapper(risk, false));
            } 
        }

        public pageReference editGreenSheet()
        {
            if(opp.CloseDate >= system.today())
            {
                editMode = true;
                for(MilestoneWrapper milestone : milestones)
                {
                    milestone.isEdit = true;
                }
                for(RiskWrapper risk : risks)
                {
                    risk.isEdit = true;
                }
                return null;
            }
            else
            {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,' The Close Date for open Opportunities may not be earlier than today. Please change the Close Date to today or a date in the future.'));                
            }
            return null;
        }
        
        public pageReference cancelGreenSheet()
        {
            editMode = false;
            for(MilestoneWrapper milestone : milestones)
            {
                milestone.isEdit = false;
            }
            for(RiskWrapper risk : risks)
            {
                risk.isEdit = false;
            }
            
            milestones.clear();
            risks.clear();
            queryRecords();
            
            return new PageReference('/' + ApexPages.currentPage().getParameters().get('id'));
        }

        public pageReference saveGreenSheet()
        {
            update opp;
            List<Milestone__c> mlist = new List<Milestone__c>();
            List<Risk_to_Close__c> riskList = new List<Risk_To_Close__c>();
            editMode = false;
            Map<Id, Milestone__c> msDeleteOrNewMap = new Map<Id, Milestone__c>();
                      	
            for(Milestone__c forDelete : milestonesToDelete)
            {
               msDeleteOrNewMap.put(forDelete.Id, forDelete);
            }
            for(Milestone__c newMstone : newMilestones)
            {
                msDeleteOrNewMap.put(newMstone.Id, newMstone);
            }
            
            Map<Id, Risk_To_Close__c> riskDeleteOrNewMap = new Map<Id, Risk_To_Close__c>();
           	
            for(Risk_To_Close__c forDelete : risksToDelete)
            {
               riskDeleteOrNewMap.put(forDelete.Id, forDelete);
            }
            for(Risk_To_Close__c newRisk : newRisks)
            {
                riskDeleteOrNewMap.put(newRisk.Id, newRisk);
            }
            
            
            for(MilestoneWrapper milestoneWrap : milestones)
            {
                milestoneWrap.isEdit = false;
                if(!msDeleteOrNewMap.containsKey(milestoneWrap.milestone.Id))
                {
                    mlist.add(milestoneWrap.milestone);
                }
            }
            for(RiskWrapper riskWrap : risks)
            {
                riskWrap.isEdit = false;
                if(!riskDeleteOrNewMap.containsKey(riskWrap.risk.Id))
                {
                     riskList.add(riskWrap.risk);
                }  
            }
            try
            {
                upsert mlist;
                upsert riskList;
                insert newMilestones;
                insert newRisks;
                delete milestonesToDelete;
                milestonesToDelete.clear();
                delete risksToDelete;
                risksToDelete.clear();
                return null;
            }
            catch (Exception e)
            {
                ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.ERROR, 'There was an issue saving the Green Sheet. Please contact your administrator.'));
            }
            return null;
        }

        public void addNewMilestone()
        {
            Milestone__c milestone = new Milestone__c();
            milestone.Opportunity__c = opp.Id;
            milestone.Due_Date__c = system.today();
            newMilestones.add(milestone);
            milestones.add(new MilestoneWrapper(milestone, true));

            return;
        }

        public class MilestoneWrapper
        {
            public Boolean isEdit{get;set;}
            public Milestone__c milestone{get;set;}

            public MilestoneWrapper(Milestone__c milestone, Boolean isEdit)
            {
                this.milestone = milestone;
                this.isEdit = isEdit;
            }
        }

        public void editMilestone()
        {
             Integer selectedWrapperIndex = Integer.valueOf(ApexPages.currentPage().getParameters().get('edit'));
             milestones[selectedWrapperIndex].isEdit = true;
        }

        public void saveMilestone()
        {
            Integer selectedWrapperIndex = Integer.valueOf(ApexPages.currentPage().getParameters().get('save'));
            upsert milestones[selectedWrapperIndex].milestone;
            milestones[selectedWrapperIndex].isEdit = false;
        }

        public void deleteMilestone()
        {
            Integer selectedWrapperIndex = Integer.valueOf(ApexPages.currentPage().getParameters().get('remove'));
            if(milestones[selectedWrapperIndex].milestone.Id != null)
            {
                milestonesToDelete.add(milestones[selectedWrapperIndex].milestone);
            }
            milestones.remove(selectedWrapperIndex);
        }

        public void addNewRisk()
        {
            Risk_To_Close__c risk = new Risk_To_Close__c();
            risk.Opportunity__c = opp.Id;
            newRisks.add(risk);
            risks.add(new RiskWrapper(risk, true));

            return;
        }

        public class RiskWrapper
        {
            public Boolean isEdit{get;set;}
            public Risk_To_Close__c risk{get;set;}

            public RiskWrapper(Risk_To_Close__c risk, Boolean isEdit)
            {
                this.risk = risk;
                this.isEdit = isEdit;
            }
        }

        public void editRisk()
        {
            Integer selectedRiskWrapperIndex = Integer.valueOf(ApexPages.currentPage().getParameters().get('editRisk'));
            risks[selectedRiskWrapperIndex].isEdit = true;
        }

        public void saveRisk()
        {
            Integer selectedRiskWrapperIndex = Integer.valueOf(ApexPages.currentPage().getParameters().get('saveRisk'));
            upsert risks[selectedRiskWrapperIndex].risk;
            risks[selectedRiskWrapperIndex].isEdit = false;
        }

        public void deleteRisk()
        {
            Integer selectedRiskWrapperIndex = Integer.valueOf(ApexPages.currentPage().getParameters().get('removeRisk'));
            if(risks[selectedRiskWrapperIndex].risk.Id != null)
            {
                risksToDelete.add(risks[selectedRiskWrapperIndex].risk);
            }
            risks.remove(selectedRiskWrapperIndex);
        }

        public void addContact()
        {
            Id contactId = ApexPages.currentPage().getParameters().get('contactId');
            List<String> greenSheetContacts = new list<String>();
            system.debug('What is Green Sheet Contacts?'+ opp.Green_Sheet_Contacts__c);
            if(opp.Green_Sheet_Contacts__c != NULL)
            {
                greenSheetContacts = opp.Green_Sheet_Contacts__c.split(';');
            }
            else
            {
                opp.Green_Sheet_Contacts__c = '';
            }
            for( Contact selectedContact : accountContacts )
            {
                if( contactId == selectedContact.Id )
                {
                    for(String contactName : greenSheetContacts)
                    {
                        if(selectedContact.Name+' - '+selectedContact.ContactsRole__c == contactName)
                        {
                            return;
                        }
                    }
                    opp.Green_Sheet_Contacts__c += selectedContact.Name + ' - ' + selectedContact.ContactsRole__c+';';
                    if( selectedContact.ContactsRole__c != NULL)
                    {
                        selectedContacts.add( selectedContact.Name + ' - ' + selectedContact.ContactsRole__c);
                    }
                    else
                    {
                        selectedContacts.add( selectedContact.Name );
                    }
                }
            }
            accountContacts.clear();
        }

        public void removeContact()
        {
             String contactName = ApexPages.currentPage().getParameters().get('selection');
             for( Integer i = 0; i < selectedContacts.size(); i++ )
             {
                 if( selectedContacts[i] == contactName )
                 {
                     selectedContacts.remove(i);
                 }
             }
             opp.Green_Sheet_Contacts__c = string.join(selectedContacts, ';');
        }

        public void searchContacts()
        {
            String searchStr = '%'+searchText+'%';
            accountContacts = [SELECT Name, ContactsRole__c FROM Contact WHERE AccountId != NULL AND Name LIKE :searchStr AND (AccountId = :opp.AccountId OR AccountId = :opp.PartnerLookup__c) ORDER BY Name ASC LIMIT 10];
        }
}