@isTest(SeeAllData = true) 

public class Find_Create_Cert_Opp_Tests {
static RecordType RT = [SELECT Id FROM RecordType WHERE DeveloperName = 'Cert_App' and SObjectType = 'Certification_Application__c' LIMIT 1];
static Campaign certCampaign;
    
static testMethod void Find_Create_Cert_OppTest() {
    
	certCampaign = new Campaign(Name = 'WEB: SSC Application Form', isActive = true);
	insert certCampaign;
    
     Account acct = new Account();
        acct.name = 'RPTestAcct1';
        acct.accountmanager__c = 'BIO Automation';
        Insert acct;   
        
     Opportunity op = new Opportunity (
            Name = 'Naming Convention WFR',
            AccountId = acct.Id,
           // RecordTypeId = '012000000004wVU',
            RecordTypeId = TestUtils.PURCHASE_RECORD_TYPE.Id,
            StageName = 'Signature',
            CloseDate = system.Today(),
            Type = 'New Business',
            OwnerId = '00500000007286zAAA',
            CampaignId = certCampaign.Id, //'701000000004fLn',
            Product__c = 'Certification',
            SSC_Volume__c = 'Tier 4',
            Pricebook2Id = '01s000000004NS6AAM',
            Certification_Type__c = 'Certification',
            Did_not_Fill_Defining_Need_Exit_Criteria__c = true,
       		Did_not_Fill_Pre_Pipeline_Exit_Criteria__c = true,
       		Did_not_Fill_Est_Value_Exit_Criteria__c = true,
       		Did_not_Fill_Nego_Terms_Exit_Criteria__c = true,
       		Did_not_Fill_Verbal_Commit_Exit_Criteria__c = true);
            
            insert op;
            
           
            
        OpportunityLineItem opl = new OpportunityLineItem(
            OpportunityId = op.Id,
            Quantity = 1,
            UnitPrice = 2500,
            Product_Status__c = 'Current',
            Description__c = 'Return Path Certification',
            Active__c = true,
            Recurring__c = false,
            PricebookentryId = '01u00000000FGwSAAW');
            
            insert opl;      
    
     
     Certification_Application__c ca = new Certification_Application__c();
         ca.Application_Status__c = 'Under Review';
         ca.Application_Result__c = 'In Progress';
         ca.Account__c = acct.Id;
         ca.Opportunity_Created__c = 'No';
         ca.Certification_App_Fee__c = 123456;
         ca.Certification_Volume__c = 'Tier 4';
         ca.Certification_Max_Volume__c = 50000;
         ca.Certification_Type__c = 'RP Certification';
         ca.Certification_Analyst__c = '005000000074sVo';
         ca.Bond_Group_Id__c = '1234567';
         ca.RecordTypeId = RT.Id;
         insert ca;
         }
         
     static testMethod void Find_Create_Cert_OppTest1() {
    
     Account acct = new Account();
        acct.name = 'RPTestAcct1';
        acct.accountmanager__c = 'BIO Automation';
        Insert acct;   
    
     
     Certification_Application__c ca = new Certification_Application__c();
         ca.Application_Status__c = 'Under Review';
         ca.Application_Result__c = 'In Progress';
         ca.Account__c = acct.Id;
         ca.Certification_App_Fee__c = 123456;
         ca.Promo_Type_Picklist__c = 'Walk-Up';
         ca.Certification_Volume__c = 'Tier 4';
         ca.Certification_Max_Volume__c = 50000;
         ca.Certification_Type__c = 'RP Certification';
         ca.Certification_Analyst__c = '005000000074sVo';
         ca.Bond_Group_Id__c = '1234567';
         ca.RecordTypeId = RT.Id;
         insert ca;
         }

    static testMethod void Find_Create_Cert_OppTest2() {
    Account acct = new Account();
        acct.name = 'RPTestAcct1';
        acct.accountmanager__c = 'BIO Automation';
        Insert acct;
        
    Account pacct = new Account();
        pacct.name = 'RPTestAcct1';
        pacct.CRM__c = '005000000074sVo';
        Insert pacct;
        
    Promo_Code__c pc = new Promo_Code__c();
        pc.Name = 'TEST001';
        pc.Account__c = pacct.Id;
        pc.Promo_Type__c = 'Referral';
        insert pc;
        
        pacct.Promo_Code__c = pc.Id;
        update pacct;    
        
     
     Certification_Application__c ca = new Certification_Application__c();
         ca.Application_Status__c = 'Under Review';
         ca.Application_Result__c = 'In Progress';
         ca.Account__c = acct.Id;
         ca.Certification_App_Fee__c = 123456;
         ca.Promo_Type_Picklist__c = 'Referral';
         ca.Partner_Account__c = pacct.Id;
         ca.Partner_CRM__c = pacct.CRM__c;
         ca.Certification_Volume__c = 'Tier 4';
         ca.Certification_Max_Volume__c = 50000;
         ca.Certification_Analyst__c = '005000000074sVo';
         ca.Certification_Type__c = 'RP Certification';
         ca.Bond_Group_Id__c = '1234567';
         ca.RecordTypeId = RT.Id;
         insert ca;
         }


}