@isTest (seeAllData = true)

public class ConverttoExtensionController_test{
    
    public static testMethod void testConverttoExtensionController1(){
    PageReference pageRef = Page.ConverttoExtensionPage;
    
    Test.setCurrentPageReference(pageRef);
    
       Account acct = new Account();
       acct.name = 'RPTestAcct1';
       insert acct;  
        
       Opportunity opp = new Opportunity();
       opp.Name = 'Test Opp';
       opp.AccountId = acct.Id;
       opp.CloseDate = system.today() + 7;
       opp.StageName = 'Signature';
       opp.Type = 'New Business';
       opp.Pricebook2Id = '01s000000004NS6AAM';
       insert opp;
       
       OpportunityLineItem oli = new OpportunityLineItem();
       oli.OpportunityId = opp.Id;
       oli.PricebookentryId = '01u00000000FeX2AAK';
       oli.UnitPrice = 100.00;
       oli.Start_Date__c = system.today();
       oli.End_Date__c = system.today()+365;
       oli.Quantity = 1;
       insert oli;
       
   ApexPages.StandardController sc = new ApexPages.standardController(opp);
   System.CurrentPageReference().getParameters().put('id',opp.Id);
   ConverttoExtensionController oPEE = new ConverttoExtensionController(sc);
   
   oPee.Convert();
   oPee.Redirect();
   
   }
   



    
    public static testMethod void testConverttoExtensionController2(){
    PageReference pageRef = Page.ConverttoExtensionPage;
    
    Test.setCurrentPageReference(pageRef);
    

    
       Account acct = new Account();
       acct.name = 'RPTestAcct1';
       insert acct;  
        
       Opportunity opp = new Opportunity();
       opp.Name = 'Test Opp';
       opp.AccountId = acct.Id;
       opp.CloseDate = system.today() + 7;
       opp.StageName = 'Signature';
       opp.Type = 'Renewal';
       opp.Pricebook2Id = '01s000000004NS6AAM';
       insert opp;
       
       OpportunityLineItem oli = new OpportunityLineItem();
       oli.OpportunityId = opp.Id;
       oli.PricebookentryId = '01u00000000FeX2AAK';
       oli.UnitPrice = 100.00;
       oli.Start_Date__c = system.today();
       oli.End_Date__c = system.today()+180;
       oli.Quantity = 1;
       insert oli;
       
   ApexPages.StandardController sc = new ApexPages.standardController(opp);
   System.CurrentPageReference().getParameters().put('id',opp.Id);
   ConverttoExtensionController oPEE = new ConverttoExtensionController(sc);
   
   oPee.Convert();
   oPee.Redirect();
   
   }
         



    
    public static testMethod void testConverttoExtensionController3(){
    PageReference pageRef = Page.ConverttoExtensionPage;
    
    Test.setCurrentPageReference(pageRef);

       Account acct = new Account();
       acct.name = 'RPTestAcct1';
       insert acct;  
        
       Opportunity opp = new Opportunity();
       opp.Name = 'Test Opp';
       opp.AccountId = acct.Id;
       opp.CloseDate = system.today() + 7;
       opp.StageName = 'Signature';
       opp.Type = 'Renewal';
       opp.Pricebook2Id = '01s000000004NS6AAM';
       insert opp;
       
       OpportunityLineItem oli = new OpportunityLineItem();
       oli.OpportunityId = opp.Id;
       oli.PricebookentryId = '01u00000000FeX2AAK';
       oli.UnitPrice = 100.00;
       oli.Start_Date__c = system.today();
       oli.End_Date__c = system.today()+365;
       oli.Quantity = 1;
       insert oli;
       
   ApexPages.StandardController sc = new ApexPages.standardController(opp);
   System.CurrentPageReference().getParameters().put('id',opp.Id);
   ConverttoExtensionController oPEE = new ConverttoExtensionController(sc);
   
   oPee.Convert();
   oPee.Redirect();
   
   }
           
}