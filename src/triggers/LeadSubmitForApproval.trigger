trigger LeadSubmitForApproval on Lead (after update) {

    List<Approval.Processsubmitrequest> reqList = new List<Approval.Processsubmitrequest>();

    for (Lead l: Trigger.new) {
    
        
     if ( l.Submit__c == true && l.Awaiting_Approval__c == false && l.OwnerId != AccountOwnerChange.ELOQUA.Id)  {       
     
            // create the new approval request to submit
           Approval.ProcessSubmitRequest req1 = new Approval.ProcessSubmitRequest();
           req1.setComments('Submitting request for approval.');
           req1.setObjectId(l.id);
           reqList.add(req1);
           //Approval.ProcessResult result = Approval.process(req1, false); 
           //System.assert(result.isSuccess()); 
           // submit the approval request for processing
           
 

 
        }

 
    }
    Approval.Processresult[] result = Approval.Process(reqList);
    

 
}