@isTest (seeAllData=true)
Public class PortalWizardSelectAcct_Controller_Test{
    public static testMethod void testPortalWizardSelectAcct_Controller(){
    PageReference pageRef = Page.PortalWizardSelectAcct;
Test.setCurrentPageReference(pageRef);

User user1;
Account portalAccount1;

UserRole portalRole = [Select Id From UserRole Where PortalType = 'None' Limit 1];
system.debug('portalRole is ' + portalRole);

Profile profile1 = [Select Id from Profile where name = 'Exec Admin'];
User portalAccountOwner1 = new User(
UserRoleId = portalRole.Id,
ProfileId = profile1.Id,
Username = System.now().millisecond() + 'test2@test.com',
Alias = 'batman',
Email='bruce.wayne@wayneenterprises.com',
EmailEncodingKey='UTF-8',
Firstname='Bruce',
Lastname='Wayne',
LanguageLocaleKey='en_US',
LocaleSidKey='en_US',
TimeZoneSidKey='America/Chicago',
Bypass_Validation_Rules__c = true
);
Database.insert(portalAccountOwner1);

//User u1 = [Select ID From User Where Id =: portalAccountOwner1.Id];

System.runAs ( portalAccountOwner1 ) {
//Create account

Account PartnerAccount1 = new Account(
Name = 'TestAccount Partner',
OwnerId = portalAccountOwner1.Id,
CRM__c = '00500000006olmF'
);
Database.insert(PartnerAccount1);

portalAccount1 = new Account(
Name = 'TestAccount',
ParentId = PartnerAccount1.Id,
OwnerId = portalAccountOwner1.Id
);
Database.insert(portalAccount1);

//Create contact
Contact contact1 = new Contact(
FirstName = 'Test',
Lastname = 'McTesty',
AccountId = portalAccount1.Id,
Email = System.now().millisecond() + 'test@test.com'
);
Database.insert(contact1);

//Create user
Profile portalProfile = [SELECT Id FROM Profile where Id = '00e000000073Tjc'];
user1 = new User(
Username = System.now().millisecond() + 'test12345@test.com',
ContactId = contact1.Id,
ProfileId = portalProfile.Id,
Alias = 'test123',
Email = 'test12345@test.com',
EmailEncodingKey = 'UTF-8',
LastName = 'McTesty',
CommunityNickname = 'test12345',
TimeZoneSidKey = 'America/Los_Angeles',
LocaleSidKey = 'en_US',
LanguageLocaleKey = 'en_US',
Bypass_Validation_Rules__c = true
);
Database.insert(user1);
}

System.runAs (user1){

System.currentPagereference().getParameters().put('acct', portalAccount1.Id);
System.currentPagereference().getParameters().put('type','Termination');
     
PortalWizardSelectAcct_Controller oPEE = new PortalWizardSelectAcct_Controller();  
oPEE.getopp();
oPEE.getacct();
oPEE.Next();

}}

    public static testMethod void testPortalWizardSelectAcct_Controller2(){
    PageReference pageRef = Page.PortalWizardSelectAcct;
Test.setCurrentPageReference(pageRef);

User user1;
Account portalAccount1;
Account portalAccount2;

UserRole portalRole = [Select Id From UserRole Where PortalType = 'None' Limit 1];
system.debug('portalRole is ' + portalRole);

Profile profile1 = [Select Id from Profile where name = 'Exec Admin'];
User portalAccountOwner1 = new User(
UserRoleId = portalRole.Id,
ProfileId = profile1.Id,
Username = System.now().millisecond() + 'test234@test.com',
Alias = 'batman',
Email='bruce.wayne@wayneenterprises.com',
EmailEncodingKey='UTF-8',
Firstname='Bruce',
Lastname='Wayne',
LanguageLocaleKey='en_US',
LocaleSidKey='en_US',
TimeZoneSidKey='America/Chicago',
Bypass_Validation_Rules__c = true
);
Database.insert(portalAccountOwner1);

//User u1 = [Select ID From User Where Id =: portalAccountOwner1.Id];

System.runAs ( portalAccountOwner1 ) {
//Create account

Account PartnerAccount1 = new Account(
Name = 'TestAccount Partner',
OwnerId = portalAccountOwner1.Id,
CRM__c = '00500000006olmF'
);
Database.insert(PartnerAccount1);

portalAccount1 = new Account(
Name = 'TestAccount',
ParentId = PartnerAccount1.Id,
OwnerId = portalAccountOwner1.Id,
CRM__c = '00500000006olmF'
);
Database.insert(portalAccount1);

portalAccount2 = new Account(
Name = 'TestAccount',
ParentId = PartnerAccount1.Id,
OwnerId = portalAccountOwner1.Id,
CRM__c = '00500000006olmF'
);
Database.insert(portalAccount2);

//Create contact
Contact contact1 = new Contact(
FirstName = 'Test',
Lastname = 'McTesty',
AccountId = portalAccount1.Id,
Email = System.now().millisecond() + 'test@test.com'
);
Database.insert(contact1);

//Create user
Profile portalProfile = [SELECT Id FROM Profile where Id = '00e000000073Tjc'];
user1 = new User(
Username = System.now().millisecond() + 'test15@test.com',
ContactId = contact1.Id,
ProfileId = portalProfile.Id,
Alias = 'test123',
Email = 'test12345@test.com',
EmailEncodingKey = 'UTF-8',
LastName = 'McTesty',
CommunityNickname = 'test12345',
TimeZoneSidKey = 'America/Los_Angeles',
LocaleSidKey = 'en_US',
LanguageLocaleKey = 'en_US',
Bypass_Validation_Rules__c = true
);
Database.insert(user1);
}

System.runAs (user1){

System.currentPagereference().getParameters().put('acct', portalAccount1.Id);
//System.currentPagereference().getParameters().put('bypass','true');
System.currentPagereference().getParameters().put('type','Service');
     
PortalWizardSelectAcct_Controller oPEE = new PortalWizardSelectAcct_Controller(); 
oPEE.strID = portalAccount2.Id;
oPEE.getopp();
oPEE.getacct();
oPEE.Next();


}}

}