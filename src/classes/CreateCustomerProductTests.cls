@isTest(SeeAllData=true)
public with sharing class CreateCustomerProductTests 
{
    
    /*static Opportunity testOpp;
    static OpportunityLineItem CertOli;
    static OpportunityLineItem EISOli;
    static OpportunityLineItem APSOli;
    static OpportunityLineItem IbOli;
    static OpportunityLineItem CertFeeOli;
    static OpportunityLineItem PartnerOli;
    static OpportunityLineItem ChFrmwkOli;
    static OpportunityLineItem DiscountOli;
    static Customer_Product_Detail__c ExistingEIS;
    
    static void setupOppandOlis()
    {   
        Account a = new Account (Name = 'Test Account');
        insert a;
        
        Account aPartner = new Account (Name = 'Test Partner Account');
        insert aPartner;
        
        Promo_Code__c promocode = new Promo_Code__c (Name ='TEST001', Account__c = a.Id, Promo_Type__c = 'Referral');
        insert promocode;
        
        aPartner.Promo_Code__c = promocode.Id;
        update aPartner;
        
        testOpp = new Opportunity (
        AccountId = a.Id, 
        Name = 'TestOpp', 
        CloseDate = system.today()+30, 
        Type = 'New Business',
        StageName = 'Demonstrating Capabilities',
        Pricebook2Id = '01s000000004NS6AAM',
        PartnerLookup__c = aPartner.Id,
        CurrencyISOCode = 'USD',
        AM__c = '00500000007Em7P',
        Approval_Type__c = 'Pricing Approval');
        
        CertOli = new OpportunityLineItem(
        PricebookEntryId = '01u00000000FGwSAAW',
        Product_Status__c = 'Current',
        Description__c = 'RP Certification',
        Start_Date__c = system.today() +30,
        End_Date__c = system.today() +394,
        UnitPrice = 1500.00,
        Previous_Value__c = 0.00,
        Quantity = 1,
        Recurring__c = true);
        
        CertFeeOli = new OpportunityLineItem(
        PricebookEntryId = '01u00000000FeX2AAK',
        Product_Status__c = 'Current',
        Start_Date__c = system.today() +30,
        End_Date__c = system.today() +394,
        UnitPrice = 500.00,
        Previous_Value__c = 0.00,
        Quantity = 1,
        Recurring__c = false);
        
        EISOli = new OpportunityLineItem(
        PricebookEntryId = '01u00000000FGw7AAG',
        Product_Status__c = 'Current',
        Description__c = 'Tools', 
        Start_Date__c = system.today() +30,
        End_Date__c = system.today() +394,
        UnitPrice = 1500.00,
        Previous_Value__c = 0.00,
        Quantity = 1,
        Recurring__c = true);
        
        APSOli = new OpportunityLineItem(
        PricebookEntryId = '01u00000000FeWaAAK',
        Product_Status__c = 'Current',
        Start_Date__c = system.today() +30,
        End_Date__c = system.today() +394,
        UnitPrice = 1500.00,
        Previous_Value__c = 0.00,
        Quantity = 1,
        Recurring__c = true);
        
        IbOli = new OpportunityLineItem(
        PricebookEntryId = '01u00000000FeWVAA0',
        Product_Status__c = 'Current',
        Start_Date__c = system.today() +30,
        End_Date__c = system.today() +394,
        UnitPrice = 1500.00,
        Previous_Value__c = 0.00,
        Quantity = 1,
        Recurring__c = true);
        
        PartnerOli = new OpportunityLineItem(
        PricebookEntryId = '01u00000000FfaKAAS',
        Product_Status__c = 'Current',
        Description__c = 'Tools',
        Start_Date__c = system.today() +30,
        End_Date__c = system.today() +394,
        UnitPrice = 1500.00,
        Previous_Value__c = 0.00,
        Quantity = 1,
        Recurring__c = true);
        
        ChFrmwkOli = new OpportunityLineItem(
        PricebookEntryId = '01u00000000FGwgAAG',
        Product_Status__c = 'Current',
        Start_Date__c = system.today() +30,
        End_Date__c = system.today() +394,
        UnitPrice = 1500.00,
        Previous_Value__c = 0.00,
        Quantity = 1,
        Recurring__c = true);
        
        DiscountOli = new OpportunityLineItem(
        PricebookEntryId = '01u00000000FH9yAAG',
        Product_Status__c = 'Current',
        Start_Date__c = system.today() +30,
        End_Date__c = system.today() +394,
        UnitPrice = 1500.00,
        Previous_Value__c = 0.00,
        Quantity = 1,
        Recurring__c = false);
        
        ExistingEIS = new Customer_Product_Detail__c(
        Account__c = a.Id,
        Name = 'Email Intellegence Solutions - Gold',
        Product_Family__c = 'Email Intelligence for Marketers',
        Activation_Date__c = system.today()- 365,
        Last_12_Months__c = 30000,
        Total_Price__c = 30000,
        Inbox_Monitor_Events__c = 50,
        Inbox_Preview_Events__c = 50,
        Inbox_Monitor_Fee__c = 20,
        Inbox_Preview_Fee__c = 20);
    }
    
   static testMethod void CreateCustomerProductDetail_Test1()
    {
        setupOppandOlis();
        insert testOpp;
        CertOli.OpportunityId = testOpp.Id;
        EISOli.OpportunityId = testOpp.Id;
        APSOli.OpportunityId = testOpp.Id;
        IbOli.OpportunityId = testOpp.Id;
        PartnerOli.OpportunityId = testOpp.Id;
        ChFrmwkOli.OpportunityId = testOpp.Id;
        DiscountOli.OpportunityId = testOpp.Id;
        insert new List<OpportunityLineItem>{EISOli, PartnerOli, DiscountOli};
        
        //List<OpportunityLineItem> OlisOnTestOpp = [SELECT Id, OpportunityId FROM OpportunityLineItem WHERE OpportunityId = :testOpp.Id];
        //system.assertEquals(7, OlisOnTestOpp.size(), 'There should be 7 OLIs on the Test Opp');
        
        Opportunity renewalOpp = new Opportunity (
        AccountId = testOpp.AccountId, 
        Name = 'TestOpp', 
        CloseDate = system.today()+30, 
        Type = 'Renewal',
        StageName = 'Demonstrating Capabilities',
        Pricebook2Id = '01s000000004NS6AAM',
        PartnerLookup__c = testOpp.PartnerLookup__c,
        CurrencyISOCode = 'USD',
        AM__c = '00500000007Em7P',
        Approval_Type__c = 'Pricing Approval',
        Previous_Opportunity_Lookup__c = testOpp.Id);
        insert renewalOpp;
        
        test.startTest();
        testOpp.Renewal_Created_del__c = true;
        testOpp.StageName = 'Finance Approved';
        update testOpp;
        test.stopTest();
        
        List<Customer_Product_Detail__c> ProdPorts = [SELECT Id, Account__c, Activation_Date__c, Total_Price__c, Blacklist_Alert_IPs__c, Certification_Tier__c, Combined_Events__c, 
        Last_12_Months__c, CurrentStartDate__c, Description__c, Discount_Amount__c, Domain_Limit__c, Inbox_Monitor_Events__c, Inbox_Monitor_Fee__c, Inbox_Preview_Events__c, Inbox_Preview_Fee__c, Invoiced_Party__c, 
        Line_Item_Id__c, Line_Item_Total_Price__c, Monthly_Campaigns__c, Monthly_Email_Volume__c, Current_Opportunity__c, Opportunity_Type__c, Non_Recurring_ACV__c, Non_Recurring_End_Date__c, Non_Recurring_Start_Date__c,
        Number_of_IPs__c, Number_of_Platinum_Contacts__c, Partner__c, Partner_Commission_Amount__c, Price__c, Product2Id__c, Product_Family__c, Product_Name__c, Product_Status__c, Renewal_Date__c, 
        Renewal_Opportunity__c, Reputation_Monitor_IPs__c, Service_Level__c, Suspended__c, Suspended_Reason__c, Type_of_Opportunity__c, Upcoming_Renewal_Amount__c
        FROM Customer_Product_Detail__c 
        WHERE Account__c = :testOpp.AccountId];
        
        /*List<Customer_Product_Detail__c> CertPorts = [SELECT Id, Account__c, Activation_Date__c, Total_Price__c, Blacklist_Alert_IPs__c, Certification_Tier__c, Combined_Events__c, 
        Last_12_Months__c, CurrentStartDate__c, Description__c, Discount_Amount__c, Domain_Limit__c, Inbox_Monitor_Events__c, Inbox_Monitor_Fee__c, Inbox_Preview_Events__c, Inbox_Preview_Fee__c, Invoiced_Party__c, 
        Line_Item_Id__c, Line_Item_Total_Price__c, Monthly_Campaigns__c, Monthly_Email_Volume__c, Current_Opportunity__c, Opportunity_Type__c, Non_Recurring_ACV__c, Non_Recurring_End_Date__c, Non_Recurring_Start_Date__c,
        Number_of_IPs__c, Number_of_Platinum_Contacts__c, Partner__c, Partner_Commission_Amount__c, Price__c, Product2Id__c, Product_Family__c, Product_Name__c, Product_Status__c, Renewal_Date__c, 
        Renewal_Opportunity__c, Reputation_Monitor_IPs__c, Service_Level__c, Suspended__c, Suspended_Reason__c, Type_of_Opportunity__c, Upcoming_Renewal_Amount__c
        FROM Customer_Product_Detail__c 
        WHERE Account__c = :testOpp.AccountId and Product_Family__c = 'Certification.EQ'];*/
        
        /*List<Customer_Product_Detail__c> EISPorts = [SELECT Id, Account__c, Activation_Date__c, Total_Price__c, Blacklist_Alert_IPs__c, Certification_Tier__c, Combined_Events__c, 
        Last_12_Months__c, CurrentStartDate__c, Description__c, Discount_Amount__c, Domain_Limit__c, Inbox_Monitor_Events__c, Inbox_Monitor_Fee__c, Inbox_Preview_Events__c, Inbox_Preview_Fee__c, Invoiced_Party__c, 
        Line_Item_Id__c, Line_Item_Total_Price__c, Monthly_Campaigns__c, Monthly_Email_Volume__c, Current_Opportunity__c, Opportunity_Type__c, Non_Recurring_ACV__c, Non_Recurring_End_Date__c, Non_Recurring_Start_Date__c,
        Number_of_IPs__c, Number_of_Platinum_Contacts__c, Partner__c, Partner_Commission_Amount__c, Price__c, Product2Id__c, Product_Family__c, Product_Name__c, Product_Status__c, Renewal_Date__c, 
        Renewal_Opportunity__c, Reputation_Monitor_IPs__c, Service_Level__c, Suspended__c, Suspended_Reason__c, Type_of_Opportunity__c, Upcoming_Renewal_Amount__c
        FROM Customer_Product_Detail__c 
        WHERE Account__c = :testOpp.AccountId and Product_Family__c = 'Email Intelligence for Marketers'];
        
        //system.assertEquals(0, CertPorts.size(), 'There should be no Product Portfolio Component for the Certification Line Item because it got cloned off');//check this later
        system.assertEquals(2, ProdPorts.size(), 'There should be the correct number of Product Portfolio Components, no Partner or Cert CPD should be created');
        //system.assertEquals(renewalOpp.Id, ProdPorts[0].Renewal_Opportunity__c, 'The Renewal Opp should be populated with the correct Id');
        system.assertEquals(1500, EISPorts[0].Partner_Commission_Amount__c, 'Partner Comm should be 1500 for the EIS Product');
        

        
    }

    static testMethod void CreateCustomerProductDetail_Test2()
    {
        setupOppandOlis();
        testOpp.Type = 'Supplement';
        insert testOpp;
        EISOli.OpportunityId = testOpp.Id;
        EISOli.UnitPrice = 10000;
        EISOli.Start_Date__c = system.today();
        EISOLi.End_Date__c = system.today() +180;
        EISOli.of_MM_Events_Allowed__c = 50;
        EISOli.of_CP_Events_Allowed__c = 50;
        insert EISOli;
        
        ExistingEIS.Line_Item_Id__c = testOpp.AccountId + '-' + ExistingEIS.Product_Family__c;
        insert ExistingEIS;
        system.debug('What is EV?' + ExistingEIS.Total_Price__c);
        
        /*List<Customer_Product_Detail__c> ProdPort = [SELECT Id, Account__c, Activation_Date__c, Total_Price__c, Blacklist_Alert_IPs__c, Certification_Tier__c, Combined_Events__c, 
        Last_12_Months__c, CurrentStartDate__c, Description__c, Discount_Amount__c, Domain_Limit__c, Inbox_Monitor_Events__c, Inbox_Monitor_Fee__c, Inbox_Preview_Events__c, Inbox_Preview_Fee__c, Invoiced_Party__c, 
        Line_Item_Id__c, Line_Item_Total_Price__c, Monthly_Campaigns__c, Monthly_Email_Volume__c, Current_Opportunity__c, Opportunity_Type__c, Non_Recurring_ACV__c, Non_Recurring_End_Date__c, Non_Recurring_Start_Date__c,
        Number_of_IPs__c, Number_of_Platinum_Contacts__c, Partner__c, Partner_Commission_Amount__c, Price__c, Product2Id__c, Product_Family__c, Product_Name__c, Product_Status__c, Renewal_Date__c, 
        Renewal_Opportunity__c, Reputation_Monitor_IPs__c, Service_Level__c, Suspended__c, Suspended_Reason__c, Type_of_Opportunity__c, Upcoming_Renewal_Amount__c
        FROM Customer_Product_Detail__c 
        WHERE Account__c = :testOpp.AccountId];
        system.assertEquals(30000, ProdPort[0].Last_12_Months__c, 'ACV should be 30k');*/    
        
        /*test.startTest();
        testOpp.StageName = 'Finance Approved';
        update testOpp;
        test.stopTest();
        
        List<Customer_Product_Detail__c> EISPorts = [SELECT Id, Account__c, Activation_Date__c, Total_Price__c, Blacklist_Alert_IPs__c, Certification_Tier__c, Combined_Events__c, 
        Last_12_Months__c, CurrentStartDate__c, Description__c, Discount_Amount__c, Domain_Limit__c, Inbox_Monitor_Events__c, Inbox_Monitor_Fee__c, Inbox_Preview_Events__c, Inbox_Preview_Fee__c, Invoiced_Party__c, 
        Line_Item_Id__c, Line_Item_Total_Price__c, Monthly_Campaigns__c, Monthly_Email_Volume__c, Current_Opportunity__c, Opportunity_Type__c, Non_Recurring_ACV__c, Non_Recurring_End_Date__c, Non_Recurring_Start_Date__c,
        Number_of_IPs__c, Number_of_Platinum_Contacts__c, Partner__c, Partner_Commission_Amount__c, Price__c, Product2Id__c, Product_Family__c, Product_Name__c, Product_Status__c, Renewal_Date__c, 
        Renewal_Opportunity__c, Reputation_Monitor_IPs__c, Service_Level__c, Suspended__c, Suspended_Reason__c, Type_of_Opportunity__c, Upcoming_Renewal_Amount__c
        FROM Customer_Product_Detail__c 
        WHERE Account__c = :testOpp.AccountId and Product_Family__c = 'Email Intelligence for Marketers'];
        
        system.assertEquals(1, EISPorts.size(), 'There should be 1 Product Portfolio Component');
        system.assertEquals(50000, EISPorts[0].Total_Price__c, 'Run Rate should be 50k');
        system.assertEquals(testOpp.Id, EISPorts[0].Current_Opportunity__c, 'Most Recent Opp should be the Supplement');
        system.assertEquals(40000, EISPorts[0].Last_12_Months__c, 'Current ACV should be 40k');
        system.assertEquals(100, EISPorts[0].Inbox_Monitor_Events__c, 'Number of Events should be 100');
        
    }
    
    static testMethod void CreateCustomerProductDetail_Test3()
    {
        setupOppandOlis();
        insert testOpp;
        CertOli.OpportunityId = testOpp.Id;
        insert CertOli;
        CertFeeOli.OpportunityId = testOpp.Id;
        insert CertFeeOli;
        
        test.startTest();
        testOpp.StageName = 'Finance Approved';
        update testOpp;
        test.stopTest();
        
       /* List<Customer_Product_Detail__c> ProdPorts = [SELECT Id, Account__c, Activation_Date__c, Total_Price__c, Blacklist_Alert_IPs__c, Certification_Tier__c, Combined_Events__c, 
        Last_12_Months__c, CurrentStartDate__c, Description__c, Discount_Amount__c, Domain_Limit__c, Inbox_Monitor_Events__c, Inbox_Monitor_Fee__c, Inbox_Preview_Events__c, Inbox_Preview_Fee__c, Invoiced_Party__c, 
        Line_Item_Id__c, Line_Item_Total_Price__c, Monthly_Campaigns__c, Monthly_Email_Volume__c, Current_Opportunity__c, Opportunity_Type__c, Non_Recurring_ACV__c, Non_Recurring_End_Date__c, Non_Recurring_Start_Date__c,
        Number_of_IPs__c, Number_of_Platinum_Contacts__c, Partner__c, Partner_Commission_Amount__c, Price__c, Product2Id__c, Product_Family__c, Product_Name__c, Product_Status__c, Renewal_Date__c, 
        Renewal_Opportunity__c, Reputation_Monitor_IPs__c, Service_Level__c, Suspended__c, Suspended_Reason__c, Type_of_Opportunity__c, Upcoming_Renewal_Amount__c
        FROM Customer_Product_Detail__c 
        WHERE Account__c = :testOpp.AccountId];*/
        
        /*List<Customer_Product_Detail__c> CertPorts = [SELECT Id, Account__c, Activation_Date__c, Total_Price__c, Blacklist_Alert_IPs__c, Certification_Tier__c, Combined_Events__c, 
        Last_12_Months__c, CurrentStartDate__c, Description__c, Discount_Amount__c, Domain_Limit__c, Inbox_Monitor_Events__c, Inbox_Monitor_Fee__c, Inbox_Preview_Events__c, Inbox_Preview_Fee__c, Invoiced_Party__c, 
        Line_Item_Id__c, Line_Item_Total_Price__c, Monthly_Campaigns__c, Monthly_Email_Volume__c, Current_Opportunity__c, Opportunity_Type__c, Non_Recurring_ACV__c, Non_Recurring_End_Date__c, Non_Recurring_Start_Date__c,
        Number_of_IPs__c, Number_of_Platinum_Contacts__c, Partner__c, Partner_Commission_Amount__c, Price__c, Product2Id__c, Product_Family__c, Product_Name__c, Product_Status__c, Renewal_Date__c, 
        Renewal_Opportunity__c, Reputation_Monitor_IPs__c, Service_Level__c, Suspended__c, Suspended_Reason__c, Type_of_Opportunity__c, Upcoming_Renewal_Amount__c
        FROM Customer_Product_Detail__c 
        WHERE Account__c = :testOpp.AccountId and Product_Family__c = 'Certification.EQ'];
        
        system.assertEquals(1, CertPorts.size(), 'There should be 1 Product Portfolio Component');
        //system.assertEquals(500, CertPorts[0].Non_Recurring_ACV__c, 'Non Recurring ACV should be 500 for the Cert Product');//check this later
        
    }*/
    
}