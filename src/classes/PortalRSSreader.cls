public class PortalRSSreader {
     
    /*public class channel {
        public String title {get;set;}
        public String link {get;set;}
        public String description {get;set;}
        public String author {get;set;}
        public String category {get;set;}
        public String copyright {get;set;}
        public String docs {get;set;}
        public PortalRSSreader.image image {get;set;}
        public list<PortalRSSreader.item> items {get;set;}
        public channel() {
            items = new list<PortalRSSreader.item>();
        }
    }
     
    public class image {
        public String url {get;set;}
        public String title {get;set;}
        public String link {get;set;}
    }
     
    public class item {
        public String title {get;set;}
        public String guid {get;set;}
        public String link {get;set;}
        public String description {get;set;}
        public String pubDate {get;set;}
        public String source {get;set;}
        public Date getPublishedDate() {
            Date result = (pubDate != null) ? Date.valueOf(pubDate.replace('T', '').replace('Z','')) : null;
            return result;
        }
       // public DateTime getPublishedDateTime() {
       //     DateTime result = (pubDate != null) ? DateTime.valueOf(pubDate.replace('T', '').replace('Z','')) : null;
        //    return result;
       // }
    }
     
    public static PortalRSSreader.channel getRSSData(string feedURL) {
         
        HttpRequest req = new HttpRequest();
        req.setEndpoint(feedURL);
        req.setMethod('GET');
         
        Dom.Document doc = new Dom.Document();
        Http h = new Http();
         
        
            HttpResponse res = h.send(req);
            doc = res.getBodyDocument();
        
         
        Dom.XMLNode rss = doc.getRootElement();
        //first child element of rss feed is always channel
        Dom.XMLNode channel = rss.getChildElements()[0];
         
        PortalRSSreader.channel result = new PortalRSSreader.channel();
         
        list<PortalRSSreader.item> rssItems = new list<PortalRSSreader.item>();
         
        //for each node inside channel
        for(Dom.XMLNode elements : channel.getChildElements()) {
            if('title' == elements.getName()) {
                result.title = elements.getText();
            }
            if('link' == elements.getName()) {
                result.link = elements.getText();
            }
            if('description' == elements.getName()) {
                result.description = elements.getText();
            }
            if('category' == elements.getName()) {
                result.category = elements.getText();
            }
            if('copyright' == elements.getName()) {
                result.copyright = elements.getText();
            }
            if('docs' == elements.getName()) {
                result.docs = elements.getText();
            }
            if('image' == elements.getName()) {
                PortalRSSreader.image img = new PortalRSSreader.image();
                //for each node inside image
                for(Dom.XMLNode xmlImage : elements.getChildElements()) {
                    if('url' == xmlImage.getName()) {
                        img.url = xmlImage.getText();
                    }
                    if('title' == xmlImage.getName()) {
                        img.title = xmlImage.getText();
                    }
                    if('link' == xmlImage.getName()) {
                        img.link = xmlImage.getText();
                    }
                }
                result.image = img;
            }
             
            if('item' == elements.getName()) {
                PortalRSSreader.item rssItem = new PortalRSSreader.item();
                //for each node inside item
                for(Dom.XMLNode xmlItem : elements.getChildElements()) {
                    if('title' == xmlItem.getName()) {
                        rssItem.title = xmlItem.getText();
                    }
                    if('guid' == xmlItem.getName()) {
                        rssItem.guid = xmlItem.getText();
                    }
                    if('link' == xmlItem.getName()) {
                        rssItem.link = xmlItem.getText();
                    }
                    if('description' == xmlItem.getName()) {
                        rssItem.description = xmlItem.getText();
                    }
                    if('pubDate' == xmlItem.getName()) {
                        rssItem.pubDate = xmlItem.getText();
                    }
                    if('source' == xmlItem.getName()) {
                        rssItem.source = xmlItem.getText();
                    }
                }
                //for each item, add to rssItem list
                rssItems.add(rssItem);
            }
             
        }
        //finish PortalRSSreader.channel object by adding the list of all rss items
        result.items = rssItems;
         
        return result;
         
    }
     
    static testMethod void RSSTest() {
        PortalRSSreader.channel chan = PortalRSSreader.getRSSData('http://blog.returnpath.com/blog/return-path/rss' + EncodingUtil.urlEncode('','UTF-8'));
        Date pDate = chan.items[0].getPublishedDate();
        //DateTime pDateTime = chan.items[0].getPublishedDateTime();
    }*/
 
}