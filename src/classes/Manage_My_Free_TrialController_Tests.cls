@ isTest

public class Manage_My_Free_TrialController_Tests{

static Opportunity newbiz;
static OpportunityLineItem newbizoli;

static void setupOppandOli()
{
    
	c2g__codaGeneralLedgerAccount__c GenLedge = new c2g__codaGeneralLedgerAccount__c (Name = 'Test', c2g__ReportingCode__c = '1234', c2g__Type__c = 'Balance Sheet');
	c2g__codaGeneralLedgerAccount__c GenLedge2 = new c2g__codaGeneralLedgerAccount__c (Name = 'Test', c2g__ReportingCode__c = '1350', c2g__Type__c = 'Balance Sheet');
	c2g__codaGeneralLedgerAccount__c GenLedge3 = new c2g__codaGeneralLedgerAccount__c (Name = 'Test', c2g__ReportingCode__c = '5000', c2g__Type__c = 'Balance Sheet');
	insert new List<c2g__codaGeneralLedgerAccount__c>{GenLedge, GenLedge2, GenLedge3};

	c2g__codaIncomeScheduleDefinition__c incsched = new c2g__codaIncomeScheduleDefinition__c (Name = 'Income Schedule', c2g__GeneralLedgerAccount__c = GenLedge.Id);
	insert incsched;
	
	c2g__codaCompany__c financeAccount = new c2g__codaCompany__c( c2g__TaxIdentificationNumber__c = '06-1566593', Name = 'TestAccount' );
	insert financeAccount;
    
    Account a = new Account (Name = 'Test Account');
    insert a;
    
    newbiz = new Opportunity (
    AccountId = a.Id, 
    Name = 'TestOpp', 
    CloseDate = system.today()+30, 
    Type = 'New Business',
    StageName = 'Establishing Value',
    Pricebook2Id = '01s000000004NS6AAM',
    CurrencyISOCode = 'USD',
    Approval_Type__c = 'Not Applicable',
    Generated_From_Free_Trial__c = true);

    
    newbizoli = new OpportunityLineItem (
    PricebookEntryId = '01u00000000FeWfAAK',
    Product_Status__c = 'Current',
    Start_Date__c = system.today() +30,
    End_Date__c = system.today() +394,
    UnitPrice = 0.00,
    Quantity = 1);
    
    
}


static testMethod void Manage_My_Free_TrialExtend()
{
    PageReference pageRef = Page.ManageMyFreeTrial;
    Test.setCurrentPageReference(pageRef);
    

    setupOppandOli();
    insert newbiz;
    newbizoli.OpportunityId = newbiz.Id;
    insert newbizoli;
    String freetrial = 'Extend';
    

    ApexPages.StandardController sc = new ApexPages.standardController(newbiz);
    System.currentPagereference().getParameters().put('id',newbiz.Id);
    Manage_My_Free_TrialController methods = new Manage_My_Free_TrialController(sc);   
    
    test.startTest();
    methods.getItems();
    methods.getfreetrial();
    methods.setfreetrial(freetrial) ;
    methods.Confirm();
    test.stopTest();
    
    list<Opportunity> extendTest = [SELECT Id, Type, StageName, RecordTypeId, Approval_Type__c, Submit_from_Trigger__c FROM Opportunity WHERE Type = 'Trial - Extension' and StageName = 'Verbal Commitment' and Approval_Type__c = 'Finance Approval'];
    system.assert(extendTest.size() == 1);   

    
}

static testMethod void Manage_My_Free_TrialTerminate()
{
    PageReference pageRef = Page.ManageMyFreeTrial;
    Test.setCurrentPageReference(pageRef);
    

    setupOppandOli();
    insert newbiz;
    newbizoli.OpportunityId = newbiz.Id;
    insert newbizoli;
    String freetrial = 'Terminate';
    

    ApexPages.StandardController sc = new ApexPages.standardController(newbiz);
    System.currentPagereference().getParameters().put('id',newbiz.Id);
    Manage_My_Free_TrialController methods = new Manage_My_Free_TrialController(sc);   
    
    test.startTest();
    methods.getItems();
    methods.getfreetrial();
    methods.setfreetrial(freetrial) ;
    methods.Confirm();
    test.stopTest();
    system.assertEquals('Terminate', methods.getfreetrial());
    
    list<Opportunity> terminateTest = [SELECT Id, Type, StageName, RecordTypeId, Approval_Type__c, Submit_from_Trigger__c FROM Opportunity WHERE Id = :newbiz.Id ];
    system.assertEquals('Approval Required', terminateTest[0].StageName, 'Opp Stage should be Approval Required');
    system.assertEquals('Finance Approval', terminateTest[0].Approval_Type__c, 'Opp Approval Type should be Finance Approval');
    system.assertEquals('Trial', terminateTest[0].Type, 'Opp Type should be Trial');
    system.assertEquals('012000000004wEC', terminateTest[0].RecordTypeId, 'Opp Record Type should be Termination');
    
    list<OpportunityLineItem> termolis = [SELECT Id, Product_Status__c, Start_Date__c, End_Date__c FROM OpportunityLineItem WHERE OpportunityId = :newbiz.Id and Product_Status__c = 'Terminated'];
    system.assert(termolis.size() == 1);
    system.assertEquals(system.today(), termolis[0].Start_Date__c, 'Start Date should be equal to today');
    system.assertEquals(system.today(), termolis[0].End_Date__c, 'End Date should be equal to today');   

    
}
}