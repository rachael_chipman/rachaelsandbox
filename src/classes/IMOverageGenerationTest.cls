@isTest(SeeAllData=true)
public with sharing class IMOverageGenerationTest {

    public static Mailbox_Monitor_Events__c imEvent;
    public static User testUser;
    public static Opportunity requiredOpp;
    
    private static Account testAccount;
    private static List<Account> testAccounts;
    private static Zuora__CustomerAccount__c testBillingAccount;

    private static Zuora__Subscription__c testSub;
    private static Zuora__SubscriptionProductCharge__c testSubProdCharge;
    private static Zuora__Product__c testProduct;

    
    public static void setupObjects()
    {
        testUser = [SELECT Id FROM User WHERE isActive = true LIMIT 1];

        testAccounts = new List<Account>();

        testAccounts.add(testAccount = TestUtils.createAccount(TestUtils.MARKETER_RECORD_TYPE.Id, true));
        system.assert(testAccount != NULL);

        testBillingAccount = TestUtils.createBillingAccounts(testAccounts, true)[0];
        system.assert(testBillingAccount != NULL);

        testSub = TestUtils.createZ_Subscription(testAccount.Id, testBillingAccount.Id, testBillingAccount.Id, 'Test EIS Subscription', 'TERMED', true);
        system.assert(testSub != NULL);
        testSub.Zuora__Status__c = 'Active';
        update testSub;

        testProduct = TestUtils.createZuoraProduct('Email Optimization', true);
        system.assert(testProduct != NULL);

        testSubProdCharge = TestUtils.createZ_SubscriptionCharge('Inbox Monitor Events', testSub.Id, testAccount.Id, testProduct.Id, 'Email Intelligence Solutions - Gold - Monthly', 'Recurring', true);
        system.assert(testSubProdCharge != NULL);
        testSubProdCharge.IncludedUnits__c = 25;
        testSubProdCharge.OveragePrice__c = 25.00;
        update testSubProdCharge;
        
        requiredOpp = new Opportunity (Name = 'Test Opportunity', AccountId = testAccount.Id, StageName = 'Finance Approved', CloseDate = system.today().addDays(-365));
        insert requiredOpp;
        
        imEvent = new Mailbox_Monitor_Events__c();
        imEvent.Name = 'TEST_IM_EVENT';
        imEvent.Month__c = system.today().addMonths(-1).toStartOfMonth();
        imEvent.Account__c = testAccount.Id;
        imEvent.Opportunity__c = requiredOpp.Id;
        imEvent.Campaigns__c = 50;
    }

     public static testMethod void testScheduledBatch()
    {
        setupObjects();
        
        List<AsyncApexJob> myJobs = [select id from AsyncApexJob where Status != 'Completed' AND JobType = 'BatchApex'];
        for(AsyncApexJob myJob : myJobs)
        {
            system.abortJob(myJob.Id);
        }

        insert imEvent;
       
        test.startTest();
        String cronID = system.scheduleBatch( new InboxMonitorOverageBatch(), 'IM Overage Batch', 1);
        test.stopTest();
        
        CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, NextFireTime FROM CronTrigger WHERE id = :cronID];
        system.assertEquals(0, ct.TimesTriggered, 'Should be 0 because the job has not started yet.');
    }
    
    public static testMethod void testOverageFeeOppGenerationIMEventInsert()
    {
        OverageFeeGenerationServices.qCurrency = 'GBP';
        setupObjects();
      
        List<AsyncApexJob> myJobs = [select id from AsyncApexJob where Status != 'Completed' AND JobType = 'BatchApex'];
        for(AsyncApexJob myJob : myJobs)
        {
            system.abortJob(myJob.Id);
        }
        insert imEvent;
       
        test.startTest();
        Database.executeBatch(new InboxMonitorOverageBatch(), 200);
        test.stopTest();
        
        //Get overage amount for assertions
        Decimal expectedAmount = (((imEvent.Campaigns__c - testSubProdCharge.IncludedUnits__c) * testSubProdCharge.OveragePrice__c)).setScale(2);
        system.debug('What amount do we expect? ' + expectedAmount);

        //Query for fee opps
        list<Opportunity> opportunitiesCreated = [SELECT Id FROM Opportunity WHERE AccountId = :testAccount.Id and Id != :requiredOpp.Id];
        system.assertEquals(1, opportunitiesCreated.size(), 'We expect there to be 1 new opportunity');
        Set<Id> oppIds = pluck.ids(opportunitiesCreated);
        //Query for fee quotes
        List<zqu__Quote__c> feeQuotes = [SELECT Id, zqu__Account__c, RealmId__c, Name, RecordTypeId, zqu__Amendment_Name__c, zqu__StartDate__c, zqu__SubscriptionType__c, zqu__ZuoraAccountID__c, zqu__ExistSubscriptionID__c
        FROM zqu__Quote__c WHERE zqu__Opportunity__c IN : oppIds];
        system.assertEquals(opportunitiesCreated.size(), feeQuotes.size(), 'We expect a quote for each opportunity');
        for(zqu__Quote__c quote : feeQuotes)
        {
            system.assertEquals(testAccount.Id, quote.zqu__Account__c, 'Each quote belongs to the same account');
            system.assertEquals(quote.RecordTypeId, OverageFeeGenerationServices.amendmentQuote.Id, 'Each quote should be an amendmentQuote');
            system.assertEquals((String) OverageFeeGenerationServices.subObject.getValue('Id'), quote.zqu__ExistSubscriptionID__c, 'Quote should be related to exisiting Subscription');
            system.assertEquals((String) OverageFeeGenerationServices.accObject.getValue('Id'), quote.zqu__ZuoraAccountID__c, 'Quote should be related to exisiting Billing Account');
        }
        
        //Query for quote amendments
        List<zqu__QuoteAmendment__c> amendments = [SELECT Id, zqu__TotalAmount__c, zqu__Quote__c FROM zqu__QuoteAmendment__c WHERE zqu__Quote__c  = :feeQuotes[0].Id ];
        system.assertEquals(feeQuotes.size(), amendments.size(), 'We expect an amendment for each quote.');
        system.assertEquals(expectedAmount, amendments[0].zqu__TotalAmount__c, 'We expect the overage amount to be the event usage less included units, multiplied by the price.');

        //Query for the rate plans
        List<zqu__QuoteRatePlan__c> ratePlans = [SELECT Id, zqu__QuoteAmendment__c FROM zqu__QuoteRatePlan__c WHERE zqu__QuoteAmendment__c IN : amendments];
        system.assertEquals(amendments.size(), ratePlans.size(), 'We expect one rate plan for each amendment');
        system.assertEquals(amendments[0].Id, ratePlans[0].zqu__QuoteAmendment__c, 'We expect the rate plan to be attached to the amendment');
        
        //Query for the rate plan charges
        List<zqu__QuoteRatePlanCharge__c> ratePlanCharges = [SELECT Id, zqu__QuoteRatePlan__c, zqu__EffectivePrice__c, zqu__TCV__c FROM zqu__QuoteRatePlanCharge__c WHERE zqu__QuoteRatePlan__c = : ratePlans[0].Id];
        system.assertEquals(ratePlans.size(), ratePlanCharges.size(), 'We expect there to be one rate plan charge for each rate plan');
        system.assertEquals(ratePlans[0].Id, ratePlanCharges[0].zqu__QuoteRatePlan__c, 'We expect the charge to be attached to the rate plan');
        system.assertEquals(expectedAmount, ratePlanCharges[0].zqu__TCV__c, 'We expect the overage amount to be the event usage less included units, multiplied by the price.');
        system.assertEquals(expectedAmount, ratePlanCharges[0].zqu__EffectivePrice__c, 'We expect the overage amount to be the event usage less included units, multiplied by the price.');

        //Query for the charge summaries
        List<zqu__QuoteChargeSummary__c> chargeSummaries = [SELECT Id, zqu__QuoteRatePlan__c, zqu__EffectivePrice__c, zqu__TotalPrice__c FROM zqu__QuoteChargeSummary__c WHERE zqu__QuoteRatePlan__c = : ratePlans[0].Id];
        system.assertEquals(ratePlans.size(), chargeSummaries.size(), 'We expect one charge summary for each rate plan.');
        system.assertEquals(expectedAmount, chargeSummaries[0].zqu__EffectivePrice__c, 'We expect the overage amount to be the event usage less included units, multiplied by the price.');
        system.assertEquals(expectedAmount, chargeSummaries[0].zqu__TotalPrice__c, 'We expect the overage amount to be the event usage less included units, multiplied by the price.');

        //Query for quote charge details
        List<zqu__QuoteChargeDetail__c> chargeDetails = [SELECT Id, zqu__Quote__c, zqu__TCV__c, zqu__DeltaTCV__c, zqu__MRR__c FROM zqu__QuoteChargeDetail__c WHERE zqu__Quote__c IN : feeQuotes];
        system.assertEquals(feeQuotes.size(), chargeDetails.size(), 'We expect in this case to create only 1 detail record for each quote.');
        system.assertEquals(expectedAmount, chargeDetails[0].zqu__DeltaTCV__c, 'We should have correct Delta TCV.');
        system.assertEquals(expectedAmount, chargeDetails[0].zqu__TCV__c, 'We should have correct TCV.');
        system.assertEquals(0.00, chargeDetails[0].zqu__MRR__c, 'The MRR should not have any value');
    }
    
    /*public static testMethod void testOverageFeeOppGenerationIMEventUpdate()
    {
         OverageFeeGenerationServices.qCurrency = 'GBP';
        setupObjects();

        insert imEvent;

        //Bring this up in code review!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! THERE SHOULD BE 2 OPPS CREATED (I THINK THE CALL TO ABORT THE BATCHES IS THE ISSUE)
        List<AsyncApexJob> myJobs = [select id from AsyncApexJob where Status != 'Completed' and MethodName != :InboxMonitorOverageBatch.BATCH_NAME AND JobType = 'BatchApex'];
        for(AsyncApexJob myJob : myJobs)
        {
            system.abortJob(myJob.Id);
        }

        test.startTest();
        imEvent.Generate_Fee_Opportunity__c = true;
        update imEvent;
        test.stopTest();

        Decimal expectedAmount = (((imEvent.Campaigns__c - testSubProdCharge.IncludedUnits__c) * testSubProdCharge.OveragePrice__c)).setScale(2);
        system.debug('What amount do we expect? ' + expectedAmount);

        //Query for fee opps
        list<Opportunity> opportunitiesCreated = [SELECT Id FROM Opportunity WHERE AccountId = :testAccount.Id and Id != :requiredOpp.Id];
        system.assertEquals(1, opportunitiesCreated.size(), 'We expect there to be 2 new opportunities');
        Set<Id> oppIds = pluck.ids(opportunitiesCreated);
        //Query for fee quotes
        List<zqu__Quote__c> feeQuotes = [SELECT Id, zqu__Account__c, RealmId__c, Name, RecordTypeId, zqu__Amendment_Name__c, zqu__StartDate__c, zqu__SubscriptionType__c, zqu__ZuoraAccountID__c, zqu__ExistSubscriptionID__c
        FROM zqu__Quote__c WHERE zqu__Opportunity__c IN : oppIds];
        system.assertEquals(opportunitiesCreated.size(), feeQuotes.size(), 'We expect a quote for each opportunity');
        for(zqu__Quote__c quote : feeQuotes)
        {
            system.assertEquals(testAccount.Id, quote.zqu__Account__c, 'Each quote belongs to the same account');
            system.assertEquals(quote.RecordTypeId, OverageFeeGenerationServices.amendmentQuote.Id, 'Each quote should be an amendmentQuote');
            system.assertEquals((String) OverageFeeGenerationServices.subObject.getValue('Id'), quote.zqu__ExistSubscriptionID__c, 'Quote should be related to exisiting Subscription');
            system.assertEquals((String) OverageFeeGenerationServices.accObject.getValue('Id'), quote.zqu__ZuoraAccountID__c, 'Quote should be related to exisiting Billing Account');
        }
        
        //Query for quote amendments
        List<zqu__QuoteAmendment__c> amendments = [SELECT Id, zqu__TotalAmount__c, zqu__Quote__c FROM zqu__QuoteAmendment__c WHERE zqu__Quote__c  IN :feeQuotes ];
        system.assertEquals(feeQuotes.size(), amendments.size(), 'We expect an amendment for each quote.');
        Map<Id, zqu__QuoteAmendment__c> idToAmendment = new Map<Id, zqu__QuoteAmendment__c>(amendments);
        for(zqu__QuoteAmendment__c amendment : amendments)
        {
            system.assertEquals(expectedAmount, amendment.zqu__TotalAmount__c, 'We expect the overage amount to be the event usage less included units, multiplied by the price.');
        }
        

        //Query for the rate plans
        List<zqu__QuoteRatePlan__c> ratePlans = [SELECT Id, zqu__QuoteAmendment__c FROM zqu__QuoteRatePlan__c WHERE zqu__QuoteAmendment__c IN : amendments];
        system.assertEquals(amendments.size(), ratePlans.size(), 'We expect one rate plan for each amendment');
        Map<Id, zqu__QuoteRatePlan__c> idToRatePlan = new Map<Id, zqu__QuoteRatePlan__c>(ratePlans);
        for(zqu__QuoteRatePlan__c ratePlan : ratePlans)
        {
            system.assert(idToAmendment.containsKey(ratePlan.zqu__QuoteAmendment__c), 'We expect the rate plan to be attached to the amendment');
        }
        
        
        //Query for the rate plan charges
        List<zqu__QuoteRatePlanCharge__c> ratePlanCharges = [SELECT Id, zqu__QuoteRatePlan__c, zqu__EffectivePrice__c, zqu__TCV__c FROM zqu__QuoteRatePlanCharge__c WHERE zqu__QuoteRatePlan__c IN : ratePlans];
        system.assertEquals(ratePlans.size(), ratePlanCharges.size(), 'We expect there to be one rate plan charge for each rate plan');

        for(zqu__QuoteRatePlanCharge__c ratePlanCharge : ratePlanCharges)
        {
            system.assert(idToRatePlan.containsKey(ratePlanCharge.zqu__QuoteRatePlan__c), 'We expect the charge to be attached to the rate plan.');
            system.assertEquals(expectedAmount, ratePlanCharge.zqu__TCV__c, 'We expect the overage amount to be the event usage less included units, multiplied by the price.');
            system.assertEquals(expectedAmount, ratePlanCharge.zqu__EffectivePrice__c, 'We expect the overage amount to be the event usage less included units, multiplied by the price.');
        }

        //Query for the charge summaries
        List<zqu__QuoteChargeSummary__c> chargeSummaries = [SELECT Id, zqu__QuoteRatePlan__c, zqu__EffectivePrice__c, zqu__TotalPrice__c FROM zqu__QuoteChargeSummary__c WHERE zqu__QuoteRatePlan__c IN : ratePlans];
        system.assertEquals(ratePlans.size(), chargeSummaries.size(), 'We expect one charge summary for each rate plan.');
        for(zqu__QuoteChargeSummary__c chargeSummary : chargeSummaries)
        {
            system.assert(idToRatePlan.containsKey(chargeSummary.zqu__QuoteRatePlan__c), 'We expect the summary to be attached to the rate plan.');
            system.assertEquals(expectedAmount, chargeSummary.zqu__EffectivePrice__c, 'We expect the overage amount to be the event usage less included units, multiplied by the price.');
            system.assertEquals(expectedAmount, chargeSummary.zqu__TotalPrice__c, 'We expect the overage amount to be the event usage less included units, multiplied by the price.');
        }
    }*/
    
    private static testMethod void testBulkInsertWithDiffAccounts()
    {
        List<Account> testAccts = TestUtils.createAccounts(200, TestUtils.MARKETER_RECORD_TYPE.Id, true);
        List<Zuora__CustomerAccount__c> testBillingAccts = TestUtils.createBillingAccounts(testAccts, true);
        system.assert(testBillingAccts != NULL);
        List<Opportunity> requiredOpps = new List<Opportunity>();
        List<Mailbox_Monitor_Events__c> testIMEvents = new List<Mailbox_Monitor_Events__c>();
        List<Zuora__Subscription__c> testSubscriptions = new List<Zuora__Subscription__c>();
        List<Zuora__SubscriptionProductCharge__c> testSubProdCharges = new List<Zuora__SubscriptionProductCharge__c>();

        Zuora__Product__c testProd = TestUtils.createZuoraProduct('Email Optimization', true);
        system.assert(testProd != NULL);
        
        for(Integer i = 0; i < testBillingAccts.size(); i++)
        {
            Zuora__Subscription__c testSubs = TestUtils.createZ_Subscription(testBillingAccts[i].Zuora__Account__c, testBillingAccts[i].Id, testBillingAccts[i].Id, 'Test EIS Subscription', 'TERMED', false);
            system.assert(testSubs != NULL);
            testSubs.Zuora__Status__c = 'Active';
            testSubscriptions.add(testSubs);

            requiredOpps.add(TestUtils.createOpportunities(1, TestUtils.RESEARCH_RECORD_TYPE.Id, testBillingAccts[i].Zuora__Account__c, false )[0]);
        }
        insert requiredOpps;
        insert testSubscriptions;


        for(Integer i = 0; i < testSubscriptions.size(); i++)
        {
            Zuora__SubscriptionProductCharge__c testSubsProdCharge = TestUtils.createZ_SubscriptionCharge('Inbox Monitor Events', testSubscriptions[i].Id, testSubscriptions[i].Zuora__Account__c, testProd.Id, 'Email Intelligence Solutions - Gold - Monthly', 'Recurring', false);
            system.assert(testSubsProdCharge != NULL);
            testSubProdCharges.add(testSubsProdCharge);
            for(Integer j = 0; j < testSubProdCharges.size(); j++)
            {
                if(j < 50)
                {
                    testSubsProdCharge.IncludedUnits__c = 10;
                    testSubsProdCharge.OveragePrice__c = 10.00;
                }
                else if(j >= 50 && j < 100)
                {
                    testSubsProdCharge.IncludedUnits__c = 15;
                    testSubsProdCharge.OveragePrice__c = 15.00;
                }
                else if(j >= 100)
                {
                    testSubsProdCharge.IncludedUnits__c = 25;
                    testSubsProdCharge.OveragePrice__c = 25.00;
                }
            }
        }
        insert testSubProdCharges;
        
        for(Integer i=0; i < requiredOpps.size(); i++)
        {
            Mailbox_Monitor_Events__c event = TestUtils.createInboxMonitorEvent(requiredOpps[i].AccountId, requiredOpps[i].Id, '99999999', 50, false);
            event.Name = 'TEST_IM_EVENT';
            event.Month__c = system.today().addMonths(-1).toStartOfMonth();
            testIMEvents.add(event);
        }

        List<AsyncApexJob> myJobs = [select id from AsyncApexJob where Status != 'Completed' AND JobType = 'BatchApex'];
        for(AsyncApexJob myJob : myJobs)
        {
            system.abortJob(myJob.Id);
        }
        insert testIMEvents;

        test.startTest();
        Database.executeBatch(new InboxMonitorOverageBatch());
        test.stopTest();
        
        //Get a set of AccountIds
        Set<Id> acctIds = pluck.Ids(testAccts);
        //Query for fee opps
        list<Opportunity> opportunitiesCreated = [SELECT Id FROM Opportunity WHERE AccountId IN : acctIds and Type = 'Fee'];
        system.assertEquals(200, opportunitiesCreated.size(), 'We expect there to be 400 new opportunities');
        Set<Id> oppIds = pluck.ids(opportunitiesCreated);
        //Query for fee quotes
        List<zqu__Quote__c> feeQuotes = [SELECT Id, zqu__Account__c, RealmId__c, Name, RecordTypeId, zqu__Amendment_Name__c, zqu__StartDate__c, zqu__SubscriptionType__c, zqu__ZuoraAccountID__c, zqu__ExistSubscriptionID__c
        FROM zqu__Quote__c WHERE zqu__Opportunity__c IN : oppIds];
        system.assertEquals(opportunitiesCreated.size(), feeQuotes.size(), 'We expect a quote for each opportunity');
        system.assertEquals(acctIds.size(), feeQuotes.size(), 'We expect each quote to have a different account.');
        for(zqu__Quote__c quote : feeQuotes)
        {
            //system.assertEquals(testAccount.Id, quote.zqu__Account__c, 'Each quote belongs to the same account');
            system.assertEquals(quote.RecordTypeId, OverageFeeGenerationServices.amendmentQuote.Id, 'Each quote should be an amendmentQuote');
            system.assertEquals((String) OverageFeeGenerationServices.subObject.getValue('Id'), quote.zqu__ExistSubscriptionID__c, 'Quote should be related to exisiting Subscription');
            system.assertEquals((String) OverageFeeGenerationServices.accObject.getValue('Id'), quote.zqu__ZuoraAccountID__c, 'Quote should be related to exisiting Billing Account');
        }
        
        //Query for quote amendments
        List<zqu__QuoteAmendment__c> amendments = [SELECT Id, zqu__TotalAmount__c, zqu__Quote__c, zqu__Quote__r.zqu__Account__c FROM zqu__QuoteAmendment__c WHERE zqu__Quote__c  IN :feeQuotes ];
        system.assertEquals(feeQuotes.size(), amendments.size(), 'We expect an amendment for each quote.');
        Map<Id, zqu__QuoteAmendment__c> idToAmendment = new Map<Id, zqu__QuoteAmendment__c>(amendments);

        //Query for all the existing charges, then map them to the quotes we generated, then validate that each amendment amount is correct
        List<Zuora__SubscriptionProductCharge__c> existingCharges = [SELECT Id, Name, Zuora__Account__c, Zuora__EffectiveEndDate__c, Zuora__EffectiveStartDate__c,
        Zuora__MonthlyRecurringRevenue__c, Product_Family__c, Zuora__Quantity__c, Zuora__Subscription__c, Zuora__Subscription__r.Zuora__Status__c, Zuora__Subscription__r.Zuora__Account__c, Zuora__Account__r.Name,
        Zuora__Subscription__r.Zuora__CustomerAccount__c, Zuora__Account__r.OwnerId, Zuora__Subscription__r.Zuora__Account__r.Owner.isActive, Zuora__Subscription__r.RealmId__c, OveragePrice__c, IncludedUnits__c,
        Zuora__Subscription__r.CurrencyIsoCode, Zuora__Subscription__r.Zuora__Version__c, Zuora__Subscription__r.Zuora__Zuora_Id__c, Zuora__Subscription__r.Zuora__CustomerAccount__r.Zuora__Zuora_Id__c
        FROM Zuora__SubscriptionProductCharge__c WHERE Zuora__Account__c IN : testAccts AND Zuora__Subscription__r.Zuora__Status__c = : OverageFeeGenerationServices.ACTIVE];

        Map<Id, List<Zuora__SubscriptionProductCharge__c>> accountIdToExistingCharge = groupBy.Ids('Zuora__Account__c', existingCharges);

        //Query for all the inbox monitor events and then map them by account Id to record 
        List<Mailbox_Monitor_Events__c> insertedEvents = [SELECT Id, Account__c, Campaigns__c FROM Mailbox_Monitor_Events__c WHERE Id IN : testIMEvents];

        Map<Id, List<Mailbox_Monitor_Events__c>> accountIdToEvents = groupBy.Ids('Account__c', insertedEvents);

        for(zqu__QuoteAmendment__c amendment : amendments)
        {
            Decimal price = accountIdToExistingCharge.get(amendment.zqu__Quote__r.zqu__Account__c)[0].OveragePrice__c;
            Integer includedUnits = Integer.valueOf(accountIdToExistingCharge.get(amendment.zqu__Quote__r.zqu__Account__c)[0].IncludedUnits__c);
            Integer eventLimit = Integer.valueOf(accountIdToEvents.get(amendment.zqu__Quote__r.zqu__Account__c)[0].Campaigns__c);

            Decimal overageAmount = ((eventLimit - includedUnits) * price).setScale(2);

            system.assertEquals(overageAmount, amendment.zqu__TotalAmount__c, 'We expect the overage amount to be the event usage less included units, multiplied by the price.');
        }
        
        //Query for the rate plans
        List<zqu__QuoteRatePlan__c> ratePlans = [SELECT Id, zqu__QuoteAmendment__c FROM zqu__QuoteRatePlan__c WHERE zqu__QuoteAmendment__c IN : amendments];
        system.assertEquals(amendments.size(), ratePlans.size(), 'We expect one rate plan for each amendment');
        Map<Id, zqu__QuoteRatePlan__c> idToRatePlan = new Map<Id, zqu__QuoteRatePlan__c>(ratePlans);
        for(zqu__QuoteRatePlan__c ratePlan : ratePlans)
        {
            system.assert(idToAmendment.containsKey(ratePlan.zqu__QuoteAmendment__c), 'We expect the rate plan to be attached to the amendment');
        }
        
        //Query for the rate plan charges
        List<zqu__QuoteRatePlanCharge__c> ratePlanCharges = [SELECT Id, zqu__QuoteRatePlan__c, zqu__QuoteRatePlan__r.zqu__QuoteAmendment__r.zqu__TotalAmount__c, zqu__EffectivePrice__c, zqu__TCV__c FROM zqu__QuoteRatePlanCharge__c WHERE zqu__QuoteRatePlan__c IN : ratePlans];
        system.assertEquals(ratePlans.size(), ratePlanCharges.size(), 'We expect there to be one rate plan charge for each rate plan');

        for(zqu__QuoteRatePlanCharge__c ratePlanCharge : ratePlanCharges)
        {
            system.assert(idToRatePlan.containsKey(ratePlanCharge.zqu__QuoteRatePlan__c), 'We expect the charge to be attached to the rate plan.');
            system.assertEquals(ratePlanCharge.zqu__QuoteRatePlan__r.zqu__QuoteAmendment__r.zqu__TotalAmount__c, ratePlanCharge.zqu__TCV__c, 'We expect the overage amount to be the event usage less included units, multiplied by the price.');
            system.assertEquals(ratePlanCharge.zqu__QuoteRatePlan__r.zqu__QuoteAmendment__r.zqu__TotalAmount__c, ratePlanCharge.zqu__EffectivePrice__c, 'We expect the overage amount to be the event usage less included units, multiplied by the price.');
        }

        //Query for the charge summaries
        List<zqu__QuoteChargeSummary__c> chargeSummaries = [SELECT Id, zqu__QuoteRatePlan__c, zqu__QuoteRatePlan__r.zqu__QuoteAmendment__r.zqu__TotalAmount__c, zqu__EffectivePrice__c, zqu__TotalPrice__c FROM zqu__QuoteChargeSummary__c WHERE zqu__QuoteRatePlan__c IN : ratePlans];
        system.assertEquals(ratePlans.size(), chargeSummaries.size(), 'We expect one charge summary for each rate plan.');
        for(zqu__QuoteChargeSummary__c chargeSummary : chargeSummaries)
        {
            system.assert(idToRatePlan.containsKey(chargeSummary.zqu__QuoteRatePlan__c), 'We expect the summary to be attached to the rate plan.');
            system.assertEquals(chargeSummary.zqu__QuoteRatePlan__r.zqu__QuoteAmendment__r.zqu__TotalAmount__c, chargeSummary.zqu__EffectivePrice__c, 'We expect the overage amount to be the event usage less included units, multiplied by the price.');
            system.assertEquals(chargeSummary.zqu__QuoteRatePlan__r.zqu__QuoteAmendment__r.zqu__TotalAmount__c, chargeSummary.zqu__TotalPrice__c, 'We expect the overage amount to be the event usage less included units, multiplied by the price.');
        }
        //Query for quote charge details
        List<zqu__QuoteChargeDetail__c> chargeDetails = [SELECT Id, zqu__Quote__c, zqu__TCV__c, zqu__DeltaTCV__c, zqu__MRR__c FROM zqu__QuoteChargeDetail__c WHERE zqu__Quote__c IN : feeQuotes];
        system.assertEquals(feeQuotes.size(), chargeDetails.size(), 'We expect in this case to create only 1 detail record for each quote.');
        for(zqu__QuoteChargeDetail__c detail : chargeDetails)
        {
            /*system.assertEquals(overageAmount, detail.zqu__DeltaTCV__c, 'We should have correct Delta TCV.');
            system.assertEquals(overageAmount, detail.zqu__TCV__c, 'We should have correct TCV.');*/ //We are checking the amount at the amendment level
            system.assertEquals(0.00, detail.zqu__MRR__c, 'The MRR should not have any value');
        }
    }
}