// Return Path Code
// Modify every Usage Data record's Week Label to be Date.toStartOfWeek().

Global class GainsightUpdateUsageDataBatch implements Database.Batchable<sObject> {

    // Variables needed to start our batch
    global final String query;

    global GainsightUpdateUsageDataBatch() {

        query = 'SELECT Id, JBCXM__WeekLabel__c, JBCXM__Date__c FROM JBCXM__UsageData__c LIMIT 50000000';
    }

    global void execute(Database.BatchableContext BC, List<JBCXM__UsageData__c> scope) {

        List<JBCXM__UsageData__c> listUD = new List<JBCXM__UsageData__c>();

        for (JBCXM__UsageData__c UD : scope) {
            
            UD.JBCXM__WeekLabel__c = UD.JBCXM__Date__c.toStartOfWeek();

            listUD.add(UD);
        }

        if (!listUD.isEmpty()) { update listUD; }
    }

    global Database.QueryLocator start(Database.BatchableContext BC) {

        return Database.getQueryLocator(Query);
    }

    global void finish(Database.BatchableContext BC) {}
}