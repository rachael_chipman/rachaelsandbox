trigger OpportunityTeamMember on OpportunityTeamMember (before update, before insert, after insert) {

  
    
	if( Trigger.isBefore && Trigger.isUpdate )
	{
        
		for (OpportunityTeamMember teamMember: Trigger.new)
		{
			if(teamMember.Percent_Commissioned_On__c == null)
			{
				teamMember.Percent_Commissioned_On__c = 100;
			}
		}
	}
	
    if(Trigger.isAfter && Trigger.isInsert )
    {
        System.debug('Trigger .new is :' + Trigger.new);
        List<OpportunityTeamMember> OTMlist = new List<OpportunityTeamMember>();
        List<OpportunityTeamMember> OTMmemberlist = Trigger.new;
        for (OpportunityTeamMember teamMember: OTMmemberlist)
		{
			if(teamMember.Exchange_Rate__c == null)
			  OTMlist.add(teamMember);
        }
        
        //Call the method to populate the exchange Rate on the OTM if its blank and Opportunity is finance approved
		if(OTMlist.size() > 0)
		{
			 List<Opportunity> opplist = getOpportunity(OTMlist);
             if(opplist.size()>0)
             {
                 Map<Id, Opportunity> OppMap = new Map<Id, Opportunity>(opplist);
                 OTMExchangeRateService.updateOppTeamMembers(opplist, OppMap);
             }   
                 
		}
    }
    
    
    
	if( Trigger.isBefore && Trigger.isInsert )
	{
		//List<OpportunityTeamMember> OTMlist = new List<OpportunityTeamMember>();
		
		for (OpportunityTeamMember teamMember: Trigger.new)
		{
						  
			if(teamMember.Percent_Commissioned_On__c == null)
			{
				teamMember.Percent_Commissioned_On__c = 100;
			}
		}
	
	}


   public List<Opportunity> getOpportunity(List<OpportunityTeamMember> OTMlist)
   {
       integer i = 0;
   	  List<Opportunity> opplist = new List<Opportunity>();
   	  List<Id> oppIds = new List<Id>();
   	  if(OTMlist!= null)
   	  {
   	  	for(OpportunityTeamMember OTM : OTMlist)
   	  	{
   	  		oppIds.add(OTM.OpportunityId);
   	  	}
   	  }
   	  if(oppIds.size() > 0)
   	  {
          System.debug('Number of SQl query is : ' + i );
   	  	 opplist = [Select Id, Name,CurrencyISOCode,CloseDate,StageName from Opportunity where Id in:oppIds AND StageName = 'Finance Approved'];
   	  }
   	 
   	 return opplist;
   }

    //Commented out due to strange non existent method error RC
    /*List<OpportunityTeamMember> teamMembersToUpdate =  new List<OpportunityTeamMember>();
    
    if ( Trigger.isBefore && Trigger.isInsert && !OpportunityServices.isRunning ){
        //update the commission on and pod. This part has to be in a before trigger. The logic in the after trigger is dependent on
        // formula fields that are calculated by the fields we update in the before trigger
        Set<Id> opportunityIds = new Set<Id>();
        Map<Id, Opportunity> opportunities = new Map<Id, Opportunity>();
        Set<Id> userIds = new Set<Id>();
        Set<String> isoCodes = new Set<String>();

        for ( OpportunityTeamMember member : Trigger.new ){
            opportunityIds.add ( member.OpportunityId );
            userIds.add ( member.UserId );
        }

        for ( Opportunity opp : [ SELECT Id, closeDate, CurrencyIsoCode 
                                  FROM Opportunity 
                                  WHERE Id IN :opportunityIds 
                                  AND StageName IN :OpportunityServices.validStageNamesforCommissionCalulation ] )
        {
            opportunities.put ( opp.Id, opp );
            isoCodes.add ( opp.CurrencyIsoCode );
        }

        Map<Id, DatedConversionRate> oppIdToConversionRate = OpportunityServices.getDatedConversionRate ( isoCodes, opportunities.values() );

        Map<Id, User> users = new Map<Id, User> ( [SELECT Id, User_Pod_Nickname2__c FROM User WHERE Id IN :userIds ] );

        Map<String, CurrencyType> isoCodeToCurrencyType = OpportunityServices.getisoCodeToCurrencyType( isoCodes );

        for ( OpportunityTeamMember member : Trigger.new ){
            Id oppId = member.OpportunityId;
            if ( opportunities.containsKey ( oppId ) && oppIdToConversionRate.containsKey( oppId ) ){
                    String podName = users.get ( member.UserId ).User_Pod_Nickname2__c;
                    DatedConversionRate dcr = oppIdToConversionRate.get ( oppId );
                    CurrencyType currType = isoCodeToCurrencyType.get ( opportunities.get ( oppId ).CurrencyIsoCode );
                    OpportunityServices.setCommissionPodAndExchangeRate ( member, podName, dcr, currType );
            }
        }
    
    }

    if ( Trigger.isAfter && Trigger.isInsert && !OpportunityServices.isRunning  ){
        OpportunityServices.isRunning = true;
        Set<Id> opportunityIds = new Set<Id>();

        for ( OpportunityTeamMember member : Trigger.new ){
            opportunityIds.add ( member.OpportunityId );
        }

        OpportunityServices.isRunning = true;
        List<Opportunity> oppWithMembers = [SELECT Id, Type, (SELECT User.Name, TeamMemberRole, UserId FROM OpportunityTeamMembers WHERE Id IN :Trigger.new )
                                            FROM Opportunity 
                                            WHERE Id IN :opportunityIds 
                                            AND StageName IN :OpportunityServices.validStageNamesforCommissionCalulation ];
            teamMembersToUpdate = OpportunityServices.calculateCommissionForTeamMembers ( oppWithMembers );

        if ( !teamMembersToUpdate.isEmpty() ){
            update teamMembersToUpdate;
        }
    }*/
}