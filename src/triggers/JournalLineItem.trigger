trigger JournalLineItem on c2g__codaJournalLineItem__c (before insert) {

	if (trigger.isBefore && trigger.isInsert){
		JournalLineItemService.populateJournalInvoiceLineItems(Trigger.new);
	}

}