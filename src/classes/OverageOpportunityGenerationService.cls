public with sharing class OverageOpportunityGenerationService {
	
	public static User SF_AUTO
	{
		get
		{
			if( SF_AUTO == NULL )
			{
				SF_AUTO = [SELECT Id FROM User WHERE Name = 'Salesforce Automation' and isActive = true];
			}
			return SF_AUTO;
		}
		private set;
	}
	
	public void processTrigger(List<Campaign_Preview_Events__c> newEvents, Map<Id, Campaign_Preview_Events__c> oldEvents)
	{
		List<Campaign_Preview_Events__c> filteredIPEvents = filterIPEventsonUpdate(newEvents, oldEvents);
		if(!filteredIPEvents.isEmpty())
		{
			processOverages(filteredIPEvents, newEvents);
		}
	}
	
	/*public void processTriggeronInsert(List<Campaign_Preview_Events__c> newEvents, Map<Id, Campaign_Preview_Events__c> newEventsMap)
	{
		processTrigger(newEvents, null);
	}*/
	
	public static void processOverages(List<Campaign_Preview_Events__c> filteredIPEvents, List<Campaign_Preview_Events__c> newEvents)
	{
		Map<Id, Customer_Product_Detail__c> IPEventtoPPCMap = IPEventsIdtoPPCMap(filteredIPEvents);

		Customer_Product_Detail__c PPC;
		Double IPTotalEvents;
		Decimal IPOverageFee;
		Double IMTotalEvents;
		Decimal IMOverageFee;
		Double BLAs;
		Double RepMon;
		String CombinedEvents;
		Boolean Gmail;
		List<OverageFeeOpportunity> feeOpptys = new List<OverageFeeOpportunity>();
		
		for(Campaign_Preview_Events__c IPEvent : filteredIPEvents)
		{
			System.debug('What is the IPEvent'+IPEvent);
			Double IPOverageEvents;
			System.debug('What is in IPEventtoPPCMap?'+IPEventtoPPCMap);
			if(!IPEventtoPPCMap.isEmpty() && IPEventtoPPCMap.containsKey(IPEvent.Account__c))
			{
				PPC = IPEventtoPPCMap.get(IPEvent.Account__c);
				System.debug('What is the PPC'+PPC);
				IPTotalEvents = PPC.Inbox_Preview_Events__c;
				IPOverageFee = PPC.Inbox_Preview_Fee__c;
				IMTotalEvents = PPC.Inbox_Monitor_Events__c;
				IMOverageFee = PPC.Inbox_Monitor_Fee__c;
				BLAs = PPC.Blacklist_Alert_IPs__c;
				RepMon = PPC.Reputation_Monitor_IPs__c;
				CombinedEvents = PPC.Combined_Events__c;
				IPEvent.Inbox_Preview_Limit__c = PPC.Inbox_Preview_Events__c;

				if(IPEvent.Messages__c > PPC.Inbox_Preview_Events__c && (PPC.Combined_Events__c == 'No' || PPC.Combined_Events__c == null))
				{
					IPOverageEvents = IPEvent.Messages__c - PPC.Inbox_Preview_Events__c;
				}
				else if(IPEvent.Messages__c > PPC.Inbox_Preview_Events__c && PPC.Combined_Events__c == 'Yes') 
				{
					IPOverageEvents = IPEvent.Messages__c - (PPC.Inbox_Preview_Events__c + PPC.Inbox_Monitor_Events__c);
				}
				
				if( IPOverageEvents!= null && PPC.Inbox_Preview_Fee__c != null && IPOverageEvents * PPC.Inbox_Preview_Fee__c > 100)
				{
					Opportunity opty = createFeeOpp(PPC);
					
					OpportunityLineItem feeoli = new OpportunityLineItem();
					feeoli.CP_Overage_Fee__c = IPOverageFee;
					feeoli.of_CP_Events_Allowed__c = IPTotalEvents;
					feeoli.Start_Date__c = system.today().addMonths(-1).toStartofMonth();
					feeoli.End_Date__c = feeoli.Start_Date__c.addMonths(1).toStartofMonth().addDays(-1);
					feeoli.Quantity = 1;
					feeoli.UnitPrice = IPOverageEvents*IPOverageFee;
					feeoli.Description__c = 'IP Overage Fee';
					feeoli.Number_of_Overage_Events__c = IPOverageEvents;
					feeoli.of_BLA_IPS__c = BLAs;
					feeoli.of_Rep_Mon_IPs__c = RepMon;
					feeoli.of_MM_Events_Allowed__c = IMTotalEvents;
					feeoli.MM_Overage_Fee__c = IMOverageFee;
					feeoli.GMail_Optimizer__c = PPC.GMail_Optimizer__c;
					feeoli.Combined_Events__c = CombinedEvents;
					
					feeOpptys.add( new OverageFeeOpportunity(opty, feeoli, PPC.Product_Name__c, PPC.Account__r.CurrencyISOCode ) );
				}
			}
		}
		saveOverageFeeOpportunities( feeOpptys , newEvents);
	}
	
	public class OverageFeeOpportunity
	{
		public Opportunity opty{get; private set;}
		public OpportunityLineItem oli{get; private set;}
		public String productName{get; private set;}
		public String isoCode{get; private set;}
		
		public OverageFeeOpportunity( Opportunity opty, OpportunityLineItem oli, String productName, String isoCode )
		{
			this.opty = opty;
			this.oli = oli;
			this.productName = productName;
			this.isoCode = isoCode;
		}		
	}
	
	public static void saveOverageFeeOpportunities( List<OverageFeeOpportunity> feeOpptys, List<Campaign_Preview_Events__c> newEvents)
	{
		List<Opportunity> feeOpportunities = new List<Opportunity>();
		List<String> productNames = new List<String>();
		List<String> currencyCodes = new List<String>();
		for( OverageFeeOpportunity feeOpty : feeOpptys )
		{
			feeOpportunities.add( feeOpty.opty );
			productNames.add( feeOpty.productName );
			currencyCodes.add( feeOpty.isoCode );
		}
		Savepoint sp = Database.setSavepoint();
		try
		{
			insert feeOpportunities;
		}
		catch (DMLException e)
		{
			for(Integer i = 0; i < e.getNumDml(); i++)
			{
				System.debug(e.getDmlMessage(i));
				for(Campaign_Preview_Events__c triggerEvent : newEvents)
				{
					triggerEvent.addError( e.getDmlMessage ( i ) );
				} 
			}
		}
		
		List<PricebookEntry> pbes = [SELECT Id, Name, CurrencyISOCode FROM PricebookEntry WHERE Name IN :ProductNames and CurrencyISOCode IN :CurrencyCodes and Pricebook2.Name = 'Return Path Pricebook'];
		Map<String, PricebookEntry> currencyProductNamePBEs = new Map<String, PricebookEntry>();
		for(PricebookEntry pbe : pbes )
		{
			currencyProductNamePBEs.put(pbe.Name + pbe.CurrencyISOCode, pbe );
		}
		
		List<OpportunityLineItem> olisToInsert = new List<OpportunityLineItem>();
		for( OverageFeeOpportunity feeOpty : feeOpptys )
		{
			OpportunityLineItem oli = feeOpty.oli;

			oli.OpportunityId = feeOpty.opty.Id;
			String key =  feeOpty.productName + feeOpty.isoCode;
			if( currencyProductNamePBEs.containsKey( key ) )
			{
				oli.PricebookEntryId = currencyProductNamePBEs.get( key ).Id;
				olisToInsert.add( oli );
			}
		}
		try
		{
			insert olisToInsert;
		}
		catch (Exception e)
		{
			for( Integer i = 0; i < e.getNumDml(); i++ )
			{
				system.debug( e.getDmlMessage( i ) );
				for(Campaign_Preview_Events__c triggerEvent : newEvents)
				{
					triggerEvent.addError( e.getDmlMessage( i ) );
				}
			}
			Database.rollback( sp );
		}
	}	
	
	//filter records so that we only have those that have been updated in the Messages field (this is the Usage number). I think this needs to fire on Insert as well...
	public static List<Campaign_Preview_Events__c> filterIPEventsonUpdate (List<Campaign_Preview_Events__c> newEvents, Map<Id, Campaign_Preview_Events__c> oldEvents)
	{
		List<Campaign_Preview_Events__c> filteredIPEvents = new list<Campaign_Preview_Events__c>();
		for(Campaign_Preview_Events__c IPEvent : newEvents)
		{
			if( IPEvent.Messages__c != NULL )
			{
				if( oldEvents == null || oldEvents.get(IPEvent.Id).Messages__c != IPEvent.Messages__c || ( !oldEvents.get(IPEvent.Id).Generate_Fee_Opportunity__c && IPEvent.Generate_Fee_Opportunity__c ) )
				{
					filteredIPEvents.add(IPEvent);
				}
			}
			
		}
		return filteredIPEvents;
	}
	
	//Map the Usage Table to the Product Portfolio, ensuring both belong to the same account
	private static Map<Id,Customer_Product_Detail__c> IPEventsIdtoPPCMap (List<Campaign_Preview_Events__c> filteredIPEvents)
	{
		Map<Id, Customer_Product_Detail__c> IPEventstoPPC = new Map<Id, Customer_Product_Detail__c>();
		List<Id> AccountIds = new List<Id>();
		
		for(Campaign_Preview_Events__c IPEvent : filteredIPEvents)
		{
			AccountIds.add(IPEvent.Account__c);
		}
		
		List<Customer_Product_Detail__c> PPCs = [SELECT Id, Product_Name__c, Combined_Events__c, Product_Family__c, Inbox_Preview_Events__c, Inbox_Preview_Fee__c, Account__c, Account__r.Name, Account__r.CurrencyISOCode, Account__r.OwnerId,
		Inbox_Monitor_Events__c, Inbox_Monitor_Fee__c, Number_of_IP_Addresses__c, Blacklist_Alert_IPs__c, Reputation_Monitor_IPs__c, Gmail_Optimizer__c, Account__r.Owner.IsActive
		FROM Customer_Product_Detail__c 
		WHERE Account__c IN :AccountIds and Product_Family__c = 'Email Intelligence for Marketers'];
		//*system.assert( false, PPCs );
		Map<Id, Customer_Product_Detail__c> productDetailsMap = new Map<Id, Customer_Product_Detail__c>();
		for( Customer_Product_Detail__c productDetail : PPCs )
		{
			productDetailsMap.put( productDetail.Account__c, productDetail);
		}
		return productDetailsMap;
		/*for(Campaign_Preview_Events__c IPEvent : filteredIPEvents)
		{
			if(productDetailsMap.containsKey( IPEvent.Account__c ) )
			{
				IPEventstoPPC.put(IPEvent.Id, productDetailsMap.get( IPEvent.Account__c ) );
			}
		}
		return IPEventstoPPC;*/
		
	} 
	//Build the opportunity using information from the Usage Table
	public static Opportunity createFeeOpp (Customer_Product_Detail__c PPC)
	{
			DateTime dt = system.now();
	        String monthAndYear = dt.format('MMM') + '-' + dt.format('YY');
	        String accountName;
	        if(PPC.Account__r.Name.length() >= 26)
	        {
	            accountName = PPC.Account__r.Name.substring(0,26);
	        }
	        else
	        {
	            accountName = PPC.Account__r.Name;
	        }
        
        Opportunity FeeOpp = new Opportunity ();
		FeeOpp.AccountId = PPC.Account__c;
		FeeOpp.Type = 'Fee';
		FeeOpp.StageName = 'Defining Needs';
		FeeOpp.RecordTypeId = TestUtils.SELECT_RECORD_TYPE.Id;
		FeeOpp.Did_not_Fill_Pre_Pipeline_Exit_Criteria__c = true;
		FeeOpp.Approval_Type__c = 'Pricing Approval';
		if( PPC.Account__r.Owner.IsActive )
		{
			FeeOpp.OwnerId = PPC.Account__r.OwnerId;
		}
		else FeeOpp.OwnerId = SF_AUTO.Id;
		system.debug('%^&*'+FeeOpp.OwnerId);
		FeeOpp.Name = accountName + '_Inbox Preview Fee_' + monthAndYear;
		FeeOpp.CloseDate = system.today();
		FeeOpp.CurrencyISOCode = PPC.Account__r.CurrencyISOCode;
		FeeOpp.of_IPs__c = PPC.Number_of_IP_Addresses__c;
		return FeeOpp;
		
	}
}