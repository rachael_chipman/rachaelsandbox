trigger ManagePrimaryQuote on zqu__Quote__c (before insert, before update, after insert, after update, after delete) {

    if(trigger.isBefore && trigger.isUpdate)
    {
        TerminationQuoteToAccountInfoHelper.pushCancelTermInfoToAccount(trigger.new, trigger.oldMap);
    }
    
    if((trigger.isUpdate || trigger.isInsert) && (trigger.isAfter))
    {
        System.debug('Calling the Service class');
        //ManageQuoteService.processTrigger(trigger.oldMap, trigger.newMap);
        ManageQuoteService.processQuotesForLineItems(trigger.new, trigger.oldMap, true);
    }

    if(trigger.isAfter && trigger.isDelete)
    {
        ManageQuoteService.Delete_OpportunityLineItems(trigger.oldMap, true);
    }

}