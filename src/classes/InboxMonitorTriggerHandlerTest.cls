@isTest(SeeAllData=true)
private class InboxMonitorTriggerHandlerTest {
	

    public static Mailbox_Monitor_Events__c imEvent;
    public static User testUser;
    public static Opportunity requiredOpp;
    
    private static Account testAccount;
    private static List<Account> testAccounts;
    private static Zuora__CustomerAccount__c testBillingAccount;

    private static Zuora__Subscription__c testSub;
    private static Zuora__SubscriptionProductCharge__c testSubProdCharge;
    private static Zuora__Product__c testProduct;

    
    public static void setupObjects()
    {
        testUser = [SELECT Id FROM User WHERE isActive = true LIMIT 1];

        testAccounts = new List<Account>();

        testAccounts.add(testAccount = TestUtils.createAccount(TestUtils.MARKETER_RECORD_TYPE.Id, true));
        system.assert(testAccount != NULL);

        testBillingAccount = TestUtils.createBillingAccounts(testAccounts, true)[0];
        system.assert(testBillingAccount != NULL);

        testSub = TestUtils.createZ_Subscription(testAccount.Id, testBillingAccount.Id, testBillingAccount.Id, 'Test EIS Subscription', 'TERMED', true);
        system.assert(testSub != NULL);
        testSub.Zuora__Status__c = 'Active';
        update testSub;

        testProduct = TestUtils.createZuoraProduct('Email Optimization', true);
        system.assert(testProduct != NULL);

        testSubProdCharge = TestUtils.createZ_SubscriptionCharge('Inbox Monitor Events', testSub.Id, testAccount.Id, testProduct.Id, 'Email Intelligence Solutions - Gold - Monthly', 'Recurring', true);
        system.assert(testSubProdCharge != NULL);
        testSubProdCharge.IncludedUnits__c = 25;
        testSubProdCharge.OveragePrice__c = 25.00;
        update testSubProdCharge;
        
        requiredOpp = new Opportunity (Name = 'Test Opportunity', AccountId = testAccount.Id, StageName = 'Finance Approved', CloseDate = system.today().addDays(-365));
        insert requiredOpp;
        
        imEvent = new Mailbox_Monitor_Events__c();
        imEvent.Account__c = testAccount.Id;
        imEvent.Opportunity__c = requiredOpp.Id;
        imEvent.Campaigns__c = 50;
    }

    private static testMethod void testIMEventInsert()
    {
    	setupObjects();

    	test.startTest();
    	insert imEvent;
    	test.stopTest();

    	List<Mailbox_Monitor_Events__c> newEvents = [SELECT Id, Inbox_Monitor_Limit__c FROM Mailbox_Monitor_Events__c WHERE Id = : imEvent.Id];
    	system.assertEquals(testSubProdCharge.IncludedUnits__c, newEvents[0].Inbox_Monitor_Limit__c, 'The limit field should be updated.');
    }
	
}