@isTest

public class MOneDispositionCreate_test{

    private static testmethod void testMOneDispositionCreate(){
    
    User user = new user(Alias = 'standt', Email='tes@rp.com', 
      EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
      LocaleSidKey='en_US', ProfileId = '00e0000000734LT', 
      TimeZoneSidKey='America/Los_Angeles', UserName='test@rp.com.rp');
      insert user;  
      
    User user2 = new user(Alias = 'standt2', Email='tes@rp.com', 
      EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
      LocaleSidKey='en_US', ProfileId = '00e0000000734LT', 
      TimeZoneSidKey='America/Los_Angeles', UserName='test@rp.com.rp2');
      insert user2;    
      
    Lead l = new Lead(Company = 'Test Company', OwnerId = user.Id, FirstName = 'Rocky',
        LastName = 'Thecat');
        insert l;
        
    l.Disposition_Admin_Only__c = true;
    l.OwnerId = user2.Id;
    update l;
    }
}