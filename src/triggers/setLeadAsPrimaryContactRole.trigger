trigger setLeadAsPrimaryContactRole on Lead (after update) {
     List<Lead> convertedLeadsToProcess = new List<Lead>();
                    
                    for (Lead l : (List<Lead>)Trigger.new){
                        if (((Lead)trigger.oldMap.get(l.Id)).isConverted == false && l.isConverted == true
                            && l.ConvertedOpportunityId != null && l.ConvertedContactId != null)
                            convertedLeadsToProcess.add(l);
                    }
                    if (convertedLeadsToProcess.size() > 0){
                        Set<Id> OpptyIdSet = new Set<Id>();
                        Set<Id> ContactIdSet = new Set<Id>();
                        for(Lead l: convertedLeadsToProcess){
                                OpptyIdSet.add(l.ConvertedOpportunityId);
                                ContactIdSet.add(l.ConvertedContactId);
                        }
                        // Map<OpportunityId, Map<ContactId, OpportunityContactRole>>
                        Map<Id, Map<Id, OpportunityContactRole>> oCRMap = new Map<Id, Map<Id, OpportunityContactRole>>();
                        for(OpportunityContactRole oCR :[SELECT Id,IsPrimary,OpportunityId,ContactId FROM OpportunityContactRole WHERE OpportunityId IN :OpptyIdSet AND ContactId IN :ContactIdSet]){
                        
                            if (oCR.IsPrimary == false){
                                if (!oCRMap.containsKey(oCR.OpportunityId))
                                    oCRMap.put(oCR.OpportunityId, new Map<Id, OpportunityContactRole>());
                                
                                oCRMap.get(oCR.OpportunityId).put(oCR.ContactId,oCR);
                            }
                        }
 
                        List<OpportunityContactRole> oCRToUpdate = new List<OpportunityContactRole>();
                        for(Lead l: convertedLeadsToProcess){
                                if (oCRMap.ContainsKey(l.ConvertedOpportunityId) && oCRMap.get(l.ConvertedOpportunityId).containsKey(l.ConvertedContactId)){
                                    OpportunityContactRole oCR = oCRMap.get(l.ConvertedOpportunityId).get(l.ConvertedContactId);
                                    oCR.IsPrimary = true;
                                    oCRToUpdate.add(oCR);
                                }
                        }
                        if (oCRToUpdate.size() > 0){
                            update oCRToUpdate;
                        }
                    }
 
 
}