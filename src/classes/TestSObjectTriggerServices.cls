@isTest
private class TestSObjectTriggerServices {

    static testMethod void testIsValidCampaign_NullString() 
    {
        test.startTest();
        	Boolean result = SObjectTriggerServices.isValidCampaign( NULL );
        test.stopTest();
        System.assert( !result, 'The result should be false when the string is null' );
    }
    
    static testMethod void testIsValidCampaign_DELIVERABILITY_CHANNEL_REFERRAL() 
    {
        test.startTest();
        	Boolean result = SObjectTriggerServices.isValidCampaign( SObjectTriggerServices.DELIVERABILITY_CHANNEL_REFERRAL );
        test.stopTest();
        System.assert( result, 'The result should be true when the string is DELIVERABILITY CHANNEL REFERRAL' );
    }
    
    static testMethod void testIsValidCampaign_CHANNEL_PROJECT_RUN_REFERRAL() 
    {
        test.startTest();
        	Boolean result = SObjectTriggerServices.isValidCampaign( SObjectTriggerServices.CHANNEL_PROJECT_RUN_REFERRAL );
        test.stopTest();
        System.assert( result, 'The result should be true when the string is CHANNEL PROJECT RUN REFERRAL' );
    }
    
    static testMethod void testIsValidCampaign_SS_WIDGET() 
    {
        test.startTest();
        	Boolean result = SObjectTriggerServices.isValidCampaign( SObjectTriggerServices.SS_WIDGET );
        test.stopTest();
        System.assert( result, 'The result should be true when the string is SS WIDGET' );
    }
}