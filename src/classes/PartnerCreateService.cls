public without sharing class PartnerCreateService {
    
    public void processTrigger(List<Opportunity> newOpportunities, Map<Id, Opportunity> OldOpps)
    {
        List<Opportunity> filteredOpportunities = filterOpportunitiesonUpdate(newOpportunities, OldOpps);
        List<Opportunity> opportunitiesWithPartners = getPartners(filteredOpportunities);
        if(!filteredOpportunities.isEmpty())
        {
        	processPartners(opportunitiesWithPartners, newOpportunities);
        }
    }
    
    public void processPartners(List<Opportunity> filteredOpportunitiesWithPartners, List<Opportunity> newOpportunities)
    {
        List<Partner> PartnerstoDelete = new List<Partner>();
        List<Partner> PartnerstoInsert = new List<Partner>();
        Map<Id, Partner> OppIdtoPartnerRecord = getPartnerforOpportunity(filteredOpportunitiesWithPartners);
        
        for(Opportunity o : filteredOpportunitiesWithPartners)
        {
            if(o.PartnerLookup__c != null)
                {
                    PartnerstoInsert.add(createPartner(o));
                    break;
                }
            
            if(!OppIdtoPartnerRecord.isEmpty() && OppIdtoPartnerRecord.containsKey(o.Id))
            {
                for(Partner partner : o.Partners)
                {
                    if(o.PartnerLookup__c == null)
                    {
                        PartnerstoDelete.add(partner);
                        break;
                    }
                    if(partner.AccounttoId != o.PartnerLookup__c && partner.AccountfromId != o.PartnerLookup__c)
                    {
                        PartnerstoDelete.add(partner);
                        break;
                    }
                    
                }
            }
        }
        if(!PartnerstoDelete.isEmpty())
        {
            delete PartnerstoDelete;
        }
        if(!PartnerstoInsert.isEmpty())
        {
            insert PartnerstoInsert;
        }
    }
    
    public static List<Opportunity> filterOpportunitiesonUpdate(List<Opportunity> newOpportunities, Map<Id,Opportunity> OldOpps)
    {
        List<Opportunity> filteredOpportunities = new List<Opportunity>();
        for(Opportunity o : newOpportunities)
        {
            Opportunity OldOpp = OldOpps.get(o.Id);
            
            if(o.PartnerLookup__c != null && OldOpp.PartnerLookup__c == null || o.PartnerLookup__c != OldOpp.PartnerLookup__c || o.PartnerLookup__c == null && OldOpp.PartnerLookup__c != null)
            {
                filteredOpportunities.add(o);
            }
        }
        return filteredOpportunities;
    }
    
    public static List<Opportunity> getPartners (List<Opportunity> filteredOpportunities)
    {
                
       return [SELECT Id, PartnerLookup__c, (SELECT Id, AccounttoId, AccountfromId, OpportunityId FROM Partners WHERE OpportunityId IN: filteredOpportunities)
                FROM Opportunity WHERE Id IN : filteredOpportunities]; // this relationship query isn't working I have to first do a describeSObjects call and from the results gather the information i need to make the query
                               
    }
    
    private Map<Id, Partner> getPartnerforOpportunity (List<Opportunity> filteredOpportunitiesWithPartners)
    {
        Map<Id, Partner> OpportunityIdtoPartner = new Map<Id, Partner>();
        for(Opportunity o : filteredOpportunitiesWithPartners)
        {
            for(Partner partner : o.Partners)
            {
                if(partner.OpportunityId == o.Id)
                {
                    OpportunityIdtoPartner.put(o.Id, partner);
                }               
            }
        }
        return OpportunityIdtoPartner;
    }
    
    public static Partner createPartner(Opportunity o)
    {
        Partner PartnertoInsert = new Partner();
        PartnertoInsert.OpportunityId = o.Id;
        PartnertoInsert.AccounttoId = o.PartnerLookup__c;
        return PartnertoInsert;
    }

}