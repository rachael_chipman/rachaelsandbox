trigger Campaign_Member_to_Cert_App on CampaignMember (After Insert, After Update) 
{
    static c2g__codaGeneralLedgerAccount__c ledgerAccount_ARTRADE
    {
        get
        {
            if(ledgerAccount_ARTRADE == null)
            {   System.debug('GETTER CALLED!');
                ledgerAccount_ARTRADE = [Select name from c2g__codaGeneralLedgerAccount__c Where Name = '1100 - AR-TRADE' Limit 1];
                System.debug(ledgerAccount_ARTRADE);
            }
            return ledgerAccount_ARTRADE;
        }
    }
    
    for (CampaignMember cm: Trigger.New)
    {

        if(cm.CampaignId == '701000000004fLn' && cm.Certification_Application_Created__c == false && cm.LeadId != null)
        {
            //Need to add a piece of code to recall the lead from an approval process if it's in one

            for(Lead L: [select id, isConverted, Company, SSC_Promo_Code__c, Clear_Opt_out_process__c, Name, 
             D_U_N_S_Number__c, Billing_Address__c, Billing_Address_2__c, Billing_City__c, Billing_Country__c , Billing_Email_Address__c, Billing_First_Name__c,Billing_Last_Name__c,  Billing_State__c, Billing_Zip__c, Email_Sent_from__c from Lead where Id = :cm.LeadId])
            {

                if (L.isConverted == false) 
                {

                    L.Remove_Workflow_Rule__c = true;
                    update L;
                    
                    Database.LeadConvert lc = new Database.LeadConvert();
                    lc.setLeadId(L.Id);
                    lc.setDoNotCreateOpportunity(true);
                    lc.setConvertedStatus('Qualified');
                    Database.LeadConvertResult lcr = Database.convertLead(lc);

                    System.debug('LEAD CONVERTED!');
                    
                    for(Account acct: [select id, Name from Account where Id = :lcr.AccountId])
                    {   
                        acct.Name = L.Company;
                        update acct;
                    }

                        for(Contact con: [select id, Name from Contact where Id = :lcr.ContactId])
                        { 
                            con.Promo_Code__c = L.SSC_Promo_Code__c;
                            con.Clear_Opt_out_process__c = L.Clear_Opt_Out_Process__c;
                            con.DUNS_Number__c = L.D_U_N_S_Number__c;
                            con.Do_you_send_email_from__c = L.Email_Sent_from__c;   
                            update con;}
                }
            }

            for(CampaignMember cm2Update: [select id, Lead_Converted__c from CampaignMember where Id = :cm.Id])
            {
               update cm2Update;
            } 

       }
       
       
       else
       {       

            if(cm.CampaignId == '701000000004fLn' && cm.Certification_Application_Created__c == false && cm.ContactId != null)
            {

                Contact c = [select Id, AccountId, Business_Begin_Date__c, Business_Model_Includes__c, Communication_URLs__c, Do_you_send_out__c, DUNS_Number__c, 
                Email_Collection_Methods__c, Email_Sent_From__c, How_long_have_you_been_in_business__c, How_often_do_you_mail__c, IP_Addresses__c, Legal_Action__c, 
                Legal_Action_Explained__c, Mailing_Domains__c, Method_of_Payment__c, Non_Profit_Id__c, Page_Left_Off__c, Peer_Initiatied_mail_URLs__c, Privacy_Policy_URL__c, 
                Promo_Code__c, Sender_Category__c, Tax_ID_Number__c, Technical_Info_Email__c, Technical_Info_First_Name__c, Technical_Info_Last_Name__c, Technical_Info_Phone__c, 
                Third_Party_Content__c, Type_of_Email_Sent__c, URL_Collecting_Email__c, URL_Collecting_Email_3rd_Party__c, 
                What_is_your_MTA_and_version__c, Who_is_your_ESP__c, Cert_Volume_to_App_Fee__c, Cert_Volume_to_Max_Volume__c, Cert_Volume_to_Cert_Tier__c,
                Abuse_and_Postmaster_Addresses__c, Agreement_Effective_Date__c, Agreement_Expire_Date__c, Agreement_ID__c, Agreement_Version__c, 
                App_Fee_Method_of_Payment__c, Applicant_Email_Address__c, Applicant_First_Name__c, Applicant_Last_Name__c, Applicant_Phone__c, 
                Applied_to_Cert_Previously__c, Are_your_IP_addresses__c, Billing_Address__c, Billing_Address_2__c, Billing_City__c, Billing_Country__c,
                Clear_Indication_of_Kind_of_Email__c, Clear_Opt_Out_Process__c, Clear_States_use_of_Subscriber_Info__c, Consent_Types__c, 
                Delivery_Environment_Protected__c, Disclose_Nature_of_Email__c, Disclose_Sharing_of_Email__c, Due_Diligence_on_Appropriate_Consent__c,
                Easy_Access_to_Unsubscribe__c, Fully_Qualified_Reverse_DNS__c, Global_Unsubscribe__c, Homepage_URLs__c, How_did_you_hear_about_us__c, 
                How_many_days_sending_over_IPs__c, Kind_of_email_as_ESP__c, Mailing_Address_2__c, MailingStreet, MailingCity, MailingCountry, 
                MailingState, MailingPostalCode, Billing_First_Name__c, Billing_Last_Name__c, Partner_Consent_Types__c, Peer_Non_Response_Standard__c, 
                Privacy_Policy_Linked__c, Privacy_Policy_Mailing_Addr_Valid__c, Registered_with_Abuse_net__c, Reliably_Recieve_Process_Bounces__c, 
                Publish_Sender_ID_Compliant__c, Signed_by_Company__c, Signed_by_Email__c, Signed_by_IP__c, Signed_by_Name__c, Signed_by_Title__c, Billing_Email_Address__c,
                Social_Peer_Network_Standard__c, Standards_Accountable__c, Standards_Compliance__c, Standards_Read__c, Standards_Version__c, Third_Parties_From__c, 
                Billing_State__c, Billing_Zip__c, Business_Description__c, Certification_Business_Model__c, Channels_for_Consent_Collection__c, Knowledge_of_Program_Details__c, 
                Sends_third_party_mail__c, Sends_corporate_mail__c, Sends_one_to_one_mail__c, Consent_Information_Accessible__c, Consent_Third_Party_Emails__c, Consent_Shared_Ips__c, 
                Consent_Corporate_Network__c, Consent_New_Domains__c, Consent_Rent_Purchase_Emails__c, Consent_One_to_one_Corporate_Mail__c, Application_URL__c
                from Contact where Id = :cm.ContactId];   


                Certification_Application__c ca = new Certification_Application__c (
                    RecordTypeId = [SELECT Id, DeveloperName FROM RecordType WHERE DeveloperName = 'Cert_App' and SObjectType = 'Certification_Application__c' LIMIT 1].Id,
                    Account__c = c.AccountId,
                    Application_Status__c = 'Application Submitted',
                    Business_Begin_Date__c = c.Business_Begin_Date__c,
                    Business_Model_Includes__c = c.Business_Model_Includes__c,
                    Certification_App_Fee__c = c.Cert_Volume_to_App_Fee__c,
                    Certification_Max_Volume__c = c.Cert_Volume_to_Max_Volume__c,
                    Certification_Volume__c = c.Cert_Volume_to_Cert_Tier__c,
                    Communication_URLs__c = c.Communication_URLs__c,
                    Do_you_send_out__c = c.Do_you_send_out__c,
                    DUNS_Number__c = c.DUNS_Number__c,
                    Email_Collection_Methods__c = c.Email_Collection_Methods__c,
                    Email_Sent_From__c = c.Email_Sent_From__c,
                    How_long_have_you_been_in_business__c = c.How_long_have_you_been_in_business__c,
                    How_often_do_you_mail__c = c.How_often_do_you_mail__c,
                    IP_Addresses__c = c.IP_Addresses__c,
                    Legal_Action__c = c.Legal_Action__c,
                    Legal_Action_Explained__c = c.Legal_Action_Explained__c,
                    Mailing_Domains__c = c.Mailing_Domains__c,
                    Method_of_Payment__c = c.Method_of_Payment__c,
                    Non_Profit_Id__c = c.Non_Profit_Id__c,
		            OwnerID = '005000000074sVo', //Salesforce automation
		            Page_Left_Off__c = c.Page_Left_Off__c,
		            Peer_Initiatied_mail_URLs__c = c.Peer_Initiatied_mail_URLs__c,
		            Privacy_Policy_URL__c = c.Privacy_Policy_URL__c,
		            Promo_Code__c = c.Promo_Code__c,
		            Sender_Category__c = c.Sender_Category__c,
		            Tax_ID_Number__c = c.Tax_ID_Number__c,
		            Technical_Info_Email__c = c.Technical_Info_Email__c,
		            Technical_Info_First_Name__c = c.Technical_Info_First_Name__c,
		            Technical_Info_Last_Name__c = c.Technical_Info_Last_Name__c,
		            Technical_Info_Phone__c = c.Technical_Info_Phone__c,
		            Third_Party_Content__c = c.Third_Party_Content__c,
		            Type_of_Email_Sent__c = c.Type_of_Email_Sent__c,
		            URL_Collecting_Email__c = c.URL_Collecting_Email__c,
		            URL_Collecting_Email_3rd_Party__c = c.URL_Collecting_Email_3rd_Party__c,
		            What_is_your_MTA_and_version__c = c.What_is_your_MTA_and_version__c,
		            Who_is_your_ESP__c = c.Who_is_your_ESP__c,
		            Abuse_and_Postmaster_Addresses__c =  c.Abuse_and_Postmaster_Addresses__c,
		            Agreement_Effective_Date__c =  c.Agreement_Effective_Date__c,
		            Agreement_Expire_Date__c =  c.Agreement_Expire_Date__c,
		            Agreement_ID__c =  c.Agreement_ID__c,
		            Agreement_Version__c =  c.Agreement_Version__c,
		            App_Fee_Method_of_Payment__c =  c.App_Fee_Method_of_Payment__c,
		            Applicant_Email_Address__c =  c.Applicant_Email_Address__c,
		            Applicant_First_Name__c =  c.Applicant_First_Name__c,
		            Applicant_Last_Name__c =  c.Applicant_Last_Name__c,
		            Applicant_Phone__c =  c.Applicant_Phone__c,
		            Applied_to_Certification_Previously__c =  c.Applied_to_Cert_Previously__c,
		            Are_your_IP_addresses__c =  c.Are_your_IP_addresses__c,
		            Billing_Address__c =  c.Billing_Address__c,
		            Billing_Address_2__c =  c.Billing_Address_2__c,
		            Billing_City__c =  c.Billing_City__c,
		            Billing_Country__c =  c.Billing_Country__c,
		            Billing_Email_Address__c =  c.Billing_Email_Address__c,
		            Billing_State__c =  c.Billing_State__c,
		            Billing_Zip__c =  c.Billing_Zip__c,
		            Business_Description__c =  c.Business_Description__c,
		            Certification_Business_Model__c =  c.Certification_Business_Model__c,
		            Channels_for_Consent_Collection__c =  c.Channels_for_Consent_Collection__c,
		            Clear_Indication_of_Kind_of_Email__c =  c.Clear_Indication_of_Kind_of_Email__c,
		            Clear_Opt_Out_Process__c =  c.Clear_Opt_Out_Process__c,
		            Clear_States_use_of_Subscriber_Info__c =  c.Clear_States_use_of_Subscriber_Info__c,
		            Consent_Types__c =  c.Consent_Types__c,
		            Delivery_Environment_Protected__c =  c.Delivery_Environment_Protected__c,
		            Disclose_nature_of_email__c =  c.Disclose_Nature_of_Email__c,
		            Disclose_sharing_of_email__c =  c.Disclose_Sharing_of_Email__c,
		            Due_Diligence_on_Appropriate_Consent__c =  c.Due_Diligence_on_Appropriate_Consent__c,
		            Easy_Access_to_Unsubscribe__c =  c.Easy_Access_to_Unsubscribe__c,
		            Fully_Qualified_Reverse_DNS_Y_N__c =  c.Fully_Qualified_Reverse_DNS__c,
		            Global_Unsubscribe__c =  c.Global_Unsubscribe__c,
		            Homepage_URLs__c =  c.Homepage_URLs__c,
		            How_did_you_hear_about_us__c =  c.How_did_you_hear_about_us__c,
		            How_many_days_sending_over_IPs__c =  c.How_many_days_sending_over_IPs__c,
		            Kind_of_email_as_ESP__c =  c.Kind_of_email_as_ESP__c,
		            Mailing_Address_2__c =  c.Mailing_Address_2__c,
		            Mailing_City__c =  c.MailingCity,
		            Mailing_Country__c =  c.MailingCountry,
		            Mailing_Zip__c =  c.MailingPostalCode,
		            Mailing_State__c =  c.MailingState,
		            Mailing_Address__c =  c.MailingStreet,
		            Billing_Contact__c = c.Billing_First_Name__c + ' ' + c.Billing_Last_Name__c,
		            Partner_Consent_Types__c =  c.Partner_Consent_Types__c,
		            Peer_Non_Response_Standard__c =  c.Peer_Non_Response_Standard__c,
		            Privacy_Policy_Linked_Y_N__c =  c.Privacy_Policy_Linked__c,
		            Privacy_Policy_Mailing_Addr_Valid__c =  c.Privacy_Policy_Mailing_Addr_Valid__c,
		            Sender_ID_Compliant_SPF_Y_N__c =  c.Publish_Sender_ID_Compliant__c,
		            Registered_With_Abuse_net__c =  c.Registered_with_Abuse_net__c,
		            Reliably_Recieve_Process_Bounces__c =  c.Reliably_Recieve_Process_Bounces__c,
		            Signed_by_Company__c =  c.Signed_by_Company__c,
		            Signed_by_Email__c =  c.Signed_by_Email__c,
		            Signed_by_IP__c =  c.Signed_by_IP__c,
		            Signed_by_Name__c =  c.Signed_by_Name__c,
		            Signed_by_Title__c =  c.Signed_by_Title__c,
		            Social_Peer_Network_Standard__c =  c.Social_Peer_Network_Standard__c,
		            Standards_Accountable__c =  c.Standards_Accountable__c,
		            Standards_Compliance__c =  c.Standards_Compliance__c,
		            Standards_Read__c =  c.Standards_Read__c,
		            Standards_Version__c =  c.Standards_Version__c,
		            Third_Parties_From__c =  c.Third_Parties_From__c,
		            Certification_Type__c = 'RP Certification',
		            Knowledge_of_Program_Details__c = c.Knowledge_of_Program_Details__c,
					Sends_third_party_mail__c = c.Sends_third_party_mail__c,
					Sends_corporate_mail__c = c.Sends_corporate_mail__c,
					Sends_one_to_one_mail__c = c.Sends_one_to_one_mail__c,
					Consent_Information_Accessible__c = c.Consent_Information_Accessible__c,
					Consent_Third_Party_Emails__c = c.Consent_Third_Party_Emails__c,
					Consent_Shared_Ips__c = c.Consent_Shared_Ips__c,
					Consent_Corporate_Network__c = c.Consent_Corporate_Network__c,
					Consent_New_Domains__c = c.Consent_New_Domains__c,
					Consent_Rent_Purchase_Emails__c = c.Consent_Rent_Purchase_Emails__c,
					Consent_One_to_one_Corporate_Mail__c = c.Consent_One_to_one_Corporate_Mail__c, 
                	Application_URL__c = c.Application_URL__c);




                    if(c.Promo_Code__c != null)
                    {

                     try{  Promo_Code__c pc = [select Id, Name, Promo_Type__c, Account__c from Promo_Code__c where Name = :c.Promo_Code__c Limit 1];
                        Account pcacct = [select Id, OwnerId, CRM__c from Account where Id = :pc.Account__c];
                        ca.Partner_Account__c = pc.Account__c; 

                        ca.Partner_CRM__c = pcacct.CRM__c;
                        ca.Promo_Type_Picklist__c = pc.Promo_Type__c;}
                        catch(Exception e){}     
                    }

                    else
                    {
                        ca.Promo_Type_Picklist__c = 'Walk-Up'; 
                    }

                insert ca;      
 
 					
                    Account acct2 =  [select id, Name, ShippingCity, ShippingCountry, ShippingPostalCode, ShippingState, ShippingStreet from Account where Id = :c.AccountId];
                    
       /*
                    acct2.ShippingStreet = c.MailingStreet;
                    acct2.ShippingState = c.MailingState;
                    acct2.ShippingCity = c.MailingCity;
                    acct2.ShippingPostalCode = c.MailingPostalCode;
                    acct2.ShippingCountry = c.MailingCountry;
                    acct2.BillingStreet = c.Billing_Address__c;
                    acct2.BillingState = c.Billing_State__c;
                    acct2.BillingCity = c.Billing_City__c;
                    acct2.BillingPostalCode = c.Billing_Zip__c;
                    acct2.BillingCountry = c.Billing_Country__c;       
                    acct2.DunsNumber = c.DUNS_Number__c;
              */

                    //Check if billing email is associate to another contact on the same account
                    List<Contact>  contactsWithSameBillingEmail = [SELECT  Id, Email, ContactsRole__c FROM Contact WHERE AccountId =: acct2.Id AND Email =:c.Billing_Email_Address__c AND Id !=: c.Id ];
                    System.debug('CONTACTS WITH BILLING EMAIL' + contactsWithSameBillingEmail);
                    if(!contactsWithSameBillingEmail.isEmpty())
                    {
                        acct2.Invoicing_Contact__c = contactsWithSameBillingEmail[0].Id;
                        acct2.c2g__CODAFinanceContact__c = contactsWithSameBillingEmail[0].Id;
                        acct2.c2g__CODAInvoiceEmail__c = contactsWithSameBillingEmail[0].Email;
                        contactsWithSameBillingEmail[0].ContactsRole__c += ';Billing Contact';
                        update contactsWithSameBillingEmail[0]; 
                    }

                    else
                    {
                        Contact newContact = new Contact (FirstName = c.Billing_First_Name__c, LastName  = c.Billing_Last_Name__c, Email = c.Billing_Email_Address__c, AccountId = acct2.Id, ContactsRole__c = 'Billing Contact', Title = 'Billing Contact');
                        insert newContact;
                        acct2.Invoicing_Contact__c = newContact.Id;
                        acct2.c2g__CODAInvoiceEmail__c = newContact.Email;
                        acct2.c2g__CODAFinanceContact__c = newContact.Id;
                            
                    }

                    acct2.c2g__CODAAccountsReceivableControl__c = ledgerAccount_ARTRADE.Id;
                    update acct2;
 				

                for(Contact c2: [select Id, RPC_App_Successfully_Created__c from Contact where Id = :cm.ContactId])
                {
                    c2.RPC_App_Successfully_Created__c = true;
                    update c2;
                } 

                for(CampaignMember cm2Update2: [select id, Certification_Application_Created__c from CampaignMember where Id = :cm.Id])
                {   
                   cm2Update2.Certification_Application_Created__c = true; 
                   cm2Update2.Certification_Application__c = ca.Id;  
                   update cm2Update2;
               }

             

            }    
     }
}  // END OF FOR LOOP ON TRIGGER.NEW    
  
}