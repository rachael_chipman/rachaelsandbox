@isTest
private class TestSalesInvoiceLineItemService {
	public static Account testAccount;
	public static product2 testProduct;
	public static c2g__codaGeneralLedgerAccount__c testGeneralLedgerAccount;
	public static integer NUM_OF_OPPORTUNITIES = 10;
	public static List<c2g__codaJournal__c> testJournals = new List<c2g__codaJournal__c>();
	public static integer NUM_OF_JOURNALS = 10;
	public static List<c2g__codaGroupingReference__c> testGroups = new List<c2g__codaGroupingReference__c>();
	public static List<Opportunity> testOpps = new List<Opportunity>();
	public static List<c2g__codaInvoice__c> testSalesInvs = new List<c2g__codaInvoice__c>();
	public static List<c2g__codaInvoiceLineItem__c> testSalesInvLineItems = new List<c2g__codaInvoiceLineItem__c>();
	public static List<c2g__codaInvoiceLineItem__c> beforeinsertSalesInvoiceLineItems = new List<c2g__codaInvoiceLineItem__c>();
	public static List<c2g__codaInvoiceLineItem__c> afterinsertSalesInvoiceLineItems = new List<c2g__codaInvoiceLineItem__c>();
				
	static void datasetup(){
		testGeneralLedgerAccount = TestUtils.createGeneralLedgerAccount(true);
		testAccount = TestUtils.createAccount_FinancialForce( TestUtils.PARTNER_RECORD_TYPE.Id, testGeneralLedgerAccount.Id, true );
		testProduct = TestUtils.createProduct_FinancialForce( testGeneralLedgerAccount.Id, true );
		PriceBookEntry testPBE = TestUtils.createPricebookEntry( testProduct.Id, TestUtils.STANDARD_PRICE_BOOK.Id, true );
		
		//testOpps = TestUtils.createOpportunities( NUM_OF_OPPORTUNITIES, TestUtils.AWARENESS_RECORD_TYPE.Id, testAccount.Id, true );
		testOpps = TestUtils.createOpportunities( NUM_OF_OPPORTUNITIES, TestUtils.RESEARCH_RECORD_TYPE.Id, testAccount.Id, true );
		testSalesInvs = new List<c2g__codaInvoice__c>();
		testSalesInvLineItems = new List<c2g__codaInvoiceLineItem__c>();
		
		for( Opportunity anOpportunity : testOpps )
		{
			TestUtils.createSalesInvoice( testAccount.Id, new Map<String,Object>{ 'c2g__Opportunity__c' => anOpportunity.Id }, false );
			testSalesInvs.add( TestUtils.createSalesInvoice( testAccount.Id, new Map<String,Object>{ 'c2g__Opportunity__c' => anOpportunity.Id }, false ) );
		}

		insert testSalesInvs;
		
		for( c2g__codaInvoice__c testSalesInvoice: testSalesInvs )
		{
			testSalesInvLineItems.add( TestUtils.createSalesInvoiceLineItem( testSalesInvoice.Id, testProduct.Id, false ) );
		}
		testJournals.addAll(TestUtils.createJournals(NUM_OF_JOURNALS,null,false));
		testGroups = testUtils.createGroupReferences(NUM_OF_JOURNALS,null,true);
	}
	
	@isTest(seeAllData=true) 	
	private static void testSalesInvoiceLineItem_includeNewGroups() 
	{
		datasetup();

		insert testSalesInvLineItems;

		insert testJournals;
		Map<ID,c2g__codaInvoiceLineItem__c> oldlineItemsMap = new Map<ID,c2g__codaInvoiceLineItem__c>([Select Id, c2g__IncomeScheduleGroup__c,c2g__Invoice__r.c2g__Opportunity__c From  c2g__codaInvoiceLineItem__c where id in :testSalesInvLineItems]); 		
		
		Map<ID,c2g__codaInvoiceLineItem__c> newlineItemsMap = new Map<ID,c2g__codaInvoiceLineItem__c>([Select Id, c2g__IncomeScheduleGroup__c,c2g__Invoice__r.c2g__Opportunity__c From  c2g__codaInvoiceLineItem__c where id in :testSalesInvLineItems]); 		
		
		for (integer i=0;i<NUM_OF_OPPORTUNITIES;i++){
			testSalesInvLineItems[i].c2g__IncomeScheduleGroup__c = testGroups[i].id;
			newlineItemsMap.put(testSalesInvLineItems[i].id,testSalesInvLineItems[i]);
		}
		test.startTest();
			List<c2g__codaInvoiceLineItem__c> filteredSalesInvoiceLineItems = SalesLineItemJournalService.filterOnscheduleGroupAdded(oldlineitemsMap,newlineItemsMap.values());
		test.stopTest();
	
		System.assertEquals(NUM_OF_JOURNALS,filteredSalesInvoiceLineItems.size(),'No salesInvoice line items are returned ');
	}
	
	
	@isTest(seeAllData=true) 	
	private static void testSalesInvoiceLineItem_excludeNotNewGroups() 
	{
		datasetup();

		afterinsertSalesInvoiceLineItems = testSalesInvLineItems;
		insert testJournals;
		for (integer i=0;i<NUM_OF_OPPORTUNITIES;i++){
			testSalesInvLineItems[i].c2g__IncomeScheduleGroup__c = testGroups[i].id;
		}
		insert testSalesInvLineItems;
		
		List<c2g__codaGroupingReference__c> othertestGroups = new List<c2g__codaGroupingReference__c>();
		othertestGroups = testUtils.createGroupReferences(NUM_OF_JOURNALS,null,true);
		for (integer i=0;i<NUM_OF_OPPORTUNITIES;i++){
			afterinsertSalesInvoiceLineItems[i].c2g__IncomeScheduleGroup__c = othertestGroups[i].id;
		}
		Map<ID,c2g__codaInvoiceLineItem__c> lineItemsMap = new Map<ID,c2g__codaInvoiceLineItem__c>([Select Id, c2g__IncomeScheduleGroup__c,c2g__Invoice__r.c2g__Opportunity__c From  c2g__codaInvoiceLineItem__c where id in :testSalesInvLineItems]); 		
		update afterinsertSalesInvoiceLineItems;
		Map<ID,c2g__codaInvoiceLineItem__c> newlineItemsMap = new Map<ID,c2g__codaInvoiceLineItem__c>([Select Id, c2g__IncomeScheduleGroup__c,c2g__Invoice__r.c2g__Opportunity__c From  c2g__codaInvoiceLineItem__c where id in :afterinsertSalesInvoiceLineItems]); 		

		test.startTest();
			List<c2g__codaInvoiceLineItem__c> filteredSalesInvoiceLineItems = SalesLineItemJournalService.filterOnscheduleGroupAdded(lineItemsMap,newlineItemsMap.values());
		test.stopTest();
		System.assertEquals(0,filteredSalesInvoiceLineItems.size(),'No salesInvoice line items are returned '+'beforeinsertSalesInvoiceLineItems '+beforeinsertSalesInvoiceLineItems+ ' lineItemsMap '+lineItemsMap);

	}
	
	@isTest(seeAllData=true) 	
	private static void testSalesInvoiceLineItemUpdates() 
	{
		datasetup();

		insert testSalesInvLineItems;
		for (integer i=0;i<NUM_OF_OPPORTUNITIES;i++){
			testJournals[i].c2g__IncomeScheduleGroup__c = testGroups[i].id;
		}		
		List<c2g__codaInvoiceLineItem__c> lineItems = [Select Id, Name, c2g__IncomeScheduleGroup__c,Opportunity__c,c2g__Invoice__r.c2g__Opportunity__c From  c2g__codaInvoiceLineItem__c 
														where id in : testSalesInvLineItems ];		
		insert testJournals;
		for (integer i=0;i<NUM_OF_OPPORTUNITIES;i++){
			testSalesInvLineItems[i].c2g__IncomeScheduleGroup__c = testGroups[i].id;
		}
		test.startTest();
			update testSalesInvLineItems;
		test.stopTest();

		List<c2g__codaJournal__c> insertedJournals = [ SELECT id, c2g__IncomeScheduleGroup__c, Opportunity__c FROM c2g__codaJournal__c WHERE Id IN :testJournals ];
		Map<ID,c2g__codaInvoiceLineItem__c> lineItemsMap = new Map<ID,c2g__codaInvoiceLineItem__c>([Select Id, c2g__IncomeScheduleGroup__c,c2g__Invoice__r.c2g__Opportunity__c From  c2g__codaInvoiceLineItem__c where c2g__IncomeScheduleGroup__c in : testGroups ]); 
		Map<ID,ID> invoiceLineItemGroupIdOppIdMap = new Map<ID,ID>();
		for (c2g__codaInvoiceLineItem__c item :lineItemsMap.values()){
			invoiceLineItemGroupIdOppIdMap.put(item.c2g__IncomeScheduleGroup__c,item.c2g__Invoice__r.c2g__Opportunity__c);
		}
		for( c2g__codaJournal__c insertedJournal: insertedJournals )
		{
			System.assertNotEquals( null,insertedJournal.Opportunity__c, 'The journal opportunity is populated' );
			System.assertEquals( insertedJournal.Opportunity__c, invoiceLineItemGroupIdOppIdMap.get(insertedJournal.c2g__IncomeScheduleGroup__c), 'The journal is populated with opportunity lookup from the invoice line item with the same group reference' );
		}

	}

	
	@isTest(seeAllData=true) 	
	private static void testSalesInvoiceLineItemUpdates_negativeCase_noGroups() 
	{
		datasetup();

		insert testSalesInvLineItems;
		for (integer i=0;i<NUM_OF_OPPORTUNITIES;i++){
			testJournals[i].c2g__IncomeScheduleGroup__c = testGroups[i].id;
		}		
		List<c2g__codaInvoiceLineItem__c> lineItems = [Select Id, Name, c2g__IncomeScheduleGroup__c,Opportunity__c,c2g__Invoice__r.c2g__Opportunity__c From  c2g__codaInvoiceLineItem__c 
														where id in : testSalesInvLineItems ];		
		insert testJournals;
		for (integer i=0;i<NUM_OF_OPPORTUNITIES;i++){
			testSalesInvLineItems[i].c2g__IncomeScheduleGroup__c = null;
		}
		test.startTest();
			update testSalesInvLineItems;
		test.stopTest();
		List<c2g__codaJournal__c> insertedJournals = [ SELECT id, c2g__IncomeScheduleGroup__c, Opportunity__c FROM c2g__codaJournal__c WHERE Id IN :testJournals ];
		Map<ID,c2g__codaInvoiceLineItem__c> lineItemsMap = new Map<ID,c2g__codaInvoiceLineItem__c>([Select Id, c2g__IncomeScheduleGroup__c,c2g__Invoice__r.c2g__Opportunity__c From  c2g__codaInvoiceLineItem__c where c2g__IncomeScheduleGroup__c in : testGroups ]); 
		Map<ID,ID> invoiceLineItemGroupIdOppIdMap = new Map<ID,ID>();
		for (c2g__codaInvoiceLineItem__c item :lineItemsMap.values()){
			invoiceLineItemGroupIdOppIdMap.put(item.c2g__IncomeScheduleGroup__c,item.c2g__Invoice__r.c2g__Opportunity__c);
		}
		for( c2g__codaJournal__c insertedJournal: insertedJournals )
		{
			System.assertEquals( null,insertedJournal.Opportunity__c, 'The journal opportunity is not populated' );
		}

	}
}