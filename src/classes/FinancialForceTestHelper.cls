@isTest
public class FinancialForceTestHelper 
{
	public static Account testAccount;
	public static product2 testProduct;
	public static c2g__codaGeneralLedgerAccount__c testGeneralLedgerAccount;
	public static PriceBookEntry testPBE;

	public static c2g.CODAAPICommon.Reference buildReference( Id sObjectId )
	{
		c2g.CODAAPICommon.Reference ref = new c2g.CODAAPICommon.Reference();
		ref.Id = sObjectId;
		return ref;
	}

	public static List<c2g.CODAAPICommon.Reference> buildReferences( Set<Id> sObjectIds )
	{
		List<c2g.CODAAPICommon.Reference> refs = new List<c2g.CODAAPICommon.Reference>();
		for( Id sObjectId : sObjectIds )
		{
			refs.add( buildReference( sObjectId ) );
		}
		return refs;
	}
	
	public static void postCreditNote( Id sObjectId  )
	{
		c2g.CODAAPISalesCreditNote_7_0.PostCreditNote( NULL, buildReference( sObjectId ) );
	}

	public static void bulkPostCreditNotes( Set<Id> creditNoteIds )
	{
		List<c2g.CODAAPICommon.Reference> refs = buildReferences( creditNoteIds );
		c2g.CODAAPISalesCreditNote_7_0.BulkPostCreditNote( NULL, refs );
	}

	public static void postSalesInvoice( Id sObjectId  )
	{
		c2g.CODAAPISalesInvoice_7_0.PostInvoice( NULL, buildReference( sObjectId ) );
	}

	public static void bulkPostSalesInvoices( Set<Id> salesInvoiceIds )
	{
		List<c2g.CODAAPICommon.Reference> refs = buildReferences( salesInvoiceIds );
		c2g.CODAAPISalesInvoice_7_0.BulkPostInvoice( NULL, refs );
	}

	public static void postJournal( Id sObjectId  )
	{
		c2g.CODAAPIJournal_7_0.PostJournal( NULL, buildReference( sObjectId ) );
	}

	public static void bulkPostJournals( Set<Id> journalIds )
	{
		List<c2g.CODAAPICommon.Reference> refs = buildReferences( journalIds );
		c2g.CODAAPIJournal_7_0.BulkPostJournal( NULL, refs );
	}

	public static void bulkCancelJournals( Set<Id> originalJournalIds )
	{
		List<c2g.CODAAPICommon.Reference > refs = buildReferences( originalJournalIds );

		List<c2g.CODAAPIJournalTypes_7_0.CancellingInformation> cancellingInformationList = new List<c2g.CODAAPIJournalTypes_7_0.CancellingInformation>();
		for (c2g.CODAAPICommon.Reference ref : refs)
		{
			c2g.CODAAPIJournalTypes_7_0.CancellingInformation cancellingInformation = new c2g.CODAAPIJournalTypes_7_0.CancellingInformation();
			cancellingInformation.OriginalJournalRef = ref;
			cancellingInformationList.add(cancellingInformation);
		}

		c2g.CODAAPIJournal_7_0.BulkCancelJournal(null, cancellingInformationList);
	}


	public static void baseSetup()
	{
		testGeneralLedgerAccount = TestUtils.createGeneralLedgerAccount(true);
		testAccount = TestUtils.createAccount_FinancialForce( TestUtils.PARTNER_RECORD_TYPE.Id, testGeneralLedgerAccount.Id, true );
		testProduct = TestUtils.createProduct_FinancialForce( testGeneralLedgerAccount.Id, true );
		testPBE = TestUtils.createPricebookEntry( testProduct.Id, TestUtils.STANDARD_PRICE_BOOK.Id, true );
	}
	
}