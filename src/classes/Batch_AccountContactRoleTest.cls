@isTest
public class Batch_AccountContactRoleTest
{
	static final String ROLE_NAME_1 = 'testRole';
	static final String ROLE_NAME_2 = 'anotherTestRole';
	
	static List<Contact> testContacts;
	static List<AccountContactRole> roles;
	
	static void setup()
	{
		Account testAccount = TestUtils.createAccounts(1, TestUtils.MARKETER_RECORD_TYPE.Id, true)[0];
		Account anotherTestAccount = TestUtils.createAccounts(1, TestUtils.MARKETER_RECORD_TYPE.Id, true)[0];
		testContacts = TestUtils.createContactsMultiple(3, 'test','test',testAccount.Id, true);
		
		roles = new List<AccountContactRole>();
		
		for(Integer i = 0; i < testContacts.size(); i++)
		{
			roles.add(new AccountContactRole(ContactId = testContacts[i].Id, AccountId=testAccount.Id,Role=ROLE_NAME_1+i));
		}
		
		for(Integer i = 0; i < testContacts.size(); i++)
		{
			roles.add(new AccountContactRole(ContactId = testContacts[i].Id, AccountId=anotherTestAccount.Id,Role=ROLE_NAME_2+i));
		}
	}
	
	static testMethod void testAccountContactRoleBatch()
	{
		setup();
		
		insert roles;
		
		Test.startTest();
			Database.executeBatch(new Batch_AccountContactRole());
		Test.stopTest();
		
		List<Contact> queriedContacts = [ SELECT Id, ContactsRole__c, Contact_Status__c FROM Contact WHERE Id IN :testContacts ];
		
		System.assert(!queriedContacts.isEmpty(), 'Contacts should have been inserted');
		
		for(Contact contact : queriedContacts)
		{
			System.assert(contact.ContactsRole__c.contains(ROLE_NAME_1), 'Roles should appear in multiselect picklist if the contact is associated with an AccountContactRole');
			System.assert(contact.ContactsRole__c.contains(ROLE_NAME_2), 'Roles should appear in multiselect picklist if the contact is associated with an AccountContactRole');
			System.assertEquals('Current', contact.Contact_Status__c, 'If roles exist for the contact, then his status should be current');
		}
	}
	
	static testMethod void testAccountContactRoleBatch_noRoles()
	{
		setup();
		
		Test.startTest();
			Database.executeBatch(new Batch_AccountContactRole());
		Test.stopTest();
		
		List<Contact> queriedContacts = [ SELECT Id, ContactsRole__c, Contact_Status__c FROM Contact WHERE Id IN :testContacts ];
		
		System.assert(!queriedContacts.isEmpty(), 'Contacts should have been inserted');
		
		for(Contact contact : queriedContacts)
		{
			System.assertEquals(null,contact.ContactsRole__c, 'Roles should not appear in multiselect picklist if the contact is not associated with an AccountContactRole');
			System.assertEquals('Not Current', contact.Contact_Status__c, 'If roles do not exist for the contact, then his status should be not current');
		}
	}
}