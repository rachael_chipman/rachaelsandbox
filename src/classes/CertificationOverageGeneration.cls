public with sharing class CertificationOverageGeneration {
	
	public static final String CERT = 'Certification - License';
	public static final String CERT_CHILD = 'Certification - License Child%';
	public static final Set<String> CERT_PRODS = new Set<String>{CERT, CERT_CHILD};
	public static final String ACTIVE = 'Active';
	public static final String CERT_OVERAGE = 'Certification - Overage';
	public static final String CERT_LICENSE = 'Certification - License - Annually';

	public static zqu__ProductRatePlanCharge__c certOverageRatePlanCharge
	{
		get
		{
			if(certOverageRatePlanCharge == NULL)
			{
				certOverageRatePlanCharge = [SELECT Id, Name, zqu__ZuoraId__c, zqu__ProductRatePlan__c, zqu__ProductRatePlan__r.zqu__ZuoraId__c, zqu__ProductRatePlan__r.zqu__ZProduct__r.Name, zqu__ProductRatePlan__r.Name FROM zqu__ProductRatePlanCharge__c WHERE Name = :CERT_OVERAGE AND zqu__Deleted__c = false AND (NOT zqu__ProductRatePlan__r.zqu__ZProduct__r.Name LIKE '%[DoNotUse]%') LIMIT 1];
			}
			return certOverageRatePlanCharge;
		}
		private set;
	}

	public static zqu__ProductRatePlanCharge__c certLicenceRatePlanCharge
	{
		get
		{
			if(certLicenceRatePlanCharge == NULL)
			{
				certLicenceRatePlanCharge = [SELECT Id, zqu__ProductRatePlan__c, zqu__ProductRatePlan__r.zqu__ZuoraId__c, zqu__ProductRatePlan__r.zqu__ZProduct__r.Name, zqu__ProductRatePlan__r.Name FROM zqu__ProductRatePlanCharge__c WHERE Name = :CERT_LICENSE AND zqu__Deleted__c = false AND (NOT zqu__ProductRatePlan__r.zqu__ZProduct__r.Name LIKE '%[DoNotUse]%') LIMIT 1];
			}
			return certLicenceRatePlanCharge;
		}
		private set;
	}

	public static Map<String, Decimal> getTierCurrencyRate()
	{
		Map<String, Decimal> tierAndCurrencyToRate = new Map<String, Decimal>();

		List<zqu__ProductRatePlanChargeTier__c> certTiers = [SELECT Id, Name, zqu__Currency__c, zqu__Price__c, zqu__ProductRatePlanCharge__c, zqu__StartingUnit__c, 
		zqu__EndingUnit__c, zqu__Tier__c, zqu__ProductRatePlanCharge__r.Name
		FROM zqu__ProductRatePlanChargeTier__c
		WHERE zqu__ProductRatePlanCharge__c = : certLicenceRatePlanCharge.Id];

		for(zqu__ProductRatePlanChargeTier__c certTier : certTiers)
		{
			tierAndCurrencyToRate.put(certTier.zqu__Tier__c + certTier.zqu__Currency__c, certTier.zqu__Price__c);
		}
		return tierAndCurrencyToRate;
	}

	public static String roundTheTotalVolumeToNextTier(Decimal totalVol)
	{
		String tier;

		if(totalVol > 0 && totalVol < 50001)
		{
			tier = '1';
		}
		else if( totalVol >= 50001 && totalVol < 250001)
		{
			tier = '2';
		}
		else if(totalVol >= 250001 && totalVol < 1000001)
		{
			tier = '3';
		}
		else if(totalVol >= 1000001 && totalVol < 5000001)
		{
			tier = '4';
		}
		else if(totalVol >= 5000001 && totalVol < 10000001)
		{
			tier = '5';
		}
		else if(totalVol >= 10000001 && totalVol < 20000001)
		{
			tier = '6';
		}
		else if(totalVol >= 20000001 && totalVol < 50000001)
		{
			tier = '7';
		}
		else if(totalVol >= 50000001 && totalVol < 100000001)
		{
			tier = '8';
		}
		else if(totalVol >= 100000001)
		{
			tier = '9';
		}
		return tier;
	}

	public static void saveQuoteInformation(List<OverageFeeGenerationServices.QuoteAndAttachments> quoteAttachments)
	{
		List<zqu__QuoteAmendment__c> newAmendments = new List<zqu__QuoteAmendment__c>();
		List<zqu__QuoteRatePlan__c> newRatePlans = new List<zqu__QuoteRatePlan__c>();
		List<zqu__QuoteRatePlanCharge__c> newRatePlanCharges = new List<zqu__QuoteRatePlanCharge__c>();
		List<zqu__QuoteChargeSummary__c> newChargeSummaries = new List<zqu__QuoteChargeSummary__c> ();
		List<zqu__QuoteChargeDetail__c> newchargedetails = new List<zqu__QuoteChargeDetail__c>();
		
		Map<String, Decimal> tierToRate = getTierCurrencyRate();
		Map<Id, Decimal> quoteIdToOveragePrice = new Map<Id, Decimal>();
			

		//First we have to loop through the QuoteAndAttachment objects so that we can insert the amendments
		for(OverageFeeGenerationServices.QuoteAndAttachments qInfo : quoteAttachments)
		{
			String tier = roundTheTotalVolumeToNextTier(qInfo.totalVol);
			Decimal overageAmount;
			if(((tierToRate.get(tier + qInfo.quote.zqu__Currency__c) / 12) - qInfo.runRate).setScale(2) > 0)
			{
				overageAmount = ((tierToRate.get(tier + qInfo.quote.zqu__Currency__c) / 12) - qInfo.runRate).setScale(2);	
			}
			else
			{
				overageAmount = 1.00;
			}

			qInfo.amendment.zqu__Quote__c = qInfo.quote.Id;
			qInfo.amendment.zqu__Description__c += certOverageRatePlanCharge.zqu__ProductRatePlan__r.zqu__ZuoraId__c + ', Rate Plan ' + CERT_OVERAGE;
			qInfo.amendment.zqu__TotalAmount__c = overageAmount;
			qInfo.amendment.zqu__DeltaTCV__c = overageAmount;
			newAmendments.add(qInfo.amendment);

			quoteIdToOveragePrice.put(qInfo.amendment.zqu__Quote__c, overageAmount);
			system.debug('What is in the overage amount map? ' + quoteIdToOveragePrice); 
		}		
		insert newAmendments;
		
		//Now that the amendments have been inserted we will need to loop through the QuoteAndAttachment objects again to update the rate plans and get them inserted
		for(OverageFeeGenerationServices.QuoteAndAttachments qInfo : quoteAttachments)
		{
			if(qInfo.amendment.Id != NULL && qInfo.amendment.zqu__Quote__c == qInfo.quote.Id)
			{
				qInfo.ratePlan.Name = CERT_OVERAGE;
				qInfo.ratePlan.zqu__QuoteAmendment__c = qInfo.amendment.Id;
				qInfo.ratePlan.zqu__AmendmentType__c = qInfo.amendment.zqu__Type__c;
				qInfo.ratePlan.zqu__ProductRatePlanZuoraId__c = certOverageRatePlanCharge.zqu__ProductRatePlan__r.zqu__ZuoraId__c;
				qInfo.ratePlan.zqu__QuoteProductName__c = certOverageRatePlanCharge.zqu__ProductRatePlan__r.zqu__ZProduct__r.Name;
				newRatePlans.add(qInfo.ratePlan);
			}
		}

			insert newRatePlans;

		//Once we have the Quote Rate Plans inserted we need to insert the Quote Rate Plan Charges and Charge Summary records
		for(OverageFeeGenerationServices.QuoteAndAttachments qInfo : quoteAttachments)
		{
			if(qInfo.ratePlan.Id != NULL)
			{
				system.debug('When I call the amount map what is in it? ' + quoteIdToOveragePrice.get(qInfo.amendment.zqu__Quote__c));

				qInfo.ratePlanCharge.zqu__QuoteRatePlan__c = qInfo.ratePlan.Id;
				qInfo.ratePlanCharge.Name = CERT_OVERAGE;
				qInfo.ratePlanCharge.zqu__ProductRatePlanChargeZuoraId__c = certOverageRatePlanCharge.zqu__ZuoraId__c;
				qInfo.ratePlanCharge.zqu__EffectivePrice__c = quoteIdToOveragePrice.get(qInfo.amendment.zqu__Quote__c);
				qInfo.ratePlanCharge.zqu__TCV__c = quoteIdToOveragePrice.get(qInfo.amendment.zqu__Quote__c);
				qInfo.ratePlanCharge.zqu__Total__c = quoteIdToOveragePrice.get(qInfo.amendment.zqu__Quote__c);
				newRatePlanCharges.add(qInfo.ratePlanCharge);

				qInfo.chargeSummary.zqu__QuoteRatePlan__c = qInfo.ratePlan.Id;
				qInfo.chargeSummary.Name = CERT_OVERAGE;
				qInfo.chargeSummary.zqu__ChangeLog__c = 'New Rate Plan ' + '\"'+ CERT_OVERAGE + '\"';
				qInfo.chargeSummary.zqu__EffectivePrice__c = quoteIdToOveragePrice.get(qInfo.amendment.zqu__Quote__c);
				qInfo.chargeSummary.zqu__TCV__c = quoteIdToOveragePrice.get(qInfo.amendment.zqu__Quote__c);
				qInfo.chargeSummary.zqu__TotalPrice__c = quoteIdToOveragePrice.get(qInfo.amendment.zqu__Quote__c);
				newChargeSummaries.add(qInfo.chargeSummary);

				//creating the zqu__QuoteChargeDetails object
				qInfo.chargeDetail.Name = CERT_OVERAGE;
				qInfo.chargeDetail.zqu__Quote__c = qInfo.quote.Id;
				qInfo.chargeDetail.zqu__ProductRatePlan__c = certOverageRatePlanCharge.zqu__ProductRatePlan__c;
				qInfo.chargeDetail.zqu__ProductRatePlanCharge__c = certOverageRatePlanCharge.Id;
				qInfo.chargeDetail.zqu__DeltaTCV__c = quoteIdToOveragePrice.get(qInfo.amendment.zqu__Quote__c);
				qInfo.chargeDetail.zqu__TCV__c = quoteIdToOveragePrice.get(qInfo.amendment.zqu__Quote__c);
				newchargedetails.add(qInfo.chargeDetail);

			}
		}
			insert newRatePlanCharges;
			insert newChargeSummaries;

			insert newchargedetails;
	}
	
	public static void processOverages( list<Accreditation_Volume__c> newAccVols )
	{
		Set<Id> accountIds = pluck.Ids('Account__c', newAccVols);
		Map<Id, Zuora__SubscriptionProductCharge__c> accountIdToSubCharge = OverageFeeGenerationServices.getExistingSubscriptionCharge(accountIds , CERT_PRODS);
		list<OverageFeeGenerationServices.OverageFeeOpportunity> feeOpps = new list<OverageFeeGenerationServices.OverageFeeOpportunity>();
		List<OverageFeeGenerationServices.QuoteAndAttachments> quoteAttachments = new List<OverageFeeGenerationServices.QuoteAndAttachments>();

		for( Accreditation_Volume__c accVol : newAccVols )
		{
			system.debug('What do we have in the loop? ' + accountIdToSubCharge);
			if(!accountIdToSubCharge.isEmpty() && accountIdToSubCharge.containsKey(accVol.Account__c))
			{
				Zuora__SubscriptionProductCharge__c charge = accountIdToSubCharge.get(accVol.Account__c);
				if(charge != NULL && charge.Zuora__MonthlyRecurringRevenue__c != NULL)
				{
					Opportunity opp = OverageFeeGenerationServices.createOverageOpp(charge);
					system.debug('Do I have an opp after create method? ' + opp);
					zqu__Quote__c quote = OverageFeeGenerationServices.createQuote(charge);
					feeOpps.add(new OverageFeeGenerationServices.OverageFeeOpportunity( opp, charge, quote, CERT));
				}
			}
		}
		OverageFeeGenerationServices.saveOverageFeeOpportunities( feeOpps );
		Map<Id, zqu__Quote__c> accountIdToInsertedQuotes = OverageFeeGenerationServices.getInsertedQuotes(accountIds, CERT);

		for(Accreditation_Volume__c accVol : newAccVols)
		{
			if(!accountIdToInsertedQuotes.isEmpty() && accountIdToInsertedQuotes.containsKey(accVol.Account__c))
			{
				zqu__Quote__c TQuote = accountIdToInsertedQuotes.get(accVol.Account__c);
                zqu__QuoteAmendment__c amendment = OverageFeeGenerationServices.createQuoteAmendment(accountIdToInsertedQuotes.get(accVol.Account__c));
				zqu__QuoteRatePlan__c ratePlan = OverageFeeGenerationServices.createQuoteRatePlan(accountIdToInsertedQuotes.get(accVol.Account__c));
				zqu__QuoteRatePlanCharge__c ratePlanCharge = OverageFeeGenerationServices.createQuoteRatePlanCharge(accountIdToInsertedQuotes.get(accVol.Account__c));
				zqu__QuoteChargeSummary__c chargeSummary = OverageFeeGenerationServices.createQuoteChargeSummary(accountIdToInsertedQuotes.get(accVol.Account__c));
				zqu__QuoteChargeDetail__c chargeDetail = OverageFeeGenerationServices.createQuoteChargeDetail(TQuote, rateplancharge, ratePlan);
				quoteAttachments.add( new OverageFeeGenerationServices.QuoteAndAttachments(accountIdToInsertedQuotes.get(accVol.Account__c), amendment, ratePlan, ratePlanCharge, chargeSummary, chargeDetail, accVol.Total_Volume__c, accountIdToSubCharge.get(accVol.Account__c).Zuora__MonthlyRecurringRevenue__c, accountIdToSubCharge.get(accVol.Account__c).Zuora__Quantity__c, 0, 0, 0.00));	
			}
		}
		saveQuoteInformation( quoteAttachments );
	}

	public static void processTrigger (list<Accreditation_Volume__c> newAccVols)
	{
		processOverages( newAccVols );
	}
}