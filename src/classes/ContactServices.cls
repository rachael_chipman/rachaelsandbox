public with sharing class ContactServices
{
	/*
	@future(callout=true)
	public static void MakeContactEmailCall(Id updatedConId, String oldEmail)
	{
		MakeContactEmailCallSync(updatedConId, oldEmail);
	}

	public static HttpResponse MakeContactEmailCallSync(Id updatedConId, String oldEmail)
	{
		List<Contact> updatedCon = [SELECT Id, FirstName, LastName, Email, Is_Admin__c, MailingCountry, MailingCity, Language_of_Choice__c, EIS_Username__c FROM Contact WHERE Id = :updatedConId];
		String baseEndpoint = ' http://w3apidev.returnpath.net/v1/salesforce/users/'+ oldEmail + '/email';
		String endpointParams = '?api_key=12340c9ec9dd31a6e805393c045996bf67722bd9&api_hash=014fd14e27d0d11a4b8ed6e7489806a786acc13cd2c96802c348a0723abef3a1&api_salt=123';
		String jsonbody;
		Http h = new Http();
		HttpRequest req= new HttpRequest();

		req.setEndpoint(baseEndpoint+endpointParams);
		req.setMethod('PUT');
		//req.setTimeout(60000);
		System.debug(req);
		// Set data to send over into post JSON
		req.setBody(JSON.serialize(updatedCon));
		// Send the request, and return a response
		System.debug(req.getBody());
		HttpResponse res;
		try
		{
			res = h.send(req);
		}
		catch (System.CalloutException e)
		{
			System.debug(e);
			throw new HTTPException('Your request has timed out, please try again in a few minutes.');
		}

		//Get JSON body - for debug purposes
		jsonbody = res.getBody();

		//system.debug('!@#$'+jsonbody);
		if(res.getStatusCode() != 200)
		{
			throw new HTTPException('Error occured while sending HTTP request, your administrator has been notified:'+ res.getStatus()+'-'+res.getStatusCode()+'+'+jsonbody);

		}
		return res;
	}
	*/
	public static void updateRoleField(List<Contact> contacts)
	{
		List<Contact> contactsToUpdate = new List<Contact>();
		for(Contact contact : contacts)
		{
			Set<String> roles = new Set<String>();

			for(AccountContactRole contactRole : contact.AccountContactRoles)
			{
				roles.add(contactRole.Role);
			}

			if(!roles.isEmpty())
			{
				List<String> rolesList = new List<String>(roles);
				contact.ContactsRole__c = String.join(rolesList, '; ');
				contact.Contact_Status__c = 'Current';
				contact.Contact_Status_Reason__c = null;
				contactsToUpdate.add(contact);
			}
			else
			{
				if(contact.Contact_Status__c != 'Not Current')
				{
					contact.ContactsRole__c = null;
					contact.Contact_Status__c='Not Current';
					contact.Contact_Status_Reason__c = null;
					contactsToUpdate.add(contact);
				}
			}
		}
		if(!contactsToUpdate.isEmpty())
		{
			update contactsToUpdate;
		}
	}
}