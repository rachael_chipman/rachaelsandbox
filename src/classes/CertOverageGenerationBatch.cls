global class CertOverageGenerationBatch implements Database.Batchable<sObject>, Database.AllowsCallouts {
	
	public static final String BATCH_NAME = 'CertOverageGenerationBatch';
    public static final String[] ERROR_EMAILS = new String[]{ 'salesforceplatform@returnpath.com' };
	Map<Id, Accreditation_Volume__c> accVolMap;
    global String errors = '';

    global CertOverageGenerationBatch(Map<Id, Accreditation_Volume__c> accVolRecords)
    {
    	accVolMap = accVolRecords;
    	//system.debug('What is in the billingAccounts map' + billingAccountMap);
    }

    /*global void execute(SchedulableContext sc)
    {
    	CertOverageGenerationBatch certFeeBatch = new CertOverageGenerationBatch();
    	Database.executeBatch(certFeeBatch);
    }*/
		
	global Database.QueryLocator start(Database.BatchableContext BC) 
	{
		//system.debug('Is the map key null? ' + billingAccountMap.keySet());
		return Database.getQueryLocator([SELECT Id, Overage__c, Tier__c, Total_Volume__c, Generate_Overage_Opportunity__c,
		Account__c, Max_Volume__c 
		FROM Accreditation_Volume__c
		WHERE Id IN : accVolMap.keySet()]);
	}

   	global void execute(Database.BatchableContext BC, List<sObject> scope) 
   	{
		try
		{
			CertificationOverageGeneration.processTrigger(scope);
		}
		catch (DmlException dmx)
		{
			for(Integer i = 0; i < dmx.getNumDml(); i++)
			{
				errors += dmx.getDmlMessage(i);
			}
		}
	}
	
	global void finish(Database.BatchableContext BC) 
	{
		AsyncApexJob job = [SELECT Id, Status, NumberOfErrors,
	                         JobItemsProcessed, TotalJobItems,
	                         CreatedBy.Email
	                         FROM AsyncApexJob
	                         WHERE Id =:bc.getJobId()];

	     Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();

	     mail.setToAddresses( ERROR_EMAILS );
	     mail.setSubject( 'Batch to ' + BATCH_NAME + ' ' + job.Status );

	     String emailBody = 'Batch to ' + BATCH_NAME + ' processed '
	                      + job.TotalJobItems
	                      + ' batches with '
	                      + job.NumberOfErrors
	                      + ' failures.';
	     if( errors != '' )
	     {
		     emailBody += '\n\n\nThe following errors occured:\n'+ errors;
		     mail.setPlainTextBody( emailBody );
		     Messaging.sendEmail( new Messaging.SingleEmailMessage[]{ mail } );
	     }	
	}
	
}