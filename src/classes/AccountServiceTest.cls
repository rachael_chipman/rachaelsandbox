@isTest
private class AccountServiceTest {
	
	static Account testAccount;
	static Opportunity testOpp;
	static User Salesperson;
	static User AM;
	static User CRM;
	static User TM;
	static User APSClientService;
	static User IIc_CS;
	static User CorpDev;
	
	/*static OpportunityLineItem testOli;
	static Pricebook2 testPb;
	static PricebookEntry testPbe;
	static Product2 testProduct;*/
	
	
	static void setUpMethod()
	{
		testAccount = TestUtils.createAccount(TestUtils.MARKETER_RECORD_TYPE.Id, true);
		Salesperson = TestUtils.SALES_USER;
		AM = TestUtils.SALES_USER;
		CRM = TestUtils.CRM_USER;
		TM = TestUtils.CRM_USER;
		APSClientService = testUtils.SALES_USER;
		IIc_CS = TestUtils.CRM_USER;
		CorpDev = TestUtils.SALES_USER;

		testAccount.Salesperson__c = Salesperson.Id;
		testAccount.AM__c = AM.Id;
		testAccount.CRM__c = CRM.Id;
		testAccount.Technical_Account_Manager__c = TM.Id;
		testAccount.APS_Client_Services__c = APSClientService.Id;
		testAccount.Inbox_Insight_Client_Services__c = IIc_CS.Id;
		testAccount.CorpDev_Other__c = CorpDev.Id;
		update testAccount;


		//testAccount.Name ='Rachael Account 1';
		//update testAccount;
		
		//testOpp = TestUtils.createOpportunity(TestUtils.DEMONSTRATING_CAPABILITIES_RECORD_TYPE.Id, testAccount.Id, true);
		
		/*testPb = TestUtils.STANDARD_PRICE_BOOK;
		
		testProduct = TestUtils.createProduct(true);
		
		testPbe = TestUtils.createPricebookEntry(testProduct.Id, testPb.Id, true);
		
		testOli = TestUtils.createOpportunityLineItem(testOpp.Id, testPbe.Id, true);*/
	}
	
	/*private static testMethod void testTermACVCalc()
	{
		setUpMethod();
		
		testOpp.StageName = 'Terminated Contract';
		testOpp.RecordTypeId = [SELECT Id FROM RecordType WHERE SObjectType = 'Opportunity' and DeveloperName= 'Termination'].Id;
		update testOpp;
		testOli.Previous_Value__c = 500.00;
		update testOli;
		
		test.startTest();
		AccountService.accountTerminationsRollUp();
		test.stopTest();
		
		list<Account> testAccounts = [SELECT Id, Sum_of_Terminated_ACV_Last_12_months__c FROM Account WHERE Id = :testAccount.Id];
		system.assertEquals(500, testAccounts[0].Sum_of_Terminated_ACV_Last_12_months__c, 'We expect the account to be updated with the correct amount');
		
	}*/

	private testMethod static void testAccountTeamRoleFunction()
	{
		setupMethod();	
        User inactiveUser = [Select Id,Name from User where isActive = false limit 1];
        testAccount.AM__c = inactiveUser.Id;
        update testAccount;
		Test.startTest();
        try
        {
            CreateAccountTeamMembers ATM = new CreateAccountTeamMembers() ;
            Database.executeBatch(ATM);
        }
        catch(Exception ex)
        {
            System.debug('Caught in exception');
        }
		Test.stopTest();
        List<AccountTeamMember> accroles = [Select AccountId, UserId, User.IsActive,User.Name, 
                IsDeleted, TeamMemberRole, CreatedDate, AccountAccessLevel from AccountTeamMember where AccountId =:testAccount.Id];
       System.assert(accroles.size() > 1);
  
	}
    
    private testMethod static void testException()
    {
        setupMethod();		
		Test.startTest();
		CreateAccountTeamMembers ATM = new CreateAccountTeamMembers() ;
		Database.executeBatch(ATM);
		Test.stopTest();
        List<AccountTeamMember> accroles = [Select AccountId, UserId, User.IsActive,User.Name, 
                IsDeleted, TeamMemberRole, CreatedDate, AccountAccessLevel from AccountTeamMember where AccountId =:testAccount.Id];
       System.assert(accroles.size() > 1);
    }
    
    private testMethod static void sendemailTest()
    {
        setupMethod();	
        Map<Id,String> errorvalues = new Map<Id,String>();
        errorvalues.put(testAccount.Id,'errors in TM roles');
		Test.startTest();
        
        AccountService.sendErrorEmail(errorvalues);
        Test.stopTest();
    }
	

}