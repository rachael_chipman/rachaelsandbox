public with sharing class AccountPlanTabViewExt {

	public Account_Plan__c acctPlan {get;set;}
	public Boolean accountPlanAvailable {get;set;}
	public Account myaccount{get;set;}

	public AccountPlanTabViewExt(ApexPages.StandardController std)
	{
		Id accountId = std.getId();
		myaccount = [ SELECT Id, Name FROM Account WHERE Id = :accountId ];
		List<Account_Plan__c> acctPlans = [SELECT Id FROM Account_Plan__c WHERE Account__c =: accountId ORDER BY createdDate DESC];

		if(!acctPlans.isEmpty())
		{
			acctPlan = acctPlans[0];
			accountPlanAvailable = TRUE;
		}

		else
		{
			accountPlanAvailable = FALSE;
			ApexPages.addmessage(new ApexPages.message(ApexPages.SEVERITY.WARNING,'No Account Plan Available! '));
		}
	}
}