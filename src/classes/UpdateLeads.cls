//class to update leads to trigger assignment rules
global class UpdateLeads implements Schedulable {
global void execute(SchedulableContext ctx) {
    
    CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, NextFireTime
                FROM CronTrigger WHERE Id = :ctx.getTriggerId()];

      System.debug(ct.CronExpression);
      System.debug(ct.TimesTriggered);
      
      datetime myDate = datetime.newInstance(2013, 01, 01);
      
      List<Lead> leads = [select Id from Lead where ((Campaign_Name__c = null and status = 'Eloqua Lead Scoring' and Assigned__c = false)
                          and (Inquiry_Category__c = 'Receiver' or Identity__c = 'Receiver' or Country = 'China' or Internal_Pod_Number__c = '15'
                          or (Identity__c = 'Channel' and Country = 'United States') or (MOne1_Tier__c = 'Tier 1')
                           or (MOne1_Tier__c = 'Tier 2')) and CreatedDate > :myDate) or (OwnerId = '00500000006olmF' and Status = 'Eloqua Lead Scoring' and CreatedDate > :myDate)];          
      
      
      
      
      For (lead l:leads){
      Database.DMLOptions dmo = new Database.DMLOptions();
      dmo.assignmentRuleHeader.useDefaultRule= true;
       l.setOptions(dmo);

    }
    update(leads);

      
      }
      }