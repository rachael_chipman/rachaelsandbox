@isTest
private class TestAccountReceivable {
    
    private static List<Account> testAccounts = new List<Account>();
    private static Integer NUM_OF_ACCOUNTS = 10;
    private static c2g__codaGeneralLedgerAccount__c glAccount = new c2g__codaGeneralLedgerAccount__c();
    
   /* private static void dataSetup(){
        glAccount = TestUtils.createGeneralLedgerAccount(false);
        glAccount.Name = AccountService.AR_TRADE;
        insert glAccount;
        testAccounts = TestUtils.createAccounts(NUM_OF_ACCOUNTS, TestUtils.PARTNER_RECORD_TYPE.id,false );
    }
    
    @isTest
    private static void populateGLAccountLookup(){
        datasetup();
        Test.startTest();
            insert testAccounts;
        Test.stopTest();
        List<Account> updatedAccounts = new List<Account>();
        updatedAccounts = [Select c2g__CODAAccountsReceivableControl__c From Account where id in: testAccounts];
        System.assertEquals(NUM_OF_ACCOUNTS,updatedAccounts.size(),'All accounts have been inserted');
        for (Account updatedAccount : updatedAccounts){
            System.assertNotEquals(null,updatedAccount.c2g__CODAAccountsReceivableControl__c,'The control lookup has been populated');
            System.assertEquals(updatedAccount.c2g__CODAAccountsReceivableControl__c,glAccount.id,'The control lookup has been set to the glAccount');      
        }
    }
    @isTest
    private static void populateGLAccountLookup_noGLAccount_negativeCase(){
        testAccounts = TestUtils.createAccounts(NUM_OF_ACCOUNTS, TestUtils.PARTNER_RECORD_TYPE.id,false );
        Test.startTest();
            insert testAccounts;
        Test.stopTest();
        List<Account> updatedAccounts = new List<Account>();
        updatedAccounts = [Select c2g__CODAAccountsReceivableControl__c From Account where id in: testAccounts];
        System.assertEquals(NUM_OF_ACCOUNTS,updatedAccounts.size(),'All accounts have been inserted');
        for (Account updatedAccount : updatedAccounts){
            System.assertEquals(null,updatedAccount.c2g__CODAAccountsReceivableControl__c,'The control lookup has not been populated');
            System.assertEquals(updatedAccount.c2g__CODAAccountsReceivableControl__c,glAccount.id,'The control lookup has not been set as the glAccount is not present');       
        }
    } */
}