@isTest(seealldata = true)
private class CampaignMemberStatusUpdateClassTest {
    static Campaign testCampaign;
    
	private static testMethod void testCampaignInsert()
    {
        testCampaign = new Campaign(name = 'Campaign1', isActive = true);
        test.startTest();
        insert testCampaign;
        test.stopTest();
        
        list<Campaign> insertedTestCampaign = [Select ID from Campaign Where ID =: testCampaign.Id];
        list<CampaignMemberStatus> CMS1 = [Select ID, Label, CampaignId, isDefault From CampaignMemberStatus Where CampaignId =: testCampaign.Id And Label = 'Registered'];
        list<CampaignMemberStatus> CMS2 = [Select ID, Label, CampaignId From CampaignMemberStatus Where CampaignId =: testCampaign.Id And Label = 'Qualified'];
        list<CampaignMemberStatus> CMS3 = [Select ID, Label, CampaignId From CampaignMemberStatus Where CampaignId =: testCampaign.Id And Label = 'Non-Qualified'];
        list<CampaignMemberStatus> CMS4 = [Select ID, Label, CampaignId From CampaignMemberStatus Where CampaignId =: testCampaign.Id And Label = 'Booth Qualified'];
        list<CampaignMemberStatus> CMS5 = [Select ID, Label, CampaignId From CampaignMemberStatus Where CampaignId =: testCampaign.Id And Label = 'Booth Non-Qualified'];
        
        system.assertEquals(True, CMS1[0].isDefault, 'We expect Registered to be default value');
        system.assertEquals('Qualified', CMS2[0].Label, 'We expect the label to be Qualified');
        system.assertEquals('Non-Qualified', CMS3[0].Label, 'We expect the label to be Non-Qualified');
        system.assertEquals('Booth Qualified', CMS4[0].Label, 'We expect the label to be Booth Qualified');
        system.assertEquals('Booth Non-Qualified', CMS5[0].Label, 'We expect the label to be Booth Non-Qualified');
        
    }
}