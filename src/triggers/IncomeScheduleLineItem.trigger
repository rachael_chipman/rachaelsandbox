trigger IncomeScheduleLineItem on c2g__codaScheduleLineItem__c (after insert,before insert) {
	
	if (trigger.isBefore && trigger.isInsert){
		IncomeScheduleLineItemService.populateScheduleInvoiceLineItems(trigger.new);
	}
	if (Trigger.isAfter && Trigger.isInsert /*&& !IncomeScheduleLineItemService.isTriggerRunning*/ ){
		IncomeScheduleLineItemService.isTriggerRunning = true;

		List<c2g__codaScheduleLineItem__c> updatedScheduleLineItems = IncomeScheduleLineItemService.processIncomeScheduleLineItemAmounts( trigger.new );
		if( !updatedScheduleLineItems.isEmpty() )
		{
			IncomeScheduleLineItemService.processAmountBalancing( updatedScheduleLineItems );
		}
	} 
}