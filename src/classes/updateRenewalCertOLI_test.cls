@isTEST
public class updateRenewalCertOLI_test{
    
    /*public static Opportunity originalOpp;
    public static OpportunityLineItem originalCertOLI;
    public static OpportunityLineItem EISOli;
    
    static void setupOppandOli()
    {
        User user = new user(Alias = 'standt', Email='tes@rp.com', 
        EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
        LocaleSidKey='en_US', ProfileId = '00e0000000734LT', 
        TimeZoneSidKey='America/Los_Angeles', UserName='test@rp.com.rp');
        insert user;
        
        Account account = new Account (Name = 'Test Account', OwnerId = user.Id);
        insert account;
        
        originalOpp = new Opportunity();
        originalOpp.AccountId = account.Id;
        originalOpp.OwnerId = user.Id;
        originalOpp.Type = 'New Business';
        originalOpp.StageName = 'Signature';
        originalOpp.Name = 'Original Opp';
        originalOpp.CloseDate = system.today();
        originalOpp.Pricebook2Id = '01s000000004NS6AAM';
        
        originalCertOLI = new OpportunityLineItem();
        originalCertOLI.Quantity = 1;
        originalCertOLI.UnitPrice = 7500.00;
        originalCertOLI.Product_Status__c = 'Current';
        originalCertOLI.Description__c = 'Return Path Certification';
        originalCertOLI.Active__c = true;
        originalCertOLI.Recurring__c = true;
        originalCertOLI.PricebookentryId = '01u00000000FGwSAAW';
        
        EISOli = new OpportunityLineItem(
        PricebookEntryId = '01u00000000FGw7AAG',
        Product_Status__c = 'Current',
        Start_Date__c = system.today(),
        End_Date__c = system.today() +394,
        UnitPrice = 1500.00,
        Previous_Value__c = 0.00,
        Recurring__c = true,
        Product_Detail_Created__c = false, 
        Quantity = 1);
            
    }
    
    /*private static testMethod void testSetUpOpp()
    {
        setupOppandOli();
        insert originalOpp;
        originalCertOLI.OpportunityId = originalOpp.Id;
        EISOli.OpportunityId = originalOpp.Id;
                                                                                                      
        insert new List<OpportunityLineItem>{originalCertOLI, EISOli};
        
        originalOpp.Send_Monkey_Email__c = 'Monkey Sent';
        originalOpp.StageName = 'Finance Approved';
        originalOpp.Begin_Automation__c = true;
        update originalOpp;
        
        List<Opportunity> RenewalOpps = [SELECT Id, Previous_Opportunity_Lookup__c FROM Opportunity WHERE Previous_Opportunity_Lookup__c = :originalOpp.Id];
        system.assertEquals(1,RenewalOpps.size(), 'There should be a renewal opp');
        
        List<Opportunity> ClonedOpp = [SELECT Id, Cloned_From_Id__c FROM Opportunity WHERE Cloned_From_Id__c = :originalOpp.Id];
        system.assertEquals(1, ClonedOpp.size(), 'There should be a cloned opp');
        
        test.startTest();
        ClonedOpp[0].StageName = 'Finance Approved';
        ClonedOpp[0].Application_Result__c = 'Pass';
        ClonedOpp[0].Activation_Date__c = system.today();
        ClonedOpp[0].Certification_Tier_Sold__c = 'Tier 4';
        ClonedOpp[0].Renewal_Updated__c = false;
        update ClonedOpp[0];
        test.stopTest();
        
        List<Opportunity> RenewalOpps2 = [SELECT Id, Previous_Opportunity_Lookup__c, Activation_Date__c FROM Opportunity WHERE Id = :RenewalOpps[0].Id];
        system.assertEquals(1, RenewalOpps2.size(), 'There should only be one Renewal Opportunity');
        system.assertEquals(system.today(), RenewalOpps2[0].Activation_Date__c, 'The activation date should be populated on the renewal');
    }
    
    private static testMethod void testSetUpOpp_Termination()
    {
        setupOppandOli();
        insert originalOpp;
        originalCertOLI.OpportunityId = originalOpp.Id;
        EISOli.OpportunityId = originalOpp.Id;
                                                                                                      
        insert new List<OpportunityLineItem>{originalCertOLI, EISOli};
        
        originalOpp.Send_Monkey_Email__c = 'Monkey Sent';
        originalOpp.StageName = 'Finance Approved';
        originalOpp.Begin_Automation__c = true;
        update originalOpp;
        
        List<Opportunity> RenewalOpps = [SELECT Id, Previous_Opportunity_Lookup__c FROM Opportunity WHERE Previous_Opportunity_Lookup__c = :originalOpp.Id];
        system.assertEquals(1,RenewalOpps.size(), 'There should be a renewal opp');
        
        List<Opportunity> ClonedOpp = [SELECT Id, Cloned_From_Id__c FROM Opportunity WHERE Cloned_From_Id__c = :originalOpp.Id];
        system.assertEquals(1, ClonedOpp.size(), 'There should be a cloned opp');
        
        test.startTest();
        ClonedOpp[0].StageName = 'Terminated Contract';
        ClonedOpp[0].Application_Result__c = 'Pass';
        ClonedOpp[0].Activation_Date__c = system.today();
        ClonedOpp[0].Certification_Tier_Sold__c = 'Tier 4';
        ClonedOpp[0].Renewal_Updated__c = false;
        update ClonedOpp[0];
        test.stopTest();
        
        
    }*/


}