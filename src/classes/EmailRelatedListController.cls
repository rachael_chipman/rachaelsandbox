public class EmailRelatedListController{

    public Certification_Application__c  ca;
    public Case cas {get;set;}
    public Case casNew {get;set;}
    public List <EmailMessage> mail {get;set;}
    Public string strID{get;set;}
    Public string usrID{get;set;}
    Public string p4 = '';
    Boolean hasmail = true;
    Boolean displaymail = false;
    
     public EmailRelatedListController(ApexPages.StandardController controller) {  
     
     ca = [SELECT Id, Has_Cases__c, Account__c FROM Certification_Application__c WHERE Id = :ApexPages.currentPage().getParameters().get('Id')];
     
     usrID = UserInfo.getUserId();
     
         
      casNew = New Case(
        OwnerId = usrID,
        AccountId = ca.Account__c, 
        Certification_Application__c = ca.Id,
        RecordTypeId = '012000000004x48',
        Status = 'New',
        Origin = 'Cert App Email',
        Priority = 'Medium'); 

     
   
     if(ca.Has_Cases__c == true) {
     cas = [SELECT Id,  ContactId from Case WHERE Certification_Application__c = :ca.Id order by CreatedDate DESC limit 1];
     mail =[SELECT Id, textBody, ToAddress, FromAddress, Subject, Status, MessageDate from EmailMessage WHERE ParentId = :cas.Id order by MessageDate desc];
     
     
     if(mail.size() > 0){hasmail = false; displaymail = true;}
     if(mail.size() == 0){hasmail = true; displaymail = false;}   
    
     }
     }
     
     public pagereference deletecase(){
     
     if(ca.Has_Cases__c== true){
     if(mail.size() == 0){
     delete cas;
     ca.Has_Cases__c=false;
     update ca;
     
     }
     
     }
     return null;
     }
     
     public Boolean gethasmail(){
     
     return hasmail;
     
     }
     
     public Boolean getdisplaymail(){
     
     return displaymail;
     
     }
     
     
     Public PageReference reply(){
     
     
     pageReference ref = new PageReference('/_ui/core/email/author/EmailAuthor?email_id='+strID+'&replyToAll=0&retURL=/'+ca.Id);
    ref.setRedirect(true);
    return ref;
     }
     
     Public PageReference replyall(){
     
         
     EmailMessage copycheck = [select Id, CcAddress from EmailMessage WHERE Id =:strID];
     
      if(copycheck.CcAddress != null){    
         
     String s = copycheck.CcAddress;
     String selectedVal = 'bio.case@returnpath.com';
     String selectedVal2 = 'bio.case@returnpath.com;';
     s = s.replace(selectedVal,'');
     s = s.replace(selectedVal2,'');
     
     p4 = s;}
     
     if(copycheck.CcAddress == null){ p4='';}
     
     pageReference ref = new PageReference('/_ui/core/email/author/EmailAuthor?email_id='+strID+'&replyToAll=1&p4='+p4+'&retURL=/'+ca.Id);
    ref.setRedirect(true);
    return ref;
     }
     
          Public PageReference read(){
     
     pageReference ref = new PageReference('/'+strID);
    ref.setRedirect(true);
    return ref;
     }
     
     
           Public PageReference create(){
    
     insert casNew;
     update casNew;
     
     Case cas2 = [select Id, Case_Id_Copy__c from Case where Id = :casNew.Id];
     
     if(ca.Has_Cases__c == false){ ca.Has_Cases__c = true; update ca;}

    pageReference ref = new PageReference('/_ui/core/email/author/EmailAuthor?rtype=003&p3_lkid='+cas2.Case_Id_Copy__c+'&retURL=%2F'+cas2.Case_Id_Copy__c);
    ref.setRedirect(true);
    return ref;
     }    

     }