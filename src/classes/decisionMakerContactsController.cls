public class decisionMakerContactsController{
    
    public list <Contact> dMconts {get;set;}
    public List <RP_Survey__c> healthCheckSurveys {get;set;}
	   
    public decisionMakerContactsController(){
        
        dMconts = [SELECT ID, Name, Email, Contact_Status__c, ContactsRole__c, Phone, LastModifiedDate, Title 
                   FROM Contact 
                   WHERE AccountId =: ApexPages.currentPage().getParameters().get('id') AND ContactsRole__c INCLUDES ('Decision Maker') AND Contact_Status__c = 'Current'];
        
        healthCheckSurveys = [SELECT ID, Name, Avg_Health_Check_Score__c 
                              FROM RP_Survey__c 
                              WHERE Account__c =: ApexPages.currentPage().getParameters().get('id') AND RecordType.Name = 'Health Check'];
        
    }
}