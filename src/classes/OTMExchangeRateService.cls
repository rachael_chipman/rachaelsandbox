public without sharing class OTMExchangeRateService {
    
    public static final String FINANCE_APPROVED_STAGE = 'Finance Approved';
    public static final String PENDING_CERT_STAGE = 'Pending Certification Qualification';
    public static final String TERMINATED_STAGE = 'Terminated Contract';
    
    public static void processTeamMembers(List<Opportunity> newOpps, Map<Id, Opportunity> oldOpps, Map<Id,Opportunity> opportunityMap)
    {
        List<Opportunity> filteredOpportunities = filterOpportunitiesOnInsertAndUpdate(newOpps, oldOpps);
        system.debug('Filtered Opportunities'+ filteredOpportunities);
        if(!filteredOpportunities.isEmpty())
        {
            updateOppTeamMembers( filteredOpportunities, opportunityMap );
        }
    }
    
    public static List<Opportunity> filterOpportunitiesOnInsertAndUpdate(List<Opportunity> newOpps, Map<Id, Opportunity> oldOpps)
    {
        List<Opportunity> filteredOpps = new List<Opportunity>();
        
        for(Opportunity opp : newOpps)
        {
            Opportunity oldOpp;
            if(oldOpps != NULL)
            {
                oldOpp = oldOpps.get(opp.Id);
            }
            
            if((oldOpp == NULL) || (oldOpp != NULL && opp.StageName != oldOpp.StageName && (opp.StageName == FINANCE_APPROVED_STAGE || opp.StageName == PENDING_CERT_STAGE || opp.StageName == TERMINATED_STAGE)) || ( opp.Update_Team_Member_Exchange_Rates__c && !oldOpp.Update_Team_Member_Exchange_Rates__c ) )
            {
                filteredOpps.add(opp);
            } 
        }
        return filteredOpps;
    }
    
    public static void updateOppTeamMembers(List<Opportunity> filteredOpps, Map<Id, Opportunity> oppMap)
    {
        Set<Id> oppIds = oppMap.keySet();
        Set<String> currencyCodes = pluck.strings('CurrencyISOCode', filteredOpps);
        system.debug('What is in Currency Codes?'+currencyCodes);
        
       /* Set<Date> closeDates = pluck.dates('CloseDate', filteredOpps);
        List<Date> closeDatesToStartOfMonth = new List<Date>();
        List<Date> closeDatesToStartOfMonthNextYear = new List<Date>();
        for(Date closeDate : closeDates)
        {
        	closeDatesToStartOfMonth.add(closeDate.toStartOfMonth());
        	closeDatesToStartOfMonthNextYear.add(closeDate.toStartOfMonth().addYears(1))
        }*/
        
        List<DatedConversionRate> conversionRates = [SELECT ConversionRate, IsoCode, StartDate FROM DatedConversionRate WHERE IsoCode IN : currencyCodes];
        system.debug('How many conversionRates'+conversionRates.size());
        List<CurrencyType> currencyTypes = [SELECT ConversionRate, IsoCode from CurrencyType where IsoCode IN : currencyCodes];
        system.debug('How many currencyTypes'+currencyTypes.size());
        Map<String, DatedConversionRate> isoCodeToConversionRate = new Map<String, DatedConversionRate>();
        Map<String, DatedConversionRate> filteredISOCodeToConversionRate = new Map<String, DatedConversionRate>();
        Set<Date> startDates = pluck.dates('StartDate', conversionRates);
        system.debug('What are the start dates?'+startDates);
        
        for(Opportunity opp : filteredOpps)
        {
	        for(DatedConversionRate convRate : conversionRates)
	        {
	            if(opp.CloseDate.toStartOfMonth() == convRate.StartDate.toStartOfMonth())
	            {
	            	isoCodeToConversionRate.put(convRate.IsoCode, convRate);
	            }
	            else if( opp.CloseDate > system.today() && convRate.StartDate.toStartOfMonth() == system.today().toStartOfMonth())
	            {
	            	isoCodeToConversionRate.put(convRate.IsoCode, convRate);
	            }
	        }
	        	/*if(isoCodeToConversionRate.containsKey(opp.CurrencyISOCode))
	        	{
	        		DatedConversionRate dcr = isoCodeToConversionRate.get(opp.CurrencyISOCode);
	        		system.debug('What is dcr?' + dcr);
		        		if(opp.CloseDate.toStartOfMonth() == dcr.StartDate.toStartOfMonth() 
		        		|| opp.CloseDate.toStartOfMonth() > system.today())
		        		{
		        			filteredISOCodeToConversionRate.put(dcr.IsoCode, dcr);
		        		}
	        	}*/
        }

        system.debug('What is in the conversion rate map?'+isoCodeToConversionRate);
        system.debug('What is in the filtered conversion rate map?'+filteredISOCodeToConversionRate);
        Map<String, CurrencyType> isoCodeToCurrencyType = new Map<String, CurrencyType>();
        
        for(CurrencyType currType : currencyTypes)
        {
            isoCodeToCurrencyType.put(currType.IsoCode, currType);
        }
        system.debug('What is in the currency Type map?'+isoCodeToCurrencyType);
        List<OpportunityTeamMember> oppTeamMembers = [SELECT Id, Exchange_Rate__c, OpportunityId, CurrencyIsoCode FROM OpportunityTeamMember WHERE OpportunityId IN : oppIds];
        system.debug('OpportunityTeamMembers'+oppTeamMembers);
        
        List<OpportunityTeamMember> teamMembersToUpdate = new List<OpportunityTeamMember>();
        List<Opportunity> oppsWithMembers = [SELECT Id, CurrencyISOCode, (SELECT Id FROM OpportunityTeamMembers) FROM Opportunity WHERE Id IN: oppIds ];
        for(Opportunity opp : oppsWithMembers)
        {
            for(OpportunityTeamMember member : opp.OpportunityTeamMembers)
            {
                if( opp.CurrencyIsoCode == 'USD' )
                {
                    member.Exchange_Rate__c = 1.00;
                }
                else
                {
                    member.Exchange_Rate__c = isoCodeToConversionRate.get(opp.CurrencyIsoCode).ConversionRate;
                }
                member.Company_Exchange_Rate__c = isoCodeToCurrencyType.get(opp.CurrencyIsoCode).ConversionRate;
                member.CurrencyISOCode = 'USD';
                teamMembersToUpdate.add(member);
            }
        }
        try
        {
            update teamMembersToUpdate;
        }
        catch (Exception dmx) 
        {
            for(Integer i=0; i<dmx.getNumDml(); i++)
            {
                Integer failedIndex = dmx.getDmlIndex(i);
                String failedMessage = dmx.getDmlMessage(i);
                oppMap.get(oppTeamMembers[failedIndex].OpportunityId).addError(failedMessage);
            }
            }
        }
    }