@isTest
private class TestSalesCreditNoteTrigger 
{

	static List<Opportunity> testCreditNoteOpportunities ;
	static List<c2g__codaCreditNote__c> testCreditNotes = new List<c2g__codaCreditNote__c>();
	static List<c2g__codaCreditNoteLineItem__c> testCreditNoteLineItems = new List<c2g__codaCreditNoteLineItem__c>();
	static integer NUM_OF_LINEITEMS = 3;
	
	private static void dataSetup(){
		FinancialForceTestHelper.baseSetup();
					
		testCreditNoteOpportunities = TestUtils.createOpportunities( 2, TestUtils.INVESTIGATING_RECORD_TYPE.Id, FinancialForceTestHelper.testAccount.Id, false );
		testCreditNotes = new List<c2g__codaCreditNote__c>();
		testCreditNoteLineItems = new List<c2g__codaCreditNoteLineItem__c>();
		for( Opportunity anOpportunity : testCreditNoteOpportunities )
		{
			anOpportunity.StageName = 'Finance Approved';
			anOpportunity.Billing_Type__c = 'Annual';
			anOpportunity.Effective_Date__c =  Date.today();
			anOpportunity.Billing_Terms__c = 'Net 30';
		}
		insert testCreditNoteOpportunities;
		for( Opportunity anOpportunity : testCreditNoteOpportunities )
		{
			testCreditNotes.add( TestUtils.createSalesCreditNote( FinancialForceTestHelper.testAccount.Id, new Map<String,Object>{ 'c2g__Opportunity__c' => anOpportunity.Id }, false ) );
		}
		insert testCreditNotes;
		
	}
	
	@isTest(seeAllData=true) 
	static void testPost() 
	{
		datasetup();
		for( c2g__codaCreditNote__c testCreditNote : testCreditNotes )
		{
			testCreditNoteLineItems.add( TestUtils.createSalesCreditNoteLineItems(1, testCreditNote.Id, FinancialForceTestHelper.testProduct.Id, false )[0] );
		}
		

		insert testCreditNoteLineItems;
		test.startTest();
			FinancialForceTestHelper.bulkPostCreditNotes( Pluck.ids( testCreditNotes ) );
		test.stopTest();

		testCreditNotes = [ SELECT c2g__Transaction__c, c2g__Transaction__r.Opportunity__c, c2g__Opportunity__c FROM c2g__codaCreditNote__c WHERE Id IN :testCreditNotes ];

		for( c2g__codaCreditNote__c testCreditNote : testCreditNotes )
		{
			System.assertEquals( testCreditNote.c2g__Opportunity__c, testCreditNote.c2g__Transaction__r.Opportunity__c, 'Transaction opportunity  lookup should be populated with related credit note\'s opportunity' );
		}
		
		Set<Id> transactionIds = Pluck.ids( 'c2g__Transaction__c', testCreditNotes );

		List<c2g__codaTransactionLineItem__c> actualTransationLineItems = [ SELECT Id, Opportunity__c, c2g__Transaction__c, c2g__Transaction__r.Opportunity__c From c2g__codaTransactionLineItem__c WHERE c2g__Transaction__c IN : transactionIds];
		System.assert( !actualTransationLineItems.isEmpty(), 'There should be transaction line items created' );
		for( c2g__codaTransactionLineItem__c testTransactionLineItem: actualTransationLineItems )
		{
			System.assertEquals( testTransactionLineItem.c2g__Transaction__r.Opportunity__c, testTransactionLineItem.Opportunity__c, 'Transaction line item opportunity  lookup should be populated with related transaction\'s opportunity' );
		}
	}
	
	@isTest(seeAllData=true) 
	static void emptyLineItemOpportunityReference_positiveTest() 
	{
		datasetup();
		for( c2g__codaCreditNote__c testCreditNote : testCreditNotes )
		{
			testCreditNoteLineItems.addAll( TestUtils.createSalesCreditNoteLineItems(NUM_OF_LINEITEMS, testCreditNote.Id, FinancialForceTestHelper.testProduct.Id, false ) );
		}

		test.startTest();
			insert testCreditNoteLineItems;
		test.stopTest();

		testCreditNotes = [ SELECT c2g__Opportunity__c FROM c2g__codaCreditNote__c WHERE Id IN :testCreditNotes ];
		List<c2g__codaCreditNoteLineItem__c> updatedLineItems = [Select  Opportunity__c, c2g__CreditNote__c, c2g__CreditNote__r.c2g__Opportunity__c From c2g__codaCreditNoteLineItem__c 
							where c2g__CreditNote__c in :testCreditNotes];

		for( c2g__codaCreditNoteLineItem__c updatedLineItem: updatedLineItems )
		{
			System.assertNotEquals(null,updatedLineItem.Opportunity__c,'The line item oppotunity lookup is populated');
			System.assertEquals( updatedLineItem.c2g__CreditNote__r.c2g__Opportunity__c, updatedLineItem.Opportunity__c,'line item Opportunity lookup is populated from credit note ');
		}
	}	

}