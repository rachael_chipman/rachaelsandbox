trigger Update_OwnerCopy_on_Owner_Update on Account (before update, before insert) {

    //When 'Owner' field is changed, update 'Owner Copy as well
    
      // Loop through the incoming records
      for (Account a : Trigger.new) {
         
           //Has Owner changed?
           if (a.OwnerID != a.Owner_Copy__c) {
               a.Owner_Copy__c = a.OwnerID;
           }
      }        
}