trigger ClonedPendingCertOpp on Opportunity (Before update) {
 
 /*list<OpportunityLineItem> CertLineItemInsert = new list<OpportunityLineItem>(); 
 list<OpportunityTeamMember> otmInsert = new list<OpportunityTeamMember>();
 list<Attachment> attInsert = new list<Attachment>();
 list<OpportunityLineItem> OlisToDelete = new list<OpportunityLineItem>();
 
 //populate varible with text and formated version of NOW()
 Datetime myDT = Datetime.now();
 String myDTstring = myDT.format('yyyy-MM-dd HH:mm', 'GMT'); 
 
 
    
    for(Opportunity opploop1 : trigger.new){ 
    
       
    if(opploop1.RecordtypeId == '012000000004x5k' 
    && opploop1.StageName == 'Finance Approved' 
    && opploop1.Cert_Only_Opportunity__c == 'No' 
    && opploop1.Has_Cert_Products_Qty__c > 0 
    && (opploop1.Type != 'Fee' && opploop1.Type != 'Supplement' && opploop1.Type != 'Extension')
    && opploop1.Account.Certification_Client__c == 0){
    
    
    
          
            OpportunityLineItem[] CertProds  = [select Id, Previous_Value__c 
        FROM OpportunityLineItem
        WHERE OpportunityId = :opploop1.Id and (Product_Code__c = '004-CERT-FEE0' or Product_Code__c = '005-CERT-REFC') and Product_Status__c = 'Current'
        and Previous_Value__c = 0];                         
    
    
    if(CertProds.size() > 0){    
    

    // Get Finance Approved Opps, non-cert only, where ACV already ran and Monkey has been sent
        
        if(opploop1.Approval_Type__c == 'Finance Approval' && opploop1.StageName == 'Finance Approved' && opploop1.Cert_Only_Opportunity__c == 'No' 
            && opploop1.Run_ACV__c == 'No' && (opploop1.Send_Monkey_Email__c == 'Monkey Sent' || opploop1.Send_Monkey_Email__c == 'No Monkey Needed')
            && opploop1.Cloned__c == false ){
            
            

    //Clone the opportunity and populate the fields that will indicate it is a cloned opp
        
         Opportunity PendingCertOpp = opploop1.clone(false);
         PendingCertOpp.StageName = 'Pending Certification Qualification';
         PendingCertOpp.Approval_Type__c = 'Certification Approval';
         PendingCertOpp.Cloned_From_ID__c = opploop1.Id;
         PendingCertOpp.Original_Opportunity__c = 0;
         PendingCertOpp.Run_ACV__c = 'No';
         PendingCertOpp.Record_Locked__c = false;
         PendingCertOpp.Product_Additional_AM_Services__c = false;
         PendingCertOpp.Product_Campaign_Preview_API__c = false;
         PendingCertOpp.Product_CorpDev_Research__c = false;
         PendingCertOpp.Product_Domain_Assurance__c = false;
         PendingCertOpp.Product_Domain_Audit_Service__c = false;
         PendingCertOpp.Product_Domain_Protect__c = false;
         PendingCertOpp.Product_Domain_Protect_Child__c = false;
         PendingCertOpp.Product_Domain_Secure__c = false;
         PendingCertOpp.Product_Domain_Secure_Child__c = false;
         PendingCertOpp.Product_Tools_Child__c = false;
         PendingCertOpp.Product_Gold__c = false;
         PendingCertOpp.Product_Internal_Use__c = false;
         PendingCertOpp.Product_Platinum__c = false;
         PendingCertOpp.Product_Email_Brand_Monitor__c = false;
         PendingCertOpp.Product_Email_Brand_Monitor_Child__c = false;
         PendingCertOpp.Product_Pixel_Tracking__c = false;
         PendingCertOpp.Product_General_Referral__c = false;
         PendingCertOpp.Product_Inbox_Measurement__c = false;
         PendingCertOpp.Product_Managed_Delivarbility__c = false;
         PendingCertOpp.Product_Preferred_Partner_Framework__c = false;
         PendingCertOpp.Product_Deliverability_Consulting__c = false;
         PendingCertOpp.Product_Regional_ISP_Mediation__c = false;
         PendingCertOpp.Product_ROI_and_Response_Consulting__c = false;
         PendingCertOpp.Product_RPost__c = false;
         PendingCertOpp.Product_Safe__c = false;
         PendingCertOpp.Product_Sender_Score_Premium__c = false;
         PendingCertOpp.Product_SSRA__c = false;
         PendingCertOpp.Product_Strategic_Media_Services__c = false;
         PendingCertOpp.Product_Technology_Services__c = false;
         PendingCertOpp.Product_Tools_Only__c = false;
         PendingCertOpp.Product_Tools_Reseller_API__c = false;
         PendingCertOpp.Product_Tools_Reseller_Framework__c = false;
         
         Insert PendingCertOpp;
         
    //Get all the Certification product line items from the original opp     
      
        list<OpportunityLineItem> CertProductsInOpps = new list<OpportunityLineItem>([SELECT Product_Status__c, Id, Quantity, UnitPrice, Description__c, 
            OpportunityId, PricebookEntryId, Product_code__c, Previous_Value__c, Active__c, Indirect__c, Recurring__c, Bundle__c, 
            Start_date__c, End_date__c 
            FROM OpportunityLineItem 
            WHERE OpportunityId = :opploop1.Id and (Product_Code__c = '004-CERT-FEE0' or Product_Code__c = '005-CERT-REFC')]);

    
    // Insert the Certification product line items into the cloned opportunity
      
      for(OpportunityLineItem prodloop1 : CertProductsInOpps){
                    OpportunityLineItem ProdCreate = new OpportunityLineItem();
                    ProdCreate.OpportunityId = PendingCertOpp.id;
                    ProdCreate.PricebookEntryId = prodloop1.pricebookentryid;
                    ProdCreate.Quantity = prodloop1.Quantity;
                    ProdCreate.UnitPrice = prodloop1.UnitPrice;
                    ProdCreate.Product_Status__c = prodloop1.Product_Status__c;
                    ProdCreate.Description__c = prodloop1.Description__c;
                    ProdCreate.Indirect__c = prodloop1.Indirect__c;
                    ProdCreate.Active__c = prodloop1.Active__c;
                    ProdCreate.Bundle__c = prodloop1.Bundle__c;
                    ProdCreate.Recurring__c = prodloop1.Recurring__c;
                    
                    CertLineItemInsert.add(ProdCreate);
                                        }
                                        
                    Insert CertLineItemInsert;
                   
      //Get the Opportunity Team Members from the original opportunity          
            
        list<OpportunityTeamMember> OppTeamMembers = new list<OpportunityTeamMember>([SELECT Id, OpportunityId, OpportunityAccessLevel, TeamMemberRole, 
             UserId, Percent_Commissioned_On__c, Commission_Percentage__c
             FROM OpportunityTeamMember
             WHERE OpportunityId = :opploop1.Id]);
      
      //Insert the Opportunity Team Members into the cloned opportunity
             
             for(OpportunityTeamMember otm : OppTeamMembers){
                 OpportunityTeamMember otmadd = new OpportunityTeamMember();
                     otmadd.OpportunityId = PendingCertOpp.Id;
                     otmadd.UserId = otm.UserId;
                     otmadd.TeamMemberRole = otm.TeamMemberRole;
                     otmadd.Percent_Commissioned_On__c = otm.Percent_Commissioned_On__c;
                     otmadd.Commission_Percentage__c = otm.Commission_Percentage__c;
                     otmInsert.add(otmadd);}
                     
                     Insert otmInsert;
                     
      //Get the Attachments from the original opportunity
      
        list<Attachment> AttachmentsInOpps = new list<Attachment>([SELECT Id, Body, Description, Name, OwnerId, ParentId
            FROM Attachment
            WHERE ParentId = :opploop1.Id]);
            
       //Insert the Attachments into the cloned opportunity
       
             for(Attachment att : AttachmentsInOpps){
                 Attachment attadd = new Attachment();
                     attadd.Body = att.Body;
                     attadd.Description = att.Description;
                     attadd.Name = att.Name;
                     attadd.OwnerId = att.OwnerId;
                     attadd.ParentId = PendingCertOpp.Id;
                     attInsert.add(attadd);}
                     
                     Insert attInsert;
                            
                     
      //Update the original opportunity to Finance Approved, mark it "cloned"   
                  
                     opploop1.Cloned__c = true;
                     opploop1.Product_Certification__c = false;
                     opploop1.Product_SSC_Referral_Child__c = false;
                     opploop1.Run_OCV__c = true;
                     
                     
                            
       }  
      }
      }
    } */       
}