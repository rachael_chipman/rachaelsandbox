public class contractExtensionController{
	
	public contractExtensionController(ApexPages.StandardController controller) {}

	public Account a {
		get {
		List<Contract> c = [select accountid from contract];
		a = [select id, ACV_TOTAL__c from account where id = :c[0].accountid];
		return a;
	}
		set;
	}
	
}