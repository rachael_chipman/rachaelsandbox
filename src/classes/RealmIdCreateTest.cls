@isTest
public class RealmIdCreateTest {
    static testMethod void runRealmIdCreateTest() {       
        account a = new account(name = 'test account');
        insert a;
        
        opportunity o = new opportunity(name='test opp',closedate=date.today(),stagename='Finance Approved', accountid=a.id);
        insert o;
        
        test.startTest();
        o.realmid__c = '123456';
        update o;
        test.stopTest();
    }
}