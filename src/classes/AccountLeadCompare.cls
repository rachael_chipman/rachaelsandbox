///////////////////This is how the batch class is called
//////////id batchinstanceid = database.executeBatch(new AccountLeadCompare('select Id, Name from Account'), 25);

global class AccountLeadCompare implements Database.Batchable<sObject>
{
global final String Query;
global AccountLeadCompare(String q)
{
Query=q;
}

global Database.QueryLocator start(Database.BatchableContext BC)
{
return Database.getQueryLocator(query);
}

global void execute(Database.BatchableContext BC,List<Account> scope){
List <Matching__c> m = New List<Matching__c>();
for(Account ac : scope){

String acctname = ac.Name;  

    
    for (List <Lead> lds : [select Id from Lead where Isconverted = false and Company Like :('%'+acctname+'%')]){
        for(Lead ls : lds){    
        Matching__c match = New Matching__c(
            Lead__c = ls.Id,
            Account__c = ac.Id);
        m.add(match);
    }
    }   
}
insert m;

}


global void finish(Database.BatchableContext BC){
//Send an email to the User after your batch completes
Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
String[] toAddresses = new String[] {'rachael.chipman@returnpath.com'};
mail.setToAddresses(toAddresses);
mail.setSubject('Apex Batch Job is done');
mail.setPlainTextBody('The batch Apex job processed');
Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
}
}