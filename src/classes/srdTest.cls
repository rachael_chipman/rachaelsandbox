@IsTest

public class srdTest {
    
	public static testMethod void testOpenController() {
        
		Account a = new Account(name = 'TestAccount');
		a.Accred_Recurring_Direct__c = 25000;
		insert a;
        
		Opportunity o = new Opportunity(name = 'TestOpp',StageName = 'Negotiating',CloseDate = date.today(),SRD__c = 'NA',AccountId = a.id);
		insert o;
        
		OpportunityLineItem oli = new OpportunityLineItem(Quantity = 1,OpportunityId = o.id,PriceBookEntryId = '01u00000000FGwyAAG',Description__c = 'Response & ROI Consulting: SRD Consulting Related to Metric Change',UnitPrice = 0);
		insert oli;
        
		PageReference p = new PageReference('/'+o.id);
		Test.setCurrentPage(p);
		
		PageReference pApproval = Page.SRD_Details;
		pApproval.getParameters().put('id',o.id);
		Test.setCurrentPage(pApproval);
		srdDetailController detailController = new srdDetailController();
		detailController.vol = '25';
		detailController.delta= '50';
		detailController.Ips = '75';
		detailController.sendDetails();
		
	}
}