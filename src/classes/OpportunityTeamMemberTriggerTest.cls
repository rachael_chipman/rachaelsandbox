@isTest
public class OpportunityTeamMemberTriggerTest {
    
   
    private static testMethod void testOTMInsert()
    {
        //setup test record
        Account Acc = testutils.createAccount(TestUtils.MARKETER_RECORD_TYPE.Id, true);
        Opportunity opp = TestUtils.createOpportunity(TestUtils.RESEARCH_RECORD_TYPE.Id, Acc.Id, true);
        opp.StageName = 'Finance Approved';
        update opp;
        OpportunityTeamMember OTM = TestUtils.createOpportunityTeamMember(opp.Id, TestUtils.ADMIN_USER.Id,'Sales rep', true);
        OpportunityTeamMember OppTM = [Select Id, Name,Exchange_Rate__c from OpportunityTeamMember Where ID =:OTM.ID];
        System.assertNotEquals(OppTM.Exchange_Rate__c, null);
     }
    
    private static testMethod void testbulkOTMInsert()
    {
        
        //setup test record
        List<Opportunity> oppslist = new List<Opportunity>();
        Account Acc = testutils.createAccount(TestUtils.MARKETER_RECORD_TYPE.Id, true);
        Opportunity opp = TestUtils.createOpportunity(TestUtils.RESEARCH_RECORD_TYPE.Id, Acc.Id, true);
        opp.StageName = 'Finance Approved';
        update opp;
        Test.startTest();
        List<OpportunityTeamMember> OPPTMlist = TestUtils.createOpportunityTeamMembers(200,opp.Id, TestUtils.ADMIN_USER.Id,'Sales rep', true);
        Test.stopTest();
        List<OpportunityTeamMember> OppTMs = [Select Id, Name,Exchange_Rate__c from OpportunityTeamMember Where ID in:OppTMlist];
        for(OpportunityTeamMember TM: OppTMs)
        {
        	System.assertNotEquals(TM.Exchange_Rate__c, null);
        }
     }

}