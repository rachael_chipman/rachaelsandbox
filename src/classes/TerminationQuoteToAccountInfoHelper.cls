public with sharing class TerminationQuoteToAccountInfoHelper {
	
	/*This purpose of this class is to grab specific information from cancellation
	quotes and push it over to the related account, the account fields are being used
	by Gainsight*/

	public static FINAL String SENT_TO_ZBILL = 'Sent to Z-Billing';
	public static RecordType CANCEL_READ_ONLY
	{
		get
		{
			if(CANCEL_READ_ONLY == NULL)
			{
				CANCEL_READ_ONLY = [SELECT Id FROM RecordType WHERE SObjectType = 'zqu__Quote__c' AND DeveloperName = 'Cancellation_ReadOnly'];
			}
			return CANCEL_READ_ONLY;
		}
		private set;
	}

	public static Map<Id, zqu__Quote__c> getAccountsWithCancellationQuotes(List<zqu__Quote__c> newQuotes, Map<Id, zqu__Quote__c> oldQuoteMap)
	{
		Map<Id, zqu__Quote__c> accountIdToQuote = new Map<Id, zqu__Quote__c>();

		for(zqu__Quote__c quote : newQuotes)
		{
			if(!oldQuoteMap.isEmpty() && quote.RecordTypeId == CANCEL_READ_ONLY.Id)
			{
				if(oldQuoteMap.get(quote.Id).zqu__Status__c != SENT_TO_ZBILL && quote.zqu__Status__c == SENT_TO_ZBILL)
				{
					accountIdToQuote.put(quote.zqu__Account__c, quote);
				}
			}
		}
		return accountIdToQuote;
	}

	public static Map<Id, Decimal> getTerminationACV(List<zqu__Quote__c> cancellationQuotes)
	{
		Map<Id, Decimal> quoteIdToTermACV = new Map<Id, Decimal>();

		List<zqu__Quote__c> queriedQuotes = [SELECT Id, zqu__Opportunity__r.Prev_Recurring_ACV_Before_Partner_Comm__c FROM zqu__Quote__c WHERE Id IN : cancellationQuotes];
		for(zqu__Quote__c quote : queriedQuotes)
		{
			quoteIdToTermACV.put(quote.Id, quote.zqu__Opportunity__r.Prev_Recurring_ACV_Before_Partner_Comm__c);
		}
		return quoteIdToTermACV;
	}

	public static Map<Id, String> getProductNamesFromOlis(List<zqu__Quote__c> quotesToProcess)
	{
		Map<Id, String> quoteIdToProdAbbrev = new Map<Id, String>();
		Set<Id> oppIds = pluck.Ids('zqu__Opportunity__c', quotesToProcess);
		List<OpportunityLineItem> lineItemRecords = [SELECT Id, Product2Id, PricebookEntryId, PricebookEntry.Product2.Name, OpportunityId FROM OpportunityLineItem WHERE OpportunityId IN : oppIds];
		system.debug('Show line items: ' + lineItemRecords);
		Map<Id, List<OpportunityLineItem>> oppIdToLineItems = groupBy.Ids('OpportunityId', lineItemRecords);
		Map<String, String> productNameToAbbreviation = getAbbreviationValues(lineItemRecords);

		for(zqu__Quote__c quote : quotesToProcess)
		{
			Set<String> productNames = new Set<String>();
			String abbreviation = '';
			if(oppIdToLineItems.containsKey(quote.zqu__Opportunity__c))
			{
				for(OpportunityLineItem oli : oppIdToLineItems.get(quote.zqu__Opportunity__c))
				{
					system.debug('Show oli Name: ' + oli.PricebookEntry.Product2.Name);
					if(!productNames.contains(oli.PricebookEntry.Product2.Name) && productNameToAbbreviation.containsKey(oli.PricebookEntry.Product2.Name))
					{
						abbreviation += productNameToAbbreviation.get(oli.PricebookEntry.Product2.Name) + '_';
						productNames.add(oli.PricebookEntry.Product2.Name);
					}
				}
			}
			abbreviation = abbreviation.removeEnd('_');
			quoteIdToProdAbbrev.put(quote.Id, abbreviation);
			productNames.clear();
		}
		return quoteIdToProdAbbrev;
	}

	public static Map<String, String> getAbbreviationValues(List<OpportunityLineItem> lineItemRecords)
	{
		Map<String, String> productNameToAbbreviation = new Map<String, String>();
		Set<String> productNames = new Set<String>();
		for(OpportunityLineItem oli : lineItemRecords)
		{
			productNames.add(oli.PricebookEntry.Product2.Name);
		}
		system.debug('Show oli Names: ' + productNames);
		List<Product2> queriedProducts = [SELECT Id, Name, Abbreviation__c FROM Product2 WHERE Name IN : productNames];

		for(Product2 prod : queriedProducts)
		{
			productNameToAbbreviation.put(prod.Name, prod.Abbreviation__c);
		}
		return productNameToAbbreviation;
	}

	public static void updateAccount(Map<Id, zqu__Quote__c> accountIdToQuote)
	{
		Map<Id, String> quoteIdToProdAbbrev = getProductNamesFromOlis(accountIdToQuote.values());
		Map<Id, Decimal> quoteIdToTermACV = getTerminationACV(accountIdToQuote.values());
		List<Account> accountsToUpdate = new List<Account>();

		for(zqu__Quote__c quote : accountIdToQuote.values())
		{
			Account acc = new Account(Id = quote.zqu__Account__c);
			acc.Most_Recent_Termination_Date__c = quote.zqu__CancellationDate__c;
			acc.Termination_ACV__c = quoteIdToTermACV.get(quote.Id);
			acc.Most_Recent_Terminated_Product__c = quoteIdToProdAbbrev.get(quote.Id);
			accountsToUpdate.add(acc);
		}
		try
		{
			update accountsToUpdate;
		}
		catch (DmlException dml)
		{
			for(Integer i = 0; i < dml.getNumDml(); i++)
			{
				accountIdToQuote.get(dml.getDmlId(i)).addError(dml.getDmlMessage(i));
				system.debug('Error message: ' + dml.getDmlMessage(i));
			}
		}
	}

	public static void pushCancelTermInfoToAccount(List<zqu__Quote__c> newQuotes, Map<Id, zqu__Quote__c> oldQuoteMap)
	{
		Map<Id, zqu__Quote__c> accIdToQuote = getAccountsWithCancellationQuotes(newQuotes, oldQuoteMap);
		if(!accIdToQuote.isEmpty() && accIdToQuote != NULL)
		{
			updateAccount(accIdToQuote);
		}
	}
}