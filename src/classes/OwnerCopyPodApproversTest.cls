@isTest
private with sharing class OwnerCopyPodApproversTest {
	
	private static User user1;
	private static User user2;
	private static Account account;
	
	private static void setUpMethod()
	{
		user1 = [SELECT Id, First_Approver__c, Second_Approver__c, User_Pod_Number__c FROM User WHERE isActive = true and First_Approver__c != NULL LIMIT 1];
		user2 = [SELECT Id, First_Approver__c, Second_Approver__c, User_Pod_Number__c FROM User WHERE isActive = true and First_Approver__c != NULL and Id != :user1.Id LIMIT 1];
		
		account = new Account (Name = 'Test Account for Owner Copy');
		insert account;

	}
	
	private static testMethod void testBulkOpportunityInsert()
	{
		setUpMethod();
		List<Opportunity> insertTestOpps = new List<Opportunity>();
		List<Opportunity> testOpps = TestUtils.createOpportunities(200, TestUtils.RESEARCH_RECORD_TYPE.Id , account.Id, false);
		for(Opportunity testOpp : testOpps)
		{
			testOpp.OwnerId = user1.Id;
			insertTestOpps.add(testOpp);
		}
		test.startTest();
		insert insertTestOpps;
		test.stopTest();
		
		List<Opportunity> insertedOpps = [SELECT Id, Owner_Copy__c, First_Approver__c, Second_Approver__c, POD__c, OwnerId FROM Opportunity WHERE Id IN : insertTestOpps];
		system.assertEquals(200, insertedOpps.size(), 'We expect there to be 200 inserted opportunities');
		for(Opportunity insertedOpp : insertedOpps)
		{
			system.assertEquals(user1.First_Approver__c, insertedOpp.First_Approver__c, 'We expect the First Approver to be populated on the opp correctly');
			system.assertEquals(user1.Second_Approver__c, insertedOpp.Second_Approver__c, 'We expect the Second Approver to be populated on the opp correctly');
			system.assertEquals(user1.User_Pod_Number__c, insertedOpp.POD__c, 'We expect the Pod to be populated on the opp correctly');
			system.assertEquals(user1.Id, insertedOpp.Owner_Copy__c, 'We expect that the Owner Copy on the opp is equal to the Owner Id');
		}
		
	}
	
	private static testMethod void testBulkOpportunityUpdate()
	{
		setUpMethod();
		List<Opportunity> updateTestOpps = new List<Opportunity>();
		List<Opportunity> ownerChangeTestOpps = new List<Opportunity>();
		List<Opportunity> testOpps = TestUtils.createOpportunities(100, TestUtils.RESEARCH_RECORD_TYPE.Id , account.Id, true);
		for(Opportunity testOpp : testOpps)
		{
			testOpp.OwnerId = user1.Id;
			updateTestOpps.add(testOpp);
		}
		update updateTestOpps;
		for(Opportunity testOpp : updateTestOpps)
		{
			testOpp.OwnerId = user2.Id;
			ownerChangeTestOpps.add(testOpp);
		}		
		test.startTest();
		update ownerChangeTestOpps;
		test.stopTest();
		
		List<Opportunity> updatedOpps = [SELECT Id, Owner_Copy__c, First_Approver__c, Second_Approver__c, POD__c, OwnerId FROM Opportunity WHERE Id IN : ownerChangeTestOpps];
		system.assertEquals(100, ownerChangeTestOpps.size(), 'We expect there to be 100 updated opportunities');
		for(Opportunity updatedOpp : updatedOpps)
		{
			system.assertEquals(user2.First_Approver__c, updatedOpp.First_Approver__c, 'We expect the First Approver to be populated on the opp correctly');
			system.assertEquals(user2.Second_Approver__c, updatedOpp.Second_Approver__c, 'We expect the Second Approver to be populated on the opp correctly');
			system.assertEquals(user2.User_Pod_Number__c, updatedOpp.POD__c, 'We expect the Pod to be populated on the opp correctly');
			system.assertEquals(user2.Id, updatedOpp.Owner_Copy__c, 'We expect that the Owner Copy on the opp is equal to the Owner Id');
		}
	}
	private static testMethod void testSingleOppUpdate()
	{
		setUpMethod();
		Opportunity testOpp = TestUtils.createOpportunity(TestUtils.RESEARCH_RECORD_TYPE.id, account.Id, true);
		testOpp.OwnerId = user1.Id;
		update testOpp;
		
		test.startTest();
		testOpp.OwnerId = user2.Id;
		update testOpp;
		test.stopTest();
		
		Opportunity updatedOpp = [SELECT Id, Owner_Copy__c, First_Approver__c, Second_Approver__c, POD__c, OwnerId FROM Opportunity WHERE Id = : testOpp.Id];
		
		system.assertEquals(user2.First_Approver__c, updatedOpp.First_Approver__c, 'We expect the First Approver to be populated on the opp correctly');
		system.assertEquals(user2.Second_Approver__c, updatedOpp.Second_Approver__c, 'We expect the Second Approver to be populated on the opp correctly');
		system.assertEquals(user2.User_Pod_Number__c, updatedOpp.POD__c, 'We expect the Pod to be populated on the opp correctly');
		system.assertEquals(user2.Id, updatedOpp.Owner_Copy__c, 'We expect that the Owner Copy on the opp is equal to the Owner Id');
	}
	private static testMethod void testSingleOppInsert()
	{
		setUpMethod();
		Opportunity testOpp = TestUtils.createOpportunity(TestUtils.RESEARCH_RECORD_TYPE.id, account.Id, false);
		
		testOpp.OwnerId = user1.Id;
		
		test.startTest();
		insert testOpp;
		test.stopTest();
		
		Opportunity insertedOpp = [SELECT Id, Owner_Copy__c, First_Approver__c, Second_Approver__c, POD__c, OwnerId FROM Opportunity WHERE Id = : testOpp.Id];
		
		system.assertEquals(user1.First_Approver__c, insertedOpp.First_Approver__c, 'We expect the First Approver to be populated on the opp correctly');
		system.assertEquals(user1.Second_Approver__c, insertedOpp.Second_Approver__c, 'We expect the Second Approver to be populated on the opp correctly');
		system.assertEquals(user1.User_Pod_Number__c, insertedOpp.POD__c, 'We expect the Pod to be populated on the opp correctly');
		system.assertEquals(user1.Id, insertedOpp.Owner_Copy__c, 'We expect that the Owner Copy on the opp is equal to the Owner Id');
		
	}
}