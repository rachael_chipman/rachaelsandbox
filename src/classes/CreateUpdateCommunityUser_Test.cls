@isTest
private class CreateUpdateCommunityUser_Test
{
	static Account testAccount;
	static Contact testContact;
	static Contact testContactInsert;
	static FINAL String EIS_EMAIL = 'test@test.com';

	static void setupObjects()
	{
		testAccount = TestUtils.createAccount(TestUtils.MARKETER_RECORD_TYPE.Id, true);
		testContact = TestUtils.createContacts('Test', 'Contact', testAccount.Id, true);
		testContactInsert = TestUtils.createContacts('Test', 'Contact2', testAccount.Id, false);
	}

	static testMethod void testContactUpdate_Single()
	{
		setupObjects();
		//Make sure that there is not any community user inserted
		List<User> communityUsers = [SELECT Id, FederationIdentifier FROM User WHERE FederationIdentifier = :testContact.Internal_Tools_Id__c and ContactId = :testContact.Id];
		system.assert(0 == communityUsers.size());

		test.startTest();

		testContact.Internal_Tools_Id__c = '000000';
		testContact.EIS_EMail_Address__c = EIS_EMAIL;
		testContact.Internal_Tools_Status__c = true;
		testContact.Internal_Tools_User_Status__c = true;
		update testContact;

		test.stopTest();
		//Be sure we've got an email on the contact
		/*system.assert(testContact.EIS_EMail_Address__c == EIS_EMAIL);

		//Query for users inserted with the contact's EIS Id
		List<User> communityUsers2 = [SELECT Id, FederationIdentifier, Email, FirstName, LastName, ProfileId FROM User WHERE FederationIdentifier = :testContact.Internal_Tools_Id__c and ContactId = :testContact.Id];
		system.assert(1 == communityUsers2.size());
		//Check out all the fields to make sure that the code is populating them properly
		system.assert(testContact.FirstName == communityUsers2[0].FirstName);
		system.assert(testContact.LastName == communityUsers2[0].LastName);
		system.assert(testContact.Internal_Tools_Id__c == communityUsers2[0].FederationIdentifier);
		system.assert(CreateUpdateCommunityUser.profileId == communityUsers2[0].ProfileId);*/
	}

	static testMethod void testContactInsert_Single()
	{
		setupObjects();
		//Make sure that there is not any community user inserted
		List<User> communityUsers = [SELECT Id, FederationIdentifier FROM User WHERE FederationIdentifier = :testContact.Internal_Tools_Id__c and ContactId = :testContact.Id];
		system.assert(0 == communityUsers.size());
		//Make sure we only have one contact inserted
		List<Contact> contacts = [SELECT Id FROM Contact];
		system.assert(1 == contacts.size());
		//update the necessary fields on the insert contact and then go ahead and insert it
		testContactInsert.Internal_Tools_Id__c = '000000';
		testContactInsert.EIS_EMail_Address__c = EIS_EMAIL;
		testContactInsert.Internal_Tools_Status__c = true;
		testContactInsert.Internal_Tools_User_Status__c = true;

		test.startTest();
		insert testContactInsert;
		test.stopTest();

		/*//Assert that there are two contacts now, and that there is only one community user inserted
		List<Contact> contacts2 = [SELECT Id, EIS_EMail_Address__c, Internal_Tools_Id__c FROM Contact WHERE Id = :testContactInsert.Id];
		system.assert(1 == contacts2.size());
		system.assert(EIS_EMAIL == contacts2[0].EIS_EMail_Address__c);
		//Make sure that there is one community user inserted
		List<User> communityUsers2 = [SELECT Id, FederationIdentifier, Email, FirstName, LastName, ProfileId FROM User WHERE FederationIdentifier = :testContactInsert.Internal_Tools_Id__c and ContactId = :testContactInsert.Id];
		system.assertEquals(1, communityUsers2.size(), 'We should just have one community user');
		//Check out all the fields to make sure that the code is populating them properly
		system.assert(testContactInsert.FirstName == communityUsers2[0].FirstName);
		system.assert(testContactInsert.LastName == communityUsers2[0].LastName);
		system.assert(testContactInsert.Internal_Tools_Id__c == communityUsers2[0].FederationIdentifier);
		system.assert(CreateUpdateCommunityUser.profileId == communityUsers2[0].ProfileId);*/
	}

	static testMethod void testContactChangeEmail_Single()
	{
		setupObjects();
		//Make sure that there is not any community user inserted
		List<User> communityUsers = [SELECT Id, FederationIdentifier FROM User WHERE FederationIdentifier = :testContact.Internal_Tools_Id__c and ContactId = :testContact.Id];
		system.assert(0 == communityUsers.size());

		//Update the inserted contact so that the user is created
		testContact.Internal_Tools_Id__c = '000000';
		testContact.EIS_EMail_Address__c = EIS_EMAIL;
		testContact.Internal_Tools_Status__c = true;
		testContact.Internal_Tools_User_Status__c = true;
		update testContact;

		/*//Query for users inserted with the contact's EIS Id
		List<User> communityUsers2 = [SELECT Id, FederationIdentifier, Email, FirstName, LastName, ProfileId FROM User WHERE FederationIdentifier = :testContact.Internal_Tools_Id__c and ContactId = :testContact.Id];
		system.assert(1 == communityUsers2.size());
		Set<Id> userIds = new Set<Id>();
		userIds.add(communityUsers2[0].Id);
		//Check out all the fields to make sure that the code is populating them properly
		system.assert(testContact.FirstName == communityUsers2[0].FirstName);
		system.assert(testContact.LastName == communityUsers2[0].LastName);
		system.assert(testContact.Internal_Tools_Id__c == communityUsers2[0].FederationIdentifier);
		system.assert(testContact.EIS_EMail_Address__c == EIS_EMAIL);
		system.assert(testContact.EIS_EMail_Address__c == communityUsers2[0].Email);
		system.assert(CreateUpdateCommunityUser.profileId == communityUsers2[0].ProfileId);*/

		//The test is to verify that a change to the contact's EIS Email will appropriately update the user record
		Contact updatedCon = [SELECT Id, Internal_Tools_Id__c, EIS_EMail_Address__c FROM Contact WHERE Id = : testContact.Id LIMIT 1];
		updatedCon.EIS_EMail_Address__c = 'meowmeow@meow.com';
		

		
		system.runAs(CreateUpdateCommunityUser.oracleUser)
		{
			test.startTest();
			update updatedCon;
			test.stopTest();
		}	
		
		/*//Verify that the contact email is updated
		system.assert(updatedCon.EIS_EMail_Address__c != EIS_EMAIL);
		//Verify that the user was also updated
		List<User> communityUsers3 = [SELECT Id, FederationIdentifier, Email, FirstName, LastName, ProfileId FROM User WHERE FederationIdentifier = :updatedCon.Internal_Tools_Id__c and ContactId = :updatedCon.Id];
		system.assert(EIS_EMAIL != communityUsers3[0].Email);
		system.assert(updatedCon.EIS_EMail_Address__c == communityUsers3[0].Email);*/
	}

	static testMethod void testContactUpdate_Bulk()
	{
		setupObjects();
		Integer count = 1;
		//Insert a bunch of contacts
		List<Contact> testContacts = TestUtils.createContactsMultiple( 200, 'Kitty', 'Cat', testAccount.Id, true);
		//Assert they are there
		List<Contact> contacts = [SELECT Id FROM Contact];
		system.assert(201 == contacts.size());
		//Assert there are no community users
		Set<String> contactIds = pluck.strings('Id', contacts);
		List<User> communityUsers = [SELECT Id, FederationIdentifier FROM User WHERE FederationIdentifier != NULL and ContactId IN : contactIds];
		system.assert(0 == communityUsers.size());

		//Update all the contacts so that users are created
		for(Contact testCon : contacts)
		{
			testCon.EIS_EMail_Address__c = EIS_EMAIL;
			testCon.Internal_Tools_Id__c = '000000' + count++;
			testCon.Internal_Tools_Status__c = true;
			testCon.Internal_Tools_User_Status__c = true;
		}

		test.startTest();
		update contacts;
		test.stopTest();

		/*//Query for updated contacts
		List<Contact> updatedContacts = [SELECT Id, EIS_EMail_Address__c, Internal_Tools_Id__c FROM Contact];
		//Assert Contacts are updated
		for(Contact updatedCon : updatedContacts)
		{
			system.assert(updatedCon.EIS_EMail_Address__c == EIS_EMAIL);
			system.assert(updatedCon.Internal_Tools_Id__c != NULL);
		}
		//pluck out the EIS Ids from the updated contacts
		Set<String> eisIds = pluck.strings('Internal_Tools_Id__c', updatedContacts);
		//Assert that the users have all been inserted
		List<User> communityUsers2 = [SELECT Id, FederationIdentifier, Email, FirstName, LastName, ProfileId FROM User WHERE FederationIdentifier IN : eisIds];
		system.assert(201 == communityUsers2.size());*/
	}
}