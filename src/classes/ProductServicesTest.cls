@isTest
public with sharing class ProductServicesTest
{
	static OpportunityLineItem currentProduct;

	static void setup()
	{
		Id priceBookId = Test.getStandardPricebookId();
		Account testAccount = TestUtils.createAccount(TestUtils.PARTNER_RECORD_TYPE.Id, true);
		Opportunity prevOpp = TestUtils.createOpportunity(TestUtils.RESEARCH_RECORD_TYPE.Id, testAccount.Id, true);
		Opportunity currentOpp = TestUtils.createOpportunity(TestUtils.RESEARCH_RECORD_TYPE.Id, testAccount.Id, false );

		currentOpp.Previous_Opportunity_Lookup__c = prevOpp.Id;
		currentOpp.Type = 'Extension';

		insert currentOpp;

		Product2 testProduct = TestUtils.createProduct( true );

		PriceBookEntry pbe = new PriceBookEntry();
		pbe.UnitPrice = 1.0;
		pbe.Pricebook2Id  = priceBookId;
        pbe.Product2Id = testProduct.Id;
        pbe.isActive = true;
		insert pbe;

		OpportunityLineItem prevProduct = TestUtils.createOpportunityLineItem( prevOpp.Id, pbe.Id, false );
		prevProduct.UnitPrice = 150;
		prevProduct.Start_Date__c = Date.Today();
		prevProduct.End_Date__c = Date.Today().addMonths(3);

		currentProduct = TestUtils.createOpportunityLineItem( currentOpp.Id, pbe.Id, false );
		currentProduct.Start_Date__c = Date.Today();
		currentProduct.End_Date__c = Date.Today().addMonths(3);
		currentProduct.Previous_Value__c = 5;
		insert new List<OpportunityLineItem>{prevProduct, currentProduct};
	}

	/*static testMethod void testPreviousValue()
	{
		setup();

		currentProduct.End_Date__c = Date.today().addMonths(4);
		Test.startTest();
			update currentProduct;
		Test.stopTest();

		List<OpportunityLineItem> queriedOli = [ SELECT Id, Previous_Value__c FROM OpportunityLineItem WHERE Id = :currentProduct.Id ];

		System.assertEquals(200, queriedOli[0].Previous_Value__c, 'Previous value should be the previous PPM times the number of months in between');
	}*/

	static testMethod void testCertTier()
	{
		Id priceBookId = Test.getStandardPricebookId();
		Account testAccount = TestUtils.createAccount(TestUtils.PARTNER_RECORD_TYPE.Id, true);
		Opportunity currentOpp = TestUtils.createOpportunity(TestUtils.RESEARCH_RECORD_TYPE.Id, testAccount.Id, false );

		currentOpp.Type = 'New Business';
		currentOpp.Certification_Max_Volume__c = 250000;
		insert currentOpp;

		Product2 testProduct1 = TestUtils.createProduct( false );
		testProduct1.Name = 'Email Optimization: Certification - License - Annually';
		insert testProduct1;
		system.debug('What is the product name? ' + testProduct1.Name);

		PriceBookEntry pbe = new PriceBookEntry();
		pbe.UnitPrice = 1.0;
		pbe.Pricebook2Id  = priceBookId;
        pbe.Product2Id = testProduct1.Id;
        pbe.isActive = true;
		insert pbe;

		PricebookEntry pbeQuery = [SELECT Id, Name FROM PricebookEntry WHERE Id = : pbe.Id];

		system.debug('What is the pbe name? ' + pbeQuery.Name);

		OpportunityLineItem newProduct = TestUtils.createOpportunityLineItem( currentOpp.Id, pbe.Id, false );
		newProduct.UnitPrice = 150;
		newProduct.Quantity = 1;
		newProduct.Start_Date__c = Date.Today();
		newProduct.End_Date__c = Date.Today().addMonths(3);
		
		//OpportunityLineItem requeryOli = [SELECT Id, PricebookEntry.Name FROM OpportunityLineItem WHERE OpportunityId = :currentOpp.Id];
		//system.debug('What is the pbe name? ' + requeryOli.PricebookEntry.Name +' product2 ' + requeryOli.Product2.Name);

		test.startTest();
		insert newProduct;
		test.stopTest();

		//Query opp for assertion
		Opportunity testOpp = [SELECT Id, Certification_Max_Volume__c, Certification_Tier_Sold__c FROM Opportunity WHERE Id = : currentOpp.Id];
		system.assertEquals('Tier ' + CertificationOverageGeneration.roundTheTotalVolumeToNextTier(testOpp.Certification_Max_Volume__c), testOpp.Certification_Tier_Sold__c, 'Opp tier should be populated');
	}
}