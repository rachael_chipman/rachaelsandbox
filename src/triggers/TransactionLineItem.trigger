trigger TransactionLineItem on c2g__codaTransactionLineItem__c ( after insert ) 
{
	List<c2g__codaTransactionLineItem__c> filteredTransactionLineItems = Select.Field.isEqual( c2g__codaTransactionLineItem__c.Transaction_Type__c, 'Invoice' ).filter( trigger.new );
	if( filteredTransactionLineItems.isEmpty() )
	{
		return;
	}
	TransactionServices.updateTransactionLineItems( TransactionServices.populateOpportunityOnLineItems( filteredTransactionLineItems ), trigger.new );
}