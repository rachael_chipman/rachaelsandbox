trigger GenRefFSROwnerChange on Lead (before update) {

/*DESCRIPTION: This trigger will reassign the Referring CRM as the Owner and then submit it for their approval, then once the CRM submits
the Lead for RD/FSR approval, the FSR who approves the Lead will become the Lead owner*/

  for(Lead l : trigger.new){
  
  if(l.Campaign_Name__c != null){
  
  //Set string variable for Most Recent Campaign
  String str = l.Campaign_Name__c;
  
  
        //Only pull Leads that have a Gen Ref Most Recent Campaign and a Referring CRM  
        if( SObjectTriggerServices.isValidCampaign( str ) && l.Referring_CRM2__c != null){

        
           //If the CRM has approved the Lead and so has the FSR, then make the FSR the owner 
           if(l.Status == 'Accepted' && l.Gen_Ref_Lead__c == true){
    
                Lead prev = trigger.oldMap.get(l.Id);
    
                    if(l.Status != prev.Status){
        
                    l.OwnerId = UserInfo.getUserID();
       
       } 
    }
}
}
}
}