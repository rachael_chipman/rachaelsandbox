trigger GenRefPartnerPush on Opportunity (before update) {
    
    
    for(opportunity o : trigger.new) {
        Opportunity placeholderopp = new Opportunity(id = o.id);
        
        if(o.SSC_Promo_Code__c != null && (o.StageName == 'Demonstrating Capabilities' || o.StageName =='Obtaining Commitment')){
        
        Promo_Code__c p = [SELECT Id, CRM_Id__c, Account__c FROM Promo_Code__c WHERE Name = :o.SSC_Promo_Code__c];
             
                    if(p.account__c != null) o.PartnerLookup__c = p.Account__c;
                    if(p.CRM_ID__c != null) o.CRM_User__c = p.CRM_Id__c;

                }
            }
}