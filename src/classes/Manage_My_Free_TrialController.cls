//This is the controller class for a VF page
//that is triggered by a custom button that will
//allow the end user to manage his/her free trial opps
//Objective: Allow them to extend the free trial, or terminate it


public class Manage_My_Free_TrialController{

    public Opportunity o {get;set;}
    public String freetrial = null;
    
    public Manage_My_Free_TrialController(ApexPages.StandardController std){
    
    o = [SELECT Id, Type, StageName, RecordTypeId, Approval_Type__c, Submit_from_Trigger__c FROM Opportunity 
    WHERE Id = :ApexPages.currentPage().getParameters().get('id')];
    
    }
    
     public List<SelectOption> getItems() {
        List<SelectOption> options = new List<SelectOption>(); 
        options.add(new SelectOption('Extend','I would like to extend this Trial.')); 
        options.add(new SelectOption('Terminate','I would like to terminate this Trial.')); 
        return options; 
    }
   
   public String getfreetrial() {
        return freetrial;
    }
                    
    public void setfreetrial(String freetrial) { this.freetrial = freetrial; } 
    
    public pageReference Confirm(){
    if(freetrial == 'Extend'){
    o.Type = 'Trial - Extension';
    o.StageName = 'Verbal Commitment';
    o.Approval_Type__c = 'Finance Approval';
    o.Submit_from_Trigger__c = 'Yes';
    update o;
    pageReference p = new pageReference('/apex/tabView?id='+o.id);
    return p;
    }
    if(freetrial == 'Terminate'){
    o.Type = 'Trial';
    o.RecordTypeId = [SELECT Id FROM RecordType WHERE DeveloperName = 'Termination' and SObjectType = 'Opportunity'].Id ;
    o.StageName = 'Approval Required';
    o.Approval_Type__c = 'Finance Approval';
    update o;
    list<OpportunityLineItem> olis = [SELECT Id, Product_Status__c, Start_Date__c, End_Date__c FROM OpportunityLineItem WHERE OpportunityId = :o.Id];
        for(OpportunityLineItem termoli : olis){
        termoli.Start_Date__c = system.today();
        termoli.End_Date__c = system.today();
        termoli.Product_Status__c = 'Terminated';
        update termoli;
        }
    pageReference p = new pageReference('/apex/tabView?id='+o.id);
    return p;
    }
    else{
    return null;
    }
    }
}