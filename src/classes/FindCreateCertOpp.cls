public with sharing class FindCreateCertOpp {
	
	public static Campaign CERT_CAMPAIGN 
  {
    get
    {
      if( CERT_CAMPAIGN == NULL )
      {
        CERT_CAMPAIGN = [SELECT Id FROM Campaign WHERE Name = 'WEB: SSC Application Form' and isActive = true AND Type = 'SS.ORG'];
      }
      return CERT_CAMPAIGN;
    }
    private set;
  }
  
  public static RecordType CERT_APP_RECORD_TYPE
  {
    get 
    {
      if( CERT_APP_RECORD_TYPE == NULL )
      {
        CERT_APP_RECORD_TYPE = [SELECT Id FROM RecordType WHERE DeveloperName = 'Cert_App' AND SObjectType = 'Certification_Application__c' ];
      }
      return CERT_APP_RECORD_TYPE;
    }
    private set;
  }
  
  public static RecordType FINANCE_APPROVED_RECORD_TYPE
  {
    get
    {
      if( FINANCE_APPROVED_RECORD_TYPE == NULL )
      {
        FINANCE_APPROVED_RECORD_TYPE = [SELECT Id FROM RecordType WHERE DeveloperName = 'Finance_Approved' AND SObjectType = 'Opportunity' ];
      }
      return FINANCE_APPROVED_RECORD_TYPE;
    }
    private set;
  }
  
  public static RecordType PURCHASE_RECORD_TYPE
    {
      get
      {
        if(PURCHASE_RECORD_TYPE == NULL)
        {
          PURCHASE_RECORD_TYPE = [SELECT Id FROM RecordType WHERE DeveloperName = 'Purchase' and SObjectType = 'Opportunity'];
        }
        return PURCHASE_RECORD_TYPE;
      }
      private set;
    }
  
  public static User SF_AUTO
  {
    get
    {
      if( SF_AUTO == NULL )
      {
        SF_AUTO = [SELECT Id FROM User WHERE Name = 'Salesforce Automation'];
      }
      return SF_AUTO;
    }
    private set;
  }
  
  public static list<Certification_Application__c> filterCertAppsOnUpdate ( list<Certification_Application__c> newApps, Map<Id, Certification_Application__c> oldApps )
  {
    list<Certification_Application__c> filteredApps = new list<Certification_Application__c>();
    
    for( Certification_Application__c app : newApps )
    {
      Certification_Application__c oldApp;
      
      if( oldApps != NULL )
      {
        oldApp = oldApps.get(app.Id);
      }
      if( app.Opportunity_Created__c != 'Yes' && app.RecordTypeId == CERT_APP_RECORD_TYPE.Id )
      {
        filteredApps.add( app );
      }
    }
    system.debug( 'What is in filtered apps ' + filteredApps );
    return filteredApps;
  }
  
  private static Map<Id, Opportunity> getExistingOpps ( list<Certification_Application__c> filteredApps )
  {
    Map<Id, Opportunity> certAppIdToExistingOpp = new Map<Id, Opportunity>();
    Set<Id> accountIds = pluck.ids( 'Account__c', filteredApps );
    list<Opportunity> existingOpportunities = [SELECT Id, StageName, Has_Cert_Products_Qty__c, AccountId FROM Opportunity WHERE ((isClosed = false  AND StageName != 'Approval Required') OR (StageName = 'Pending Certification Qualification' AND isClosed = true)) and Cert_Only_Opportunity__c = 'Yes' and Type != 'Fee' and AccountId IN :accountIds LIMIT 1];
    for( Certification_Application__c app : filteredApps )
    {
      for( Opportunity existingOpp : existingOpportunities )
      {
        if( app.Account__c == existingOpp.AccountId )
        {
          certAppIdToExistingOpp.put(  app.Id, existingOpp );
        }
      }
    }
    return certAppIdToExistingOpp;
  }
  
  public static Opportunity createAppFeeOpp ( Certification_Application__c certApp )
  {
    Opportunity appFeeOpp = new Opportunity();
    appFeeOpp.Name = 'Cert_App_Fee_Opportunity';
    appFeeOpp.Description = 'Fee Amount: ' + certApp.Certification_App_Fee__c + ', Tier: ' + certApp.Certification_Volume__c + ', Max Vol: ' + certApp.Certification_Max_Volume__c;
    appFeeOpp.AccountId = certApp.Account__c;
    //appFeeOpp.RecordTypeId = FINANCE_APPROVED_RECORD_TYPE.Id;
    appFeeOpp.RecordTypeId = TestUtils.PURCHASE_RECORD_TYPE.Id;
    //appFeeOpp.StageName = 'Finance Approved';
    appFeeOpp.StageName = 'Signature';
    appFeeOpp.CloseDate = system.today();
    appFeeOpp.Type = 'Fee';
    appFeeOpp.OwnerId = SF_AUTO.Id;
    appFeeOpp.CampaignId = CERT_CAMPAIGN.Id;
    appFeeOpp.Product__c = 'Certification';
    appFeeOpp.Certification_Max_Volume__c = certApp.Certification_Max_Volume__c;
    appFeeOpp.Certification_Tier_Sold__c = certApp.Certification_Volume__c;
    appFeeOpp.Product_Certification_App_Fee__c = true;
    appFeeOpp.Certification_Type__c = 'Application Fee';
    appFeeOpp.Billing_Type__c = 'Annual';
    appFeeOpp.Promo_Type_from_Cert_App__c = certApp.Promo_Type_Picklist__c;
    appFeeOpp.Bypass_Financial_Force__c = true;
    appFeeOpp.Submit_From_Trigger__c = 'Yes';
    appFeeOpp.Approval_Type__c = 'Finance Approval';
    return appFeeOpp;
  }
  
  public static Opportunity createLicenseFeeOpp( Certification_Application__c certApp )
  {
    Opportunity licenseFeeOpp = new Opportunity();
    licenseFeeOpp.AccountId = certApp.Account__c;
    //licenseFeeOpp.StageName = 'Obtaining Commitment';
    //licenseFeeOpp.RecordTypeId = OBTAINING_COMMITMENT_RECORD_TYPE.Id;
        licenseFeeOpp.StageName = 'Signature';
        licenseFeeOpp.Did_not_Fill_Pre_Pipeline_Exit_Criteria__c = true;
        licenseFeeOpp.Did_not_Fill_Defining_Need_Exit_Criteria__c = true;
        licenseFeeOpp.Did_not_Fill_Est_Value_Exit_Criteria__c = true;
        licenseFeeOpp.Did_not_Fill_Verbal_Commit_Exit_Criteria__c = true;
        licenseFeeOpp.Did_not_Fill_Nego_Terms_Exit_Criteria__c = true;
    licenseFeeOpp.RecordTypeId = TestUtils.PURCHASE_RECORD_TYPE.Id;
    licenseFeeOpp.Name = 'Cert_License_Opportunity';
    licenseFeeOpp.Description = ' Promo Type: ' + certApp.Promo_Type_Picklist__c + ', Amount: ' + certApp.Cert_Tier_to_Unit_Price__c + ',' + '\n' + 'Tier: ' + certApp.Certification_Volume__c + ', Max Vol: ' + certApp.Certification_Max_Volume__c; 
    licenseFeeOpp.CloseDate = system.today();
    licenseFeeOpp.Type = 'New Business';
    licenseFeeOpp.CurrencyIsoCode = certApp.CurrencyIsoCode;
    licenseFeeOpp.CampaignId = CERT_CAMPAIGN.Id;
    if(certApp.Promo_Type_Picklist__c == 'Walk-Up')
    {
      licenseFeeOpp.OwnerId = SF_AUTO.Id;
    }
    else if(certApp.Promo_Type_Picklist__c == 'Referral')
    {
      licenseFeeOpp.OwnerId = certApp.Partner_CRM__c;
    }
    licenseFeeOpp.Product__c = 'Certification';
    licenseFeeOpp.Certification_Max_Volume__c = certApp.Certification_Max_Volume__c;
    licenseFeeOpp.Certification_Tier_Sold__c = certApp.Certification_Volume__c;
    licenseFeeOpp.SSC_Qual_Team_Member__c = certApp.Certification_Analyst__c;
    licenseFeeOpp.Certification_Type__c = certApp.Certification_Type__c;
    licenseFeeOpp.Approval_Type__c = 'Finance Approval';
    licenseFeeOpp.Promo_Type_from_Cert_App__c = certApp.Promo_Type_Picklist__c;
    if( licenseFeeOpp.Promo_Type_from_Cert_App__c == 'Referral' )
    {
      licenseFeeOpp.Product_SSC_Referral_Child__c = true;
      licenseFeeOpp.CRM_User__c = certApp.Partner_CRM__c;
      licenseFeeOpp.PartnerLookup__c = certApp.Partner_Account__c;
      
    }
    if( licenseFeeOpp.Promo_Type_from_Cert_App__c == 'Walk-Up' )
    {
      licenseFeeOpp.Product_Certification__c = true;
    }
    licenseFeeOpp.Certification_Analyst__c = certApp.Certification_Analyst__c;
    return licenseFeeOpp;
  }
  
  public static void processApplications ( list<Certification_Application__c> filteredApps, list<Certification_Application__c> newApps )
  {
    list<Opportunity> existingOppsToUpdate = new list<Opportunity>();
    list<AppFeeOpportunity> feeOppsToInsert = new list<AppFeeOpportunity>();
    list<LicenseFeeOpportunity> licenseOppsToInsert = new list<LicenseFeeOpportunity>();
    Map<Id, Opportunity> certAppIdToExistingOpportunity = getExistingOpps(  filteredApps );
    
    if( certAppIdToExistingOpportunity.isEmpty() )
    {
      for( Certification_Application__c app : filteredApps )
      {
        if(app.Promo_Type_Picklist__c == 'Walk-Up' && app.Application_Fee_Opportunity__c == NULL )
        {
          Opportunity feeOpp = createAppFeeOpp( app );
          OpportunityLineItem feeItem = new OpportunityLineItem();
          feeItem.UnitPrice = app.Certification_App_Fee__c;
          feeItem.Start_Date__c = system.today();
          feeItem.End_Date__c = system.today();
          feeItem.Product_Status__c = 'Current';
          feeItem.Description__c = 'Return Path Certification';
          feeItem.Active__c = true;
          feeItem.Recurring__c = false;
          
          feeOppsToInsert.add( new AppFeeOpportunity( feeOpp, feeItem, 'Certification - Application Fee', app.CurrencyIsoCode ) );
          
        }
        
        if( ( app.Promo_Type_Picklist__c == 'Walk-Up' || app.Promo_Type_Picklist__c == 'Referral' ) && app.Application_Result__c == 'In Progress' )
        {
          Opportunity licenseOpp = createLicenseFeeOpp( app ) ;
          OpportunityLineItem licenseItem = new OpportunityLineItem();
          licenseItem.UnitPrice = app.Cert_Tier_to_Unit_Price__c;
          licenseItem.Product_Status__c = 'Current';
          licenseItem.Description__c = 'Return Path Certification';
          licenseItem.Indirect__c = true;
          licenseItem.Active__c = true;
          licenseItem.Recurring__c = true;
          
          if(app.Promo_Type_Picklist__c == 'Referral')
          {
            licenseOppsToInsert.add( new LicenseFeeOpportunity( licenseOpp, licenseItem, 'Certification - Child FEE', app.CurrencyIsoCode ) );
          }
          else
          {
            licenseOppsToInsert.add( new LicenseFeeOpportunity( licenseOpp, licenseItem, 'Certification - License', app.CurrencyIsoCode ) );
          }
          
        }
      } 
      if( !feeOppsToInsert.isEmpty() )
      {
        saveAppFeeOpportunities( feeOppsToInsert, newApps );
      }
      if( !licenseOppsToInsert.isEmpty() )
      {
        saveLicenseOpportunities(  licenseOppsToInsert, newApps ); 
      }
    }
    else if( !certAppIdToExistingOpportunity.isEmpty() )
    {
      for( Certification_Application__c app : filteredApps)
      {
        Opportunity existingOppToUpdate = certAppIdToExistingOpportunity.get(app.Id);
        if( existingOppToUpdate.StageName != 'Pending Certification Qualification' )
        {
          existingOppToUpdate.StageName = 'Signature';
          existingOppToUpdate.Record_Locked__c = false;
          if(existingOppToUpdate.Problem_Buyer_articulated_we_can_solve__c == NULL ||
            existingOppToUpdate.Preliminary_timeline_for_decision__c == NULL ||
            existingOppToUpdate.Confirm_access_to_funding__c == NULL)
          {
            existingOppToUpdate.Did_not_Fill_Pre_Pipeline_Exit_Criteria__c = true;
          }
          if(existingOppToUpdate.Describe_the_buying_team__c == NULL ||
            existingOppToUpdate.Describe_the_evaluation_criteria__c == NULL)
          {
            existingOppToUpdate.Did_not_Fill_Defining_Need_Exit_Criteria__c = true;
          }
          if(existingOppToUpdate.Describe_mutally_shared_goal__c == NULL || 
            existingOppToUpdate.Describe_their_desired_solution__c == NULL)
          {
            existingOppToUpdate.Did_not_Fill_Est_Value_Exit_Criteria__c = true;
          }
          if(existingOppToUpdate.Describe_Buyer_Partner_steps_to_close__c == NULL || 
            existingOppToUpdate.Describe_Buyer_Partner_timing_to_close__c == NULL ||
            existingOppToUpdate.Confirm_the_Buyer_Approval_Process__c == NULL ||
            existingOppToUpdate.Document_Verbal_Agreement__c == NULL)
          {
            existingOppToUpdate.Did_not_Fill_Verbal_Commit_Exit_Criteria__c = true;
          }
          if(existingOppToUpdate.Estimated_Start_Date__c == NULL)
          {
            existingOppToUpdate.Did_not_Fill_Nego_Terms_Exit_Criteria__c = true;
          }

        }
        existingOppToUpdate.RecordTypeId = TestUtils.PURCHASE_RECORD_TYPE.Id;
        existingOppToUpdate.Approval_Type__c = 'Finance Approval';
        existingOppToUpdate.CampaignId = CERT_CAMPAIGN.Id;
        existingOppToUpdate.Promo_Type_from_Cert_App__c = app.Promo_Type_Picklist__c;
        existingOppToUpdate.Certification_Analyst__c = app.Certification_Analyst__c;
        existingOppsToUpdate.add(  existingOppToUpdate );
        
        app.License_Fee_Opportunity__c = existingOppToUpdate.Id;
        app.Opportunity_Created__c = 'Yes';
      }
      
      try
      {
        update existingOppsToUpdate;
      }
      catch (Exception e)
      {
        for( Integer i = 0; i < e.getNumDml(); i++ )
        {
          system.debug( e.getDmlMessage( i ) );
          for(Certification_Application__c triggerEvent : newApps)
          {
            triggerEvent.addError( e.getDmlMessage( i ) );
          }
        }
      }
    }
    
    
  }
  
  public class AppFeeOpportunity
  {
    public Opportunity opty{get; private set;}
    public OpportunityLineItem oli{get; private set;}
    public String productName{get; private set;}
    public String isoCode{get; private set;}
    
    public AppFeeOpportunity ( Opportunity opty, OpportunityLineItem oli, String productName, String isoCode )
    {
      this.opty = opty;
      this.oli = oli;
      this.productName = productName;
      this.isoCode = isoCode;
    }
  }
  
  public static void saveAppFeeOpportunities( List<AppFeeOpportunity> feeOppsToInsert, List<Certification_Application__c> newApps)
  {
    List<Opportunity> feeOpportunities = new List<Opportunity>();
    List<String> productNames = new List<String>();
    List<String> currencyCodes = new List<String>();
    for( AppFeeOpportunity feeOpty : feeOppsToInsert )
    {
      feeOpportunities.add( feeOpty.opty );
      productNames.add( feeOpty.productName );
      currencyCodes.add( feeOpty.isoCode );
    }
    Savepoint sp = Database.setSavepoint();
    try
    {
      insert feeOpportunities;
    }
    catch (DMLException e)
    {
      for(Integer i = 0; i < e.getNumDml(); i++)
      {
        System.debug(e.getDmlMessage(i));
        for(Certification_Application__c triggerEvent : newApps)
        {
          triggerEvent.addError( e.getDmlMessage ( i ) );
        } 
      }
    }
    
    Map<Id, Opportunity> accountIdToOpportunity = new Map<Id, Opportunity>();
    for( Opportunity insertedOpp : feeOpportunities )
    {
      accountIdToOpportunity.put(insertedOpp.AccountId, insertedOpp);
    }
    for( Certification_Application__c app : newApps )
    {
      if( accountIdToOpportunity.containsKey(app.Account__c) )
      {
        app.Application_Fee_Opportunity__c = accountIdToOpportunity.get(app.Account__c).Id;
      }
    }
    
    List<PricebookEntry> pbes = [SELECT Id, Name, CurrencyISOCode FROM PricebookEntry WHERE Name IN :ProductNames and CurrencyISOCode IN :CurrencyCodes and Pricebook2.Name = 'Return Path Pricebook'];
    Map<String, PricebookEntry> currencyProductNamePBEs = new Map<String, PricebookEntry>();
    for(PricebookEntry pbe : pbes )
    {
      currencyProductNamePBEs.put(pbe.Name + pbe.CurrencyISOCode, pbe );
    }
    
    List<OpportunityLineItem> olisToInsert = new List<OpportunityLineItem>();
    for( AppFeeOpportunity feeOpty : feeOppsToInsert )
    {
      OpportunityLineItem oli = feeOpty.oli;

      oli.OpportunityId = feeOpty.opty.Id;
      String key =  feeOpty.productName + feeOpty.isoCode;
      if( currencyProductNamePBEs.containsKey( key ) )
      {
        oli.PricebookEntryId = currencyProductNamePBEs.get( key ).Id;
        olisToInsert.add( oli );
      }
    }
    /*try
    {
      insert olisToInsert;
    }
    catch (Exception e)
    {
      for( Integer i = 0; i < e.getNumDml(); i++ )
      {
        system.debug( e.getDmlMessage( i ) );
        for(Certification_Application__c triggerEvent : newApps)
        {
          triggerEvent.addError( e.getDmlMessage( i ) );
        }
      }
      Database.rollback( sp );
    }*/
  }
  
  public class LicenseFeeOpportunity
  {
    public Opportunity opty{get; private set;}
    public OpportunityLineItem oli{get; private set;}
    public String productName{get; private set;}
    public String isoCode{get; private set;}
    
    public LicenseFeeOpportunity ( Opportunity opty, OpportunityLineItem oli, String productName, String isoCode )
    {
      this.opty = opty;
      this.oli = oli;
      this.productName = productName;
      this.isoCode = isoCode;
    }
  }
  
  public static void saveLicenseOpportunities( List<LicenseFeeOpportunity> licenseOppsToInsert, List<Certification_Application__c> newApps)
  {
    List<Opportunity> licenseOpportunities = new List<Opportunity>();
    List<String> productNames = new List<String>();
    List<String> currencyCodes = new List<String>();
    for( LicenseFeeOpportunity licenseOpty : licenseOppsToInsert )
    {
      licenseOpportunities.add( licenseOpty.opty );
      productNames.add( licenseOpty.productName );
      currencyCodes.add( licenseOpty.isoCode );
    }
    Savepoint sp = Database.setSavepoint();
    try
    {
      insert licenseOpportunities;
    }
    catch (DMLException e)
    {
      for(Integer i = 0; i < e.getNumDml(); i++)
      {
        System.debug(e.getDmlMessage(i));
        for(Certification_Application__c triggerEvent : newApps)
        {
          triggerEvent.addError( e.getDmlMessage ( i ) );
        } 
      }
    }
    
    Map<Id, Opportunity> accountIdToOpportunity = new Map<Id, Opportunity>();
    for( Opportunity insertedOpp : licenseOpportunities )
    {
      accountIdToOpportunity.put(insertedOpp.AccountId, insertedOpp);
    }
    for( Certification_Application__c app : newApps )
    {
      if( accountIdToOpportunity.containsKey(app.Account__c) )
      {
        app.License_Fee_Opportunity__c = accountIdToOpportunity.get(app.Account__c).Id;
        app.Opportunity_Created__c = 'Yes';
      }
    }
    
    List<PricebookEntry> pbes = [SELECT Id, Name, CurrencyISOCode FROM PricebookEntry WHERE Name IN :ProductNames and CurrencyISOCode IN :CurrencyCodes and Pricebook2.Name = 'Return Path Pricebook'];
    Map<String, PricebookEntry> currencyProductNamePBEs = new Map<String, PricebookEntry>();
    for(PricebookEntry pbe : pbes )
    {
      currencyProductNamePBEs.put(pbe.Name + pbe.CurrencyISOCode, pbe );
    }
    
    List<OpportunityLineItem> olisToInsert = new List<OpportunityLineItem>();
    for( LicenseFeeOpportunity licenseOpty : licenseOppsToInsert )
    {
      OpportunityLineItem oli = licenseOpty.oli;

      oli.OpportunityId = licenseOpty.opty.Id;
      String key =  licenseOpty.productName + licenseOpty.isoCode;
      if( currencyProductNamePBEs.containsKey( key ) )
      {
        oli.PricebookEntryId = currencyProductNamePBEs.get( key ).Id;
        olisToInsert.add( oli );
      }
    }
    /*try
    {
      insert olisToInsert;
    }
    catch (Exception e)
    {
      for( Integer i = 0; i < e.getNumDml(); i++ )
      {
        system.debug( e.getDmlMessage( i ) );
        for(Certification_Application__c triggerEvent : newApps)
        {
          triggerEvent.addError( e.getDmlMessage( i ) );
        }
      }
      Database.rollback( sp );
    }*/
  }
  
  public static void processTrigger ( list<Certification_Application__c> newApps, Map<Id, Certification_Application__c> oldApps )  
  {
    List<Certification_Application__c> filteredApplications = filterCertAppsOnUpdate ( newApps, oldApps );
    if( !filteredApplications.isEmpty() )
    {
      processApplications(filteredApplications, newApps);
    }
  }

}