@isTest
public with sharing class TestIncomeScheduleLineItemService {

    public static Account testAccount;
    public static product2 testProduct;
    public static c2g__codaGeneralLedgerAccount__c testGeneralLedgerAccount;
    public static integer NUM_OF_OPPORTUNITIES = 10;
    public static List<c2g__codaInvoice__c> testSalesInvoices = new List<c2g__codaInvoice__c>();
    public static integer NUM_OF_LINE_ITEMS = 10;
    public static List<c2g__codaScheduleLineItem__c> testScheduleLineItems = new List<c2g__codaScheduleLineItem__c>();
	public static PriceBookEntry testPBE;

    static void datasetup(){
        testGeneralLedgerAccount = TestUtils.createGeneralLedgerAccount(true);
        testAccount = TestUtils.createAccount_FinancialForce( TestUtils.PARTNER_RECORD_TYPE.Id, testGeneralLedgerAccount.Id, true );
        testProduct = TestUtils.createProduct_FinancialForce( testGeneralLedgerAccount.Id, true );
        testPBE = TestUtils.createPricebookEntry( testProduct.Id, TestUtils.STANDARD_PRICE_BOOK.Id, true );
    }
    
    @isTest(seeAllData=true)    
    static void testJournalLineItemInserts() 
    {
        datasetup();
        
        //List<Opportunity> testOpportunities = TestUtils.createOpportunities( NUM_OF_OPPORTUNITIES, TestUtils.AWARENESS_RECORD_TYPE.Id, testAccount.Id, true );
 		List<Opportunity> testOpportunities = TestUtils.createOpportunities( NUM_OF_OPPORTUNITIES, TestUtils.RESEARCH_RECORD_TYPE.Id, testAccount.Id, true );
		List<c2g__codaInvoiceLineItem__c> testSalesInvoiceLineItems = new List<c2g__codaInvoiceLineItem__c>();
		
        for( Opportunity anOpportunity : testOpportunities )
        {
	        testSalesInvoices.addAll(testUtils.createSalesInvoices(1, testAccount.id, new Map<String,Object>{ 'c2g__Opportunity__c' => anOpportunity.Id }, false));
        }
        insert testSalesInvoices;
 		for( c2g__codaInvoice__c testSalesInvoice: testSalesInvoices )
		{
			testSalesInvoiceLineItems.add( TestUtils.createSalesInvoiceLineItem( testSalesInvoice.Id, testProduct.Id, false ) );
		}  
		insert testSalesInvoiceLineItems;     
        for (c2g__codaInvoice__c testSalesInvoice: testSalesInvoices){
            testScheduleLineItems.addAll( TestUtils.createScheduleLineItems(1, testSalesInvoice.Id,  false) );		
        }
        Integer i= 0;
        
		for (c2g__codaScheduleLineItem__c testScheduleLineItem : testScheduleLineItems){
			testScheduleLineItem.c2g__Amount__c = 10;
			testScheduleLineItem.c2g__UnitofWork__c = 1; 
			testScheduleLineItem.c2g__SalesInvoiceLineItem__c = testSalesInvoiceLineItems[i].id; 
			testScheduleLineItem.c2g__LineNumber__c  = i+ 1;
			
			i++;
		}
		
        test.startTest();
            insert testScheduleLineItems;
        test.stopTest();

		List<c2g__codaInvoice__c> insertedInvoices = [ SELECT id, c2g__Opportunity__c FROM c2g__codaInvoice__c WHERE Id IN :testSalesInvoices ];
        List<c2g__codaScheduleLineItem__c> insertedScheduleLineItems = [ SELECT id, Opportunity__c,c2g__Invoice__c FROM c2g__codaScheduleLineItem__c WHERE id IN :testScheduleLineItems ]; 
        Map<ID,List<c2g__codaScheduleLineItem__c>> invoiceSLIMap = new Map<ID,List<c2g__codaScheduleLineItem__c>>();
        for (c2g__codaScheduleLineItem__c sli :insertedScheduleLineItems){
            if(!invoiceSLIMap.containsKey(sli.c2g__Invoice__c)){
                invoiceSLIMap.put(sli.c2g__Invoice__c,new List<c2g__codaScheduleLineItem__c>());
            }
			invoiceSLIMap.get(sli.c2g__Invoice__c).add(sli);
        }

        System.assertEquals(NUM_OF_OPPORTUNITIES,insertedInvoices.size(),'All invoice have been retrieved');
        System.assertEquals(NUM_OF_LINE_ITEMS,insertedScheduleLineItems.size(),'All schedule line items have been retrieved');
        
        for( c2g__codaInvoice__c insertedInvoice: insertedInvoices )
        {
            for (c2g__codaScheduleLineItem__c sli : invoiceSLIMap.get(insertedInvoice.id)){
            	System.assertNotEquals( null,sli.Opportunity__c, 'The line item  opportunity is populated' );
            	System.assertEquals( insertedInvoice.c2g__Opportunity__c, sli.Opportunity__c, 'The inovice line item is populated with opportunity lookup from the inovice' );
            }
        }

    }
    
    @isTest(seeAllData=true)    
    static void testJournalLineItemInserts_noOpp_negativeCase() 
    {
        datasetup();
        
        //List<Opportunity> testOpportunities = TestUtils.createOpportunities( NUM_OF_OPPORTUNITIES, TestUtils.AWARENESS_RECORD_TYPE.Id, testAccount.Id, true );
 		List<Opportunity> testOpportunities = TestUtils.createOpportunities( NUM_OF_OPPORTUNITIES, TestUtils.RESEARCH_RECORD_TYPE.Id, testAccount.Id, true );
		List<c2g__codaInvoiceLineItem__c> testSalesInvoiceLineItems = new List<c2g__codaInvoiceLineItem__c>();
		
        for( Opportunity anOpportunity : testOpportunities )
        {
	        testSalesInvoices.addAll(testUtils.createSalesInvoices(1, testAccount.id, null, false));
        }
        insert testSalesInvoices;
 		for( c2g__codaInvoice__c testSalesInvoice: testSalesInvoices )
		{
			testSalesInvoiceLineItems.add( TestUtils.createSalesInvoiceLineItem( testSalesInvoice.Id, testProduct.Id, false ) );
		}  
		insert testSalesInvoiceLineItems;     
        for (c2g__codaInvoice__c testSalesInvoice: testSalesInvoices){
            testScheduleLineItems.addAll( TestUtils.createScheduleLineItems(1, testSalesInvoice.Id,  false) );		
        }
        Integer i= 0;
        
		for (c2g__codaScheduleLineItem__c testScheduleLineItem : testScheduleLineItems){
			testScheduleLineItem.c2g__Amount__c = 10;
			testScheduleLineItem.c2g__UnitofWork__c = 1; 
			testScheduleLineItem.c2g__SalesInvoiceLineItem__c = testSalesInvoiceLineItems[i].id; 
			testScheduleLineItem.c2g__LineNumber__c  = i+ 1;
			i++;
		}
		
        test.startTest();
            insert testScheduleLineItems;
        test.stopTest();

		List<c2g__codaInvoice__c> insertedInvoices = [ SELECT id, c2g__Opportunity__c FROM c2g__codaInvoice__c WHERE Id IN :testSalesInvoices ];
        List<c2g__codaScheduleLineItem__c> insertedScheduleLineItems = [ SELECT id, Opportunity__c,c2g__Invoice__c FROM c2g__codaScheduleLineItem__c WHERE id IN :testScheduleLineItems ]; 
        Map<ID,List<c2g__codaScheduleLineItem__c>> invoiceSLIMap = new Map<ID,List<c2g__codaScheduleLineItem__c>>();
        for (c2g__codaScheduleLineItem__c sli :insertedScheduleLineItems){
            if(!invoiceSLIMap.containsKey(sli.c2g__Invoice__c)){
                invoiceSLIMap.put(sli.c2g__Invoice__c,new List<c2g__codaScheduleLineItem__c>());
            }
			invoiceSLIMap.get(sli.c2g__Invoice__c).add(sli);
        }

        System.assertEquals(NUM_OF_OPPORTUNITIES,insertedInvoices.size(),'All invoices have been retrieved');
        System.assertEquals(NUM_OF_LINE_ITEMS,insertedScheduleLineItems.size(),'All schedule line items have been retrieved');
        
        for( c2g__codaInvoice__c insertedInvoice: insertedInvoices )
        {
            for (c2g__codaScheduleLineItem__c sli : invoiceSLIMap.get(insertedInvoice.id)){
            	System.assertEquals( null,sli.Opportunity__c, 'The line item  opportunity is not populated' );
            }
        }

    }
    
}