trigger SubmitFromTrigger on Opportunity (after insert, after update) {

    list<Opportunity> updateOpps = new list<Opportunity>();
    list<Approval.ProcessSubmitRequest> reqList = new list<Approval.ProcessSubmitRequest>();
    Map<Id, ProcessInstanceWorkItem> oppIdToWorkItem = new Map<Id, ProcessInstanceWorkItem>();
    Set<Id> oppIdsForSubmit = new Set<Id>();
    List<Approval.ProcessWorkitemRequest> recallItems = new List<Approval.ProcessWorkitemRequest>();
    for(Opportunity opp : trigger.new)
    {
        if( opp.submit_from_trigger__c == 'Yes')
        {
            oppIdsForSubmit.add(opp.Id);
        }
    }

    if(!oppIdsForSubmit.isEmpty())
    {
        List<ProcessInstanceWorkItem> workItems = [SELECT Id, ProcessInstance.TargetObjectId 
        FROM ProcessInstanceWorkitem 
        WHERE ProcessInstance.TargetObjectId IN : oppIdsForSubmit];

        if(workItems.isEmpty())
        {
            for(Opportunity opp : [SELECT Id, StageName, Awaiting_Approval__c, Submit_From_Trigger__c FROM Opportunity WHERE Id IN : oppIdsForSubmit])
            {
                system.debug('Show opp: ' + opp);
                Opportunity placeHolderOpp = new Opportunity(id = opp.id);
                updateOpps.add(placeHolderOpp);
                approval.ProcessSubmitRequest req1 = new Approval.ProcessSubmitRequest();
                req1.setObjectId(opp.id);
                reqList.add(req1);
            }
        }
        approval.ProcessResult[] result = Approval.process(reqList);
        update updateOpps;
    }

}