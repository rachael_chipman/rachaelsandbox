@isTest
public class ReassignLeads_test{

    static testMethod void runReassignLeads_test1(){
    
    //This test is for a lead that in Nurture Status that has a score change meeting the threshold
    
    Lead l = new Lead(
    Company = 'Hummingbird Cakes',
    LastName = 'O Hara',
    FirstName = 'Scarlett',
    OwnerId = '00500000006olmF',
    Status = 'Nurture',
    Lead_Rating_Combined__c = 'D1',
    Campaign_Name__c = '12Q4_NA_PRO_PE_Oct_ETConnections_Indianapolis');
    insert l;
    
    l.Lead_Rating_Combined__c = 'B3';
    update l;
    
    }
    
    static testMethod void runReassignLeads_test2(){
    
    //This test is for a lead that in Nurture Status that gets assigned to a Contact Us Campaign
    
    Lead l = new Lead(
    Company = 'Hummingbird Cakes',
    LastName = 'O Hara',
    FirstName = 'Scarlett',
    OwnerId = '00500000006olmF',
    Status = 'Nurture',
    Lead_Rating_Combined__c = 'D1',
    Campaign_Name__c = '12Q4_NA_PRO_PE_Oct_ETConnections_Indianapolis');
    insert l;
    
    l.Campaign_Name__c = 'Contact Us';
    update l;
    
    }
}