global class InboxPreviewOverageBatch implements Database.Batchable<sObject>, Database.AllowsCallouts, Schedulable {
	
	public static final String BATCH_NAME = 'InboxPreviewOverageBatch';
    public static final String[] ERROR_EMAILS = new String[]{ 'salesforceplatform@returnpath.com' };
	Map<Id,Campaign_Preview_Events__c> eventMap;
    global String errors = '';
    global String query;
	
	/*global InboxPreviewOverageBatch(Map<Id, Campaign_Preview_Events__c> triggerEventMap) 
	{
		eventMap = triggerEventMap;	
	}*/

	global Date usageMonth = system.today().addMonths(-1).toStartOfMonth();

	global String buildQuery()
	{
		String queryString = 'SELECT Id, Messages__c, Account__c, Inbox_Preview_Limit__c, Month__c FROM Campaign_Preview_Events__c WHERE Month__c = : usageMonth AND CreatedDate = THIS_MONTH';
		if(test.isRunningTest())
		{
			queryString += ' AND Name = ' + '\'TEST_IP_EVENT\'' + ' LIMIT 200';
		}
		return queryString;
	}

	/*global InboxPreviewOverageBatch(String queryString)
	{
		query = queryString;
	}*/

	global void execute(SchedulableContext sc)
	{
		InboxPreviewOverageBatch schedBatch = new InboxPreviewOverageBatch();
		Database.executeBatch(schedBatch);
	}

	global Database.QueryLocator start(Database.BatchableContext BC) {
		return Database.getQueryLocator(buildQuery());
	}

   	global void execute(Database.BatchableContext BC, List<sObject> scope) 
   	{
		try
		{
			IPOverageOpportunityGenerationService.processTrigger(scope);
		}
		catch (DmlException dmx)
		{
			for(Integer i = 0; i < dmx.getNumDml(); i++)
			{
				errors += dmx.getDmlMessage(i);
			}
		}
	}
	
	global void finish(Database.BatchableContext BC) 
	{
		AsyncApexJob job = [SELECT Id, Status, NumberOfErrors,
	                         JobItemsProcessed, TotalJobItems,
	                         CreatedBy.Email
	                         FROM AsyncApexJob
	                         WHERE Id =:bc.getJobId()];

	     Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();

	     mail.setToAddresses( ERROR_EMAILS );
	     mail.setSubject( 'Batch to ' + BATCH_NAME + ' ' + job.Status );

	     String emailBody = 'Batch to ' + BATCH_NAME + ' processed '
	                      + job.TotalJobItems
	                      + ' batches with '
	                      + job.NumberOfErrors
	                      + ' failures.';
	     if( errors != '' )
	     {
		     emailBody += '\n\n\nThe following errors occured:\n'+ errors;
		     mail.setPlainTextBody( emailBody );
		     Messaging.sendEmail( new Messaging.SingleEmailMessage[]{ mail } );
	     }		
	}
	
}