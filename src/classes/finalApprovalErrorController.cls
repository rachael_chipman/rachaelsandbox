//Will allow us to direct users to the fields where they have missing information
//that needs to be populated before submitting to Finance for approval

public class finalApprovalErrorController{


        /*public Opportunity o {get;set;}
        public Boolean stageOk{get;set;}
        public list <OpportunityLineItem> olis;
        public OpportunityLineItem oli {get;set;}
        public list <OpportunityContactRole> contactRoles {get;set;}
        public Opportunity opp;
        public Account a {get;set;}
        public list <RealmID__c> RealmIds {get;set;}
        public Boolean Partner {get;set;}
        public Boolean PartnerComm {get;set;}
        public Boolean BillingType {get;set;}
        public Boolean WhoisInvoiced {get;set;}
        public Boolean InvoicingContact {get;set;}
        public Boolean Reasons {get;set;}
        public Boolean DealClose {get;set;}
        public Boolean AutoRenew {get;set;}
        public Boolean BillTerms {get;set;}
        public Boolean Type {get;set;}
        public Boolean Products = false;
        public Boolean ProdStart = false;
        public Boolean ProdEnd = false;
        public Boolean CertTier = false;
        public Boolean CertVol = false;
        public Boolean Events = false;
        public Boolean EFP = false;
        public Boolean Insight = false;
        public Boolean Fees = false;
        public Boolean Hide {get;set;}
        public pageReference toOpp;
        public pageReference toError;
        public pageReference toApprovalProcess;
        public pageReference toProductError;
        public pageReference toProductEntry;
        public pageReference toRealmsPage;
        public decimal PrevComm;
        public decimal Discount;
        public decimal PrevDisc;
        public Boolean InvoiceDistribution {get;set;}
        public Boolean CloseDate {get;set;}
        public Boolean DeactivationDate = false;


    @TestVisible public static c2g__codaGeneralLedgerAccount__c generalLedgerAccount
    {
        get
        {
            if( generalLedgerAccount == NULL )
            {
                generalLedgerAccount = [SELECT Id FROM c2g__codaGeneralLedgerAccount__c WHERE Name='1100 - AR-TRADE'];
            }
            return generalLedgerAccount;
        }
        set;
    }

    public Boolean contactRolesPresent {get;set;}
    public finalApprovalErrorController(ApexPages.StandardController std){

        o = [SELECT Id, CurrencyISOCode, CloseDate, AccountId, Account.c2g__CODAAccountsReceivableControl__c, Type, PartnerLookup__r.c2g__CODAAccountsReceivableControl__c, PartnerLookup__c, Partner_Commission__c, Who_Is_Invoiced__c, Billing_Type__c, Billing_Terms__c, Auto_Renew__c, Pricebook2Id, Nature_of_Involvement__c,
            Reasons_for_Lost_Opportunities__c, RecordTypeId, Deal_Closing_Through__c, Previous_Opportunity__c, StageName,  Type_of_Involvementtext__c, Lost_Pass_Details__c, Final_Approval_Error__c, CRM_User__c,
            HasOpportunityLineItem, Minimum_Start_Date__c, Has_Cert_Products_Qty__c, Cert_Only_Opportunity__c, of_IPs__c, Certification_Tier_Sold__c, Certification_Max_Volume__c, Account.Certification_Client__c, Hide_Product_Details_on_Invoice__c, Invoice_Distribution_Method__c, Tools_Child_ONLY__c,
            Product_Domain_Protect__c, Product_Domain_Protect_Child__c, Product_Domain_Secure__c, Product_Domain_Secure_Child__c, Product_Email_Brand_Monitor__c, Product_Email_Brand_Monitor_Child__c, Product_Domain_Audit_Service__c, Clone_for_Pending_Cert__c
            FROM Opportunity WHERE Id = :ApexPages.currentPage().getParameters().get('id')];

    try{
        contactRoles = [Select Id from OpportunityContactRole where OpportunityID = :o.Id AND IsPrimary = True];
    }
    catch (Exception e) {}

        a = [SELECT Id, Invoicing_Contact__c, c2g__CODAInvoiceEmail__c FROM Account WHERE Id = :o.AccountId];

        RealmIds = [SELECT Id, Name, Account__c FROM RealmID__c WHERE Account__c = :o.AccountId];

        olis = [SELECT Id, UnitPrice, Start_Date__c, End_Date__c, Product_Code__c, of_CP_Events_Allowed__c, of_MM_Events_Allowed__c, MM_Overage_Fee__c, CP_Overage_Fee__c,
        Number_of_Competitor_Domains__c, Number_of_Monitored_Domains__c, Product_Deactivation_Date__c
        FROM OpportunityLineItem WHERE OpportunityId = :o.Id];

            if(o.PartnerLookup__c == null && o.Nature_of_Involvement__c != 'No'){
                Partner = true;
                }
            if((o.Partner_Commission__c == null || o.Partner_Commission__c == 0) && o.PartnerLookup__c != null){
                PartnerComm = true;
                }
            if((o.Billing_Type__c != 'Annual' && o.Billing_Type__c != 'Monthly' && o.Billing_Type__c != 'Quarterly' && o.Billing_Type__c != 'Semi-Annual' && o.Billing_Type__c != 'Custom' && o.Type != 'Trial')|| (o.Billing_Type__c == null && o.Type != 'Trial')){
                BillingType = true;
                }
            if(o.PartnerLookup__c != null && o.Who_Is_Invoiced__c == null){
                WhoisInvoiced = true;
                }
            if(a.Invoicing_Contact__c == null && o.Type != 'Trial' && o.Deal_Closing_Through__c != 'Partner'){
                InvoicingContact = true;
                }
            if(o.RecordTypeId == '012000000004wEC' && (o.Reasons_for_Lost_Opportunities__c == null || o.Reasons_for_Lost_Opportunities__c == 'None')){
                Reasons = true;
                }
            if(o.Deal_Closing_Through__c == null && o.PartnerLookup__c != null){
                DealClose = true;
                }
            if(o.Billing_Terms__c == null && o.Type != 'Trial'){
                BillTerms = true;
                }
            if(o.Hide_Product_Details_on_Invoice__c == null && o.Type != 'Trial'){
                Hide = true;
                }
            if(o.Auto_Renew__c == null && o.Type != 'Trial'){
                AutoRenew = true;
                }
            if(o.Type == null){
                Type = false;
                }
            if(o.HasOpportunityLineItem == false){
                Products = true;
                }
            if(o.Has_Cert_Products_Qty__c > 0 && (o.Certification_Tier_Sold__c == null || o.Certification_Tier_Sold__c == 'No Certification Product')){
                CertTier = true;
                }
            if(o.Has_Cert_Products_Qty__c > 0 && o.Certification_Max_Volume__c == null)
            {
                CertVol = true;
            }
            if(o.CloseDate < system.today())
            {
                CloseDate = true;
            }
            if(o.Invoice_Distribution_Method__c == null)
            {
                InvoiceDistribution = true;
            }
             if(contactRoles.isEmpty() && o.Tools_Child_ONLY__c == false)
            {
                contactRolesPresent = false;
            }
            else
            {
                contactRolesPresent = true;
            }



            //Variable Definitions
            toOpp = new pageReference('/apex/tabView?id='+o.id);
            toError = new pageReference('/apex/finalApprovalError?id='+o.id);
            toApprovalProcess = new pageReference('/p/process/Submit?id='+o.id+'&retURL=%2F'+o.id);
            toProductError = new pageReference('/apex/finalApprovalProductError?id='+o.id);
            toProductEntry = new pageReference('/apex/ProductEntry?id='+o.id);
            toRealmsPage = new pageReference('/apex/CheckRealmsPage?id='+o.id);
            stageOk = false;




  }

    public pagereference checkpartner()
    {
        if(o.StageName != 'Signature' && o.StageName != 'Approval Required')
        {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'The opportunity must be in Signature stage before you can submit for final approval. If this is a Termination then the stage must be Approval Required.'));
            return null;
        }
        else
        {
            stageOk = true;
            //This if statement is here in order to check a box that will send an alert if an EFP opp has been submitted for approval.
            if(o.Product_Domain_Protect__c == true ||
            o.Product_Domain_Protect_Child__c == true ||
            o.Product_Domain_Secure__c == true ||
            o.Product_Email_Brand_Monitor__c == true ||
            o.Product_Email_Brand_Monitor_Child__c == true ||
            o.Product_Domain_Audit_Service__c == true)
            {
                //o.Send_EFP_Alert__c = true;
                o.Send_APS_Alert__c = true;
                update o;
            }

            List<OpportunityLineItem> olis2 = [SELECT Id, UnitPrice, Start_Date__c, End_Date__c, Product_Code__c, of_CP_Events_Allowed__c, of_MM_Events_Allowed__c, MM_Overage_Fee__c, CP_Overage_Fee__c, Number_of_Competitor_Domains__c, Number_of_Monitored_Domains__c, Opportunity.Type, Opportunity.Account.Bonded_Sender_Customer__c, Product_Deactivation_Date__c, Opportunity.Reasons_for_Lost_Opportunities__c, Opportunity.RecordType.Name FROM OpportunityLineItem WHERE OpportunityId = :o.Id];
            List<OpportunityLineItem> oppsToMarkForDeletion = new list<OpportunityLineItem>();
            if(olis2.size() > 0)
            {
                String Opptype = o.Type;
                System.debug('Opptype is:' + Opptype);
                for(OpportunityLineItem oli:olis2)
                {
                    if((oli.Start_Date__c == null && (oli.Product_Code__c != '005-CERT-REFC' && oli.Product_Code__c != '004-CERT-FEE0')) ||
                    (oli.Start_Date__c == null && (oli.Product_Code__c == '005-CERT-REFC' || oli.Product_Code__c == '004-CERT-FEE0') && o.Account.Certification_Client__c > 0)) {
                    ProdStart = true;
                    }
                    if((oli.End_Date__c == null && (oli.Product_Code__c != '005-CERT-REFC' && oli.Product_Code__c != '004-CERT-FEE0')) ||
                    (oli.End_Date__c == null && (oli.Product_Code__c == '005-CERT-REFC' || oli.Product_Code__c == '004-CERT-FEE0') && o.Account.Certification_Client__c > 0)) {
                    ProdEnd = true;
                    }
                  
                    if((oli.Product_Code__c == '012-ESPR-INTU' || oli.Product_Code__c == '002-DMSU-PLAT' || oli.Product_Code__c ==  '001-DMSU-GOLD') && (Opptype!= 'Supplement') && (oli.of_MM_Events_Allowed__c == null || oli.of_CP_Events_Allowed__c == null || (oli.of_MM_Events_Allowed__c == 0 || oli.of_CP_Events_Allowed__c == 0 ))){
                    Events = true;
                    }
               
                    if((oli.Product_Code__c == '012-ESPR-INTU' || oli.Product_Code__c == '002-DMSU-PLAT' || oli.Product_Code__c ==  '001-DMSU-GOLD') && (Opptype!= 'Supplement') && (oli.MM_Overage_Fee__c == null || oli.CP_Overage_Fee__c == null || (oli.MM_Overage_Fee__c == 0.0 || oli.CP_Overage_Fee__c == 0.0 ))){
                    Fees = true;
                    }
                    if((oli.Product_Code__c == '022-EMBR-MNTR' || oli.Product_Code__c == '026-DOMN-SECR' || oli.Product_Code__c == '024-DOMN-PROT' || oli.Product_Code__c == '029-DOMN-AUDT' || oli.Product_Code__c == '023-EMBR-MTCH' || oli.Product_Code__c == '028-DOMN-SECH' || oli.Product_Code__c == '028-DOMN-PRCH') && oli.Number_of_Monitored_Domains__c == null){
                    EFP = true;
                    }
                    if(o.RecordTypeId=='012000000004wEC' && oli.Product_Deactivation_Date__c == null && oli.Product_Code__c != 'REF-CR' && oli.Product_Code__c != 'RES-CR' && oli.Product_Code__c !='999-ACCT-DISC' && (o.Reasons_for_Lost_Opportunities__c =='RP Initiated' || o.Reasons_for_Lost_Opportunities__c =='Client Intiated')){
                    DeactivationDate = true;
                    }
                    if(oli.Product_Code__c == '030-DMSU-IBM' && oli.Number_of_Competitor_Domains__c == null){
                    Insight = true;
                    }
                    if(o.Clone_for_Pending_Cert__c == 'Yes' && (oli.Product_Code__c == '005-CERT-REFC' || oli.Product_Code__c == '004-CERT-FEE0'))
                    {
                        oli.Mark_for_Deletion__c = true;
                        oppsToMarkForDeletion.add(oli);
                    }
                }
                update oppsToMarkForDeletion;
            }

            
                
            if(o.Final_Approval_Error__c == 'No' && ProdStart == false && ProdEnd == false && 
                Events == false && Fees == false && CertTier == false && CertVol == false && 
                EFP == false && Insight == false && contactRolesPresent == true)
            {
                addcom();
                if( !updateAccountField() )
                {
                    return null;
                }
                toApprovalProcess.setRedirect(true);
                return toApprovalProcess;
            }

           /* else if(Products == true || ProdStart == true || ProdEnd == true || Events == true ||
                Fees == true || CertTier == true || CertVol == true || EFP == true || Insight == true || 
                DeactivationDate == True)
            {
                toProductError.setRedirect(true);
                return toProductError;
            }*/

            /*else
            {
                //return null;
                toApprovalProcess.setRedirect(true);
                return toApprovalProcess;
            }
        }
        

    }

    public pageReference goToProducts(){
    toProductEntry.setRedirect(true);
    return toProductEntry;
    }



    public pagereference addcom(){

        if(o.Partnerlookup__c != null && o.Partner_Commission__c != null){

        List<Opportunity> prevopps = [Select Id, Partner_Commission__c FROM Opportunity WHERE Id = :o.Previous_Opportunity__c LIMIT 1];
        if(prevopps.size() < 1){
        PrevComm = null;}
        else{
            for(Opportunity prevopp2 : prevopps){
            PrevComm = prevopp2.Partner_Commission__c;}
        }

        List<OpportunityLineItem> prevDiscOlis = ([SELECT Id, UnitPrice, Quantity, Description FROM OpportunityLineItem WHERE (Copy_of_Product_Name__c like 'Discount%') and OpportunityId = :o.Previous_Opportunity__c LIMIT 1]);
        if(prevDiscOlis.size() < 1){
        PrevDisc = 0;}
        else{
            for(OpportunityLineItem prevDiscOli : prevDiscOlis){
            PrevDisc = prevDiscOli.UnitPrice.SetScale(2);}
        }

         List<OpportunityLineItem> olistoDelete = new list<OpportunityLineItem> ([SELECT Id, OpportunityId, Copy_of_Product_Name__c FROM OpportunityLineItem WHERE OpportunityId = :o.Id and
        (Copy_of_Product_Name__c = 'Partner Commissions - Referral' or Copy_of_Product_Name__c = 'Partner Commissions - Reseller')]);


        if(olistodelete.size() > 0){
        o.Has_Partner_Line_Items__c = 'No';
        update o;
        delete olistoDelete;
        }

        List<OpportunityLineItem> olis = new list<OpportunityLineItem> ([SELECT Id, Start_Date__c, End_Date__c, UnitPrice, OpportunityId, Quantity, Product_Status__c, Previous_Value__c,
            Description__c FROM OpportunityLineItem WHERE OpportunityId = :o.Id and
             (Copy_of_Product_Name__c != 'Partner Commissions - Referral' and (NOT Copy_of_Product_Name__c like 'Discount%') and Copy_of_Product_Name__c != 'Partner Commissions - Reseller') and Mark_for_Deletion__c = false]);

        List<OpportunityLineItem> discountOlis = new list<OpportunityLineItem> ([SELECT Id, UnitPrice, Quantity, Description FROM OpportunityLineItem WHERE (Copy_of_Product_Name__c like 'Discount%') and OpportunityId = :o.Id LIMIT 1]);
        if (discountOlis.size() < 1){
        Discount = 0;}
        else{
            for(OpportunityLineItem disc : discountOlis){
                Discount = disc.UnitPrice.setScale(2);}

        }

        if(o.Deal_Closing_Through__c == 'Return Path'){

        PricebookEntry pbe = [SELECT Id, CurrencyISOCode, Pricebook2Id, Name FROM PricebookEntry WHERE IsActive = true and Pricebook2Id = :o.Pricebook2Id and CurrencyISOCode = :o.CurrencyISOCode and Name = 'Partner Commissions - Referral' LIMIT 1];

            for(OpportunityLineItem pcomm : olis){
            OpportunityLineItem poli = new OpportunityLineItem();
                poli.OpportunityId = o.Id;
                poli.Quantity = pcomm.Quantity;
                if(o.RecordTypeId == '012000000004wEC') poli.UnitPrice = 0.00;
                else poli.UnitPrice = ((pcomm.UnitPrice + Discount) * (o.Partner_Commission__c/100))* -1;
                if(o.Type == 'Renewal'&& pcomm.Previous_Value__c > 0 && PrevComm != null && o.RecordTypeId != '012000000004wEC') poli.Previous_Value__c = (pcomm.Previous_Value__c + PrevDisc)* ((PrevComm/100)* -1);
                if(o.RecordTypeId == '012000000004wEC') poli.Previous_Value__c = ((pcomm.Previous_Value__c + Discount) * (o.Partner_Commission__c/100)) *-1;
                if(o.RecordTypeId == '012000000004wEC') poli.Product_Status__c = 'Terminated';
                else poli.Product_Status__c = 'Current';
                poli.PricebookEntryId = pbe.Id;
                poli.Description__c = pcomm.Description__c;
                poli.Start_Date__c = pcomm.Start_Date__c;
                poli.End_Date__c = pcomm.End_Date__c;
                insert poli;
                }//Close oli loop
                }//Close if Deal Closing Through RP statement

        if(o.Deal_Closing_Through__c == 'Partner'){

        PricebookEntry pbe = [SELECT Id, CurrencyISOCode, Pricebook2Id, Name FROM PricebookEntry WHERE IsActive = true and Pricebook2Id = :o.Pricebook2Id and CurrencyISOCode = :o.CurrencyISOCode and Name = 'Partner Commissions - Reseller' LIMIT 1];

            for(OpportunityLineItem pcomm : olis){
            OpportunityLineItem poli = new OpportunityLineItem();
                poli.OpportunityId = o.Id;
                poli.Quantity = pcomm.Quantity;
                if(o.RecordTypeId == '012000000004wEC') poli.UnitPrice = 0.00;
                else poli.UnitPrice = ((pcomm.UnitPrice + Discount) * (o.Partner_Commission__c/100))* -1;
                if(o.Type == 'Renewal'&& pcomm.Previous_Value__c > 0 && PrevComm != null && o.RecordTypeId != '012000000004wEC') poli.Previous_Value__c = (pcomm.Previous_Value__c + PrevDisc)* ((PrevComm/100)* -1);
                if(o.RecordTypeId == '012000000004wEC') poli.Previous_Value__c = ((pcomm.Previous_Value__c + Discount) * (o.Partner_Commission__c/100)) *-1;
                if(o.RecordTypeId == '012000000004wEC') poli.Product_Status__c = 'Terminated';
                else poli.Product_Status__c = 'Current';
                poli.PricebookEntryId = pbe.Id;
                poli.Description__c = pcomm.Description__c;
                poli.Start_Date__c = pcomm.Start_Date__c;
                poli.End_Date__c = pcomm.End_Date__c;
                insert poli;
                }//Close oli loop
                }//Close if Deal Closing Through Partner statement


        update o;
        return null;

        }
        else{return null;}
        }

    // This function updates the Account's and the PartnerLookup's
    // c2g__CODAAccountsReceivableControl__c field to '1100 - AR-Trade'
    // if they are not alrady set.
    // It returns a boolean which if true indicates that the updates were
    // done successfully else returns false.
    public Boolean updateAccountField()
    {
        List<Account> accountsToBeUpdated = new List<Account>();
        Account oppAccount = new Account( Id = o.AccountId );

        if(String.isBlank(o.Account.c2g__CODAAccountsReceivableControl__c ))
        {
            oppAccount.c2g__CODAAccountsReceivableControl__c = generalLedgerAccount.Id;
            accountsToBeUpdated.add( oppAccount );
        }
        if( !String.isBlank( o.PartnerLookup__c ))
        {
            Account oppPartnerAccount = new Account( Id = o.PartnerLookup__c );
            if(String.isBlank(o.PartnerLookup__r.c2g__CODAAccountsReceivableControl__c ))
            {
                oppPartnerAccount.c2g__CODAAccountsReceivableControl__c = generalLedgerAccount.Id;
                accountsToBeUpdated.add( oppPartnerAccount );
            }
        }
        try
        {
            update accountsToBeUpdated;
        }
        catch( DmlException dmlEx )
        {
            Apexpages.addMessages( dmlEx );
            return false;
        }
        return true;
     }


     public pagereference recalc(){

        if(o.Partnerlookup__c != null && o.Partner_Commission__c != null){

        List<Opportunity> prevopps = [Select Id, Partner_Commission__c FROM Opportunity WHERE Id = :o.Previous_Opportunity__c LIMIT 1];
        if(prevopps.size() < 1){
        PrevComm = null;}
        else{
            for(Opportunity prevopp2 : prevopps){
            PrevComm = prevopp2.Partner_Commission__c;}
        }

        List<OpportunityLineItem> prevDiscOlis = ([SELECT Id, UnitPrice, Quantity, Description FROM OpportunityLineItem WHERE (Copy_of_Product_Name__c like 'Discount%') and OpportunityId = :o.Previous_Opportunity__c LIMIT 1]);
        if(prevDiscOlis.size() < 1){
        PrevDisc = 0;}
        else{
            for(OpportunityLineItem prevDiscOli : prevDiscOlis){
            PrevDisc = prevDiscOli.UnitPrice.SetScale(2);}
        }

         List<OpportunityLineItem> olistoDelete = new list<OpportunityLineItem> ([SELECT Id, OpportunityId, Copy_of_Product_Name__c FROM OpportunityLineItem WHERE OpportunityId = :o.Id and
        (Copy_of_Product_Name__c = 'Partner Commissions - Referral' or Copy_of_Product_Name__c = 'Partner Commissions - Reseller')]);


        if(olistodelete.size() > 0){
        delete olistoDelete;
        }

        List<OpportunityLineItem> olis = new list<OpportunityLineItem> ([SELECT Id, Start_Date__c, End_Date__c, UnitPrice, OpportunityId, Quantity, Product_Status__c, Previous_Value__c,
            Description__c FROM OpportunityLineItem WHERE OpportunityId = :o.Id and
             (Copy_of_Product_Name__c != 'Partner Commissions - Referral' and (NOT Copy_of_Product_Name__c like 'Discount%') and Copy_of_Product_Name__c != 'Partner Commissions - Reseller') and Mark_for_Deletion__c = false]);

        List<OpportunityLineItem> discountOlis = new list<OpportunityLineItem> ([SELECT Id, UnitPrice, Quantity, Description FROM OpportunityLineItem WHERE (Copy_of_Product_Name__c like 'Discount%') and OpportunityId = :o.Id LIMIT 1]);
        if (discountOlis.size() < 1){
        Discount = 0;}
        else{
            for(OpportunityLineItem disc : discountOlis){
                Discount = disc.UnitPrice.setScale(2);}

        }

        if(o.Deal_Closing_Through__c == 'Return Path'){

        PricebookEntry pbe = [SELECT Id, CurrencyISOCode, Pricebook2Id, Name FROM PricebookEntry WHERE IsActive = true and Pricebook2Id = :o.Pricebook2Id and CurrencyISOCode = :o.CurrencyISOCode and Name = 'Partner Commissions - Referral' LIMIT 1];

            for(OpportunityLineItem pcomm : olis){
            OpportunityLineItem poli = new OpportunityLineItem();
                poli.OpportunityId = o.Id;
                poli.Quantity = pcomm.Quantity;
                poli.UnitPrice = ((pcomm.UnitPrice + Discount) * (o.Partner_Commission__c/100))* -1;
                if(o.Type == 'Renewal'&& pcomm.Previous_Value__c > 0 && PrevComm != null && o.RecordTypeId != '012000000004wEC') poli.Previous_Value__c = (pcomm.Previous_Value__c + PrevDisc)* ((PrevComm/100)* -1);
                if(o.RecordTypeId == '012000000004wEC') poli.Previous_Value__c = ((pcomm.Previous_Value__c + Discount) * (o.Partner_Commission__c/100)) *-1;
                if(o.RecordTypeId == '012000000004wEC') poli.Product_Status__c = 'Terminated';
                else poli.Product_Status__c = 'Current';
                poli.PricebookEntryId = pbe.Id;
                poli.Description__c = pcomm.Description__c;
                poli.Start_Date__c = pcomm.Start_Date__c;
                poli.End_Date__c = pcomm.End_Date__c;
                insert poli;
                }//Close oli loop
                }//Close if Deal Closing Through RP statement

        if(o.Deal_Closing_Through__c == 'Partner'){

        PricebookEntry pbe = [SELECT Id, CurrencyISOCode, Pricebook2Id, Name FROM PricebookEntry WHERE IsActive = true and Pricebook2Id = :o.Pricebook2Id and CurrencyISOCode = :o.CurrencyISOCode and Name = 'Partner Commissions - Reseller' LIMIT 1];

            for(OpportunityLineItem pcomm : olis){
            OpportunityLineItem poli = new OpportunityLineItem();
                poli.OpportunityId = o.Id;
                poli.Quantity = pcomm.Quantity;
                if(o.RecordTypeId == '012000000004wEC') poli.UnitPrice = 0.00;
                else poli.UnitPrice = ((pcomm.UnitPrice + Discount) * (o.Partner_Commission__c/100))* -1;
                if(o.Type == 'Renewal'&& pcomm.Previous_Value__c > 0 && PrevComm != null && o.RecordTypeId != '012000000004wEC') poli.Previous_Value__c = (pcomm.Previous_Value__c + PrevDisc)* ((PrevComm/100)* -1);
                if(o.RecordTypeId == '012000000004wEC') poli.Previous_Value__c = ((pcomm.Previous_Value__c + Discount) * (o.Partner_Commission__c/100)) *-1;
                if(o.RecordTypeId == '012000000004wEC') poli.Product_Status__c = 'Terminated';
                else poli.Product_Status__c = 'Current';
                poli.PricebookEntryId = pbe.Id;
                poli.Description__c = pcomm.Description__c;
                poli.Start_Date__c = pcomm.Start_Date__c;
                poli.End_Date__c = pcomm.End_Date__c;
                insert poli;
                }//Close oli loop
                }//Close if Deal Closing Through Partner statement


        update o;
        return new PageReference('/' + ApexPages.currentPage().getParameters().get('Id'));

        }
        else{return null;}
        }



    public pageReference GoBack(){
    toOpp.setRedirect(true);
    return toOpp;
    }

    public pageReference SaveChanges(){
    if(o.PartnerLookup__c != null){
    account a = [select crm__c from account where id = :o.partnerlookup__c limit 1];
    if(o.CRM_User__c == null)
    {
        o.crm_user__c = a.crm__c;
    }
    WhoisInvoiced();}
    update o;
    Opp = [Select Id, Final_Approval_Error__c from Opportunity where Id = :o.Id];
    if(Opp.Final_Approval_Error__c == 'Yes' || contactRolesPresent==false){
    toError.setRedirect(true);
    return toError;
    }
    else{
    OpportunityServices.sendEmail('II_Internal_Use_Financial_Approval', o.Id);
    addcom();
    toApprovalProcess.setRedirect(true);
    return toApprovalProcess;
    }
    }

    public PageReference WhoisInvoiced(){
        if(o.Deal_Closing_Through__c == 'Partner'){
        o.Who_is_Invoiced__c = 'Partner is Invoiced';
        o.Type_of_Involvementtext__c = 'Reseller';
        o.Nature_of_Involvement__c = 'Yes';
        update o;
        return null;
        }
        if(o.Deal_Closing_Through__c == 'Return Path'){
        o.Type_of_Involvementtext__c = 'Referral';
        o.Nature_of_Involvement__c = 'Yes';
        update o;
        return null;
        }
        if(o.Deal_Closing_Through__c == null){
        o.Type_of_Involvementtext__c = null;
        update o;
        return null;
        }
        return null;
    }

    public PageReference UpdateOpp(){
    if(o.PartnerLookup__c != null){
    account a = [select crm__c from account where id = :o.partnerlookup__c limit 1];
    if(o.CRM_User__c == null)
    {
        o.crm_user__c = a.crm__c;
    }
    update o;
    toError.setRedirect(true);
    return toError;
    }
    return null;
    }*/
}