public without sharing class LeadOwnerChange_QualifyingStatus {
    
    public static String SDR_ACCEPT = 'SDR Accept';
    public static String SDR_DQ = 'SDR DQ';
    public static String SDR_QUALIFIED = 'SDR Qualified';
    public static String OPEN = 'Open';
    public static String RSE_DQ = 'RSE DQ';
    public static String RSE_QUALIFIED = 'RSE Qualified';
    public static String Eloqua_Automation = 'Eloqua Automation';
    public static String SDRProfile = 'SDR';
    /*public static boolean IsLeadCampaignMostRecentChanged = false;
    public static boolean IsLeadSourceOriginalChanged = false;
    public static boolean IsLeadSourceMostRecentChanged = false;
    public static boolean IsLeadAssetOriginalChanged = false;
    public static boolean IsLeadAssetMostRecentChanged = false;
    public static boolean IsLeadStatusChanged = false;*/
    public static boolean hasTriggerRan = false;
  
   public static String CurrentUserProfile
    {
        get
        {
            if(CurrentUserProfile == NULL)
            {
                Id profileId = UserInfo.getProfileId();
                CurrentUserProfile = [Select Id,Name from Profile where Id =: profileId limit 1].Name;
            }
            return CurrentUserProfile;

        }
        private set;
    }
   
    public static Id currentUserId
    {
        get
        {
            if(currentUserId == NULL)
            {
                currentUserId = UserInfo.getUserId();
            }
            return currentUserId;
        }
        private set; 
    }

    
    public static list<Lead> filterLeadsOnUpdate (list<Lead> newLeads, Map<Id, Lead> oldLeads)
    {
        list<Lead> filteredLeads = new list<Lead>();
        integer i =1;
        for(Lead aLead : newLeads)
        {
            System.debug('Size of the new list is :' + newLeads.size());
            Lead oldLead;
            if(oldLeads != NULL)
            {
                oldLead = oldLeads.get(aLead.Id);
            }
            system.debug('Is old lead null' + oldLead);
            system.debug('What is the current status: '+ aLead.Status + ' What is the old Status: ' + oldLead.Status);
            system.debug('What is the old asset? ' + oldLead.Campaign_Asset_Most_Recent__c + 'What is the new Asset? ' + aLead.Campaign_Asset_Most_Recent__c);
            system.debug('What is the old source? ' + oldLead.Campaign_Source_Most_Recent__c + 'What is the new Source? ' + aLead.Campaign_Source_Most_Recent__c);
            //This was previously done when QSH was only created when the Qualifying status Changed. Now its created when any of the field
            //like Lead-Source original , Lead Source Original Detail etc changes. See the requirement doc to see all fields.           
            if((aLead.Status != oldLead.Status) /*|| 
              ((aLead.Campaign_Asset_Most_Recent__c != oldLead.Campaign_Asset_Most_Recent__c)  && aLead.Campaign_Asset_Most_Recent__c != NULL) ||
              ((aLead.Campaign_Source_Most_Recent__c != oldLead.Campaign_Source_Most_Recent__c) && aLead.Campaign_Source_Most_Recent__c != NULL)*/ && !aLead.isConverted) 
            {
               
                filteredLeads.add(aLead);
                system.debug(' In the status block ' +filteredLeads.size());
            }
                    
            /*if(aLead.Most_Recent_Lead_Source__c!= oldLead.Most_Recent_Lead_Source__c)
                IsLeadSourceMostRecentChanged = true;
                system.debug('Boolean 1' + IsLeadSourceMostRecentChanged);
            if(!String.IsEmpty(aLead.LeadSource) && String.IsEmpty(oldLead.LeadSource))
                IsLeadSourceOriginalChanged = true;
                system.debug('Boolean 2' +  IsLeadSourceOriginalChanged);
            if(!String.IsEmpty(aLead.Lead_Asset_Original__c) && String.IsEmpty(oldLead.Lead_Asset_Original__c))
                IsLeadAssetoriginalChanged = true;
                system.debug('Boolean 3' + IsLeadAssetOriginalChanged);
            if(aLead.Lead_Asset_Most_Recent__c != oldLead.Lead_Asset_Most_Recent__c)
                IsLeadAssetMostRecentChanged= true;
                system.debug('Boolean 4' +  IsLeadAssetMostRecentChanged);
            if(aLead.Status != oldLead.Status)
            {
                IsLeadStatusChanged = true;
            }
            if(aLead.Campaign_Name__c != oldLead.Campaign_Name__c)
            {
                IsLeadCampaignMostRecentChanged = true;
            }*/
            
        }
        system.debug('What is in filtered Leads size is '+filteredLeads.size());
        return filteredLeads;
    }
    
   
    
    public static void processLeads(list<Lead> filteredLeads, map<Id, Lead> oldLeads)
    { 
        System.debug('Filtered Lead list Count is : in Process ' + filteredLeads.size());
      
        
        list<Lead> leadsToUpdate = new list<Lead>();
        list<Qualifying_Status_History__c> historyRecords = new list<Qualifying_Status_History__c>();
        //get the ID of the 'Eloqua Automation' user
        ID EL_ID = [Select Id, Name from User where Name = :Eloqua_Automation limit 1][0].Id;

        Map<Id, Lead> updateLeadMap = new Map<Id, Lead>();
        
        for(Lead aLead : filteredLeads)
        {
            String ownerIdString = aLead.OwnerId;
            System.debug('CreatedBy Id of the Lead is : ' +aLead.CreatedById);
            system.debug('The Lead Status is' + aLead.Status);
             
            //Lead updateLead = new Lead(Id= aLead.Id);
            Lead oldLead;
            system.debug('what is in old leads?' + oldLeads);
            if(oldLeads != NULL)
            {
                oldLead = oldLeads.get(aLead.Id);
            }
            system.debug(logginglevel.INFO, 'What is the value of Original SDR?' + aLead.Original_SDR__c);
			system.debug(logginglevel.INFO, 'What is the Lead Status' + aLead.Status);            
            if(aLead.Original_SDR__c == NULL && (aLead.Status == SDR_ACCEPT || aLead.Status ==  SDR_DQ))
            {
                system.debug(logginglevel.INFO, 'What is the current user Id' + currentUserId);
                //populate this field only if the user is a SDR
                if(CurrentUserProfile.contains(SDRProfile))
                    aLead.Original_SDR__c = currentUserId;

            }
            else if((aLead.Status == SDR_DQ || aLead.Status == RSE_DQ) && aLead.OwnerId != AccountOwnerChange.ELOQUA.Id)
            {
                //updateLead.OwnerId = AccountOwnerChange.ELOQUA.Id;
                //leadsToUpdate.add(updateLead);
                aLead.OwnerId = AccountOwnerChange.ELOQUA.Id;
            }
            else if(aLead.Status == SDR_QUALIFIED && aLead.For_RSE__c != NULL)
            {
                //updateLead.OwnerId = aLead.For_RSE__c;
                //leadsToUpdate.add(updateLead);
                aLead.OwnerId = aLead.For_RSE__c;
            }                                                                                                            //Making sure the owner of the lead isn't a queue   
            else if(aLead.Status == OPEN && aLead.Original_Marketing_Router__c == NULL &&  EL_ID == aLead.CreatedById && ownerIdString.substring(0,3) != '00G')
            {
                
                //updateLead.Original_Marketing_Router__c = aLead.OwnerId;
                //leadsToUpdate.add(updateLead);
                aLead.Original_Marketing_Router__c = aLead.OwnerId;
            }
           
            //Trying time stamp population differently - RC (Want to base on actual change of Source and Asset campaign Ids,
            // because I feel like we are missing the time stamps if there are two campaigns in a row of the same type, because then the source field wouldn't actually change)
            system.debug('Is there a difference with the source campaign? New:' + aLead.Campaign_Original__c + '- Old:' + oldLead.Campaign_Original__c);
            /*if(aLead.Campaign_Original__c != oldLead.Campaign_Original__c)
            {
                //updateLead.Lead_Source_Original_Time_Date_Stamp__c = system.now();
                //updateLead.Lead_Source_Most_Recent_TimeSTamp__c = system.now();
                //leadsToUpdate.add(updateLead);
                aLead.Lead_Source_Original_Time_Date_Stamp__c = system.now();
                aLead.Lead_Source_Most_Recent_TimeSTamp__c = system.now();

            }
            else if((aLead.Campaign_Source_Most_Recent__c != oldLead.Campaign_Source_Most_Recent__c) && (aLead.Campaign_Original__c == oldLead.Campaign_Original__c))
            {
                //updateLead.Lead_Source_Most_Recent_TimeSTamp__c = system.now();
                //leadsToUpdate.add(updateLead);
                aLead.Lead_Source_Most_Recent_TimeSTamp__c = system.now();
            }
            
            if(aLead.Campaign_Asset_Original__c != oldLead.Campaign_Asset_Original__c)
            {
                //updateLead.Lead_Asset_Original_Time_Date_Stamp__c = system.now();
                //updateLead.Lead_Asset_Most_Recent_Time_Date_Stamp__c = system.now();
                //leadsToUpdate.add(updateLead);
                aLead.Lead_Asset_Original_Time_Date_Stamp__c = system.now();
                aLead.Lead_Asset_Most_Recent_Time_Date_Stamp__c = system.now();
            }
            else if((aLead.Campaign_Asset_Most_Recent__c != oldLead.Campaign_Asset_Most_Recent__c) && (aLead.Campaign_Asset_Original__c == oldLead.Campaign_Asset_Original__c))
            {
                //updateLead.Lead_Asset_Most_Recent_Time_Date_Stamp__c = system.now();
                //leadsToUpdate.add(updateLead);
                aLead.Lead_Asset_Most_Recent_Time_Date_Stamp__c = system.now();
            }*/
            //populate/update the timestamp fields for LeadSource-Original and Lead Source Most Recent
            /*if(IsLeadSourceOriginalChanged)
                updateLead.Lead_Source_Original_Time_Date_Stamp__c = System.now();
            if(IsLeadSourceMostRecentChanged)
             updateLead.Lead_Source_Most_Recent_TimeSTamp__c = System.now(); 
			if(IsLeadAssetOriginalChanged)
                updateLead.Lead_Asset_Original_Time_Date_Stamp__c = System.now();
            if(IsLeadAssetMostRecentChanged)
                updateLead.Lead_Asset_Most_Recent_Time_Date_Stamp__c = System.now();*/
            system.debug('The Lead Status is 2'+aLead.Status);
            
            /*if(updateLeadMap.containsKey(aLead.Id))

            {
                updateLeadMap.put(updateLead.Id, updateLead);
            }
            else
            {
                updateLeadMap.put(aLead.Id, aLead);
            }*/
            
            System.debug('Creating history records for leads' +alead.Status);
            Qualifying_Status_History__c history = new Qualifying_Status_History__c();
            history.Lead__c = aLead.Id;
            history.Campaign_Source_Original__c = aLead.Campaign_Original__c;
            history.Campaign_Source_Most_Recent__c = aLead.Campaign_Source_Most_Recent__c;
            history.Campaign_Asset_Original__c = aLead.Campaign_Asset_Original__c;
            history.Campaign_Asset_Most_Recent__c = aLead.Campaign_Asset_Most_Recent__c;
            history.Lead_Source__c = aLead.Most_Recent_Lead_Source__c;
            history.Campaign_Name__c = aLead.Campaign_Name__c;
            history.Lead_Source_Original__c = aLead.LeadSource;
            history.Lead_Source_Original_Detail__c = aLead.Lead_Source_Original_Detail__c;
            history.Lead_Source_Most_Recent_Detail__c = aLead.Lead_Source_Most_Recent_Detail__c;
            history.Original_Campaign_Name__c = aLead.First_Campaign__c;
            history.Qualifying_Status__c = aLead.Status;
            history.Previous_Qualifying_Status__c = oldLead.Status;
            history.Lead_Asset_Most_Recent__c = aLead.Lead_Asset_Most_Recent__c;
            history.Lead_Asset_Most_Recent_Detail__c = aLead.Lead_Asset_Most_Recent_Detail__c;
            history.Lead_Asset_Original__c = aLead.Lead_Asset_Original__c;
            history.Lead_Asset_Original_Detail__c = aLead.Lead_Asset_Original_Detail__c;
            history.Unique_ID__c = aLead.Status + oldLead.Status + system.now() + aLead.Id;
            /*if(IsLeadCampaignMostRecentChanged || IsLeadStatusChanged)
            {*/
                historyRecords.add(history);
            //}
            
        }
       
        //put a savepoint
        Savepoint SP = Database.setSavepoint();
        try
        {            
            if(!CampaignFieldUpdate.updateLeadMap.isEmpty())
            {
                update CampaignFieldUpdate.updateLeadMap.values();
            }

            //update updateLeadMap.values();
        }
        catch (DmlException e)
        {
            //add rollback
            for(Integer i = 0; i < e.getNumDml(); i++)
            {
                system.debug('What was the error message?'+e.getDmlMessage(i));
                CampaignFieldUpdate.updateLeadMap.values()[e.getDmlIndex(i)].addError(e.getDmlMessage(i));
            }
        }
        try
        {
            System.debug('Upserting history records for leads, size is :' +historyRecords.size());
            upsert historyRecords Unique_Id__c;
            //insert historyRecords;
        }
        catch (DmlException e)
        {
            Database.rollback(SP);
            /*for(Qualifying_Status_History__c history : historyRecords)
            {
                system.debug('What was the error message?'+e);
                history.addError('There was an issue creating the history record.');
            }*/

            Map<Id, Lead> leadMap = new Map<Id, Lead>(filteredLeads);
            for(Integer i = 0; i < e.getNumDml(); i++)
            {
                system.debug('What is causing the rollback?' + e.getDmlMessage(i));
                Qualifying_Status_History__c errorHistory = historyRecords[e.getDmlIndex(i)];
                Lead errorLead = leadMap.get(errorHistory.Lead__c);
                errorLead.addError(e.getDmlMessage(i));
            } 
        }
      
    }
    
    public static void processTrigger(list<Lead> newLeads, Map<Id, Lead> oldLeads)
    {
        list<Lead> filterLeads = filterLeadsOnUpdate(newLeads, oldLeads);
        if(!filterLeads.isEmpty())
        {
            System.debug('Before processing the filtered leads');
            processLeads(filterLeads, oldLeads);
        }
    }
    
    /*public static void CheckLead_Source_AssetOriginalValuesChanged(list<Lead> newLeads, Map<Id, Lead> oldLeads)
    {
        for(Lead aLead:newLeads)
        {
           
            Lead oldLead;
            if(oldLeads != NULL)
            {
                oldLead = oldLeads.get(aLead.Id);
            }
            //Prevent update Lead SOurce Original and Lead Asset Original Values
            /*if((aLead.Lead_Asset_Original__c!= oldLead.Lead_Asset_Original__c) && (!String.IsEmpty(aLead.Lead_Asset_Original__c) && !String.isEmpty(oldLead.Lead_Asset_Original__c)))
            {
           	   aLead.Lead_Asset_Original__c = oldLead.Lead_Asset_Original__c;
               aLead.Lead_Asset_Original_Detail__c = oldLead.Lead_Asset_Original_Detail__c;
            }
            if((aLead.Lead_Asset_Original_Detail__c!= oldLead.Lead_Asset_Original_Detail__c) && (!String.IsEmpty(aLead.Lead_Asset_Original_Detail__c) && !String.isEmpty(oldLead.Lead_Asset_Original_Detail__c)))
            {
           	   aLead.Lead_Asset_Original__c = oldLead.Lead_Asset_Original__c;
               aLead.Lead_Asset_Original_Detail__c = oldLead.Lead_Asset_Original_Detail__c;
            }
            
            if((aLead.LeadSource!= oldLead.LeadSource) && (!String.IsEmpty(aLead.LeadSource) && !String.isEmpty(oldLead.LeadSource)))
            {
           	   aLead.LeadSource = oldLead.LeadSource;
               aLead.Lead_Source_Original_Detail__c = oldLead.Lead_Source_Original_Detail__c;
            }
            if((aLead.Lead_Source_Original_Detail__c!= oldLead.Lead_Source_Original_Detail__c) && (!String.IsEmpty(aLead.Lead_Source_Original_Detail__c) && !String.isEmpty(oldLead.Lead_Source_Original_Detail__c)))
            {
           	   aLead.LeadSource = oldLead.LeadSource;
               aLead.Lead_Source_Original_Detail__c = oldLead.Lead_Source_Original_Detail__c;
            }
        }
    }*/

    
  }