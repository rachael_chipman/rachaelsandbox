@isTest
public class MonkeyEmailSendTest {
    
   static Pricebook2 pb;
    static Product2 testProduct;
    Static PricebookEntry toolsProduct;
    
    static testMethod void processMonkeyEmailSend(){
        
        Account testAcct = testUtils.createAccount(TestUtils.MARKETER_RECORD_TYPE.Id, True);
        
        Opportunity testOpp1 = testUtils.createOpportunity(TestUtils.RESEARCH_RECORD_TYPE.Id, testAcct.Id, false);
        testOpp1.Type = 'New Business';
        testOpp1.StageName = 'Finance Approved';
        insert testOpp1;
        
        pb = TestUtils.STANDARD_PRICE_BOOK;
        
        testProduct = TestUtils.createProduct(false);
        testProduct.Family = 'Email Intelligence for Marketers';
        insert testProduct;
        
        toolsProduct = TestUtils.createPricebookEntry(testProduct.Id, pb.Id, true);
        
        OpportunityLineItem testOLI = testUtils.createOpportunityLineItem(testOpp1.Id, toolsProduct.Id, false);
        testOLI.Start_Date__c = system.today().addDays(-365);
        testOli.End_Date__c = system.today().addDays(-1);
        testOLI.UnitPrice = 1200;
        insert testOLI;
        System.debug('What is the Net ACV' + testOpp1.Inc_ACV_Before_Partner_Comm__c);
        
        test.startTest();
        testOpp1.Send_Monkey_Email__c = 'Send';
        update testOpp1;
        test.stopTest();

        Opportunity opp = [SELECT Send_Monkey_Email__c FROM Opportunity WHERE Id =: testOpp1.id];
        
        System.assertEquals('Monkey Sent', opp.Send_Monkey_Email__c, 'We expect value to update to Monkey Sent');        
        
    }
    
    static testMethod void processMonkeyEmailSendFee(){
        
        Account testAcct = testUtils.createAccount(TestUtils.MARKETER_RECORD_TYPE.Id, True);
        
        Opportunity testOpp2 = testUtils.createOpportunity(TestUtils.RESEARCH_RECORD_TYPE.Id, testAcct.Id, false);
        testOpp2.Type = 'Fee';
        testOpp2.StageName = 'Finance Approved';
        insert testOpp2;
        
        pb = TestUtils.STANDARD_PRICE_BOOK;
        
        testProduct = TestUtils.createProduct(false);
        testProduct.Family = 'Email Intelligence for Marketers';
        insert testProduct;
        
        toolsProduct = TestUtils.createPricebookEntry(testProduct.Id, pb.Id, true);
        
        OpportunityLineItem testOLI = testUtils.createOpportunityLineItem(testOpp2.Id, toolsProduct.Id, false);
        testOLI.Start_Date__c = system.today().addDays(-365);
        testOli.End_Date__c = system.today().addDays(-1);
        testOLI.UnitPrice = 1200;
        insert testOLI;
        System.debug('What is the Net ACV' + testOpp2.Inc_ACV_Before_Partner_Comm__c);
        
        test.startTest();
        testOpp2.Send_Monkey_Email__c = 'Send';
        update testOpp2;
        test.stopTest();

        Opportunity opp2 = [SELECT Send_Monkey_Email__c FROM Opportunity WHERE Id =: testOpp2.id];
        
        System.assertEquals('No Monkey Needed', opp2.Send_Monkey_Email__c, 'We expect value to update to Monkey Sent');        

    }
}