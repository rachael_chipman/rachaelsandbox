public class GenRefOppCreateController{
 

    Public Contact con;   
   
   
    public GenRefOppCreateController(ApexPages.StandardController stdController){
    con = [SELECT Id, AccountId, OwnerId, Referring_CRM__c, Gen_Ref_Contact__c, First_Campaign__c, Campaign_Name__c, Most_Recent_Campaign_ID__c, Referring_Partner__c 
    FROM Contact 
    WHERE Id = :ApexPages.currentPage().getParameters().get('Id')];  
    }
    

    
    
    public PageReference NewOpp(){
        Opportunity o = new Opportunity(
        name = 'Gen Ref Opportunity',
        AccountId = con.AccountId,
        StageName='Pre-Pipeline',
        RecordTypeId = TestUtils.RESEARCH_RECORD_TYPE.Id,
        CloseDate=system.today(),
        CRM_User__c = con.Referring_CRM__c,
        Gen_Referral_Opp__c = true,
        OwnerId = con.OwnerId,
        First_Campaign__c = con.First_Campaign__c,
        CampaignId = con.Most_Recent_Campaign_Id__c, 
        Most_Recent_Campaign__c = con.Campaign_Name__c,
        Partnerlookup__c = con.Referring_Partner__c);

        insert o;
        
        OpportunityContactRole cr = new OpportunityContactRole(
        ContactId = con.Id,
        IsPrimary = true,
        OpportunityId = o.Id,
        Role = 'Decision Maker');
        
        insert cr; 
        
               pageReference ref = new PageReference('/'+o.Id);
    ref.setRedirect(true);
    return ref;
    }

}