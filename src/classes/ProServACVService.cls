public with sharing class ProServACVService {
	
	/*public void processTrigger ( List<Opportunity> newOpportunities, Map<Id, Opportunity> OldOpps )
	{
		List<Opportunity> filteredOpportunities = filterOpportunitiesOnUpdate( newOpportunities , oldOpps );
        
        if(!filteredOpportunities.isEmpty())
        {
        	List<OpportunityLineItem> opportunityWithLineItems = getOLIs( filteredOpportunities );
            processProServLineItems ( opportunityWithLineItems, newOpportunities);
        }
	}
	
	public static void processProServLineItems ( List<OpportunityLineItem> ProServOlis, List<Opportunity> newOpportunities ) 
	{
		Map<Id,List<OpportunityLineItem>> AccountIdtoProServLinesMap = OpptoProServLineMap ( ProServOlis );
		
		List<Account> updateAccounts = new List<Account>();
		
		List<AggregateResult> price = [SELECT Opportunity.AccountId acctid, SUM(TotalPrice)sum 
		FROM OpportunityLineItem 
		WHERE Id IN : ProServOlis and (Start_Date__c = LAST_N_DAYS:181 or Start_Date__c = NEXT_N_DAYS: 181) 
		GROUP BY Opportunity.AccountId ];
           
        for (AggregateResult ar : price)  
        {
        	Id AccountId = (id)ar.get('acctid');
		
			if(AccountIdtoProServLinesMap.containsKey(AccountId))
			{
				Account acc = new Account (Id = AccountId);	        	
	        	acc.ProServ_Client__c = true;
	        	acc.ProServ_ACV__c = (Decimal) ar.get('sum');
	        	updateAccounts.add(acc);
			}
		}

        update updateAccounts;
	}
	
	public static List<Opportunity> filterOpportunitiesOnUpdate( List<Opportunity> newOpportunities, Map<Id, Opportunity> OldOpps )
	{
		List<Opportunity> filteredOpportunities = new List<Opportunity>();
        for( Opportunity o : newOpportunities )
        {
            Opportunity OldOpp = OldOpps.get( o.Id );
            if( o.StageName == 'Finance Approved' && o.Product_Deliverability_Consulting__c == true && oldOpp.StageName != 'Finance Approved' )
            {
            	filteredOpportunities.add( o );
            } 
        }
		return filteredOpportunities;
	}
	
	public static List<OpportunityLineItem> getOLIs ( List<Opportunity> filteredOpportunities )
	{
		 List<OpportunityLineItem>  ProServOLIs = [SELECT Id, Account_Id__c, Start_Date__c, End_Date__c, Product_Code__c, UnitPrice, TotalPrice, OpportunityId 
				FROM OpportunityLineItem
				WHERE Product_Code__c = '008-PROF-GENC' and OpportunityId IN :filteredOpportunities];
		
		 return ProServOlis;
	}
	
	private static Map<Id, List<OpportunityLineItem>> OpptoProServLineMap (List<OpportunityLineItem> ProServOlis)
	{
		Map<Id, List<OpportunityLineItem>> AcctIdtoProServLines = new Map<Id, List<OpportunityLineItem>>();
		
		
			for( OpportunityLineItem proServOli : ProServOlis ) 
			{
				if(!AcctIdtoProServLines.containsKey(proServOli.Account_Id__c) )
				{
					AcctIdtoProServLines.put(proServOli.Account_Id__c, new List<OpportunityLineItem>() );
				}
					AcctIdtoProServLines.get(proServOli.Account_Id__c).add(proServOli);
			}
			
		return AcctIdtoProServLines;	

	}*/
	
}