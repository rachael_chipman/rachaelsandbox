public class AddCRMToOppTeam {
	
	public static list<Opportunity> filterOpportunitiesWithCRM ( List<Opportunity> newOpps, Map<Id, Opportunity> oldOpps )
	{
		list<Opportunity> filteredOpps = new list<Opportunity>();
		
		for( Opportunity opp : newOpps )
		{
			Opportunity oldOpp;
			if( oldOpps != NULL )
			{
				oldOpp = oldOpps.get(opp.Id);
			}
			if(( (oldOpp == NULL && opp.CRM_User__c != NULL) || 
               (oldOpp != NULL && opp.CRM_User__c != oldOpp.CRM_User__c )) && 
               (opp.Type == 'New Business' || opp.Type =='Cross-Sell') && 
               !opp.Created_in_Portal__c && 
               opp.StageName != 'Finance Approved')
               
                
			{
				filteredOpps.add( opp );
			}
		}
		system.debug( 'What is in the filter (Add CRM to Opp Team)?'+ filteredOpps );
		return filteredOpps;
	}
	
	private static map<Id, User> getCRMUser ( list<Opportunity> filteredOpps )
	{
		map<Id, User> oppIdToCRMUser = new map<Id, User>();
		Set<Id> crmUserIds = pluck.ids( 'CRM_User__c', filteredOpps );
		
		list<User> crmUsers = [SELECT Id FROM User WHERE Id IN : crmUserIds];
		
		for( Opportunity opp : filteredOpps )
		{
			for( User crm : crmUsers )
			{
				if( opp.CRM_User__c == crm.Id )
				{
					oppIdToCRMUser.put( opp.Id, crm );
				}
			}
		}
		system.debug( 'What is in the oppIdToCRMUser map?' + oppIdToCRMUser );
		return oppIdToCRMUser;

	}
	
	
	private static map<Id, list<OpportunityTeamMember>> getOppTeamMembersToDelete (list<Opportunity> filteredOpps, map<Id, Opportunity> oldOpps)
	{
		map<Id, list<OpportunityTeamMember>> oppIdToMembersToDelete = new map<Id, list<OpportunityTeamMember>>();
		Set<Id> oldCRMIds = new Set<Id>();
		
		for(Opportunity opp : filteredOpps)
		{
			Opportunity oldOpp;
			if( oldOpps != NULL )
			{
				oldOpp = oldOpps.get( opp.Id );
				oldCRMIds.add( oldOpp.CRM_User__c );
			}
		}
		list<UserTeamMember> oldCRMsTeamMembers = [SELECT Id, UserId FROM UserTeamMember WHERE OwnerId IN : oldCRMIds];
        system.debug('How many oldCRMsTeamMembers'+oldCRMsTeamMembers.size());
		Set<Id> oldCRMTeamMemberUserIds = pluck.ids('UserId', oldCRMsTeamMembers);
        system.debug('How many old members'+oldCRMTeamMemberUserIds.size()); 
		list<User> users =[SELECT Id FROM User WHERE isActive = true AND Id IN: oldCRMTeamMemberUserIds];
        list<OpportunityTeamMember> teamMembers = [SELECT Id, UserId, OpportunityId FROM OpportunityTeamMember WHERE UserId IN : users and OpportunityId IN: pluck.ids(filteredOpps)];
        system.debug('What is in the list of team members'+teamMembers);
		
		for( OpportunityTeamMember memberToDelete : teamMembers )
		{
			if( !oppIdToMembersToDelete.containsKey( memberToDelete.OpportunityId ) )
			{
				oppIdToMembersToDelete.put( memberToDelete.OpportunityId , new list<OpportunityTeamMember>() );
			}
			oppIdToMembersToDelete.get( memberToDelete.OpportunityId ).add( memberToDelete );
			
		}
		system.debug( 'What is in the map of team members to delete?' + oppIdToMembersToDelete );
		return oppIdToMembersToDelete;
	}
	
	private static map<Id, list<UserTeamMember>> getUserTeamMembers( list<Opportunity> filteredOpps )
	{
		map<Id, list<UserTeamMember>> crmUserIdToUserTeamMembers = new map<Id, list<UserTeamMember>>();
		
		Set<Id> crmUserIds = pluck.ids( 'CRM_User__c', filteredOpps );

		list<UserTeamMember> crmTeamMembers = [SELECT Id, OwnerId, UserId, TeamMemberRole FROM UserTeamMember WHERE OwnerId IN : crmUserIds];
		
		for( Id crmId : crmUserIds )
		{
			crmUserIdToUserTeamMembers.put( crmId, new list<UserTeamMember>() );
				
				for( UserTeamMember teamMember : crmTeamMembers)
				{
					if( crmId == teamMember.OwnerId )
					{
						crmUserIdToUserTeamMembers.get(crmId).add( teamMember );
					}
				}
		}
		system.debug( 'What is in the crmUserIdToUserTeamMembers map'+ crmUserIdToUserTeamMembers );
		return crmUserIdToUserTeamMembers;
	}
	
	
	public static void processCRMsAndTeamMembers ( list<Opportunity> filteredOpps, list<Opportunity> newOpps, map<Id, Opportunity> oldOpps )
	{
		map<Id, User> opportunityIdToCRMUser = getCRMUser( filteredOpps );
		map<Id, list<UserTeamMember>> crmUserIdtoTeamMembers = getUserTeamMembers( filteredOpps );
		map<Id, list<OpportunityTeamMember>> opportunityIdToTeamMembersToDelete = getOppTeamMembersToDelete ( filteredOpps, oldOpps );
		list<OpportunityTeamMember> oppTeamMembersToInsert = new list<OpportunityTeamMember>();
		list<OpportunityTeamMember> oppTeamMembersToDelete = new list<OpportunityTeamMember>();
		
		for( Opportunity opp : filteredOpps )
		{
			if( !opportunityIdToCRMUser.isEmpty() && opportunityIdToCRMUser.containsKey( opp.Id ) )
			{
				OpportunityTeamMember crmMember = new OpportunityTeamMember();
				crmMember.UserId = opportunityIdToCRMUser.get(opp.Id).Id;
				crmMember.TeamMemberRole = 'Channel Relationship Manager';
				crmMember.OpportunityId = opp.Id;
				crmMember.CurrencyIsoCode = opp.CurrencyIsoCode;
                crmMember.Commission_Percentage__c = 100.00;        
               
				oppTeamMembersToInsert.add( crmMember );
			}
			
			if(  !crmUserIdtoTeamMembers.isEmpty() && crmUserIdtoTeamMembers.containsKey( opp.CRM_User__c ) )
			{
				for( UserTeamMember crmDefaultMember : crmUserIdtoTeamMembers.get( opp.CRM_User__c ) )
				{
					OpportunityTeamMember defaultMember = new OpportunityTeamMember();
					defaultMember.UserId = crmDefaultMember.UserId;
					defaultMember.TeamMemberRole = crmDefaultMember.TeamMemberRole;
					defaultMember.OpportunityId = opp.Id;
					defaultMember.CurrencyIsoCode = opp.CurrencyIsoCode;
					oppTeamMembersToInsert.add( defaultMember );
				}
			}
			if( !opportunityIdToTeamMembersToDelete.isEmpty() && opportunityIdToTeamMembersToDelete.containsKey( opp.Id ) )
			{
				oppTeamMembersToDelete.addAll( opportunityIdToTeamMembersToDelete.get( opp.Id ) );
			}
		}
		
		try
		{
			insert oppTeamMembersToInsert;
		}
		catch (DMLException e)
		{
			for(Integer i = 0; i < e.getNumDml(); i++)
			{
				System.debug(e.getDmlMessage(i));
				for( Opportunity triggerOpp : newOpps )
				{
					triggerOpp.addError( e.getDmlMessage( i ) );
				}
			}
		}
		try
		{
			delete oppTeamMembersToDelete;
		}
		catch (DMLException e)
		{
			for(Integer i = 0; i < e.getNumDml(); i++)
			{
				System.debug(e.getDmlMessage(i));
				for( Opportunity triggerOpp : newOpps )
				{
					triggerOpp.addError( e.getDmlMessage( i ) );
				}
			}
		}
		
	}
	
	public static void processTrigger( list<Opportunity> newOpps, map<Id, Opportunity> oldOpps )
	{
		list<Opportunity> filteredOpportunities = filterOpportunitiesWithCRM( newOpps, oldOpps );
		if( !filteredOpportunities.isEmpty() )
		{
			processCRMsAndTeamMembers( filteredOpportunities, newOpps, oldOpps );
		}
	}

}