public with sharing class JournalCreationService 
{
	public static String errors = '';
	public static final String MANUAL_JOURNAL = 'Manual Journal';
	public static final String LINE_TYPE_GENERAL_LEDGER = 'General Ledger Account';

    public static c2g__codaGeneralLedgerAccount__c DEFERRED_COMMISSION_GLA
    {
    	get
    	{
	    	if( DEFERRED_COMMISSION_GLA == NULL )
	    	{
	    		DEFERRED_COMMISSION_GLA = [SELECT Id, Name FROM c2g__codaGeneralLedgerAccount__c WHERE c2g__ReportingCode__c = '1350' LIMIT 1];
	    	}
	    	return DEFERRED_COMMISSION_GLA;
    	}
    	private set;
    }

    public static c2g__codaGeneralLedgerAccount__c COMMISSIONS_PARTNERS_GLA
    {
    	get
    	{
	    	if( COMMISSIONS_PARTNERS_GLA == NULL )
	    	{
	    		COMMISSIONS_PARTNERS_GLA = [SELECT Id, Name FROM c2g__codaGeneralLedgerAccount__c WHERE c2g__ReportingCode__c = '5000' LIMIT 1];
	    	}
	    	return COMMISSIONS_PARTNERS_GLA;
    	}
    	private set;
    }

    public static c2g__codaGeneralLedgerAccount__c COMMISSIONS_PAYABLE_REFERRAL_GLA
    {
    	get
    	{
	    	if( COMMISSIONS_PAYABLE_REFERRAL_GLA == NULL )
	    	{
	    		COMMISSIONS_PAYABLE_REFERRAL_GLA = [SELECT Id, Name FROM c2g__codaGeneralLedgerAccount__c WHERE c2g__ReportingCode__c = '2080' LIMIT 1];
	    	}
	    	return COMMISSIONS_PAYABLE_REFERRAL_GLA;
    	}
    	private set;
    }
    
    public static c2g__codaPeriod__c PERIOD_CURRENT
    {
        get
        {
            if( PERIOD_CURRENT == NULL )
            {

                 PERIOD_CURRENT = [SELECT Id, Name, c2g__YearName__c, c2g__StartDate__c FROM c2g__codaPeriod__c WHERE c2g__StartDate__c = :Date.today().toStartOfMonth()
                                  AND c2g__PeriodNumber__c != '000' LIMIT 1];
                
            }
            return PERIOD_CURRENT;
        }
        private set;
    }

    public static void createJournalsAndLineItems( Opportunity oppWithOLI, c2g__codaInvoice__c invoice )
	{
		List<c2g__codaJournal__c> journals = new List<c2g__codaJournal__c>(); 
		journals = createJournals( invoice, invoice.c2g__InvoiceCurrency__c );
		try
		{
			insert journals;
		}
		catch( System.DmlException ex )
		{
			for ( Integer index = 0 ; index < ex.getNumDml() ; index++ )
            {
                errors += ( ' ' + ex.getDmlMessage( index ) );
				System.assert( false, 'asdf errors ' + errors + journals[index] );

            }
            return;
		}
		
		List<c2g__codaJournalLineItem__c> journalLineItemsToBeInserted = new List<c2g__codaJournalLineItem__c>();
		Decimal oppExRate = invoice.Opportunity_Exchange_Rate__c != NULL ? invoice.Opportunity_Exchange_Rate__c : 1.0;
		Decimal partnerExRate = invoice.Partner_Exchange_Rate__c != NULL ? invoice.Partner_Exchange_Rate__c : 1.0 ;

		OpportunityLineItem relatedOLI = oppWithOLI.OpportunityLineItems[0];
		Date startDate = invoice.Contract_Service_Period_Start_Date__c;
		Date endDate = invoice.Contract_Service_Period_End_Date__c;

		journalLineItemsToBeInserted.addAll( createFullJournalLineItems( journals[0], ( invoice.Partner_Commission_Amount__c/oppExRate) * partnerExRate ) );
		journals.remove( 0 ); //Remove full journal
		Integer numOfMonthlyJournals = journals.size();

		List<Decimal> jliPrices = getJournalLineItemPrices( invoice.Partner_Commission_Amount__c, startDate, endDate, numOfMonthlyJournals, oppExRate, partnerExRate );
		for( Integer i = 0; i < numOfMonthlyJournals; i++ )
		{
			journalLineItemsToBeInserted.addAll( createMonthlyJournalLineItems( journals[i], jliPrices[i] ) );
		}
		try
		{
			insert journalLineItemsToBeInserted;
		}
		catch( System.DmlException ex )
		{
			for ( Integer index = 0 ; index < ex.getNumDml() ; index++ )
            {
                errors += ( ' ' + ex.getDmlMessage( index ) );
            }
            return;
		}
		

	}

	public static List<c2g__codaJournal__c> createJournals( c2g__codaInvoice__c invoice, Id accountingCurrencyId )
	{
		List<c2g__codaJournal__c> invoiceJournals = new List<c2g__codaJournal__c>();
		Date startDate = invoice.Contract_Service_Period_Start_Date__c;
		Date endDate = invoice.Contract_Service_Period_End_Date__c;

		c2g__codaJournal__c fullJournal = createJournal( invoice, startDate, accountingCurrencyId );
		invoiceJournals.add( fullJournal );
		Integer numOfJournals = getNumberOfMonthlyJournalsCount( startDate, endDate );
		List<c2g__codaJournal__c> monthlyJournals = new List<c2g__codaJournal__c>();
		for( Integer i = 0; i < numOfJournals; i++ )
		{
			Date journalDate;
			if( i == 0 )
			{
				journalDate = startDate;
			}
			else
			{
				journalDate = startDate.addMonths( i ).toStartOfMonth();
			}

			monthlyJournals.add( createJournal( invoice, journalDate, accountingCurrencyId ) );
		}
		invoiceJournals.addAll( monthlyJournals );
		return invoiceJournals;
	}

	public static List<c2g__codaJournalLineItem__c> createFullJournalLineItems( c2g__codaJournal__c journal, Decimal price )
	{
		return createJournalLineItems( journal, price, COMMISSIONS_PAYABLE_REFERRAL_GLA, DEFERRED_COMMISSION_GLA );
	}

	public static List<c2g__codaJournalLineItem__c> createMonthlyJournalLineItems( c2g__codaJournal__c journal, Decimal price )
	{
		return createJournalLineItems( journal, price, DEFERRED_COMMISSION_GLA, COMMISSIONS_PARTNERS_GLA );
	}

	private static List<c2g__codaJournalLineItem__c> createJournalLineItems( c2g__codaJournal__c journal, Decimal price, c2g__codaGeneralLedgerAccount__c generalLedgerAccount1, c2g__codaGeneralLedgerAccount__c generalLedgerAccount2)
	{
		List<c2g__codaJournalLineItem__c> journalLineItems = new List<c2g__codaJournalLineItem__c>();
		journalLineItems.add( new c2g__codaJournalLineItem__c(
        c2g__LineType__c = LINE_TYPE_GENERAL_LEDGER,
        //CurrencyIsoCode = o.CurrencyIsoCode,
        c2g__GeneralLedgerAccount__c = generalLedgerAccount1.Id,
        c2g__Journal__c = journal.Id,
        c2g__Value__c = price.SetScale(2)*(-1)));

        journalLineItems.add(new c2g__codaJournalLineItem__c(
        c2g__LineType__c = LINE_TYPE_GENERAL_LEDGER,
        //CurrencyIsoCode = o.CurrencyIsoCode,
        c2g__GeneralLedgerAccount__c = generalLedgerAccount2.Id,
        c2g__Journal__c = journal.Id,
        c2g__Value__c = price.SetScale(2)));

        return journalLineItems;
	}

	public static Integer getNumberOfMonthlyJournalsCount( Date startDate, Date endDate )
	{
		if( startDate.year() == endDate.year() )
		{
			return ( endDate.month() - startDate.month() ) + 1;
		}
		else
		{
			return ( 12 - startDate.month() ) + 1 + endDate.month();
		}
	}

	private static c2g__codaJournal__c createJournal( c2g__codaInvoice__c invoice, Date journalDate, Id accountingCurrencyId )
	{
		c2g__codaJournal__c newJournal = new c2g__codaJournal__c(
	    c2g__Type__c = 	MANUAL_JOURNAL,
	    Sales_Invoice__c = invoice.Id,
	    c2g__JournalDate__c = journalDate,
	    c2g__JournalCurrency__c = accountingCurrencyId,
	    c2g__Reference__c = '',
	    Opportunity__c = invoice.c2g__Opportunity__c,
	    ffgl__DeriveCurrency__c = false,
	    c2g__JournalDescription__c = '');
		Date currentDate = Date.today();
	    if( journalDate.year() < currentDate.year() || ( journalDate.year() == currentDate.year() && journalDate.month() < currentDate.month() ) )
	    {
	    	newJournal.ffgl__DerivePeriod__c = false;
	    	newJournal.c2g__Period__c = PERIOD_CURRENT.Id;
	    	newJournal.c2g__JournalDate__c = PERIOD_CURRENT.c2g__StartDate__c;
	    }
		return newJournal;
	}

	public static List<Decimal> getJournalLineItemPrices( Decimal oppPartnerCommissionAmount, Date startDate, Date endDate, Integer numOfJournals, Decimal oppExRate, Decimal partnerExRate )
	{
		List<Decimal> prices = new List<Decimal>();
		Decimal partnerCommissionAmount = ( oppPartnerCommissionAmount/oppExRate ) * partnerExRate;
		Decimal priceperday = partnerCommissionAmount/( startDate.daysBetween( endDate ) + 1);

		if( numOfJournals == 1 )
		{
			prices.add( partnerCommissionAmount );
			return prices;
		}
		Decimal firstJLIPrice = calculatePriceForDays( startDate, startDate.addMonths( 1 ).toStartOfMonth().addDays( -1 ), priceperday );
		prices.add( firstJLIPrice );
		for( Integer i = 1; i < numOfJournals-1; i++ )
		{
            prices.add( calculatePriceForCompletePeriod( startDate.addMonths( i ), priceperday ) );
		}
		Decimal lastJLIPrice = calculatePriceForDays( startDate.addMonths( numOfJournals - 1 ).toStartOfMonth(), endDate, priceperday );
		prices.add( lastJLIPrice );

		return prices;
	}

	public static Decimal calculatePriceForCompletePeriod( Date aDate, Decimal priceperday )
    {
        return ( Date.daysInMonth( aDate.year(), aDate.month() ) * pricePerDay ).setScale( 2 );
    }

    public static Decimal calculatePriceForDays( Date startDate, Date endDate, Decimal priceperday )
    {
        return( ( ( startDate.DaysBetween( endDate ) + 1 ) * pricePerDay ).setScale( 2 ) );
    }

}