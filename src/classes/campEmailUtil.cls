/* Written by Adam Bryant, April 2012 for Return Path
*
* This is a utility class that accepts email messages from a trigger on the CampaignMember object,
* and sends them at once. This allows to get around the 10 email invocation governor limit.
*
*/

global class campEmailUtil { 

	// CampaignMember trigger will call this method to get its messages into our class
	public static Messaging.Email[] mailArray = new List<Messaging.Email>();

	// We need to loop through the list of emails and put them in a new list, in order to send them as one. The method below uses this.
	public static Messaging.Email[] allMail = new List<Messaging.Email>();

    // Trigger calls this method at the end of its for loop, in order to actually send all of the messages its processed.
	public static void sendCampEmail() {      

		for (Integer i = 0; i < mailArray.size(); i++) {
			allMail.add(mailArray.get(i));
		}

		Messaging.SendEmailResult[] results = Messaging.sendEmail(allMail);
	}	
 
}