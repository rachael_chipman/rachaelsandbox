public without sharing class updateAccountOwnerfromRM {
	
	public static void processTrigger(List<Account> newAccounts)
	{
		List<Account> filteredAccounts = filterAccountsOnUpdate(newAccounts);
		if(!filteredAccounts.isEmpty())
		{
			processAccounts(filteredAccounts);
		}
	}
	
	public static void processAccounts(List<Account> filteredAccounts)
	{		
		for(Account acc : filteredAccounts)
		{
			acc.OwnerId = acc.AM__c;
		}
	}
	
	public static List<Account> filterAccountsOnUpdate(List<Account> newAccounts)
	{
		List<Account> filteredAccounts = new List<Account>();
		
		for(Account acc : newAccounts)
		{
			if(acc.OwnerId != acc.AM__c && acc.AM__c !=null)
			{
				filteredAccounts.add(acc);
			}
		}
		return filteredAccounts;
	}
}