@isTest(seeAllData=true)
private class TestCloneOpportunity 
{
    final static String OLD_VALUE = '0ld V47ue';
    
    final static Integer NUM_OF_OLIS = 3;
    
    static Opportunity testOpportunity;
    static List<OpportunityLineItem> testOlis;
    
    static void setupOpportunity()
    {
    	Account testAccount = TestUtils.createAccount( TestUtils.MARKETER_RECORD_TYPE.Id, true );
    	testOpportunity = TestUtils.createOpportunity( TestUtils.PURCHASE_RECORD_TYPE.Id, testAccount.Id, false );
    	
    	testOpportunity.StageName = OLD_VALUE;
    	testOpportunity.Run_ACV__c = OLD_VALUE;
    	testOpportunity.Send_Monkey_Email__c = OLD_VALUE;
    	testOpportunity.Approval_Type__c = OLD_VALUE;
    	testOpportunity.RD_Pricing_Review__c = OLD_VALUE;
    	testOpportunity.Certification_Team_Owner__c = TestUtils.ADMIN_USER.id;

    	testOpportunity.Invoice_Created__c = true;
    	
    	insert testOpportunity;
    	
    	testOpportunity.Alternate_Language__c = OLD_VALUE; //This value is used for querying since we can't used the Name field and don't have access to the Id
    	
    	upsert testOpportunity;
    }
    
    static void setupOpportunityLineItems()
    {
        testOlis = TestUtils.createOpportunityLineItems( testOpportunity.Id, NUM_OF_OLIS, true);
    }
    
    static testMethod void testCloneOpportunityWithProducts() 
    {
        //Setup opportunity and related line items
        setupOpportunity();
        setupOpportunityLineItems();
        
        //Set the test page
        PageReference currentPage = Page.CloneOpportunityWithProducts;
        Test.setCurrentPage(currentPage);
        PageReference actualRef;
        
        
        Test.startTest();
            //Instantiate the controller and execute the clone method (with products)
            ApexPages.StandardController con = new ApexPages.StandardController( testOpportunity );
            
            CloneOpportunityContExt ext = new CloneOpportunityContExt( con );
            
            actualRef = ext.cloneOpportunityWithProducts();
        Test.stopTest();
        
        //Query for the cloned opportunity. We query for an opportunity that does not have the 
        //same Id as the original but with the same Alternate_Language__c field 
        //(we gave that field a unique enough value that only the desired clone will be returned)
        Opportunity actualClonedOpp  = [SELECT  Id, 
                                                StageName,
                                                RecordTypeId, 
                                                Run_ACV__c,
                                                Send_Monkey_Email__c,
                                                Approval_Type__c,
                                                RD_Pricing_Review__c,
                                                Reporting_Date__c,
                                                Certification_Team_Owner__c,
                                                Legal_Approval_Status__c,
                                                Contract__c,
                                                Previous_Opportunity_Lookup__c,
                                                Invoice_Created__c,
                                                ( SELECT Id FROM OpportunityLineItems )
                                             FROM Opportunity
                                             WHERE Alternate_Language__c = :OLD_VALUE AND Id <> :testOpportunity.Id];
        //Creating a pagereference of where we expect the controller method will direct us
        PageReference expectedRef = new PageReference('/' + actualClonedOpp.Id + '/e' );
        expectedRef.getParameters().put(CloneOpportunityContExt.RET_URL, actualClonedOpp.Id);
        
        //Assert that all of the fields on the clone are set to what we specified (as defined in the controller)
        System.assertEquals(CloneOpportunityContExt.CLONED_STAGE, actualClonedOpp.StageName, 'The cloned opportunity should have been updated to the default stage that is different than the original opportunity\'s' );
	System.assertEquals(TestUtils.SELECT_RECORD_TYPE.Id, actualClonedOpp.RecordTypeId, 'The cloned opportunity\'s record type should have been updated to a default that is different than the original opportunity\'s' );
        System.assertEquals(CloneOpportunityContExt.CLONED_APPROVAL_TYPE, actualClonedOpp.Approval_Type__c, 'The cloned opportunity should have been updated to the default approval type that is different than the original opportunity\'s' );
        System.assertEquals(NULL, actualClonedOpp.Run_ACV__c, 'The cloned opportunity should not have cloned the Run ACV' );
        System.assertEquals(NULL, actualClonedOpp.Send_Monkey_Email__c, 'The cloned opportunity should not have cloned the Send Monkey Email' );
        System.assertEquals(NULL, actualClonedOpp.RD_Pricing_Review__c, 'The cloned opportunity should not have cloned the RD Pricing Review' );
        System.assertEquals(NULL, actualClonedOpp.Reporting_Date__c, 'The cloned opportunity should not have cloned the Reporting_Date__c' );
        System.assertEquals(NULL, actualClonedOpp.Certification_Team_Owner__c, 'The cloned opportunity should not have cloned the Certification Team Owner' );
        System.assertEquals(NULL, actualClonedOpp.Contract__c, 'The cloned opportunity should not have cloned the Contract Lookup' );
        System.assertEquals(NULL, actualClonedOpp.Previous_Opportunity_Lookup__c, 'The cloned opportunity should not have cloned the previous opportunity field' );
        System.assertEquals(false, actualClonedOpp.Invoice_Created__c, 'The cloned opportunity should not have cloned the previous opportunity field' );
        //Assert that the olis were cloned as well
        System.assertEquals( NUM_OF_OLIS, actualClonedOpp.OpportunityLineItems.size(), 'All of the related opportunity line items should have been cloned as well' );
        
        System.assertEquals( actualRef.getUrl(), expectedRef.getUrl(), 'The returned page reference should point to the cloned opportunity detail page' );
    }
    static testMethod void testCloneOpportunityWithoutProducts() 
    {
        //setup only the opportunity 
        setupOpportunity();
        
        //setup the test page
        PageReference currentPage = Page.CloneOpportunityWithoutProducts;
        Test.setCurrentPage(currentPage);
        PageReference actualRef;
        
        Test.startTest();
            //Instantiate the controller and execute the clone method (with products)
            ApexPages.StandardController con = new ApexPages.StandardController( testOpportunity );
            
            CloneOpportunityContExt ext = new CloneOpportunityContExt( con );
            
            actualRef = ext.cloneOpportunityWithoutProducts();
        Test.stopTest();
    
        //Query for the cloned opportunity. We query for an opportunity that does not have the 
        //same Id as the original but with the same Alternate_Language__c field 
        //(we gave that field a unique enough value that only the desired clone will be returned)
        Opportunity actualClonedOpp  = [SELECT  Id, 
                                                StageName,
                                                RecordTypeId, 
                                                Run_ACV__c,
                                                Send_Monkey_Email__c,
                                                Approval_Type__c,
                                                RD_Pricing_Review__c,
                                                Reporting_Date__c,
                                                Certification_Team_Owner__c,
                                                Legal_Approval_Status__c,
                                                Contract__c,
                                                Previous_Opportunity_Lookup__c,
                                                ( SELECT Id FROM OpportunityLineItems )
                                             FROM Opportunity
                                             WHERE Alternate_Language__c = :OLD_VALUE AND Id <> :testOpportunity.Id];
        
        //Creating a pagereference of where we expect the controller method will direct us
        PageReference expectedRef = new PageReference('/' + actualClonedOpp.Id + '/e' );
        expectedRef.getParameters().put(CloneOpportunityContExt.RET_URL, actualClonedOpp.Id);
        
        //Assert that all of the fields on the clone are set to what we specified (as defined in the controller)
        System.assertEquals(CloneOpportunityContExt.CLONED_STAGE, actualClonedOpp.StageName, 'The cloned opportunity should have been updated to the default stage that is different than the original opportunity\'s' );
	System.assertEquals(TestUtils.SELECT_RECORD_TYPE.Id, actualClonedOpp.RecordTypeId, 'The cloned opportunity\'s record type should have been updated to a default that is different than the original opportunity\'s' );
        System.assertEquals(CloneOpportunityContExt.CLONED_APPROVAL_TYPE, actualClonedOpp.Approval_Type__c, 'The cloned opportunity should have been updated to the default approval type that is different than the original opportunity\'s' );
        System.assertEquals(NULL, actualClonedOpp.Run_ACV__c, 'The cloned opportunity should not have cloned the Run ACV' );
        System.assertEquals(NULL, actualClonedOpp.Send_Monkey_Email__c, 'The cloned opportunity should not have cloned the Send Monkey Email' );
        System.assertEquals(NULL, actualClonedOpp.RD_Pricing_Review__c, 'The cloned opportunity should not have cloned the RD Pricing Review' );
        System.assertEquals(NULL, actualClonedOpp.Reporting_Date__c, 'The cloned opportunity should not have cloned the Reporting_Date__c' );
        System.assertEquals(NULL, actualClonedOpp.Certification_Team_Owner__c, 'The cloned opportunity should not have cloned the Certification Team Owner' );
        System.assertEquals(NULL, actualClonedOpp.Contract__c, 'The cloned opportunity should not have cloned the Contract Lookup' );
        System.assertEquals(NULL, actualClonedOpp.Previous_Opportunity_Lookup__c, 'The cloned opportunity should not have cloned the previous opportunity field' );
        
        //Assert that there are still no OLIS
        System.assertEquals( 0, actualClonedOpp.OpportunityLineItems.size(), 'None of the related opportunity line items should have been cloned' );
        
        System.assertEquals( actualRef.getUrl(), expectedRef.getUrl(), 'The returned page reference should point to the cloned opportunity detail page' );
    }
}