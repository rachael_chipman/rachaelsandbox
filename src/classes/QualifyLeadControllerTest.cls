@isTest
private class QualifyLeadControllerTest {  
    
    static Lead testLead; 
    static PageReference pageRef;
    static QualifyLeadController con;
    static ApexPages.StandardController SC;
    static QualifyLeadController con2;
    static ApexPages.StandardController SC2; 
    
    static void setup() 
    {
        system.runAs(TestUtils.ADMIN_USER)
        {
            testLead = TestUtils.createLeads('TestCompany', 'TestLead', 'Pending Assignment', true);
            List<Lead> queryLeads = [SELECT Id, Owner.Name FROM Lead];
            system.debug('What is the Owner Name #1'+ queryLeads[0].Owner.Name);
            system.debug('What is the OwnerId'+ queryLeads[0].Owner.Id);
            testLead.First_Campaign__c = 'Some Campaign';
            testLead.LeadSource = 'Source';
            testLead.Status = 'SDR Accept';
            testLead.Email = 'testemail@test.com';
            update testLead;
            system.debug('Show me the setup Lead email' + testLead.Email);
            system.debug('Show me the setup Lead Domain' + testLead.Domain__c);
            List<Lead> queryLeads2 = [SELECT Id, Owner.Name FROM Lead];
            system.debug('What is the Owner Name #2'+ queryLeads2[0].Owner.Name);
        }
         
        //testLead.OwnerId = TestUtils.ADMIN_USER.Id;
        
        pageRef = Page.QualifyLeadMergeLeads;
        pageRef.getParameters().put('Id', String.valueOf(testLead.Id));
        Test.setCurrentPage(pageRef);
        SC = new Apexpages.StandardController(testLead);
        con = new QualifyLeadController(SC);
        con.qLeadId = testlead.Id;
        con.qLead = [SELECT Id, Name, Company,Original_SDR__c, Email, Domain__c, Pod_Name__c, OwnerId,
                 Owner.Name,Lead_Source_Original_Time_Date_Stamp__c, isConverted, Opportunity_Source__c, 
                 Status, For_RSE__c, First_Campaign__c, LeadSource, Lead_Asset_Original__c, What_problems_have_you_experienced__c,
                 Dedicated_IP__c, Volume_of_Emails_sent_per_month__c, Business_primarily_sells_to_B2B_B2C__c, Role_in_Purchasing__c,
                 What_ISPs_are_Problematic__c, Willing_to_speak_to_Sales_Rep__c, Most_Recent_Lead_Source__c, Goals__c, SDR_Background__c,
                 Business_Objectives_Pain_Points__c, Campaign_Name__c FROM Lead WHERE Id = :testLead.Id];
        //con.domain = testLead.Domain__c;
    }

    
    static testMethod void testQualifyLeadbutton()
    {
        //call the setup method
        setup();
        
       Lead conLead = [Select Id, Name, Email, Company from Lead where Id =:con.qLeadId][0];
       testLead = [Select Id, Name, Email, Company from Lead where Id =:testLead.Id];
       //Assert that the values are what we expected.
       system.assertEquals(conLead.Id, testLead.Id,'We expect the Lead Ids to be same');
       System.assertEquals(conLead.Name, testLead.Name, 'We expect Lead Names to be Same');
       System.assert(con.stepcounter == 1, 'Step 1 of the process');
    }
    
    static testMethod void testCancelButton()
    {
        //call the setup method
        setup();
        String cancelPageUrl =  con.cancelQualify().getUrl();
        System.assertEquals(cancelPageUrl,'/apex/Lead_Tab_View?id='+ testLead.Id);
    }
    
    static testMethod void testsearchMatchingLeads()
    {
        //call the setup method
        setup();
        //insert new test leads.
        Lead TLead2  = TestUtils.createLeads('TestCompany', 'TestLead2', 'Open', true);
        TLead2.Email = 'test1@test.com';
        update TLead2;
        Lead TLead3  = TestUtils.createLeads('TestCompany', 'TestLead3', 'Open', true);
        TLead3.Email = 'test2@test.com';
        update TLead3;
        con.searchLeads();
        system.assertEquals(con.matchingLeads.size() , con.matchingLeads.size(),' size is greate than zero');
    }
    
    static testMethod void testsearchNOMatchingLeads()
    {
        //call the setup method
        setup();
        con.searchLeads();
        system.assert(con.matchingLeads.size() == 0 , ' No Matching leads');
    }
    
    static testMethod void testMergeMatchingLeads()
    {
        //call the setup method
        setup();
        system.debug('Now tell me what qleads domain is' + con.qLead.Email + con.qLead.Domain__c);
        //insert new test leads.
        Lead TLead2  = TestUtils.createLeads('TestCompany', 'TestLead2', 'Open', true);
        TLead2.Email = 'test1@test.com';
        update TLead2;
        Lead TLead3  = TestUtils.createLeads('TestCompany', 'TestLead3', 'Open', true);
        TLead3.Email = 'test2@test.com';
        update TLead3;
        system.debug('Show me the TLead2 Email'+ TLead2.Email);
        system.debug('What is the domain' + TLead2.Domain__c);
        List<Lead> testLeadsWithDomain = [SELECT Id, Email, Domain__c FROM Lead];
        for(Lead aLead : testLeadsWithDomain)
        {
            system.debug('Show me the domain for each lead'+ aLead.Domain__c);
            system.debug('Show me the email for each lead'+ aLead.Email);
        }
        con.searchLeads();
        Integer oldleadstomerge = con.matchingLeads.size();
        pageRef.getParameters().put('merge', String.valueOf(con.matchingLeads[0].aLead.Id));
        //con.matchingLeads[0].isSelected = true;
        con.mergeSelectedLeads();
        //search for matching leads again. there should be one lead left
        con.searchLeads();
        system.assert(oldleadstomerge > con.matchingLeads.size(), 'one Lead is less as its merged');
    }
    
    static testMethod void testsearchMatchingaccountsNegative()
    {
        //call the setup method
        setup();
        //insert new test leads.
        Lead TLead2  = TestUtils.createLeads('TestCompany 11', 'TestLead2', 'Open', true);
        Lead TLead3  = TestUtils.createLeads('TestCompany 1', 'TestLead3', 'Open', true);
        con.searchExistingAccounts();
        system.debug('Exsisting acc are :' + con.existingAccounts.size());
        System.assertEquals(0, con.existingAccounts.size(), 'No Matching accounts were found');
    }
    
    static testMethod void testsearchMatchingaccountsPositive()//This is still a negative test, need to set search criteria
    {
        //call the setup method
        setup();
        //insert new test leads.
        Lead TLead2  = TestUtils.createLeads('TestCompany 11', 'TestLead2', 'Open', true);
        Lead TLead3  = TestUtils.createLeads('TestCompany 1', 'TestLead3', 'Open', true);
        con.searchExistingAccounts();
        system.debug('Exsisting acc are :' + con.existingAccounts.size());
        System.assertEquals(0, con.existingAccounts.size(), 'No Matching accounts were found');
    }
    
    static testMethod void testgetStepCounterValue()
    {
        //call the setup method
        setup();        
        PageReference tpage = con.next();
        Test.setCurrentPage(tpage);
        String counterValue = con.getStepCounterValue();
        
        system.assertEquals('2', counterValue, 'We expect to be on step 2');

    }
    
    static testMethod void testNext()
    {
        //call the setup method
        setup();
        //insert new test leads.
        Lead TLead2  = TestUtils.createLeads('TestCompany', 'TestLead2', 'Open', true);
        TLead2.Email = 'test1@test.com';
        update TLead2;
        Lead TLead3  = TestUtils.createLeads('TestCompany', 'TestLead3', 'Open', true);
        TLead3.Email = 'test2@test.com';
        update TLead3;
        //call search Leads method (so that inserted leads are added to wrapper class)
        con.searchLeads();
        system.assertEquals(con.matchingLeads.size() , con.matchingLeads.size(),' size is greater than zero');
        //set first indexed lead isSelected boolean to true
        con.matchingLeads[0].isSelected = true;
        //call next method
        con.next();
        
        system.assertEquals(1, con.additionalLeadsToConvert.size(), 'We expect there to be one additional lead to convert');
        system.assertEquals(false, con.searchActive, 'searchActive variable should be set to false');
        system.assertEquals(con.matchingLeads[0].aLead.Id, Id.valueOf(con.leadIdsToConvert), 'We expect the selected lead id to be part of our string.');    
    }
    
    private static testMethod void testUpdateSelectedAccounts()
    {
        setup();
        List<Lead> queryLeads3 = [SELECT Id, Owner.Name FROM Lead];
        system.debug('What is the Owner Name #3'+ queryLeads3[0].Owner.Name);
        //insert test accounts so that search will retrieve matches
        list<Account> testAccts = TestUtils.createAccounts(3, TestUtils.MARKETER_RECORD_TYPE.Id, false);
        list<Account> insertedAccounts = new list<Account>();
        for(Account testAcct : testAccts)
        {
            testAcct.Name = 'TestCompany';
            insertedAccounts.add(testAcct);
        }
        insert insertedAccounts;
        system.assertNotEquals(0, insertedAccounts.size(), 'There should be 3 inserted accounts');
        list<Id> accountIds = new list<Id>();
        for(Account testAccount : insertedAccounts)
        {
            accountIds.add(testAccount.Id);
        }
        Test.setFixedSearchResults(accountIds);
        //call search method
        system.assertEquals('TestCompany', con.qLead.Company, 'The company name should be TestCompany');
        con.searchExistingAccounts();
        //ssystem.assertEquals(con.qLead.Company, con.companyName, 'The lead company name should be the qLead company name');
        system.assertNotEquals(0 , con.existingAccounts.size(),' size is greater than zero');
        //set first indexed account isForUpdate boolean to true
        con.existingAccounts[0].isForUpdate = true;
        //call update method so that the selected account will be updated
        con.updateSelectedAccounts();
        
        //set account variable to be equal to the first indexed account
        Account updatedAccount = con.existingAccounts[0].anAcct;
        
        //query for updated accounts
        list<Account> updatedAccounts = [SELECT Id, Escalate_for_Review__c, Escalated_At__c, Escalated_By__c, Escalation_Comment__c
                          FROM Account
                          WHERE Id = :updatedAccount.Id];

        List<Lead> qLeads = [SELECT Id, Owner.Name FROM Lead WHERE Id = :con.qLead.Id];
        
        system.assertEquals(true, updatedAccounts[0].Escalate_for_Review__c, 'We expect the account to be escalated for review');
        system.assertEquals(system.today(), updatedAccounts[0].Escalated_At__c, 'We expect the account to have an escalated at value');
        system.assertEquals(UserInfo.getName(), updatedAccounts[0].Escalated_By__c, 'We expect the lead owner to be the person who escalated the account');
        system.assertEquals('Account should potentially be merged. Search for '+ con.qLead.Company, updatedAccounts[0].Escalation_Comment__c, 'We expect the updated account to have an escalation comment.'); 
    }

    private static testMethod void testNext2()
    {
        setup();
        //insert test accounts so that we can ensure a selected account makes it into the URL
        list<Account> testAccounts = TestUtils.createAccounts(3, TestUtils.MARKETER_RECORD_TYPE.Id, false);
        list<Account> insertedAccounts = new list<Account>();
        for(Account testAcct : testAccounts)
        {
            testAcct.Name = 'TestCompany';
            insertedAccounts.add(testAcct);
        }
        insert insertedAccounts;
        system.assertNotEquals(0, insertedAccounts.size(), 'There should be 3 inserted accounts');
        list<Id> accountIds = new list<Id>();
        for(Account testAccount : insertedAccounts)
        {
            accountIds.add(testAccount.Id);
        }
        Test.setFixedSearchResults(accountIds);
        //call search method
        system.assertEquals('TestCompany', con.qLead.Company, 'The company name should be TestCompany');
        con.searchExistingAccounts();
        system.assertNotEquals(0, con.existingAccounts.size(), 'Existing accounts size is greater than 0');
        //Select the first indexed existing account for conversion
        con.existingAccounts[0].isSelected = true;
        //call method we are testing
        con.next2();
        PageReference tpage = con.next2();
        Test.setCurrentPage(tpage);
        String strurl = ApexPages.currentPage().getUrl();
        String strurlName = strurl.split('apex/')[1];
        system.assertEquals(con.existingAccounts[0].anAcct.Id, ApexPages.currentPage().getParameters().get('acctId'),'We expect the URL to contain the Id of the account we selected');
        system.assertEquals('QualifyLeadStep2?acctId='+con.existingAccounts[0].anAcct.Id+'&leadId='+con.qLead.Id+'&stepCounter=3', strurlName, 'We expect to be on the correct page');
    }

    private static testMethod void testSkipThisStep()
    {
        setup();
        con.skipThisStep();
        PageReference tpage = con.skipThisStep();
        Test.setCurrentPage(tpage);
        String strurl = ApexPages.currentPage().getUrl();
        String strurlName = strurl.split('apex/')[1];
        system.assertEquals(con.qLead.Id, ApexPages.currentPage().getParameters().get('leadId'),'We expect the URL to contain the Id of the lead we are working with');
        system.assertEquals('QualifyLeadStep3?leadId='+con.qLead.Id+'&stepCounter=3', strurlName, 'We expect to be on the correct page');

    }
    
    private static testMethod void testGoBackOneStep()//The step counter should never be negative...check this test RC$
    {
        setup();
        con.goBackOneStep();
        PageReference tpage = con.goBackOneStep();
        Test.setCurrentPage(tpage);
        String strurl = ApexPages.currentPage().getUrl();
        String strurlName = strurl.split('apex/')[1];
        system.assertEquals(con.qLead.Id, ApexPages.currentPage().getParameters().get('id'),'We expect the URL to contain the Id of the lead we are working with');
        system.assertEquals('QualifyLead?id='+con.qLead.Id+'&stepCounter=-1', strurlName, 'We expect to be on the correct page');
    }

    private static testMethod void testSearchMatchingContacts_positive()
    {
        setup();
        Account testExistingAccount = TestUtils.createAccount(TestUtils.MARKETER_RECORD_TYPE.Id, false);
        testExistingAccount.Name = 'Test Company Inc.';
        insert testExistingAccount;
        Contact testExistingContact = TestUtils.createContacts('Rachael', 'Chipman', testExistingAccount.Id, false);
        testExistingContact.Email = 'testemail@test.com';
        insert testExistingContact;

        //Because we are using SOSL queries we must pass a list of our Ids to the fixedSearchResults method
         Id [] fixedSearchResults= new Id[1];
        fixedSearchResults[0] = testExistingAccount.Id;
        Test.setFixedSearchResults(fixedSearchResults);
        //search for existing accoutns and verify the search returns the account we inserted
        con.searchExistingAccounts();        
        system.assertEquals(1, con.existingAccounts.size(), 'We expect there to be one account returned in our search.');
        //select this account for conversion so that we have the Id in the URL and go to the next step to search for contacts
        con.existingAccounts[0].isSelected = true;
        con.next2();
        //assert that the account Id is in the URL
        Test.setCurrentPage(con.next2());
        system.assertEquals(con.existingAccounts[0].anAcct.Id, ApexPages.currentPage().getParameters().get('acctId'), 'We expect our account Id to be in the URL');
        
        //Because we are using SOSL queries we must pass a list of our Ids to the fixedSearchResults method
         Id [] fixedSearchResults2= new Id[1];
        fixedSearchResults2[0] = testExistingContact.Id;
        Test.setFixedSearchResults(fixedSearchResults2);
        //Trying to reinstantiate the controller so that it picks up the selected account
        SC2 = new Apexpages.StandardController(con.qLead);
        con2 = new QualifyLeadController(SC);
        //search for existing contacts and verify that the search returns the contact that we inserted
        con2.searchMatchingContacts();
        system.assertEquals(1, con2.matchingContacts.size(), 'We expect there to be one contact returned in our search.');

    }

    private static testMethod void testSearchMatchingContacts_negative()
    {
        setup();
        Account testExistingAccount = TestUtils.createAccount(TestUtils.MARKETER_RECORD_TYPE.Id, false);
        testExistingAccount.Name = 'Test Company Inc.';
        insert testExistingAccount;
        
        //Because we are using SOSL queries we must pass a list of our Ids to the fixedSearchResults method
         Id [] fixedSearchResults= new Id[1];
        fixedSearchResults[0] = testExistingAccount.Id;
        Test.setFixedSearchResults(fixedSearchResults);
        //search for existing accoutns and verify the search returns the account we inserted
        con.searchExistingAccounts();        
        system.assertEquals(1, con.existingAccounts.size(), 'We expect there to be one account returned in our search.');
        //select this account for conversion so that we have the Id in the URL and go to the next step to search for contacts
        con.existingAccounts[0].isSelected = true;
        con.next2();
        //assert that the account Id is in the URL
        Test.setCurrentPage(con.next2());
        system.assertEquals(con.existingAccounts[0].anAcct.Id, ApexPages.currentPage().getParameters().get('acctId'), 'We expect our account Id to be in the URL'); 

        //Trying to reinstantiate the controller so that it picks up the selected account
        SC2 = new Apexpages.StandardController(con.qLead);
        con2 = new QualifyLeadController(SC);
        //search for existing contacts and verify that the search returns the contact that we inserted
        con2.searchMatchingContacts();
        system.assertEquals(0, con2.matchingContacts.size(), 'We do not expect there to be any contacts returned in our search.');
        //Check that if there are no matching contacts that we are displaying the message to continue
        List<ApexPages.Message> msgList = ApexPages.getMessages();
        for(ApexPages.Message msg : msgList)
        {
            system.assertEquals('There are no matching Contacts, please click the Skip this Step button.', msg.getSummary(), 'Checking that we have the correct page message.');
            system.assertEquals(ApexPages.Severity.INFO, msg.getSeverity(), 'We expect there to be an Info Message.');
        }  
    }

    private static testMethod void testUpdateExistingContacts()
    {
        setup();
        Account testExistingAccount = TestUtils.createAccount(TestUtils.MARKETER_RECORD_TYPE.Id, false);
        testExistingAccount.Name = 'Test Company Inc.';
        insert testExistingAccount;
        Contact testExistingContact = TestUtils.createContacts('Rachael', 'Chipman', testExistingAccount.Id, false);
        testExistingContact.Email = 'testemail@test.com';
        insert testExistingContact;

        //Because we are using SOSL queries we must pass a list of our Ids to the fixedSearchResults method
         Id [] fixedSearchResults= new Id[1];
        fixedSearchResults[0] = testExistingAccount.Id;
        Test.setFixedSearchResults(fixedSearchResults);
        //search for existing accoutns and verify the search returns the account we inserted
        con.searchExistingAccounts();        
        system.assertEquals(1, con.existingAccounts.size(), 'We expect there to be one account returned in our search.');
        //select this account for conversion so that we have the Id in the URL and go to the next step to search for contacts
        con.existingAccounts[0].isSelected = true;
        con.next2();
        //assert that the account Id is in the URL
        Test.setCurrentPage(con.next2());
        system.assertEquals(con.existingAccounts[0].anAcct.Id, ApexPages.currentPage().getParameters().get('acctId'), 'We expect our account Id to be in the URL');
        
        //Because we are using SOSL queries we must pass a list of our Ids to the fixedSearchResults method
         Id [] fixedSearchResults2= new Id[1];
        fixedSearchResults2[0] = testExistingContact.Id;
        Test.setFixedSearchResults(fixedSearchResults2);
        //Trying to reinstantiate the controller so that it picks up the selected account
        SC2 = new Apexpages.StandardController(con.qLead);
        con2 = new QualifyLeadController(SC);
        //search for existing contacts and verify that the search returns the contact that we inserted
        con2.searchMatchingContacts();
        system.assertEquals(1, con2.matchingContacts.size(), 'We expect there to be one contact returned in our search.');
        //Set wrapper class variable isForUpdate
        con2.matchingContacts[0].isForUpdate = true;
        //Call method to update the contact
        test.startTest();
        con2.updateSelectedContacts();
        test.stopTest();
        //Assert that the updates have completed
        Contact testCon = [SELECT Id, Escalate_for_Sales_Ops_Review__c, Escalated_At__c, Escalated_By__c, Escalation_Comment__c FROM Contact WHERE Id = :con2.matchingContacts[0].aContact.Id];
        system.assertEquals(true, testCon.Escalate_for_Sales_Ops_Review__c, 'We expect the box to be checked (Escalation)');
        system.assertEquals(system.today(), testCon.Escalated_At__c, 'Value should be today');
        system.assertEquals(TestUtils.ADMIN_USER.Id, testCon.Escalated_By__c, 'Lead owner is the escalator');
        system.assertEquals('Contact should potentially be merged, search for '+con2.qLead.Email, testCon.Escalation_Comment__c, 'Comment should be updated and include email');

    }

    private static testMethod void testSkipThisStep2()
    {
        setup();
        Account testExistingAccount = TestUtils.createAccount(TestUtils.MARKETER_RECORD_TYPE.Id, false);
        testExistingAccount.Name = 'Test Company Inc.';
        insert testExistingAccount;
        
        //Because we are using SOSL queries we must pass a list of our Ids to the fixedSearchResults method
         Id [] fixedSearchResults= new Id[1];
        fixedSearchResults[0] = testExistingAccount.Id;
        Test.setFixedSearchResults(fixedSearchResults);
        //search for existing accoutns and verify the search returns the account we inserted
        con.searchExistingAccounts();        
        system.assertEquals(1, con.existingAccounts.size(), 'We expect there to be one account returned in our search.');
        //select this account for conversion so that we have the Id in the URL and go to the next step to search for contacts
        con.existingAccounts[0].isSelected = true;
        con.next2();
        //assert that the account Id is in the URL
        Test.setCurrentPage(con.next2());
        system.assertEquals(con.existingAccounts[0].anAcct.Id, ApexPages.currentPage().getParameters().get('acctId'), 'We expect our account Id to be in the URL'); 

        //Trying to reinstantiate the controller so that it picks up the selected account
        SC2 = new Apexpages.StandardController(con.qLead);
        con2 = new QualifyLeadController(SC);
        //search for existing contacts and verify that the search returns the contact that we inserted
        con2.searchMatchingContacts();
        system.assertEquals(0, con2.matchingContacts.size(), 'We do not expect there to be any contacts returned in our search.');
        //Check that if there are no matching contacts that we are displaying the message to continue
        List<ApexPages.Message> msgList = ApexPages.getMessages();
        for(ApexPages.Message msg : msgList)
        {
            system.assertEquals('There are no matching Contacts, please click the Skip this Step button.', msg.getSummary(), 'Checking that we have the correct page message.');
            system.assertEquals(ApexPages.Severity.INFO, msg.getSeverity(), 'We expect there to be an Info Message.');
        }
        //Call the Skip this Step Method
        test.startTest();
        con2.skipThisStep2();
        test.stopTest();
        //Set the current page for our test    
        PageReference tpage = con2.skipThisStep2();
        Test.setCurrentPage(tpage);
        String strurl = ApexPages.currentPage().getUrl();
        String strurlName = strurl.split('apex/')[1];
        //Assert that our URL is as it should be
        system.assertEquals('QualifyLeadStep3?acctId='+con.existingAccounts[0].anAcct.Id+'&leadId='+con.qLead.Id+'&stepCounter=5', strurlName, 'We expect our URL to be correct.');

    }
    
    private static testMethod void testNext3()
    {
        setup();
        Account testExistingAccount = TestUtils.createAccount(TestUtils.MARKETER_RECORD_TYPE.Id, false);
        testExistingAccount.Name = 'Test Company Inc.';
        insert testExistingAccount;
        Contact testExistingContact = TestUtils.createContacts('Rachael', 'Chipman', testExistingAccount.Id, false);
        testExistingContact.Email = 'testemail@test.com';
        insert testExistingContact;

        //Because we are using SOSL queries we must pass a list of our Ids to the fixedSearchResults method
         Id [] fixedSearchResults= new Id[1];
        fixedSearchResults[0] = testExistingAccount.Id;
        Test.setFixedSearchResults(fixedSearchResults);
        //search for existing accoutns and verify the search returns the account we inserted
        con.searchExistingAccounts();        
        system.assertEquals(1, con.existingAccounts.size(), 'We expect there to be one account returned in our search.');
        //select this account for conversion so that we have the Id in the URL and go to the next step to search for contacts
        con.existingAccounts[0].isSelected = true;
        con.next2();
        //assert that the account Id is in the URL
        Test.setCurrentPage(con.next2());
        system.assertEquals(con.existingAccounts[0].anAcct.Id, ApexPages.currentPage().getParameters().get('acctId'), 'We expect our account Id to be in the URL');
        
        //Because we are using SOSL queries we must pass a list of our Ids to the fixedSearchResults method
         Id [] fixedSearchResults2= new Id[1];
        fixedSearchResults2[0] = testExistingContact.Id;
        Test.setFixedSearchResults(fixedSearchResults2);
        //Trying to reinstantiate the controller so that it picks up the selected account
        SC2 = new Apexpages.StandardController(con.qLead);
        con2 = new QualifyLeadController(SC);
        //search for existing contacts and verify that the search returns the contact that we inserted
        con2.searchMatchingContacts();
        system.assertEquals(1, con2.matchingContacts.size(), 'We expect there to be one contact returned in our search.');
        //Set the wrapper class isSelected variable so that we can test going to the next page
        con2.matchingContacts[0].isSelected = true;
        test.startTest();
        con2.next3();
        test.stopTest();
        //Set the current page for our test    
        PageReference tpage = con2.next3();
        Test.setCurrentPage(tpage);
        String strurl = ApexPages.currentPage().getUrl();
        String strurlName = strurl.split('apex/')[1];
        //Assert that our URL is as it should be
        system.assertEquals('QualifyLeadStep3?acctId='+con.existingAccounts[0].anAcct.Id+'&conId='+con2.matchingContacts[0].aContact.Id+'&leadId='+con.qLead.Id+'&stepCounter=5', strurlName, 'We expect our URL to be correct.');
    }

    private static testMethod void testNext3_noSelection()
    {
        setup();
        Account testExistingAccount = TestUtils.createAccount(TestUtils.MARKETER_RECORD_TYPE.Id, false);
        testExistingAccount.Name = 'Test Company Inc.';
        insert testExistingAccount;
        Contact testExistingContact = TestUtils.createContacts('Rachael', 'Chipman', testExistingAccount.Id, false);
        testExistingContact.Email = 'testemail@test.com';
        insert testExistingContact;

        //Because we are using SOSL queries we must pass a list of our Ids to the fixedSearchResults method
         Id [] fixedSearchResults= new Id[1];
        fixedSearchResults[0] = testExistingAccount.Id;
        Test.setFixedSearchResults(fixedSearchResults);
        //search for existing accoutns and verify the search returns the account we inserted
        con.searchExistingAccounts();        
        system.assertEquals(1, con.existingAccounts.size(), 'We expect there to be one account returned in our search.');
        //select this account for conversion so that we have the Id in the URL and go to the next step to search for contacts
        con.existingAccounts[0].isSelected = true;
        con.next2();
        //assert that the account Id is in the URL
        Test.setCurrentPage(con.next2());
        system.assertEquals(con.existingAccounts[0].anAcct.Id, ApexPages.currentPage().getParameters().get('acctId'), 'We expect our account Id to be in the URL');
        
        //Because we are using SOSL queries we must pass a list of our Ids to the fixedSearchResults method
         Id [] fixedSearchResults2= new Id[1];
        fixedSearchResults2[0] = testExistingContact.Id;
        Test.setFixedSearchResults(fixedSearchResults2);
        //Trying to reinstantiate the controller so that it picks up the selected account
        SC2 = new Apexpages.StandardController(con.qLead);
        con2 = new QualifyLeadController(SC);
        //search for existing contacts and verify that the search returns the contact that we inserted
        con2.searchMatchingContacts();
        system.assertEquals(1, con2.matchingContacts.size(), 'We expect there to be one contact returned in our search.');
        //Set the wrapper class isSelected variable so that we can test going to the next page
        //con2.matchingContacts[0].isSelected = true;
        test.startTest();
        con2.next3();
        test.stopTest();
        //Check that if there are no matching contacts that we are displaying the message to continue
        List<ApexPages.Message> msgList = ApexPages.getMessages();
        for(ApexPages.Message msg : msgList)
        {
            system.assertEquals('You must select a Contact to continue.', msg.getSummary(), 'Checking that we have the correct page message.');
            system.assertEquals(ApexPages.Severity.ERROR, msg.getSeverity(), 'We expect there to be an Error Message.');
        }
    }

    private static testMethod void testNext3_tooManySelected()
    {
        setup();
        Account testExistingAccount = TestUtils.createAccount(TestUtils.MARKETER_RECORD_TYPE.Id, false);
        testExistingAccount.Name = 'Test Company Inc.';
        insert testExistingAccount;
        Contact testExistingContact = TestUtils.createContacts('Rachael', 'Chipman', testExistingAccount.Id, false);
        testExistingContact.Email = 'testemail@test.com';
        insert testExistingContact;
        Contact testExistingContact2 = TestUtils.createContacts('Rachael', 'Huffman', testExistingAccount.Id, false);
        testExistingContact2.Email = 'testemail@test.com';
        insert testExistingContact2;

        //Because we are using SOSL queries we must pass a list of our Ids to the fixedSearchResults method
         Id [] fixedSearchResults= new Id[1];
        fixedSearchResults[0] = testExistingAccount.Id;
        Test.setFixedSearchResults(fixedSearchResults);
        //search for existing accoutns and verify the search returns the account we inserted
        con.searchExistingAccounts();        
        system.assertEquals(1, con.existingAccounts.size(), 'We expect there to be one account returned in our search.');
        //select this account for conversion so that we have the Id in the URL and go to the next step to search for contacts
        con.existingAccounts[0].isSelected = true;
        con.next2();
        //assert that the account Id is in the URL
        Test.setCurrentPage(con.next2());
        system.assertEquals(con.existingAccounts[0].anAcct.Id, ApexPages.currentPage().getParameters().get('acctId'), 'We expect our account Id to be in the URL');
        
        //Debugging
        List<Contact> contacts = [SELECT Id, AccountId FROM Contact WHERE Id = :testExistingContact.Id OR Id = :testExistingContact2.Id];
        system.assertEquals(2, contacts.size(), 'There should be two contacts in the system');
        for(Contact testContact : contacts)
        {
            system.assertEquals(testContact.AccountId, con.existingAccounts[0].anAcct.Id);
        }
        
        //Trying to reinstantiate the controller so that it picks up the selected account
        SC2 = new Apexpages.StandardController(con.qLead);
        con2 = new QualifyLeadController(SC);
        //search for existing contacts and verify that the search returns the contact that we insertedN./
        con2.searchMatchingContacts();
        system.debug('What contact is being returned' + con2.matchingContacts[0].aContact.LastName);
        system.assertEquals(2, con2.matchingContacts.size(), 'We expect there to be two contacts returned in our search.');
        //Set the wrapper class isSelected variable so that we can test going to the next page
        for(Integer i = 0; i < con2.matchingContacts.size(); i++)
        {
            con2.matchingContacts[i].isSelected = true;
        }
        test.startTest();
        con2.next3();
        test.stopTest();
        //Check that if there are no matching contacts that we are displaying the message to continue
        List<ApexPages.Message> msgList = ApexPages.getMessages();
        for(ApexPages.Message msg : msgList)
        {
            system.assertEquals('You must choose only one Contact to continue with.', msg.getSummary(), 'Checking that we have the correct page message.');
            system.assertEquals(ApexPages.Severity.ERROR, msg.getSeverity(), 'We expect there to be an Error Message.');
        }
    }

    private static testMethod void testQualifyIt_NEW()
    {
        setup();
        //call next method to get to account selection
        con.next();
        //call skip this step method to create new account
        con.skipThisStep();
        //Assert that we are on the right page
        PageReference tpage = con.skipThisStep();
        Test.setCurrentPage(tpage);
        String strurl = ApexPages.currentPage().getUrl();
        String strurlName = strurl.split('apex/')[1];
        system.assertEquals('QualifyLeadStep3?leadId='+con.qLead.Id+'&stepCounter=4', strurlName, 'We expect to be on the last page of the wizard');

        //Pass in the required values and call the qualify it method
        con.qLead.For_RSE__c = TestUtils.SALES_USER.Id;
        con.qLead.Opportunity_Source__c = 'Some Value';
        con.qLead.Status = 'Qualified';
        con.selectedContactRole = 'Some Role';

        con.qualifyIt();

        //Query for the new objects
        List<Opportunity> newOpps = [SELECT Id FROM Opportunity];
        List<Contact> newContacts = [SELECT Id FROM Contact];
        List<Account> newAccounts = [SELECT Id FROM Account];

        system.assertEquals(1, newOpps.size(), 'There should be an opportunity');
        system.assertEquals(1, newContacts.size(), 'There should be a contact');
        system.assertEquals(1, newAccounts.size(), 'There should be an account');

    }

    private static testMethod void testSelectOptionLists()
    {
        setup();

        system.assertNotEquals(1, con.getRoles().size());
        system.assertNotEquals(1, con.LeadStatusOption.size());
    }

    
}