@isTest

public class updateAccount_test{

public static Account a;
public static Promo_Code__c pc;

	private static testmethod void testupdateAccount()
	{
		a = new Account(
	    Name = 'Test Account');
	    insert a;
		        
		pc = new Promo_Code__c(
	    Name = 'TEST001');
	    insert pc;
		        
	    test.startTest();
	    pc.Account__c = a.Id;
	    update pc;
	    test.stopTest();
	    
		List<Account> updatedAccount = [SELECT Id, Promo_Code__c FROM Account WHERE Id = :a.Id];
	    system.assertEquals(pc.Id, updatedAccount[0].Promo_Code__c, 'The Account should have the correct Promo Code');
	        
	}
}