trigger InboxMonitorTrigger on Mailbox_Monitor_Events__c (before insert) {
	
	if(trigger.isBefore && trigger.isInsert)
	{
		InboxMonitorTriggerHandler.processEvents(trigger.new);
	}
}