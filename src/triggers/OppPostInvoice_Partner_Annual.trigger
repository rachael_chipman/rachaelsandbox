trigger OppPostInvoice_Partner_Annual on Opportunity (Before Update) {

//Define all varibles that will be used.
/*Date inDate1;
Date inDate2;
Date inDate3;
Date inDate4;
Date inDate5;
Date inDate6;
Date inDate7;
Date inDate8;
Date inDate9;
Date inDate10;
Date inDate11;
Date inDate12;
List<OpportunityLineItem> opplineitems;
Decimal price;
Decimal refprice;
list<c2g__codaInvoice__c> newinvoices = new list<c2g__codaInvoice__c>();
List<c2g__codaInvoiceLineItem__c> silis = new List<c2g__codaInvoiceLineItem__c>();
Decimal rate;
Decimal rate2;
c2g__codaAccountingCurrency__c curcy;
String acctid;
c2g__codaIncomeScheduleDefinition__c incsched;
c2g__codaGeneralLedgerAccount__c DefExp;
c2g__codaGeneralLedgerAccount__c ComPart;
c2g__codaGeneralLedgerAccount__c ComPayable;

for(Opportunity o : Trigger.New){
//conditions for the trigger
     if(o.StageName == 'Finance Approved' && o.Invoice_Created__c == false && o.Billing_Type__c == 'Annual' && o.PartnerLookup__c != null
     && (o.Type=='New Business' || o.type== 'Renewal') && o.Minimum_Start_Date__c != null && o.FF_Company_Formula__c == 'Return Path, Inc.'
     && o.Bypass_Financial_Force__c == false && o.ExpectedRevenue > 0 && o.Begin_Automation__c == true){

//Queries for information used later
    c2g__codaAccountingCurrency__c cur = [select Id, Name from c2g__codaAccountingCurrency__c where Name = :o.CurrencyIsoCode and c2g__OwnerCompany__c = 'a2I00000000CaqE'];
    Account partner = [select Id, CurrencyIsoCode from Account where Id = :o.PartnerLookup__c];
    incsched = [SELECT Id, Name FROM c2g__codaIncomeScheduleDefinition__c WHERE Name = 'Income Schedule' LIMIT 1];
    DefExp = [SELECT Id, Name FROM c2g__codaGeneralLedgerAccount__c WHERE c2g__ReportingCode__c = '1350' LIMIT 1];
    ComPart = [SELECT Id, Name FROM c2g__codaGeneralLedgerAccount__c WHERE c2g__ReportingCode__c = '5000' LIMIT 1];
    ComPayable = [SELECT Id, Name FROM c2g__codaGeneralLedgerAccount__c WHERE c2g__ReportingCode__c = '2080' LIMIT 1];

//define the dates that will be used based on today's date
    if(system.Today() <= o.Minimum_Start_Date__c) inDate1=o.Minimum_Start_Date__c;
    if(system.Today() > o.Minimum_Start_Date__c) inDate1=system.Today();
    If(system.Today() < o.Minimum_Start_Date__c.addMonths(1))inDate2=o.Minimum_Start_Date__c.addMonths(1);
    If(system.Today() >= o.Minimum_Start_Date__c.addMonths(1))inDate2=system.Today();
    If(system.Today() < o.Minimum_Start_Date__c.addMonths(2))inDate3=o.Minimum_Start_Date__c.addMonths(2);
    If(system.Today() >= o.Minimum_Start_Date__c.addMonths(2))inDate3=system.Today();
    If(system.Today() < o.Minimum_Start_Date__c.addMonths(3))inDate4=o.Minimum_Start_Date__c.addMonths(3);
    If(system.Today() >= o.Minimum_Start_Date__c.addMonths(3))inDate4=system.Today();
    If(system.Today() < o.Minimum_Start_Date__c.addMonths(4))inDate5=o.Minimum_Start_Date__c.addMonths(4);
    If(system.Today() >= o.Minimum_Start_Date__c.addMonths(4))inDate5=system.Today();
    If(system.Today() < o.Minimum_Start_Date__c.addMonths(5))inDate6=o.Minimum_Start_Date__c.addMonths(5);
    If(system.Today() >= o.Minimum_Start_Date__c.addMonths(5))inDate6=system.Today();
    If(system.Today() < o.Minimum_Start_Date__c.addMonths(6))inDate7=o.Minimum_Start_Date__c.addMonths(6);
    If(system.Today() >= o.Minimum_Start_Date__c.addMonths(6))inDate7=system.Today();
    If(system.Today() < o.Minimum_Start_Date__c.addMonths(7))inDate8=o.Minimum_Start_Date__c.addMonths(7);
    If(system.Today() >= o.Minimum_Start_Date__c.addMonths(7))inDate8=system.Today();
    If(system.Today() < o.Minimum_Start_Date__c.addMonths(8))inDate9=o.Minimum_Start_Date__c.addMonths(8);
    If(system.Today() >= o.Minimum_Start_Date__c.addMonths(8))inDate9=system.Today();
    If(system.Today() < o.Minimum_Start_Date__c.addMonths(9))inDate10=o.Minimum_Start_Date__c.addMonths(9);
    If(system.Today() >= o.Minimum_Start_Date__c.addMonths(9))inDate10=system.Today();
    If(system.Today() < o.Minimum_Start_Date__c.addMonths(10))inDate11=o.Minimum_Start_Date__c.addMonths(10);
    If(system.Today() >= o.Minimum_Start_Date__c.addMonths(10))inDate11=system.Today();
    If(system.Today() < o.Minimum_Start_Date__c.addMonths(11))inDate12=o.Minimum_Start_Date__c.addMonths(11);
    If(system.Today() >= o.Minimum_Start_Date__c.addMonths(11))inDate12=system.Today();

    if(o.Who_is_Invoiced__c == 'Partner is Invoiced'){acctid = partner.Id;}
    if(o.Who_is_Invoiced__c != 'Partner is Invoiced'){acctid = o.AccountId;}

//find the exchange rates needed for building the line items.
     if((o.Who_is_Invoiced__c != 'Partner is Invoiced') || (o.Who_is_Invoiced__c == 'Partner is Invoiced' && o.CurrencyISOCode == partner.CurrencyISOCode)) {
     curcy = cur; rate = 1; rate2 = 1;}

    if(o.Who_is_Invoiced__c == 'Partner is Invoiced' && o.CurrencyISOCode != partner.CurrencyISOCode){
    c2g__codaAccountingCurrency__c cur2 = [select Id, Name from c2g__codaAccountingCurrency__c where Name = :partner.CurrencyIsoCode and c2g__OwnerCompany__c = 'a2I00000000CaqE'];
    curcy = cur2;

    if(o.CurrencyISOCode == 'USD'){ rate=1;}

    if(o.CurrencyISOCode != 'USD') {
        c2g__codaExchangeRate__c exrate = [select Id, c2g__Rate__c from c2g__codaExchangeRate__c where c2g__ExchangeRateCurrency__r.Id = :cur.Id and c2g__ExchangeRateCurrency__r.c2g__OwnerCompany__c = 'a2I00000000CaqE' and c2g__Startdate__c <= :indate1 order by c2g__Startdate__c desc limit 1];
        rate = exrate.c2g__Rate__c;}

    if(partner.CurrencyISOCode != 'USD'){
        c2g__codaExchangeRate__c exrate2 = [select Id, c2g__Rate__c from c2g__codaExchangeRate__c where c2g__ExchangeRateCurrency__r.Id = :curcy.Id
                                            and c2g__StartDate__c <= :inDate1 order by c2g__StartDate__c desc limit 1 ];
         rate2 = exrate2.c2g__Rate__c;}

     if(partner.CurrencyISOCode == 'USD'){
         rate2 = 1;}}

//Create invoice
        c2g__codaInvoice__c invoice = new c2g__codaInvoice__c(
        c2g__OwnerCompany__c = 'a2I00000000CaqE',
        c2g__Account__c = acctid,
        Opportunity_Account__c = o.AccountId,
        c2g__Opportunity__c = o.Id,
        ffbilling__CopyAccountValues__c = true,
        c2g__InvoiceDate__c = inDate1,
        c2g__InvoiceCurrency__c = curcy.Id,
        c2g__DueDate__c = inDate1 +Integer.valueof(o.Billing_Terms_Number_Calc__c),
        Contract_Service_Period_Start_Date__c = o.Minimum_Start_Date__c,
        Contract_Service_Period_End_Date__c = o.Minimum_Start_Date__c.AddYears(1).addDays(-1),
        ffbilling__DerivePeriod__c = true,
        ffbilling__DeriveDueDate__c = false,
        c2g__InvoiceStatus__c = 'In Progress',
        Invoicing_Contact__c = o.Account.Invoicing_Contact__c,
        c2g__PrintStatus__c = 'Not Printed',
        ffbilling__DeriveCurrency__c = false );



        if(o.Who_is_Invoiced__c == 'Partner is Invoiced'){invoice.Partner_is_Billing_Agent__c = true;}


   newinvoices.add(invoice);

    insert newinvoices;

//Query for Products based on who is invoiced.
     if(o.Who_is_Invoiced__c == 'Customer is Invoiced')
     opplineitems = [select Id, CurrencyIsoCode, Description, Description__c, Discount, Start_Date__c, End_Date__c, Number_of_Journals_Calc__c,
        UnitPrice, Start_Date_on_the_First__c, PricebookEntry.Product2Id, Quantity from OpportunityLineItem where OpportunityId = :o.Id and
        (UnitPrice > 0 or PricebookEntry.Product2.Name like 'Discount%')];

    if(o.Who_is_Invoiced__c == 'Partner is Invoiced' )
    opplineitems = [select Id, CurrencyIsoCode, Description, Description__c, Discount, Start_Date__c, End_Date__c, Number_of_Journals_Calc__c,
        UnitPrice, Start_Date_on_the_First__c, PricebookEntry.Product2Id, Quantity from OpportunityLineItem where OpportunityId = :o.Id];

//Create Invoice line items for each product.
   for(OpportunityLineItem olis : opplineitems){

     price = ((olis.UnitPrice * (1/rate)) * rate2);

    c2g__codaInvoiceLineItem__c sili = new c2g__codaInvoiceLineItem__c(
        c2g__Invoice__c = invoice.Id,
        c2g__Product__c = olis.PricebookEntry.Product2Id,
        c2g__Quantity__c = olis.Quantity,
        c2g__UnitPrice__c = price.setscale(2),
        Opportunity__c = o.Id,
        c2g__StartDate__c = olis.Start_Date__c,
        End_Date__c = olis.End_Date__c,
        c2g__LineDescription__c = olis.Description,
        ffbilling__DeriveUnitPriceFromProduct__c = false,
        c2g__NumberofJournals__c = olis.Number_of_Journals_Calc__c,
        c2g__IncomeSchedule__c = incsched.Id);



        silis.add(sili);

}

          insert silis;


//lists for updating income schedule line items
        list<c2g__codaScheduleLineItem__c > schlis = new list<c2g__codaScheduleLineItem__c >();
        list<c2g__codaScheduleLineItem__c > schlistodelete = new list<c2g__codaScheduleLineItem__c >();

         c2g__codaPeriod__c period = [select Id from c2g__codaPeriod__c where Name = :o.Close_Date_Period__c limit 1];

//Query all Income Schedule Line items to be updated on Daily Revenue Recognigition.
         for( c2g__codaScheduleLineItem__c schli : [select Id, Period_Month__c, c2g__Period__c, c2g__Period__r.Name, SILI_End_Date_Month__c, Period_Change_Needed__c, SILI_End_Date_Year__c,
                                                    c2g__Amount__c, Sales_Invoice_Line_Item_End_Date__c, Sales_Invoice_Line_Item_Start_Date__c, SILI_Start_Date_Month__c, Start_Date_Year__c,
                                                    c2g__SalesInvoiceLineItem__r.Price_Per_Day__c, c2g__Period__r.c2g__EndDate__c, c2g__Period__r.c2g__StartDate__c, c2g__LineNumber__c,
                                                    c2g__Period__r.Days_in_Period__c
                                                    from c2g__codaScheduleLineItem__c where c2g__SalesInvoiceLineItem__c in :silis ]){


            //for invoices approved after contract start date. Need to update revenue schedule
            String operiod = schli.c2g__Period__r.Name;

            if(o.Close_Date_Formula__c > 0 && schli.Period_Change_Needed__c == 'true' ){
            schli.c2g__Period__c = period.Id;
            schli.Original_Contract_Period__c = operiod;
            }

            if(schli.c2g__LineNumber__c == 1 && schli.Sales_Invoice_Line_Item_End_Date__c > schli.c2g__Period__r.c2g__EndDate__c){
            schli.c2g__Amount__c = ((schli.Sales_Invoice_Line_Item_Start_Date__c.daysbetween(schli.c2g__Period__r.c2g__EndDate__c)+1)*schli.c2g__SalesInvoiceLineItem__r.Price_Per_Day__c).setscale(2);}

            if(schli.c2g__LineNumber__c == 1 && schli.Sales_Invoice_Line_Item_End_Date__c < schli.c2g__Period__r.c2g__EndDate__c){
            schli.c2g__Amount__c = ((schli.Sales_Invoice_Line_Item_Start_Date__c.daysbetween(schli.Sales_Invoice_Line_Item_End_Date__c)+1)*schli.c2g__SalesInvoiceLineItem__r.Price_Per_Day__c).setscale(2);}

            if(schli.c2g__LineNumber__c > 1 && schli.Sales_Invoice_Line_Item_End_Date__c > schli.c2g__Period__r.c2g__EndDate__c){
            schli.c2g__Amount__c = (schli.c2g__Period__r.Days_in_Period__c*schli.c2g__SalesInvoiceLineItem__r.Price_Per_Day__c).setscale(2);}

            if(schli.c2g__LineNumber__c > 1 && schli.Sales_Invoice_Line_Item_End_Date__c < schli.c2g__Period__r.c2g__EndDate__c){
            schli.c2g__Amount__c = ((schli.c2g__Period__r.c2g__StartDate__c.daysbetween(schli.Sales_Invoice_Line_Item_End_Date__c)+1)*schli.c2g__SalesInvoiceLineItem__r.Price_Per_Day__c).setscale(2);}

            if(schli.SILI_Start_Date_Month__c != schli.Period_Month__c && schli.SILI_End_Date_Month__c < schli.Period_Month__c && schli.c2g__Period__r.c2g__StartDate__c > schli.Sales_Invoice_Line_Item_End_Date__c){
            schli.c2g__Amount__c = 0;}



         if(schli.c2g__Amount__c != 0){schlis.add(schli);}
         if(schli.c2g__Amount__c == 0){schlistodelete.add(schli);}



            }
//Only keep line items that are > 0
            update schlis;
            delete schlistodelete;

//if there is a balance remaining for a product then add the remainder to the last Income Schedule Line Item.
       list<c2g__codaScheduleLineItem__c > updatesli = new list<c2g__codaScheduleLineItem__c >();

           for(c2g__codaInvoiceLineItem__c ili : silis){
               if(ili.ffbilling__ScheduleNetTotal__c != (ili.c2g__UnitPrice__c * ili.c2g__Quantity__c)){
                   Decimal UP = ili.c2g__UnitPrice__c * ili.c2g__Quantity__c;

                   AggregateResult[] groupedResults  = [select SUM(c2g__Amount__c) amt from c2g__codaScheduleLineItem__c where c2g__SalesInvoiceLineItem__c = :ili.Id];
                   Decimal SNT;

                   for (AggregateResult ar : groupedResults)  {
                   SNT = (Decimal)ar.get('amt');
                   }
                   if (UP == null) UP = 0;
                   if (SNT == null) SNT = 0;
                   Decimal balance = UP - SNT;
                   List<c2g__codaScheduleLineItem__c> lastsli = [select Id, c2g__Amount__c from c2g__codaScheduleLineItem__c where c2g__SalesInvoiceLineItem__c = :ili.Id order by c2g__LineNumber__c DESC Limit 1];
                   // Paul Rice - null check added because of schedule line item deletion
                   if( !lastsli.isEmpty() )
                   {
                       Decimal curamt = lastsli[0].c2g__Amount__c;
                       lastsli[0].c2g__Amount__c = curamt + balance;
                       updatesli.add(lastsli[0]);
                   }
               }
            }

            update updatesli;

//Creating Commission Journals
if(o.Deal_Closing_Through__c != 'Partner' && o.Partner_Commission__c > 0){

 c2g__codaJournal__c fullJournal = new c2g__codaJournal__c(
    c2g__Type__c = 'Manual Journal',
    c2g__JournalDate__c = inDate1,
    c2g__JournalCurrency__c = cur.Id,
    c2g__Reference__c = '',
    Opportunity__c = o.Id,
    ffgl__DeriveCurrency__c = false,
    c2g__JournalDescription__c = '');

    insert fullJournal;

        List<c2g__codaJournalLineItem__c> fullJLIs = new List<c2g__codaJournalLineItem__c>();


    Decimal fullJLIprice = (o.ACV_Before_Partner_Comm__c * (o.Partner_Commission__c/100));

        fullJLIs.add(new c2g__codaJournalLineItem__c(
        c2g__LineType__c = 'General Ledger Account',
        CurrencyIsoCode = o.CurrencyIsoCode,
        c2g__GeneralLedgerAccount__c = ComPayable.Id,
        c2g__Journal__c = fullJournal.Id,
        c2g__Value__c = fullJLIprice.SetScale(2)*(-1)));

        fullJLIs.add(new c2g__codaJournalLineItem__c(
        c2g__LineType__c = 'General Ledger Account',
        CurrencyIsoCode = o.CurrencyIsoCode,
        c2g__GeneralLedgerAccount__c = DefExp.Id,
        c2g__Journal__c = fullJournal.Id,
        c2g__Value__c = fullJLIprice.SetScale(2)));



        insert fullJLIs;

      List<c2g__codaJournal__c>  monthlyJournals = new List<c2g__codaJournal__c >();

    c2g__codaJournal__c mJournal1 = new c2g__codaJournal__c(
        c2g__Type__c = 'Manual Journal',
    c2g__JournalDate__c = inDate1,
    c2g__JournalCurrency__c = cur.Id,
    ffgl__DeriveCurrency__c = false,
    c2g__Reference__c = '',
    Opportunity__c = o.Id,

    c2g__JournalDescription__c = '');

         monthlyJournals.add(mJournal1);

        c2g__codaJournal__c mJournal2 = new c2g__codaJournal__c(
        c2g__Type__c = 'Manual Journal',
    c2g__JournalDate__c = inDate2,
    c2g__JournalCurrency__c = cur.Id,
    ffgl__DeriveCurrency__c = false,
    c2g__Reference__c = '',
    Opportunity__c = o.Id,
    c2g__JournalDescription__c = '');

         monthlyJournals.add(mJournal2);

        c2g__codaJournal__c mJournal3 = new c2g__codaJournal__c(
         c2g__Type__c = 'Manual Journal',
    c2g__JournalDate__c = inDate3,
    c2g__JournalCurrency__c = cur.Id,
    ffgl__DeriveCurrency__c = false,
    c2g__Reference__c = '',
    Opportunity__c = o.Id,
    c2g__JournalDescription__c = '');

         monthlyJournals.add(mJournal3);

        c2g__codaJournal__c mJournal4 = new c2g__codaJournal__c(
         c2g__Type__c = 'Manual Journal',
    c2g__JournalDate__c = indate4,
    c2g__JournalCurrency__c = cur.Id,
    ffgl__DeriveCurrency__c = false,
    c2g__Reference__c = '',
    Opportunity__c = o.Id,
    c2g__JournalDescription__c = '');

         monthlyJournals.add(mJournal4);

        c2g__codaJournal__c mJournal5 = new c2g__codaJournal__c(
         c2g__Type__c = 'Manual Journal',
    c2g__JournalDate__c = indate5,
    c2g__JournalCurrency__c = cur.Id,
    ffgl__DeriveCurrency__c = false,
    c2g__Reference__c = '',
    Opportunity__c = o.Id,
    c2g__JournalDescription__c = '');

         monthlyJournals.add(mJournal5);

        c2g__codaJournal__c mJournal6 = new c2g__codaJournal__c(
         c2g__Type__c = 'Manual Journal',
    c2g__JournalDate__c = indate6,
    c2g__JournalCurrency__c = cur.Id,
    ffgl__DeriveCurrency__c = false,
    c2g__Reference__c = '',
    Opportunity__c = o.Id,
    c2g__JournalDescription__c = '');

         monthlyJournals.add(mJournal6);

        c2g__codaJournal__c mJournal7 = new c2g__codaJournal__c(
         c2g__Type__c = 'Manual Journal',
    c2g__JournalDate__c = indate7,
    c2g__JournalCurrency__c = cur.Id,
    ffgl__DeriveCurrency__c = false,
    c2g__Reference__c = '',
    Opportunity__c = o.Id,
    c2g__JournalDescription__c = '');

         monthlyJournals.add(mJournal7);

        c2g__codaJournal__c mJournal8 = new c2g__codaJournal__c(
         c2g__Type__c = 'Manual Journal',
    c2g__JournalDate__c = indate8,
    c2g__JournalCurrency__c = cur.Id,
    ffgl__DeriveCurrency__c = false,
    c2g__Reference__c = '',
    Opportunity__c = o.Id,
    c2g__JournalDescription__c = '');

         monthlyJournals.add(mJournal8);

        c2g__codaJournal__c mJournal9 = new c2g__codaJournal__c(
         c2g__Type__c = 'Manual Journal',
    c2g__JournalDate__c = indate9,
    c2g__JournalCurrency__c = cur.Id,
    ffgl__DeriveCurrency__c = false,
    c2g__Reference__c = '',
    Opportunity__c = o.Id,
    c2g__JournalDescription__c = '');

         monthlyJournals.add(mJournal9);

        c2g__codaJournal__c mJournal10 = new c2g__codaJournal__c(
         c2g__Type__c = 'Manual Journal',
    c2g__JournalDate__c = indate10,
    c2g__JournalCurrency__c = cur.Id,
    ffgl__DeriveCurrency__c = false,
    c2g__Reference__c = '',
    Opportunity__c = o.Id,
    c2g__JournalDescription__c = '');

         monthlyJournals.add(mJournal10);

        c2g__codaJournal__c mJournal11 = new c2g__codaJournal__c(
         c2g__Type__c = 'Manual Journal',
    c2g__JournalDate__c = indate11,
    c2g__JournalCurrency__c = cur.Id,
    ffgl__DeriveCurrency__c = false,
    c2g__Reference__c = '',
    Opportunity__c = o.Id,
    c2g__JournalDescription__c = '');

         monthlyJournals.add(mJournal11);

        c2g__codaJournal__c mJournal12 = new c2g__codaJournal__c(
         c2g__Type__c = 'Manual Journal',
    c2g__JournalDate__c = indate12,
    c2g__JournalCurrency__c = cur.Id,
    ffgl__DeriveCurrency__c = false,
    c2g__Reference__c = '',
    Opportunity__c = o.Id,
    c2g__JournalDescription__c = '');

         monthlyJournals.add(mJournal12);

        insert monthlyJournals;

                List<c2g__codaJournalLineItem__c> monthlyJLIs = new List<c2g__codaJournalLineItem__c>();


    Decimal monthlyJLIprice = fullJLIprice/12;

        monthlyJLIs.add(new c2g__codaJournalLineItem__c(
        c2g__LineType__c = 'General Ledger Account',
        CurrencyIsoCode = o.CurrencyIsoCode,
        c2g__GeneralLedgerAccount__c = DefExp.Id,
        c2g__Journal__c = mJournal1.Id,
        c2g__Value__c = monthlyJLIprice.SetScale(2)*(-1)));

        monthlyJLIs.add(new c2g__codaJournalLineItem__c(
        c2g__LineType__c = 'General Ledger Account',
        CurrencyIsoCode = o.CurrencyIsoCode,
        c2g__GeneralLedgerAccount__c = ComPart.Id,
        c2g__Journal__c = mJournal1.Id,
        c2g__Value__c = monthlyJLIprice.SetScale(2)));

        monthlyJLIs.add(new c2g__codaJournalLineItem__c(
        c2g__LineType__c = 'General Ledger Account',
        CurrencyIsoCode = o.CurrencyIsoCode,
        c2g__GeneralLedgerAccount__c = DefExp.Id,
        c2g__Journal__c = mJournal2.Id,
        c2g__Value__c = monthlyJLIprice.SetScale(2)*(-1)));

        monthlyJLIs.add(new c2g__codaJournalLineItem__c(
        c2g__LineType__c = 'General Ledger Account',
        CurrencyIsoCode = o.CurrencyIsoCode,
        c2g__GeneralLedgerAccount__c = ComPart.Id,
        c2g__Journal__c = mJournal2.Id,
        c2g__Value__c = monthlyJLIprice.SetScale(2)));

        monthlyJLIs.add(new c2g__codaJournalLineItem__c(
        c2g__LineType__c = 'General Ledger Account',
        CurrencyIsoCode = o.CurrencyIsoCode,
        c2g__GeneralLedgerAccount__c = DefExp.Id,
        c2g__Journal__c = mJournal3.Id,
        c2g__Value__c = monthlyJLIprice.SetScale(2)*(-1)));

        monthlyJLIs.add(new c2g__codaJournalLineItem__c(
        c2g__LineType__c = 'General Ledger Account',
        CurrencyIsoCode = o.CurrencyIsoCode,
        c2g__GeneralLedgerAccount__c = ComPart.Id,
        c2g__Journal__c = mJournal3.Id,
        c2g__Value__c = monthlyJLIprice.SetScale(2)));

        monthlyJLIs.add(new c2g__codaJournalLineItem__c(
        c2g__LineType__c = 'General Ledger Account',
        CurrencyIsoCode = o.CurrencyIsoCode,
        c2g__GeneralLedgerAccount__c = DefExp.Id,
        c2g__Journal__c = mJournal4.Id,
        c2g__Value__c = monthlyJLIprice.SetScale(2)*(-1)));

        monthlyJLIs.add(new c2g__codaJournalLineItem__c(
        c2g__LineType__c = 'General Ledger Account',
        CurrencyIsoCode = o.CurrencyIsoCode,
        c2g__GeneralLedgerAccount__c = ComPart.Id,
        c2g__Journal__c = mJournal4.Id,
        c2g__Value__c = monthlyJLIprice.SetScale(2)));

        monthlyJLIs.add(new c2g__codaJournalLineItem__c(
        c2g__LineType__c = 'General Ledger Account',
        CurrencyIsoCode = o.CurrencyIsoCode,
        c2g__GeneralLedgerAccount__c = DefExp.Id,
        c2g__Journal__c = mJournal5.Id,
        c2g__Value__c = monthlyJLIprice.SetScale(2)*(-1)));

        monthlyJLIs.add(new c2g__codaJournalLineItem__c(
        c2g__LineType__c = 'General Ledger Account',
        CurrencyIsoCode = o.CurrencyIsoCode,
        c2g__GeneralLedgerAccount__c = ComPart.Id,
        c2g__Journal__c = mJournal5.Id,
        c2g__Value__c = monthlyJLIprice.SetScale(2)));

        monthlyJLIs.add(new c2g__codaJournalLineItem__c(
        c2g__LineType__c = 'General Ledger Account',
        CurrencyIsoCode = o.CurrencyIsoCode,
        c2g__GeneralLedgerAccount__c = DefExp.Id,
        c2g__Journal__c = mJournal6.Id,
        c2g__Value__c = monthlyJLIprice.SetScale(2)*(-1)));

        monthlyJLIs.add(new c2g__codaJournalLineItem__c(
        c2g__LineType__c = 'General Ledger Account',
        CurrencyIsoCode = o.CurrencyIsoCode,
        c2g__GeneralLedgerAccount__c = ComPart.Id,
        c2g__Journal__c = mJournal6.Id,
        c2g__Value__c = monthlyJLIprice.SetScale(2)));

        monthlyJLIs.add(new c2g__codaJournalLineItem__c(
        c2g__LineType__c = 'General Ledger Account',
        CurrencyIsoCode = o.CurrencyIsoCode,
        c2g__GeneralLedgerAccount__c = DefExp.Id,
        c2g__Journal__c = mJournal7.Id,
        c2g__Value__c = monthlyJLIprice.SetScale(2)*(-1)));

        monthlyJLIs.add(new c2g__codaJournalLineItem__c(
        c2g__LineType__c = 'General Ledger Account',
        CurrencyIsoCode = o.CurrencyIsoCode,
        c2g__GeneralLedgerAccount__c = ComPart.Id,
        c2g__Journal__c = mJournal7.Id,
        c2g__Value__c = monthlyJLIprice.SetScale(2)));

        monthlyJLIs.add(new c2g__codaJournalLineItem__c(
        c2g__LineType__c = 'General Ledger Account',
        CurrencyIsoCode = o.CurrencyIsoCode,
        c2g__GeneralLedgerAccount__c = DefExp.Id,
        c2g__Journal__c = mJournal8.Id,
        c2g__Value__c = monthlyJLIprice.SetScale(2)*(-1)));

        monthlyJLIs.add(new c2g__codaJournalLineItem__c(
        c2g__LineType__c = 'General Ledger Account',
        CurrencyIsoCode = o.CurrencyIsoCode,
        c2g__GeneralLedgerAccount__c = ComPart.Id,
        c2g__Journal__c = mJournal8.Id,
        c2g__Value__c = monthlyJLIprice.SetScale(2)));

        monthlyJLIs.add(new c2g__codaJournalLineItem__c(
        c2g__LineType__c = 'General Ledger Account',
        CurrencyIsoCode = o.CurrencyIsoCode,
        c2g__GeneralLedgerAccount__c = DefExp.Id,
        c2g__Journal__c = mJournal9.Id,
        c2g__Value__c = monthlyJLIprice.SetScale(2)*(-1)));

        monthlyJLIs.add(new c2g__codaJournalLineItem__c(
        c2g__LineType__c = 'General Ledger Account',
        CurrencyIsoCode = o.CurrencyIsoCode,
        c2g__GeneralLedgerAccount__c = ComPart.Id,
        c2g__Journal__c = mJournal9.Id,
        c2g__Value__c = monthlyJLIprice.SetScale(2)));

        monthlyJLIs.add(new c2g__codaJournalLineItem__c(
        c2g__LineType__c = 'General Ledger Account',
        CurrencyIsoCode = o.CurrencyIsoCode,
        c2g__GeneralLedgerAccount__c = DefExp.Id,
        c2g__Journal__c = mJournal10.Id,
        c2g__Value__c = monthlyJLIprice.SetScale(2)*(-1)));

        monthlyJLIs.add(new c2g__codaJournalLineItem__c(
        c2g__LineType__c = 'General Ledger Account',
        CurrencyIsoCode = o.CurrencyIsoCode,
        c2g__GeneralLedgerAccount__c = ComPart.Id,
        c2g__Journal__c = mJournal10.Id,
        c2g__Value__c = monthlyJLIprice.SetScale(2)));

        monthlyJLIs.add(new c2g__codaJournalLineItem__c(
        c2g__LineType__c = 'General Ledger Account',
        CurrencyIsoCode = o.CurrencyIsoCode,
        c2g__GeneralLedgerAccount__c = DefExp.Id,
        c2g__Journal__c = mJournal11.Id,
        c2g__Value__c = monthlyJLIprice.SetScale(2)*(-1)));

        monthlyJLIs.add(new c2g__codaJournalLineItem__c(
        c2g__LineType__c = 'General Ledger Account',
        CurrencyIsoCode = o.CurrencyIsoCode,
        c2g__GeneralLedgerAccount__c = ComPart.Id,
        c2g__Journal__c = mJournal11.Id,
        c2g__Value__c = monthlyJLIprice.SetScale(2)));

        monthlyJLIs.add(new c2g__codaJournalLineItem__c(
        c2g__LineType__c = 'General Ledger Account',
        CurrencyIsoCode = o.CurrencyIsoCode,
        c2g__GeneralLedgerAccount__c = DefExp.Id,
        c2g__Journal__c = mJournal12.Id,
        c2g__Value__c = monthlyJLIprice.SetScale(2)*(-1)));

        monthlyJLIs.add(new c2g__codaJournalLineItem__c(
        c2g__LineType__c = 'General Ledger Account',
        CurrencyIsoCode = o.CurrencyIsoCode,
        c2g__GeneralLedgerAccount__c = ComPart.Id,
        c2g__Journal__c = mJournal12.Id,
        c2g__Value__c = monthlyJLIprice.SetScale(2)));

        insert monthlyJLIs;



    }






    o.Invoice_Created__c = true;


    }


}*/}