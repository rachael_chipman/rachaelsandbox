@isTest
private class TestJournalTrigger 
{
	
	@isTest(seeAllData=true) 
	static void testPost() 
	{
		FinancialForceTestHelper.baseSetup();
		List<Opportunity> testOpportunities = TestUtils.createOpportunities( 2, TestUtils.RESEARCH_RECORD_TYPE.Id, FinancialForceTestHelper.testAccount.Id, true );
		List<c2g__codaJournal__c> testJournals = new List<c2g__codaJournal__c>();
		List<c2g__codaJournalLineItem__c> testJournalLineItems = new List<c2g__codaJournalLineItem__c>();
		
		for( Opportunity anOpportunity : testOpportunities )
		{
			testJournals.add( TestUtils.createJournal( new Map<String,Object>{ 'Opportunity__c' => anOpportunity.Id }, false ) );
		}
		insert testJournals;
		
		for( c2g__codaJournal__c testJournal: testJournals )
		{
			testJournalLineItems.add( TestUtils.createJournalLineItem( testJournal.Id, 'General Ledger Account', FinancialForceTestHelper.testGeneralLedgerAccount.Id, 10.0, false ) );
			testJournalLineItems.add( TestUtils.createJournalLineItem( testJournal.Id, 'General Ledger Account', FinancialForceTestHelper.testGeneralLedgerAccount.Id, -10.0, false ) );
		}
		insert testJournalLineItems; 

		test.startTest();
			FinancialForceTestHelper.bulkPostJournals( Pluck.ids( testJournals ) );
		test.stopTest();

		testJournals = [ SELECT c2g__Transaction__r.Opportunity__c, Opportunity__c, c2g__Transaction__c FROM c2g__codaJournal__c WHERE Id IN :testJournals ];

		for( c2g__codaJournal__c testJournal: testJournals )
		{
			System.assertEquals( testJournal.Opportunity__c, testJournal.c2g__Transaction__r.Opportunity__c, 'Transaction opportunity  lookup should be populated with related journal\'s opportunity' );
		}

		Set<Id> transactionIds = Pluck.ids( 'c2g__Transaction__c', testJournals );

		List<c2g__codaTransactionLineItem__c> actualTransationLineItems = [ SELECT Id, Opportunity__c, c2g__Transaction__c, c2g__Transaction__r.Opportunity__c From c2g__codaTransactionLineItem__c WHERE c2g__Transaction__c IN : transactionIds];
		System.assert( !actualTransationLineItems.isEmpty(), 'There should be transaction line items created' );
		for( c2g__codaTransactionLineItem__c testTransactionLineItem: actualTransationLineItems )
		{
			System.assertEquals( testTransactionLineItem.c2g__Transaction__r.Opportunity__c, testTransactionLineItem.Opportunity__c, 'Transaction line item opportunity  lookup should be populated with related transaction\'s opportunity' );
		}
	}
	
}