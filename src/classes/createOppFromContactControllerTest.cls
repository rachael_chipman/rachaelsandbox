@isTest

private class createOppFromContactControllerTest {

    static Contact testCon;

static void setupContact()

{
    Campaign c = new Campaign (
    Name = 'test campaign',
    IsActive=True
    );
    insert c;

    Campaign salesOut = new Campaign(Name = 'Sales Outreach', IsActive = true);
    insert salesOut;

    Campaign contactUs = new Campaign(Name = 'Contact Us', IsActive = true);
    insert contactUs;

    Campaign salesInsight = new Campaign(Name = 'Sales Insight Reports', IsActive = true);
    insert salesInsight;

    Campaign csOut = new Campaign(Name = 'Client Services Outreach', IsActive = true);
    insert csOut;

    Campaign clientCk = new Campaign(Name = 'Client Check-In', IsActive = true);
    insert clientCk;

    Campaign clientInb = new Campaign(Name = 'Client Inbound Contact Us', IsActive = true);
    insert clientInb;

    Campaign tools = new Campaign(Name = 'Tools Supplement', IsActive = true);
    insert tools;

    Campaign chanRef = new Campaign(Name = 'Channel Referral', IsActive = true);
    insert chanRef;

    Campaign incSol = new Campaign(Name = 'Incremental Solution Sell', IsActive = true);
    insert incSol;

    Account a = new Account(Name='Test Account');
    insert a;

    testCon= new Contact (
    AccountId=a.Id,
    FirstName='TestFirstName',
    LastName= 'TestLastName',
    ContactsRole__c='Billing Contact',
    Title='TestTitle',
    CurrencyIsoCode='USD'
    );

}
    static testMethod void  createOppFromContactControllerTestCampaignNotNull()
    {
        setupContact();
        testcon.Most_Recent_Campaign_Id__c=[SELECT Id from Campaign WHERE IsActive=True limit 1].id;
        insert testCon;
        
        ApexPages.StandardController sc = new ApexPages.standardController(testCon);
        System.currentPagereference().getParameters().put('id',testCon.Id);
        createOpportunityFromContactController methods = new createOpportunityFromContactController(sc);
        
        test.startTest();
        methods.createOpp();
        test.stopTest();
    }

    static testMethod void  createOppFromContactControllerTest_salesOut()
    {
        setupContact();
        insert testCon;
        
        ApexPages.StandardController sc = new ApexPages.standardController(testCon);
        System.currentPagereference().getParameters().put('id',testCon.Id);
        createOpportunityFromContactController methods = new createOpportunityFromContactController(sc);
        methods.createOpp();
        Opportunity opp = [SELECT Id FROM Opportunity];
        opp.Opportunity_Source__c = 'Sales Outreach';

        test.startTest();
        methods.updateOpp();
        test.stopTest();
    }

    static testMethod void  createOppFromContactControllerTest_cancel()
    {
        setupContact();
        testcon.Most_Recent_Campaign_Id__c=[SELECT Id from Campaign WHERE IsActive=True limit 1].id;
        insert testCon;
        
        ApexPages.StandardController sc = new ApexPages.standardController(testCon);
        System.currentPagereference().getParameters().put('id',testCon.Id);
        createOpportunityFromContactController methods = new createOpportunityFromContactController(sc);
        methods.createOpp();

        test.startTest();
        methods.Cancel();
        test.stopTest();
    }

}