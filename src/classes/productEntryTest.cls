@isTest
public class productEntryTest {
    
    /*static Account a;
    static Account partner;
    static Opportunity o;
    static PageReference p;
    static ApexPages.standardController std;
    static productEntryController ext;
    static string selected;

    static void init() {
        a = new Account (name = 'test account');
        insert a;
        
        o = new Opportunity(name = 'test opp', closedate = date.today(), accountid = a.id, stagename = 'Demonstrating Capabilities', currencyisocode = 'USD');
        insert o;
        
        p = Page.productEntry;
        p.getParameters().put('id',o.id);
        test.setCurrentPage(p);
        
        std = new ApexPages.StandardController(o);
        ext = new productEntryController(std);      
    }

    static testMethod void test1() {
        init();
        
        test.startTest();
        
        ext.addProd();
        ext.editAccess = true;
        ext.family = 'Certification.EQ';
        ext.getTable();
        
        for (productEntryController.prodChoice pc : ext.prodChoices) {
            if(pc.prodCode == '004-CERT-FEE0') ext.toSelect = pc.pb.id;
        }
        
        ext.addtoShoppingCart();
        
        test.stopTest();
    
    }
        
    static testMethod void test2() {
        
        init(); 
        
        test.startTest();
        
        ext.family = 'Professional Services';
        ext.getTable();
        
        for(productEntryController.prodChoice pc : ext.prodChoices) {
            if(pc.prodCode == '008-PROF-GENC') {
                pc.descrip = 'Deliverability & Technology Consulting: Deliverability Report Card';
                break;
            }
        }
        
        ext.priceProServ();
        
        for (productEntryController.prodChoice pc : ext.prodChoices) {
            if(pc.prodCode == '008-PROF-GENC') ext.toSelect = pc.pb.id;
        }
        
        ext.addToShoppingCart();
        
        test.stopTest();
        
        //system.assertEquals(1,ext.shoppingCart.size());
        
    }

    static testMethod void test3() {
        init();
        
        ext.getProdFamilies();
        ext.getAMServices();
        
        ext.quickUpdateOpp();
        
        ext.editAccess = true;
        ext.addProd();
        ext.family = 'Insight.EQ';
        ext.getTable();
        
        for(productEntryController.prodChoice pc : ext.prodChoices) {
            if(pc.prodCode == '030-DMSU-IBM') {
                ext.toSelect = pc.pb.id;
            }
        }
        
        ext.addToShoppingCart();
        
        test.startTest();
        
        for(productEntryController.cartItem item : ext.shoppingCart) {
            if(item.oli.product_code__c == '030-DMSU-IBM') {
                ext.toEdit = item.oli.id;
                ext.getOli();
                ext.editOli();
                ext.setOli();
                ext.toUnSelect = ext.toEdit;
                ext.removeFromShoppingCart();
                break;
            }
        }
        
        ext.onSave();
        
        ext.onCancel();
        
        test.stopTest();
    }
    
    static testMethod void test4() {
        init();
        ext.editAccess = true;
        
        test.startTest();
        
        ext.family = 'Email Intelligence for Marketers';
        ext.o.Number_of_Platinum_Contacts__c = 2;
        ext.getTable();
        
        for (productEntryController.prodChoice pc : ext.prodChoices) {
            if(pc.prodCode == '002-DMSU-PLAT') {
                ext.toSelect = pc.pb.id;
                ext.addToShoppingCart();
                break;
            }
        }
        
        test.stopTest();
    }
    
    static testMethod void test5() {
        init();
        
        test.startTest();
        ext.getQuickServ();
        ext.seeMore();
        ext.getServices();
        ext.onSave();
        test.stopTest();
    }
    
     static testMethod void test6() {
        init();
        
        ext.getProdFamilies();
        
        ext.editAccess = true;
        ext.addProd();
        ext.family = 'Anti-Phishing Solutions';
        ext.getTable();
        
        for(productEntryController.prodChoice pc : ext.prodChoices) {
            if(pc.prodCode == '022-EMBR-MNTR') {
                ext.toSelect = pc.pb.id;
            }
        }
        
        ext.addToShoppingCart();
        
        test.startTest();
        
        for(productEntryController.cartItem item : ext.shoppingCart) {
            if(item.oli.product_code__c == '022-EMBR-MNTR') {
                ext.toEdit = item.oli.id;
                ext.getOli();
                ext.editOli();
                ext.setOli();
                ext.toUnSelect = ext.toEdit;
                ext.removeFromShoppingCart();
                break;
            }
        }
        
        ext.onSave();
        
        test.stopTest();
    }
    
    static testMethod void test7() {
        init();
        
        ext.getProdFamilies();
        
        ext.editAccess = true;
        ext.addProd();
        ext.family = 'Anti-Phishing Solutions';
        ext.getTable();
        
        for(productEntryController.prodChoice pc : ext.prodChoices) {
            if(pc.prodCode == '024-DOMN-PROT') {
                ext.toSelect = pc.pb.id;
            }
        }
        
        ext.addToShoppingCart();
        
        test.startTest();
        
        for(productEntryController.cartItem item : ext.shoppingCart) {
            if(item.oli.product_code__c == '024-DOMN-PROT') {
                ext.toEdit = item.oli.id;
                ext.getOli();
                ext.editOli();
                ext.setOli();
                ext.toUnSelect = ext.toEdit;
                ext.removeFromShoppingCart();
                break;
            }
        }
        
        ext.onSave();
        
        test.stopTest();
    }
    
    static testMethod void test8() {
        init();
        
        ext.getProdFamilies();
        
        ext.editAccess = true;
        ext.addProd();
        ext.family = 'Anti-Phishing Solutions';
        ext.getTable();
        
        for(productEntryController.prodChoice pc : ext.prodChoices) {
            if(pc.prodCode == '026-DOMN-SECR') {
                ext.toSelect = pc.pb.id;
            }
        }
        
        ext.addToShoppingCart();
        
        test.startTest();
        
        for(productEntryController.cartItem item : ext.shoppingCart) {
            if(item.prodCode == '026-DOMN-SECR') {
                ext.toUnSelect = item.oli.id;
                ext.removeFromShoppingCart();
                break;
            }
        }
        
        ext.onSave();
        
        test.stopTest();
    }
    
     static testMethod void test9() {
        
        init(); 
        
        test.startTest();
        
        ext.family = 'Professional Services';
        ext.getTable();
        
        for(productEntryController.prodChoice pc : ext.prodChoices) {
            if(pc.prodCode == '008-PROF-GENC') {
                pc.descrip = 'Competitor Analysis';
                break;
            }
        }
        
        ext.getVerticals();
        ext.getPanelCust();
        ext.getPanelComp();
        
        ext.priceProServ();
        
        for (productEntryController.prodChoice pc : ext.prodChoices) {
            if(pc.prodCode == '008-PROF-GENC') ext.toSelect = pc.pb.id;
        }
        
        ext.addToShoppingCart();
        
        test.stopTest();
        
        //system.assertEquals(1,ext.shoppingCart.size());
        
    }*/

}