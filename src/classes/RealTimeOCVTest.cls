@isTest
public class RealTimeOCVTest {
    static testMethod void runRealTimeOCVTest() {

        //Run as Sales Ops
        User SalesOps = new User(id = '00500000006olmF');
        System.RunAs(SalesOps){
        
            Account acct = new Account();
            acct.name = 'RPTestAcct';
            Insert acct;
                
            Opportunity opp1 = new Opportunity();
            opp1.AccountId = acct.id; 
            opp1.Name = 'RPTestOpp1'; 
            opp1.StageName = 'Negotiating'; 
            opp1.CloseDate = system.today(); 
            opp1.OwnerId = '00500000006olmF'; 
            opp1.RecordTypeId = '012000000004u5b';
            opp1.Approval_Type__c = 'Pricing Approval';
            opp1.Pricebook2Id = '01s000000004NS6AAM';
            Insert opp1;
                
            OpportunityLineItem prod1 = new OpportunityLineItem();
            prod1.OpportunityId = opp1.id;
            prod1.PricebookEntryId = '01u00000000FGw7AAG';
            prod1.Product_Status__c = 'Current';
            prod1.Quantity = 12;
            prod1.Previous_Value__c = 0;
            prod1.UnitPrice = 1200;
            prod1.Active__c = true;
            prod1.Description__c = 'Deliverability Tools';
            insert prod1;
        
            test.startTest();
            update opp1;
            test.stopTest();
        
            //Opportunity theNewOpp = [select Opportunity_Contract_Value__c from Opportunity where id = :opp1.id];
            //system.assertEquals(14400,theNewOpp.Opportunity_Contract_Value__c);
        
        }
    }
}