public with sharing class UpdateRenewalCertOLI {
	
	/*public static void processTrigger(List<Opportunity> newOpportunities, Map<Id, Opportunity> OldOpps)
	{
		List<Opportunity> filteredOpportunities = filterOpportunitiesOnUpdate(newOpportunities, OldOpps);
		if(!filteredOpportunities.isEmpty())
		{
			List<Opportunity> opportunityWithLineItems = getOLIs(filteredOpportunities);
			processRenewals(opportunityWithLineItems, newOpportunities);
		}
	}
	
	public static void processRenewals(List<Opportunity> filteredOpportunitiesWithLineItems, List<Opportunity> newOpportunities)
	{
		Map<Id, Opportunity> RenewalOpp = getRenewalOpp(filteredOpportunitiesWithLineItems);
		Opportunity Renewal;
		List<Opportunity> UpdateRenewals = new List<Opportunity>();
		List<Opportunity> UpdateClonedOpps = new List<Opportunity>();
		List<OpportunityLineItem> UpdateRenewalOLIs = new List<OpportunityLineItem>();
		List<OpportunityLineItem> DeleteRenewalOLIs = new List<OpportunityLineItem>();
		
		for(Opportunity o : filteredOpportunitiesWithLineItems)
		{
			o.Renewal_Updated__c = true;
			UpdateClonedOpps.add(o);
			
			if(!RenewalOpp.isEmpty() && RenewalOpp.containsKey(o.Id) && o.StageName == 'Finance Approved')
			{
				Renewal = RenewalOpp.get(o.Id); 
				Renewal.Activation_Date__c = o.Activation_Date__c;
				Renewal.Certification_Tier_Sold__c = o.Certification_Tier_Sold__c;
				Renewal.SSC_Volume__c = o.SSC_Volume__c;
				UpdateRenewals.add(Renewal);
				if(Renewal.OpportunityLineItems != null && !Renewal.OpportunityLineItems.isEmpty())
				{
					OpportunityLineItem renewalOLI = Renewal.OpportunityLineItems[0];
					if(o.OpportunityLineItems[0].Start_Date__c != null) 
						renewalOLI.Start_Date__c = o.OpportunityLineItems[0].End_Date__c.addDays(1);
					if(renewalOLI.Start_Date__c != null)
						renewalOLI.End_Date__c = renewalOLI.Start_Date__c.addDays(364);
					renewalOLI.UnitPrice = o.OpportunityLineItems[0].UnitPrice;
					UpdateRenewalOLIs.add(renewalOLI);
				}
			}
			else if(o.StageName == 'Terminated Contract')
			{
				Renewal = RenewalOpp.get(o.Id);
				if(Renewal.OpportunityLineItems != null && !Renewal.OpportunityLineItems.isEmpty())
				{ 
					OpportunityLineItem renewalOLI = Renewal.OpportunityLineItems[0];
					DeleteRenewalOLIs.add(renewalOLI);
				}
				
			}
			
		}
		update UpdateRenewals;
		update UpdateRenewalOLIs;
		delete DeleteRenewalOLIs;
		update UpdateClonedOpps;		 
	}
	
	public static List<Opportunity> filterOpportunitiesOnUpdate (List<Opportunity> newOpportunities, Map<Id, Opportunity> OldOpps) // get updated opps
	{
		List<Opportunity> filteredOpportunities = new List<Opportunity>();
		for(Opportunity o : newOpportunities)
		{
			Opportunity OldOpp = OldOpps.get(o.Id);
			if(o.isClosed == true && o.Cloned_From_ID__c != null && o.Application_Result__c != null && OldOpp.Application_Result__c == null && o.Renewal_Updated__c == false )
			{
				filteredOpportunities.add(o);
			}
		}
		return filteredOpportunities;
	}
	
	public static List<Opportunity> getOLIs (List<Opportunity> filteredOpportunities) // get OLIS related to updated opps
	{
		return[SELECT Id, IsClosed, StageName, Application_Result__c, Cloned_From_Id__c, Activation_Date__c, SSC_Volume__c, Certification_Tier_Sold__c, Renewal_Updated__c, Invoice_Created__c, (SELECT Id, OpportunityId, Start_Date__c, End_Date__c, UnitPrice, Product_Code__c 
		FROM OpportunityLineItems WHERE OpportunityId IN : filteredOpportunities and (Product_Code__c = '004-CERT-FEE0' or Product_Code__c = '005-CERT-REFC')LIMIT 1)
		FROM Opportunity WHERE Id IN : filteredOpportunities];
	}
	
	private static Map<Id, Opportunity> getRenewalOpp( List<Opportunity> filteredOpportunitiesWithLineItems) //Map the updated opp to the open renewal using the Cloned From ID
	{
		Map<Id, Opportunity> OpportunityIdtoRenewal = new Map <Id, Opportunity>();
		List<Id> ClonedFromIds = new List<Id>();

		for(Opportunity o : filteredOpportunitiesWithLineItems)
		{
			if(o.Cloned_From_Id__c != null)
			{
				ClonedFromIds.add(o.Cloned_From_Id__c);
			}
		}
		List<Opportunity> RenewalOpps = [SELECT Id,Application_Result__c, Cloned_From_Id__c, Activation_Date__c, SSC_Volume__c, Certification_Tier_Sold__c, Previous_Opportunity_Lookup__c,
										(SELECT Id, Start_Date__c, End_Date__c, UnitPrice, Product_Code__c 
										FROM OpportunityLineItems WHERE (Product_Code__c = '004-CERT-FEE0' or Product_Code__c = '005-CERT-REFC')LIMIT 1)
										FROM Opportunity WHERE Previous_Opportunity_Lookup__c IN : ClonedFromIds];

		for(Opportunity o : filteredOpportunitiesWithLineItems)
		{
			
			
			for(Opportunity renewal : RenewalOpps)
				{
					if(renewal.Previous_Opportunity_Lookup__c == o.Cloned_From_Id__c)
					{
						OpportunityIdtoRenewal.put(o.Id, renewal);
					}
				}
			
		}
		return OpportunityIdtoRenewal;
	}*/

}