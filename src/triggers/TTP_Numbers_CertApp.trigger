trigger TTP_Numbers_CertApp on Certification_Application__c (before update) {

    for(Certification_Application__c ca : trigger.new){
    
    Certification_Application__c prev = trigger.oldMap.get(ca.Id);
    
    TTPCertUtils util = new TTPCertUtils();
    
    
    if(ca.Application_Status__c != prev.Application_Status__c || ca.Application_Result__c != prev.Application_Result__c || (ca.Activation_Date__c != null && prev.Activation_Date__c == null))
    
    {
    	//Received to Under Review
    	if(ca.Date_Audit_1_Results_Sent__c != null && ca.Date_Received__c != null)
    	{
    		ca.TTP_Received_to_Under_Review__c = util.getDiffBusinessDays(ca.Date_Received__c, ca.Date_Audit_1_Results_Sent__c) - ca.TTP_Days_on_Hold__c;
    	}   
	    //Under Review to Audit Complete
    	if(ca.Date_Audit_2_Passed_with_Limitations__c != null )
    	{
    		if(ca.Date_Audit_1_Results_Sent__c != null)
    		{
    			ca.TTP_Under_Review_to_Audit_Complete__c = util.getDiffBusinessDays(ca.Date_Audit_1_Results_Sent__c, ca.Date_Audit_2_Passed_with_Limitations__c) - ca.TTP_Days_on_Hold__c;
    		}
			//In Progress to Audit Complete
			if(ca.Date_App_Reached_In_Progress__c != null)
			{
				ca.TTP_In_Progress_to_Audit_Complete__c = util.getDiffBusinessDays(ca.Date_App_Reached_In_Progress__c, ca.Date_Audit_2_Passed_with_Limitations__c)- ca.TTP_Days_on_Hold__c;
			}
    	}
	    //Received to Closed
	    if(ca.Application_Closed_Time_Syamp__c != null)
	    { 
			if(ca.Date_Received__c != null)
			{
				ca.TTP_Received_to_Closed__c = util.getDiffBusinessDays(ca.Date_Received__c, ca.Application_Closed_Time_Syamp__c) - ca.TTP_Days_on_Hold__c;
			}
			//Under Review to Closed 
			if(ca.Date_Audit_1_Results_Sent__c != null)
			{	    
			ca.TTP_Under_Review_to_Closed__c = util.getDiffBusinessDays(ca.Date_Audit_1_Results_Sent__c, ca.Application_Closed_Time_Syamp__c) - ca.TTP_Days_on_Hold__c;
			}
			// Fixes Needed to Closed
	    	if(ca.Date_App_Reached_Fixes_Needed__c != null)
	    	{
	    	ca.TTP_Fixes_Needed_to_Closed__c = util.getDiffBusinessDays(ca.Date_App_Reached_Fixes_Needed__c, ca.Application_Closed_Time_Syamp__c) - ca.TTP_Days_on_Hold__c;
	    	}
	    }	
	    //Received to Active    
    	if(ca.Activation_Date__c != null && ca.Date_Received__c != null)
    	{
    		ca.TTP_Received_to_Active__c = util.getDiffBusinessDays(ca.Date_Received__c, ca.Activation_Date__c)- ca.TTP_Days_on_Hold__c;   
    	}
	    //Investigating to In Progress   
		if(ca.Date_App_Reached_Investigating__c != null && ca.Date_App_Reached_In_Progress__c != null)
		{
			ca.TTP_Investigating_to_In_Progress__c = util.getDiffBusinessDays(ca.Date_App_Reached_Investigating__c, ca.Date_App_Reached_In_Progress__c) - ca.TTP_Days_on_Hold__c;  
		}
	    //Received to Fixes Needed	
	    if(ca.Date_App_Reached_Fixes_Needed__c != null && ca.Date_Received__c != null)
	    {    
			ca.TTP_Received_to_Fixes_Needed__c = util.getDiffBusinessDays(ca.Date_Received__c, ca.Date_App_Reached_Fixes_Needed__c) - ca.TTP_Days_on_Hold__c;
	    }   
	    //In Progress to Fixes Needed
	    if(ca.Date_App_Reached_Fixes_Needed__c != null && ca.Date_App_Reached_In_Progress__c != null)
	    {	    
			ca.TTP_In_Progress_to_Fixes_Needed__c = util.getDiffBusinessDays(ca.Date_App_Reached_In_Progress__c, ca.Date_App_Reached_Fixes_Needed__c) - ca.TTP_Days_on_Hold__c;
	    }   
	   
	        
	}
}
}