@isTest (seeAllData=True)

private class TestAutoSubmitCertApps {
    Static Account a;
    Static Customer_Product_Detail__c p;
    Static List <Opportunity> testOpps;
    /*Static RecordType obtaining {
        get{
            if(obtaining == null){
                obtaining = [SELECT ID FROM RecordType WHERE DeveloperName = 'Obtaining_Commitment'];
            }
            return obtaining;
            }
            private set;
    }*/
    Static PriceBook2 pb;
    Static Product2 testProduct;
    Static PriceBookEntry certProduct;

    static void setupMethod(){
    List <OpportunityLineItem> testOlis = new List <OpportunityLineItem>();
    //Build Account
    a = TestUtils.createAccount(TestUtils.MARKETER_RECORD_TYPE.id, True);
        //a.SSC_New_Tier__c = 'Tier 4';
        a.Cert_Active_Subscriptions__c = 1;
        update a;
        
    //Build PPC    
    /*p = TestUtils.createProductPortfolio(a.Id, 'Certification - License', false);
        p.Certification_Tier__c = 'Tier 4';
        p.Product_Family__c = 'Certification.EQ';
        p.Product_Status__c = 'Active';
        insert p;
        update a;*/
    
    //Build Test Opportunities
    testOpps = TestUtils.createOpportunities(2, TestUtils.PURCHASE_RECORD_TYPE.id, a.Id, False);
        for(Opportunity opp : testOpps){
            opp.type = 'Renewal';
            //opp.RecordTypeId = obtaining.Id;
            opp.Certification_Tier_Sold__c = 'Tier 3';
            opp.StageName = 'Signature';
            opp.Auto_Submit_WFR_Fired__c = FALSE;
            opp.CloseDate = system.today().addDays(20);
            
        }
        insert testOpps;
    
    //Build product and Opportunity Line Item
    pb = TestUtils.STANDARD_PRICE_BOOK;
        
        testProduct = TestUtils.createProduct(false);
        testProduct.Family = 'Certification';
        testProduct.Name = 'Certification - License';
        insert testProduct;
        
        certProduct = TestUtils.createPricebookEntry(testProduct.Id, pb.Id, true);
        
        for(Opportunity opp : testOpps){
            OpportunityLineItem testOli = TestUtils.createOpportunityLineItem(opp.Id, certProduct.Id, false);
            testOli.Previous_Value__c = 2000;
            testOli.UnitPrice = 2000;
            
            testOlis.add(testOli);
        }
        
        insert testOlis;
        update testOpps;
        
        List<Opportunity> insertedOpps = [SELECT auto_submit_cert_only__c, StageName, Type, Inc_ACV_Before_Partner_Comm__c, AccountId, Certification_Tier_Sold__c, Has_Cert_Products_Qty__c, All_Products_Count__c, Submit_From_Trigger__c, Cert_Tier_High_or_Low__c FROM Opportunity WHERE ID IN :testOpps]; 
        Account acct = [SELECT Cert_Only_Client__c, SSC_New_Tier__c FROM Account WHERE ID =: insertedOpps[0].accountID];
      
        system.assertEquals('Signature',insertedOpps[0].StageName, 'We expect Signature');
        system.assertEquals('Renewal', insertedOpps[0].Type, 'We expect renewal');
        system.assertEquals(0.00, insertedOpps[0].Inc_ACV_Before_Partner_Comm__c, 'We expect no INC ACV');
        system.assertEquals('Tier 3', insertedOpps[0].Certification_Tier_Sold__c, 'We expect Tier 4');
        system.assertEquals(1, insertedOpps[0].Has_Cert_Products_Qty__c, 'We expect 1');
        system.assertEquals(1, insertedOpps[0].All_Products_Count__c, 'We expect 1 total product');
        system.assertEquals(null,insertedOpps[0].Submit_From_Trigger__c, 'We expect null value');
        system.assertEquals('Low',insertedOpps[0].Cert_Tier_High_or_Low__c, 'We expect Low');
        system.debug('Opportunity Is' + insertedOpps[0]);
        system.debug('Account is' + acct);
        system.assertEquals('Yes', insertedOpps[0].auto_submit_cert_only__c, 'We expect the formula to be yes');
        
    }
    
    static testMethod void testAutoSubmitCert(){
        setupMethod();
        system.debug('Setup method ran');
        test.startTest();
        OpportunityServices.autoSubmitCertApps();
        test.stopTest();
        system.debug('Auto Submit logic ran');
        List<Opportunity> queriedOpps = [SELECT Id, Submit_From_Trigger__c, Auto_Submit_WFR_Fired__c FROM Opportunity WHERE ID IN: testOpps];
        for(Opportunity opp : queriedOpps){
            system.assertEquals(true, opp.Auto_Submit_WFR_Fired__c,'We expect field to be True');
           //We expect No because the SubmitFromTrigger logic will update the value to No after it submits opp
            system.assertEquals('No', opp.Submit_From_Trigger__c,'We expect value of field to be No');
            
        }
        
    }
        
    
    
       
}