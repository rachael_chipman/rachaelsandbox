@isTest

public with sharing class OnHoldReopenControllerTest 
{
	
	public static Account acct;
	public static Opportunity opp;
	public static Certification_Application__c certApp;
	
	public static void setupTestObjects()
	{
		  c2g__codaGeneralLedgerAccount__c GenLedge = new c2g__codaGeneralLedgerAccount__c (Name = 'Test', c2g__ReportingCode__c = '1234', c2g__Type__c = 'Balance Sheet');
		  c2g__codaGeneralLedgerAccount__c GenLedge2 = new c2g__codaGeneralLedgerAccount__c (Name = 'Test', c2g__ReportingCode__c = '1350', c2g__Type__c = 'Balance Sheet');
		  c2g__codaGeneralLedgerAccount__c GenLedge3 = new c2g__codaGeneralLedgerAccount__c (Name = 'Test', c2g__ReportingCode__c = '5000', c2g__Type__c = 'Balance Sheet');
		  insert new List<c2g__codaGeneralLedgerAccount__c>{GenLedge, GenLedge2, GenLedge3};
		
		  c2g__codaIncomeScheduleDefinition__c incsched = new c2g__codaIncomeScheduleDefinition__c (Name = 'Income Schedule', c2g__GeneralLedgerAccount__c = GenLedge.Id);
		  insert incsched;
		  
		  c2g__codaCompany__c financeAccount = new c2g__codaCompany__c( c2g__TaxIdentificationNumber__c = '06-1566593', Name = 'TestAccount' );
		  insert financeAccount;
		
		acct = new Account();
        acct.name = 'RPTestAcct1';
        acct.accountmanager__c = 'BIO Automation';
        acct.Customer_Relationship_Manager_Primary__c = 'BIO Automation';
        insert acct;
        
        opp = new Opportunity();
        opp.AccountId = acct.Id;
        opp.Name = 'Test Opp';
        opp.CloseDate = system.today();
        opp.Type = 'New Business';
        opp.StageName = 'Signature';
        opp.Pricebook2Id = '01s000000004NS6AAM';
        opp.Partner_Commission__c = .25;
        opp.CurrencyISOCode = 'USD';
        opp.Deal_Closing_Through__c = 'Partner';
        opp.Who_Is_Invoiced__c = 'Partner';
        opp.CurrencyISOCode = 'USD';
        opp.Nature_of_Involvement__c = 'Yes';
        insert opp;
        
        certApp = new Certification_Application__c();
        certApp.Account__c = acct.Id;
        certApp.Application_Status__c = 'Application Submitted';
        certApp.License_Fee_Opportunity__c = opp.Id;
        certApp.Certification_Volume__c = 'Tier 3';
        certApp.Certification_Max_Volume__c = 50000;
        certApp.Bond_Group_ID__c = '1234567';
	}
	
	public static testMethod void testOnHoldController()
	{
		pageReference pageRef = page.OnHold;
		Test.setCurrentPageReference(pageRef);
		
		setupTestObjects();
		certApp.Application_Status__c = 'Under Review';
		certApp.Application_Result__c = 'In Progress';
		insert certApp;
		
		ApexPages.Standardcontroller sc = new ApexPages.standardController(certApp);
		System.currentPageReference().getParameters().put('id', certApp.Id);
		OnHoldPageController methods = new OnHoldPageController(sc);
				
		test.startTest();
		methods.checkStatus();
		methods.SaveandBacktoApp();
		test.stopTest();
		
		Certification_Application__c testApp = [SELECT Id, Application_Status__c, Application_Result__c, Date_to_On_Hold__c, Record_Locked__c FROM Certification_Application__c WHERE Id = :certApp.Id]; 
		
		system.AssertEquals('On Hold', testApp.Application_Status__c, 'Status should be On Hold');
		system.AssertEquals('On Hold', testApp.Application_Result__c, 'Result should be On Hold');
		system.AssertEquals(system.today(), testApp.Date_to_On_Hold__c, 'Date to On Hold should be today');
		system.AssertEquals(true, testApp.Record_Locked__c, 'Record should be locked');	
	}
	
	public static testMethod void testReOpenController()
	{
		pageReference pageRef = page.ReOpenApplication;
		Test.setCurrentPageReference(pageRef);
		
		setupTestObjects();
		certApp.Application_Status__c = 'On Hold';
		certApp.Application_Result__c = 'On Hold';
		insert certApp;
		
		ApexPages.Standardcontroller sc = new ApexPages.standardController(certApp);
		System.currentPageReference().getParameters().put('id', certApp.Id);
		ReOpenPageController methods = new ReOpenPageController(sc);
				
		test.startTest();
		methods.ReOpen();
		test.stopTest();
		
		Certification_Application__c testApp = [SELECT Id, Application_Status__c, Application_Result__c, Date_to_On_Hold__c, Record_Locked__c FROM Certification_Application__c WHERE Id = :certApp.Id]; 
		
		system.AssertEquals('Under Review', testApp.Application_Status__c, 'Status should be Under Review');
		system.AssertEquals('In Progress', testApp.Application_Result__c, 'Result should be In Progress');
	}
	
	public static testMethod void testReOpenController2()
	{
		pageReference pageRef = page.ReOpenApplication;
		Test.setCurrentPageReference(pageRef);
		
		setupTestObjects();
		insert certApp;
		
		ApexPages.Standardcontroller sc = new ApexPages.standardController(certApp);
		System.currentPageReference().getParameters().put('id', certApp.Id);
		ReOpenPageController methods = new ReOpenPageController(sc);
				
		test.startTest();
		methods.ReOpen();
		test.stopTest();
		
		Certification_Application__c testApp = [SELECT Id, Application_Status__c, Application_Result__c, Date_to_On_Hold__c, Record_Locked__c FROM Certification_Application__c WHERE Id = :certApp.Id]; 
		
		system.AssertEquals('Application Submitted', testApp.Application_Status__c, 'Status should be Application Submitted');
	}
}