@ isTest(SeeAllData=true)
Private class OppTeamMemberExRateUpdate_Test{

  static Account testAccount;
  static List<Opportunity> testOpps;
  static User testUser;
  static User testMemberRD;
  static UserTeamMember teamMemberRD;
  
  private static void setUpObjects()
  {
  	testAccount = TestUtils.createAccount(TestUtils.MARKETER_RECORD_TYPE.Id, true);
  	
  	testOpps = TestUtils.createOpportunities(201, TestUtils.RESEARCH_RECORD_TYPE.Id, testAccount.Id, false);
  	
  	/*testUser = TestUtils.getTestUser('TestUser', 'Sales');
  	insert testUser;*/

    testUser = [SELECT ID FROM User WHERE ID = '00500000007Em7P'];
  	
  	testMemberRD = TestUtils.getTestUser('TeamMember', 'Regional Director');
  	insert testMemberRD;
  		
  }
  
  private static testMethod void insertBulkOpps()
  {
  	setupObjects();
  	
  	system.RunAs(TestUtils.SALES_USER)
  	{
  		test.startTest();
  			for(Opportunity testOpp : testOpps)
  			{
  				testOpp.StageName = 'Pre-Pipeline';
  				testOpp.CurrencyIsoCode = 'GBP';
  			}
  			insert testOpps;
  		test.stopTest();
  	}
  	
  	
  	List<DatedConversionRate> conversionRates = [SELECT ConversionRate, IsoCode FROM DatedConversionRate WHERE StartDate = THIS_MONTH AND IsoCode ='GBP'];
  	List<CurrencyType> currencyTypes = [SELECT ConversionRate, IsoCode from CurrencyType where IsoCode = 'GBP'];
  	
  	List<OpportunityTeamMember> testMembers = [SELECT Id, CurrencyIsoCode, Exchange_Rate__c, Company_Exchange_Rate__c FROM OpportunityTeamMember WHERE OpportunityId IN : testOpps];
  	system.assert(testMembers.size() > 0 );
  	for(OpportunityTeamMember testMember : testMembers)
  	{
  		system.assertEquals('USD', testMember.CurrencyIsoCode, 'The ISOcode on the members should be set to USD');
  		system.assertEquals(conversionRates[0].ConversionRate, testMember.Exchange_Rate__c, 'The exchange rate should match the appropriate dated conversion rate');
  		system.assertEquals(currencyTypes[0].ConversionRate, testMember.Company_Exchange_Rate__c,'The company exchange rate should match the appropriate currency types conversion rate');
  	}
  	
  }
  
    private static testMethod void updateBulkOpps()
  {
  	setupObjects();
  	
  	system.RunAs(TestUtils.SALES_USER)
  	{
  		for(Opportunity testOpp : testOpps)
  		{
	  		testOpp.CurrencyIsoCode = 'GBP';
  		}
  		insert testOpps;
  		
  	}	
  	
  	List<Opportunity> insertedOpps = [SELECT Id, CurrencyISOCode FROM Opportunity WHERE Id IN: testOpps];
  	test.startTest();
  		for(Opportunity testOpp : insertedOpps)
  		{
  			testOpp.StageName = 'Finance Approved';
  		}
  		update insertedOpps;
  	test.stopTest();
  	
  	
  	List<DatedConversionRate> conversionRates = [SELECT ConversionRate, IsoCode FROM DatedConversionRate WHERE StartDate = THIS_MONTH AND IsoCode ='GBP'];
  	List<CurrencyType> currencyTypes = [SELECT ConversionRate, IsoCode from CurrencyType where IsoCode = 'GBP'];
  	
  	List<OpportunityTeamMember> testMembers = [SELECT Id, CurrencyIsoCode, Exchange_Rate__c, Company_Exchange_Rate__c FROM OpportunityTeamMember WHERE OpportunityId IN : testOpps];
  	system.assert(testMembers.size() > 0 );
  	for(OpportunityTeamMember testMember : testMembers)
  	{
  		system.assertEquals('USD', testMember.CurrencyIsoCode, 'The ISOcode on the members should be set to USD');
  		system.assertEquals(conversionRates[0].ConversionRate, testMember.Exchange_Rate__c, 'The exchange rate should match the appropriate dated conversion rate');
  		system.assertEquals(currencyTypes[0].ConversionRate, testMember.Company_Exchange_Rate__c, 'The company exchange rate should match the appropriate currency types conversion rate');
  	}
  	
  }
  
  private static testMethod void testFilterMethod_Negative()
  {
  		Account testAccount2 = TestUtils.createAccount(TestUtils.MARKETER_RECORD_TYPE.Id, true);
  		Opportunity testOpportunity = TestUtils.createOpportunity(TestUtils.RESEARCH_RECORD_TYPE.Id, testAccount2.Id, true);
  		testOpportunity.StageName = 'Random';
        testOpportunity.Type = 'New Business';
        update testOpportunity;
        
        // Old Map contents
        Map<Id, Opportunity> oldOppMap = new Map<Id, Opportunity>{ testOpportunity.Id => testOpportunity };
        //oldOppMap.put( testOpportunity.Id, testOpportunity );
        
        //New list Contents
        Opportunity copyTestOpp = testOpportunity.Clone(true, true);
        testOpportunity.StageName = 'Random';
        List< Opportunity> newOppList = new List<Opportunity>{copyTestOpp};

        Test.startTest();
            List<Opportunity> filterOpps = OTMExchangeRateService.filterOpportunitiesOnInsertAndUpdate( newOppList, oldOppMap );
        Test.stopTest();
        
        System.assertEquals( 0, filterOpps.size(), 'There should be no opportunity returned');
  }
}