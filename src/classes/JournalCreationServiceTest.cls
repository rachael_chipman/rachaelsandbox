@isTest
public class JournalCreationServiceTest 
{
	final static String USD_CURRENCY_CODE = 'USD';
	public static Opportunity testOpportunity;
	public static List<OpportunityLineItem> testOLIs;
	public static List<c2g__codaInvoice__c> testSalesInvoices;
	public static Account testAccount;
	public static Product2 testProduct;
	public static Account testPartner;

	public static void setupOppAndOLI()
	{
		final Decimal RANDOM_UNIT_PRICE = 10.00;
		String defaultCurrency = USD_CURRENCY_CODE;
		Date defaultOliStartDate = Date.today();
		setupOppAndOLI( RANDOM_UNIT_PRICE, defaultCurrency, defaultCurrency, defaultOliStartDate );
	}

	public static void setupOppAndOLI( Decimal oliUnitPrice, String oppCurrencyCode, String partnerCurrencyCode, Date oliStartDate )
	{
        c2g__codaGeneralLedgerAccount__c testGeneralLedgerAccount = TestUtils.createGeneralLedgerAccount(true);
        testAccount = TestUtils.createAccount_FinancialForce( TestUtils.MARKETER_RECORD_TYPE.Id, testGeneralLedgerAccount.Id, false );
        testAccount.CurrencyISOCode = oppCurrencyCode;
        testPartner = TestUtils.createAccount_FinancialForce( TestUtils.PARTNER_RECORD_TYPE.Id, testGeneralLedgerAccount.Id, false );
        testPartner.CurrencyISOCode = partnerCurrencyCode;
        insert new List<Account>{ testAccount, testPartner };

        RecordType opportunityRecordType = [ SELECT Id FROM RecordType WHERE SObjectType = 'Opportunity' AND IsActive = TRUE LIMIT 1 ];
        testOpportunity = TestUtils.createOpportunity( opportunityRecordType.Id, testAccount.Id, false);
        testOpportunity.PartnerLookup__c = testPartner.Id;
        testOpportunity.Who_is_Invoiced__c = OpportunityServices.PARTNER_INVOICED;
        testOpportunity.Deal_Closing_Through__c = OpportunityServices.DEAL_CLOSING_THROUGH_RP;
        insert testOpportunity;
        
        testProduct = TestUtils.createProduct_FinancialForce( testGeneralLedgerAccount.Id, false );
		Product2 testProduct_Referral = TestUtils.createProduct_FinancialForce( testGeneralLedgerAccount.Id, false );
		testProduct_Referral.ProductCode = OpportunityServices.PRODUCT_CODE_REFERRAL;
		insert new List<Product2>{ testProduct, testProduct_Referral };
		PriceBookEntry testPBE = TestUtils.createPricebookEntry( testProduct.Id, TestUtils.STANDARD_PRICE_BOOK.Id, false );
		PriceBookEntry testPBE_Referral = TestUtils.createPricebookEntry( testProduct_Referral.Id, TestUtils.STANDARD_PRICE_BOOK.Id, false );
		insert new List<PricebookEntry>{ testPBE, testPBE_Referral };
        
        testOLIs = TestUtils.createOpportunityLineItems(testOpportunity.Id, new List<PricebookEntry>{ testPBE, testPBE_Referral }, false );
        for( OpportunityLineItem anOLI : testOLIs )
        {
        	anOLI.UnitPrice = oliUnitPrice;
        	anOLI.Start_Date__c = oliStartDate;
        }
        insert testOLIs;
	}

	public static void setupInvoice( Integer numOfInvoices, Date startDate, Date endDate )
	{
		Map<String, Object> additionalFieldValues = new Map<String, Object>();
		additionalFieldValues.put( 'c2g__Opportunity__c', testOpportunity.Id );
		additionalFieldValues.put( 'Contract_Service_Period_Start_Date__c', startDate );
		additionalFieldValues.put( 'Contract_Service_Period_End_Date__c', endDate );

        testSalesInvoices = TestUtils.createSalesInvoices(numOfInvoices, testAccount.id, additionalFieldValues, false );
        for( c2g__codaInvoice__c testSalesInvoice : testSalesInvoices )
		{
			testSalesInvoice.Ready_For_Journals__c = true;
			testSalesInvoice.Opportunity_Exchange_Rate__c = 1.0;
			testSalesInvoice.Partner_Exchange_Rate__c = 1.0;
			testSalesInvoice.Partner_Commission_Amount__c = testOLIs[0].UnitPrice/numOfInvoices;
		}
		insert testSalesInvoices;
		List<c2g__codaInvoiceLineItem__c> testSalesInvoiceLineItems = new List<c2g__codaInvoiceLineItem__c>();
 		for( c2g__codaInvoice__c testSalesInvoice: testSalesInvoices )
		{
			testSalesInvoiceLineItems.add( TestUtils.createSalesInvoiceLineItem( testSalesInvoice.Id, testProduct.Id, false ) );
		}  
		insert testSalesInvoiceLineItems;  
	}

	@isTest(seeAllData=true)
	private static void testCreateJournals()
	{
		Date startDate = Date.newInstance( 2084, 01, 05 );
		Date endDate = Date.newInstance( 2084, 05, 15 );
		Decimal expectedNoOfJournals = 6;
		List<Date> expectedMonthlyJournalDates = new List<Date>{ Date.newInstance( 2084, 01, 05 ), Date.newInstance( 2084, 02, 01 ), Date.newInstance( 2084, 03, 01 ), Date.newInstance( 2084, 04, 01 ), Date.newInstance( 2084, 05, 01 ) };
		setupOppAndOLI();
		setupInvoice( 1, startDate, endDate );
		test.startTest();
			List<c2g__codaJournal__c> actualJournals = JournalCreationService.createJournals( testSalesInvoices[0], TestUtils.ACCOUNTING_CURRENCY_USD.Id );
		test.stopTest();
		System.assertEquals( expectedNoOfJournals, actualJournals.size(), 'All the expected number of journals should be returned' );
		for( c2g__codaJournal__c aJournal : actualJournals )
		{
			System.assertEquals( JournalCreationService.MANUAL_JOURNAL, aJournal.c2g__Type__c, 'All the journals created should be of type manual' );
			System.assertEquals( testSalesInvoices[0].Id, aJournal.Sales_Invoice__c, 'All the journals created should have sales invoice populated with test sales invoice' );
			System.assertEquals( TestUtils.ACCOUNTING_CURRENCY_USD.Id, aJournal.c2g__JournalCurrency__c, 'All the journals created should have the right account currency' );
			System.assertEquals( testOpportunity.Id, aJournal.Opportunity__c, 'All the journals created should have opportunity lookup populated with invoice\'s opportunity' );
			System.assertEquals( false, aJournal.ffgl__DeriveCurrency__c, 'All the journals created should have derive currency checbox set to false' );
		}
		System.assertEquals( Date.newInstance( 2084, 01, 05 ), actualJournals[0].c2g__JournalDate__c, 'The first journal should be the full journal with the invocie contract start Date' );
		for( Integer i = 0; i < expectedMonthlyJournalDates.size(); i++ )
		{
			System.assertEquals( expectedMonthlyJournalDates[i], actualJournals[i+1].c2g__JournalDate__c, 'There should be a monthly journal created with each expected Date' );
		}
	}

	private static void testCreateJournalLineItems()
	{
		final Decimal PRICE = 10.0;
		List<c2g__codaJournal__c> testJournals = TestUtils.createJournals( 2, new Map<String, Object>{ 'Opportunity__c' => testOpportunity.Id }, true );
		c2g__codaJournal__c fullJournal = testJournals[0];
		c2g__codaJournal__c monthlyJournal = testJournals[1];

		test.startTest();
			List<c2g__codaJournalLineItem__c> actualJournalLineItems = JournalCreationService.createFullJournalLineItems( fullJournal, PRICE );
			actualJournalLineItems.addAll( JournalCreationService.createMonthlyJournalLineItems( monthlyJournal, PRICE ) );
		test.stopTest();
		System.assertEquals( 4, actualJournalLineItems.size(), 'There should be 2 journal line items created for each journal ' );
		for( c2g__codaJournalLineItem__c aJLI : actualJournalLineItems )
		{
			System.assertEquals( JournalCreationService.LINE_TYPE_GENERAL_LEDGER, aJLI.c2g__LineType__c, 'All the journal line items created should have the line type set to appropriate value' );
			System.assert( aJLI.c2g__Journal__c == fullJournal.Id || aJLI.c2g__Journal__c == monthlyJournal.Id, 'All the journal line items created should be for eaither full journal or monthly journal' );
			
			if( aJLI.c2g__Journal__c == fullJournal.Id )
			{
				System.assert( JournalCreationService.COMMISSIONS_PAYABLE_REFERRAL_GLA.Id == aJLI.c2g__GeneralLedgerAccount__c ||
								JournalCreationService.DEFERRED_COMMISSION_GLA.Id == aJLI.c2g__GeneralLedgerAccount__c, 'All the full journal line items created should have COMMISSIONS_PAYABLE_REFERRAL General ledger or DEFERRED_COMMISSION General Ledger' );

				if( JournalCreationService.COMMISSIONS_PAYABLE_REFERRAL_GLA.Id == aJLI.c2g__GeneralLedgerAccount__c  )
				{
					System.assertEquals( PRICE*(-1), aJLI.c2g__Value__c, 'All the full journal line items for COMMISSIONS_PAYABLE_REFERRAL general ledger should have negated amount ' );
				}
				else
				{
					System.assertEquals( PRICE, aJLI.c2g__Value__c, 'All the journal line items created should have the right unit price ' );
				}
			}
			else
			{
				System.assert( JournalCreationService.DEFERRED_COMMISSION_GLA.Id == aJLI.c2g__GeneralLedgerAccount__c ||
								JournalCreationService.COMMISSIONS_PARTNERS_GLA.Id == aJLI.c2g__GeneralLedgerAccount__c, 'All the journal line items created should ' );
				if( JournalCreationService.DEFERRED_COMMISSION_GLA.Id == aJLI.c2g__GeneralLedgerAccount__c  )
				{
					System.assertEquals( PRICE*(-1), aJLI.c2g__Value__c, 'All the monthly journal line items for DEFERRED_COMMISSION general ledger should have negated amount ' );
				}
				else
				{
					System.assertEquals( PRICE, aJLI.c2g__Value__c, 'All the journal line items created should have the right unit price ' );
					
				}

			}

		}
	}

	private static testMethod void testGetNumberOfMonthlyJournalsCount_SameYear()
	{
		Date startDate = Date.newInstance( 2014, 01, 05 );
		Date endDate = Date.newInstance( 2014, 05, 15 );
		Integer expectedCount = 5;

		test.startTest();
			Integer actualNoOfJournals = JournalCreationService.getNumberOfMonthlyJournalsCount( startDate, endDate );
		test.stopTest();
		System.assertEquals( expectedCount, actualNoOfJournals, 'The number of journals should be equal to num of months between start date and end date inclusive' );
	}

	private static testMethod void testGetNumberOfMonthlyJournalsCount_DifferentYear()
	{
		Date startDate = Date.newInstance( 2014, 10, 05 );
		Date endDate = Date.newInstance( 2015, 02, 15 );
		Integer expectedCount = 5;

		test.startTest();
			Integer actualNoOfJournals = JournalCreationService.getNumberOfMonthlyJournalsCount( startDate, endDate );
		test.stopTest();
		System.assertEquals( expectedCount, actualNoOfJournals, 'The number of journals should be equal to num of months between start date and end date inclusive' );
	}

	private static testMethod void testGetJournalLineItemPrices() 
	{
		Decimal UNIT_PRICE = 120.0;
		Date startDate = Date.newInstance( 2014, 01, 05 );
		Date endDate = Date.newInstance( 2014, 05, 15 );
		Decimal priceperday = UNIT_PRICE/( startDate.daysBetween( endDate ) + 1);

		Integer expectedNoOfPrices = 5;
		Decimal expectedPrice_1 = ( ( 31 - 5 + 1 ) * priceperday ).setScale( 2 );
		Decimal expectedPrice_2 = ( 28 * priceperday ).setScale( 2 ); 
		Decimal expectedPrice_3 = ( 31 * priceperday ).setScale( 2 );
		Decimal expectedPrice_4 = ( 30 * priceperday ).setScale( 2 );
		Decimal expectedPrice_5 = ( 15 * priceperday ).setScale( 2 );

		List<Decimal> expectedPrices = new List<Decimal>{ expectedPrice_1, expectedPrice_2, expectedPrice_3, expectedPrice_4, expectedPrice_5 };
		test.startTest();
			List<Decimal> actualPrices = JournalCreationService.getJournalLineItemPrices( UNIT_PRICE, startDate, endDate, JournalCreationService.getNumberOfMonthlyJournalsCount( startDate, endDate ), 1.0, 1.0 );
		test.stopTest();
		system.assertEquals( expectedNoOfPrices, actualPrices.size() );
		for( Integer i = 0; i < expectedNoOfPrices; i++ )
		{
			System.assertEquals( expectedPrices[i], actualPrices[i], 'The returned prices should match with the expected price' + i );
		}
	}
}