public with sharing class ProductFamilyToSubscriptionCharge {

	public static list<Zuora__SubscriptionProductCharge__c> filterNewSubscriptionCharges(List<Zuora__SubscriptionProductCharge__c> newCharges)
	{
		Select.Filter myFilter = Select.Record.isNew();
		return myFilter.filter(newCharges);
	}

	public static Map<Id, String> getFamily(List<Zuora__SubscriptionProductCharge__c> filteredCharges)
	{
		Map<Id, String> chargeToFamily = new Map<Id, String>();
		Set<String> ratePlanNames = pluck.strings('Zuora__RatePlanName__c', filteredCharges);
		system.debug('Rate Plan Names ' + ratePlanNames);

		List<zqu__ProductRatePlan__c> ratePlanQuery = [SELECT Id, Name, ProductFamily__c, zqu__ZProduct__r.zqu__Deleted__c, zqu__ZProduct__r.Name
													   FROM zqu__ProductRatePlan__c WHERE Name IN :ratePlanNames AND zqu__Deleted__c = false AND 
													   zqu__ZProduct__r.zqu__Deleted__c = false AND (NOT zqu__ZProduct__r.Name LIKE '[DoNotUse]%')];

		Map<String, List<zqu__ProductRatePlan__c>> ratePlanMap = groupBy.strings('Name', ratePlanQuery); 
		system.debug('Rate Plan Map ' + ratePlanMap);
		for(Zuora__SubscriptionProductCharge__c charge : filteredCharges)
		{
			if(ratePlanMap.containsKey(charge.Zuora__RatePlanName__c))
			{
				system.debug('Adding to charge to family map...');
				//Review this map in code review because it would be better to have the ratePlanMap be String to String instead of to a list of objects
				chargeToFamily.put(charge.Id, ratePlanMap.get(charge.Zuora__RatePlanName__c)[0].ProductFamily__c);
			}
		}
		system.debug('Is there a charge to family? ' + chargeToFamily);
		return chargeToFamily;
	}

	public static void processCharges(List<Zuora__SubscriptionProductCharge__c> filteredCharges)
	{
		Map<Id, String> chargeIdToProductFamily = getFamily(filteredCharges);
		Map<Id, Zuora__SubscriptionProductCharge__c> chargesToUpdate = new Map<Id, Zuora__SubscriptionProductCharge__c>();
		Map<Id,Zuora__Subscription__c> updateSubscriptions = new Map<Id,Zuora__Subscription__c>();
		Set<String> chargeNames = new Set<String>();

		for(Zuora__SubscriptionProductCharge__c charge : filteredCharges)
		{
			if(!chargeIdToProductFamily.isEmpty() && chargeIdToProductFamily.containsKey(charge.Id))
			{
				system.debug('In product charge update');
				Zuora__SubscriptionProductCharge__c placeHolder = new Zuora__SubscriptionProductCharge__c(Id=charge.Id);
				placeHolder.Product_Family__c = chargeIdToProductFamily.get(charge.Id);
				if(!chargesToUpdate.containsKey(placeHolder.Id))
				{
					system.debug('Adding charge to map');
					chargesToUpdate.put(placeHolder.Id, placeHolder);
				}
			}
			//If we have a cert charge we want to bring the Tier over to the quote
			if(charge.Name.contains(CertificationOverageGeneration.CERT) && charge.Zuora__Quantity__c != NULL)
			{
				system.debug('charge is cert license...');
				Zuora__Subscription__c certSub = new Zuora__Subscription__c(Id = charge.Zuora__Subscription__c);
				certSub.Certification_Tier__c = 'Tier ' + CertificationOverageGeneration.roundTheTotalVolumeToNextTier(charge.Zuora__Quantity__c);
				if(!updateSubscriptions.containsKey(certSub.Id))
				{
					updateSubscriptions.put(certSub.Id, certSub);
				}
			}
		}
		try
		{
			update chargesToUpdate.values();
		}
		catch (System.DmlException e)
		{
			for(Integer i = 0; i < e.getNumDml(); i++)
			{
				chargesToUpdate.values()[ e.getDmlIndex(i) ].addError( e.getDmlMessage(i));
                system.debug('What is the error message (charge)' + e.getDmlMessage(i));
			}
		}
		try
		{
			update updateSubscriptions.values();
		}
		catch (System.DmlException e)
		{
			for(Integer i = 0; i < e.getNumDml(); i++)
			{
				Map<Id, List<Zuora__SubscriptionProductCharge__c>> subIdToCharges = groupBy.Ids('Zuora__Subscription__c', chargesToUpdate.values());
				Id errorSubId = updateSubscriptions.values()[ e.getDmlIndex(i) ].Id;
				subIdToCharges.get(errorSubId)[0].addError( e.getDmlMessage(i));
                system.debug('What is the error message (subscription)' + e.getDmlMessage(i));
			}
		}
	}

	public static void updateChargesWithFamily(List<Zuora__SubscriptionProductCharge__c> filteredCharges)
	{
		List<Zuora__SubscriptionProductCharge__c> chargesToUpdate = filterNewSubscriptionCharges(filteredCharges);
		system.debug('Any charges to Update?' + chargesToUpdate);
		if(!chargesToUpdate.isEmpty())
		{
			processCharges(chargesToUpdate);
		}
	}
}