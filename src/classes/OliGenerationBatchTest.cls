@isTest(SeeAllData=true)
private class OliGenerationBatchTest {

	private static Pricebook2 testPricebook = TestUtils.STANDARD_PRICE_BOOK;
	private static Product2 testProduct;
	private static PricebookEntry testPbe;
	private static List<Account> testAccounts;
    private static List<Opportunity> testOpportunities;
    private static List<zqu__Quote__c> testQuotes;
    private static List<zqu__QuoteChargeDetail__c> testChargeDetails;
    private static Zuora__Product__c zProd;
	private static zqu__ProductRatePlan__c ratePlan1;
	private static zqu__ProductRatePlan__c ratePlan2;
	private static zqu__ZProduct__c zProduct;
	private static zqu__ProductRatePlanCharge__c ratePlanCharge1;
	private static zqu__ProductRatePlanCharge__c ratePlanCharge2;

	private static void setupReturnPathProducts(List<String> productNames)
	{
		List<Product2> testProducts = new List<Product2>();

		for(Integer i = 0; i < productNames.size(); i++)
		{
			testProduct = TestUtils.createProduct(false);
			testProduct.Name = productNames[i];
			testProducts.add(testProduct);
		}
		insert testProducts;

		List<PricebookEntry> testPbes = new List<PricebookEntry>();

		for(Product2 testProd : testProducts)
		{
			testPbe = TestUtils.createPricebookEntry(testProd.Id, testPricebook.Id, false);
			testPbes.add(testPbe);
		}
		insert testPbes;
	}

	private static String buildQuery()
    {
        String quoteQuery = '';
        /*quoteQuery += 'SELECT Id, Is_Primary__c, zqu__Opportunity__c, zqu__Currency__c, zqu__ExistSubscriptionID__c,';
        quoteQuery += ' zqu__InitialTerm__c, zqu__RenewalTerm__c, zqu__StartDate__c, zqu__SubscriptionTermStartDate__c, zqu__SubscriptionTermEndDate__c,'; 
        quoteQuery += ' zqu__CancellationDate__c, RecordTypeId FROM zqu__Quote__c WHERE zqu__Opportunity__c != NULL AND Is_Primary__c = true AND zqu__Quote__c.Name = \'Quote Name - Quote for Testing Batch\'';*/

        /*quoteQuery += 'SELECT Id, Is_Primary__c, zqu__Opportunity__c, zqu__Currency__c, zqu__ExistSubscriptionID__c, zqu__Subscription_Name__c, zqu__InvoiceOwnerId__c,';
        quoteQuery += ' zqu__InitialTerm__c, zqu__RenewalTerm__c, zqu__StartDate__c, zqu__SubscriptionTermStartDate__c, zqu__SubscriptionTermEndDate__c,';*/
        quoteQuery += LineItemGenerationService.buildQuery(); 
        quoteQuery += ' AND zqu__Quote__r.Name = \'Quote Name - Quote for Testing Batch\'';
        return quoteQuery;
    }

	private static void buildMultipleQuotesWithCharges(Integer numToCreate, String period)
	{
		List<String> productNames = new List<String>{'Email Optimization: Certification - License - Annually', 'Email Optimization: Email Intelligence Solutions - Gold - Annually'};
		//call method to build rp products (will have to use see all data = true so we can access the standard pricebook, no way around it)
		setupReturnPathProducts(productNames);

	    zProduct = TestUtils.createZProduct('Email Optimization', true);
		zProd = TestUtils.createZuoraProduct('Email Optimization', true);
		ratePlan1 = TestUtils.createZ_ProductRatePlan(zProduct.Id, '000-TEST-000', 'Email Intelligence Solutions', 'Certification - License - Annually', true);
		ratePlan2 = TestUtils.createZ_ProductRatePlan(zProduct.Id, '000-TEST-000', 'Certification', 'Email Intelligence Solutions - Gold - Annually', true);

		ratePlanCharge1 = TestUtils.createZ_ProductRatePlanCharge(ratePlan1.Name, ratePlan1.Id, LineItemGenerationService.RECURRING, 'Per Unit', true);
		ratePlanCharge2 = TestUtils.createZ_ProductRatePlanCharge(ratePlan2.Name, ratePlan2.Id, LineItemGenerationService.RECURRING, 'Per Unit', true);
		
		//Set up account with bill to and sold to contacts	    
	    Account acc = new Account();
	    acc.Name = 'account test';
      	insert acc;

	    Contact billto = new Contact();
	    billto.FirstName = 'Bill To FirstName';
	    billto.LastName = 'Bill To LastName';
	    billto.AccountId = acc.Id;
		insert billto;

	    Contact soldto = new Contact();
	    soldto.FirstName = 'Sold To FirstName';
	    soldto.LastName = 'Sold To LastName';
	    soldto.AccountId = acc.Id;
      	insert soldto;

      	//Insert multiple accounts
	    testAccounts = TestUtils.createAccounts(numToCreate, TestUtils.MARKETER_RECORD_TYPE.Id, true);

	    //Insert 1 opportunity for each account
	    testOpportunities = new List<Opportunity>();
	    for(Integer i = 0; i < testAccounts.size(); i++)
	    {
	    	testOpportunities.add(TestUtils.createOpportunity(TestUtils.RESEARCH_RECORD_TYPE.Id, testAccounts[i].Id, false));
	    }
	    insert testOpportunities;
	    
	    //Insert 1 quote for each opportunity
	    testQuotes = new List<zqu__Quote__c>();
	    for(Integer i = 0; i < testOpportunities.size(); i++)
	    {
		    zqu__Quote__c quote = new zqu__Quote__c();
		    quote.zqu__Currency__c = ExtendedTestDataSetup.DEFAULT_CURRENCY;
		    quote.Name = 'Quote Name - Quote for Testing Batch';
		    quote.zqu__AutoRenew__c = true;
		    quote.zqu__Opportunity__c = testOpportunities[i].Id;
		    quote.zqu__BillToContact__c = billTo.Id;
		    quote.zqu__SoldToContact__c = soldTo.Id;
		    quote.zqu__InitialTerm__c = 12.0;
		    quote.zqu__RenewalTerm__c = 6.0;
		    quote.zqu__PaymentMethod__c = 'Credit Card';
		    quote.zqu__ValidUntil__c = ExtendedTestDataSetup.DEFAULT_EFFECTIVE_START_DATE;
		    quote.zqu__StartDate__c = ExtendedTestDataSetup.DEFAULT_EFFECTIVE_START_DATE;
		    quote.zqu__SubscriptionTermStartDate__c = ExtendedTestDataSetup.DEFAULT_EFFECTIVE_START_DATE;
		    quote.zqu__SubscriptionTermEndDate__c = ExtendedTestDataSetup.DEFAULT_EFFECTIVE_END_DATE;
		    quote.zqu__BillingMethod__c = 'Both';
		    quote.zqu__Subscription_Term_Type__c = 'Termed';
		    testQuotes.add(quote);
	    }
	    insert testQuotes;

	    List<zqu__ProductRatePlanCharge__c> chargeList1 = new List<zqu__ProductRatePlanCharge__c>{ratePlanCharge1};
	    List<zqu__ProductRatePlanCharge__c> chargeList2 = new List<zqu__ProductRatePlanCharge__c>{ratePlanCharge2};

	    /*//Insert 1 quote charge for each quote (Using two different rate plans for variety)
	    testCharges = new List<zqu__QuoteCharge__c>();
	    for(Integer i = 0; i < testQuotes.size(); i++)
	    {
	        zqu__QuoteCharge__c quoteCharge = new zqu__QuoteCharge__c();
	        
	        if(math.mod(i,2) == 0)
	        {
	        	quoteCharge.Name = ratePlanCharge1.Name;
	        	quoteCharge.zqu__ProductRatePlanName__c = ratePlan1.Name;
	        	quoteCharge.zqu__ProductRatePlanCharge__c = ratePlanCharge1.Id;
	        	quoteCharge.zqu__RatePlanCharge_ZuoraID__c = ratePlan1.zqu__ZuoraId__c;
	        }
	        else
	        {
	        	quoteCharge.Name = ratePlanCharge2.Name;
	        	quoteCharge.zqu__ProductRatePlanName__c =ratePlan2.Name;
	        	quoteCharge.zqu__ProductRatePlanCharge__c = ratePlanCharge2.Id;
	        	quoteCharge.zqu__RatePlanCharge_ZuoraID__c =ratePlan2.zqu__ZuoraId__c;
	        }
	        quoteCharge.zqu__ProductName__c = zProduct.Name;
	        quoteCharge.zqu__ChargeType__c = LineItemGenerationService.RECURRING;
	        quoteCharge.zqu__Currency__c = testQuotes[i].zqu__Currency__c;
	        quoteCharge.zqu__EffectivePrice__c = 1000;
	        quoteCharge.zqu__Model__c = 'Some Model';
	        quoteCharge.zqu__Quote__c = testQuotes[i].Id;
	        quoteCharge.zqu__Quantity__c = 1;
	        quoteCharge.zqu__Total__c = quoteCharge.zqu__EffectivePrice__c * quoteCharge.zqu__Quantity__c; 
	        quoteCharge.zqu__Period__c = period;
	        testCharges.add(quoteCharge);
	    }
	    insert testCharges;*/

	    testChargeDetails = new List<zqu__QuoteChargeDetail__c>();
	    for(Integer i = 0; i < testQuotes.size(); i++)
	    {
	    	if(math.mod(i,2) == 0)
	    	{
	    		zqu__QuoteChargeDetail__c detailRecord = TestUtils.createZuoraChargeDetails(testQuotes[i].Id, chargeList1, false)[0];
	    		detailRecord.zqu__MRR__c = 1000.00;
	    		detailRecord.zqu__DeltaMRR__c = 1000.00;
	    		detailRecord.zqu__TCV__c = 12000.00;
	    		detailRecord.zqu__DeltaTCV__c = 12000.00;
	    		testChargeDetails.add(detailRecord);
	    	}
	    	else
	    	{
	    		zqu__QuoteChargeDetail__c detailRecord = TestUtils.createZuoraChargeDetails(testQuotes[i].Id, chargeList1, false)[0];
	    		detailRecord.zqu__MRR__c = 1500.00;
	    		detailRecord.zqu__DeltaMRR__c = 1500.00;
	    		detailRecord.zqu__TCV__c = 18000.00;
	    		detailRecord.zqu__DeltaTCV__c = 18000.00;
	    		testChargeDetails.add(detailRecord);
	    	}
	    }
	    insert testChargeDetails;
    }
	
	private static testMethod void testLineItemBatch() 
	{
		buildMultipleQuotesWithCharges(200, 'MONTH');

		//Kill any other batches that are running in here so we can run ours
		List<AsyncApexJob> myJobs = [select MethodName, Id from AsyncApexJob where Status != 'Completed' AND JobType = 'BatchApex'];
        for(AsyncApexJob myJob : myJobs)
        {
            system.abortJob(myJob.Id);
        }

		test.startTest();
		BatchToCreateOlis oliBatch = new BatchToCreateOlis(buildQuery());
		system.debug('what batch is running? ' + myJobs);
		Database.executeBatch(oliBatch);
		test.stopTest();

		//Query the opp Ids from all the opps we inserted
		Set<Id> oppIds = pluck.Ids(testOpportunities);
		//check to make sure we have an oli for each opportunity
		List<OpportunityLineItem> olisFromBatch = [SELECT Id, OpportunityId FROM OpportunityLineItem WHERE OpportunityId IN : oppIds];
		system.assert(testOpportunities.size() == olisFromBatch.size(), 'We expect the same amount of line items as we have opps');
		Set<Id> oliOppIds = pluck.Ids('OpportunityId', olisFromBatch);
		system.assert(oppIds.size() == oliOppIds.size(), 'If the size of each set of opp Ids does not match then we have an issue olis should be 1:1 with opps');
	}

	private static testMethod void testLineItemBatch_Exception() 
	{
		buildMultipleQuotesWithCharges(200, 'MONTH');

		testQuotes[0].zqu__Opportunity__c = NULL;
		update testQuotes[0];

		//Kill any other batches that are running in here so we can run ours
		List<AsyncApexJob> myJobs = [select MethodName, Id from AsyncApexJob where Status != 'Completed' AND JobType = 'BatchApex'];
        for(AsyncApexJob myJob : myJobs)
        {
            system.abortJob(myJob.Id);
        }

		test.startTest();
		BatchToCreateOlis oliBatch = new BatchToCreateOlis(buildQuery());
		system.debug('what batch is running? ' + myJobs);
		Database.executeBatch(oliBatch);
		test.stopTest();

		//check to make sure we have an exception
		//Query the opp Ids from all the opps we inserted
		Set<Id> oppIds = pluck.Ids(testOpportunities);
		//check to make sure we have an oli for each opportunity
		Integer i = [SELECT COUNT() FROM OpportunityLineItem WHERE OpportunityId IN : oppIds];
		system.assertEquals((oppIds.size() - 1), i, 'We expect the batch to fail for one of the quotes because we are missing an opp id');
	}
	
}