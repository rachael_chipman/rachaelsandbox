@isTest ( SeeAllData=true )
Public Class DeferredRevBatchIntRuleEngine_Test
{
@isTest public static void TestTrigger1()
{

     Account acct = new Account();
        acct.name = 'RPTestacct1';
        acct.BillingCountry = 'USA';
        Insert acct;
        
        Opportunity opp1 = new Opportunity(
        AccountId = acct.id, 
        Name = 'RPTestOpp1', 
        StageName = 'Signature', 
        CloseDate = system.today(), 
        Type = 'New Business',
        Billing_Terms__c = 'Net 30',
        Invoice_Created__c = false,
        Billing_Type__c = 'Annual',
        currencyISOcode = 'USD',
        Who_is_Invoiced__c = 'Partner is Invoiced', 
        Partner_Commission__c = 10        
        );
        
        Insert opp1;


        Deferred_Rev_Fcst_Rpt_Staging__c objToInsert = new Deferred_Rev_Fcst_Rpt_Staging__c();
       
        objToInsert.Journal_Date__c = System.today();
        objToInsert.Reference__c =  'Test';
        objToInsert.Transaction_Currency__c =  'USD';
        objToInsert.Journal_Period__c =  '2013/008';
        objToInsert.SFDC_OP_ID__c = opp1.Id;
        objToInsert.Invoice_Description__c =  'Test';
        objToInsert.Historic_Data_Upload__c =  True;
       
                insert objTOInsert;
      
    }
}