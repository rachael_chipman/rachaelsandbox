@IsTest 
private class NuggetRouting_Test {
private static testmethod void testNuggetRouting(){


Nugget__c n = new Nugget__c(
Company_Name__c = 'test comp',
State__c = 'CA',
Country__c = 'United States',
Status__c = 'Unassigned');

insert n;

User u = [select Id from User where UserRoleId = '00E000000072RTW' limit 1];        
n.OwnerId = u.Id;
update n;

n.Returned_to_RD__c = true;
update n;

Nugget__c n2 = new Nugget__c(
Company_Name__c = 'test comp',
State__c = 'CA',
Country__c = 'United States',
Status__c = 'Unassigned');

insert n2;

n2.Status__c = 'Closed';
update n2;

Nugget__c n3 = new Nugget__c(
Company_Name__c = 'test comp',
State__c = 'CA',
Country__c = 'United States',
Sales_User_ID__c = u.Id,
Status__c = 'Unassigned');

insert n3;

}
}