/*DESCRIPTION: This trigger will look for exisitng Cert Only Opportunities, if no opp is found
and the Promo Type is Walk-Up or Referral, an opp will be created. If the Promo Type is Bundle 
or blank the Cert app will continue to look for the Cert Only Opp each time it's edited*/

trigger FindCreateCertOpp on Certification_Application__c (before insert, before update) {
RecordType RT = [SELECT Id FROM RecordType WHERE DeveloperName = 'Cert_App' and SObjectType = 'Certification_Application__c' LIMIT 1];

    for(Certification_Application__c ca : trigger.new)
    
    if(ca.Opportunity_Created__c != 'Yes' && ca.RecordTypeId == RT.Id){
   //Look for Existing open Cert Only Opps 
   List<Opportunity> existingOpps = [SELECT Id, StageName, Has_Cert_Products_Qty__c, AccountId FROM Opportunity WHERE IsClosed = false and Cert_Only_Opportunity__c = 'Yes' and AccountId = :ca.Account__c LIMIT 1];
   
      //If Walk-Up, create app fee opp       
         if(ca.Application_Fee_Opportunity__c == null && ca.Promo_Type_Picklist__c == 'Walk-Up')
           {
                Opportunity op = new Opportunity (
                Name = 'Naming Convention WFR',
                AccountId = ca.Account__c,
                RecordTypeId = '012000000004x5k',
                StageName = 'Finance Approved',
                CloseDate = system.Today(),
                Type = 'Fee',
                OwnerId = '00500000007286zAAA',
                CampaignId = '701000000004fLn',
                Product__c = 'Certification',
                Certification_Max_Volume__c = ca.Certification_Max_Volume__c,
                Certification_Tier_Sold__c = ca.Certification_Volume__c,
                Pricebook2Id = '01s000000004NS6AAM',
                Product_Certification_App_Fee__c = true,
                Certification_Type__c = 'Application Fee',
                Billing_Type__c = 'Annual',
                Promo_Type_From_Cert_App__c = ca.Promo_Type_Picklist__c,
                Bypass_Financial_Force__c = true);
    
                insert op;
                
               
                
            OpportunityLineItem opl = new OpportunityLineItem(
                OpportunityId = op.Id,
                Quantity = 1,
                UnitPrice = ca.Certification_App_Fee__c,
                Start_Date__c = system.today(),
                End_Date__c = system.today(),
                Product_Status__c = 'Current',
                Description__c = 'Return Path Certification',
                Active__c = true,
                Recurring__c = false,
                PricebookentryId = '01u00000000FeX2AAK');
                
                insert opl;
                    
               ca.Application_Fee_Opportunity__c = op.id;
           }          
      
      if(existingOpps.size() > 0)
      {
           //If found, update the opportunity accordingly    
           for(Opportunity updateOpp : existingOpps)
           {            
               if(updateOpp.StageName != 'Pending Certification Qualification'){
               updateOpp.StageName = 'Obtaining Commitment';
               updateOpp.Approval_Type__c = 'Finance Approval';}
               updateOpp.CampaignID = '701000000004fLnAAI';
               updateOpp.Promo_Type_From_Cert_App__c = ca.Promo_Type_Picklist__c;
               updateOpp.Certification_Analyst__c = ca.Certification_Analyst__c;
               update updateOpp;
                               
               
               ca.License_Fee_Opportunity__c = updateOpp.Id;
               ca.Opportunity_Created__c = 'Yes';
           } 
       } 

       //If Walk-Up, create opp and app fee opp    
       else if(ca.Promo_Type_Picklist__c == 'Walk-Up' && ca.Application_Result__c == 'In Progress' ){
       
       if(ca.Application_Fee_Opportunity__c == null){
       Opportunity op = new Opportunity (
            Name = 'Naming Convention WFR',
            AccountId = ca.Account__c,
            RecordTypeId = '012000000004wVU',
            StageName = 'Finance Approved',
            CloseDate = system.Today(),
            Type = 'Fee',
            OwnerId = '00500000007286zAAA',
            CampaignId = '701000000004fLn',
            Product__c = 'Certification',
            Certification_Max_Volume__c = ca.Certification_Max_Volume__c,
            Certification_Tier_Sold__c = ca.Certification_Volume__c,
            Pricebook2Id = '01s000000004NS6AAM',
            Product_Certification_App_Fee__c = true,
            Certification_Type__c = 'Application Fee',
            Billing_Type__c = 'Annual',
            Promo_Type_From_Cert_App__c = ca.Promo_Type_Picklist__c,
            Bypass_Financial_Force__c = true);
            
            insert op;
            
           
            
        OpportunityLineItem opl = new OpportunityLineItem(
            OpportunityId = op.Id,
            Quantity = 1,
            UnitPrice = ca.Certification_App_Fee__c,
            Start_Date__c = system.today(),
            End_Date__c = system.today(),
            Product_Status__c = 'Current',
            Description__c = 'Return Path Certification',
            Active__c = true,
            Recurring__c = false,
            PricebookentryId = '01u00000000FeX2AAK');
            
            insert opl;
                
           ca.Application_Fee_Opportunity__c = op.id;
           }
       
       
       Opportunity newOpp = new Opportunity(
            AccountId = ca.Account__c,
            RecordTypeID = '012000000004wVUAAY',
            Name = 'Name',
            StageName = 'Obtaining Commitment',
            CloseDate = system.today(),
            Type = 'New Business',
            CurrencyISOCode = ca.CurrencyISOCode,
            CampaignID = '701000000004fLnAAI',
            Pricebook2Id = '01s000000004NS6AAM',
            OwnerId = '00500000007286zAAA',
            Product__c = 'Certification',
            Certification_Max_Volume__c = ca.Certification_Max_Volume__c,
            Certification_Tier_Sold__c = ca.Certification_Volume__c,
            SSC_Qual_Team_Member__c = ca.Certification_Analyst__c,
            Approval_Type__c = 'Finance Approval',
            Product_Certification__c = true,
            Certification_Type__c = ca.Certification_Type__c,
            Promo_Type_From_Cert_App__c = ca.Promo_Type_Picklist__c,
            Certification_Analyst__c = ca.Certification_Analyst__c);

            insert newopp;
    
        OpportunityLineItem newoli = new OpportunityLineItem(
            OpportunityId = newopp.Id,
            UnitPrice = ca.Cert_Tier_to_Unit_Price__c,
            Quantity = 1,
            Product_Status__c = 'Current',
            Description__c = 'Return Path Certification',
            Indirect__c = true,
            Active__c = true,
            Recurring__c = true);
        
       if(newopp.CurrencyISOCode == 'AUD'){newoli.PricebookEntryId = '01u00000000FGwVAAW';}
       if(newopp.CurrencyISOCode == 'BRL'){newoli.PricebookEntryId = '01u00000000FQoqAAG';}
       if(newopp.CurrencyISOCode == 'CAD'){newoli.PricebookEntryId = '01u00000000FRK8AAO';}
       if(newopp.CurrencyISOCode == 'EUR'){newoli.PricebookEntryId = '01u00000000FGwUAAW';}
       if(newopp.CurrencyISOCode == 'GBP'){newoli.PricebookEntryId = '01u00000000FGwTAAW';}
       if(newopp.CurrencyISOCode == 'USD'){newoli.PricebookEntryId = '01u00000000FGwSAAW';}        
        
            insert newoli;
       
               ca.License_Fee_Opportunity__c = newOpp.Id;
               ca.Opportunity_Created__c = 'Yes';   
    
    }
       //If Referral create opp with Channel info (Cert Ref Child Product) 
       else if(ca.Promo_Type_Picklist__c == 'Referral' && ca.Application_Result__c == 'In Progress'){
    
     Opportunity newopp = new Opportunity(
        AccountId = ca.Account__c,
        RecordTypeID = '012000000004wVUAAY',
        Name = 'Name',
        StageName = 'Obtaining Commitment',
        CloseDate = system.today(),
        Type = 'New Business',
        CurrencyISOCode = ca.CurrencyISOCode,
        CampaignID = '701000000004fLnAAI',
        Pricebook2Id = '01s000000004NS6AAM',
        OwnerId = ca.Partner_CRM__c,
        Product__c = 'Certification',
        Certification_Max_Volume__c = ca.Certification_Max_Volume__c,
        Certification_Tier_Sold__c = ca.Certification_Volume__c,
        SSC_Qual_Team_Member__c = ca.Certification_Analyst__c,
        Certification_Type__c = ca.Certification_Type__c,
        Approval_Type__c = 'Finance Approval',
        Product_SSC_Referral_Child__c = true,
        CRM_User__c = ca.Partner_CRM__c,
        PartnerLookup__c = ca.Partner_Account__c,
        Promo_Type_From_Cert_App__c = ca.Promo_Type_Picklist__c,
        Certification_Analyst__c = ca.Certification_Analyst__c);
    
        insert newopp;

    
            OpportunityLineItem newoli = new OpportunityLineItem(
                OpportunityId = newopp.Id,
                UnitPrice = ca.Cert_Tier_to_Unit_Price__c,
                Quantity = 1,
                Product_Status__c = 'Current',
                Description__c = 'Return Path Certification',
                Indirect__c = true,
                Active__c = true,
                Recurring__c = true);
        
       if(newopp.CurrencyISOCode == 'AUD'){newoli.PricebookEntryId = '01u00000000Fb2JAAS';}
       if(newopp.CurrencyISOCode == 'BRL'){newoli.PricebookEntryId = '01u00000000Fb2FAAS';}
       if(newopp.CurrencyISOCode == 'CAD'){newoli.PricebookEntryId = '01u00000000Fb2EAAS';}
       if(newopp.CurrencyISOCode == 'EUR'){newoli.PricebookEntryId = '01u00000000Fb2IAAS';}
       if(newopp.CurrencyISOCode == 'GBP'){newoli.PricebookEntryId = '01u00000000Fb2HAAS';}
       if(newopp.CurrencyISOCode == 'USD'){newoli.PricebookEntryId = '01u00000000Fb2GAAS';}  
        
            insert newoli;
            

     Partner toacct = new Partner(
           OpportunityId = newopp.Id,
            AccountToId = ca.Partner_Account__c,
            Role = 'Channel',
            IsPrimary = true);
            
        insert toacct; 
            
     Partner fromacct = new Partner(
           AccountToId = ca.Partner_Account__c,
           AccountFromId = ca.Account__c,
           Role = 'Channel',
           IsPrimary = true);
                              
        insert fromacct;
        
               ca.License_Fee_Opportunity__c = newOpp.Id;
               ca.Opportunity_Created__c = 'Yes';                  
    }
    

}
}