trigger findSRDlineItem on OpportunityLineItem (before insert) {
    
    List<Opportunity> ou = new List<Opportunity>();
    
    Set<Id> oliIds = new Set<Id>();
    for(OpportunityLineItem oli : Trigger.new)
        oliIds.add(oli.Id);
    
    Map<Id, OpportunityLineItem> olis = new Map<Id, OpportunityLineItem>(
        [select id, description__c, opportunityid from opportunitylineitem where id IN :oliIds]);
    
    for(OpportunityLineItem oli : Trigger.new) {
        if (oli.description__c == 'Response & ROI Consulting: SRD Consulting Related to Metric Change') {
            Opportunity o = new Opportunity(Id = oli.opportunityid);
            o.srd__c = 'Needed';
            ou.add(o);
        }
    }
    update(ou);
}