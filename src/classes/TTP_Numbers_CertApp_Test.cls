@isTest(SeeAllData = true) 

public class TTP_Numbers_CertApp_Test{

static Certification_Application__c testApp;

    static void setupObjects()
    {
    Account a = new Account (Name = 'Test Account');
    insert a;
    
    Account p = new Account (Name = 'Test Partner', CRM__c = '00500000007Em7P');
    insert p;
    
    Promo_Code__c pc = new Promo_Code__c(Name = 'TEST004', Promo_Type__c = 'Referral', Account__c = p.Id);
    insert pc;
    
    p.Promo_Code__c = pc.Id;
    update p;
    
    
    testApp = new Certification_Application__c(
    Account__c = a.Id,
    Application_Status__c = 'Application Submitted',
    Date_Received__c = system.today()-7,
    Promo_Type_Picklist__c = 'Referral',
    Certification_Volume__c = 'Tier 2',
    Certification_Max_Volume__c = 50000,
    Promo_Code__c = 'TEST004',
    Partner_Account__c = p.Id,
    Partner_CRM__c = p.CRM__c,
    Certification_Analyst__c = '00500000007Em7P',
    Bond_Group_Id__c = '1234567');
    }
    
   static testMethod void TTP_Numbers_CertAppRecvdtoUR()
   {
   setupObjects();
   insert testApp;
   
   test.startTest();
   testApp.Application_Status__c = 'Under Review';
   testApp.Application_Result__c = 'Investigating';
   update testApp;
   test.stopTest();
   
   list<Certification_Application__c> apps = [SELECT Id, TTP_Received_to_Under_Review__c FROM Certification_Application__c WHERE Id = :testApp.Id];
   //system.assertEquals(5, apps[0].TTP_Received_to_Under_Review__c, 'Days Received to Under Review should be 5.');
   }
   
    static testMethod void TTP_Numbers_CertAppURtoAC()
   {
   setupObjects();
   testApp.Application_Status__c = 'Under Review';
   testApp.Application_Result__c = 'In Progress';
   testApp.Date_Audit_1_Results_Sent__c = system.today()-7;
   insert testApp;
   
   test.startTest();
   testApp.Application_Status__c = 'Audit Complete';
   testApp.Application_Result__c = 'Fixes Needed';
   update testApp;
   test.stopTest();
   
   list<Certification_Application__c> apps = [SELECT Id, TTP_Received_to_Under_Review__c, TTP_Under_Review_to_Audit_Complete__c FROM Certification_Application__c WHERE Id = :testApp.Id];
   //system.assertEquals(5, apps[0].TTP_Under_Review_to_Audit_Complete__c, 'Days Under Review to Audit Complete should be 5.');
   }
   
   static testMethod void TTP_Numbers_CertAppRectoClose()
   {
   setupObjects();
   insert testApp;
   
   test.startTest();
   testApp.Application_Status__c = 'Closed';
   testApp.Application_Result__c = 'Pass';
   update testApp;
   test.stopTest();
   
   list<Certification_Application__c> apps = [SELECT Id, TTP_Received_to_Under_Review__c, TTP_Under_Review_to_Audit_Complete__c, TTP_Received_to_Closed__c FROM Certification_Application__c WHERE Id = :testApp.Id];
   //system.assertEquals(5, apps[0].TTP_Received_to_Closed__c, 'Days Received to Closed should be 5.');
   }
   
    static testMethod void TTP_Numbers_CertAppFNtoClosed()
   {
   setupObjects();
   testApp.Application_Status__c = 'Audit Complete';
   testApp.Application_Result__c = 'Fixes Needed';
   testApp.Date_App_Reached_Fixes_Needed__c = system.today()-7;
   insert testApp;
   
   test.startTest();
   testApp.Application_Status__c = 'Closed';
   testApp.Application_Result__c = 'Pass';
   update testApp;
   test.stopTest();
   
   list<Certification_Application__c> apps = [SELECT Id, TTP_Received_to_Under_Review__c, TTP_Under_Review_to_Audit_Complete__c, TTP_Fixes_Needed_to_Closed__c FROM Certification_Application__c WHERE Id = :testApp.Id];
   //system.assertEquals(5, apps[0].TTP_Fixes_Needed_to_Closed__c, 'Days Fixes Needed to Closed should be 5.');
   }
   
   static testMethod void TTP_Numbers_CertAppRecvdtoActive()
   {
   setupObjects();
   insert testApp;
   
   test.startTest();
   testApp.Application_Result__c = 'Fixes Needed';
   testApp.Activation_Date__c = system.today();
   update testApp;
   test.stopTest();
   
   list<Certification_Application__c> apps = [SELECT Id, TTP_Received_to_Under_Review__c, TTP_Under_Review_to_Audit_Complete__c, TTP_Fixes_Needed_to_Closed__c, TTP_Received_to_Fixes_Needed__c, TTP_Received_to_Active__c FROM Certification_Application__c WHERE Id = :testApp.Id];
   //system.assertEquals(5, apps[0].TTP_Received_to_Active__c, 'Days Received to Active should be 5.');
   //system.assertEquals(5, apps[0].TTP_Received_to_Fixes_Needed__c, 'Days Received to Fixes Needed should be 5.');
   }
   
   static testMethod void TTP_Numbers_CertAppInvsttoInPrg()
   {
   setupObjects();
   testApp.Application_Status__c = 'Under Review';
   testApp.Application_Result__c = 'Investigating';
   testApp.Date_App_Reached_Investigating__c = system.today()-7;
   insert testApp;
   
   test.startTest();
   testApp.Application_Result__c = 'In Progress';
   update testApp;
   test.stopTest();
   
   list<Certification_Application__c> apps = [SELECT Id, TTP_Received_to_Under_Review__c, TTP_Under_Review_to_Audit_Complete__c, TTP_Investigating_to_In_Progress__c, TTP_Fixes_Needed_to_Closed__c, TTP_Received_to_Fixes_Needed__c, TTP_Received_to_Active__c FROM Certification_Application__c WHERE Id = :testApp.Id];
   //system.assertEquals(5, apps[0].TTP_Investigating_to_In_Progress__c, 'Days Investigatig to In Progress should be 5.');
   }
   
   static testMethod void TTP_Numbers_CertAppURtoClosed()
   {
   setupObjects();
   testApp.Application_Status__c = 'Under Review';
   testApp.Application_Result__c = 'In Progress';
   testApp.Date_Audit_1_Results_Sent__c = system.today()-7;
   insert testApp;
   
   test.startTest();
   testApp.Application_Status__c = 'Closed';
   testApp.Application_Result__c = 'Pass';
   update testApp;
   test.stopTest();
   
   list<Certification_Application__c> apps = [SELECT Id, TTP_Received_to_Under_Review__c, TTP_Under_Review_to_Audit_Complete__c, TTP_Under_Review_to_Closed__c, TTP_Investigating_to_In_Progress__c, TTP_Fixes_Needed_to_Closed__c, TTP_Received_to_Fixes_Needed__c, TTP_Received_to_Active__c FROM Certification_Application__c WHERE Id = :testApp.Id];
   //system.assertEquals(5, apps[0].TTP_Under_Review_to_Closed__c, 'Days Under Review to Closed should be 5.');
   }
}