trigger CampaignTrigger on Campaign (after insert, after update) {

    if(trigger.isAfter && trigger.isInsert)
    {
        CampaignMemberStatusUpdateClass.processTrigger(trigger.new, trigger.oldMap);
    }
    
 	if(trigger.isAfter && trigger.isUpdate)
    {
        CampaignMemberStatusUpdateClass.processTrigger(trigger.new, trigger.oldMap);
    }
}