@isTest(SeeAllData=true)
public class RHX_TEST_Zuora_Subscription {
	static testMethod void RHX_Testmethod() {
        List<sObject> sourceList = [SELECT Id 
			FROM Zuora__Subscription__c LIMIT 1];
        if(sourceList.size() == 0) {
            sourceList.add(
                    new Zuora__Subscription__c()
            );
        }
    	Database.upsert(sourceList);
    }
}