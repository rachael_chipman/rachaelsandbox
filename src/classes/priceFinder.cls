public class priceFinder {

    // Pricing Rates as of July 25th, 2012

    /*public void getPrice(productEntryController.prodChoice p) {
        if(p.prodCode == '030-DMSU-IBM') {
            Decimal vertPremium;
            Decimal volPremium;
            Decimal domPremium;
            Decimal baseRate;

            baseRate = 30000;
            vertPremium = (p.vertical == 'Select...' || p.vertical == null ? 0 : .015);
            volPremium = (p.panelCust == 'Weak' ? -.10 : p.panelCust == 'Medium' ? 0 : p.panelCust == 'Strong' ? .15 : 0) + (p.panelComp == 'Weak' ? -.05 : p.panelComp == 'Medium' ? 0 : p.panelComp == 'Strong' ? .10 : 0);
            domPremium = (p.compdomains >= 20 ? 2000 : p.compDomains >= 10 ? 1000 : 0);

            p.sugPrice = baseRate + ((vertPremium + volPremium) * baseRate) + (domPremium * p.monthsService);
        }

        if(p.prodCode == '003-CERT-APP0') {
            p.sugPrice = (
                p.vol > 100000000 ? 5000 :
                p.vol > 50000000 ? 3500 :
                p.vol > 20000000 ? 2500 :
                p.vol > 10000000 ? 1250 :
                p.vol > 5000000 ? 1000 :
                p.vol > 1000000 ? 1000 :
                p.vol > 250000 ? 650 :
                p.vol > 50000 ? 500 :
                p.vol > 0 ? 200 : 0);
        }

        if(p.prodCode == '004-CERT-FEE0' || p.prodName == '005-CERT-REFC') {
            p.sugPrice = (
                p.vol > 800000000 ? 375000 :
                p.vol > 700000000 ? 355000 :
                p.vol > 600000000 ? 330000 :
                p.vol > 500000000 ? 300000 :
                p.vol > 400000000 ? 265000 :
                p.vol > 300000000 ? 225000 :
                p.vol > 200000000 ? 180000 :
                p.vol > 150000000 ? 132500 :
                p.vol > 100000000 ? 107500 :
                p.vol > 50000000 ? 82500 :
                p.vol > 20000000 ? 55000 :
                p.vol > 10000000 ? 27500 :
                p.vol > 5000000 ? 16500 :
                p.vol > 1000000 ? 9350 :
                p.vol > 250000 ? 2750 :
                p.vol > 50000 ? 1375 :
                p.vol > 1 ? 440 : 0);

            p.tierSold = (
                p.vol > 800000000 ? 'Tier 17' :
                p.vol > 700000000 ? 'Tier 16' :
                p.vol > 600000000 ? 'Tier 15' :
                p.vol > 500000000 ? 'Tier 14' :
                p.vol > 400000000 ? 'Tier 13' :
                p.vol > 300000000 ? 'Tier 12' :
                p.vol > 200000000 ? 'Tier 11' :
                p.vol > 150000000 ? 'Tier 10' :
                p.vol > 100000000 ? 'Tier 9' :
                p.vol > 50000000 ? 'Tier 8' :
                p.vol > 20000000 ? 'Tier 7' :
                p.vol > 10000000 ? 'Tier 6' :
                p.vol > 5000000 ? 'Tier 5' :
                p.vol > 1000000 ? 'Tier 4' :
                p.vol > 250000 ? 'Tier 3' :
                p.vol > 50000 ? 'Tier 2' :
                p.vol > 1 ? 'Tier 1' : 'Unknown');
        }

        if(p.prodCode == '020-DMSU-PIXE') {
            Decimal mpv = p.vol * 0.3;

            p.sugPrice = (
                mpv > 100000000 ? 150000 :
                mpv > 75000000 ? 120000 :
                mpv > 50000000 ? 90000 :
                mpv > 25000000 ? 60000 :
                mpv > 10000000 ? 30000 :
                mpv > 5000000 ? 15000 :
                mpv > 2000000 ? 6000 :
                mpv > 1 ? 0 : 0);
        }

        if(p.prodCode == '001-DMSU-GOLD' || p.prodCode == '015-ESPR-REFC' || p.prodCode == '012-ESPR-INTU') {
            p.sugPrice = 0;

            Decimal eventsPrice = 0;
            Decimal ipsfee = 0;
            Decimal domainsfee = 0;

            eventsPrice = (
                p.events > 5000 ? 125000 :
                p.events > 3000 ? 100000 :
                p.events > 2000 ? 75000 :
                p.events > 1000 ? 55000 :
                p.events > 500 ? 39000 :
                p.events > 250 ? 33500 :
                p.events > 100 ? 28000 :
                p.events > 30 ? 22500 :
                p.events > 1 ? 19000 : 0);

            ipsfee = (
                p.ips > 5 ? ((p.ips - 5) * 250) : 0);

            domainsfee = (
                p.doms > 1 ? ((p.doms - 1) * 3500) : 0);

            p.sugPrice = eventsPrice + ipsfee + domainsfee;
        }

        if(p.prodCode == '002-DMSU-PLAT') {
            Decimal eventsPrice = 0;

            Integer platFee = (
                p.doms == 1 ? 51000 :
                p.doms == 2 ? 61200 :
                p.doms == 3 ? 66300 :
                p.doms == 4 ? 71400 :
                p.doms == 5 ? 76500 :
                p.doms == 6 ? 81600 :
                p.doms == 7 ? 86700 :
                p.doms == 8 ? 91800 :
                p.doms == 9 ? 96900 :
                p.doms == 10 ? 102000 :
                p.doms == 11 ? 107100 :
                p.doms == 12 ? 112200 :
                p.doms == 13 ? 117300 :
                p.doms == 14 ? 122400 :
                p.doms == 15 ? 127500 :
                p.doms == 16 ? 132600 :
                p.doms == 17 ? 137700 :
                p.doms == 18 ? 142800 :
                p.doms == 19 ? 147900 :
                p.doms >= 20 ? 153000 : 0);

            eventsPrice = (
                p.events > 5000 ? 125000 :
                p.events > 3000 ? 100000 :
                p.events > 2000 ? 75000 :
                p.events > 1000 ? 55000 :
                p.events > 500 ? 39000 :
                p.events > 250 ? 33500 :
                p.events > 100 ? 28000 :
                p.events > 30 ? 22500 :
                p.events > 1 ? 19000 : 0);

            p.sugPrice = eventsPrice + platFee;

            if(p.platinumContacts == 1) p.sugPrice = p.sugPrice;
            if(p.platinumContacts == 2) p.sugPrice = p.sugPrice * 1.025;
            if(p.platinumContacts == 3) p.sugPrice = p.sugPrice * 1.05;
            if(p.platinumContacts == 4) p.sugPrice = p.sugPrice * 1.075;
            if(p.platinumContacts == 5) p.sugPrice = p.sugPrice * 1.1;
            if(p.platinumContacts == 6) p.sugPrice = p.sugPrice * 1.125;
            if(p.platinumContacts == 7) p.sugPrice = p.sugPrice * 1.15;
            if(p.platinumContacts == 8) p.sugPrice = p.sugPrice * 1.2;
            if(p.platinumContacts == 9) p.sugPrice = p.sugPrice * 1.225;
            if(p.platinumContacts == 10) p.sugPrice = p.sugPrice * 1.25;
        }

        /*Removing the 'suggested price' functionality for APS products based on CK trello card 3/6
        if(p.prodCode == '027-DOMN-AUDT') p.sugPrice = 5000;

        if(p.prodCode == '022-EMBR-MNTR' || p.prodCode == '023-EMBR-MTCH') {
            p.sugPrice = 10000;
        }

        if(p.prodCode == '024-DOMN-PROT' || p.prodCode == '027-DOMN-PRCH') {
            Decimal volPrice;
            Decimal domPrice;
            Decimal baseFee;
            Decimal svcFee;

            volPrice = (
                p.vol > 50000000 ? 120000 :
                p.vol > 20000000 ? 80000 :
                p.vol > 10000000 ? 75000 :
                p.vol > 5000000 ? 38000 : 20000
            );

            domPrice = (
                p.doms > 500 ? 60000 :
                p.doms > 100 ? 45000 :
                p.doms > 50 ? 30000 :
                p.doms > 10 ? 15000 : 5000
            );

            baseFee = volPrice + domPrice;
            svcFee = 0.8 * baseFee;

            if(p.daservice == 'Self' || p.daservice == null) {
                p.sugPrice = baseFee;
            } else {
                p.sugPrice = baseFee + (svcFee > 80000 ? svcFee : 80000);
            }
        }

        if(p.prodCode == '026-DOMN-SECR' || p.prodCode == '028-DOMN-SECH') {
            Decimal volPrice;
            Decimal domPrice;
            Decimal baseFee;
            Decimal svcFee;

            volPrice = (
                p.vol > 50000000 ? 180000 :
                p.vol > 20000000 ? 120000 :
                p.vol > 10000000 ? 112500 :
                p.vol > 5000000 ? 57000 : 30000
            );

            domPrice = (
                p.doms > 500 ? 120000 :
                p.doms > 100 ? 90000 :
                p.doms > 50 ? 60000 :
                p.doms > 10 ? 30000 : 10000
            );

            baseFee = volPrice + domPrice;
            svcFee = 0.8 * baseFee;

            if(p.daservice == 'Self' || p.daservice == null) {
                p.sugPrice = baseFee;
            } else {
                p.sugPrice = baseFee + (svcFee > 120000 ? svcFee : 120000);
            }
        }*/

        /*if(p.prodCode == '008-PROF-GENC') {
            if (p.descrip == 'Response & ROI Consulting: Competitive Benchmark Analysis') p.sugPrice = 10000;
            if (p.descrip == 'Response & ROI Consulting: 360 Email Program Evaluation') p.sugPrice = 50000;
            if (p.descrip == 'Deliverability & Technology Consulting: Authentication Review and Implementation') p.sugPrice = 2500;
            if (p.descrip == 'Deliverability & Technology Consulting: B2B Deliverability Report Card') p.sugPrice = 10000;
            if (p.descrip == 'Response & ROI Consulting: Best Practices Training Seminar') p.sugPrice = 12000;
            if (p.descrip == 'Deliverability & Technology Consulting: Bounce Processing Review and Optimization') p.sugPrice = 3000;
            if (p.descrip == 'Deliverability & Technology Consulting: Complaint Processing Review') p.sugPrice = 5000;
            if (p.descrip == 'Response & ROI Consulting: Complaint Reduction Report Card') p.sugPrice = 7500;
            if (p.descrip == 'Deliverability & Technology Consulting: Complaint Source Analysis') p.sugPrice = 10000;
            if (p.descrip == 'Deliverability & Technology Consulting: Connection and Throughput') p.sugPrice = 650;
            if (p.descrip == 'Response & ROI Consulting: Creative Assessment') p.sugPrice = 2500;
            if (p.descrip == 'Response & ROI Consulting: Creative Optimization & Testing Plan') p.sugPrice = 12500;
            if (p.descrip == 'Deliverability & Technology Consulting: Delivery Log Analysis') p.sugPrice = 650;
            if (p.descrip == 'Deliverability & Technology Consulting: Deliverability Report Card') p.sugPrice = 10000;
            //if (p.descrip == 'Deliverability & Technology Consulting: Email Deliverability 101') p.sugPrice = 8000;
            if (p.descrip == 'Response & ROI Consulting: Email Launch Program') p.sugPrice = 30000;
            if (p.descrip == 'Deliverability & Technology Consulting: Email Migration Program') p.sugPrice = 18000;
            if (p.descrip == 'Deliverability & Technology Consulting: Email Program for New Mailers') p.sugPrice = 40000;
            if (p.descrip == 'Response & ROI Consulting: Email Program Report Card') p.sugPrice = 7500;
            if (p.descrip == 'Deliverability & Technology Consulting: ESP Kick-Start Program') p.sugPrice = 14000;
            if (p.descrip == 'Feedback Loop Extractor ') p.sugPrice = 0;
            if (p.descrip == 'Deliverability & Technology Consulting: Import Address Book Report Card') p.sugPrice = 5000;
            if (p.descrip == 'Deliverability & Technology Consulting: Infrastructure and Operations Optimization') p.sugPrice = 15000;
            if (p.descrip == 'Deliverability & Technology Consulting: Infrastructure Review') p.sugPrice = 0;
            if (p.descrip == 'Response & ROI Consulting: Intelligent List Growth With Organic Capture') p.sugPrice = 15000;
            if (p.descrip == 'Deliverability & Technology Consulting ISP Connect') p.sugPrice = 2500;
            if (p.descrip == 'Deliverability & Technology Consulting: List Quality Analysis') p.sugPrice = 10000;
            if (p.descrip == 'Response & ROI Consulting: Preference Center Optimization') p.sugPrice = 10000;
            if (p.descrip == 'Report Card TBD') p.sugPrice = 7500;
            if (p.descrip == 'Deliverability & Technology Consulting: Spam Trap Removal Program') p.sugPrice = 10000;
            if (p.descrip == 'Response & ROI Consulting: Negative SRD Feedback Reduction Report Card') p.sugPrice = 7500;
            if (p.descrip == 'Whitelist & Feedback Loop Setup Fee') p.sugPrice = 0;
            if (p.descrip == 'Response & ROI Consulting: Reengagement Strategy') p.sugPrice = 15000;
            if (p.descrip == 'Deliverability & Technology Consulting: Consulting Retainer') p.sugPrice = 0;
            if (p.descrip == 'Response & ROI Consulting: Consulting Retainer') p.sugPrice = 10500;
            if (p.descrip == 'Deliverability & Technology Consulting: Custom') p.sugPrice = 0;
            if (p.descrip == 'Response & ROI Consulting: Custom') p.sugPrice = 0;
            if (p.descrip == 'ROI & Response Consulting: Baseline Assessment') p.sugPrice = 0;
            if (p.descrip == 'Response & ROI Consulting: Pre-Certification Report Card') p.sugPrice = 10000;
            if (p.descrip == 'Competitor Analysis') {
                if(p.competitors == null) p.competitors = 1;
                if(p.monthsService == null) p.monthsService = 3;
                Decimal base;
                Decimal compPremium;
                Decimal dataPremium;
                Decimal freqDiscount;
                Integer reportFreq;
                Decimal subTotal;

                base = 7500;
                compPremium = (p.competitors - 1) * 1500;
                dataPremium = (p.monthsService > 3 ? (p.monthsService - 3) * 1500 : 0);
                reportFreq = (p.reportFreq == 'Quarterly' ? 4 : p.reportFreq == 'Semi Annually' ? 2 : 1);

                subTotal = (base + compPremium + dataPremium) * reportFreq;
                freqDiscount = (subTotal * (p.reportFreq == 'Quarterly' ? .2 : p.reportFreq == 'Semi Annually' ? .1 : 0));
                p.sugPrice = subTotal - freqDiscount;
            }

        }

        if(p.prodCode == '028-DMSU-SERV') {
            if(p.descrip == 'Additional Account Management Services: Deliverablity Report Card') p.sugPrice = 5000;
            if(p.descrip == 'Additional Account Management Services: Email Program Report Card') p.sugPrice = 5000;
            if(p.descrip == 'Additional Account Management Services: List Quality Report Card') p.sugPrice = 5000;
            if(p.descrip == 'Additional Account Management Services: ISP Connect') p.sugPrice = 2500;
            if(p.descrip == 'Additional Account Management Services: Connection and Throughput') p.sugPrice = 500;
        }


    }*/

}