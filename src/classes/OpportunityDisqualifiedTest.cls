@isTest

public class OpportunityDisqualifiedTest {
    
    static testMethod void OpportunityDisqualifiedTest()
    {
        //setup test records
        List<opportunity> opplist = CreateTestRecords(1);
        if(opplist!= null)
        {
            Opportunity TOpp = opplist.get(0);
            PageReference pageref = Page.OpportunityDisqualified;
            pageref.getParameters().put('id',TOpp.Id);
            Test.setCurrentPage(pageref);
            ApexPages.StandardController sc = new ApexPages.StandardController(TOpp);
            Opportunity_Disqualified_Lost testcontroller = new  Opportunity_Disqualified_Lost(sc);
            test.startTest();
            testcontroller.Opp.Lost_Pass_Specifics__c = 'Test Comments';
            //testcontroller.Opp.Disqualified_Sub_Status__c = 'Nuture';
            testcontroller.Submit();
            test.stopTest();
            assertResults(opplist);   
        }
    }
    
    static testMethod void OpportunityLostTest()
    {
        //setup test records
        List<opportunity> opplist = CreateTestRecords(1);
        if(opplist!= null)
        {
            Opportunity TOpp = opplist.get(0);
            PageReference pageref = Page.OpportunityLost;
            pageref.getParameters().put('id',TOpp.Id);
            Test.setCurrentPage(pageref);
            ApexPages.StandardController sc = new ApexPages.StandardController(TOpp);
            Opportunity_Disqualified_Lost testcontroller = new  Opportunity_Disqualified_Lost(sc);
            test.startTest();
            testcontroller.Opp.Disqualified_Sub_Status__c = 'Nurture';
            testcontroller.Opp.Lost_Pass_Specifics__c = 'Test Comments';
            testcontroller.Opp.Type = 'New Business';
            testcontroller.Opp.Reasons_for_Lost_Opportunities__c = 'Closing Elsewhere';
            testcontroller.Opp.Lost_Pass_Details__c = 'Merged into another Opp';
            testcontroller.OppLost();
            test.stopTest();
            System.assertEquals(testcontroller.Opp.StageName, 'Lost');
        }
    }
  
    public static void assertResults(List<opportunity> opplist)
    {
        List<Id> conIds = new List<Id>();
        List<Opportunity> opps = [Select Id,StageName,Disqualified_Sub_Status__c,Lost_Pass_Specifics__c from Opportunity where Id in :opplist ];
        //assert the Opportunity Field
        for(Opportunity opp: opps)    
        {
            system.assert(opp.StageName == 'Disqualified');
            //system.assert(opp.Disqualified_Sub_Status__c == 'Nurture' );
            system.assert(opp.Lost_Pass_Specifics__c == 'Test Comments');
        }
        
        //assert the contact Field
        List<OpportunityContactRole> ContactRoles = [select Contactid, Id from OpportunityContactRole where OpportunityId in :opplist];
        if(ContactRoles!= null)
        {
            for(OpportunityContactRole Tcon: ContactRoles)
            {
                conIds.add(Tcon.ContactId);
            }
        }
        
        List<Contact> contacts = [select Id, Qualifying_Status__c from contact where Id in :conIds];
        if(contacts!= null)
        {
            for(Contact con: contacts)
            {
                system.assertEquals(con.Qualifying_Status__c, 'RSE DQ', 'values match');
            }
        }
    }
    
    public static list<Opportunity> CreateTestRecords(integer records)
    {
        List<Opportunity> opplist = new List<Opportunity>();
        List<Contact> conlist = new List<Contact>();
        List<OpportunityContactRole> conrolelist = new List<OpportunityContactRole>();
        
        //Create a new Opportunity Object of rec type pre-pipeline
        List<RecordType> rectype = [Select Id, name from RecordType where Name='Pre-Pipeline'];
        for(integer i=0;i<records;i++)
        {
            Opportunity newOpp = new Opportunity();
            newOpp.Name = 'Test '+ i;
            newOpp.CloseDate = Date.today();
            newOpp.StageName = 'Pre-Pipeline';
            //newOpp.Opportunity_Accepted__c = System.;
            //newOpp.ForecastCategory = 'Pipeline';
            newOpp.RecordTypeId = rectype[0].Id;
            opplist.add(newOpp);
        }
        
        insert opplist;
        //Create the contact object
        for(Integer i=0;i<10;i++)
        {
            Contact con = new Contact();
            con.LastName = 'TestCon' +i;
            con.Contact_Status__c = 'Current';
            con.ContactsRole__c = 'Return Path User';
            conlist.add(con);
        }
        
        insert conlist;
        //Create the Contact Role object
        for(Opportunity topp: opplist)
        {
            for(Integer i=0;i<10;i++)
            {
                OpportunityContactRole conrole = new OpportunityContactRole();
                conrole.ContactId = conlist[i].Id;
                conrole.OpportunityId = topp.Id;
                conrolelist.add(conrole);
            }
        }
        
        insert conrolelist;
        return opplist;
    }

}