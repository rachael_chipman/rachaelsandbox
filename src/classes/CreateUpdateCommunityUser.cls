public with sharing class CreateUpdateCommunityUser {

	public static Id profileId
	{
		get
		{
			if(profileId == NULL)
			{
				profileId = [SELECT Id, Name FROM Profile WHERE Name = 'RP Community User (Logins)'].Id;
			}
			return profileId;	
		}
		private set;
	}

	public static User oracleUser
	{
		get
		{
			if(oracleUser == NULL)
			{
				oracleUser = [SELECT Id, LocaleSidKey, LanguageLocaleKey, TimeZoneSidKey, EmailEncodingKey FROM User WHERE Name = 'Internal Tools Automation' LIMIT 1];
			}
			return oracleUser;
		}
		private set;
	}

	public static List<Contact> filterContacts (List<Contact> newContacts, Map<Id, Contact> oldContacts)
	{
		Select.Filter filterA = Select.Field.hasChanged(Contact.Internal_Tools_Id__c);
		Select.Filter filterB = Select.Field.hasChanged(Contact.EIS_Email_Address__c);
		Select.Filter filterC = Select.Field.hasChanged(Contact.Internal_Tools_Status__c);
		Select.Filter filterD = Select.Field.hasChanged(Contact.Internal_Tools_User_Status__c);

		Select.Filter myFilter = filterA.orx(filterB).orx(filterC).orx(filterD);

		return myFilter.filter(newContacts, oldContacts);
	}

	public static Map<String,User> getExistingUser(List<Contact> filteredContacts)
	{
		Map<String, User> eisIdToUserRecord = new Map<String, User>();
		//Pluck the EIS Ids from the Contacts in our filter
		Set<String> eisIds = pluck.Strings('Internal_Tools_Id__c', filteredContacts);
		//Query for users with the same Federation Id as the Contact record's EIS Id
		List<User> existingUsers = [SELECT Id, Email, IsActive, ContactId, FederationIdentifier, UserName FROM User WHERE FederationIdentifier IN : eisIds];
		//Create a map of Federation Ids to Users
		Map<String, List<User>> federationIdToUser = groupBy.strings('FederationIdentifier', existingUsers);
		//Loop through the filtered Contacts and queried Users, checking each one to see if there is a user with a matching Federation Id and if there is add it to the map
		for(Contact con : filteredContacts)
		{
			if(federationIdToUser.containsKey(con.Internal_Tools_Id__c))
			{
				eisIdToUserRecord.put(con.Internal_Tools_Id__c, federationIdToUser.get(con.Internal_Tools_Id__c)[0]);
			}
		}
		return eisIdToUserRecord;
	}

	private static Map<String,User> getExistingUserName(List<Contact> filteredContacts)
	{
		Map<String, User> eisEmailToUser = new Map<String, User>();
		//Pluck the EIS Emails from our list of Contacts
		Set<String> eisEmails = pluck.strings('EIS_Email_Address__c', filteredContacts);
		//Query for users with the same UserName as the Contact record's EIS Email Address
		List<User> existingUsers = [SELECT Id, Email, IsActive, ContactId, FederationIdentifier, UserName FROM User WHERE UserName IN : eisEmails];
		for(User existing : existingUsers)
		{
			eisEmailToUser.put(existing.UserName, existing);
		}
		return eisEmailToUser;
	}

	public static User createCommunityUser(Contact con)
	{
		String aliasFirstPart = con.FirstName.subString(0,1);
		String aliasLastPart;
		if(con.LastName.length() > 6)
		{
			aliasLastPart = con.LastName.subString(0,5);
		}
		else
		{
			aliasLastPart = con.LastName;
		}
		User communityUser = new User();
		communityUser.FederationIdentifier = con.Internal_Tools_Id__c;
		communityUser.FirstName = con.FirstName;
		communityUser.LastName = con.LastName;
		communityUser.UserName = con.EIS_Email_Address__c + '.' + con.Internal_Tools_Id__c;
		communityUser.Email = con.EIS_Email_Address__c;
		communityUser.ProfileId = profileId;
		communityUser.ContactId = con.Id;
		communityUser.IsActive = true;
		communityUser.Alias = aliasFirstPart + aliasLastPart;
		communityUser.TimeZoneSidKey = oracleUser.TimeZoneSidKey;
		communityUser.LocaleSidKey =  oracleUser.LocaleSidKey;
		communityUser.EmailEncodingKey = oracleUser.EmailEncodingKey;
		communityUser.LanguageLocaleKey = oracleUser.LanguageLocaleKey;
		communityUser.DefaultGroupNotificationFrequency = 'N';
		communityUser.DigestFrequency = 'N';
		communityUser.UserPermissionsChatterAnswersUser =  true;


		return communityUser;
	}

	public static void processMethod(List<Contact> filteredContacts)
	{
		Map<String, User> eisIdToUser = getExistingUser(filteredContacts);
		Map<String, User> eisEmailToUser = getExistingUserName(filteredContacts);
		List<User> usersToInsert = new List<User>();
		//List<User> usersToUpdate = new List<User>();
		Set<Id> userIdsForUpdate = new Set<Id>();

		for(Contact con : filteredContacts)
		{
			if(!eisIdToUser.isEmpty() && eisIdToUser.containsKey(con.Internal_Tools_Id__c))
			{
				//Add User Id to set to pass into future method to avoid mixed DML error due to UserName update
				userIdsForUpdate.add(eisIdToUser.get(con.Internal_Tools_Id__c).Id); 
			}
			else if(con.EIS_Email_Address__c != NULL && con.Internal_Tools_Id__c != NULL && (con.Internal_Tools_Status__c && con.Internal_Tools_User_Status__c))
			{
				usersToInsert.add(createCommunityUser(con));
			}
		}
		try
		{
			Database.DMLOptions dmlo = new Database.DMLOptions();
			dmlo.EmailHeader.triggerUserEmail = false;

			//insert usersToInsert;
		}
		catch (System.DmlException e)
		{
			Map<Id, Contact> contacts = new Map<Id, Contact>(filteredContacts);
			for(Integer i = 0; i < e.getNumDml(); i++)
			{
				Id errorId = usersToInsert[ e.getDmlIndex(i) ].ContactId;
                contacts.get(errorId).addError( e.getDmlMessage(i) );
                system.debug('What is the error message' + e.getDmlMessage(i));
			}
		}
		system.debug('Whats in my set of userIds ' + userIdsForUpdate);
		if(!userIdsForUpdate.isEmpty())
		{
			updateCommunityUserFuture(userIdsForUpdate);	
		}
		/*try
		{
			Database.DMLOptions dmlo = new Database.DMLOptions();
			dmlo.EmailHeader.triggerUserEmail = false;

			update usersToUpdate;
		}
		catch (System.DmlException e)
		{
			Map<Id, Contact> contacts = new Map<Id, Contact>(filteredContacts);
			for(Integer i = 0; i < e.getNumDml(); i++)
			{
				Id errorId = usersToUpdate[ e.getDmlIndex(i) ].ContactId;
                contacts.get(errorId).addError( e.getDmlMessage(i) );
                system.debug('What is the error message' + e.getDmlMessage(i));
			}
		}*/
	}

	@future
	public static void updateCommunityUserFuture(Set<Id> userIds)
	{
		updateCommunityUserSync(userIds);
	}

	public static void updateCommunityUserSync(Set<Id> userIds)
	{
		List<User> communityUsers = [SELECT Id, ContactId, UserName, Email FROM User WHERE Id IN: userIds];
		Set<String> contactIds = pluck.strings('ContactId', communityUsers); 
		List<Contact> updatedContacts = [SELECT Id, Name, Internal_Tools_Id__c, EIS_Email_Address__c, Internal_Tools_Status__c, Internal_Tools_User_Status__c FROM Contact WHERE Id IN: contactIds];
		Map<String, User> eisIdToUser = CreateUpdateCommunityUser.getExistingUser(updatedContacts);
		system.debug('What is in the id to user map (future method)' + eisIdToUser);
		List<User> usersToUpdate = new List<User>();

		for(Contact con : updatedContacts)
		{
			if(eisIdToUser.containsKey(con.Internal_Tools_Id__c))
			{
				eisIdToUser.get(con.Internal_Tools_Id__c).Email = con.EIS_Email_Address__c;
				eisIdToUser.get(con.Internal_Tools_Id__c).UserName = con.EIS_Email_Address__c + '.' + con.Internal_Tools_Id__c;
				if(!con.Internal_Tools_Status__c || !con.Internal_Tools_User_Status__c)
				{
					eisIdToUser.get(con.Internal_Tools_Id__c).IsActive = false;
				}
				if(!eisIdToUser.get(con.Internal_Tools_Id__c).IsActive && (con.Internal_Tools_Status__c && con.Internal_Tools_User_Status__c))
				{
					eisIdToUser.get(con.Internal_Tools_Id__c).IsActive = true;
				}
				usersToUpdate.add(eisIdToUser.get(con.Internal_Tools_Id__c));
			}
		}		
		try
		{
			Database.DMLOptions dmlo = new Database.DMLOptions();
			dmlo.EmailHeader.triggerUserEmail = false;

			//update usersToUpdate;
		}
		catch (System.DmlException e)
		{
			List<Messaging.SingleEmailMessage> errorsToSend = new List<Messaging.SingleEmailMessage>();
			Map<Id, Contact> contacts = new Map<Id, Contact>(updatedContacts);
			for(Integer i = 0; i < e.getNumDml(); i++)
			{
				Id errorId = communityUsers[ e.getDmlIndex(i) ].ContactId;
                //contacts.get(errorId).addError( e.getDmlMessage(i) );
                system.debug('What is the error message' + e.getDmlMessage(i));

                Messaging.SingleEmailMessage errorAlert = new Messaging.SingleEmailMessage();
                errorAlert.setToAddresses(new String[] { 'salesforceplatform@returnpath.com' });
                errorAlert.setSenderDisplayName('Community User Update Failures');
                errorAlert.setSubject('Community User Update Failed for Contact:' + contacts.get(errorId).Name);
                String body = 'ATTENTION REQUIRED!\n\n'; 
                body += 'The Contact in the subject of this email encountered an error.\n';
                body += 'Error message: ' + e.getDmlMessage(i) + '\n\n';
                body += 'Please access the contact record to investigate the issue.\n';
                body += 'Contact link: /apex/Contact_Tab_View?id=' + errorId + '\n';
                body += 'User record link: /' + communityUsers[e.getDmlIndex(i)].Id + '\n\n';
                body += 'This email is generated due to an error caused by an update to the EIS Email Address field.\n';
                body += 'Apex Class: CreateUpdateCommunityUser';
                errorAlert.setHtmlBody(body);

                errorsToSend.add(errorAlert);
			}
			Messaging.sendEmail(errorsToSend);
		}
	}


	public static void createUpdateCommunityUsers(List<Contact> newContacts, Map<Id, Contact> oldContacts)
	{
		List<Contact> filterMethodContacts = filterContacts(newContacts, oldContacts);
		if(!filterMethodContacts.isEmpty())
		{
			processMethod(filterMethodContacts);
		}
	}
}