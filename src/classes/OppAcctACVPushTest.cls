@isTest
public class OppAcctACVPushTest {
    /*static testMethod void runOppAcctACVPushTest() {

         //Run as Sales Ops
        User SalesOps = new User(id = '00500000006olmF');
        System.RunAs(SalesOps){
        
        Account acct = new Account();
        acct.name = 'RPTestacct1';
        Insert acct;
        
        Opportunity opp1 = new Opportunity();
        opp1.AccountId = acct.id; 
        opp1.Name = 'RPTestOpp1'; 
        opp1.StageName = 'Negotiating'; 
        opp1.CloseDate = Date.valueOf('2009-01-01'); 
        opp1.OwnerId = '00500000006olmF'; 
        opp1.RecordTypeId = '012000000004u5b';
        opp1.Approval_Type__c = 'Pricing Approval';
        opp1.Pricebook2Id = '01s000000004NS6AAM';
        opp1.Type = 'New Business';
        opp1.product__c = 'Gold;Platinum;Certification;Domain Assurance';
        opp1.Domain_Assurance_Volume_Tier__c = 'Tier 5';
        opp1.TermofContractinmonths__c = '12';
        opp1.Effective_Date__c = Date.valueOf('2009-01-01');
        Insert opp1;
        
        //Gold
        OpportunityLineItem prod1 = new OpportunityLineItem();
        prod1.OpportunityId = opp1.id;
        prod1.PricebookEntryId = '01u00000000FGw7AAG';
        prod1.Quantity = 12;
        prod1.UnitPrice = 1200;
        prod1.of_MM_Events_Allowed__c = 20;
        prod1.of_CP_Events_Allowed__c = 20;
        prod1.MM_Overage_Fee__c = 25;
        prod1.CP_Overage_Fee__c = 25;
        prod1.of_BLA_IPs__c = 10;
        prod1.of_Rep_Mon_IPs__c = 10;
        prod1.Product_Status__c = 'Current';
        prod1.Description__c = 'Deliverability Tools';
        prod1.Indirect__c = false;
        prod1.Active__c = true;
        prod1.Previous_Value__c = 0;
        prod1.Bundle__c = false;
        prod1.Recurring__c = true;
        prod1.Start_Date__c = date.newinstance(1900,1,1);
        prod1.End_Date__c = date.newinstance(1900,1,1);
        Insert prod1;
        
        //Certification
        OpportunityLineItem prod2 = new OpportunityLineItem();
        prod2.OpportunityId = opp1.id;
        prod2.PricebookEntryId = '01u00000000FGwSAAW';
        prod2.Quantity = 1;
        prod2.UnitPrice = 25000;
        prod2.Product_Status__c = 'Current';
        prod2.Description__c = 'Return Path Certification';
        prod2.Indirect__c = false;
        prod2.Active__c = true;
        prod2.previous_value__c = 0;
        prod2.Bundle__c = false;
        prod2.Recurring__c = true;
        Insert prod2;
        
        //Platinum
        OpportunityLineItem prod3 = new OpportunityLineItem();
        prod3.OpportunityId = opp1.id;
        prod3.PricebookEntryId = '01u00000000FGwIAAW';
        prod3.Quantity = 12;
        prod3.UnitPrice = 3500;
        prod3.of_MM_Events_Allowed__c = 20;
        prod3.of_CP_Events_Allowed__c = 20;
        prod3.MM_Overage_Fee__c = 25;
        prod3.CP_Overage_Fee__c = 25;
        prod3.of_BLA_IPs__c = 10;
        prod3.of_Rep_Mon_IPs__c = 10;
        prod3.Product_Status__c = 'Current';
        prod3.Description__c = 'Deliverability Tools';
        prod3.Indirect__c = false;
        prod3.Active__c = true;
        prod3.previous_value__c = 0;
        prod3.Bundle__c = false;
        prod3.Recurring__c = true;
        prod3.Start_Date__c = date.newinstance(1900,1,1);
        prod3.End_Date__c = date.newinstance(1900,1,1);
        Insert prod3;
        
        //Domain Assurance
        OpportunityLineItem prod4 = new OpportunityLineItem();
        prod4.OpportunityId = opp1.id;
        prod4.PricebookEntryId = '01u00000000FIhMAAW';
        prod4.Quantity = 1;
        prod4.UnitPrice = 55000;
        prod4.Product_Status__c = 'Current';
        prod4.Description__c = 'Domain Assurance';
        prod4.Indirect__c = false;
        prod4.previous_value__c = 0;
        prod4.Active__c = true;
        prod4.Bundle__c = false;
        prod4.Recurring__c = true;
        Insert prod4;
        
        //Tools Child
        OpportunityLineItem prod8 = new OpportunityLineItem();
        prod8.OpportunityId = opp1.id;
        prod8.PricebookEntryId = '01u00000000FGxkAAG';
        prod8.Quantity = 12;
        prod8.UnitPrice = 3500;
        prod8.of_MM_Events_Allowed__c = 20;
        prod8.of_CP_Events_Allowed__c = 20;
        prod8.MM_Overage_Fee__c = 25;
        prod8.CP_Overage_Fee__c = 25;
        prod8.of_BLA_IPs__c = 10;
        prod8.of_Rep_Mon_IPs__c = 10;
        prod8.Product_Status__c = 'Current';
        prod8.Description__c = 'Deliverability Tools';
        prod8.Indirect__c = true;
        prod8.Active__c = true;
        prod8.previous_value__c = 0;
        prod8.Bundle__c = false;
        prod8.Recurring__c = true;
        prod8.Start_Date__c = date.newinstance(1900,1,1);
        prod8.End_Date__c = date.newinstance(1900,1,1);
        Insert prod8;
        
        Account acct3 = new Account();
        acct3.name = 'RPTestacct2';
        acct3.Tools_Start_Date__c = date.newinstance(2000,1,1);
        acct3.Certification_Start_Date__c = date.newinstance(2000,1,1);
        acct3.DA_Start_Date__c = date.newinstance(2000,1,1);
        acct3.Domain_Assurance_Renewal_Date__c = date.newinstance(2000,1,1);
        acct3.Renewal_Date__c = date.newinstance(2000,1,1);
        acct3.Renewed_to_Date__c = date.newinstance(2000,1,1);
        acct3.ProServ_End_Date__c = date.newinstance(2000,1,1);
        acct3.Monthly_MM_Events__c = 5;
        acct3.Monthly_CP_Events__c = 5;
        acct3.MM_Overage_Fee__c = 5;
        acct3.CP_Overage_Fee__c = 5;
        acct3.Blacklist_Alert_IPs__c = 5;
        acct3.Reputation_Monitor_IPs__c = 5;
        acct3.accred_non_recurring_direct__c = 1000;
        acct3.accred_non_recurring_indirect__c = 1000;
        acct3.Internal_Use_Recurring_Indirect__c = 1000;
        acct3.Tools_Child_Recurring_Indirect__c = 1000;
        acct3.SSRA_Recurring_Indirect__c = 1000;
        acct3.SSC_Referral_Child_Recurring_Indirect__c = 1000;
        acct3.SSC_SME_Recurring_Indirect__c = 1000;
        acct3.Tools_Reseller_API_Recurring_Indirect__c = 1000;
        acct3.ProServ_Recurring_Direct__c = 1000;
        acct3.ProServ_Recurring_Indirect__c = 1000;
        acct3.ProServ_Non_Recurring_Direct__c = 1000;
        acct3.ProServ_Non_Recurring_Indirect__c = 1000;
        acct3.ACV_Inbox_Measurement_Recurring_Direct__c = 1000;
        acct3.ACV_Pixel_Tracking_Recurring_Direct__c = 1000;
        acct3.ACV_CorpDev_Research_Non_Recurring_Indi__c = 1000;
        acct3.ACV_Regional_ISP_Med_Non_Recurring_Indi__c = 1000;
        acct3.Tools_Overage_Fees__c = 1000;
        acct3.Additional_AM_Services__c = 1000;
        Insert acct3;

        Opportunity opp3 = new Opportunity();
        opp3.AccountId = acct3.id; 
        opp3.Name = 'RPTestOpp2'; 
        opp3.StageName = 'Negotiating'; 
        opp3.CloseDate = Date.valueOf('2009-01-01'); 
        opp3.OwnerId = '00500000006olmF'; 
        opp3.RecordTypeId = '012000000004u5b';
        opp3.Approval_Type__c = 'Pricing Approval';
        opp3.Pricebook2Id = '01s000000004NS6AAM';
        opp3.Type = 'Extension';
        opp3.product__c = 'Internal Use;Certification Referral Child;Professional Services';
        opp3.TermofContractinmonths__c = '12';
        opp3.Effective_Date__c = Date.valueOf('2009-01-01');
        opp3.currencyisocode = 'GBP';
        Insert opp3;

        Partner P1 = new Partner();
        P1.OpportunityId = opp1.id;
        P1.AccounttoId = acct3.id;
        P1.IsPrimary = true;
        P1.Role = 'Channel';
        insert P1;
        
        //Internal Use
        OpportunityLineItem prod5 = new OpportunityLineItem();
        prod5.OpportunityId = opp3.id;
        prod5.PricebookEntryId = '01u00000000FGyNAAW';
        prod5.Quantity = 12;
        prod5.UnitPrice = 3500;
        prod5.of_MM_Events_Allowed__c = 20;
        prod5.of_CP_Events_Allowed__c = 20;
        prod5.MM_Overage_Fee__c = 25;
        prod5.CP_Overage_Fee__c = 25;
        prod5.of_BLA_IPs__c = 10;
        prod5.of_Rep_Mon_IPs__c = 10;
        prod5.Product_Status__c = 'Current';
        prod5.Description__c = 'Deliverability Tools';
        prod5.Indirect__c = true;
        prod5.Active__c = true;
        prod5.previous_value__c = 0;
        prod5.Bundle__c = false;
        prod5.Recurring__c = true;
        prod5.Start_Date__c = date.newinstance(1900,1,1);
        prod5.End_Date__c = date.newinstance(1900,1,1);
        Insert prod5;
        
        //Certification Referral Child
        OpportunityLineItem prod6 = new OpportunityLineItem();
        prod6.OpportunityId = opp3.id;
        prod6.PricebookEntryId = '01u00000000FGwdAAG';
        prod6.Quantity = 1;
        prod6.UnitPrice = 25000;
        prod6.Product_Status__c = 'Current';
        prod6.Description__c = 'Return Path Certification';
        prod6.Indirect__c = true;
        prod6.Active__c = true;
        prod6.previous_value__c = 0;
        prod6.Bundle__c = false;
        prod6.Recurring__c = true;
        Insert prod6;
        
        //Professional Services
        OpportunityLineItem prod7 = new OpportunityLineItem();
        prod7.OpportunityId = opp3.id;
        prod7.PricebookEntryId = '01u00000000FGwzAAG';
        prod7.Quantity = 1;
        prod7.UnitPrice = 25000;
        prod7.Product_Status__c = 'Current';
        prod7.Description__c = 'ProServ';
        prod7.Indirect__c = true;
        prod7.Active__c = true;
        prod7.previous_value__c = 0;
        prod7.Bundle__c = false;
        prod7.Recurring__c = false;
        Insert prod7;
        
        //Tools Only
        OpportunityLineItem prod9 = new OpportunityLineItem();
        prod9.OpportunityId = opp3.id;
        prod9.PricebookEntryId = '01u00000000FPd2AAG';
        prod9.Quantity = 12;
        prod9.UnitPrice = 3500;
        prod9.of_MM_Events_Allowed__c = 20;
        prod9.of_CP_Events_Allowed__c = 20;
        prod9.MM_Overage_Fee__c = 25;
        prod9.CP_Overage_Fee__c = 25;
        prod9.of_BLA_IPs__c = 10;
        prod9.of_Rep_Mon_IPs__c = 10;
        prod9.Product_Status__c = 'Current';
        prod9.Description__c = 'Deliverability Tools';
        prod9.Indirect__c = false;
        prod9.Active__c = true;
        prod9.previous_value__c = 0;
        prod9.Bundle__c = false;
        prod9.Recurring__c = true;
        prod9.Start_Date__c = date.newinstance(1900,1,1);
        prod9.End_Date__c = date.newinstance(1900,1,1);
        Insert prod9;

        List<Opportunity> OppsToUpdate = new list<Opportunity>();
        
        opp1.StageName = 'Finance Approved';
        opp1.Run_ACV__c = 'Yes';
        OppsToUpdate.add(opp1);
        
        opp3.StageName = 'Finance Approved';
        opp3.Run_ACV__c = 'Yes';
        OppsToUpdate.add(opp3);
        
        test.startTest();
        Update OppsToUpdate;
        test.stopTest();
        
        
        }    
    }*/
    
}