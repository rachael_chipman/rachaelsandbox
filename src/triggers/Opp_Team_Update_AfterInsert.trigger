trigger Opp_Team_Update_AfterInsert on Opportunity (After Insert) {

for(Opportunity o : Trigger.New){

if(o.OwnerId != null && o.IsClosed == false){


List<OpportunityTeamMember> otm = [select Id, Percent_Commissioned_on__c from OpportunityTeamMember where OpportunityId = :o.Id ];

 if (otm.isEmpty()) return;

    for (OpportunityTeamMember otms: otm) otms.Percent_Commissioned_on__c = 100;

    update otm;


}
}
}