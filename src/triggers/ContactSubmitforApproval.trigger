trigger ContactSubmitforApproval on Contact (after update) {

list<Approval.ProcessSubmitRequest> reqList = new list<Approval.ProcessSubmitRequest>();
 
    for (Integer i = 0; i < Trigger.new.size(); i++) {
 
    Boolean inApproval = Trigger.new[i].Awaiting_Approval__c;
     
 System.debug('\n\n Old Submit = '+ Trigger.old[i].Submit__c  + '\n New Submit = '+Trigger.new[i].Submit__c +'\n\n');
 System.debug('\n\n Old OwnerId = '+ Trigger.old[i].OwnerId  + '\n New OwnerId = '+Trigger.new[i].OwnerId +'\n\n');
 
 
        if ( Trigger.new[i].Submit__c == true && inApproval == false)  {
        
 
            // create the new approval request to submit
            Approval.ProcessSubmitRequest req = new Approval.ProcessSubmitRequest();
            req.setComments('Submitted for approval. Please approve.');
            req.setObjectId(Trigger.new[i].Id);
            reqList.add(req);
            // submit the approval request for processing
            //Approval.ProcessResult result = Approval.process(req);
 
        }
 
    }
    
    approval.ProcessResult[] result = Approval.process(reqList);
 
}