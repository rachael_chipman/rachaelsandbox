@isTest (seeAllData=true)
Public class PortalWizardReferral_Controller_Test{
    public static testMethod void testPortalWizardReferral_Controller(){
    PageReference pageRef = Page.PortalWizardReferral;
Test.setCurrentPageReference(pageRef);

User user1;
Account portalAccount1;

UserRole portalRole = [Select Id From UserRole Where PortalType = 'None' Limit 1];
system.debug('portalRole is ' + portalRole);

Profile profile1 = [Select Id from Profile where name = 'Exec Admin'];

User portalAccountOwner1 = new User(
UserRoleId = portalRole.Id,
ProfileId = profile1.Id,
Username = String.valueOf(DateTime.now().getTime()) + 'test2@test.com',
Alias = 'batman',
Email='bruce.wayne@wayneenterprises.com',
EmailEncodingKey='UTF-8',
Firstname='Bruce',
Lastname='Wayne',
LanguageLocaleKey='en_US',
LocaleSidKey='en_US',
TimeZoneSidKey='America/Chicago',
Bypass_Validation_Rules__c = true
);
Database.insert(portalAccountOwner1);

//User u1 = [Select ID From User Where Id =: portalAccountOwner1.Id];

System.runAs ( portalAccountOwner1 ) {
//Create account

Account PartnerAccount1 = new Account(
Name = 'TestAccount Partner',
OwnerId = portalAccountOwner1.Id,
CRM__c = '00500000006olmF'
);
Database.insert(PartnerAccount1);

portalAccount1 = new Account(
Name = 'TestAccount',
ParentId = PartnerAccount1.Id,
OwnerId = portalAccountOwner1.Id
);
Database.insert(portalAccount1);

//Create contact
Contact contact1 = new Contact(
FirstName = 'Test',
Lastname = 'McTesty',
AccountId = portalAccount1.Id,
Email = String.valueOf(DateTime.now().getTime()) + 'test@test.com'
);
Database.insert(contact1);

//Create user
Profile portalProfile = [SELECT Id FROM Profile where Id = '00e000000073Tjc'];
user1 = new User(
Username = String.valueOf(DateTime.now().getTime())+ 'test12345@test.com',
ContactId = contact1.Id,
ProfileId = portalProfile.Id,
Alias = 'test123',
Email = 'test12345@test.com',
EmailEncodingKey = 'UTF-8',
LastName = 'McTesty',
CommunityNickname = 'test12345',
TimeZoneSidKey = 'America/Los_Angeles',
LocaleSidKey = 'en_US',
LanguageLocaleKey = 'en_US',
Bypass_Validation_Rules__c = true
);
Database.insert(user1);
}

System.runAs (user1){


Lead l = new Lead(LastName = 'Test', Company = 'RP Test co');
insert l;

    ApexPages.StandardController sc = new ApexPages.standardController(l);

PortalWizardReferral_Controller oPEE = new PortalWizardReferral_Controller(sc);
oPEE.l.LastName = 'Test LN';
oPEE.l.Company = 'Test Co';
oPEE.getPCs();
oPEE.Cancel();
oPEE.Next();


}}}