@isTest
private class GenRefOppOwnerChangeTest {
	
	private static testMethod void testSingleOppUpdate()
	{
		Account account = new Account (Name='Test Account');
		insert account;
		
		Opportunity testOpp = testUtils.createOpportunity(testUtils.RESEARCH_RECORD_TYPE.Id, account.Id, true);
		
		testOpp.Gen_Referral_Opp__c = true;
		testOpp.Gen_Ref_CRM_Approved_Time_Stamp__c = system.now();
		testOpp.Gen_Ref_Originated_By__c = 'Channel Partner';
		testOpp.Approval_Type__c = 'Gen Ref Approval';
		testOpp.RSE_for_Gen_Ref_Approval__c = TestUtils.SALES_USER.Id;
		update testOpp;

		Approval.ProcessSubmitRequest req1 = new Approval.ProcessSubmitRequest();
		req1.setComments('Submitting Opp for Approval');
		req1.setObjectId(testOpp.Id);
		req1.setNextApproverIds(new Id[] {UserInfo.getUserId()});
		
		Approval.ProcessResult result = Approval.process(req1);
		
		system.assert(result.isSuccess());
		
		System.assertEquals('Pending', result.getInstanceStatus(), 'Instance Status'+result.getInstanceStatus());
		
		List<Id> newWorkItemIds = result.getNewWorkitemIds();
		
		Approval.ProcessWorkitemRequest req2 = 
            new Approval.ProcessWorkitemRequest();
        req2.setComments('Approving request.');
        req2.setAction('Approve');
        req2.setNextApproverIds(new Id[] {UserInfo.getUserId()});
        
        // Use the ID from the newly created item to specify the item to be worked
        req2.setWorkitemId(newWorkItemIds.get(0));
        
        //approve the request
        Approval.ProcessResult result2 =  Approval.process(req2);
        
        // Verify the results
        System.assert(result2.isSuccess(), 'Result Status:'+result2.isSuccess());
        
        System.assertEquals(
            'Pending', result2.getInstanceStatus(), 
            'Instance Status'+result2.getInstanceStatus());
            
        List<Id> newWorkItemIds2 = result2.getNewWorkitemIds();
        
        Approval.ProcessWorkitemRequest req3 = 
            new Approval.ProcessWorkitemRequest();
        req3.setComments('Approving request.');
        req3.setAction('Approve');
        req3.setNextApproverIds(new Id[] {UserInfo.getUserId()});
        
        // Use the ID from the newly created item to specify the item to be worked
        req3.setWorkitemId(newWorkItemIds2.get(0));
        
        // Submit the request for approval
        test.startTest();
        Approval.ProcessResult result3 =  Approval.process(req3);
        test.stopTest();
        
        // Verify the results
        System.assert(result3.isSuccess(), 'Result Status:'+result3.isSuccess());
        
        System.assertEquals(
            'Approved', result3.getInstanceStatus(), 
            'Instance Status'+result3.getInstanceStatus());
	}

}