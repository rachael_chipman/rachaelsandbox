@isTest
public Class GenRefFSROwnerChange_test{
    
   /* private static testmethod void testGenRefFSROwnerChange(){
    
//Insert User
    User u = new user();
    u.Alias = 'tstuser';
    u.Email='testrc@rp.com';
    u.EmailEncodingKey='UTF-8'; 
    u.LastName='Testing';
    u.LanguageLocaleKey='en_US';
    u.LocaleSidKey='en_US';
    u.ProfileId = '00e0000000734LT'; 
    u.TimeZoneSidKey='America/Los_Angeles'; 
    u.UserName='testrc@rp.com.rp';
        
    insert u;    
    
//Insert a second User
    User u2 = new user();
    u2.Alias = 'tstuser2';
    u2.Email='testrc2@rp.com';
    u2.EmailEncodingKey='UTF-8'; 
    u2.LastName='Testing';
    u2.LanguageLocaleKey='en_US';
    u2.LocaleSidKey='en_US';
    u2.ProfileId = '00e0000000734LT'; 
    u2.TimeZoneSidKey='America/Los_Angeles'; 
    u2.UserName='testrc2@rp.com.rp';
        
    insert u2;
    
//Insert Lead
    Lead l = new Lead ();
    l.OwnerId = u2.Id;
    l.Referring_CRM2__c = u2.Id;
    l.Company = 'Test Company';
    l.LastName = 'Thecat';
    l.Gen_Ref_Lead__c = true;
    l.Status = 'Qualified';
    l.Campaign_Name__c = '2012_GL_PRO_WF_.NET Deliverability Channel Referral Form NEW';
    
    insert l;
    
    //Update Lead as original User
    System.runAs(u){
    
    l.Status = 'Accepted';
    l.Submit__c = false;
    update l;
    }
}

 private static testMethod void testChangingOwnerOnContact(){
        
        List<User> testUsers = new List<User>();
        testUsers.add ( TestUtils.getTestUser( 'TestUser123', 'Exec Admin') );
        testUsers.add ( TestUtils.getTestUser( 'TestUser456', 'Exec Admin') );
         
        System.runAs( TestUtils.ADMIN_USER ){
            insert testUsers;
        } 
        
        Lead testLead = TestUtils.createLeads( 'Test Company', 'Test Lead', 'Qualified', false );
        testLead.Campaign_Name__c = '2012_GL_PRO_WF_.NET Deliverability Channel Referral Form NEW';
        testLead.OwnerId = testUsers[0].Id;
        testLead.Referring_CRM2__c = testUsers[0].Id;
        testLead.Gen_Ref_Lead__c = true;
        insert testLead;
        
        Test.startTest();
            System.runAs ( testUsers[1] ){
                testLead.Status = 'Accepted';
                testLead.Submit__c = false;
                update testLead;
            }
        Test.stopTest();
        
        List<Lead> updatedLead = [SELECT Id, OwnerId FROM Lead];
        
        System.assertEquals ( 1, updatedLead.size(), 'There should only be one contact that was created in the test');
        System.assertEquals ( testUsers[1].Id, updatedLead[0].OwnerId, 'The owner should be set to the user updating the contact' );
        
    }*/
}