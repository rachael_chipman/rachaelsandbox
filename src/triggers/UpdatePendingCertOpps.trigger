//Updating this class IAW the request to no longer Finance Approve the License Fee Opps
//https://trello.com/c/HONIsx15/1449-certification-application-activation-error-send-to-z-billing-issue
//https://trello.com/c/i1RmXihg/1444-stop-forcing-finance-approvals
trigger UpdatePendingCertOpps on Certification_Application__c (before update) {


    list<Opportunity> UpdatePendingCertOpps = new list<Opportunity>();
    list<OpportunityLineItem> UpdatePendingCertOlis = new list<OpportunityLineItem>();

     public String Cloned;
     public Opportunity PendingCertOpp;  
     public OpportunityLineItem PendingCertoli;


    for(Certification_Application__c ca:trigger.new)
    {
	    if((ca.Activation_Date__c != null || ca.Application_Result__c == 'Fail') && ca.License_Fee_Opp_Stage__c != 'Finance Approved' && ca.License_Fee_Opp_Stage__c != 'Lost' && ca.License_Fee_Opp_Stage__c != 'Terminated Contract' )
	    {
	         /*Account acct = [SELECT Id, SSC_Start_Date__c FROM Account WHERE Id = :ca.Account__c];
	         //update account RPC Start Date if application passed 
	         system.debug('Is there an Account?'+ca.Account__r.SSC_Start_Date__c);
	         /*if(ca.Activation_Date__c != null && ca.Account__c != null)
	         { 
	         	acct.SSC_Start_Date__c = ca.Activation_Date__c;
	         	update acct;
	         }*/
	         //update Cert App to closed even if the Opp doesn't exist
	         if(ca.License_Fee_Opp_Stage__c != 'Pending Certification Qualification' && ca.Activation_Date__c != NULL)
	         {
	         	ca.addError('You cannot activate this application until the related license opportunity is in Pending Certificaiton Qualification stage.');
	         }
	         if(ca.Application_Status__c == 'Audit Complete' && (ca.Application_Result__c == 'Pass' || ca.Application_Result__c == 'Fail'))
	         {
	             ca.Application_Status__c = 'Closed';
	         } 
	         if(ca.Application_Result__c != 'Pass' && ca.Application_Result__c != 'Fail')
	         {
	             ca.Early_Activation__c = true;
	         } 
	     
	        //Get Pending Cert Opp
	        try
	        {
	            //if Activation date is added to the cert ap, then look for opp that has stagename  pending certification qualification

	            Certification_Application__c Old_CA = trigger.oldMap.get(ca.Id);
	            System.debug('Pending Cert Opp is :' + Old_CA);
	            System.debug('Pending Cert Opp is :' + Old_CA.Application_Result__c);
	            System.debug('New App result is :' + ca.Application_Result__c);
	            if(Old_Ca!= null)
	            {
	            	System.debug('Old Activation date is :' +Old_CA.Activation_Date__c);
	            	System.debug('New Activation date is :' +ca.Activation_Date__c);
	            	if(Old_CA.Activation_Date__c != ca.Activation_Date__c)
		            {
			            PendingCertOpp = [SELECT Id, StageName, Application_Result__c, isClosed, Activation_Date__c, SSC_Volume__c, Certification_Tier_Sold__c, Awaiting_Approval__c, Cloned_From_ID__c
			            FROM Opportunity
			            WHERE AccountId = :ca.Account__c and Application_Result__c = null and StageName = 'Pending Certification Qualification'];
			        }
            		if(Old_CA.Application_Result__c != 'Fail' && ca.Application_Result__c == 'Fail')
			        {
			        	PendingCertOpp = [SELECT Id, StageName, Application_Result__c, isClosed, Activation_Date__c, SSC_Volume__c, Certification_Tier_Sold__c, Awaiting_Approval__c, Cloned_From_ID__c
			            FROM Opportunity
			            WHERE Id = :ca.License_Fee_Opportunity__c ];

			            if(PendingCertOpp != NULL && PendingCertOpp.Awaiting_Approval__c == true)
			            {
			            	//recall pending approval associated with the Opportunity.
			            	List<ProcessInstanceWorkItem> workItem = [Select Id, ProcessInstance.TargetObjectId from ProcessInstanceWorkitem where 
                                                                      ProcessInstance.TargetObjectId = :PendingCertOpp.Id ];
                            
                            System.debug('WorkItem is :' + workItem);
                            if(workItem!= null && !workItem.isEmpty())
                            {
                                ProcessInstanceWorkItem pwrItem = workItem[0];
                                Approval.ProcessWorkitemRequest pwr = new Approval.ProcessWorkitemRequest();
                                pwr.setWorkitemId(pwrItem.Id);
                                pwr.setAction('Removed');
                                Approval.ProcessResult res = Approval.process(pwr);
                            }
			            	
                            
			            }
				        
			        }
			        system.debug('What is the Pending Cert Opp'+ PendingCertOpp);
			    }

	        }
	    	
	        catch (system.queryexception PendingCertOpp1)
	        {
	            return;
	        }    
	        
           //Build list of OLIs that will be updated. Cert line item and Parter Commission -- if channel 
           /*List<OpportunityLineItem> olisToUpdate = [SELECT Id, Start_Date__c, End_Date__c, Product_Status__c, Product_Code__c,
	            					UnitPrice, OpportunityId
	            					FROM OpportunityLineItem
	            					WHERE  OpportunityId = :PendingCertOpp.Id and (Product_Code__c = '004-CERT-FEE0' or Product_Code__c = '005-CERT-REFC' or Product_Code__c = 'REF-CR' or Product_Code__c = 'RES-CR')];
           		system.debug(olisToUpdate.size());*/
			
	        //Set variable for Cloned
	        /*Cloned = (
	        PendingCertOpp.Cloned_From_Id__c != null ? 'Cloned':
	        PendingCertOpp.Cloned_From_Id__c == null ? 'Not Cloned':'');*/
			if(PendingCertOpp != NULL)
			{
		        system.debug('Does the PendingCertOpp meet the if statement?'+ PendingCertOpp.Awaiting_Approval__c);
		        if(PendingCertOpp.Awaiting_Approval__c == false)
		        {
		        	Opportunity updateOpp = new Opportunity( Id = PendingCertOpp.Id );
	                
	                /*for(OpportunityLineItem updateoli: olisToUpdate){
	                updateoli.Bond_Group_ID__c = ca.Bond_Group_ID__c;
		        	updateoli.Certification_Type_Active__c = ca.Certification_Type__c;
		        	updateoli.Agreement_Effective_Date__c = ca.Agreement_Effective_Date__c;
			        updateoli.Agreement_Expire_Date__c = ca.Agreement_Expire_Date__c;
			        updateoli.Agreement_ID__c = ca.Agreement_ID__c;
			        updateoli.Agreement_Version__c = ca.Agreement_Version__c;
			        updateoli.Signed_by_Company__c = ca.Signed_by_Company__c;
			        updateoli.Signed_by_Email__c = ca.Signed_by_Email__c;
			        updateoli.Signed_by_IP__c = ca.Signed_by_IP__c;
			        updateoli.Signed_by_Name__c = ca.Signed_by_Name__c;
			        updateoli.Signed_by_Title__c = ca.Signed_by_Title__c;
			        updateoli.Standards_Accountable__c = ca.Standards_Accountable__c;
			        updateoli.Standards_Compliance__c = ca.Standards_Compliance__c;
			        updateoli.Standards_Read__c = ca.Standards_Read__c;
			        updateoli.Standards_Version__c = ca.Standards_Version__c;
	                
	                }*/
	                    
		            if(ca.Application_Result__c == 'Fail')
		            {
		            	System.debug('Marking the stage to Lost');
		            	updateOpp.Updated_By_Cert_App__c = true;
		            	updateOpp.Certification_Max_Volume__c = ca.Certification_Max_Volume__c;
		            	updateOpp.Certification_Tier_Sold__c = ca.Certification_Volume__c;
		            	updateOpp.Application_Result__c = ca.Application_Result__c;
		            	//updateOpp.RecordTypeId = '012000000004wVU';
		            	updateOpp.StageName = 'Lost';
		            	updateOpp.Reasons_for_Lost_Opportunities__c = 'RP Initiated';
		            	updateOpp.Lost_Pass_Details__c = 'Cert Application Failed';
		            	//updateOpp.Run_ACV__c = 'Yes'; 
		            	updateOpp.CloseDate = system.today();
		            	
	                    /*for(OpportunityLineItem updateoli: olisToUpdate){
		            	updateoli.Start_Date__c = system.today();
		            	updateoli.End_Date__c = system.today();
	                    
	                    }*/
		            }
		            
		            else if(ca.Application_Result__c != 'Fail')
		            {
		            	updateOpp.Updated_By_Cert_App__c = true;
		            	updateOpp.Certification_Max_Volume__c = ca.Certification_Max_Volume__c;
		            	updateOpp.Certification_Tier_Sold__c = ca.Certification_Volume__c;
		            	updateOpp.Application_Result__c = ca.Application_Result__c;
		            	updateOpp.Activation_Date__c = ca.Activation_Date__c;
		            	updateOpp.Bond_Group_ID__c = ca.Bond_Group_ID__c;
		            	//updateopp.RecordTypeId ='012000000004x5k';
		            	updateOpp.RecordTypeId = FindCreateCertOpp.PURCHASE_RECORD_TYPE.Id;
		            	//updateopp.StageName = 'Finance Approved';
		            	updateOpp.Approval_Type__c = 'Finance Approval';
		            	updateOpp.Submit_From_Trigger__c = 'Yes';
		            	updateOpp.CloseDate = system.today();
		            	
	                    /*for(OpportunityLineItem updateoli: olisToUpdate){
		            	updateoli.Start_Date__c = ca.Activation_Date__c;
		            	updateoli.End_Date__c = ca.Renewed_to_Date__c;
		            	updateoli.Product_Status__c = 'Current';
	                        
	                    }*/
	                                        
		            }
	                
		            //update olisToUpdate;
		            
		            // Update the Pending Cert opportunity after opportunity line items because validation rule doesn't allow
		            // opportunity line items to be updated after opportunity record is locked.
		            if ( updateopp.StageName == 'Lost' )
		            {
		                updateopp.Record_Locked__c = true; // lock record if stage is lost
		            }
		            
		            UpdatePendingCertOpps.add(updateopp); 
		            system.debug('What is in UpdatePendingCertOpps'+ UpdatePendingCertOpps);
		            update UpdatePendingCertOpps;
		        }
				
			}
	        
	        //If there is an app fee Opportunity mark it with Pass details as well
	        if(ca.Promo_Type_Picklist__c == 'Walk-Up' && ca.Activation_Date__c != null && ca.Application_Fee_Opportunity__c != null && ca.Application_Fee_Opportunity__r.Application_Result__c == null)
	        {
	            Opportunity op = [select Id, Application_Result__c from Opportunity where Id = :ca.Application_Fee_Opportunity__c];
	            op.Application_Result__c = 'Pass';
	            update op;
	            /*try{
		            OpportunityLineItem oli = [select Id, Start_Date__c, End_Date__c from OpportunityLineItem where OpportunityId = :op.Id];
		            if(oli!= null)
		            {
		            	oli.Start_Date__c = system.today();
			            oli.End_Date__c = system.Today()+365;
			            update oli;    
			        }
			    }
			    catch(Exception ex)
			    {
			    	System.debug('Exception occured');
			    }*/
	        }
	            
	        //If there is an App Fee opp and the App failed - mark the opportunity with the appropriate details
	        if(ca.Application_Result__c == 'Fail' && ca.Application_Fee_Opportunity__c != null && ca.Application_Fee_Opportunity__r.Application_Result__c == null)
	        {
	
	            Opportunity op = [select Id, Application_Result__c from Opportunity where Id = :ca.Application_Fee_Opportunity__c];
	            op.Application_Result__c = 'Fail';
	            update op;
	            /*try
	            {
		            OpportunityLineItem oli = [select Id, Start_Date__c, End_Date__c from OpportunityLineItem where OpportunityId = :op.Id];
		            if(oli!= null)
		            {
			            oli.Start_Date__c = system.today();
			            oli.End_Date__c = system.Today();
			            update oli;    
			        }

			    }
			    catch(Exception ex)
			    {
			    	System.debug('Exception occured');
			    }*/
		
	            ca.Application_Status__c = 'Closed';
	        }    
	    
	    }

    }
    
}