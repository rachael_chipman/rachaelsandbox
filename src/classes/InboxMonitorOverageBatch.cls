global class InboxMonitorOverageBatch implements Database.Batchable<sObject>, Database.AllowsCallouts, Schedulable {
	
	public static final String BATCH_NAME = 'InboxMonitorOverageBatch';
    public static final String[] ERROR_EMAILS = new String[]{ 'salesforceplatform@returnpath.com' };
	Map<Id,Mailbox_Monitor_Events__c> eventMap;
    global String errors = '';
    global final String query;
	
	/*global InboxMonitorOverageBatch(Map<Id, Mailbox_Monitor_Events__c> triggerEventMap) 
	{
		eventMap = triggerEventMap;	
	}*/

	/*global InboxMonitorOverageBatch(String queryString)
	{
		query = queryString;
	}*/

	global Date usageMonth = system.today().addMonths(-1).toStartOfMonth();

	global String buildQuery()
	{
		String queryString = 'SELECT Id, Name, Campaigns__c, Account__c, Month__c, Inbox_Monitor_Limit__c FROM Mailbox_Monitor_Events__c WHERE Month__c = : usageMonth AND CreatedDate = THIS_MONTH';
		if(test.isRunningTest())
		{
			queryString += ' AND Name = ' + '\'TEST_IM_EVENT\'' + ' LIMIT 200';
		}
		return queryString;
	}

	/*global InboxPreviewOverageBatch(String queryString)
	{
		query = queryString;
	}*/

	global void execute(SchedulableContext sc)
	{
		InboxMonitorOverageBatch schedBatch = new InboxMonitorOverageBatch();
		Database.executeBatch(schedBatch);
	}
	
	global Database.QueryLocator start(Database.BatchableContext BC) {
		return Database.getQueryLocator(buildQuery());
	}

   	global void execute(Database.BatchableContext BC, List<sObject> scope) 
   	{
		try
		{
			IMOverageOpportunityGenerationService.processTrigger(scope);
		}
		catch (DmlException dmx)
		{
			for(Integer i = 0; i < dmx.getNumDml(); i++)
			{
				errors += dmx.getDmlMessage(i);
			}
		}
	}
	
	global void finish(Database.BatchableContext BC) 
	{
		AsyncApexJob job = [SELECT Id, Status, NumberOfErrors,
	                         JobItemsProcessed, TotalJobItems,
	                         CreatedBy.Email
	                         FROM AsyncApexJob
	                         WHERE Id =:bc.getJobId()];

	     Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();

	     mail.setToAddresses( ERROR_EMAILS );
	     mail.setSubject( 'Batch to ' + BATCH_NAME + ' ' + job.Status );

	     String emailBody = 'Batch to ' + BATCH_NAME + ' processed '
	                      + job.TotalJobItems
	                      + ' batches with '
	                      + job.NumberOfErrors
	                      + ' failures.';
	     if( errors != '' )
	     {
		     emailBody += '\n\n\nThe following errors occured:\n'+ errors;
		     mail.setPlainTextBody( emailBody );
		     Messaging.sendEmail( new Messaging.SingleEmailMessage[]{ mail } );
	     }		
	}
	
}