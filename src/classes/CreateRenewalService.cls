public with sharing class CreateRenewalService {
    
    public static FINAL String WHERE_CLAUSE_OPP = 'Type != \'Supplement\' AND Type != \'Fee\' AND Id IN : newOpps';
    public static FINAL String WHERE_CLAUSE_OLI = 'Recurring__c = true AND OpportunityId IN : oppsThatNeedRenewals';
    public static FINAL String TRIAL = 'Trial';
    public static FINAL String TRIAL_EXT = 'Trial - Extension';
    public static FINAL String CLIENT = 'Current Client/Partner';
    public static FINAL String EXT = 'Extension';
    public static FINAL String RENEWAL = 'Renewal';
    public static FINAL String NEW_BIZ = 'New Business';
    public static FINAL String CX_SELL = 'Cross-Sell';
    public static FINAL String YES = 'Yes';
    public static FINAL String RM = 'Relationship Manager';
    public static FINAL String CRM = 'Channel Manager';
    public static FINAL String EST_VAL_STAGE = 'Establishing Value';
    public static FINAL String SIG_STAGE = 'Signature';
    public static FINAL String LEGAL_APP = 'Legal Approval';
    public static FINAL String FINANCE_APP = 'Finance Approval';
    public static FINAL Set<String> TRIAL_TYPES = new Set<String>{TRIAL, TRIAL_EXT};
    public static FINAL String LOW = 'Low';
    public static FINAL String HIGH = 'High';
    public static Map<Id, String> originalOppToType = new Map<Id, String>();
    public static FINAL List <String> SF_REQUESTS_EMAIL = new List<String>{'salesforcerequests@returnpath.com'};
    public static FINAL List<String> CC_EMAIL = new List<String>{'chris.kingdon@returnpath.com'};
    public static Set<Id> noOwnerOpps = new Set<Id>();
    public static User DAVID_REDPATH
    {
        get
        {
            if(DAVID_REDPATH == NULL)
            {
                DAVID_REDPATH = [SELECT Id FROM User WHERE Name = 'David Redpath' AND isActive = true];
            }
            return DAVID_REDPATH;
        }
        private set;
    }

    public static List<Opportunity> oppsThatNeedRenewals(List<Opportunity> newOpps)
    {
        String soql = SObjectTriggerServices.getCreatableFieldsSOQLOpp('Opportunity', WHERE_CLAUSE_OPP);
        List<Opportunity> oppsThatNeedRenewals = Database.query(soql);
        return oppsThatNeedRenewals;
    }

    public static List<OpportunityLineItem> olisForRenewalOpp(List<Opportunity> oppsThatNeedRenewals)
    {
        String soql = SObjectTriggerServices.getCreatableFieldsSOQL('OpportunityLineItem', WHERE_CLAUSE_OLI);
        List<OpportunityLineItem> originalOlis = Database.query(soql);
        return originalOlis;
    }

    public static Map<Id, Account> getAccounts(List<Opportunity> oppsThatNeedRenewals)
    {
        Map<Id, Account> oppToAccount = new Map<Id, Account>();
        Set<Id> accountIds = pluck.Ids('AccountId', oppsThatNeedRenewals);

        List<Account> accounts = [SELECT Id, Type, Cert_Only_Client__c, OwnerId, AM__c, (SELECT UserId, AccountId, IsDeleted, TeamMemberRole, CreatedDate 
            FROM AccountTeamMembers 
            WHERE IsDeleted != true ORDER BY CreatedDate  DESC) 
        FROM Account 
        WHERE Id IN : accountIds];

        Map<Id, Account> accountIdToAccount = new Map<Id, Account>(accounts);

        for(Opportunity opp : oppsThatNeedRenewals)
        {
            if(accountIdToAccount.containsKey(opp.AccountId))
            {
                oppToAccount.put(opp.Id, accountIdToAccount.get(opp.AccountId));
            }
        }
        return oppToAccount;
    }

    public static Map<Id, zqu__Quote__c> getPrimaryQuote(List<Opportunity> oppsThatNeedRenewals)
    {
       List<zqu__Quote__c> primaryQuotes = [SELECT Id, zqu__Opportunity__c, Is_Primary__c, zqu__RenewalTerm__c FROM zqu__Quote__c WHERE Is_Primary__c = true AND zqu__Opportunity__c IN : oppsThatNeedRenewals];

       Map<Id, zqu__Quote__c> oppIdToPrimaryQuote = new Map<Id, zqu__Quote__c>();
       for(zqu__Quote__c quote : primaryQuotes)
       {
        oppIdToPrimaryQuote.put(quote.zqu__Opportunity__c, quote);
       }
       return oppIdToPrimaryQuote;
    }

    public static List<Opportunity> renewalOpportunities(List<Opportunity> oppsThatNeedRenewals)
    {
        List<Opportunity> renewalOpps = new List<Opportunity>();
        List<Opportunity> inactiveRenewalOwnerOpps = new List<Opportunity>();
        Map<Id, User> inactiveOwnerOppIdToUser = new Map<Id, User>();
        List<Opportunity> renewalsToCreate = new List<Opportunity>();
        Map<Id, Account> oppToAccount = getAccounts(oppsThatNeedRenewals);
        Map<Id, zqu__Quote__c> oppToQuote = getPrimaryQuote(oppsThatNeedRenewals);

        for(Opportunity prevOpp : oppsThatNeedRenewals)
        {
            Id accountOwnerId;
            Id relationshipManager;
            Id clientRelationshipManager; 
            Boolean certOnlyAccount = false;
            originalOppToType.put(prevOpp.Id, prevOpp.Type);
            
            if(!oppToQuote.isEmpty() && oppToQuote.containsKey(prevOpp.Id) && 
                oppToQuote.get(prevOpp.Id).zqu__RenewalTerm__c != NULL && 
                oppToQuote.get(prevOpp.Id).zqu__RenewalTerm__c != 0)
            {
                system.debug(LoggingLevel.INFO, 'Do I have an account map?' + oppToAccount);
                if(!oppToAccount.isEmpty() && oppToAccount.containsKey(prevOpp.Id))
                {
                    system.debug(LoggingLevel.INFO,'Do I have an account map?' + oppToAccount);
                    accountOwnerId = oppToAccount.get(prevOpp.Id).OwnerId;
                    if(oppToAccount.get(prevOpp.Id).Cert_Only_Client__c == YES)
                    {
                        certOnlyAccount = true;
                    }
                    else
                    {
                        certOnlyAccount = false;
                    }
                    system.debug(LoggingLevel.INFO,'members in my map?' + oppToAccount.get(prevOpp.Id).AccountTeamMembers);
                    for(AccountTeamMember member : oppToAccount.get(prevOpp.Id).AccountTeamMembers)
                    {
                        system.debug(LoggingLevel.INFO,'Am I in the team memeber loop?' + oppToAccount.get(prevOpp.Id).AccountTeamMembers);
                        system.debug(LoggingLevel.INFO,'Member role: ' + member.TeamMemberRole);
                        if(member.TeamMemberRole == CRM)
                        {
                           clientRelationshipManager = member.UserId;
                        }
                        if(member.TeamMemberRole == RM)
                        {
                            relationshipManager = member.UserId;
                        }
                    }
                }
                Opportunity renewalOpp = prevOpp.clone(false, true, false, false);
                //Reset all the exit criteria fields
                renewalOpp.Description = null;
                renewalOpp.Problem_Buyer_articulated_we_can_solve__c = null;
                renewalOpp.Preliminary_timeline_for_decision__c = null;
                renewalOpp.Confirm_access_to_funding__c = null;
                renewalOpp.Describe_the_buying_team__c = null;
                renewalOpp.Describe_the_evaluation_criteria__c = null;
                renewalOpp.Describe_mutally_shared_goal__c = null;
                renewalOpp.Describe_their_desired_solution__c = null;
                renewalOpp.Describe_Buyer_Partner_steps_to_close__c = null;
                renewalOpp.Describe_Buyer_Partner_timing_to_close__c = null;
                renewalOpp.Confirm_the_Buyer_Approval_Process__c = null;
                renewalOpp.Document_Verbal_Agreement__c = null;
                renewalOpp.Estimated_Start_Date__c = null;
                renewalOpp.Date_Pre_Pipeline_ByPassed__c = null;
                renewalOpp.Date_Defining_Needs_ByPassed__c = null;
                renewalOpp.Date_Establishing_Value_ByPassed__c = null;
                renewalOpp.Date_Verbal_Commitment_ByPassed__c = null;
                renewalOpp.Date_Negotiating_Terms_ByPassed__c = null;
                //Set the fields that will be same for all renewals, then go through exceptions
                renewalOpp.Type = RENEWAL;
                renewalOpp.Previous_Opportunity_Lookup__c = prevOpp.Id;
                renewalOpp.StageName = EST_VAL_STAGE;
                renewalOpp.Approval_Type__c = LEGAL_APP;
                renewalOpp.Did_not_Fill_Pre_Pipeline_Exit_Criteria__c = true;
                renewalOpp.Did_not_Fill_Defining_Need_Exit_Criteria__c = true;
                renewalOpp.RecordTypeId = TestUtils.SELECT_RECORD_TYPE.Id;
                if(prevOpp.Minimum_Start_Date__c != NULL)
                {
                    if(Date.isLeapYear(prevOpp.Minimum_Start_Date__c.year()))
                    {
                        renewalOpp.CloseDate = prevOpp.Minimum_Start_Date__c.addYears(1).addDays(-1);
                    }
                    else
                    {
                        renewalOpp.CloseDate = prevOpp.Minimum_Start_Date__c.addYears(1);
                    }
                }
                else
                {
                    if(Date.isLeapYear(prevOpp.CloseDate.year()))
                    {
                        renewalOpp.CloseDate = prevOpp.CloseDate.addYears(1).addDays(-1);
                    }
                    else
                    {
                        renewalOpp.CloseDate = prevOpp.CloseDate.addYears(1);
                    }
                }
                if(accountOwnerId != NULL)
                {
                    if(accountOwnerId == DAVID_REDPATH.Id)
                    {
                        renewalOpp.OwnerId = DAVID_REDPATH.Id;
                    }
                    if(accountOwnerId != DAVID_REDPATH.Id) 
                    {
                        if(prevOpp.Cert_Only_Opportunity__c == YES) 
                        {
                            system.debug(LoggingLevel.INFO,'Cert only...');
                            if(certOnlyAccount)
                            {
                                system.debug(LoggingLevel.INFO,'Account is cert only');
                                if(prevOpp.Cert_Tier_High_or_Low__c == LOW)
                                {
                                    renewalOpp.OwnerId = TestUtils.SFORCE_AUTO.Id;
                                    renewalOpp.StageName = SIG_STAGE;
                                    renewalOpp.Approval_Type__c = FINANCE_APP;
                                    renewalOpp.Did_not_Fill_Est_Value_Exit_Criteria__c = true;
                                    renewalOpp.Did_not_Fill_Nego_Terms_Exit_Criteria__c = true;
                                    renewalOpp.Did_not_Fill_Verbal_Commit_Exit_Criteria__c = true;
                                    renewalOpp.RecordTypeId = TestUtils.PURCHASE_RECORD_TYPE.Id;
                                }
                                if(prevOpp.Cert_Tier_High_or_Low__c == HIGH)
                                {
                                    if(relationshipManager != NULL)
                                    {
                                        renewalOpp.OwnerId = relationshipManager;
                                    }
                                    else if(clientRelationshipManager != NULL)
                                    {
                                        renewalOpp.OwnerId = clientRelationshipManager;
                                    }
                                    else
                                    {
                                        renewalOpp.OwnerId = TestUtils.SFORCE_AUTO.Id;
                                    }
                                }
                            }
                            else if(!certOnlyAccount && oppToAccount.get(prevOpp.Id).Type != CLIENT)
                            {
                                system.debug(LoggingLevel.INFO,'Not Cert only adn Not Client...');
                                if(prevOpp.Cert_Tier_High_or_Low__c == LOW)
                                {
                                    renewalOpp.OwnerId = TestUtils.SFORCE_AUTO.Id;
                                    renewalOpp.StageName = SIG_STAGE;
                                    renewalOpp.Approval_Type__c = FINANCE_APP;
                                    renewalOpp.Did_not_Fill_Est_Value_Exit_Criteria__c = true;
                                    renewalOpp.Did_not_Fill_Nego_Terms_Exit_Criteria__c = true;
                                    renewalOpp.Did_not_Fill_Verbal_Commit_Exit_Criteria__c = true;
                                    renewalOpp.RecordTypeId = TestUtils.PURCHASE_RECORD_TYPE.Id;
                                }
                                if(prevOpp.Cert_Tier_High_or_Low__c == HIGH)
                                {
                                    if(relationshipManager != NULL)
                                    {
                                        renewalOpp.OwnerId = relationshipManager;
                                    }
                                    else if(clientRelationshipManager != NULL)
                                    {
                                        renewalOpp.OwnerId = clientRelationshipManager;
                                    }
                                    else
                                    {
                                        renewalOpp.OwnerId = TestUtils.SFORCE_AUTO.Id;
                                    }
                                }
                            }
                            else if (!certOnlyAccount && oppToAccount.get(prevOpp.Id).Type == CLIENT)
                            {
                                system.debug(LoggingLevel.INFO,'Not Cert only and Client...');
                                system.debug(LoggingLevel.INFO,'CRM: ' + clientRelationshipManager + CRM);
                                if(relationshipManager != NULL)
                                {
                                    renewalOpp.OwnerId = relationshipManager;
                                }
                                else if(clientRelationshipManager != NULL)
                                {
                                    renewalOpp.OwnerId = clientRelationshipManager;
                                }
                                else
                                {
                                    renewalOpp.OwnerId = accountOwnerId;
                                }
                            }
                        }
                    }
                }
                if(TRIAL_TYPES.contains(prevOpp.Type) || prevOpp.Type == EXT)
                {
                    renewalOpp.CloseDate = prevOpp.Max_End_Date__c;
                    if(TRIAL_TYPES.contains(prevOpp.Type))
                    {
                        renewalOpp.Generated_From_Free_Trial__c = true;
                        if(oppToAccount.get(prevOpp.Id).Type != CLIENT)
                        {
                            renewalOpp.Type = NEW_BIZ;
                        }
                        else
                        {
                            renewalOpp.Type = CX_SELL;
                        }
                    }
                }
                renewalOpps.add(renewalOpp);
            }
        }
        for(Opportunity renewalToCreate : renewalOpps)
        {
            String message = '';
            message += 'n\'' + 'RENEWAL NOTE FROM SFDC (CREATED): This renewal was set to be assigned to an inactive user. ';
            message += 'n\'' + 'Instead it has been assigned to that user' +'\''+'s First Approver or Manager.';
            Boolean ownerActive = ownerIsActive(renewalToCreate);
            system.debug('Can I see the owner status? ' + ownerActive + 'And the ID: ' + renewalToCreate.OwnerId + ' and the prevOpp: ' + renewalToCreate.Previous_Opportunity_Lookup__c);
            if(!ownerActive)
            {
                //proceed and add opp to the renewal list
                system.debug('Owner is inactive...');
                renewalToCreate.Description = message; 
                inactiveRenewalOwnerOpps.add(renewalToCreate);
            }
            else
            {
                system.debug('Owner is active...');
                renewalsToCreate.add(renewalToCreate);
            }
        }
        if(!inactiveRenewalOwnerOpps.isEmpty())
        {
            inactiveOwnerOppIdToUser = getUserInfo(inactiveRenewalOwnerOpps);
            system.debug('User Map: ' + inactiveOwnerOppIdToUser);
            if(!inactiveOwnerOppIdToUser.isEmpty())
            {
                List<Messaging.SingleEmailMessage> mails = new List<Messaging.SingleEmailMessage>();
                for(Opportunity opp : inactiveRenewalOwnerOpps)
                {
                    if(inactiveOwnerOppIdToUser.containsKey(opp.Previous_Opportunity_Lookup__c))
                    {
                        if(inactiveOwnerOppIdToUser.get(opp.Previous_Opportunity_Lookup__c).First_Approver_Id__c != 'None' && inactiveOwnerOppIdToUser.get(opp.Previous_Opportunity_Lookup__c).First_Approver_Active__c)
                        {
                            system.debug('First Approver is: ' + inactiveOwnerOppIdToUser.get(opp.Previous_Opportunity_Lookup__c).First_Approver_Id__c);
                            opp.OwnerId = inactiveOwnerOppIdToUser.get(opp.Previous_Opportunity_Lookup__c).First_Approver_Id__c;
                            renewalsToCreate.add(opp);
                        }
                        else if(inactiveOwnerOppIdToUser.get(opp.Previous_Opportunity_Lookup__c).ManagerId != NULL && inactiveOwnerOppIdToUser.get(opp.Previous_Opportunity_Lookup__c).Manager_Active__c)
                        {
                            opp.OwnerId = inactiveOwnerOppIdToUser.get(opp.Previous_Opportunity_Lookup__c).ManagerId;
                            renewalsToCreate.add(opp);
                        }
                        else
                        {
                            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                            mail.setToAddresses(SF_REQUESTS_EMAIL);
                            mail.setCCAddresses(CC_EMAIL);
                            mail.setSubject('RENEWAL NOT CREATED FOR: ' + opp.Name);
                            String body = 'This is an alert regarding the renewal for: ' + opp.Name;
                            body += '\n' + 'No renewal was created because the designated owner of the renewal is an ';
                            body += 'inactive user. Additionally, there is either no manager or first approver, ';
                            body += 'or the manager or first approver on the record is also inactive. Please access ';
                            body += 'the opportunity and account and assess the users involved on the account team.';
                            body += '\n' + 'Once there are no inactive users involved an administrator can access the opportunity and ';
                            body += 'check the ' + '\'' + 'Generate Renewal Opportunity' + '\'' + ' checkbox, which will trigger ';
                            body += 'renewal creation.' + '\n' + 'Opportunity Link: https://ssl.salesforce.com/apex/tabView?id=' + opp.Previous_Opportunity_Lookup__c;
                            mail.setHtmlBody(body);
                            mails.add(mail);
                            noOwnerOpps.add(opp.Previous_Opportunity_Lookup__c);
                        }
                    }
                }
                if(!mails.isEmpty()&& !Test.isRunningTest())
                {
                    Messaging.sendEmail(mails);
                }
            }
        }
        system.debug('What is in the list of renewals? '+ inactiveRenewalOwnerOpps);
        //return inactiveRenewalOwnerOpps;
        return renewalsToCreate;
    }

    public static List<OpportunityLineItem> renewalLineItems(List<OpportunityLineItem> originalOlis)
    {
        List<OpportunityLineItem> renewalOlis = new List<OpportunityLineItem>();
        for(OpportunityLineItem original : originalOlis)
        {
            renewalOlis.add(original.clone(false, true, false, false));
        }
        return renewalOlis;
    }

    public static Boolean ownerIsActive(Opportunity renewalOpp)
    {
        Boolean canBeOwner = [SELECT Id, isActive, First_Approver__c FROM User WHERE Id = :renewalOpp.OwnerId].isActive;
        return canBeOwner; 
    }

    public static Map<Id, User> getUserInfo(List<Opportunity> opportunityList)
    {
        Map<Id, User> oppIdToUser = new Map<Id, User>();
        Set<Id> ownerIds = pluck.Ids('OwnerId', opportunityList);
        system.debug('show owner Ids: ' + ownerIds);
        List<User> inactiveUsers = [SELECT Id, isActive, First_Approver_Id__c, First_Approver_Active__c, ManagerId, Manager_Active__c FROM User WHERE Id IN : ownerIds];
        system.debug('show user list: ' + inactiveUsers);
        Map<Id, User> inactiveUserMap = new Map<Id, User>(inactiveUsers);
        system.debug('Show inactive user map: ' + inactiveUserMap);
        for(Opportunity opp : opportunityList)
        {
            if(inactiveUserMap.containsKey(opp.OwnerId))
            {
                oppIdToUser.put(opp.Previous_Opportunity_Lookup__c, inactiveUserMap.get(opp.OwnerId));
            }
        }
        system.debug('Show opp Id to user map: ' + oppIdToUser);
        return oppIdToUser;
    }

    public static void matchOlisToOpps(List<Opportunity> renewalOpps, List<OpportunityLineItem> renewalOlis, List<Opportunity> originalOpps, List<Opportunity> newOpps)
    {
        Map<Id, Opportunity> previousOppIdToRenewalOpp = new Map<Id, Opportunity>();
        List<Opportunity> originalsToUpdate = new List<Opportunity>();

        for(Opportunity renewalOpp : renewalOpps)
        {
            previousOppIdToRenewalOpp.put(renewalOpp.Previous_Opportunity_Lookup__c, renewalOpp);
        }

        try
        {
            insert previousOppIdToRenewalOpp.values();
            for(Id origId : previousOppIdToRenewalOpp.keySet())
            {
                 Opportunity originalOpp = new Opportunity(Id = origId);
                 originalOpp.Renewal_Created_del__c = true;
                 originalOpp.Generate_Renewal_Opportunity__c = false;
                 originalsToUpdate.add(originalOpp);
            }
            try
            {
                update originalsToUpdate;
            }
            catch (Exception ex)
            {
                ExLog.ExLogConfig config = new ExLog.ExLogConfig();
                ExLog.log(ex, config);
            }
        }
        catch (Exception ex)
        {
            ExLog.ExLogConfig config = new ExLog.ExLogConfig();
            ExLog.log(ex, config);
        }

        for(OpportunityLineItem renewalOli : renewalOlis)
        {
            Date renewalCloseDate;
            Id renewalOppId;
            String previousOppType; 
            system.debug('Show renewal opp Map: ' + previousOppIdToRenewalOpp);
            if(!previousOppIdToRenewalOpp.isEmpty() && previousOppIdToRenewalOpp.containsKey(renewalOli.OpportunityId))
            {
                renewalOppId = previousOppIdToRenewalOpp.get(renewalOli.OpportunityId).Id;
                system.debug('Show renewal opp Id: ' + renewalOppId);
                if(renewalOppId != NULL)
                {
                    renewalCloseDate = previousOppIdToRenewalOpp.get(renewalOli.OpportunityId).CloseDate;
                    previousOppType = renewalOli.Opportunity_Type__c;
                    renewalOli.TotalPrice = null;
                    renewalOli.Previous_Value__c = renewalOli.UnitPrice;
                    if(previousOppType == EXT)
                    {
                        renewalOli.UnitPrice = ((renewalOli.UnitPrice / renewalOli.Start_Date__c.monthsBetween(renewalOli.End_Date__c) + 1) * 12).setScale(2);
                    }
                    if(renewalOli.End_Date__c != NULL)
                    {
                        renewalOli.Start_Date__c = renewalOli.End_Date__c;
                        if(Date.isLeapYear(renewalCloseDate.year()))
                        {
                            renewalOli.End_Date__c = renewalOli.Start_Date__c.addYears(1);
                        }
                        else
                        {
                            renewalOli.End_Date__c = renewalOli.Start_Date__c.addYears(1).addDays(-1);
                        }
                    }
                    else
                    {
                        renewalOli.Start_Date__c = renewalCloseDate;
                        if(Date.isLeapYear(renewalCloseDate.year()))
                        {
                            renewalOli.End_Date__c = renewalCloseDate.addYears(1);
                        }
                        else
                        {
                            renewalOli.End_Date__c = renewalCloseDate.addYears(1).addDays(-1);
                        }
                    }
                    renewalOli.OpportunityId = renewalOppId;
                }
            }
        }
        try
        {
            insert renewalOlis;
        }
        catch (Exception ex)
        {
            ExLog.ExLogConfig config = new ExLog.ExLogConfig();
            ExLog.log(ex, config);
        }
    }

    public static void processOppsAndOlis(List<Opportunity> originalOpps, List<OpportunityLineItem> originalOlis, List<Opportunity> newOpps)
    {
        List<OpportunityLineItem> renewalOlis = renewalLineItems(originalOlis);
        if(!renewalOlis.isEmpty())
        {
            List<Opportunity> renewals = renewalOpportunities(originalOpps);
            system.debug('What is in the problem opp list? ' + noOwnerOpps);
            if(!noOwnerOpps.isEmpty())
            {
                updateNoOwnerOpps(newOpps);
            }
            if(!renewals.isEmpty())
            {
                matchOlisToOpps(renewals, renewalOlis, originalOpps, newOpps);
            }
        }
    }

    public static void updateNoOwnerOpps(List<Opportunity> newOpps)
    {
        system.debug('What is in the problem opp set? ' + noOwnerOpps);
        Map<Id, Opportunity> problemOppsMap = new Map<Id, Opportunity>();
        for(Id oppId : noOwnerOpps)
        {
            String message = '';
            message = ' RENEWAL NOTE FROM SFDC (NOT CREATED): Renewal not generated due to inactive user determined as owner. Check account people team/fields.';
            message += '\n' + ' Once there are no inactive users on the account team, check the ' + '\'' + 'Generate Renewal Opportunity' + '\'' + ' checkbox to create the missing renewal.';
            Opportunity opp = new Opportunity(Id = oppId);
            opp.Description += '\n' + message;
            opp.Generate_Renewal_Opportunity__c = false;
            problemOppsMap.put(opp.Id, opp);
        }
        try
        {
            update problemOppsMap.values();
        }
        catch (Exception ex)
        {
            ExLog.ExLogConfig config = new ExLog.ExLogConfig();
            ExLog.log(ex, config);
        }
    }

    public static void generateRenewalsWithOlis(List<Opportunity> newOpps)
    {
        List<Opportunity> oppsToCreateRenewals = oppsThatNeedRenewals(newOpps);
        if(!oppsToCreateRenewals.isEmpty())
        {
            List<OpportunityLineItem> olisToCreateRenewals = olisForRenewalOpp(oppsToCreateRenewals);
            if(!olisToCreateRenewals.isEmpty())
            {
                processOppsAndOlis(oppsToCreateRenewals, olisToCreateRenewals, newOpps);
            }
        }
    }
}