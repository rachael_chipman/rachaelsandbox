@isTest (SeeAllData=true)
public with sharing class OTMServiceTest {
	
	/*public static List<Opportunity> createOpps(ID accountID, Integer howMany)
	{
		List<Opportunity> opps = new List<Opportunity>();
		
		for(Integer i = 0; i < howMany; i++)
		{
		   Opportunity testOpp = new Opportunity (
		    AccountId = accountID, 
		    Name = 'TestOpp', 
		    CloseDate = system.today()+30, 
		    Type = 'New Business',
		    StageName = 'Demonstrating Capabilities',
		    CurrencyISOCode = 'USD',
		    Approval_Type__c = 'Pricing Approval');
		    
		    opps.add(testOpp);
		}
		
		return opps;
		
	}
	public static testMethod void filterOpportunitiesWithChangedOwners()
	{
		Account a = new Account (Name = 'Test Account');
	    insert a;
	    
	    final Integer TOTAL_OPPS = 50;
		List<Opportunity> oldOpps = createOpps(a.ID,TOTAL_OPPS);
		insert oldOpps;
		
		oldOpps = [Select ID, OwnerId From Opportunity Where ID IN:oldOpps];
		
		User secondUser = [Select ID From User Where IsActive=True And ID!=:oldOpps[0].OwnerID Limit 1];
		
		List<Opportunity> newOpps = oldOpps.deepClone(true);
		Set<ID> oppsWithChangedOwners = new Set<ID>();
		
		for(Integer i = 0; i < newOpps.size(); i+=2)
		{
			newOpps[i].OwnerID = secondUser.ID;
			oppsWithChangedOwners.add(newOpps[i].ID);
		}
		
		Test.startTest();
			List<Opportunity> filteredOpps = OTMCommUpdateService.filterOpportunitiesWithChangedOwners(newOpps, new Map<ID,Opportunity>(oldOpps));
		Test.stopTest();
		
		System.assertEquals(oppsWithChangedOwners.size(), filteredOpps.size(), 'We expect to get back the right number of opportunities that have changed owners' );
		for(Opportunity opp : filteredOpps)
		{
			System.assert(oppsWithChangedOwners.contains(opp.ID),'We expect to only filter those opportunities that have changed owner');
		}
		
	}*/
	
	public static testMethod void OTMCommUpdateServiceTest1()
	{
		Account a = new Account (Name = 'Test Account');
	    insert a;
	    
	    Opportunity testOpp = new Opportunity (
	    AccountId = a.Id, 
	    Name = 'TestOpp', 
	    CloseDate = system.today()+30, 
	    Type = 'New Business',
	    StageName = 'Defining Needs',
	    CurrencyISOCode = 'USD',
	    Cloned__c = false, 
	    Approval_Type__c = 'Pricing Approval');
	    insert testOpp;
	    
	    OpportunityTeamMember otm = new OpportunityTeamMember(
	    OpportunityId = testOpp.Id,
	    UserId = '00500000007Em7P',
	    Percent_Commissioned_On__c = 15.00,
	    TeamMemberRole = 'Sales'
	    );
	    insert otm;
	    
	    User secondUser = [Select ID From User Where IsActive=True And Id!= :testOpp.OwnerId Limit 1];
	    
	    test.startTest();
	    testOpp.OwnerId = secondUser.Id;
	    update testOpp;
	    test.stopTest();
	    
	    //Query the testOpp
	    List<Opportunity> testOpps = [SELECT Id, OwnerId FROM Opportunity WHERE Id = :testOpp.Id];
	    system.AssertEquals(secondUser.Id, testOpps[0].OwnerId, 'The owner should be changed');
	    
	    list<OpportunityTeamMember> otms = [SELECT Id, OpportunityId, Percent_Commissioned_On__c FROM OpportunityTeamMember WHERE OpportunityId = :testOpp.Id];
	    system.AssertEquals(100, otms[0].Percent_Commissioned_On__c, 'Percent Commissioned On should be 100%');
	}
	
	public static testMethod void OTMCommInsertServiceTest()
	{
		Account a = new Account (Name = 'Test Account');
	    insert a;
	    
	    Opportunity testOpp = new Opportunity (
	    OwnerId = [SELECT Id, ProfileId, isActive FROM User WHERE ProfileId = '00e000000073QIq' and isActive = true LIMIT 1].Id,
	    AccountId = a.Id, 
	    Name = 'TestOpp', 
	    CloseDate = system.today()+30, 
	    Type = 'New Business',
	    StageName = 'Defining Needs',
	    CurrencyISOCode = 'USD',
	    Cloned__c = false, 
	    Approval_Type__c = 'Pricing Approval');
	    
	    
	    test.startTest();
	    insert testOpp;
	    test.stopTest();
	    
	    //Query the testOpp	    
	    list<OpportunityTeamMember> otms = [SELECT Id, OpportunityId, Percent_Commissioned_On__c FROM OpportunityTeamMember WHERE OpportunityId = :testOpp.Id];
	    system.AssertEquals(100, otms[0].Percent_Commissioned_On__c, 'Percent Commissioned On should be 100%');
	}
}