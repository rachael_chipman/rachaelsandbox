@isTest (SeeAllData=true)
public with sharing class createTermOppFromCaseTest {
	
	public static Case aCase;
	public static User INTERNAL_TOOLS
	{
		get
		{
			if( INTERNAL_TOOLS == NULL )
			{
				INTERNAL_TOOLS = [SELECT Id FROM User WHERE Name = 'Internal Tools Automation' and isActive = true];
			}
			return INTERNAL_TOOLS;
		}
		private set;
	}
	
	public static void setUpCase()
	{
		Account account = new Account (Name='Test Account');
		insert account;
		Contact contact = new Contact (AccountId = account.Id, FirstName = 'Test', LastName = 'Contact', Title='Contact');
		insert contact;
		
		aCase = new Case();
		aCase.AccountId = account.Id;
		aCase.ContactId = contact.Id;
		aCase.RecordTypeId = createTermOppFromCase.TERMINATION.Id;
		aCase.SuppliedName = 'Test Case';
		aCase.Subject = 'Termination-Voluntary';
	}
	
	public static testMethod void testCaseinsertbulk()
	{
		Account account = new Account (Name='Test Account');
		insert account;
		Contact contact = new Contact (AccountId = account.Id, FirstName = 'Test', LastName = 'Contact', Title='Contact');
		insert contact;
		
		User user = [SELECT Id FROM User WHERE isActive = true LIMIT 1];
		
		List<Case> cases = TestUtils.createCases(100,false);
		for(Case c : cases)
		{
			c.AccountId = account.Id;
			c.ContactId = contact.Id;
			c.RecordTypeId = createTermOppFromCase.TERMINATION.Id;
			c.SuppliedName = 'Test Case';
			c.Subject = 'Termination-Voluntary';
			c.CurrencyISOCode = 'USD';
			c.OwnerId = user.Id;
			c.Terminated_Annual_Value__c = 500.00;
		}
		
		system.runAs(INTERNAL_TOOLS)
		{
			test.startTest();
			insert cases;
			test.stopTest();
		}
		
		List<Opportunity> termOpps = [SELECT Id, Termination_Case__c, AccountId, StageName, CloseDate, RecordTypeId, Name, Type, CurrencyISOCode, Pricebook2Id, OwnerId, Detailed_Termination_Reasons__c, Termination_Timing__c, Product__c, Reasons_for_Lost_Opportunities__c, Lost_Pass_Details__c FROM Opportunity WHERE Termination_Case__c IN: cases];
		System.assertEquals(cases.size(),termOpps.size(), 'The cases and opps lists should be the same in size');
		List<OpportunityLineItem> termOlis = [SELECT Id, OpportunityId, PricebookEntryId, UnitPrice, Quantity, Previous_Value__c, Description__c FROM OpportunityLineItem WHERE OpportunityId IN : termOpps];
		system.assertEquals(termOlis.size(), termOpps.size(), 'We should have the same number of olis as opps');

		Set<ID> expectedCaseIds = new Set<ID>();
		Set<ID> actualCaseIDs = new Set<ID>();
		Set<Id> actualOppIds = new Set<Id>();
		Set<Id> expectedOppIds = new Set<Id>();
		
		Map<Id, Opportunity> caseIdToOpportunity = new Map<Id, Opportunity>();
		Map<Id, OpportunityLineItem> oppIdToOpportunityLineItem = new Map<Id, OpportunityLineItem>();

		for(Opportunity opp : termOpps)
		{
			caseIdToOpportunity.put(opp.Termination_Case__c,opp);
			
		}
		for(OpportunityLineItem oli : termOlis)
		{
			oppIdToOpportunityLineItem.put(oli.OpportunityId, oli);
		}
		for(Case c : cases)
		{
			expectedCaseIds.add(c.ID);
			System.assert(caseIdToOpportunity.containsKey(c.ID),'Each case Id should match 1:1 with the Termination Case Ids on the Opps');
			Opportunity oppty = caseIdToOpportunity.get(c.ID);
			/* oppty asserttion*/
			system.assertEquals(oppty.AccountId, c.AccountId, 'We expect the case and the opp to have the same account');
			system.assertEquals(oppty.StageName, 'Approval Required', 'We expect the opp stage to be Approval Required');
			system.assertEquals(oppty.RecordTypeId, createTermOppFromCase.OPPTERMINATION.Id, 'We expect the opp record type to be Termination');
			system.assertEquals(oppty.CloseDate, system.today(), 'We expect the opp close date to be today');
			system.assertEquals(oppty.Type, 'Renewal', 'We expect the opp type to be Renewal');
			system.assertEquals(oppty.CurrencyISOCode, c.CurrencyIsoCode, 'We expect the opp currency and the case currency to tbe the same');
			system.assertEquals(oppty.Pricebook2Id, createTermOppFromCase.RPPRICEBOOK.Id, 'We expect the opp pricebook to be the RP pricebook');
			system.assertEquals(oppty.Detailed_Termination_Reasons__c, c.Cancellation_Reasons__c, 'Termination Reasons should match Cancellation Reasons from case');
			system.assertEquals(oppty.Termination_Timing__c, c.Termination_Timing__c, 'We expect the termination timing to match from the case to the opp');
			system.assertEquals(oppty.Product__c, 'Tools Child', 'We expect the value in the products field to be tools child');
			system.assertEquals(oppty.Reasons_for_Lost_Opportunities__c, c.Type_Sub_Category__c, 'We expect the Reasons for Lost to be equal to the case subType');
			system.assertEquals(oppty.Lost_Pass_Details__c, c.Reasons_for_Cancellation__c, 'We expect the Lost Pass Details on the opp to match the reasons for cancellation on the case');
			System.assert(oppIdToOpportunityLineItem.containsKey(oppty.ID), 'Each oli opportunity Id should match 1:1 with the Termination opp Ids');
			OpportunityLineItem oli = oppIdToOpportunityLineItem.get(oppty.ID);
			/* oli assertions*/
			system.assertEquals(oli.UnitPrice, 0.00, 'We expect the price to be 0.00');
			system.assertEquals(oli.Quantity, 1, 'We expect the quantity to be 1');
			system.assertEquals(oli.Description__c, 'Product Deactivation', 'We expect the description to be Product Deactivation');
			system.assertEquals(oli.Previous_Value__c, c.Terminated_Annual_Value__c, 'We expect the previous value to match the case terminated value');
		}
	}
	
	public static testMethod void testCaseInsert()
	{
		setUpCase();
		
		system.runAs(INTERNAL_TOOLS)
		{
			test.startTest();
			insert aCase;
			test.stopTest();
		}

		List<Opportunity> termOpps = [SELECT Id, Termination_Case__c, RecordTypeId FROM Opportunity WHERE Termination_Case__c = :aCase.Id];
		system.assertEquals(aCase.Id, termOpps[0].Termination_Case__c, 'The opportunity should be inserted and have the correct case Id');
		system.assertEquals(createTermOppFromCase.OPPTERMINATION.Id, termOpps[0].RecordTypeId, 'The record type should be Termination');
	}

}