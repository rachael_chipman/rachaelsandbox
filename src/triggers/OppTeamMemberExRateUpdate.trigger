trigger OppTeamMemberExRateUpdate on Opportunity (After Insert, After Update) {

for(Opportunity o : Trigger.New){

if(o.StageName == 'Finance Approved' || o.StageName == 'Pending Certification Qualification' || o.StageName == 'Terminated Contract' || trigger.isInsert && test.isRunningTest() != true){

//String oppid=o.Id;
//if(test.isRunningTest() != true){
//id batchinstanceid = database.executeBatch(new OppTeamExRateBatch('select Id, Exchange_Rate__c, CurrencyIsoCode from OpportunityTeamMember where OpportunityId = \'' + o.Id + '\'', oppid));
//}

List<OpportunityTeamMember> otm = new List<OpportunityTeamMember>();

DatedConversionRate dcr = [select ConversionRate from DatedConversionRate where StartDate <= :o.closeDate and NextStartDate > :o.closeDate and IsoCode = :o.CurrencyIsoCode limit 1];

CurrencyType ct = [select ConversionRate from CurrencyType where IsoCode = :o.CurrencyIsoCode limit 1];
    
    for (OpportunityTeamMember otms: [select Id, Exchange_Rate__c, CurrencyIsoCode from OpportunityTeamMember where OpportunityId = :o.Id] ) {
    otms.Exchange_Rate__c = dcr.ConversionRate;
    otms.Company_Exchange_Rate__c = ct.ConversionRate;
    otms.CurrencyIsoCode = 'USD';
    otm.add(otms);
    }
   

    update otm;

}
}}