@isTest(seeallData=true)

public class SubmitOpptoFinance_Tests{

static testMethod void submitOpptoFinanceTest() {
    
     Account acct = new Account();
        acct.name = 'RPTestAcct1';
        acct.accountmanager__c = 'BIO Automation';
        Insert acct;
    
    Campaign certCampaign = new Campaign(Name = 'Cert Campaign', isActive = true);
    insert certCampaign;
        
     Opportunity op = new Opportunity (
            Name = 'Naming Convention WFR',
            AccountId = acct.Id,
            RecordTypeId = TestUtils.PURCHASE_RECORD_TYPE.Id,
            StageName = 'Signature',
            CloseDate = system.Today(),
            Type = 'New Business',
            OwnerId = '00500000007286zAAA',
            CampaignId = certCampaign.Id,
            Product__c = 'Certification',
            SSC_Volume__c = 'Tier 4',
            Pricebook2Id = '01s000000004NS6AAM',
            Certification_Type__c = 'Certification',
            Did_not_Fill_Defining_Need_Exit_Criteria__c = true,
       		Did_not_Fill_Pre_Pipeline_Exit_Criteria__c = true,
       		Did_not_Fill_Est_Value_Exit_Criteria__c = true,
       		Did_not_Fill_Nego_Terms_Exit_Criteria__c = true,
       		Did_not_Fill_Verbal_Commit_Exit_Criteria__c = true );
            
            insert op;
            
           
            
        OpportunityLineItem opl = new OpportunityLineItem(
            OpportunityId = op.Id,
            Quantity = 1,
            UnitPrice = 2500,
            Product_Status__c = 'Current',
            Description__c = 'Return Path Certification',
            Active__c = true,
            Recurring__c = false,
            PricebookentryId = '01u00000000FGwSAAW');
            
            insert opl;      
    
     
     Certification_Application__c ca = new Certification_Application__c();
         ca.Application_Status__c = 'Application Submitted';
         ca.Account__c = acct.Id;
         ca.License_Fee_Opportunity__c = op.Id;
         ca.Certification_App_Fee__c = 123456;
         ca.Certification_Volume__c = 'Tier 4';
         insert ca;
         
             ca.Application_Status__c = 'Under Review';
             ca.Application_Result__c = 'Investigating';
             update ca;
         }
         
}