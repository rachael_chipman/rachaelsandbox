@isTest

public with sharing class MarkasTerminationControllerTest {


static Opportunity testOpp;
static OpportunityLineItem testOppoli;

static void setupOppandOli()
{
    
  c2g__codaGeneralLedgerAccount__c GenLedge = new c2g__codaGeneralLedgerAccount__c (Name = 'Test', c2g__ReportingCode__c = '1234', c2g__Type__c = 'Balance Sheet');
  c2g__codaGeneralLedgerAccount__c GenLedge2 = new c2g__codaGeneralLedgerAccount__c (Name = 'Test', c2g__ReportingCode__c = '1350', c2g__Type__c = 'Balance Sheet');
  c2g__codaGeneralLedgerAccount__c GenLedge3 = new c2g__codaGeneralLedgerAccount__c (Name = 'Test', c2g__ReportingCode__c = '5000', c2g__Type__c = 'Balance Sheet');
  insert new List<c2g__codaGeneralLedgerAccount__c>{GenLedge, GenLedge2, GenLedge3};

  c2g__codaIncomeScheduleDefinition__c incsched = new c2g__codaIncomeScheduleDefinition__c (Name = 'Income Schedule', c2g__GeneralLedgerAccount__c = GenLedge.Id);
  insert incsched;
  
  c2g__codaCompany__c financeAccount = new c2g__codaCompany__c( c2g__TaxIdentificationNumber__c = '06-1566593', Name = 'TestAccount' );
  insert financeAccount;
    
    Account a = new Account (Name = 'Test Account');
    insert a;
    
    testOpp = new Opportunity (
    AccountId = a.Id, 
    Name = 'TestOpp', 
    CloseDate = system.today()+30, 
    Type = 'New Business',
    StageName = 'Defining Needs',
    Did_not_Fill_Pre_Pipeline_Exit_Criteria__c = true,
    Pricebook2Id = '01s000000004NS6AAM',
    CurrencyISOCode = 'USD',
    Approval_Type__c = 'Not Applicable',
    Generated_From_Free_Trial__c = true);

    
    testOppoli = new OpportunityLineItem (
    PricebookEntryId = '01u00000000FeWfAAK',
    Product_Status__c = 'Current',
    Start_Date__c = system.today() +30,
    End_Date__c = system.today() +394,
    UnitPrice = 1500.00,
    Previous_Value__c = 0.00,
    Quantity = 1);
    
    
}
    static testMethod void  MarkasTerminationControllertestRenewal()
    {
        setupOppandOli();
        testOpp.Type = 'Renewal';
        testOpp.Reasons_for_Lost_Opportunities__c = 'RP Initiated';
        testOpp.Lost_Pass_Details__c = 'Test Reason';
        testOpp.Lost_Pass_Specifics__c = 'Test Specifics';
        testOpp.Product_Deactivation_Date__c = system.today();
        testOpp.Outstanding_Billing_Directive__c = 'Other';
        insert testOpp;
        testOppoli.OpportunityId = testOpp.Id;
        insert testOppoli;
        
        ApexPages.StandardController sc = new ApexPages.standardController(testOpp);
        System.currentPagereference().getParameters().put('id',testOpp.Id);
        MarkasTerminationController methods = new MarkasTerminationController(sc); 
        
        test.startTest();
        methods.CheckOpp();
        methods.CheckReason();
        methods.UpdateObjects(); 
        test.stopTest();
        
        list<Opportunity> testOpps = [SELECT Id, Approval_Type__c, RecordTypeId, StageName FROM Opportunity WHERE Id = :testOpp.Id];
        system.assertEquals('Approval Required', testopps[0].StageName, 'Stage should be updated to Approval Required');
        system.assertEquals('Finance Approval', testopps[0].Approval_Type__c, 'Approval Type should be updated to Finance Approval');
        system.assertEquals(TestUtils.TERMINATION_RECORD_TYPE.Id, testopps[0].RecordTypeId, 'Record Type should be updated to Termination');
        
        list<OpportunityLineItem> testOlis = [SELECT Id, Start_Date__c, End_Date__c, Product_Status__c, UnitPrice, Previous_Value__c FROM OpportunityLineItem WHERE Id = :testOppoli.Id];
        system.assertEquals('Terminated', testOlis[0].Product_Status__c, 'Product Status should be Terminated');
        system.assertEquals(0.00, testOlis[0].UnitPrice, 'Unit Price shoudl be 0.00'); 

         
    }
    
    static testMethod void  MarkasTerminationControllertestExtension()
    {
        setupOppandOli();
        testOpp.Type = 'Extension';
        testOpp.Reasons_for_Lost_Opportunities__c = 'RP Initiated';
        testOpp.Lost_Pass_Details__c = 'Test Reason';
        testOpp.Lost_Pass_Specifics__c = 'Test Specifics';
        testOpp.Product_Deactivation_Date__c = system.today();
        testOpp.Outstanding_Billing_Directive__c = 'Other';
        insert testOpp;
        testOppoli.OpportunityId = testOpp.Id;
        testOppoli.Start_Date__c = system.today() - 90;
        testOppoli.End_Date__c = system.today();
        insert testOppoli;
        
        ApexPages.StandardController sc = new ApexPages.standardController(testOpp);
        System.currentPagereference().getParameters().put('id',testOpp.Id);
        MarkasTerminationController methods = new MarkasTerminationController(sc); 
        
        test.startTest();
        methods.CheckOpp();
        methods.CheckReason();
        methods.UpdateObjects(); 
        test.stopTest();
        
        list<Opportunity> testOpps = [SELECT Id, Approval_Type__c, RecordTypeId, StageName FROM Opportunity WHERE Id = :testOpp.Id];
        system.assertEquals('Approval Required', testopps[0].StageName, 'Stage should be updated to Approval Required');
        system.assertEquals('Finance Approval', testopps[0].Approval_Type__c, 'Approval Type should be updated to Finance Approval');
        system.assertEquals(TestUtils.TERMINATION_RECORD_TYPE.Id, testopps[0].RecordTypeId, 'Record Type should be updated to Termination');
        
        list<OpportunityLineItem> testOlis = [SELECT Id, Start_Date__c, End_Date__c, Product_Status__c, UnitPrice, Previous_Value__c FROM OpportunityLineItem WHERE Id = :testOppoli.Id];
        system.assertEquals('Terminated', testOlis[0].Product_Status__c, 'Product Status should be Terminated');
        system.assertEquals(0.00, testOlis[0].UnitPrice, 'Unit Price shoudl be 0.00');
       // system.assertEquals(6000.00, testOlis[0].Previous_Value__c, 'Previous Value should be 6000.00');  
    }
    

    
    static testMethod void  MarkasTerminationControllertestNotRenewal()
    {
        setupOppandOli();
        insert testOpp;
        testOppoli.OpportunityId = testOpp.Id;
        insert testOppoli;
        
        ApexPages.StandardController sc = new ApexPages.standardController(testOpp);
        System.currentPagereference().getParameters().put('id',testOpp.Id);
        MarkasTerminationController methods = new MarkasTerminationController(sc); 
        
        test.startTest();
        methods.CheckOpp();
        methods.GoBack();
        methods.Cancel();
        test.stopTest();
        
        list<Opportunity> testOpps = [SELECT Id, Approval_Type__c, RecordTypeId, StageName FROM Opportunity WHERE Id = :testOpp.Id];
        system.assertEquals('Defining Needs', testopps[0].StageName, 'Stage should not be changed');
        //system.assertEquals(TestUtils.CONSIDERATION_RECORD_TYPE.Id, testopps[0].RecordTypeId, 'Record Type should be updated to Demonstrating Capabilities');
        system.assertEquals(TestUtils.SELECT_RECORD_TYPE.Id, testopps[0].RecordTypeId, 'Record Type should be updated to Demonstrating Capabilities');
    }
}