@isTest
private class TestBatchToCreateJournals
{
	static final String OPP_CURRENCY = 'USD';

	@isTest (seeAllData=true)
	static void testBatch_SameCurrency()
	{
		final Decimal UNIT_PRICE = 120.00;
		Date startDate = Date.newInstance( 2014, 01, 05 );
		Date endDate = Date.newInstance( 2014, 05, 15 );
		
		JournalCreationServiceTest.setupOppAndOLI( UNIT_PRICE, OPP_CURRENCY, OPP_CURRENCY, startDate );
		JournalCreationServiceTest.setupInvoice( 1, startDate, endDate);
		
		Integer expectedNoOfJournals = 6;
		List<Date> expectedMonthlyJournalDates = new List<Date>{ Date.newInstance( 2014, 01, 05 ), Date.newInstance( 2014, 02, 01 ), Date.newInstance( 2014, 03, 01 ), Date.newInstance( 2014, 04, 01 ), Date.newInstance( 2014, 05, 01 ) };

		Decimal priceperday = UNIT_PRICE/( startDate.daysBetween( endDate ) + 1);
		Decimal expectedPrice_1 = ( ( 31 - 5 + 1 ) * priceperday ).setScale( 2 );
		Decimal expectedPrice_2 = ( 28 * priceperday ).setScale( 2 ); 
		Decimal expectedPrice_3 = ( 31 * priceperday ).setScale( 2 );
		Decimal expectedPrice_4 = ( 30 * priceperday ).setScale( 2 );
		Decimal expectedPrice_5 = ( 15 * priceperday ).setScale( 2 );
		List<Decimal> expectedPrices = new List<Decimal>{ expectedPrice_1, expectedPrice_2, expectedPrice_3, expectedPrice_4, expectedPrice_5 };
		List<Decimal> expectedPrices_negated = new List<Decimal>{ expectedPrice_1 * ( -1 ), expectedPrice_2 * ( -1 ), expectedPrice_3 * ( -1 ), expectedPrice_4 * ( -1 ), expectedPrice_5 * ( -1 ) };

		test.startTest();
			Database.executeBatch( new BatchToCreateJournals() );
		test.stopTest();

		List<c2g__codaJournal__c> actualJournals = [ SELECT Id, c2g__Type__c, Sales_Invoice__c, c2g__JournalDate__c, c2g__JournalCurrency__c, Opportunity__c, ffgl__DeriveCurrency__c, 
													( SELECT Id, c2g__LineType__c, c2g__GeneralLedgerAccount__c, c2g__Value__c FROM c2g__JournalLineItems__r )
													FROM c2g__codaJournal__c WHERE Opportunity__c = :JournalCreationServiceTest.testOpportunity.Id ORDER BY c2g__JournalDate__c ];
		
		System.assertEquals( expectedNoOfJournals, actualJournals.size(), 'All the expected number of journals should be returned' );
		
		Set<Decimal> actualPrices = new Set<Decimal>();
		for( c2g__codaJournal__c aJournal : actualJournals )
		{
			actualPrices.addAll( Pluck.decimals( 'c2g__Value__c', aJournal.c2g__JournalLineItems__r ) );
		}

		Set<Decimal> expectedPricesSet = new Set<Decimal>();
		expectedPricesSet.addAll(expectedPrices);
		expectedPricesSet.addAll(expectedPrices_negated);
		expectedPricesSet.add( UNIT_PRICE );
		expectedPricesSet.add( UNIT_PRICE * (-1) );
		
		System.assertEquals( expectedPrices.size() + expectedPrices_negated.size() + 2, actualPrices.size() );
		System.assert( expectedPricesSet.containsAll( actualPrices ), 'There should be a journal line item for each of the expected Prices' );
		System.assert( actualPrices.contains( UNIT_PRICE ), 'There should be atleast one full journal with a journal line item with full unit price. Unit Prices : ' + actualPrices );
		System.assert( actualPrices.containsAll( expectedPrices ), 'There should be a journal line item for each of the expected Prices' );
		System.assert( actualPrices.containsAll( expectedPrices_negated ), 'There should be a journal line item for each of the negated expected Prices' );

		List<c2g__codaJournal__c> monthlyJournals = new List<c2g__codaJournal__c>();
		for( c2g__codaJournal__c aJournal : actualJournals )
		{
			System.assertEquals( JournalCreationService.MANUAL_JOURNAL, aJournal.c2g__Type__c, 'All the journals created should be of type manual' );
			System.assertEquals( JournalCreationServiceTest.testSalesInvoices[0].Id, aJournal.Sales_Invoice__c, 'All the journals created should have sales invoice populated with test sales invoice' );
			System.assertEquals( TestUtils.ACCOUNTING_CURRENCY_USD.Id, aJournal.c2g__JournalCurrency__c, 'All the journals created should have the right account currency' );
			System.assertEquals( JournalCreationServiceTest.testOpportunity.Id, aJournal.Opportunity__c, 'All the journals created should have opportunity lookup populated with invoice\'s opportunity' );
			System.assertEquals( false, aJournal.ffgl__DeriveCurrency__c, 'All the journals created should have derive currency checbox set to false' );

			System.assertEquals( 2, aJournal.c2g__JournalLineItems__r.size(), 'There should be 2 journal line items created for each journal' );
			if( !isFullJournal( aJournal, UNIT_PRICE ) )
			{
				monthlyJournals.add( aJournal );
			}
			
			for( c2g__codaJournalLineItem__c aJLI : aJournal.c2g__JournalLineItems__r )
			{
				System.assertEquals( JournalCreationService.LINE_TYPE_GENERAL_LEDGER, aJLI.c2g__LineType__c, 'All the journals created should be of type manual' );
				//System.assertEquals( UNIT_PRICE, actualJournals[0].c2g__JournalDate__c, 'The first journal should be the full journal with the invocie contract start Date' );
				if( isFullJournal( aJournal, UNIT_PRICE ) )
				{
					System.assert( JournalCreationService.COMMISSIONS_PAYABLE_REFERRAL_GLA.Id == aJLI.c2g__GeneralLedgerAccount__c ||
									JournalCreationService.DEFERRED_COMMISSION_GLA.Id == aJLI.c2g__GeneralLedgerAccount__c, 'All the full journal line items created should have COMMISSIONS_PAYABLE_REFERRAL General ledger or DEFERRED_COMMISSION General Ledger' );

					if( JournalCreationService.COMMISSIONS_PAYABLE_REFERRAL_GLA.Id == aJLI.c2g__GeneralLedgerAccount__c  )
					{
						System.assertEquals( UNIT_PRICE*(-1), aJLI.c2g__Value__c, 'All the full journal line items for COMMISSIONS_PAYABLE_REFERRAL general ledger should have negated amount ' );
					}
					else
					{
						System.assertEquals( UNIT_PRICE, aJLI.c2g__Value__c, 'All the journal line items created should have the right unit price ' );
					}
				}
				else //Is a monthly journal
				{
					System.assert( JournalCreationService.DEFERRED_COMMISSION_GLA.Id == aJLI.c2g__GeneralLedgerAccount__c ||
									JournalCreationService.COMMISSIONS_PARTNERS_GLA.Id == aJLI.c2g__GeneralLedgerAccount__c, 'All the journal line items created should ' );
				}
			}

		}
		System.assertEquals( Date.today().toStartOfMonth(), actualJournals[0].c2g__JournalDate__c, 'The first journal should be the full journal with the invocie contract start Date' );
		
		System.assertEquals( expectedMonthlyJournalDates.size(), monthlyJournals.size(), 'All the expected number of monthly journals should be returned' );
		for( Integer i = 0; i < expectedMonthlyJournalDates.size(); i++ )
		{
			System.assertEquals( Date.today().toStartofMonth(), monthlyJournals[i].c2g__JournalDate__c, 'There should be a monthly journal created with each expected Date' );
			for( c2g__codaJournalLineItem__c aJLI : monthlyJournals[i].c2g__JournalLineItems__r )
			{
				System.assert( aJLI.c2g__Value__c == expectedPrices[i] || aJLI.c2g__Value__c == expectedPrices_negated[i], 'All the monthly journal line items should have the expected price depending upon the period' );
			}
		}
	}

	@isTest (seeAllData=true)
	static void testBatch_DifferentCurrency()
	{
		final Decimal UNIT_PRICE = 120.00;
		final String PARTNER_CURRENCY = 'EUR';

		Date startDate = Date.newInstance( 2014, 01, 05 );
		Date endDate = Date.newInstance( 2014, 05, 15 );
		
		c2g__codaExchangeRate__c expectedRate_EUR = [SELECT Id, c2g__Rate__c, c2g__Startdate__c FROM c2g__codaExchangeRate__c WHERE c2g__ExchangeRateCurrency__r.Name = :PARTNER_CURRENCY 
														AND c2g__ExchangeRateCurrency__r.c2g__OwnerCompany__c = :OpportunityServices.RETURN_PATH_COMPANY.Id 
														AND c2g__Startdate__c <= TODAY ORDER BY c2g__Startdate__c desc LIMIT 1][0];

		c2g__codaAccountingCurrency__c partnerAccountingCurrency = [SELECT Id FROM c2g__codaAccountingCurrency__c WHERE Name = :PARTNER_CURRENCY LIMIT 1];

		//Set different currencies on opportunity and partner
		JournalCreationServiceTest.setupOppAndOLI( UNIT_PRICE, OPP_CURRENCY, PARTNER_CURRENCY, startDate );
		
		JournalCreationServiceTest.setupInvoice( 1, startDate, endDate);
		for( c2g__codaInvoice__c aTestInvoice : JournalCreationServiceTest.testSalesInvoices )
		{
			aTestInvoice.c2g__InvoiceCurrency__c = partnerAccountingCurrency.Id;
			aTestInvoice.CurrencyISOCode = PARTNER_CURRENCY;
			aTestInvoice.Partner_Exchange_Rate__c = expectedRate_EUR.c2g__Rate__c;
		}
		update JournalCreationServiceTest.testSalesInvoices;

		Integer expectedNoOfJournals = 6;
		List<Date> expectedMonthlyJournalDates = new List<Date>{ Date.newInstance( 2014, 01, 05 ), Date.newInstance( 2014, 02, 01 ), Date.newInstance( 2014, 03, 01 ), Date.newInstance( 2014, 04, 01 ), Date.newInstance( 2014, 05, 01 ) };

		Decimal convertedUnitPrice = ( UNIT_PRICE/1.0 ) * expectedRate_EUR.c2g__Rate__c;

		Decimal priceperday = convertedUnitPrice/( startDate.daysBetween( endDate ) + 1);
		Decimal expectedPrice_1 = ( ( 31 - 5 + 1 ) * priceperday ).setScale( 2 );
		Decimal expectedPrice_2 = ( 28 * priceperday ).setScale( 2 ); 
		Decimal expectedPrice_3 = ( 31 * priceperday ).setScale( 2 );
		Decimal expectedPrice_4 = ( 30 * priceperday ).setScale( 2 );
		Decimal expectedPrice_5 = ( 15 * priceperday ).setScale( 2 );
		List<Decimal> expectedPrices = new List<Decimal>{ expectedPrice_1, expectedPrice_2, expectedPrice_3, expectedPrice_4, expectedPrice_5 };
		List<Decimal> expectedPrices_negated = new List<Decimal>{ expectedPrice_1 * ( -1 ), expectedPrice_2 * ( -1 ), expectedPrice_3 * ( -1 ), expectedPrice_4 * ( -1 ), expectedPrice_5 * ( -1 ) };

		test.startTest();
			Database.executeBatch( new BatchToCreateJournals() );
		test.stopTest();

		List<c2g__codaJournal__c> actualJournals = [ SELECT Id, c2g__Type__c, Sales_Invoice__c, c2g__JournalDate__c, c2g__JournalCurrency__c, Opportunity__c, ffgl__DeriveCurrency__c,
													( SELECT Id, c2g__LineType__c, c2g__GeneralLedgerAccount__c, c2g__Value__c FROM c2g__JournalLineItems__r )
													FROM c2g__codaJournal__c WHERE Opportunity__c = :JournalCreationServiceTest.testOpportunity.Id ORDER BY c2g__JournalDate__c ];
		
		System.assertEquals( expectedNoOfJournals, actualJournals.size(), 'All the expected number of journals should be returned' );
		
		Set<Decimal> actualPrices = new Set<Decimal>();
		Set<Decimal> testSet2 = new Set<Decimal>();

		for( c2g__codaJournal__c aJournal : actualJournals )
		{
			System.assertEquals( partnerAccountingCurrency.Id, aJournal.c2g__JournalCurrency__c, 'All the journals created should have the right account currency' );
			actualPrices.addAll( Pluck.decimals( 'c2g__Value__c', aJournal.c2g__JournalLineItems__r ) );
		}
		System.assertEquals( expectedPrices.size() + expectedPrices_negated.size() + 2, actualPrices.size() );
		
		Set<Decimal> expectedPricesSet = new Set<Decimal>();
		expectedPricesSet.addAll(expectedPrices);
		expectedPricesSet.addAll(expectedPrices_negated);
		expectedPricesSet.add( convertedUnitPrice.setScale(2) );
		expectedPricesSet.add( convertedUnitPrice.setScale(2) * (-1) );

		System.assert( actualPrices.contains( convertedUnitPrice.setScale(2) ), 'There should be atleast one full journal with a journal line item with full unit price. ' + convertedUnitPrice + ' Unit Prices : ' + actualPrices );
		System.assert( expectedPricesSet.containsAll( actualPrices ), 'There should be a journal line item for each of the expected Prices' );
		System.assert( actualPrices.containsAll( expectedPrices ), 'There should be a journal line item for each of the expected Prices' + expectedPrices + ' Unit Prices : ' + actualPrices);
		System.assert( actualPrices.containsAll( expectedPrices_negated ), 'There should be a journal line item for each of the negated expected Prices' );
	}

	static Boolean isFullJournal( c2g__codaJournal__c journal, Decimal unitPrice )
	{
		c2g__codaJournalLineItem__c aJLI = journal.c2g__JournalLineItems__r[0];
		return ( aJLI.c2g__Value__c == unitPrice || aJLI.c2g__Value__c == ( unitPrice * ( -1 ) ) );
	}
	
	
}