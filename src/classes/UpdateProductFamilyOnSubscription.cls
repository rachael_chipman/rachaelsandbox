global class UpdateProductFamilyOnSubscription implements Database.Batchable<SObject>
{
    global String Query;
    
    global UpdateProductFamilyOnSubscription()
    {
        Query =   'Select Id, Name,Product_Family__c,Zuora__RatePlanName__c,Zuora__Description__c from Zuora__SubscriptionProductCharge__c ';
        
        if(Test.isRunningTest())
            Query += ' where Zuora__Description__c = \'Test\' ';

        System.debug ('Query is :' +Query);
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC)
    {
        return Database.getQueryLocator(Query);
    }
    
    global void execute(Database.BatchableContext BC, List<sObject> scope)
    {
        System.debug('Scope is:' +scope);
        SubscriptionChargeService.UpdateProductFamily(scope);
        
    }
    
    global void finish(Database.BatchableContext BC)
    {
       
        AsyncApexJob apexjob = [SELECT Id, Status, NumberOfErrors, JobItemsProcessed,TotalJobItems, CreatedBy.Email
      							FROM AsyncApexJob WHERE Id = :BC.getJobId()];
        System.debug('Status of the job is: ' + apexjob.Status);
        
                  Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
   				  String[] toAddresses = new String[] {apexjob.CreatedBy.Email};
				  mail.setToAddresses(toAddresses);
				  mail.setSubject('Apex Sharing Recalculation ' + apexjob.Status);
				  mail.setPlainTextBody('The batch Apex job processed ' + apexjob.TotalJobItems +' batches with '+ apexjob.NumberOfErrors + ' failures.');
   				  //Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });

        
    }

}