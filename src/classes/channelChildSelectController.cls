public class channelChildSelectController {

    static FINAL String codeCoverage = 'Should have one line';
    /*public String acctId {get;set;}
    public String directoppId {get;set;}
    public String toSelect {get;set;}
    public String target {get;set;}
    public List<Opportunity> chanOpps {get;set;}
    public List<Certification_Application__c> apps {get;set;}

    public channelChildSelectController() {
        if(acctId == null) acctId = System.currentPageReference().getParameters().get('acctId');
        if(directoppId == null) directoppId = System.currentPageReference().getParameters().get('directoppId');
        if(target == null) target = System.currentPageReference().getParameters().get('target');
        
        system.debug('working with partner account: ' + acctId + ' and directOpp ' + directOppId + ' and looking for ' + target);
    }

    public List<Opportunity> getOpps() {
        system.debug('finding opps');

        string realmid;
        account a = [select id, (select name from realmids__r) from account where id = :acctId limit 1];

        for(realmid__c rid : a.realmids__r) {
            realmid = rid.name;
        }

        if(chanOpps == null) chanOpps = new List<Opportunity>();

        if(realmId == null) {
            // do nothing
        } else {
            for(opportunity o : [select id, IsClosed, name, amount, CRM_User__c, CreatedDate, account.name from opportunity o where o.Parent_Realm_ID__c = :realmid and o.IsClosed = true and o.CreatedDate = LAST_90_DAYS and o.createdbyid = '005000000072lyT']) {
                chanOpps.add(o);
            }
        }

        return chanOpps;
    }

    public List<Certification_Application__c> getCertApps() {
        system.debug('finding cert apps');
        list<promo_code__c> promoCodes = new list<promo_code__c>();
        if(promoCodes == null) promoCodes = [select id, name from promo_code__c where Promo_Type__c = 'Referral' and Account__c = :acctId];
        
        system.debug('found ' + promocodes.size() + ' promo codes');
        
        list<String> stringCodes = new List<string>();
        for(promo_code__c pc : promoCodes) {
            stringCodes.add(pc.name);
        }
        
        system.debug('added ' + stringcodes.size() + ' promo codes as strings');
        
        if(apps == null) apps = new List<Certification_Application__c>();
        
        for(certification_application__c certApp : [select id, name, Account__c, Application_Status__c, Source__c, CreatedDate from certification_application__c where Promo_Code__c IN :stringCodes and Application_result__c = 'Evaluating' and Linked_opportunity__c != :directOppId]) {
            apps.add(certApp);
        }
        
        system.debug('there are ' + apps.size() + ' apps in the final list');
        
        return apps;
    }

    public PageReference processSelected() {

        opportunity directOpp = [select id, Channel_Child_Opp__c, CRM_User__c, Submit_From_Trigger__c from Opportunity where id = :directOppId and stagename != 'Lost'];

        if(target == 'toolsChild') {
            for (opportunity opp : getOpps()) {
                if((String)opp.id == toSelect) {
                    
                    directOpp.channel_child_opp__c = opp.id;
                    directOpp.CRM_User__c = opp.CRM_User__c;                    
                    update directOpp;
    
                    Approval.ProcessSubmitRequest req1 = new Approval.ProcessSubmitRequest();
                    req1.setObjectId(directOpp.id);
                    Approval.ProcessResult result = Approval.process(req1);
    
                    break;
                }
            }
        }
        
        if(target == 'certChild') {
            for(certification_application__c app : getCertApps()) {
                if((String)app.id == toSelect) {
                    certification_application__c targetApp = [select id,name,linked_opportunity__c from certification_application__c where id = :toSelect limit 1];
                    targetApp.Linked_Opportunity__c = directOppId;
                    
                    try {
                        update targetApp;
                    } catch(Exception e) {
                        ApexPages.addMessages(e);
                    }
                    
                    break;
                }
            }
        }

        return new pagereference('/'+directopp.id);
    }

    public PageReference goBack() {
        pagereference pageref;
        pageref = new pagereference('/'+directoppid);
        return pageref;
    }*/

}