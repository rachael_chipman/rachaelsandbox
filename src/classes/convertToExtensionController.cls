//Will allow us to convert Renewals to Extensions based on 
//the condition that the product end dates have already been 
//updated and have a span of less than 12 months
//Will also only allow conversion to Extension from Renewal Opps

public class convertToExtensionController{

    public Opportunity o {get;set;}
    public list<OpportunityLineItem> RequireDateChange = new list<OpportunityLineItem> ();
    public String buttonText {get;set;}
    
    public convertToExtensionController(ApexPages.StandardController std){
    
    o = [SELECT Id, Type FROM Opportunity WHERE Id = :ApexPages.currentPage().getParameters().get('id')];
    
    list<OpportunityLineItem> olis = new list<OpportunityLineItem>([SELECT Id, Start_Date__c, End_Date__c FROM OpportunityLineItem WHERE OpportunityId = :o.Id and Start_Date__c != null and End_Date__c !=null and Opportunity_Type__c = 'Renewal']);
    
    if(olis.size()>0){
        for(OpportunityLineItem oli : olis){
            if(oli.Start_Date__c.monthsBetween(oli.End_Date__c) >= 11){
            RequireDateChange.add(oli);
            }
            }
            }
            
    buttonText = (
            RequireDateChange.size() > 0 ? 'Go to Product Entry' :
            o.type != 'Renewal' ? 'Return to Opp':''
            );
            
    if(RequireDateChange.size() > 0){
    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Please ensure that your product start and end dates span less than 12 months before converting this Opportunity to an Extension'));
    }
    }    
    
    public pageReference Convert(){
    
    if(o.Type == 'Renewal' && RequireDateChange.size() == 0){
    o.Type = 'Extension';
    update o;
    pageReference p = new pageReference('/apex/tabView?id='+o.id);
    return p;
    }
    if(o.Type != 'Renewal'){
    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Only Renewal Opportunities can be converted to Extensions'));
    return null;   
    }
    else{
    return null;
    }
    }

    public pageReference Redirect(){
    
    if(o.Type != 'Renewal'){
    pageReference p = new pageReference('/apex/tabView?id='+o.id);
    return p;
    }
    if(RequireDateChange.size() > 0){
    pageReference p = new pageReference('/apex/productEntry?id='+o.id);
    return p;
    }
    else{
    return null;
    }
    }
    
}