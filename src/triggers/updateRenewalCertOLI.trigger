trigger updateRenewalCertOLI on Opportunity (before update) {

Opportunity Renewal;
OpportunityLineItem Roli;

//Only pull opportunities that have been cloned for Pending Certification
    
    for(Opportunity o : trigger.new){
    
        if(o.IsClosed == true && o.Cloned_From_ID__c != null && o.Renewal_Updated__c == false && o.Application_Result__c != null){
        
        //Get Updated OLIs
        OpportunityLineItem oli = [SELECT Id, Start_Date__c, End_Date__c, UnitPrice FROM OpportunityLineItem WHERE OpportunityId = :o.Id and (Product_Code__c = '004-CERT-FEE0' or Product_Code__c = '005-CERT-REFC')];
        
        //Get Renewal Opp
        try{Renewal = [SELECT Id, Activation_Date__c, SSC_Volume__c, Certification_Tier_Sold__c FROM Opportunity WHERE IsClosed = false and Type = 'Renewal' and Previous_Opportunity_Lookup__c = :o.Cloned_From_Id__c LIMIT 1];}
        catch (Exception e){}
        
        //Get Renewal Opp Cert OLI
        try{Roli = [SELECT Id, Start_Date__c, End_Date__c, UnitPrice FROM OpportunityLineItem WHERE OpportunityId = :Renewal.Id and (Product_Code__c = '004-CERT-FEE0' or Product_Code__c = '005-CERT-REFC') LIMIT 1];}
        catch (Exception e) {}
        
            if(o.StageName == 'Finance Approved' && Renewal != null && Roli != null){
            
            Renewal.Activation_Date__c = o.Activation_Date__c;
            Renewal.SSC_Volume__c = o.SSC_Volume__c;
            Renewal.Certification_Tier_Sold__c = o.Certification_Tier_Sold__c;
            update Renewal;
            
            Roli.Start_Date__c = oli.End_Date__c + 1;
            Roli.End_Date__c = Roli.Start_Date__c + 364;
            Roli.UnitPrice = oli.UnitPrice;
            update Roli;
            
            o.Renewal_Updated__c = true;
            }
            
            if(o.StageName != 'Finance Approved' && Renewal != null && Roli != null){
                      
            delete Roli;
            
            o.Renewal_Updated__c = true;
            
            }
        }
    }
}