@isTest
private class CampaignFieldUpdateTest {
    
    static Campaign sourceCampaign;
    static Campaign assetCampaign;
    static List<Lead> testLeads;
    static List<Contact> testContacts;
    static Account testAccount;
    
    private static void setUpMethod()
    {
        sourceCampaign = new Campaign (Name='Test Source Campaign', isActive = true, Campaign_Sub_Type__c = 'Source', Type = 'SomeSource');
        insert sourceCampaign;

        assetCampaign = new Campaign (Name='Test Asset Campaign', isActive = true, Campaign_Sub_Type__c = 'Asset', Type = 'SomeAsset');
        insert assetCampaign;

        testLeads = TestUtils.createLeadsMultiple( 101, 'ACME Test Company', 'Thecat', 'Pending Assignment', false);
        system.runAs(AccountOwnerChange.ELOQUA)
        {
            for(Lead testLead : testLeads)
            {
                testLead.State = 'California';
                testLead.Lead_Rating_Combined__c = 'B3';
                testLead.isConverted = false;
            }
            insert testLeads;
        }
        
        testAccount = TestUtils.createAccount(TestUtils.MARKETER_RECORD_TYPE.Id, true);
        testContacts = TestUtils.createContactsMultiple(101, 'Oliver', 'Thecat', testAccount.Id, true);
        
    }
    
    private static testMethod void testBulkCampaignMemberInsert_LEAD_SOURCE_CAMPAIGN()
    {
        setUpMethod();
        List<CampaignMember> testMembers = new List<CampaignMember>();
        
        for(Integer i = 0; i < testLeads.size(); i++)
        {
            CampaignMember testSourceMember = TestUtils.createCampaignMember(sourceCampaign.Id, testLeads[i].Id, null, false);
            testSourceMember.Run_First_Most_Recent_Campaign_Sync__c = true;
            //CampaignMember testAssetMember = TestUtils.createCampaignMember(assetCampaign.Id, testLeads[i].Id, null, false);
            //testAssetMember.Run_First_Most_Recent_Campaign_Sync__c = true;
            testMembers.add(testSourceMember);
            //testMembers.add(testAssetMember);
        }
        test.startTest();
       // system.debug('What is in testMembers?'+testMembers);
        insert testMembers;
        test.stopTest();
        
        //Query for leads
        List<Lead> updatedLeads = [SELECT Id, First_Campaign__c, Campaign_Name__c, Most_Recent_Lead_Source__c, LeadSource,
        Campaign_Original__c, Campaign_Asset_Original__c, Campaign_Source_Most_Recent__c, Campaign_Asset_Most_Recent__c, 
       Lead_Source_Original_Detail__c, Lead_Asset_Original_Detail__c, Lead_Asset_Most_Recent__c FROM Lead WHERE Id IN: testLeads];
        for(Lead updatedLead : updatedLeads)
        {
            system.assertNotEquals(NULL, updatedLead.First_Campaign__c, 'First Campaign should not be null');
            system.assertNotEquals(NULL, updatedLead.Campaign_Name__c, 'Most Recent Campaign should not be null');
            system.assertEquals(sourceCampaign.Name, updatedLead.Lead_Source_Original_Detail__c, 'We expect the source campaign name to Source Detail');
            //system.assertEquals(assetCampaign.Name, updatedLead.Lead_Asset_Original_Detail__c, 'We expect the asset campaign name to populate the Asset Detail');
            system.assertEquals(sourceCampaign.Type, updatedLead.LeadSource, 'Source should be the campaign type');
            //system.assertEquals(assetCampaign.Type, updatedLead.Lead_Asset_Most_Recent__c, 'Asset should be the campaign type');
            //system.assertEquals(assetCampaign.Id, updatedLead.Campaign_Asset_Original__c, 'Campaign Asset shoudl be the asset campaign');
            system.assertEquals(sourceCampaign.Id, updatedLead.Campaign_Original__c, 'Campaign Source should be the source campaign.');
        }
    }

    private static testMethod void testBulkCampaignMemberInsert_LEAD_ASSET_CAMPAIGN()
    {
        setUpMethod();
        List<CampaignMember> testMembers = new List<CampaignMember>();
        
        for(Integer i = 0; i < testLeads.size(); i++)
        {
            //CampaignMember testSourceMember = TestUtils.createCampaignMember(sourceCampaign.Id, testLeads[i].Id, null, false);
            //testSourceMember.Run_First_Most_Recent_Campaign_Sync__c = true;
            CampaignMember testAssetMember = TestUtils.createCampaignMember(assetCampaign.Id, testLeads[i].Id, null, false);
            testAssetMember.Run_First_Most_Recent_Campaign_Sync__c = true;
            //testMembers.add(testSourceMember);
            testMembers.add(testAssetMember);
        }
        test.startTest();
       // system.debug('What is in testMembers?'+testMembers);
        insert testMembers;
        test.stopTest();
        
        //Query for leads
        List<Lead> updatedLeads = [SELECT Id, First_Campaign__c, Campaign_Name__c, Most_Recent_Lead_Source__c, LeadSource,
        Campaign_Original__c, Campaign_Asset_Original__c, Campaign_Source_Most_Recent__c, Campaign_Asset_Most_Recent__c, 
       Lead_Source_Original_Detail__c, Lead_Asset_Original_Detail__c, Lead_Asset_Most_Recent__c FROM Lead WHERE Id IN: testLeads];
        for(Lead updatedLead : updatedLeads)
        {
            system.assertNotEquals(NULL, updatedLead.First_Campaign__c, 'First Campaign should not be null');
            system.assertNotEquals(NULL, updatedLead.Campaign_Name__c, 'Most Recent Campaign should not be null');
            //system.assertEquals(sourceCampaign.Name, updatedLead.Lead_Source_Original_Detail__c, 'We expect the source campaign name to Source Detail');
            system.assertEquals(assetCampaign.Name, updatedLead.Lead_Asset_Original_Detail__c, 'We expect the asset campaign name to populate the Asset Detail');
            //system.assertEquals(sourceCampaign.Type, updatedLead.LeadSource, 'Source should be the campaign type');
            system.assertEquals(assetCampaign.Type, updatedLead.Lead_Asset_Most_Recent__c, 'Asset should be the campaign type');
            system.assertEquals(assetCampaign.Id, updatedLead.Campaign_Asset_Original__c, 'Campaign Asset shoudl be the asset campaign');
            //system.assertEquals(sourceCampaign.Id, updatedLead.Campaign_Original__c, 'Campaign Source should be the source campaign.');
        }
    }
    
    private static testMethod void testBulkCampaignMemberInsert_CONTACT_ASSET_CAMPAIGN()
    {
    	setUpMethod();
    	
    	Opportunity testOpp = TestUtils.createOpportunity(TestUtils.RESEARCH_RECORD_TYPE.Id, testAccount.Id, true);
    	List<OpportunityContactRole> testContactRoles = new List<OpportunityContactRole>();
    	List<CampaignMember> testMembers = new List<CampaignMember>();
    	
    	for(Integer i = 0; i < testContacts.size(); i++)
    	{
    		OpportunityContactRole testRole = new OpportunityContactRole();
    		testRole.ContactId = testContacts[i].Id;
    		testRole.OpportunityId = testOpp.Id;
    		testRole.Role = 'TestRole';
    		testContactRoles.add(testRole);
    		
    		//CampaignMember sourceMember = TestUtils.createCampaignMember(sourceCampaign.Id, null, testContacts[i].Id, false);
            //sourceMember.Run_First_Most_Recent_Campaign_Sync__c = true;
            CampaignMember assetMember = TestUtils.createCampaignMember(assetCampaign.Id, null, testContacts[i].Id, false);
            assetMember.Run_First_Most_Recent_Campaign_Sync__c = true;
            //testMembers.add(sourceMember);
            testMembers.add(assetMember);
    	}
    	
    	insert testContactRoles;
    	
    	test.startTest();
    	insert testMembers;
    	test.stopTest();
    	
    	List<Contact> updatedContacts = [SELECT Id, First_Campaign__c, Campaign_Name__c,Most_Recent_Lead_Source__c, LeadSource,
        Campaign_Source_Original__c, Campaign_Asset_Original__c, Campaign_Source_Most_Recent__c, Campaign_Asset_Most_Recent__c, 
        Lead_Source_Original_Detail__c, Lead_Asset_Original_Detail__c, Lead_Asset_Most_Recent__c FROM Contact WHERE Id IN : testContacts];
    	for(Contact updatedContact : updatedContacts)
    	{
    		system.assertNotEquals(NULL, updatedContact.First_Campaign__c, 'We expect the campaign name to populate the First Campaign field');
    		system.assertNotEquals(NULL, updatedContact.Campaign_Name__c, 'We expect the campaign name to populate the Most Recent Campaign field');
            //system.assertEquals(sourceCampaign.Name, updatedContact.Lead_Source_Original_Detail__c, 'We expect the source campaign name to Source Detail');
            system.assertEquals(assetCampaign.Name, updatedContact.Lead_Asset_Original_Detail__c, 'We expect the asset campaign name to populate the Asset Detail');
            //system.assertEquals(sourceCampaign.Type, updatedContact.LeadSource, 'Source should be the campaign type');
            system.assertEquals(assetCampaign.Type, updatedContact.Lead_Asset_Most_Recent__c, 'Asset should be the campaign type');
            system.assertEquals(assetCampaign.Id, updatedContact.Campaign_Asset_Original__c, 'Campaign Asset shoudl be the asset campaign');
            //system.assertEquals(sourceCampaign.Id, updatedContact.Campaign_Source_Original__c, 'Campaign Source should be the source campaign.');
    	}
    	
    	List<Opportunity> updatedOpps = [SELECT Id, Most_Recent_Campaign__c FROM Opportunity WHERE Id = :testOpp.Id];
    	system.assertEquals(assetCampaign.Name, updatedOpps[0].Most_Recent_Campaign__c, 'We expect the campaign name to populate the Most Recent Campaign field on the opportunity');
    }

    private static testMethod void testBulkCampaignMemberInsert_CONTACT_SOURCE_CAMPAIGN()
    {
        setUpMethod();
        
        Opportunity testOpp = TestUtils.createOpportunity(TestUtils.RESEARCH_RECORD_TYPE.Id, testAccount.Id, true);
        List<OpportunityContactRole> testContactRoles = new List<OpportunityContactRole>();
        List<CampaignMember> testMembers = new List<CampaignMember>();
        
        for(Integer i = 0; i < testContacts.size(); i++)
        {
            OpportunityContactRole testRole = new OpportunityContactRole();
            testRole.ContactId = testContacts[i].Id;
            testRole.OpportunityId = testOpp.Id;
            testRole.Role = 'TestRole';
            testContactRoles.add(testRole);
            
            CampaignMember sourceMember = TestUtils.createCampaignMember(sourceCampaign.Id, null, testContacts[i].Id, false);
            sourceMember.Run_First_Most_Recent_Campaign_Sync__c = true;
            //CampaignMember assetMember = TestUtils.createCampaignMember(assetCampaign.Id, null, testContacts[i].Id, false);
            //assetMember.Run_First_Most_Recent_Campaign_Sync__c = true;
            testMembers.add(sourceMember);
            //testMembers.add(assetMember);
        }
        
        insert testContactRoles;
        
        test.startTest();
        insert testMembers;
        test.stopTest();
        
        List<Contact> updatedContacts = [SELECT Id, First_Campaign__c, Campaign_Name__c,Most_Recent_Lead_Source__c, LeadSource,
        Campaign_Source_Original__c, Campaign_Asset_Original__c, Campaign_Source_Most_Recent__c, Campaign_Asset_Most_Recent__c, 
        Lead_Source_Original_Detail__c, Lead_Asset_Original_Detail__c, Lead_Asset_Most_Recent__c FROM Contact WHERE Id IN : testContacts];
        for(Contact updatedContact : updatedContacts)
        {
            system.assertNotEquals(NULL, updatedContact.First_Campaign__c, 'We expect the campaign name to populate the First Campaign field');
            system.assertNotEquals(NULL, updatedContact.Campaign_Name__c, 'We expect the campaign name to populate the Most Recent Campaign field');
            system.assertEquals(sourceCampaign.Name, updatedContact.Lead_Source_Original_Detail__c, 'We expect the source campaign name to Source Detail');
            //system.assertEquals(assetCampaign.Name, updatedContact.Lead_Asset_Original_Detail__c, 'We expect the asset campaign name to populate the Asset Detail');
            system.assertEquals(sourceCampaign.Type, updatedContact.LeadSource, 'Source should be the campaign type');
            //system.assertEquals(assetCampaign.Type, updatedContact.Lead_Asset_Most_Recent__c, 'Asset should be the campaign type');
            //system.assertEquals(assetCampaign.Id, updatedContact.Campaign_Asset_Original__c, 'Campaign Asset shoudl be the asset campaign');
            system.assertEquals(sourceCampaign.Id, updatedContact.Campaign_Source_Original__c, 'Campaign Source should be the source campaign.');
        }
        
        List<Opportunity> updatedOpps = [SELECT Id, Most_Recent_Campaign__c FROM Opportunity WHERE Id = :testOpp.Id];
        system.assertEquals(sourceCampaign.Name, updatedOpps[0].Most_Recent_Campaign__c, 'We expect the campaign name to populate the Most Recent Campaign field on the opportunity');
    }

}