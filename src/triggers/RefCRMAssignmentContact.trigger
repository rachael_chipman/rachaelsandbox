trigger RefCRMAssignmentContact on Contact (Before update) {

/*DESCRIPTION: This trigger will assign the Referring CRM and the Referring Partner to a contact
based on the Promo Code that gets updated on the Contact by Eloqua via the General Referral web form*/

    for(Contact c : trigger.new){
    
    //Set string variable for Most Recent Campaign
    String str = c.Campaign_Name__c;
    //Set variable for owner change and auto submit
    Contact prev = trigger.oldMap.get(c.Id);
    
    //Only pull Contacts that have a Gen Ref Most Recent Campaign
    if( SObjectTriggerServices.isValidCampaign( str ) && c.Gen_Ref_Contact__c == false){
        
        //If the Promo Code field is not blank get the CRM and Account from the Promo Code --> Otherwise assign Peter M. as the Referring CRM
        if(c.Promo_Code__c != null && c.Gen_Ref_Contact__c == false){
    
        List<Promo_Code__c> pc = [select Id, Name, Promo_Type__c, Account__c from Promo_Code__c where Name = :c.Promo_Code__c Limit 1];
       
           if(pc.size() < 1){
               c.Referring_CRM__c = '005000000074QZs';
               c.OwnerId = '005000000074QZs';}
           else{
                for(Promo_Code__c pc2 : pc){
                    if(pc2.Account__c == null){c.Referring_CRM__c = '005000000074QZs'; c.OwnerId = '005000000074QZs';}
                    if(pc2.Account__c != null){
        
                try{Account pcacct = [select Id, CRM__c from Account where Id = :pc2.Account__c and CRM__c != null];
            
                c.Referring_Partner__c = pc2.Account__c;             
                c.Referring_CRM__c = pcacct.CRM__c;
                c.OwnerId = pcacct.CRM__c;
    
                }
                catch(Exception e){}
                }}}}
  
            
      if(c.Promo_Code__c == null ){
    
                c.Referring_CRM__c = '005000000074QZs';
                c.OwnerId = '005000000074QZs';         
          } 
      }     
                    
}
}