@isTest
public class OpportunityAcceptExtensionTest {
    
    static testMethod void testOpportunityAcceptExtension()
    {
        //setup test records
        List<opportunity> opplist = CreateTestRecords(1);
        if(opplist!= null)
        {
            Opportunity TOpp = opplist.get(0);
            PageReference pageref = Page.OpportunityAccept;
            pageref.getParameters().put('id',TOpp.Id);
            Test.setCurrentPage(pageref);
            ApexPages.StandardController sc = new ApexPages.StandardController(TOpp);
             OpportunityAcceptExtension testcontroller = new  OpportunityAcceptExtension(sc);
            test.startTest();
            testcontroller.updateContact();
            test.stopTest();
            assertResults(opplist);   
        }
    }
  
    public static void assertResults(List<opportunity> opplist)
    {
        List<Id> conIds = new List<Id>();
        List<Opportunity> opps = [Select Id,Opportunity_Accepted__c from Opportunity where Id in :opplist ];
        //assert the Opportunity Field
    	for(Opportunity opp: opps)    
        {
            system.assert(opp.Opportunity_Accepted__c <= system.now());
		}
        
        //assert the contact Field
        List<OpportunityContactRole> ContactRoles = [select Contactid, Id from OpportunityContactRole where OpportunityId in :opplist];
        if(ContactRoles!= null)
        {
            for(OpportunityContactRole Tcon: ContactRoles)
            {
                conIds.add(Tcon.ContactId);
            }
        }
        
        List<Contact> contacts = [select Id, Qualifying_Status__c from contact where Id in :conIds];
        if(contacts!= null)
        {
            for(Contact con: contacts)
            {
                system.assertEquals(con.Qualifying_Status__c, 'RSE Accept', 'values match');
            }
        }
    }
    
    public static list<Opportunity> CreateTestRecords(integer records)
    {
        List<Opportunity> opplist = new List<Opportunity>();
        List<Contact> conlist = new List<Contact>();
        List<OpportunityContactRole> conrolelist = new List<OpportunityContactRole>();
        
        //Create a new Opportunity Object of rec type pre-pipeline
        List<RecordType> rectype = [Select Id, name from RecordType where Name='Pre-Pipeline'];
        for(integer i=0;i<records;i++)
        {
            Opportunity newOpp = new Opportunity();
            newOpp.Name = 'Test '+ i;
            newOpp.CloseDate = Date.today();
            newOpp.StageName = 'Pre-Pipeline';
            //newOpp.Opportunity_Accepted__c = System.;
            //newOpp.ForecastCategory = 'Pipeline';
            newOpp.RecordTypeId = rectype[0].Id;
            opplist.add(newOpp);
        }
        
        insert opplist;
        //Create the contact object
        for(Integer i=0;i<10;i++)
        {
            Contact con = new Contact();
            con.LastName = 'TestCon' +i;
            con.Contact_Status__c = 'Current';
            con.ContactsRole__c = 'Return Path User';
            conlist.add(con);
        }
        
        insert conlist;
        //Create the Contact Role object
        for(Opportunity topp: opplist)
        {
            for(Integer i=0;i<10;i++)
            {
                OpportunityContactRole conrole = new OpportunityContactRole();
                conrole.ContactId = conlist[i].Id;
                conrole.OpportunityId = topp.Id;
                conrolelist.add(conrole);
            }
        }
        
        insert conrolelist;
        return opplist;
    }

}