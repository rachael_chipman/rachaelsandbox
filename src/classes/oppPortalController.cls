public class oppPortalController {

    private string oppid;
    private string recordType;
    private pageReference p;

    public oppPortalController (ApexPages.standardController std) {
        oppid = ApexPages.currentPage().getParameters().get('id');
        recordType = [select recordtype.name from opportunity where id = :oppid limit 1].recordtype.name;

        if(recordType == 'Quarantined' ||
           recordType == 'Pre-Pipeline' ||
           recordType == 'Research' || 
           recordType == 'Select' ||
           recordType == 'Purchase'|| 
           recordType == 'Finance Approved' || 
           recordType == 'Termination'  || 
           recordType == 'Tools Child')
        {
            p = new pageReference('/apex/tabView?id='+oppid);
        } 
        else 
        { 
            p = new PageReference('/apex/defaultOpp?id='+oppid); 
        }
    }

    public PageReference oppRedirect() {
        p.setRedirect(true);
        return p;
    }

}