public with sharing class AccountService {

    public static void populateAccountsReceivableControl(List<Account> newAccounts){
        
        List<c2g__codaGeneralLedgerAccount__c> glTradeAccount = [Select Id  From c2g__codaGeneralLedgerAccount__c c where Name = '1100 - AR-TRADE'];
        
        if (glTradeAccount != null && glTradeAccount.size() == 1){
            for (Account newAccount : newAccounts ){
                newAccount.c2g__CODAAccountsReceivableControl__c = glTradeAccount[0].id; 
            }
        }
    }
    
    /*public static void accountTerminationsRollUp()
    {
        list<Account> scope = [SELECT Id FROM Account WHERE Count_of_Termination_Ops__c > 0];
        system.debug('What is in the scope?'+scope);
        
        set<Id> uniqueCustomObjectSet = new set<Id>();
        for( Account acc : scope )
        {
            uniqueCustomObjectSet.add( acc.Id );
        }
        
        list<AggregateResult> totalTermAmounts = [SELECT AccountId acctId, SUM(Prev_Recurring_ACV_Before_Partner_Comm__c)sum 
        FROM Opportunity 
        WHERE AccountId IN: uniqueCustomObjectSet AND CloseDate = LAST_N_DAYS:365 and StageName = 'Terminated Contract'
        GROUP BY AccountId ];
        system.debug('What is in the totalTermAmounts list?'+ totalTermAmounts);
        
        list<Account> accountsToUpdate = new list<Account>();
        
        for( AggregateResult total : totalTermAmounts )
        {
            Id accountId = ( id ) total.get( 'acctId' ); 
            
            Account acc = new Account (Id = accountId, Sum_of_Terminated_ACV_Last_12_months__c = ( decimal ) total.get( 'sum' )  );
            accountsToUpdate.add( acc );
            
            for( Account acct : scope )
            {
                if( acct.Id == accountId )
                {
                    acct.Sum_of_Terminated_ACV_Last_12_months__c = ( decimal ) total.get( 'sum' );
                }
            }
        }
        update accountsToUpdate;
    }*/
    
    private static Map<Id,AccountShare> createShareMap(List<Account> scope, List<AccountShare> acclist)
    {
        List<AccountShare> accshare = new List<AccountShare>();
        List<AccountTeamMember> accteam = new List<AccountTeamMember>();
        
        Map<Id,AccountShare> accshareMap = new Map<Id,AccountShare>();
        //first build the map of the valid userId.
       for(Account taccount: scope)
        {
           if(taccount.AccountTeamMembers!= null || taccount.AccountTeamMembers.size() > 0)
           {
                accteam = taccount.AccountTeamMembers;
               System.debug('Size of accountTeam is :' + accteam.size());
              
               for(AccountTeamMember taccTM :accteam)
               {
                   System.debug('Acc team is :' + taccTM);
                    accshareMap.put(taccTM.UserId,null);
                }
           }
        }
        
       // UserIds = accshareMap.
        //now filer the valid users from the accountShare list for this account
        for(Account taccount: scope)
        {
         // acclist = taccount.Shares;
           System.debug('Share list is :' + acclist);
            System.debug('Size of Share list is :' + acclist.size());
         if(acclist!= null || acclist.size() > 0)
          {
              for(AccountShare tshare: acclist)
              {
                  System.debug('tshare key and user Id is :' + tshare.UserOrGroupId );
                  if(accshareMap.containsKey(tshare.UserOrGroupId) && !tshare.IsDeleted)
                  {
                        accshareMap.put(tshare.UserOrGroupId, tshare);
                   }
               }
          }
        }
        
        System.debug('Acc share Map is : ' + accshareMap);
        return accshareMap;
    }
    
    public static Map<Id,String> updatePeoplesTeamSection(List<Account> scope)
    {
        List<AccountTeamMember> accteam = new List<AccountTeamMember>();
        List<AccountTeamMember> accteamupdatelist = new List<AccountTeamMember>();
        List<AccountShare> accsharelist = new List<AccountShare>();
        List<AccountShare> accsharelist2 = new List<AccountShare>();
        string TMrole;
        boolean isTAMupdate;
        boolean isCMupdate;
        boolean isRMupdate;
        boolean isAPSClientupdate;
        boolean isInboxInsightClentupdate;
        boolean isSalespersonupdated;
        boolean isRegionalDirectorupdated;
        boolean isCorpDevOtherupdated;
        boolean isCollectionTMupdted;
        boolean isChannelPartnerRep;
        boolean isCommercialSalesRep;
        string errormessage = '';
        Map<Id,String> failedaccountMap = new Map<Id,String>();
        //Map<Id,AccountShare> userIdAccshareMap = new Map<Id,AccountShare>();
        //accsharelist = [Select Id, AccountId, AccountAccessLevel,OpportunityAccessLevel,UserOrGroupId,IsDeleted,RowCause from AccountShare where AccountId in:scope and RowCause = 'Rule' ];
        //build the accountsharemap
        //userIdAccshareMap = createShareMap(scope,accsharelist);
        for(Account taccount: scope)
        {
            //Initilize the booleans to false;
           isTAMupdate = false;
           isCMupdate = false;
           isRMupdate = false;
           isAPSClientupdate = false;
           isInboxInsightClentupdate = false;
           isSalespersonupdated = false;
           isRegionalDirectorupdated= false;
           isCorpDevOtherupdated = false;
           isCollectionTMupdted = false;
           //isChannelPartnerRep = false;
           isRegionalDirectorupdated = false;
           isCommercialSalesRep = false;
           String accesslevel;
           if(taccount.AccountTeamMembers!= null || taccount.AccountTeamMembers.size() > 0)
           {
               accteam = taccount.AccountTeamMembers;
               for(AccountTeamMember taccTM :accteam)
               {
                   TMrole = taccTM.TeamMemberRole;
                   
                   if(TMrole!= null)
                   {
                                           
                       if((TaccTM.AccountAccessLevel == 'None' || TaccTM.AccountAccessLevel.Contains('Read')) && (taccTM.User.IsActive) && ( TaccTM.UserId!= taccount.OwnerId))
                       {
                             System.debug('Account Accesslevel for user ' +taccTM.UserId + 'is :' + taccTM.AccountAccessLevel);
                           accsharelist.add(CreateAccountShare(taccTM.AccountId,taccTM.UserId,'Edit','Edit','Edit','Edit'));
                       }
                       
                     if(TMrole.equalsIgnoreCase('Channel Manager') && isCMupdate!= true)
                     {
                         taccount.CRM__c = TaccTM.UserId;
                         isCMupdate = true;
                     }
                     else if(TMrole.equalsIgnoreCase('Relationship Manager') && isRMupdate!= true)
                     {
                        taccount.AM__c = TaccTM.UserId;
                        isRMupdate = true;
                     }
                     else if(TMrole.equalsIgnoreCase('Technical Account Manager') && isTAMupdate!= true)
                     {
                         taccount.Technical_Account_Manager__c = TaccTM.UserId;
                         isTAMupdate = true;
                     }
                       
                     else if(TMrole.equalsIgnoreCase('EFP Client Services') && isAPSClientupdate!= true )
                      {
                          taccount.APS_Client_Services__c = TaccTM.UserId;
                          isAPSClientupdate = true;
                      }
                      else if(TMrole.equalsIgnoreCase('Inbox Insight Client Services') && isInboxInsightClentupdate!= true )
                      {
                          taccount.Inbox_Insight_Client_Services__c = TaccTM.UserId;
                          isInboxInsightClentupdate = true;
                      }
                      else if(TMrole.equalsIgnoreCase('RSE Sales Rep') && isSalespersonupdated!= true)
                      {
                          taccount.Salesperson__c = TaccTM.UserId;
                          isSalespersonupdated = true;
                      }
                       else if(TMrole.equalsIgnoreCase('Collections Lead') && isCollectionTMupdted!= true)
                       {
                            taccount.Collections_Team_Member__c = taccTM.User.Name;
                           isCollectionTMupdted = true;
                       }
                      /* else if(TMrole.equalsIgnoreCase('Channel Partner Rep') && isChannelPartnerRep!= true)
                       {
                            taccount.AM__c = TaccTM.UserId;
                           isChannelPartnerRep = true;
                       }*/
                       else if(TMrole.equalsIgnoreCase('Regional Director') && isRegionalDirectorupdated != true )
                       {
                           taccount.Regional_Director__c= TaccTM.UserId;
                           
                           isRegionalDirectorupdated = true;
                       }
                       else if(TMrole.equalsIgnoreCase('Provider BD') && isCorpDevOtherupdated!= true )
                       {
                           taccount.CorpDev_Other__c = TaccTM.UserId;
                           isCorpDevOtherupdated = true;
                       }
                       else if(TMrole.equalsIgnoreCase('Commercial Sales Rep') && isCommercialSalesRep!= true)
                       {
                          taccount.Commerical_Sales_Rep__c = TaccTM.UserId;
                          isCommercialSalesRep = true;
                       }
                       
                   }
                   
               }
           }
           
            
        if(Test.isRunningTest())
        {
            failedaccountmap.put(taccount.Id , 'Test Error Message');
        }
      }
        
         //update the AccountShare
        if(!accsharelist.isEmpty())
        {
            System.debug('Updating the Account Share' + accsharelist);
            string msg ='';
            List<Database.SaveResult> Result = Database.insert(accsharelist,false);
            for(Database.SaveResult dr: Result)
            {
                if(!dr.isSuccess())
                {
                    for(Database.Error errs: dr.getErrors())
                    {
                        msg+=  errs.getMessage() + '\n' + errs.getStatusCode() + '\n';
                    }
                }
            }
            if(!String.isEmpty(msg))
            {
                System.debug ('error message is : ' + msg);
                Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                String[] toAddress = new String[] {'sangita.ghosh@returnpath.com'};
                mail.setToAddresses(toAddress );
                mail.setPlainTextBody('Error in setting the Access level    ' +msg);
                mail.setSubject('Error in Setting Access level');
                Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
            }

         }
        //update the values
        System.debug('Updating the Peoples Field' + scope);
        List<Database.SaveResult> DSRS = Database.update(scope,false);
        for(Database.SaveResult drs :DSRS )
        {
                if(!drs.isSuccess())
                {
                    for(Database.Error errs: drs.getErrors())
                    {
                        errormessage+=  errs.getMessage() + '\n' + errs.getStatusCode() + '\n';
                    }
                    failedaccountmap.put(drs.getId(),errormessage);
                }
         }
        
        
       
        return failedaccountMap;
      }
    
    
    public static void sendErrorEmail(Map<Id, String> emaildatavalues)
    {
         string blobvalues = '';
        //send email to the SF SPLAT team for list of failed updates
        if(!emaildatavalues.isEmpty())
        {
            blob csvblob;
            Messaging.EmailFileAttachment csvAtt = new Messaging.EmailFileAttachment();
            for(Id tid : emaildatavalues.keySet())
            {
                
                blobvalues+= tid + ',' +  emaildatavalues.get(tid) + ',';
              
             }
            //remove the extra comma from the end
            blobvalues = blobvalues.removeEnd(',');
            csvblob = Blob.valueOf(blobvalues);
            csvAtt.setBody(csvblob);
            csvAtt.setFileName('AccountPeopleUpdateError.csv');
            Messaging.SingleEmailMessage emailmsg = new Messaging.SingleEmailMessage();
            List<String> ToAddress = new List<String>();
            ToAddress.add('SalesforceRequests@returnpath.com');
            emailmsg.setToAddresses(ToAddress);
            emailmsg.setSubject('Error related to Update Peoples team Batch');
            emailmsg.setPlainTextBody('Attached is the list of Account failed');
            emailmsg.setFileAttachments(new Messaging.EmailFileAttachment[]{csvAtt});
            Messaging.SendEmailResult[] emailresult = Messaging.sendEmail(new Messaging.SingleEmailMessage[]{emailmsg});
            
        }
        
    }
   
    public static AccountTeamMember createAccTeamMember(Id accId, String accrole,Id accUserId)
    {
        AccountTeamMember ATM = new AccountTeamMember();
        ATM.AccountId = accId;
       // ATM.AccountAccessLevel = accAccessLevel;
        ATM.TeamMemberRole = accrole;
        ATM.UserId = accUserId;
        return ATM;
    }
    
    public static AccountShare CreateAccountShare(Id accId, Id UserId,  string accaccess, string oppaccess,string caseaccess,string contactaccess)
    {
        AccountShare accesslevel = new AccountShare();
        accesslevel.AccountId = accId;
        accesslevel.UserOrGroupId = UserId;
        accesslevel.AccountAccessLevel = accaccess;
        accesslevel.CaseAccessLevel = caseaccess;
        accesslevel.ContactAccessLevel = contactaccess;
        accesslevel.OpportunityAccessLevel = oppaccess;
       // accesslevel.RowCause = 'Team'; 
        return accesslevel;
    }
    
    private static Map<Id,String> createDBResultMap(List<Database.SaveResult> SDSR, string errmsg)
    {
        String errormessage =  errmsg + ' ,';
        Map<Id,String> failedaccountMap = new Map<Id,String>();
        //failedaccountMap.put('000',errmsg);
        for(Database.SaveResult drs:SDSR)
            {
                if(!drs.isSuccess())
                {
                    for(Database.Error errs: drs.getErrors())
                    {
                        errormessage+=  errs.getMessage() + '\n' + errs.getStatusCode() + '\n';
                    }
                    failedaccountmap.put(drs.getId(),errormessage);
                }
            }
        return failedaccountMap;
    }
    
    public static Map<Id,String> createAccountTeamMembersandInsert(List<sObject> scope )
     {
        Map<Id,String> failedaccountMap = new Map<Id,String>();
        List<AccountTeamMember> accTM = new List<AccountTeamMember>();
        //list to hold new account sharing rules
        List<AccountShare> acctSharingList = new List<AccountShare>();
        Map<Id, AccountShare> accShareMap = new Map<Id, AccountShare>();
     
        String errormessage = '';
        for(sObject temp: scope)
        {
            Account tacc = (Account) temp;
            if(tacc.Salesperson__c!= null && tacc.Salesperson__r.IsActive )
            {
                if(tacc.Salesperson__r.Name == 'Ray Miller' || tacc.Salesperson__r.Name == 'Charles Martelli')
                    accTM.add(createAccTeamMember(tacc.Id,'Enterprise Sales Rep',tacc.Salesperson__c));
                
                else if(tacc.Salesperson__r.Name == 'Jim Small' || tacc.Salesperson__r.Name == 'Dick Hichens' || tacc.Salesperson__r.Name == 'Grant Revan' )
                      accTM.add(createAccTeamMember(tacc.Id,'EFP Sales Rep',tacc.Salesperson__c));
                else
                   accTM.add(createAccTeamMember(tacc.Id,'RSE Sales Rep',tacc.Salesperson__c)); 
                
               acctSharingList.add(createAccountShare(tacc.Id,tacc.Salesperson__c,'Edit','Edit','Edit','Edit'));
               accShareMap.put(tacc.Salesperson__c,createAccountShare(tacc.Id,tacc.Salesperson__c,'Edit','Edit','Edit','Edit'));
            }
            
            if(tacc.AM__c!= null && tacc.AM__r.IsActive){
              accTM.add(createAccTeamMember(tacc.Id,'Relationship Manager',tacc.AM__c));
              acctSharingList.add(createAccountShare(tacc.Id,tacc.AM__c,'Edit','Edit','Edit','Edit'));
              accShareMap.put(tacc.AM__c,createAccountShare(tacc.Id,tacc.AM__c,'Edit','Edit','Edit','Edit'));
            }
            if(tacc.CRM__c!= null && tacc.CRM__r.IsActive){
                accTM.add(createAccTeamMember(tacc.Id,'Channel Manager',tacc.CRM__c));
                acctSharingList.add(createAccountShare(tacc.Id,tacc.CRM__c,'Edit','Edit','Edit','Edit'));
                accShareMap.put(tacc.CRM__c,createAccountShare(tacc.Id,tacc.CRM__c,'Edit','Edit','Edit','Edit'));
            }
            if(tacc.Technical_Account_Manager__c!= null && tacc.Technical_Account_Manager__r.IsActive){
                accTM.add(createAccTeamMember(tacc.Id,'Technical Account Manager',tacc.Technical_Account_Manager__c));
                acctSharingList.add(createAccountShare(tacc.Id,tacc.Technical_Account_Manager__c,'Edit','Edit','Edit','Edit'));
                accShareMap.put(tacc.Technical_Account_Manager__c,createAccountShare(tacc.Id,tacc.Technical_Account_Manager__c,'Edit','Edit','Edit','Edit'));
            }
            if(tacc.APS_Client_Services__c!= null && tacc.APS_Client_Services__r.IsActive){
                accTM.add(createAccTeamMember(tacc.Id,'EFP Client Services',tacc.APS_Client_Services__c));
                acctSharingList.add(createAccountShare(tacc.Id,tacc.APS_Client_Services__c,'Edit','Edit','Edit','Edit'));
                accShareMap.put(tacc.APS_Client_Services__c,createAccountShare(tacc.Id,tacc.APS_Client_Services__c,'Edit','Edit','Edit','Edit'));
            }
            if(tacc.Inbox_Insight_Client_Services__c != null && tacc.Inbox_Insight_Client_Services__r.IsActive){
               accTM.add(createAccTeamMember(tacc.Id,'Inbox Insight Client Services',tacc.Inbox_Insight_Client_Services__c));
               acctSharingList.add(createAccountShare(tacc.Id,tacc.Inbox_Insight_Client_Services__c,'Edit','Edit','Edit','Edit'));
                accShareMap.put(tacc.Inbox_Insight_Client_Services__c,createAccountShare(tacc.Id,tacc.Inbox_Insight_Client_Services__c,'Edit','Edit','Edit','Edit'));
            }
         
            if(tacc.CorpDev_Other__c != null && tacc.CorpDev_Other__r.IsActive && (tacc.CorpDev_Other__r.Name == 'Alex Rubin' ||
                tacc.CorpDev_Other__r.Name == 'Georges Smine'  || tacc.CorpDev_Other__r.Name == 'Jake Curtis'  || tacc.CorpDev_Other__r.Name == 'Martin Schuller' 
                 || tacc.CorpDev_Other__r.Name == 'Melinda Plemel')){
                accTM.add(createAccTeamMember(tacc.Id,'Provider BD',tacc.CorpDev_Other__c));
                acctSharingList.add(createAccountShare(tacc.Id,tacc.CorpDev_Other__c, 'Edit','Edit','Edit','Edit'));
                if(Test.isRunningTest())
                   accShareMap.put(tacc.CorpDev_Other__c,createAccountShare(null,tacc.CorpDev_Other__c,'None','None','None','None'));
                else
                    accShareMap.put(tacc.CorpDev_Other__c,createAccountShare(tacc.Id,tacc.CorpDev_Other__c,'Edit','Edit','Edit','Edit'));
                 
            }
             
        } 
        if(!accTM.isEmpty())
        {
            List<Database.SaveResult> SDSR = Database.insert(accTM,false);
           
            System.debug('Database insert for Acc Team is : ' + SDSR);
            
            failedaccountMap.putAll(createDBResultMap(SDSR,'Error in AccountTeamMember Insert'));
            //remove the errored account for the accountShare
            if(!failedaccountMap.isEmpty())
            {
                //Set<Id> accId = failedaccountMap.keySet();
                List<Id> Idlist = new List<Id>();
                Idlist.addAll(failedaccountMap.keySet());
                for(Id Tid: Idlist)
                {
                    accShareMap.remove(Tid);
                }
            }
            System.debug('Acc Share Map is  : ' + accShareMap);
            //insert the AccountShare list && check the Id in the map <TMId, acctshare> 
            //remove the errored accounts
              
            //if(!acctSharingList.isEmpty() )
            if(accShareMap.values()!= null || accShareMap.Values().size() > 0)
            {
                 //SDSR = Database.insert(acctSharingList,false);
                 SDSR = Database.insert(accShareMap.values(), false);
                failedaccountMap.putAll(createDBResultMap(SDSR,'Error in AccountShare Insert'));
            }
        }
        return failedaccountMap;
      }
    
    
    
    
    
    
    
    
    
    
    

}