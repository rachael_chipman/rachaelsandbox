///////////////////This is how the batch class is called
/*id batchinstanceid = database.executeBatch(new AccountLeadCompareConvert('select Id, LeadId, Campaign.Name, Campaign.Type, ContactID, Lead.isconverted, Lead.First_Campaign__c, Lead.Campaign_Name__c,
       Lead.Most_Recent_Lead_Source__c, Lead.LeadSource,  Contact.First_Campaign__c, Contact.Campaign_Name__c, Contact.First_Campaign_Id__c , 
       Contact.Account.AccountManager__c, Contact.Account.Customer_Relationship_Manager_Primary__c, Contact.Account.CRM__c,  Contact.Account.CRM__r.Email, 
       Contact.Account.Owner.Email, Contact.Account.AM__r.Email, Contact.Name, Contact.Account.Name, Contact.Account.Type
        from Campaignmember where Run_First_Most_Recent_Campaign_Sync__c = true'), 10);*/

global class CampaignTrackerBatch implements Database.Batchable<sObject>{
global final String Query;
global CampaignTrackerBatch(String q){Query=q;}

global Database.QueryLocator start(Database.BatchableContext BC){return Database.getQueryLocator(query);}

global void execute(Database.BatchableContext BC,List<Campaignmember> scope){

 /*List<Messaging.SingleEmailMessage> mails = new List<Messaging.SingleEmailMessage>();  
    
    List<Campaignmember> cmtoupdate = new List<Campaignmember>(); 
    
    List<Lead> assignlead = new List<Lead>();
 //for updating campaign members associated with leads.   
    for(Campaignmember cm : scope){
    
    if(cm.LeadId != null && cm.ContactId == null){
    
                
    if(cm.Lead.First_Campaign__c != null && cm.Lead.isconverted == false){
        cm.Lead.Campaign_Name__c = cm.Campaign.Name;
        cm.Lead.Most_Recent_Lead_Source__c = cm.Campaign.Type;
    

    update cm.Lead;
    
     }
     
     if(cm.Lead.First_Campaign__c == null && cm.Lead.isconverted == false){
         cm.Lead.First_Campaign__c = cm.Campaign.Name;
         cm.Lead.Campaign_Name__c = cm.Campaign.Name;
         cm.Lead.Most_Recent_Lead_Source__c = cm.Campaign.Type;
         cm.Lead.LeadSource = cm.Campaign.Type;
     
    update cm.Lead;
    
 }
 //run assignment rules
     if(cm.Lead.OwnerId == '00500000006olmF' && cm.Lead.Status == 'Eloqua Lead Scoring'){
         Database.DMLOptions dmo = new Database.DMLOptions();
         dmo.assignmentRuleHeader.useDefaultRule= true;
         cm.Lead.setOptions(dmo);
         assignlead.add(cm.Lead);
}
 
}
//if the compaign member is associated with a contact.
if(cm.ContactId != null){
     
     if(cm.Contact.First_Campaign__c != null){
     cm.Contact.Campaign_Name__c = cm.Campaign.Name;
     cm.Contact.Most_Recent_Campaign_Id__c = cm.CampaignId; 
     update cm.Contact; 
     
           Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
     
      
   if(cm.Contact.Account.AM__c != null && cm.Contact.Account.AM__r.Email != null){mail.setToAddresses(new String[] {cm.Contact.Account.AM__r.Email}); }
   
   else if(cm.Contact.Account.AM__c == null && cm.Contact.Account.CRM__c != null && cm.Contact.Account.CRM__r.Email != null){mail.setToAddresses(new String[] {cm.Contact.Account.CRM__r.Email}); }
   
   else if(cm.Contact.Account.AM__c == null && cm.Contact.Account.CRM__c == null && cm.Contact.Account.Owner.Email != null){mail.setToAddresses(new String[] {cm.Contact.Account.Owner.Email});}
   
   else {mail.setToAddresses(new String[] {'rachael.chipman@returnpath.com'});}
             
            
            mail.setSenderDisplayName('Salesforce Alerts');
            mail.setSubject('NOTICE: A contact or lead of yours has been added to an additional campaign');
            mail.setccAddresses(new String[] {'chris.kingdon@returnpath.com'});
            mail.setHTMLBody(
                '<p>A contact under an account related to you has been added to an additional campaign.' +
                '<p>Name: ' + cm.Contact.Name +
                '<br/>Account: ' + cm.Contact.Account.Name +
                '<br/>Account Type: ' + cm.Contact.Account.Type +
                '<br/>New campaign: ' + cm.Campaign.Name +
                '<p>Please review the contact record and follow up as appropriate: ' + 'https://ssl.salesforce.com/' + cm.ContactId);

     mails.add(mail);
     if (mails.size() > 0){Messaging.sendEmail(mails);}
     
//look to see if there is an opportunity that is associated to the contact and update the Most Recent field.
     try {
List<OpportunityContactRole> ocr = [select Id, OpportunityId from OpportunityContactRole where ContactId =:cm.ContactId];
           
    for(OpportunityContactRole ocrs : ocr){
                 
       List<Opportunity> o = [select Id, Name, Most_Recent_Campaign__c, isClosed from Opportunity where Id = :ocrs.OpportunityId];
                    
     
     for(Opportunity ops : o){
     
    if(ops.isClosed == false){
        ops.Most_Recent_Campaign__c = cm.Campaign.Name; 
        update ops;
        }
             
     
     } } }
     catch(DmlException e) {}
   
 }


        if(cm.Contact.First_Campaign__c == null ){
    cm.Contact.First_Campaign__c = cm.Campaign.Name; 
    cm.Contact.Campaign_Name__c = cm.Campaign.Name; 
    cm.Contact.First_Campaign_Id__c = cm.CampaignId; 
    update cm.Contact;
    }
 }





cm.Run_First_Most_Recent_Campaign_Sync__c = false;
cmtoupdate.add(cm);
}

update assignlead;

 

update cmtoupdate;*/

    
 
}

Global void finish(Database.BatchableContext BC){
//Send an email to the User after your batch completes
Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
String[] toAddresses = new String[] {'rachael.chipman@returnpath.com'};
mail.setToAddresses(toAddresses);
mail.setSubject('Campaign Tracker Batch Job is done');
mail.setPlainTextBody('Campaign Tracker Batch job processed');
Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
}
}