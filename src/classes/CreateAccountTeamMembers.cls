global class CreateAccountTeamMembers implements Database.Batchable<sObject>, Schedulable 
{
    global final String Query;
	Map<Id,String> failedaccountMap;
    public CreateAccountTeamMembers()
    {
        failedaccountMap = new Map<Id,String>();
        Query = 'Select Id, Name,AM__c, AM__r.isActive, CRM__c, CRM__r.isActive, ' +
                'APS_Client_Services__c, APS_Client_Services__r.IsActive, Inbox_Insight_Client_Services__c, ' 
                 + 'Original_Salesperson__c, Inbox_Insight_Client_Services__r.IsActive, Salesperson__c, Salesperson__r.IsActive,Salesperson__r.Name,'+
                'Regional_Director__c,CorpDev_Other__c, CorpDev_Other__r.IsActive,CorpDev_Other__r.Name,Technical_Account_Manager__c,Technical_Account_Manager__r.IsActive,'
                 + 'Collections_Team_Member__c, Channel_Partner_Rep__c from Account';
    }
    
    
    global Database.QueryLocator start(Database.BatchableContext BC)
    {
         return Database.getQueryLocator(Query);
    }

    global void execute(Database.BatchableContext BC, List<sObject> scope)
    {
        failedaccountMap  = AccountService.createAccountTeamMembersandInsert(scope);
    }
    
    global void finish(Database.BatchableContext BC)
    {
        //if(!failedaccountMap.isEmpty())
            AccountService.sendErrorEmail(failedaccountMap);
    }
    
     global void execute(SchedulableContext sc) {
      CreateAccountTeamMembers UPB = new CreateAccountTeamMembers(); 
      database.executebatch(UPB);
   }
}