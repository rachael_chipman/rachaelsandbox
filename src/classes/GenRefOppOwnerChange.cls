public without sharing class GenRefOppOwnerChange {
	
	public static void processTrigger(List<Opportunity> newOpportunities, Map<Id, Opportunity> oldOpps)
	{
		List<Opportunity> filteredOpps = filterOpportunitiesOnUpdate(newOpportunities, oldOpps);
		if(!filteredOpps.isEmpty())
		{
			processOpportunities(filteredOpps);
		}
	}
	
	public static void processOpportunities(List<Opportunity> filteredOpportunities)
	{
		Map<Id, ProcessInstanceWorkItem> oppIdToPiwi = oppToProcessInstanceWorkItemMap(filteredOpportunities);
		
		for(Opportunity opp : filteredOpportunities)
		{
			if(!oppIdToPiwi.isEmpty() && oppIdToPiwi.containsKey(opp.Id))
			{
				opp.OwnerId = oppIdToPiwi.get(opp.Id).ActorId;
				if(opp.StageName == 'Verbal Commitment' || opp.StageName == 'Negotiating Terms' || opp.StageName == 'Signature')
				{
					opp.Approval_Type__c = 'Finance Approval';
				}
				else if(opp.StageName == 'Establishing Value')
				{
					opp.Approval_Type__c = 'Legal Approval';
				}
				else
				{
					opp.Approval_Type__c = 'Not Applicable';
				}
			}
		}
	}
	
	public static List<Opportunity> filterOpportunitiesOnUpdate(List<Opportunity> newOpportunities, Map<Id, Opportunity> oldOpps)
	{
		List<Opportunity> filteredOpportunities = new List<Opportunity>();
		
		for(Opportunity opp : newOpportunities)
		{
			Opportunity oldOpp = oldOpps.get(opp.Id);
			
			if(opp.Update_Opp_Owner__c == true && oldOpp.Update_Opp_Owner__c == false)
			{
				filteredOpportunities.add(opp);
			}
		}
		return filteredOpportunities;
	}
	
	private static Map<Id, ProcessInstanceWorkItem> oppToProcessInstanceWorkItemMap (List<Opportunity> filteredOpportunities)
	{
		List<ProcessInstance> processInstancesWithWorkItems = [SELECT Id, TargetObjectId, (SELECT Id, ActorId, ProcessInstanceId FROM Workitems)
		FROM ProcessInstance WHERE TargetObjectId IN : filteredOpportunities];
		
		Map<Id, ProcessInstanceWorkItem> oppIdToProcessInstanceWorkItem = new Map<Id, ProcessInstanceWorkItem>();
		
		for(Opportunity opp : filteredOpportunities)
		{
			for(ProcessInstance processInstance : processInstancesWithWorkItems)
			{
				if(opp.Id == processInstance.TargetObjectId)
				{
					for(ProcessInstanceWorkItem piwi : processInstance.Workitems)
					{
						oppIdToProcessInstanceWorkItem.put(opp.Id, piwi);
					}
				}
			}
		}
		
		return oppIdToProcessInstanceWorkItem;
	}

}