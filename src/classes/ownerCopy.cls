@isTest
private class ownerCopy {

    Static Opportunity opp;
    

    static testMethod void myUnitTest_Insert(){
        Test.startTest();
        opp = new Opportunity( name = 'Tests 2', ownerId = TestUtils.SALES_USER.Id ,StageName = 'Negotiating',CloseDate =system.Today(),owner_copy__C = '00500000007Em7P');
        insert opp;
        Test.stopTest();
    }

    static testMethod void myUnitTest_Update() {
        
        opp = new Opportunity( name = 'Tests 2', ownerId = TestUtils.SALES_USER.Id ,StageName = 'Verbal Agreement',CloseDate = system.Today(),owner_copy__C = '005000000073t0V');
        insert opp;
        
        Test.startTest();
        update opp;
       

        Test.stopTest();
        
    }
}