@isTest (seeAllData=true)
Public class NuggetHomeView_Controller_Test{
    public static testMethod void testNuggetHomeView_Controller(){
    PageReference pageRef = Page.NuggetHomeView;
Test.setCurrentPageReference(pageRef);

User u = [select Id from User where UserRoleId = '00E000000072RTW' limit 1];

Nugget__c n = new Nugget__c(
OwnerId = u.Id,
Company_Name__c = 'test comp',
State__c = 'CA',
Country__c = 'United States',
Status__c = 'Assigned');

insert n;

Nugget__c n2 = new Nugget__c(
OwnerId = u.Id,
Company_Name__c = 'test comp',
State__c = 'CA',
Country__c = 'United States',
Status__c = 'Assigned');

insert n2;

Nugget__c n3 = new Nugget__c(
OwnerId = u.Id,
Company_Name__c = 'test comp',
State__c = 'CA',
Country__c = 'United States',
Status__c = 'Assigned');

insert n3;

//ystem.runAs(u){}

NuggetHomeView_Controller oPEE = new NuggetHomeView_Controller();   

oPEE.strID = n.Id;
oPEE.getHSnugs();
oPEE.getMRnugs();
oPEE.goto();

}}