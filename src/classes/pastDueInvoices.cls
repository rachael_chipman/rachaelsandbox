global class pastDueInvoices implements Schedulable{
    
    global void execute(SchedulableContext SC) {
    
    list<c2g__codaInvoice__c> DirInv =[SELECT Id, c2g__Account__c, c2g__PaymentStatus__c,Days_Past_Due__c FROM c2g__codaInvoice__c WHERE Days_Past_Due__c = 1 and c2g__PaymentStatus__c = 'Unpaid' and Partner_Account__c = null ];
        
        
        for(c2g__codaInvoice__c di: DirInv){
            di.Send_Past_Due_Notice__c = 'Send';
            
            update(DirInv);
        }
        
    }
    }
       /* Add new field
        Then have WFR action that marks the field as Sent*/