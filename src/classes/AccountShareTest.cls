@isTest
private with sharing class AccountShareTest {
    
    private static Account account;
    private static User user1;
    
    private static void setUpMethod()
    {
        account = testUtils.createAccount(testUtils.MARKETER_RECORD_TYPE.Id, false);
        user1 = [SELECT Id FROM User WHERE isActive = true and First_Approver__c != NULL LIMIT 1];
    }
    
    private static testMethod void testAccountShareOnInsert()
    {
        setUpMethod();
        
        test.startTest();
        account.Technical_Account_Manager__c = user1.Id;
        insert account;
        test.stopTest();
        
        Integer theNewAccountShareCount = [SELECT COUNT() FROM AccountShare WHERE userOrGroupId = :user1.Id and accountId = :account.Id];
        system.assert(theNewAccountShareCount > 0);
    }
    
    private static testMethod void testAccountShareOnUpdate()
    {
        setUpMethod();
        insert account;
        
        test.startTest();
        account.Technical_Account_Manager__c = user1.Id;
        update account;
        test.stopTest();
        
        Integer theNewAccountShareCount = [SELECT COUNT() FROM AccountShare WHERE userOrGroupId = :user1.Id and accountId = :account.Id];
        system.assert(theNewAccountShareCount > 0);
    }
    
    private static testMethod void testBulkAccountShareInsert()
    {
        setUpMethod();
        List<Account> insertTestAccounts = new List<Account>();
        List<Account> testAccounts = testUtils.createAccounts(200, testUtils.MARKETER_RECORD_TYPE.Id, false);
        for(Account testAccount : testAccounts)
        {
            testAccount.Technical_Account_Manager__c = user1.Id;
            insertTestAccounts.add(testAccount);
        }
        test.startTest();
        insert insertTestAccounts;
        test.stopTest();
        
        Integer theNewAccountShareCount = [SELECT COUNT() FROM AccountShare WHERE userOrGroupId = :user1.Id and accountId IN :insertTestAccounts];
        system.assertEquals(theNewAccountShareCount, 200, 'we expect there to be 200 account shares');

    }

}