@isTest (SeeAllData = true)
Public class NuggetConvert_Controller_Test{
    public static testMethod void testNuggetNuggetConvert_Controller1(){
    PageReference pageRef = Page.NuggetConvert;
Test.setCurrentPageReference(pageRef);

User u = [select Id from User where UserRoleId = '00E000000072RTW' limit 1];

Nugget__c n = new Nugget__c(
OwnerId = u.Id,
Company_Name__c = 'test nugget67845 comp',
State__c = 'CA',
Country__c = 'United States',
Status__c = 'Assigned',
City__c = 'LA');

insert n;

      
n.OwnerId = u.Id;
update n;

Account a = New Account(
Name = 'test Account');

insert a;

Lead l = new Lead(
LastName = 'testguy',
company = 'rptest');

insert l;

Nugget_Match__c nm1 = New Nugget_Match__c(
Nugget__c = n.Id,
Matching_Account__c = a.Id,
Merge_with_Nugget_Account__c = true);

insert nm1;

Nugget_Match__c nm2 = New Nugget_Match__c(
Nugget__c = n.Id,
Matching_Lead__c = l.Id,
Merge_with_Nugget_Account__c = true);

insert nm2;



ApexPages.StandardController sc = new ApexPages.standardController(n);
System.currentPagereference().getParameters().put('id',n.Id);
NuggetConvert_Controller oPEE = new NuggetConvert_Controller(sc);   

oPEE.getnm();
oPEE.getanm();
oPEE.convert();
oPEE.cancel();
oPEE.createacct();

}

    public static testMethod void testNuggetNuggetConvert_Controller2(){
    PageReference pageRef = Page.NuggetConvert;
Test.setCurrentPageReference(pageRef);

User u = [select Id from User where UserRoleId = '00E000000072RTW' limit 1];

Nugget__c n = new Nugget__c(
OwnerId = u.Id,
Company_Name__c = 'test comp',
State__c = 'CA',
Country__c = 'US',
Status__c = 'Assigned',
City__c = 'LA');

insert n;

      
n.OwnerId = u.Id;
update n;

Account a = New Account(
Name = 'test Account');

insert a;

Lead l = new Lead(
LastName = 'testguy',
company = 'rptest');

insert l;

Nugget_Match__c nm1 = New Nugget_Match__c(
Nugget__c = n.Id,
Matching_Account__c = a.Id);

insert nm1;





ApexPages.StandardController sc = new ApexPages.standardController(n);
System.currentPagereference().getParameters().put('id',n.Id);
NuggetConvert_Controller oPEE = new NuggetConvert_Controller(sc);   

oPEE.getnm();
oPEE.getanm();
oPEE.convert();
oPEE.cancel();
oPEE.createacct();

}

    public static testMethod void testNuggetNuggetConvert_Controller3(){
    PageReference pageRef = Page.NuggetConvert;
Test.setCurrentPageReference(pageRef);

User u = [select Id from User where UserRoleId = '00E000000072RTW' limit 1];

Nugget__c n = new Nugget__c(
OwnerId = u.Id,
Company_Name__c = 'test comp',
State__c = 'CA',
Country__c = 'United States',
Status__c = 'Assigned');

insert n;

      
n.OwnerId = u.Id;
update n;

Account a = New Account(
Name = 'test Account');

insert a;

Lead l = new Lead(
LastName = 'testguy',
company = 'rptest');

insert l;

Nugget_Match__c nm1 = New Nugget_Match__c(
Nugget__c = n.Id,
Matching_Lead__c = l.Id,
Merge_with_Nugget_Account__c = true);

insert nm1;





ApexPages.StandardController sc = new ApexPages.standardController(n);
System.currentPagereference().getParameters().put('id',n.Id);
NuggetConvert_Controller oPEE = new NuggetConvert_Controller(sc);   

oPEE.getnm();
oPEE.getanm();
oPEE.convert();
oPEE.cancel();
oPEE.createacct();

}

    public static testMethod void testNuggetNuggetConvert_Controller4(){
    PageReference pageRef = Page.NuggetConvert;
Test.setCurrentPageReference(pageRef);

User u = [select Id from User where UserRoleId = '00E000000072RTW' limit 1];

Nugget__c n = new Nugget__c(
OwnerId = u.Id,
Company_Name__c = 'test comp',
State__c = 'CA',
Country__c = 'United States',
Status__c = 'Assigned');

insert n;

      
n.OwnerId = u.Id;
update n;

Account a = New Account(
Name = 'test Account');

insert a;

Account a2 = New Account(
Name = 'test Account');

insert a2;

Lead l = new Lead(
LastName = 'testguy',
company = 'rptest');

insert l;

Nugget_Match__c nm1 = New Nugget_Match__c(
Nugget__c = n.Id,
Matching_Account__c = a.Id,
Merge_with_Nugget_Account__c = true);

insert nm1;

Nugget_Match__c nm2 = New Nugget_Match__c(
Nugget__c = n.Id,
Matching_Account__c = a2.Id,
Merge_with_Nugget_Account__c = true);

insert nm2;





ApexPages.StandardController sc = new ApexPages.standardController(n);
System.currentPagereference().getParameters().put('id',n.Id);
NuggetConvert_Controller oPEE = new NuggetConvert_Controller(sc);   

oPEE.getnm();
oPEE.getanm();
oPEE.convert();
oPEE.cancel();
oPEE.createacct();

}

    public static testMethod void testNuggetNuggetConvert_Controller5(){
    PageReference pageRef = Page.NuggetConvert;
Test.setCurrentPageReference(pageRef);

User u = [select Id from User where UserRoleId = '00E000000072RTW' limit 1];

Nugget__c n = new Nugget__c(
OwnerId = u.Id,
Company_Name__c = 'test comp',
State__c = 'CA',
Country__c = 'United States',
Status__c = 'Assigned');

insert n;

      
n.OwnerId = u.Id;
update n;

Account a = New Account(
Name = 'test Account');

insert a;

Account a2 = New Account(
Name = 'test Account');

insert a2;

Lead l = new Lead(
LastName = 'testguy',
company = 'rptest');

insert l;

Nugget_Match__c nm1 = New Nugget_Match__c(
Nugget__c = n.Id,
Matching_Account__c = a.Id);

insert nm1;

Nugget_Match__c nm2 = New Nugget_Match__c(
Nugget__c = n.Id,
Matching_Account__c = a2.Id,
Merge_with_Nugget_Account__c = true);

insert nm2;





ApexPages.StandardController sc = new ApexPages.standardController(n);
System.currentPagereference().getParameters().put('id',n.Id);
NuggetConvert_Controller oPEE = new NuggetConvert_Controller(sc);   

oPEE.getnm();
oPEE.getanm();
oPEE.convert();
oPEE.cancel();
oPEE.createacct();

}

    public static testMethod void testNuggetNuggetConvert_Controller6(){
    PageReference pageRef = Page.NuggetConvert;
Test.setCurrentPageReference(pageRef);

User u = [select Id from User where UserRoleId = '00E000000072RTW' limit 1];

Nugget__c n = new Nugget__c(
OwnerId = u.Id,
Company_Name__c = 'test comp',
State__c = 'CA',
Country__c = 'United States',
Status__c = 'Assigned');

insert n;

      
n.OwnerId = u.Id;
update n;

Account a = New Account(
Name = 'test Account');

insert a;

Account a2 = New Account(
Name = 'test Account');

insert a2;

Lead l = new Lead(
LastName = 'testguy',
company = 'rptest');

insert l;

Nugget_Match__c nm1 = New Nugget_Match__c(
Nugget__c = n.Id,
Matching_Lead__c = l.Id,
Merge_with_Nugget_Account__c = true);

insert nm1;

Nugget_Match__c nm2 = New Nugget_Match__c(
Nugget__c = n.Id,
Matching_Account__c = a.Id);

insert nm2;




ApexPages.StandardController sc = new ApexPages.standardController(n);
System.currentPagereference().getParameters().put('id',n.Id);
NuggetConvert_Controller oPEE = new NuggetConvert_Controller(sc);   

oPEE.getnm();
oPEE.getanm();
oPEE.convert();
oPEE.cancel();
oPEE.createacct();

}


    public static testMethod void testNuggetNuggetConvert_Controller7(){
    PageReference pageRef = Page.NuggetConvert;
Test.setCurrentPageReference(pageRef);

User u = [select Id from User where UserRoleId = '00E000000072RTW' limit 1];

Nugget__c n = new Nugget__c(
OwnerId = u.Id,
Company_Name__c = 'test comp',
State__c = 'CA',
Country__c = 'United States',
Status__c = 'Assigned');

insert n;

      
n.OwnerId = u.Id;
update n;


ApexPages.StandardController sc = new ApexPages.standardController(n);
System.currentPagereference().getParameters().put('id',n.Id);
NuggetConvert_Controller oPEE = new NuggetConvert_Controller(sc);   

oPEE.getnm();
oPEE.getanm();
oPEE.convert();
oPEE.cancel();
oPEE.createacct();

}
	public static final String SEARCH_INPUT = 'TestNugget';
	public static Nugget__c testNugget;
	public static Account expectedAccount;
	public static Account account;
	public static Lead lead;
	public static NuggetConvert_Controller nuggetController;
	
	private static void setUpMethod()
	{
        
        String companyname = 'Test Nugget5678928 Company';
		testNugget = testUtils.createNuggets(1, companyname, null, true)[0];

		expectedAccount = testUtils.createAccount(testUtils.MARKETER_RECORD_TYPE.id, false);
		expectedAccount.Name = SEARCH_INPUT+'Account';
		
		
		account = testUtils.createAccount(testUtils.MARKETER_RECORD_TYPE.id, false);
		account.Name = 'Account';
		
		insert new List<Account> {expectedAccount, account};
		
		lead = testUtils.createLeads(SEARCH_INPUT+'Company', 'TestLead', 'In Progress', true);
		
		ApexPages.StandardController std = new ApexPages.standardController(testNugget);
		System.currentPageReference().getParameters().put('id',testNugget.Id); 
		nuggetController = new NuggetConvert_Controller(std);
	}
	
	private static testMethod void testFindTestMatches()
	{
		setUpMethod();
		
		test.startTest();
		nuggetController.searchInput = SEARCH_INPUT; 
		nuggetController.find();
		test.stopTest();
		
		//system.assertEquals(1, nuggetController.searchAccounts.size(), 'We expect 1 Account result');
		system.assertEquals(expectedAccount.id, nuggetController.searchAccounts[0].anAcct.Id, 'We expect the searchAccount to be the expectedAccount');
		system.assertEquals(1, nuggetController.searchLeads.size(), 'We expect 1 Lead result');
		system.assertEquals(lead.Id, nuggetController.searchLeads[0].aLead.Id, 'We expect the searchLead to be the lead we inserted');
	}
	
	private static testMethod void testConvertAccountMatches()
	{
		setUpMethod();
		nuggetController.searchInput = SEARCH_INPUT;
		nuggetController.find();
		
		nuggetController.searchAccounts[0].isSelected = true;
		
		test.startTest();
		pageReference pageRef = nuggetController.convertSearchResults();
		test.stopTest();
		
		system.assertEquals(0, ApexPages.getMessages().size(), 'We expect that there are no errors');
		system.assert(pageRef.getURL().contains(expectedAccount.Id));
		
		testNugget = [SELECT Id, Status__c, Promoted_Account__c FROM Nugget__c WHERE Id =: testNugget.Id];
		system.assertEquals('Promoted to Account', testNugget.Status__c, 'We expect the status to be updated');
		system.assertEquals(expectedAccount.Id, testNugget.Promoted_Account__c, 'We expect the correct promoted account ot be populated.');
		
		//system.assertEquals(2, [SELECT Id FROM Account where Id =:expectedAccount.Id OR Id =:account.Id].size(), 'We expect there are only 2 accounts');
	}
	
	private static testMethod void testConvertLeadMatches()
	{
		setUpMethod();
		nuggetController.searchInput = SEARCH_INPUT;
		nuggetController.find();
		
		nuggetController.searchLeads[0].isSelected = true;
		
		test.startTest();
		pageReference pageRef = nuggetController.convertSearchResults();
		test.stopTest();
		
		system.assertEquals(0, ApexPages.getMessages().size(), 'We expect that there are no errors');
		
		testNugget = [SELECT Id,Company_Name__c, Status__c, Promoted_Account__c, D_U_N_S_Number__c, City__c, State__c, Country__c, OwnerId  FROM Nugget__c WHERE Id =: testNugget.Id];
		system.assertEquals('Promoted to Account', testNugget.Status__c, 'We expect the status to be updated');
		
		
		//system.assertEquals(3, [SELECT Id FROM Account where Id =:expectedAccount.Id OR Id =:account.Id OR  name =:testNugget.Company_Name__c].size(), 'We expect there are now 3 accounts');
		
		Account convertedAccount = [SELECT Id, DunsNumber, BillingCity, BillingState, BillingCountry, Salesperson__c FROM Account WHERE name =:testNugget.Company_Name__c /*Id != :expectedAccount.Id and Id != :account.Id*/];
		system.assertEquals(convertedAccount.DunsNumber, testNugget.D_U_N_S_Number__c, 'We expect the DUNS numbers to match');
		system.assertEquals(convertedAccount.BillingCity, testNugget.City__c, 'We expect the city to match');
		system.assertEquals(convertedAccount.BillingState, testNugget.State__c, 'We expect the State to match');
		system.assertEquals(convertedAccount.BillingCountry, testNugget.Country__c, 'We expect the country to match');
		system.assertEquals(convertedAccount.Salesperson__c, testNugget.OwnerId, 'We expect the nugget owner to be the Salesperson'); 
		system.assert(pageRef.getURL().contains(convertedAccount.Id));
		system.assertEquals(convertedAccount.Id, testNugget.Promoted_Account__c, 'We expect the correct promoted account ot be populated.');
	}
}