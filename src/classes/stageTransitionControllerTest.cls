@isTest
private class stageTransitionControllerTest {
	
	static Account testAccount;
	static Opportunity testOpportunity;
	static stageTransitionController testController;
	static FINAL String BUTTON_TEXT = 'Submit Loss Reasons';
	static FINAL String LOST_STAGE = 'Lost';

	static void setup()
	{
		testAccount = TestUtils.createAccount( TestUtils.MARKETER_RECORD_TYPE.Id, true );
		testOpportunity = TestUtils.createOpportunity( TestUtils.SELECT_RECORD_TYPE.Id, testAccount.Id, false );
		insert testOpportunity;
	}

	private static testMethod void testStageTransitionController_Constructor()
	{
		setup();
		PageReference p = Page.stageTransition;
		p.getParameters().put( 'lost','true' );
		p.getParameters().put( 'id', testOpportunity.Id );
		Test.setCurrentPage( p );
		
		Test.startTest();
		
			ApexPages.StandardController stdController = new ApexPages.StandardController( testOpportunity );
			testController = new stageTransitionController( stdController );
		
		Test.stopTest();
		
		System.assertEquals( BUTTON_TEXT, testController.buttonText, 'Text should be for lost opp' );
	}
	
	private static testMethod void testSubmit()
	{
		setup();
		PageReference p = Page.stageTransition;
		p.getParameters().put( 'lost','true' );
		p.getParameters().put( 'id', testOpportunity.Id );
		Test.setCurrentPage( p );
		ApexPages.StandardController stdController = new ApexPages.StandardController( testOpportunity );
		testController = new stageTransitionController( stdController );
			
		Test.startTest();
		
			testController.submit();
			
		Test.stopTest();

		Opportunity lostOpp = [SELECT Id, StageName, CloseDate, Record_Locked__c FROM Opportunity WHERE Id = : testOpportunity.Id];

		System.assertEquals( LOST_STAGE, lostOpp.StageName, 'The opp stage should be Lost' );
		System.assertEquals( true, lostOpp.Record_Locked__c, 'Record should be locked');
		System.assertEquals( system.today(), lostOpp.CloseDate, 'Close Date should be today');
	}
}