@isTest

public class CA_Welcome_Email_Tests {
	
     static testMethod void CA_Welcome_Email_Test1() 
 	{		
		Account acct = new Account();
        acct.name = 'RPTestAcct1';
        acct.accountmanager__c = 'BIO Automation';
        Insert acct;
		        
		Contact con = new Contact();
        con.AccountId = acct.Id;
        con.LastName = 'Test';
        con.FirstName = 'Trigger';
        con.ContactsRole__c = 'Certification Daily Report Recipient';
        con.Email = 'test@returnpath.com';
        Insert Con;
		        
		Certification_Application__c ca = New Certification_Application__c();
        ca.Account__c = acct.Id;
        insert ca;      
     	
     	test.startTest();   
        ca.Activation_Date__c = system.today();
        update ca;
        test.stopTest();
        
        List<Contact> updatedContacts = [SELECT Id, ContactsRole__c, Certification_Welcome_Program__c FROM Contact WHERE Id = :con.Id];
        system.assertEquals(true, updatedContacts[0].Certification_Welcome_Program__c, 'The email program checkbox should be marked as true');
    }   
}