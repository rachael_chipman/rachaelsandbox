public with sharing class OverageFeeGenerationServices {
	
	public static final String ACTIVE = 'Active';
	public static final String ZSUB_QUERY = 'select Id, Name, Version, AccountId, OriginalId, ContractEffectiveDate, AutoRenew, InitialTerm, TermStartDate, TermEndDate, TermType, RenewalTerm from subscription where Id = ';
	public static final String ZACC_QUERY = 'select Id, BillToId, SoldToId, Currency from account where Id = ';
	public static final String TERMED = 'Termed';
	public static Zuora.zObject subObject{get;set;}
    public static Zuora.zObject accObject{get;set;}
    public static String qCurrency = 'USD';
	public static String keyString
	{
		get
		{
			if(keyString == NULL)
			{
				keyString = String.valueOf(system.now()) + '_Overage_Fee_for_Charge: ';
			}
			return keyString;
		}
		private set;
	}

	public static User SF_AUTO
	{
		get
		{
			if(SF_AUTO == NULL)
			{
				SF_AUTO = [SELECT Id FROM User WHERE Name = 'Salesforce Automation' and isActive = true];
			}
			return SF_AUTO;
		}
		private set;
	}

	public static RecordType amendmentQuote
	{
		get
		{
			if(amendmentQuote == NULL)
			{
				amendmentQuote = [SELECT Id FROM RecordType WHERE DeveloperName = 'Amendment' AND SObjectType = 'zqu__Quote__c'];
			}
			//system.debug('What is the RT Id: ' + amendmentQuote.Id);
			return amendmentQuote;
		}
		private set;
	}

	private static Zuora.zApi api 
 	{
	   get 
	   {
	     if (api == null) 
	     {
     	api = new Zuora.zApi();
	     }
	     return api;
	   }
	   set;
 	}

 	public static void callToZuora()
 	{
 		Zuora.zApi.LoginResult result = api.zlogin();
 		system.debug('login result: ' + result);
 	}
	
	public static Map<Id, Zuora__SubscriptionProductCharge__c> getExistingSubscriptionCharge(Set<Id> accountIds, Set<String> chargeNames)
	{
		Map<Id, Zuora__SubscriptionProductCharge__c> accountToSubCharge = new Map<Id, Zuora__SubscriptionProductCharge__c>();

		if(chargeNames.contains(CertificationOverageGeneration.CERT))
		{
			chargeNames.clear();
			chargeNames.add('Certification - License%');
		}

		List<Zuora__SubscriptionProductCharge__c> existingCharges = [SELECT Id, Name, Zuora__Account__c, Zuora__EffectiveEndDate__c, Zuora__EffectiveStartDate__c,
		Zuora__MonthlyRecurringRevenue__c, Product_Family__c, Zuora__Quantity__c, Zuora__Subscription__c, Zuora__Subscription__r.Zuora__Status__c, Zuora__Subscription__r.Zuora__Account__c, Zuora__Account__r.Name,
		Zuora__Subscription__r.Zuora__CustomerAccount__c, Zuora__Account__r.OwnerId, Zuora__Subscription__r.Zuora__Account__r.Owner.isActive, Zuora__Subscription__r.RealmId__c, Zuora__Price__c, IncludedUnits__c,
		Zuora__Subscription__r.CurrencyIsoCode, Zuora__Subscription__r.Zuora__Version__c, Zuora__Subscription__r.Zuora__Zuora_Id__c, Zuora__Subscription__r.Zuora__CustomerAccount__r.Zuora__Zuora_Id__c, OveragePrice__c
		FROM Zuora__SubscriptionProductCharge__c WHERE Name LIKE : chargeNames AND Zuora__Account__c IN : accountIds AND Zuora__Subscription__r.Zuora__Status__c = : ACTIVE];

		for(Zuora__SubscriptionProductCharge__c charge : existingCharges)
		{
			accountToSubCharge.put(charge.Zuora__Account__c, charge);
		}
		system.debug('Do I have a charge?' + accountToSubCharge);
		return accountToSubCharge;
	}

	public static Opportunity createOverageOpp (Zuora__SubscriptionProductCharge__c existingCharge)
	{
		DateTime dt = system.now();
        String monthAndYear = dt.format('MMM') + '-' + dt.format('YY');
        String accountName;
        if(existingCharge.Zuora__Account__r.Name.length() >= 26)
        {
            accountName = existingCharge.Zuora__Account__r.Name.substring(0,26);
        }
        else
        {
            accountName = existingCharge.Zuora__Account__r.Name;
        }

        String chargeName;
        if(existingCharge.Name.length() >= 23)
        {
        	chargeName = existingCharge.Name.substring(0,23);
        }
        else
        {
        	chargeName = existingCharge.Name;
        }

		Opportunity overageOpp = new Opportunity();
		overageOpp.AccountId = existingCharge.Zuora__Account__c;
		if( existingCharge.Zuora__Subscription__r.Zuora__Account__r.Owner.isActive )
		{
			overageOpp.OwnerId = existingCharge.Zuora__Subscription__r.Zuora__Account__r.OwnerId;
		}
		else overageOpp.OwnerId = SF_AUTO.Id;
		overageOpp.CurrencyIsoCode = existingCharge.Zuora__Subscription__r.CurrencyIsoCode;
		overageOpp.Type = 'Fee';
        overageOpp.StageName = 'Defining Needs';
        overageOpp.RecordTypeId = TestUtils.SELECT_RECORD_TYPE.Id;
        overageOpp.Did_not_Fill_Pre_Pipeline_Exit_Criteria__c = true;
		overageOpp.Approval_Type__c = 'Not Applicable';
		overageOpp.Name = accountName + '_' + chargeName + '_Overage Fee_' + monthAndYear;
		overageOpp.CloseDate = system.today();
		return overageOpp;
	}
	
	public class OverageFeeOpportunity
	{
		public Opportunity opp{get; private set;}
		public Zuora__SubscriptionProductCharge__c existingCharge{get;private set;}
		public zqu__Quote__c feeQuote{get;private set;}
		public String chargeName{get;private set;}
		
		
		public OverageFeeOpportunity( Opportunity opp, Zuora__SubscriptionProductCharge__c existingCharge, Zqu__Quote__c feeQuote, String chargeName)
		{
			this.opp = opp;
			this.existingCharge = existingCharge;
			this.feeQuote = feeQuote;
			this.chargeName = chargeName;
		}		
	}
	public static void saveOverageFeeOpportunities( list<OverageFeeOpportunity> feeOpps)
	{
     	List<String> billingaccountIds = new List<String>();
     	List<String> subscriptionIds = new List<String>();
     	Map<String, Zuora.zObject> acczobjsMap = new Map<String, Zuora.zObject>();
     	Map<String, Zuora.zObject> subobjsMap = new Map<String, Zuora.zObject>();
     	subObject = new Zuora.zObject('Subscription');
     	accObject = new Zuora.zObject('Account');
     	Map<Id, Opportunity> oppIdToOpp = new Map<Id, Opportunity>();

		
		for( OverageFeeOpportunity feeOpp : feeOpps )
		{
			system.debug('What is the feeOpp.opp? ' + feeOpp.opp + ' What is the Customer Account Zuora Id: ' + feeOpp.existingCharge.Zuora__Subscription__r.Zuora__CustomerAccount__r.Zuora__Zuora_Id__c);
			billingaccountIds.add(feeOpp.existingCharge.Zuora__Subscription__r.Zuora__CustomerAccount__r.Zuora__Zuora_Id__c);
			subscriptionIds.add(feeOpp.existingCharge.Zuora__Subscription__r.Zuora__Zuora_Id__c);
		}
		//Savepoint sp = Database.setSavepoint();
		try
		{
			if(!Test.isRunningTest())
			{
				callToZuora();

				acczobjsMap = performZOQLQuery(billingaccountIds, ZACC_QUERY);
				system.debug('acczobjsMap = ' + acczobjsMap);
				subobjsMap = performZOQLQuery(subscriptionIds, ZSUB_QUERY);
			}
			else
			{
				//system.debug('Am I making it in here');
				
				subObject.setValue('Id', 'FZnVCoj2UMG1nNlEy7rQQ6MNbcGKpVto');
				subObject.setValue('TermStartDate', system.now());
				subObject.setValue('AutoRenew', true);
				subObject.setValue('InitialTerm', 12);
				subObject.setValue('RenewalTerm', 12);
				subObject.setValue('TermStartDate', system.now());
				subObject.setValue('TermEndDate', system.now().addDays(365));
				subObject.setValue('TermType', 'TERMED');
				subObject.setValue('Name', 'Subscription Name');
				subObject.setValue('Version', 1);
				
				subobjsMap.put((String) subObject.getValue('Id'), subObject);

				accObject.setValue('Id', 'vkXwBQSvPn6hVfFoCDxMNWQCnmxdS64O');
				accObject.setValue('Currency', qCurrency);

				acczobjsMap.put((String) accObject.getValue('Id'), accObject);
			}
			
     	}
     	catch (Exception e)
     	{
     		//throw new zqu.ZQException(e.getMessage());
     		system.debug('Error with callout: ' + e.getMessage() + e.getCause());
     	}

		list<Opportunity> feeOpportunities = new list<Opportunity>();
		
		for( OverageFeeOpportunity feeOpp : feeOpps )
		{
			Zuora.zObject acczobj;
			system.debug('What is the feeOpp.opp? ' + feeOpp.opp);
			if(!test.isRunningTest())
			{
				acczobj = acczobjsMap.get(feeOpp.existingCharge.Zuora__Subscription__r.Zuora__CustomerAccount__r.Zuora__Zuora_Id__c);
				feeOpp.opp.put('CurrencyIsoCode', (String) acczobj.getValue('Currency'));
			}
			else
			{
				feeOpp.opp.CurrencyIsoCode = qCurrency;
			}
			feeOpportunities.add( feeOpp.opp );
		}

		Savepoint sp = Database.setSavepoint();
		
		insert feeOpportunities;
		system.debug('Do I have any opps?' + feeOpportunities);
		for(Opportunity opp : feeOpportunities)
		{
			oppIdToOpp.put(opp.Id, opp);
		}

		List<zqu__Quote__c> feeQuotes = new List<zqu__Quote__c>();
		
		for( OverageFeeOpportunity feeOpp : feeOpps )
		{
			zqu__Quote__c newQuote = feeOpp.feeQuote;
			newQuote.OwnerId = feeOpp.opp.OwnerId;
			system.debug('What is the opp Id? ' + feeOpp.opp.Id);
			Zuora.zObject subobj;
			Zuora.zObject acczobj;
			newQuote.Overage_Key__c = keyString + feeOpp.chargeName;
			newQuote.Name = feeOpp.opp.Name;
			system.debug('What is the overage key? ' +  feeOpp.feeQuote.Overage_Key__c);

			if(!Test.isRunningTest())
			{
				subobj = subobjsMap.get(feeOpp.existingCharge.Zuora__Subscription__r.Zuora__Zuora_Id__c);
				acczobj = acczobjsMap.get(feeOpp.existingCharge.Zuora__Subscription__r.Zuora__CustomerAccount__r.Zuora__Zuora_Id__c);
			}
			else
			{
				subobj = subobjsMap.get((String) subObject.getValue('Id'));
				system.debug('Do i have a subObj' + subobj);
				acczobj = acczobjsMap.get((String) accObject.getValue('Id'));
				system.debug('Do I have an accobj' + acczobj);
			}
			//if(oppIdToOpp.containsKey(feeOpp.opp.Id))
			//{
				newQuote.put('zqu__Opportunity__c', feeOpp.opp.Id);
			//}
			//Set up something for the start date, build a variable that is the start of the previous month, 
			//then check to make sure that date is not before the term start date and make sure it
			//is also not after the term end date
			Date startDate = system.today().addMonths(-1).toStartOfMonth();
			if(acczobj != NULL)
			{
				newQuote.put('zqu__ZuoraAccountID__c', (String) acczobj.getValue('Id'));
				newQuote.put('zqu__Currency__c', (String) acczobj.getValue('Currency'));
			}
			if(subobj != NULL)
			{
				newQuote.put('zqu__ExistSubscriptionID__c', (String) subobj.getValue('Id'));
				/*if((subobj.getValue('TermEndDate')) != NULL)
				{
					if(((Datetime) subobj.getValue('TermEndDate')).date() >= system.today().toStartOfMonth())
					{
						newQuote.put('zqu__StartDate__c', system.today().toStartOfMonth());
					}
					else 
					{
						newQuote.put('zqu__StartDate__c', ((Datetime) subobj.getValue('TermEndDate')).date().addMonths(-1).toStartOfMonth());
					}
				}
				else
				{
					newQuote.put('zqu__StartDate__c', system.today().toStartOfMonth());
				}*/
				if(((String)subobj.getValue('TermType')) == TERMED)
				{
	         		newQuote.put('zqu__SubscriptionTermEndDate__c', ((Datetime) subobj.getValue('TermEndDate')).date().addDays(1));
					if(((Datetime) subobj.getValue('TermEndDate')) >= startDate && ((Datetime) subobj.getValue('TermStartDate')) <= startDate)
					{
						newQuote.put('zqu__StartDate__c', startDate);
					}
					else if(((Datetime)subobj.getValue('TermStartDate')) > startDate)
					{
						//If the subscription term start is AFTER the startDate variable (1st day of previous month (month previous to 'today'))
						//then set the quote start date to the first day of the month AFTER the term start date
						newQuote.put('zqu__StartDate__c', ((Datetime) subobj.getValue('TermStartDate')).date().addMonths(1).toStartOfMonth());
					}
					else if(((Datetime)subobj.getValue('TermEndDate')) < startDate)
					{
						//If the subscription end date is BEFORE the startDate variable(1st day of previous month (month previous to 'today'))
						//then set the quote start date to the first day of the month BEFORE the term end date
						newQuote.put('zqu__StartDate__c', ((Datetime) subobj.getValue('TermEndDate')).date().addMonths(-1).toStartOfMonth());
					}
				}
				else
				{
					newQuote.put('zqu__StartDate__c', startDate);
				}
				newQuote.put('zqu__AutoRenew__c', (Boolean) subobj.getValue('AutoRenew'));
				newQuote.put('zqu__InitialTerm__c', (Integer) subobj.getValue('InitialTerm'));
				newQuote.put('zqu__RenewalTerm__c', (Integer) subobj.getValue('RenewalTerm'));
				newQuote.put('zqu__SubscriptionTermStartDate__c', ((Datetime) subobj.getValue('TermStartDate')).date().addDays(1));
	         	newQuote.put('zqu__Subscription_Term_Type__c', (String) subobj.getValue('TermType'));
	         	newQuote.put('zqu__Hidden_Subscription_Name__c', (String) subobj.getValue('Name'));
	         	newQuote.put('zqu__SubscriptionVersion__c', (Integer) subobj.getValue('Version'));
	        }
			feeQuotes.add(newQuote);
		}
		try
		{
			//system.debug('What quotes are we inserting?'+ feeQuotes);
			insert feeQuotes;
           /* List<zqu__Quote__c> quotelist = [Select RecordTypeId, Id , zqu__Opportunity__c, zqu__ZuoraAccountID__c,zqu__Account__c from zqu__Quote__c where Id in:feeQuotes];
            for(zqu__Quote__c Tquote : quotelist)
            {
                System.debug('Record Type when inserted is : and opp Id is  ' + Tquote.RecordTypeId  + ' ' +Tquote.zqu__Opportunity__c + ' ' + Tquote.zqu__Account__c + ' ' +Tquote.zqu__ZuoraAccountID__c);
            }*/
		}
		catch ( DMLException e )
		{
			for( Integer i = 0; i < e.getNumDml(); i++ )
			{
				//system.assert(false, 'Quote insert is failing: ' + e.getDmlMessage( i ) );
				/*for( Accreditation_Volume__c triggerAccVol : newAccVols )
				{
					triggerAccVol.addError( e.getDmlMessage( i ) );
				}*/
			}
			Database.rollback( sp );
		}
	}


	//------------------------------------------------ADDING ZUORA COMPONENTS ------------------------------------------------------

	public class QuoteAndAttachments
	{
		public zqu__Quote__c quote{get; private set;}
		public zqu__QuoteAmendment__c amendment{get; private set;}
		public zqu__QuoteRatePlan__c ratePlan{get; private set;}
		public zqu__QuoteRatePlanCharge__c ratePlanCharge{get; private set;}
		public zqu__QuoteChargeSummary__c chargeSummary{get; private set;}
		public zqu__QuoteChargeDetail__c chargeDetail{get;private set;}
		public Decimal totalVol{get;private set;}
		public Decimal runRate{get;private set;}
		public Decimal maxVol{get;private set;}
		public Integer eventsUsed{get;private set;}
		public Integer includedUnits{get;private set;}
		public Decimal price{get; private set;}

		public QuoteAndAttachments( zqu__Quote__c quote, zqu__QuoteAmendment__c amendment, zqu__QuoteRatePlan__c ratePlan, zqu__QuoteRatePlanCharge__c ratePlanCharge, zqu__QuoteChargeSummary__c chargeSummary, zqu__QuoteChargeDetail__c chargeDetail, Decimal totalVol, Decimal runRate, Decimal maxVol, Integer eventsUsed, Integer includedUnits, Decimal price)
		{
			this.quote = quote;
			this.amendment = amendment;
			this.ratePlan = ratePlan;
			this.ratePlanCharge = ratePlanCharge;
			this.chargeSummary = chargeSummary;
			this.chargeDetail = chargedetail;
			this.totalVol = totalVol;
			this.runRate = runRate;
			this.maxVol = maxVol;
			this.eventsUsed = eventsUsed;
			this.includedUnits = includedUnits;
			this.price = price;
		}
	}

	private static Map<String, Zuora.zObject> performZOQLQuery(List<String> zObjectIds, String queryString)
	{
		Integer numberOfIds = zObjectIds.size();
		List<List<String>> allIds = new list<List<String>>();
		Integer plusOne = math.mod(numberOfIds, 200) == 0 ? 0 : 1;
		Integer numOfQueries = (numberOfIds /  200) + plusOne;
		
		for(Integer i = 0; i < numOfQueries; i++)
		{
			Integer startIndex = i * 200;
			Integer endIndex = (i +1) * 200;

			if(endIndex > = numberOfIds)
			{
				endIndex = numberOfIds;
			}

			List<String> subList = new List<String>();
			for(Integer j = startIndex; j < endIndex; j++)
			{
				subList.add(zObjectIds[j]);
			}
			allIds.add(subList);
		}

		List <Zuora.zObject > zobjsReturned = new List<Zuora.zObject>();
		for(List<STring> subList : allIds)
		{
			String query = '';
			for(String zObjId : subList)
			{
				query +=  '\'' + zObjId + '\'' + ' or Id = ';
			}
			query = query.substring(0, query.length() -9);
			system.debug('Show the query String ' + query);
			String zoqlStatement = queryString + query;
	   		system.debug('ZOQL query ' + zoqlStatement);
	   		zobjsReturned.addAll(api.zquery(zoqlStatement));//This is a query inside a for loop!!!
	   	}

   	    Map<String, Zuora.zObject> idStringToZObject = new Map<String, Zuora.zObject>();

	    for(Zuora.zObject zObj : zobjsReturned)
	    {
	   		idStringToZObject.put((String) zObj.getValue('Id'), zObj);
	    }
	  
	   return idStringToZObject;
	}

	public static zqu__Quote__c createQuote(Zuora__SubscriptionProductCharge__c existingCharge)
	{
		zqu__Quote__c amend = new zqu__Quote__c();
		amend.zqu__Is_Charge_Expired__c = false;
		amend.zqu__Account__c = existingCharge.Zuora__Account__c;
		amend.RealmId__c = existingCharge.Zuora__Subscription__r.RealmId__c;
		//amend.Name = existingCharge.Name + ' Usage Limit Exceeded';
		amend.RecordTypeId = amendmentQuote.Id;
		amend.zqu__Amendment_Name__c = existingCharge.Name + ' - Usage Limit Exceeded';
		amend.Is_Primary__c = true;
		//amend.zqu__StartDate__c = system.today().addMonths(-1).toStartOfMonth();
		amend.zqu__SubscriptionType__c = 'Amend Subscription';
		return amend;
	}

	public static zqu__QuoteRatePlan__c createQuoteRatePlan(zqu__Quote__c quote)
	{
		zqu__QuoteRatePlan__c qrp = new zqu__QuoteRatePlan__c();
		//qrp.Name = CERT_OVERAGE;
		qrp.CurrencyIsoCode = quote.zqu__Currency__c;
		qrp.zqu__Quote__c = quote.Id;
		//qrp.zqu__QuoteAmendment__c = qa.Id;
		//qrp.zqu__AmendmentType__c = qa.zqu__Type__c;
		//qrp.zqu__ProductRatePlanZuoraId__c = certOverageRatePlanCharge.zqu__ProductRatePlan__r.zqu__ZuoraId__c;
		//qrp.zqu__QuoteProductName__c = certOverageRatePlanCharge.zqu__ProductRatePlan__r.zqu__ZProduct__r.Name;

		return qrp;
	}

	public static zqu__QuoteRatePlanCharge__c createQuoteRatePlanCharge(zqu__Quote__c quote)
	{
		zqu__QuoteRatePlanCharge__c qrpc = new zqu__QuoteRatePlanCharge__c();
		//qrpc.Name = CERT_OVERAGE;
		//qrpc.zqu__QuoteRatePlan__c = qrp.Id;
		qrpc.CurrencyIsoCode = quote.zqu__Currency__c;
		//qrpc.zqu__ProductRatePlanChargeZuoraId__c = certOverageRatePlanCharge.zqu__ZuoraId__c;
		qrpc.zqu__UOM__c = 'Overage';
		qrpc.zqu__Quantity__c = 1;
		qrpc.zqu__Model__c = 'Per Unit Pricing';
		qrpc.zqu__FeeType__c = 'Per Unit';
		qrpc.zqu__ChargeType__c = 'One-Time';
		qrpc.zqu__ListTotal__c = 0.00;
		qrpc.zqu__ListPrice__c = 0.00;
		qrpc.zqu__Discount__c = 0;
		qrpc.zqu__MRR__c = 0;
		return qrpc;
	}

	public static zqu__QuoteChargeSummary__c createQuoteChargeSummary(zqu__Quote__c quote)
	{
		zqu__QuoteChargeSummary__c qcs = new zqu__QuoteChargeSummary__c();
		//qcs.Name = CERT_OVERAGE;
		qcs.CurrencyIsoCode = quote.zqu__Currency__c;
		//qcs.zqu__ChangeLog__c = 'New Rate Plan ' + '\"'+ qcs.Name + '\"';
		qcs.zqu__Model__c = 'Per Unit Pricing';
		qcs.zqu__Quantity__c = 1;
		qcs.zqu__Type__c = 'One-Time';
		qcs.zqu__UOM__c = 'Overage';
		qcs.zqu__Discount__c = 0;
		qcs.zqu__ListPrice__c = 0.00;
		qcs.zqu__MRR__c = 0;
		qcs.zqu__ListTotal__c = 0.00;
		return qcs;
	}

	public static zqu__QuoteChargeDetail__c createQuoteChargeDetail(zqu__Quote__C quote,zqu__QuoteRatePlanCharge__c Rateplancharge,zqu__QuoteRatePlan__c ratePlan )
	{
		zqu__QuoteChargeDetail__c Chargedetail = new zqu__QuoteChargeDetail__c();
		chargeDetail.zqu__Quote__c = quote.Id;
		chargeDetail.zqu__ProductRatePlanCharge__c = RateplanCharge.zqu__ProductRatePlanCharge__c;
		chargeDetail.zqu__ProductRatePlan__c = ratePlan.zqu__ProductRatePlan__c;
		chargeDetail.zqu__DeltaTCV__c = 0.0;
		chargeDetail.zqu__TCV__c = 0.0;
		chargeDetail.zqu__DeltaMRR__c = 0.0;
		chargeDetail.zqu__MRR__c = 0.0;
		return chargedetail;
	}

	public static zqu__QuoteAmendment__c createQuoteAmendment(zqu__Quote__c quote)
	{
		zqu__QuoteAmendment__c qa = new zqu__QuoteAmendment__c();
		qa.Name = quote.zqu__Amendment_Name__c;
		qa.zqu__Quote__c = quote.Id;
		qa.CurrencyIsoCode = quote.zqu__Currency__c;
		qa.zqu__Type__c = 'NewProduct';
		qa.zqu__AutoRenew__c = String.valueOf(quote.zqu__AutoRenew__c);
		qa.zqu__Description__c = 'Added Product ';
		qa.zqu__InitialTerm__c = quote.zqu__InitialTerm__c;
		qa.zqu__RenewalTerm__c = quote.zqu__RenewalTerm__c;
		qa.zqu__Status__c = quote.zqu__Status__c;
		qa.zqu__TermStartDate__c = quote.zqu__SubscriptionTermStartDate__c;
		qa.zqu__TermType__c = quote.zqu__Subscription_Term_Type__c;
		return qa;
	}

	public static Map<Id, zqu__Quote__c> getInsertedQuotes(Set<Id> accountIds, String chargeName)
	{
		Map<Id, zqu__Quote__c> accountIdToQuote = new Map<Id, zqu__Quote__c>();

		keyString += chargeName;
        System.debug('Key String is: ' +keyString);
		List<zqu__Quote__c> insertedQuotes = [SELECT Id, zqu__Account__c, RecordTypeId, zqu__Currency__c, zqu__Amendment_Name__c, zqu__AutoRenew__c, zqu__InitialTerm__c, zqu__RenewalTerm__c, zqu__Status__c, 
		zqu__SubscriptionTermStartDate__c, zqu__Subscription_Term_Type__c, Overage_Key__c, Is_Primary__c, zqu__Opportunity__c, zqu__ExistSubscriptionID__c, zqu__InvoiceOwnerId__c,
		zqu__StartDate__c, zqu__Opportunity__r.Type, zqu__Opportunity__r.CurrencyIsoCode, zqu__Subscription_Name__c, zqu__SubscriptionTermEndDate__c, zqu__Previewed_Total__c
		FROM zqu__Quote__c 
		WHERE zqu__Account__c IN :accountIds 
		AND Overage_Key__c = : keyString];
         
         System.debug('Size of the inserted quotes are:' + insertedQuotes.size());
		 for(zqu__Quote__c quote : insertedQuotes)
		 {
		 	accountIdToQuote.put(quote.zqu__Account__c, quote);
		 }
         System.debug('AccountTdToQuote is :' + accountIdToQuote);
         return accountIdToQuote;
	}
}