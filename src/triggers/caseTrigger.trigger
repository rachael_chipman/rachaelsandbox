trigger caseTrigger on Case (after insert, after update) {
	
	Id INTERNAL_TOOLS = [SELECT Id FROM User WHERE Name = 'Internal Tools Automation' and isActive = true LIMIT 1].Id;
	list<Case> casesToCreateOpps = new list<Case>();
	
	for(Case c : trigger.new)
	{
		if(c.CreatedById == INTERNAL_TOOLS)
		{
			casesToCreateOpps.add( c );
		}
	}
	
	if(trigger.isInsert && trigger.isAfter)
	{
		createTermOppFromCase termOppSvc = new createTermOppFromCase();
		termOppSvc.processTrigger(casesToCreateOpps);
	}
	
	if(trigger.isUpdate && trigger.isAfter)
	{
		createTermOppFromCase termOppSvc = new createTermOppFromCase();
		termOppSvc.processTrigger(casesToCreateOpps);
	}

}