public with sharing class CertAppService {
	
	public void processTrigger(List<Certification_Application__c> newApps, Map<Id, Certification_Application__c> oldApps)
	{
		List<Certification_Application__c> filteredApps = filterApplicationsOnUpdate( newApps , oldApps );
        if(!filteredApps.isEmpty())
        {
        	List<Contact> DPRRecipients = getContacts( filteredApps );
        	if(!DPRRecipients.isEmpty())
        	{
    			processApplications( filteredApps, newApps, DPRRecipients );
        	}
        }
        
	}
	
	public void processApplications(List<Certification_Application__c> filteredApplications, List<Certification_Application__c> newApps, List<Contact> getContacts)
	{
		Map<Id, List<Contact>> AppIdtoContactsMap = MapApptoContacts(filteredApplications, getContacts);
		List<Contact> UpdateContacts = new List<Contact>();
		
		for(Certification_Application__c certApp : filteredApplications)
		{
			if(!AppIdtoContactsMap.isEmpty() && AppIdtoContactsMap.containsKey(certApp.Id))
			{
				for(Contact contactToUpdate : AppIdtoContactsMap.get(certApp.Id))
				{
					contactToUpdate.Certification_Welcome_Program__c = true;
					UpdateContacts.add(contactToUpdate);
				}
			}
		}
		update UpdateContacts;
	}
	
	public static List<Certification_Application__c> filterApplicationsonUpdate(List<Certification_Application__c> newApps, Map<Id, Certification_Application__c> oldApps)
	{
		List<Certification_Application__c> filteredApplications = new List<Certification_Application__c>();
   		
		for(Certification_Application__c certApp : newApps)
		{
			Certification_Application__c oldApp = oldApps.get(certApp.id);
			if(certApp.Activation_Date__c != null && oldApp.Activation_Date__c == null)
			{
				filteredApplications.add(certApp);
			}
		}
		return filteredApplications;
	}
	
	public static List<Contact> getContacts(List<Certification_Application__c> filteredApplications)
	{
		Set<Id> AccountIds = new Set<Id>();
		for(Certification_Application__c certApp : filteredApplications)
		{
			if(certApp.Account__c != null)
			{
				AccountIds.add(certApp.Account__c);
			}
		}
		
		List<Contact> Contacts = [SELECT Id, Certification_Welcome_Program__c, AccountId, ContactsRole__c FROM Contact WHERE AccountId IN : AccountIds and ContactsRole__c INCLUDES('Certification Daily Report Recipient')];
		return Contacts;
	}

	private static Map<Id, List<Contact>> MapApptoContacts (List<Certification_Application__c> filteredApplications, List<Contact> Contacts)
	{
		Map<Id, List<Contact>> ApptoContactsMap = new Map<Id, List<Contact>>();
		
		for(Certification_Application__c certApp : filteredApplications)
		{
			ApptoContactsMap.put(certApp.Id, new List<Contact>());
			
			if(!Contacts.isEmpty())
			{
				for(Contact con : Contacts)
				{
					if(con.AccountId == certApp.Account__c)
					{
						ApptoContactsMap.get(certApp.Id).add(con);
					}
					
				}
			}
		}
		return ApptoContactsMap;
	}
}