// Trigger records the RPC start date on the Contact for Eloqua sync


trigger RPCstartdate on Account (After Insert, After Update) {

for(Account acct : Trigger.new){

if(acct.SSC_Start_Date__c != null){

            Date dt = acct.SSC_Start_Date__c;
            


if(Trigger.oldMap.get(acct.id).SSC_Start_Date__c != dt ){

List<Contact> cons = [select Id, RPC_Start_Date__c from Contact where AccountId = :acct.Id];

for(Contact c : cons){

c.RPC_Start_Date__c = acct.SSC_Start_Date__c;

update c;
}
}
}
}
}