@IsTest (SeeAllData = true)
private class AccountLeadCompareConvert_Test {
private static testmethod void testAccountLeadCompareConvert(){

Account a = new Account( Name='testing');
insert a;

Lead l = new Lead (FirstName = 'test', Lastname = 'test', Company = 'testing');
insert l;

Matching__c m = new Matching__c(
Lead__c = l.Id,
Account__c = a.Id,
Convert_Lead__c = true);

insert m;

//id batchinstanceid = database.executeBatch(new AccountLeadCompareConvert('select Id, Name, Lead__c, Account__c, Account__r.OwnerId from Matching__c where Convert_Lead__c = true'));
}}