@isTest

private class CampaignPushTest {
	
	static testMethod void testLead() {
		
		campaign c = new campaign();
		c.name = 'Test Campaign';
		c.isactive = true;
		insert c;
		
		campaign c2 = new campaign();
		c2.name = 'Second Test Campaign';
		c2.isactive = true;
		insert c2;
		
		lead l = new lead();
		l.firstname = 'Test';
		l.lastname = 'Name';
		l.company = 'Test Company';
		l.title = 'VP of Testing';
		l.status = 'Open';
		insert l;
		
		campaignmember cm = new campaignmember();
		cm.leadid = l.id;
		cm.campaignid = c.id;
		insert cm;
		
		campaignmember cm2 = new campaignmember();
		cm2.leadid = l.id;
		cm2.campaignid = c2.id;
		insert cm2;
		
	}
	
	static testMethod void testContact() {
		
		account a = new account(name = 'Test Account', accountmanager__c = 'Mary Matthews');
		insert a;
		
		contact c = new contact(firstname = 'Test',lastname = 'Contact',title = 'VP of Testing',accountid = a.id);
		insert c;
		
		campaign cp = new campaign(name = 'Test Campaign');
		insert cp;
		
		campaign cp2 = new campaign(name = 'Second Test Campaign');
		insert cp2;
		
		campaignmember cm = new CampaignMember(contactid = c.id, campaignid = cp.id);
		insert cm;
		
	}
}