trigger NuggetMatchType on Nugget_Match__c (before Insert, Before Update) {

for(Nugget_Match__c nm : trigger.new){

if(nm.Matching_Account__c != null){nm.Account_Match__c = true;}
else{nm.Account_Match__c = false;}

if(nm.Matching_Lead__c != null){nm.Lead_Match__c = true;}
else{nm.Lead_Match__c = false;}
}}