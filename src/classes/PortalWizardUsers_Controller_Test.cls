@isTest (seeAllData=true)
Public class PortalWizardUsers_Controller_Test{
    public static testMethod void testPortalWizardUsers_Controller(){
    PageReference pageRef = Page.PortalWizardUsers;
Test.setCurrentPageReference(pageRef);

           
     Account acct = new Account();
        acct.name = 'RPTestAcct1';
        acct.accountmanager__c = 'BIO Automation';
        acct.Customer_Relationship_Manager_Primary__c = 'BIO Automation';
        Insert acct;
        
             
        Pricebook2 standardPB = [select id from Pricebook2 where isStandard=true];

        Pricebook2 pb = new Pricebook2(Name = 'Standard Price Book 2009', Description = 'Price Book 2009 Products', IsActive = true);
        insert pb;
        
        Product2 prod = new Product2(Name = 'Anti-infectives 2007', Family = 'Best Practices', IsActive = true);
        insert prod;
        
        Product2 prod2 = new Product2(Name = 'Partner Commissions - Reseller', Family = 'Partner Commissions', IsActive = true);
        insert prod2;

        PricebookEntry standardPrice = new PricebookEntry(Pricebook2Id = standardPB.Id, Product2Id = prod.Id, UnitPrice = 10000, IsActive = true, UseStandardPrice = false);
        insert standardPrice;
        
        PricebookEntry standardPrice2 = new PricebookEntry(Pricebook2Id = standardPB.Id, Product2Id = prod2.Id, UnitPrice = 10000, IsActive = true, UseStandardPrice = false);
        insert standardPrice2;

        PricebookEntry pbe = new PricebookEntry(Pricebook2Id = pb.Id, Product2Id = prod.Id, UnitPrice = 10000, IsActive = true, UseStandardPrice = false);
        insert pbe;
        PricebookEntry pbe2 = new PricebookEntry(Pricebook2Id = pb.Id, Product2Id = prod2.Id, UnitPrice = 10000, IsActive = true, UseStandardPrice = false);
        insert pbe2;

        
   Opportunity opp = new Opportunity();
       opp.AccountId = acct.Id;
       opp.Name = 'Test Opp';
       opp.CloseDate = system.today();
       opp.Type = 'Renewal';
       opp.StageName = 'Signature';
       opp.Pricebook2Id = pb.Id;
       opp.Partner_Commission__c = .25;
       opp.CurrencyISOCode = 'USD';
       insert opp;            
       
        OpportunityLineItem oli = new OpportunityLineItem();
       oli.OpportunityId = opp.Id;
       oli.UnitPrice = 100.00;
       oli.Quantity = 1;
       oli.PricebookEntryId = pbe.Id;
       insert oli;
       
       Contact c = new Contact(LastName = 'test');
       insert c;
       
       Contact c2 = new Contact(LastName = 'test');
       insert c2;
       
       
      
      System.currentPagereference().getParameters().put('acct',acct.Id);
      System.currentPagereference().getParameters().put('opp',opp.Id);
      
PortalWizardUsers_Controller oPEE = new PortalWizardUsers_Controller();  

oPEE.addrow();
oPEE.Next();

}}