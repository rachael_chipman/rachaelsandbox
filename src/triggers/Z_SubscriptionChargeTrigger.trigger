/** Trigger for the Subscription object
 *
 * @author Marin Zaimov (marin.zaimov@zuora.com)
 * @version 1.0 June 11, 2015
 */
 trigger Z_SubscriptionChargeTrigger on Zuora__SubscriptionProductCharge__c (after insert) {
Z_SubscriptionChargeTriggerHandler handler = new Z_SubscriptionChargeTriggerHandler();
    
    //NOTE(Marin): Leaving below code in here to show how I typically use triggers through handler classes.
    //if (Trigger.isBefore) {
    //    if (Trigger.isDelete) {
    //      handler.onBeforeDelete(Trigger.old, Trigger.oldMap);          
    //    }
    //    if (Trigger.isInsert) {
    //      handler.onBeforeInsert(Trigger.new, Trigger.newMap);
    //    }
    //    if (Trigger.isUpdate) {
    //        //handler.onBeforeUpdate(Trigger.new, Trigger.old, Trigger.newMap, Trigger.oldMap);
    //    }
    //}

    if (Trigger.isAfter) {
      if (Trigger.isInsert) {
        handler.onAfterInsert(Trigger.new, Trigger.newMap);
      }
    }
}