trigger App_Pass_Fail on Certification_Application__c (Before Update) {

Certification_Application__c ca = Trigger.New[0];

if(ca.Application_Result__c == 'Fail' && ca.Application_Fee_Opportunity__c != null){

    Opportunity op = [select Id, Application_Result__c from Opportunity where Id = :ca.Application_Fee_Opportunity__c];
        op.Application_Result__c = 'Fail';
            update op;
        
    OpportunityLineItem oli = [select Id, Start_Date__c, End_Date__c from OpportunityLineItem where OpportunityId = :op.Id];
        oli.Start_Date__c = system.today();
        oli.End_Date__c = system.Today();
            update oli;    
 
         ca.Application_Status__c = 'Closed';}   
     

if(ca.Promo_Type_Picklist__c == 'Walk-Up' && ((ca.Application_Result__c == 'Pass' && ca.Application_Status__c == 'Audit Complete' && ca.Locked__c == 'Yes') || (ca.Application_Result__c == 'In Progress' && ca.Application_Status__c == 'Under Review' && ca.Locked__c == 'Early Activation')) &&
        ca.Early_Activation__c == False && ca.Application_Fee_Opportunity__c !=null){

    Opportunity op = [select Id, Application_Result__c from Opportunity where Id = :ca.Application_Fee_Opportunity__c];
        op.Application_Result__c = 'Pass';
            update op;
        
    OpportunityLineItem oli = [select Id, Start_Date__c, End_Date__c from OpportunityLineItem where OpportunityId = :op.Id];
        oli.Start_Date__c = system.today();
        oli.End_Date__c = system.Today()+365;
            update oli;    
 
    Opportunity newopp = new Opportunity(
        AccountId = ca.Account__c,
        RecordTypeID = '012000000004wVUAAY',
        Name = 'Name',
        StageName = 'Obtaining Commitment',
        CloseDate = system.today(),
        Type = 'New Business',
        CurrencyISOCode = ca.CurrencyISOCode,
        CampaignID = '701000000004fLnAAI',
        Pricebook2Id = '01s000000004NS6AAM',
        OwnerId = '00500000007286zAAA',
        Product__c = 'Certification',
        Renewed_to_Date__c = ca.Renewed_to_Date__c,
        SSC_Volume__c = ca.Certification_Volume__c,
        Application_Result__c = ca.Application_Result__c,
        SSC_Qual_Team_Member__c = ca.Certification_Analyst__c,
        Activation_Date__c = ca.Activation_Date__c,
        Bond_Group_ID__c = ca.Bond_Group_ID__c,
        Approval_Type__c = 'Finance Approval',
        Product_Certification__c = true,
        Certification_Type__c = ca.Certification_Type__c);
            insert newopp;
    
    OpportunityLineItem newoli = new OpportunityLineItem(
        OpportunityId = newopp.Id,
        UnitPrice = ca.Cert_Tier_to_Unit_Price__c,
        Quantity = 1,
        Product_Status__c = 'Current',
        Description__c = 'Return Path Certification',
        Indirect__c = true,
        Active__c = true,
        Recurring__c = true,
        Start_Date__c = ca.Activation_Date__c,
        End_Date__c = ca.Renewed_to_Date__c);
        
       if(newopp.CurrencyISOCode == 'AUD'){newoli.PricebookEntryId = '01u00000000FGwVAAW';}
       if(newopp.CurrencyISOCode == 'BRL'){newoli.PricebookEntryId = '01u00000000FQoqAAG';}
       if(newopp.CurrencyISOCode == 'CAD'){newoli.PricebookEntryId = '01u00000000FRK8AAO';}
       if(newopp.CurrencyISOCode == 'EUR'){newoli.PricebookEntryId = '01u00000000FGwUAAW';}
       if(newopp.CurrencyISOCode == 'GBP'){newoli.PricebookEntryId = '01u00000000FGwTAAW';}
       if(newopp.CurrencyISOCode == 'USD'){newoli.PricebookEntryId = '01u00000000FGwSAAW';}        
        
            insert newoli;
            
    Account acct = [select Id, Bond_Group_Id__c, Bonded_Sender_Customer__c, Certification_Max_Volume__c, Certification_Type_Active__c, Channel_Prospect__c,
                    Customer_Relationship_Manager_Primary__c, Date_Joined__c, Renewed_to_Date__c, SSC_Parent_Account_ID__c, SSC_Start_Date__c,
                    SSC_Tools_Child__c from Account where Id = :ca.Account__c];
        acct.Bond_Group_ID__c = ca.Bond_Group_ID__c;
        acct.Bonded_Sender_Customer__c = true;
        acct.Certification_Max_Volume__c = ca.Certification_Max_Volume__c;
        acct.Certification_Type_Active__c = ca.Certification_Type__c;
        acct.Renewed_to_Date__c = ca.Renewed_to_Date__c;
        //acct.SSC_New_Tier__c = ca.Certification_Volume__c;
        acct.SSC_Start_Date__c = ca.Activation_Date__c;
        acct.SSC_Tools_Child__c = false;
        acct.RPC_Agreement_Effective_Date__c = ca.Agreement_Effective_Date__c;
        acct.RPC_Agreement_Expire_Date__c = ca.Agreement_Expire_Date__c;
        acct.RPC_Agreement_ID__c = ca.Agreement_ID__c;
        acct.RPC_Agreement_Version__c = ca.Agreement_Version__c;
        acct.RPC_Signed_by_Company__c = ca.Signed_by_Company__c;
        acct.RPC_Signed_by_Email__c = ca.Signed_by_Email__c;
        acct.RPC_Signed_by_IP__c = ca.Signed_by_IP__c;
        acct.RPC_Signed_by_Name__c = ca.Signed_by_Name__c;
        acct.RPC_Signed_by_Title__c = ca.Signed_by_Title__c;
        acct.RPC_Standards_Accountable__c = ca.Standards_Accountable__c;
        acct.RPC_Standards_Compliance__c = ca.Standards_Compliance__c;
        acct.RPC_Standards_Read__c = ca.Standards_Read__c;
        acct.RPC_Standards_Version__c = ca.Standards_Version__c;

    
   if(acct.Date_Joined__c == null){
        acct.Date_Joined__c = ca.Activation_Date__c;}
        
        update Acct;                          
    
    if(ca.Application_Status__c == 'Audit Complete'){ca.Application_Status__c = 'Closed';}
    if(ca.Application_Status__c == 'Under Review'){ ca.Early_Activation__c = true;}
    
    ca.License_Fee_Opportunity__c = newopp.Id;
    ca.Opportunity_Created__c = 'Yes';} 

if(ca.Promo_Type_Picklist__c == 'Referral' && ca.Partner_CRM__c != null &&((ca.Application_Result__c == 'Pass' && ca.Application_Status__c == 'Audit Complete' && ca.Locked__c == 'Yes') || (ca.Application_Result__c == 'In Progress' && ca.Application_Status__c == 'Under Review' && ca.Locked__c == 'Early Activation')) &&
        ca.Early_Activation__c == False){
        
        Account partner = [select Id, OwnerId, CRM__c from Account where Id = :ca.Partner_Account__c];
    
     Opportunity newopp = new Opportunity(
        AccountId = ca.Account__c,
        RecordTypeID = '012000000004wVUAAY',
        Name = 'Name',
        StageName = 'Obtaining Commitment',
        CloseDate = system.today(),
        Type = 'New Business',
        CurrencyISOCode = ca.CurrencyISOCode,
        CampaignID = '701000000004fLnAAI',
        Pricebook2Id = '01s000000004NS6AAM',
        OwnerId = ca.Partner_CRM__c,
        Product__c = 'Certification',
        Renewed_to_Date__c = ca.Renewed_to_Date__c,
        SSC_Volume__c = ca.Certification_Volume__c,
        Application_Result__c = ca.Application_Result__c,
        SSC_Qual_Team_Member__c = ca.Certification_Analyst__c,
        Activation_Date__c = ca.Activation_Date__c,
        Bond_Group_ID__c = ca.Bond_Group_ID__c,
        Certification_Type__c = ca.Certification_Type__c,
        Approval_Type__c = 'Finance Approval',
        Product_Certification__c = true,
        CRM_User__c = ca.Partner_CRM__c);
            insert newopp;
    
    OpportunityLineItem newoli = new OpportunityLineItem(
        OpportunityId = newopp.Id,
        UnitPrice = ca.Cert_Tier_to_Unit_Price__c,
        Quantity = 1,
        Product_Status__c = 'Current',
        Description__c = 'Return Path Certification',
        Indirect__c = true,
        Active__c = true,
        Recurring__c = true,
        Start_Date__c = ca.Activation_Date__c,
        End_Date__c = ca.Renewed_to_Date__c);
        
       if(newopp.CurrencyISOCode == 'AUD'){newoli.PricebookEntryId = '01u00000000Fb2JAAS';}
       if(newopp.CurrencyISOCode == 'BRL'){newoli.PricebookEntryId = '01u00000000Fb2FAAS';}
       if(newopp.CurrencyISOCode == 'CAD'){newoli.PricebookEntryId = '01u00000000Fb2EAAS';}
       if(newopp.CurrencyISOCode == 'EUR'){newoli.PricebookEntryId = '01u00000000Fb2IAAS';}
       if(newopp.CurrencyISOCode == 'GBP'){newoli.PricebookEntryId = '01u00000000Fb2HAAS';}
       if(newopp.CurrencyISOCode == 'USD'){newoli.PricebookEntryId = '01u00000000Fb2GAAS';}  
        
            insert newoli;
            
     Partner toacct = new Partner(
        OpportunityId = newopp.Id,
        AccountToId = ca.Partner_Account__c,
        Role = 'Channel',
        IsPrimary = true);
            insert toacct; 
            
      Partner fromacct = new Partner(
               AccountToId = ca.Partner_Account__c,
               AccountFromId = ca.Account__c,
               Role = 'Channel',
               IsPrimary = true);
                              insert fromacct;                 
    
     Account acct = [select Id, Bond_Group_Id__c, Bonded_Sender_Customer__c, Certification_Max_Volume__c, Certification_Type_Active__c, Channel_Prospect__c,
                    Customer_Relationship_Manager_Primary__c, Date_Joined__c, Renewed_to_Date__c, SSC_Parent_Account_ID__c, SSC_Start_Date__c,
                    SSC_Tools_Child__c from Account where Id = :ca.Account__c];
                    
        acct.Bond_Group_ID__c = ca.Bond_Group_ID__c;
        acct.Bonded_Sender_Customer__c = true;
        acct.Certification_Max_Volume__c = ca.Certification_Max_Volume__c;
        acct.Certification_Type_Active__c = ca.Certification_Type__c;
        acct.Renewed_to_Date__c = ca.Renewed_to_Date__c;
        //acct.SSC_New_Tier__c = ca.Certification_Volume__c;
        acct.SSC_Start_Date__c = ca.Activation_Date__c;
        acct.SSC_Tools_Child__c = true;
        acct.Channel_Prospect__c = 'Child';
        acct.Customer_Relationship_Manager_Primary__c = ca.Partner_CRM__c;
        acct.SSC_Parent_Account_ID__c = ca.Partner_Account__c;
        acct.RPC_Agreement_Effective_Date__c = ca.Agreement_Effective_Date__c;
        acct.RPC_Agreement_Expire_Date__c = ca.Agreement_Expire_Date__c;
        acct.RPC_Agreement_ID__c = ca.Agreement_ID__c;
        acct.RPC_Agreement_Version__c = ca.Agreement_Version__c;
        acct.RPC_Signed_by_Company__c = ca.Signed_by_Company__c;
        acct.RPC_Signed_by_Email__c = ca.Signed_by_Email__c;
        acct.RPC_Signed_by_IP__c = ca.Signed_by_IP__c;
        acct.RPC_Signed_by_Name__c = ca.Signed_by_Name__c;
        acct.RPC_Signed_by_Title__c = ca.Signed_by_Title__c;
        acct.RPC_Standards_Accountable__c = ca.Standards_Accountable__c;
        acct.RPC_Standards_Compliance__c = ca.Standards_Compliance__c;
        acct.RPC_Standards_Read__c = ca.Standards_Read__c;
        acct.RPC_Standards_Version__c = ca.Standards_Version__c;
    
   if(acct.Date_Joined__c == null){
        acct.Date_Joined__c = ca.Activation_Date__c;}
        
        update Acct;                          
    
    if(ca.Application_Status__c == 'Audit Complete'){ca.Application_Status__c = 'Closed';}
    if(ca.Application_Status__c == 'Under Review'){ ca.Early_Activation__c = true;}
    
    ca.License_Fee_Opportunity__c = newopp.Id;
    ca.Opportunity_Created__c = 'Yes';
    
    } 
    
if(ca.Promo_Type_Picklist__c == 'Bundle' && ((ca.Application_Result__c == 'Pass' && ca.Application_Status__c == 'Audit Complete' && ca.Locked__c == 'Yes') || (ca.Application_Result__c == 'In Progress' && ca.Application_Status__c == 'Under Review' && ca.Locked__c == 'Early Activation')) &&
        ca.Early_Activation__c == False){   
   
   Account acct = [select Id, Bond_Group_Id__c, Bonded_Sender_Customer__c, Certification_Max_Volume__c, Certification_Type_Active__c, Channel_Prospect__c,
                    Customer_Relationship_Manager_Primary__c, Date_Joined__c, Renewed_to_Date__c, SSC_Parent_Account_ID__c, SSC_Start_Date__c,
                    SSC_Tools_Child__c from Account where Id = :ca.Account__c];
        acct.Bond_Group_ID__c = ca.Bond_Group_ID__c;
        acct.Bonded_Sender_Customer__c = true;
        acct.Certification_Max_Volume__c = ca.Certification_Max_Volume__c;
        acct.Certification_Type_Active__c = ca.Certification_Type__c;
        acct.Renewed_to_Date__c = ca.Renewed_to_Date__c;
        //acct.SSC_New_Tier__c = ca.Certification_Volume__c;
        acct.SSC_Start_Date__c = ca.Activation_Date__c;
        acct.SSC_Tools_Child__c = false;
        acct.RPC_Agreement_Effective_Date__c = ca.Agreement_Effective_Date__c;
        acct.RPC_Agreement_Expire_Date__c = ca.Agreement_Expire_Date__c;
        acct.RPC_Agreement_ID__c = ca.Agreement_ID__c;
        acct.RPC_Agreement_Version__c = ca.Agreement_Version__c;
        acct.RPC_Signed_by_Company__c = ca.Signed_by_Company__c;
        acct.RPC_Signed_by_Email__c = ca.Signed_by_Email__c;
        acct.RPC_Signed_by_IP__c = ca.Signed_by_IP__c;
        acct.RPC_Signed_by_Name__c = ca.Signed_by_Name__c;
        acct.RPC_Signed_by_Title__c = ca.Signed_by_Title__c;
        acct.RPC_Standards_Accountable__c = ca.Standards_Accountable__c;
        acct.RPC_Standards_Compliance__c = ca.Standards_Compliance__c;
        acct.RPC_Standards_Read__c = ca.Standards_Read__c;
        acct.RPC_Standards_Version__c = ca.Standards_Version__c;        
        update acct;
   
   if(ca.Application_Status__c == 'Audit Complete'){
    ca.Application_Status__c = 'Closed';}
    
    if(ca.Application_Status__c == 'Under Review'){ ca.Early_Activation__c = true;}
    
    }       
}