public with sharing class OwnerCopyPodApprovers {
	
	public static void processTrigger(list<Opportunity> newOpportunities, map<id, opportunity> oldOpps)
	{
		list<Opportunity> filteredOpps = filterOpportunitiesOnInsertAndOnUpdate(newOpportunities, oldOpps);
		if(!filteredOpps.isempty())
		{
			processOpportunities(filteredOpps);
		}
	}
	
	public static void processOpportunities(list<Opportunity> filteredOpportunities)
	{
		map<id, user> OpportunityIdtoOwner = oppIdtoOwnerMap(filteredOpportunities);
		for(Opportunity Opp : filteredOpportunities)
		{
			if(!OpportunityIdtoOwner.isempty() && OpportunityIdtoOwner.containskey(Opp.Id))
			{
				opp.Owner_Copy__c = OpportunityIdtoOwner.get(opp.id).Id;
				opp.First_Approver__c = OpportunityIdtoOwner.get(opp.id).First_Approver__c;
				opp.Second_Approver__c = OpportunityIdtoOwner.get(opp.id).Second_Approver__c;
				opp.POD__c = OpportunityIdtoOwner.get(opp.id).User_Pod_Number__c;
				opp.Reset_1st_and_2nd_Approver_Fields__c = false;
			}
		}
		
	}
	
	public static List<Opportunity> filterOpportunitiesOnInsertAndOnUpdate(List<Opportunity> newOpportunities, Map<Id, Opportunity> oldOpps)
	{
		List<Opportunity> filteredOpportunities = new List<Opportunity>();
		for(Opportunity opp : newOpportunities)
		{
			Opportunity oldOpp = oldOpps.get(opp.Id);
			if(opp.IsClosed == false || opp.Reset_1st_and_2nd_Approver_Fields__c)
			{
				filteredOpportunities.add(opp);
			}
			
		}
		return filteredOpportunities;
	}
	
	private static Map<Id, User> oppIdToOwnerMap (List<Opportunity> filteredOpportunities)
	{
		Map<Id, User> oppIdtoOwner = new Map<Id, User>();
		Set<Id> ownerUserIds = new Set<Id>();
		
		for(Opportunity opp : filteredOpportunities)
		{
			ownerUserIds.add(opp.OwnerId);
		}
		List<User> owners = [SELECT Id, First_Approver__c, Second_Approver__c, User_Pod_Number__c FROM User WHERE Id IN: ownerUserIds];
		for(Opportunity opp : filteredOpportunities)
		{
			for(User owner : owners)
			{
				if(owner.Id == opp.OwnerId)
				{
					oppIdtoOwner.put(opp.Id, owner);
				}
			}
		}
		system.debug('What is in our Owner Map'+oppIdtoOwner);
		return oppIdToOwner;
	}

}