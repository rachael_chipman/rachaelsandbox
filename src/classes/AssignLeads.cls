public class AssignLeads{

    public static Boolean assignAlreadyCalled = false;
    
        public static Boolean assignAlreadyCalled(){
            return assignAlreadyCalled;
        }
        
    @future
    public static void assign(List<Id> LeadstoAssign){
        
        assignAlreadyCalled = true;
        List<Lead> Leads = [SELECT Id from Lead WHERE Id in : LeadstoAssign];
        
        for(Lead l:leads){
        
            Database.DMLOptions dmo = new Database.DMLOptions();
            dmo.assignmentRuleHeader.useDefaultRule= true;
            l.setOptions(dmo);
            
            }
            update (leads);
        }
    }