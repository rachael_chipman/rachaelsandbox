public with sharing class PartnerInvoiceController {

    Public List<c2g__codaInvoice__c> invoices;
    Public List<c2g__codaCreditNote__c> credits;
    Public Partner_Invoice__c pi {get;set;}
    Public c2g__codaInvoice__c si {get;set;}
    Public Account acct {get;set;}
    Public Date invdate;
    Public Boolean creditstrue {get;set;}
    Public Map<String, Double> mapInvoiceCurrencyToTotal = new Map<String, Double>();
    Public Map<String, Double> mapCreditCurrencyToTotal = new Map<String, Double>();

    public PartnerInvoiceController(ApexPages.StandardController controller) {

        pi = [select Id, Name, PO_Number__c, Invoice_Date__c, Invoice_Total__c, Invoices_Total__c, Credits_Total__c, Partner_Invoice_Total__c, Invoice_Due_Date__c, Partner_Account__c from Partner_Invoice__c where Id = :ApexPages.currentPage().getParameters().get('Id')];

        acct = [select Id, Name, BillingCity, BillingStreet, BillingState, BillingCountry, BillingPostalCode, c2g__CODAInvoiceEmail__c  from Account where Id = :pi.Partner_Account__c];

        creditstrue = false;

        credits = new List<c2g__codaCreditNote__c>();
        invoices = new List<c2g__codaInvoice__c>();


    }

    Public List<c2g__codaInvoice__c> getinvoices() {

        invoices = [select Id, Name, c2g__DueDate__c, c2g__InvoiceDate__c, c2g__Account__c, c2g__Account__r.Name,
         c2g__InvoiceTotal__c, c2g__Opportunity__r.Account.Name, c2g__codaInvoice__c.c2g__InvoiceCurrency__r.Name,
         Contract_Service_Period_Start_Date__c, Contract_Service_Period_End_Date__c,c2g__InvoiceCurrency__r.CurrencyIsoCode,
         Invoice_Total__c,
         (select Id, c2g__Product__c, c2g__Quantity__c, c2g__UnitPrice__c,
                 c2g__Product__r.Description, c2g__Product__r.Name, Quantity_Text__c,
                 Product_Line_Item_Description__c, c2g__NetValue__c, c2g__LineDescription__c from c2g__InvoiceLineItems__r 
          where AR_Gross_Up_Item__c != true order by c2g__UnitPrice__c DESC)
                 from c2g__codaInvoice__c where Partner_Invoice__c = :pi.Id order by Name];

        return invoices;
    }

    Public List<c2g__codaCreditNote__c> getcredits() {

        credits = [select Id, Name, c2g__Account__c, c2g__Account__r.Name, c2g__Invoice__c, c2g__Invoice__r.Name, c2g__CreditNoteDate__c, c2g__CreditNoteCurrency__c,
        c2g__CreditNoteTotal__c, c2g__CreditNoteCurrency__r.Name,
        (select Id, c2g__Product__c, c2g__Quantity__c, c2g__UnitPrice__c,
                 c2g__Product__r.Description, Credited_Product__r.Name from c2g__CreditNoteLineItems__r order by Name DESC)

         from c2g__codaCreditNote__c where Partner_Invoice__c = :pi.Id];

         if(credits.size() > 0){ creditstrue = true;}

        return credits;
    }

    Public Map<String, Double> getmapInvoiceCurrencyToTotal()
    {


        for(Integer i = 0; i < invoices.size(); i++)
        {
            if(!mapInvoiceCurrencyToTotal.containsKey(invoices[i].c2g__InvoiceCurrency__r.CurrencyIsoCode))
            {
                mapInvoiceCurrencyToTotal.put(Invoices[i].c2g__InvoiceCurrency__r.CurrencyIsoCode,Invoices[i].Invoice_Total__c);
            }
            else
            {
                mapInvoiceCurrencyToTotal.put(Invoices[i].c2g__InvoiceCurrency__r.CurrencyIsoCode, mapInvoiceCurrencyToTotal.get(Invoices[i].c2g__InvoiceCurrency__r.CurrencyIsoCode) + Invoices[i].Invoice_Total__c);
            }
            if( !mapCreditCurrencyToTotal.containsKey( Invoices[i].c2g__InvoiceCurrency__r.CurrencyIsoCode ) )
            {
                mapCreditCurrencyToTotal.put( Invoices[i].c2g__InvoiceCurrency__r.CurrencyIsoCode, 0 );
            }
        }

        return mapInvoiceCurrencyToTotal;

    }

    Public Map<String, Double> getmapCreditCurrencyToTotal()
    {

        for(Integer i = 0; i < credits.size(); i++)
        {
            if(!mapCreditCurrencyToTotal.containsKey(credits[i].c2g__CreditNoteCurrency__r.Name))
            {
                mapCreditCurrencyToTotal.put(credits[i].c2g__CreditNoteCurrency__r.Name,credits[i].c2g__CreditNoteTotal__c);
            }

            else
            {
                mapCreditCurrencyToTotal.put(credits[i].c2g__CreditNoteCurrency__r.Name, mapCreditCurrencyToTotal.get(credits[i].c2g__CreditNoteCurrency__r.Name) + credits[i].c2g__CreditNoteTotal__c);
            }
        }


        return mapCreditCurrencyToTotal;

    }


}