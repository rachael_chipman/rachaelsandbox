@isTest
private class TestSalesInvoiceTrigger {	


	@isTest(seeAllData=true) 
	static void testPost() 
	{
		FinancialForceTestHelper.baseSetup();
		//List<Opportunity> testOpportunities = TestUtils.createOpportunities( 2, TestUtils.AWARENESS_RECORD_TYPE.Id, FinancialForceTestHelper.testAccount.Id, true );
		List<Opportunity> testOpportunities = TestUtils.createOpportunities( 2, TestUtils.RESEARCH_RECORD_TYPE.Id, FinancialForceTestHelper.testAccount.Id, true );
		List<c2g__codaInvoice__c> testSalesInvoices = new List<c2g__codaInvoice__c>();
		List<c2g__codaInvoiceLineItem__c> testSalesInvoiceLineItems = new List<c2g__codaInvoiceLineItem__c>();
		
		for( Opportunity anOpportunity : testOpportunities )
		{
			testSalesInvoices.add( TestUtils.createSalesInvoice( FinancialForceTestHelper.testAccount.Id, new Map<String,Object>{ 'c2g__Opportunity__c' => anOpportunity.Id }, false ) );
		}
		insert testSalesInvoices;
		
		for( c2g__codaInvoice__c testSalesInvoice: testSalesInvoices )
		{
			testSalesInvoiceLineItems.add( TestUtils.createSalesInvoiceLineItem( testSalesInvoice.Id, FinancialForceTestHelper.testProduct.Id, false ) );
		}
		insert testSalesInvoiceLineItems; 

		test.startTest();
			FinancialForceTestHelper.bulkPostSalesInvoices( Pluck.ids( testSalesInvoices ) );
		test.stopTest();

		testSalesInvoices = [ SELECT c2g__Transaction__c, c2g__Transaction__r.Opportunity__c, c2g__Opportunity__c FROM c2g__codaInvoice__c WHERE Id IN :testSalesInvoices ];
		for( c2g__codaInvoice__c testSalesInvoice: testSalesInvoices )
		{
			System.assertEquals( testSalesInvoice.c2g__Opportunity__c, testSalesInvoice.c2g__Transaction__r.Opportunity__c, 'Transaction opportunity  lookup should be populated with related credit note\'s opportunity' );
		}

		Set<Id> transactionIds = Pluck.ids( 'c2g__Transaction__c', testSalesInvoices );

		List<c2g__codaTransactionLineItem__c> actualTransationLineItems = [ SELECT Id, Opportunity__c, c2g__Transaction__c, c2g__Transaction__r.Opportunity__c From c2g__codaTransactionLineItem__c WHERE c2g__Transaction__c IN : transactionIds];
		System.assert( !actualTransationLineItems.isEmpty(), 'There should be transaction line items created' );
		for( c2g__codaTransactionLineItem__c testTransactionLineItem: actualTransationLineItems )
		{
			System.assertEquals( testTransactionLineItem.c2g__Transaction__r.Opportunity__c, testTransactionLineItem.Opportunity__c, 'Transaction line item opportunity  lookup should be populated with related transaction\'s opportunity' );
		}

	}

}