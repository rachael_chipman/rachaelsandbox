global class Batch_AccountContactRole implements Database.Batchable<SObject>, Database.Stateful, Schedulable
{
	public static String EMAIL_USER
    {
        get
        {
            if ( null == EMAIL_USER )
            {
                EMAIL_USER = [ SELECT Id, Email FROM User WHERE Name = 'Bluewolf Beyond' ].Email;
            }
            return EMAIL_USER;
        }
        set;
    }

    public static final String BATCH_NAME = 'AccountContactRole';
    public static final String BASE_QUERY = 'SELECT Id, ContactsRole__c, Contact_Status__c, (SELECT Id, Role FROM AccountContactRoles ) FROM Contact';
    public static final String[] ERROR_EMAILS = new String[]{ EMAIL_USER };
    public List<Id> contactIds;

    private String query;
    global String errors = '';

    global Batch_AccountContactRole()
    {
        query = BASE_QUERY;
    }

    global Batch_AccountContactRole( List<Id> contactIds )
    {
    	this.contactIds = contactIds;
        query = BASE_QUERY + ' WHERE Id IN :contactIds';
        System.debug(query);
    }

    global void execute( SchedulableContext sc )
    {
        Batch_AccountContactRole batch = new Batch_AccountContactRole();
        Database.executeBatch( batch );
    }

    global Database.QueryLocator start( Database.BatchableContext BC )
    {
        return Database.getQueryLocator( query );
    }

    global void execute( Database.BatchableContext BC, List<sObject> scope )
    {
    	 try
    	 {
            ContactServices.updateRoleField(scope);
    	 }
	     catch( DMLException ex )
	     {
	         for( Integer i = 0; i < ex.getNumDml(); i++ )
	         {
	             errors += ex.getDmlMessage(i) + '\n';
	         }
	     }
    }

    global void finish( Database.BatchableContext BC )
    {

	     AsyncApexJob job = [SELECT Id, Status, NumberOfErrors,
	                         JobItemsProcessed, TotalJobItems,
	                         CreatedBy.Email
	                         FROM AsyncApexJob
	                         WHERE Id =:bc.getJobId()];

	     Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();

	     mail.setToAddresses( ERROR_EMAILS );
	     mail.setSubject( 'Batch to ' + BATCH_NAME + ' ' + job.Status );

	     String emailBody = 'Batch to ' + BATCH_NAME + ' processed '
	                      + job.TotalJobItems
	                      + ' batches with '
	                      + job.NumberOfErrors
	                      + ' failures.';
	     if( errors != '' )
	     {
		     emailBody += '\n\n\nThe following errors occured:\n'+ errors;
		     mail.setPlainTextBody( emailBody );
		     Messaging.sendEmail( new Messaging.SingleEmailMessage[]{ mail } );
	     }

    }
}