public with sharing class PrimaryCampaignPush {
    
  public static void processTrigger(list<Opportunity> newOpportunities, map<id, opportunity> oldOpps)
  {
    list<Opportunity> filteredOpps = filterOpportunitiesOnInsertAndOnUpdate(newOpportunities, oldOpps);
    if(!filteredOpps.isempty())
    {
      processOpportunities(filteredOpps);
    }
  }
  
  public static void processOpportunities(list<Opportunity> filteredOpportunities)
  {
    map<id, campaign> OpportunityIdtoCampaign = oppIdtoCampaignMap(filteredOpportunities);
    List<Opportunity> oppsToUpdate = new List<Opportunity>(); 
      
    for(Opportunity Opp : filteredOpportunities)
    {
      if(!OpportunityIdtoCampaign.isempty() && OpportunityIdtoCampaign.containskey(Opp.Id))
      {
        Opportunity newOpp = new Opportunity (Id=opp.Id);	
        newOpp.CampaignId = OpportunityIdtoCampaign.get(opp.id).id;
        oppsToUpdate.add(newOpp);  
      }
    }
    try
    {
        update oppsToUpdate;
    }
    catch (DMLException e)
    {
       for(Opportunity opp : oppsToUpdate)
       {
           opp.addError('There was a problem updating the primary campaign');
       }
    }
    
  }
  
  public static List<Opportunity> filterOpportunitiesOnInsertAndOnUpdate(List<Opportunity> newOpportunities, Map<Id, Opportunity> oldOpps)
  {
    List<Opportunity> filteredOpportunities = new List<Opportunity>();
    for(Opportunity opp : newOpportunities)
    {
      Opportunity oldOpp;
      if(oldOpps != null)
      {
      	oldOpp = oldOpps.get(opp.Id);
      }
      	if(oldOpp == NULL || opp.CampaignId == NULL && (opp.Opportunity_Source__c != oldOpp.Opportunity_Source__c))
      	{
        	filteredOpportunities.add(opp);
      	}
      
        
    }
    return filteredOpportunities;
  }
  
  private static Map<Id, Campaign> oppIdtoCampaignMap (List<Opportunity> filteredOpportunities)
  {
    Map<Id, Campaign> oppIdtoCampaign = new Map<Id, Campaign>();
    Set<string> campaignNames= pluck.strings('Opportunity_Source__c', filteredOpportunities);
      
    List<Campaign> Campaigns = [SELECT Id, Name FROM Campaign WHERE Name IN: campaignNames AND isActive = true];
    for(Opportunity opp : filteredOpportunities)
    {
      for(Campaign c : Campaigns)
      {
        if(opp.Opportunity_Source__c == c.Name)
        {
          oppIdtoCampaign.put(opp.Id, c);
        }
      }
    }
    system.debug('What is in our Campaign map'+oppIdtoCampaign);
    return oppIdToCampaign;
  }

}