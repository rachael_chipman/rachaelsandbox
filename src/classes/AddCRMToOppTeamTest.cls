@isTest(SeeAllData=true)
private class AddCRMToOppTeamTest {
    
    static Opportunity opp;
    static Account account;
    static User crmUser;
    static User oppOwner;
    
    private static void setUpMethod()
    {
        account = TestUtils.createAccount(TestUtils.MARKETER_RECORD_TYPE.Id, true);
        opp = TestUtils.createOpportunity(TestUtils.RESEARCH_RECORD_TYPE.Id, account.Id, true);
        crmUser = TestUtils.CRM_USER;
        oppOwner = TestUtils.SALES_USER;
        
    }
    
    private static testMethod void updateOpportunityWithCRM()
    {
        setUpMethod();
        test.startTest();
        opp.CRM_User__c = crmUser.Id;
        opp.OwnerId = oppOwner.Id;
        opp.Type = 'New Business';
        update opp;
        test.stopTest();
        
        list<OpportunityTeamMember> oppTeamMembers = [SELECT Id, UserId FROM OpportunityTeamMember WHERE OpportunityId = :opp.Id];
        list<UserTeamMember> userTeamMembers = [SELECT Id, UserID FROM UserTeamMember WHERE OwnerId = :crmUser.Id];
        list<UserTeamMember> userTeamMembersOppOwner = [SELECT Id, UserID FROM UserTeamMember WHERE OwnerId = :oppOwner.Id];
        Set<String> memberIds = new Set<String>();
        Boolean idPresent;
        For(OpportunityTeamMember member: oppTeamMembers){
            memberIds.add(member.UserId);
        }
        for(UserTeamMember utm : userTeamMembers)
        {
            idPresent = false;
            if(memberIds.contains(utm.UserId))
            {
                idPresent = true;
            }
            system.assertEquals(true, idPresent, 'We expect the Id to be contained in the string of Ids');
        }
        for(UserTeamMember utmOwner : userTeamMembersOppOwner)
        {
            idPresent = false;
            if(memberIds.contains(utmOwner.UserId))
            {
                idPresent = true;
            }
            system.assertEquals(true, idPresent, 'We expect the Id to be contained in the string of Ids');
        }
        
        
        
    }
    
    private static testMethod void updateOpportunityRemoveCRM()
    {
        setUpMethod();
        opp.CRM_User__c = crmUser.Id;
        update opp;
        test.startTest();
        opp.CRM_User__c = NULL;
        update opp;
        test.stopTest();
        
        list<OpportunityTeamMember> oppTeamMembers = [SELECT Id FROM OpportunityTeamMember WHERE OpportunityId = :opp.Id];
        list<UserTeamMember> userTeamMembers = [SELECT Id FROM UserTeamMember WHERE OwnerId = :crmUser.Id];
        system.assert(oppTeamMembers.size()<userTeamMembers.size());
        
    }
}