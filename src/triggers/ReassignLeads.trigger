trigger ReassignLeads on Lead (before update) {

List<Id> LeadstoAssign = new list<Id>();


    for(Lead l : trigger.new){
    
String cam = l.Campaign_Name__c;
    
Lead prev = trigger.oldMap.get(l.id);
    
        Database.DMLOptions dmo = new Database.DMLOptions();
        dmo.assignmentRuleHeader.useDefaultRule= true;
        
        if((prev.Lead_Rating_Combined__c != l.Lead_Rating_Combined__c && (prev.Lead_Rating_Combined__c == 'A4' || prev.Lead_Rating_Combined__c =='A5' || prev.Lead_Rating_Combined__c == 'B4' || prev.Lead_Rating_Combined__c == 'B5' || 
            prev.Lead_Rating_Combined__c == 'C4' || prev.Lead_Rating_Combined__c == 'C5' ||prev.Lead_Rating_Combined__c == 'D1' || prev.Lead_Rating_Combined__c == 'D2' || prev.Lead_Rating_Combined__c == 'D3' || prev.Lead_Rating_Combined__c == 'D4' ||
            prev.Lead_Rating_Combined__c == 'D5' || prev.Lead_Rating_Combined__c == 'E1' || prev.Lead_Rating_Combined__c == 'E2' || prev.Lead_Rating_Combined__c == 'E3' || prev.Lead_Rating_Combined__c == 'E4' || prev.Lead_Rating_Combined__c == 'E5'))|| (cam != null && l.MOne1_Tier__c != 'Tier 4')){    
            
        
            if(l.OwnerId =='00500000006olmF' && l.Status == 'Nurture' && (l.Lead_Rating_Combined__c == 'A1' || l.Lead_Rating_Combined__c == 'A2' || l.Lead_Rating_Combined__c == 'A3' || l.Lead_Rating_Combined__c == 'B1' ||
                l.Lead_Rating_Combined__c == 'B2' || l.Lead_Rating_Combined__c == 'B3' || l.Lead_Rating_Combined__c == 'C1' || l.Lead_Rating_Combined__c == 'C2' || l.Lead_Rating_Combined__c == 'C3') || (cam.contains('Contact Us') || cam.contains('MarketOne Touch'))){ 
            
              LeadstoAssign.add(l.Id);
            
            }          
        
        
        if(AssignLeads.assignAlreadyCalled() == false){
            system.debug('Assign already called? ' + AssignLeads.assignAlreadyCalled());
            AssignLeads.Assign(LeadstoAssign);
            
        }
        }
}
}