@isTest(SeeAllData=true)
private class ApprovalProcessService_Test {

	private static Opportunity Opp;
	private static Account Acc;
	//private static zqu__Quote__c TQuote;
	private static ApprovalProcessServices APP;
	private static User Sales_User;
	private static String RETURN_PATH = 'Return Path Pricebook';
	private static PricebookEntry testPbe;   
    private static OpportunityLineItem testLineItem;
    private static  String c_code ;
    
    private static PricebookEntry productId (String testValue, string currencycode )
    {
        System.debug('currency code is:' +currencycode);
        PricebookEntry product = [SELECT Id, Name FROM PricebookEntry WHERE isActive = true AND CurrencyIsoCode = :currencycode AND Pricebook2.Name = :RETURN_PATH AND Name = :testValue LIMIT 1];
        return product;
    }
    
    private static string getOppCurrencyCode()
    {
        String ccode = [Select Id, Name,CurrencyIsoCode from Opportunity Where Id = :Opp.Id].CurrencyIsoCode;
        return ccode;
    }
    
    private static void setUpPricebookInfo()
    {
        Pricebook2 PB = [Select Id, Name from Pricebook2 where Name = :RETURN_PATH];
        Opp.Pricebook2Id = PB.Id;
      //  Opp.CurrencyIsoCode = 'USD';
    }
    
    private static void setSalesUser()
    {
         String currencycode = 'USD - U.S. Dollar';
         Sales_User = [SELECT Id FROM User WHERE Profile.Name = 'Sales' AND First_Approver__c != null AND /*DefaultCurrencyIsoCode =:currencycode AND*/ isActive = true LIMIT 1];
         system.debug('Show the sales user? ' + Sales_User);
    }
	private static void setup()
	{
		RecordType AccRecType = TestUtils.MARKETER_RECORD_TYPE;
		RecordType OppRecType = TestUtils.SELECT_RECORD_TYPE;
 	    Acc = TestUtils.createAccount(AccRecType.Id, true);
		Opp = TestUtils.createOpportunity(OppRecType.Id, Acc.Id, true);
       
	}

   
   
    private static void AddProduct(String currencycode)
    {
        testPbe = ProductId('Email Optimization: Certification - License - Annually', currencycode);
        testLineItem = TestUtils.createOpportunityLineItem(Opp.Id,testPbe.Id,true);
    }
	
	/*@isTest static void InitialPricingApproval_FeeOpp() {
		//Sales_User = TestUtils.SALES_USER;
         setSalesUser();
        System.runAs(Sales_User)
        {
            List<ProcessInstanceStep> steps = new List<ProcessInstanceStep>();
            setup();
            c_code = getOppCurrencyCode();
            System.debug('Currency code is :' + c_code);
            setUpPricebookInfo();
            AddProduct(c_code);
            //set the Opp Id in the page
            ApexPages.currentPage().getParameters().put('Id',Opp.id);
            ApexPages.currentPage().getParameters().put('buttoname','InitialPricing');
            ApexPages.StandardController Con = new ApexPages.StandardController(Opp);
            APP = new ApprovalProcessServices(Con);
    
            Opp.StageName = 'Establishing Value';
            Opp.Did_not_Fill_Defining_Need_Exit_Criteria__c = true;
            Opp.Did_not_Fill_Pre_Pipeline_Exit_Criteria__c = true;
            Opp.Approval_Type__c = 'Pricing Approval';
            Opp.Type = 'Fee';
            Test.startTest();
            update Opp;
            App.ProcessApprovalButtons();
            Test.stopTest();
            //select the WorkProcessItem to approvethe record
            List<ProcessInstance> process = [Select Id, Status from ProcessInstance where TargetObjectId = :Opp.Id];
            System.debug('Process are:' + process);
            System.assertEquals(1,process.size(),'There should only be 1 record for approval');
           // process[0].Status = 'Approved';
            //update process
            List<ProcessInstanceStep> processInstanceSteps = [SELECT ActorId, OriginalActorId, StepStatus 
                                                                FROM ProcessInstanceStep WHERE ProcessInstanceId = :process[0].id];
    
            System.debug('Process Instance steps are:' + processInstanceSteps);				
            
           // Opportunity TOpp = [Select Id, Name, Approval_Type__c, Awaiting_Approval__c from Opportunity where Id =:Opp.Id];
            System.assertEquals(1,processInstanceSteps.size(),'There are 1 steps in the Initial Pricing');
            //System.assertEquals(TOpp.Awaiting_Approval__c,false,'We expect the Awaiting approval to be false');
        }

	}

	@isTest static void InitialPricingApproval() {
        //Sales_User = TestUtils.SALES_USER;
        setSalesUser();
        System.runAs(Sales_User)
        {
            List<ProcessInstanceStep> steps = new List<ProcessInstanceStep>();
            setup();
            c_code = getOppCurrencyCode();
            setUpPricebookInfo();
            AddProduct(c_code);
            //set the Opp Id in the page
            ApexPages.currentPage().getParameters().put('Id',Opp.id);
            ApexPages.currentPage().getParameters().put('buttoname','InitialPricing');
            ApexPages.StandardController Con = new ApexPages.StandardController(Opp);
            APP = new ApprovalProcessServices(Con);
    
            Opp.StageName = 'Establishing Value';
            Opp.Did_not_Fill_Defining_Need_Exit_Criteria__c = true;
            Opp.Did_not_Fill_Pre_Pipeline_Exit_Criteria__c = true;
            Opp.Approval_Type__c = 'Pricing Approval';
            Opp.Type = 'New Business';
            Test.startTest();
            update Opp;
            App.ProcessApprovalButtons();
            Test.stopTest();
            //select the WorkProcessItem to approvethe record
            List<ProcessInstance> process = [Select Id, Status from ProcessInstance where TargetObjectId = :Opp.Id];
            System.debug('Process are:' + process);
            System.assertEquals(1,process.size(),'There should only be 1 record for approval');
           // process[0].Status = 'Approved';
            //update process
            List<ProcessInstanceStep> processInstanceSteps = [SELECT ActorId, OriginalActorId, StepStatus 
                                                                FROM ProcessInstanceStep WHERE ProcessInstanceId = :process[0].id];
            Opportunity TOpp = [Select Id, Name, Approval_Type__c,First_Approver__c, Awaiting_Approval__c from Opportunity where Id =:Opp.Id];
            List<ProcessInstanceWorkItem> WorkItems = [Select ActorId,OriginalActorId,ProcessInstanceId from ProcessInstanceWorkItem where ProcessInstanceId = :process[0].id];
            System.debug('Process Instance steps are:' + processInstanceSteps);				
            
            System.assertEquals(1,processInstanceSteps.size(),'This is the first step Initial Pricing');
            System.assertEquals(TOpp.First_Approver__c,WorkItems[0].ActorId,'We expect the Fist Approver to be the same as the Acort Id');
        }
	}

	@isTest static void InitialPricingApproval_Negative() {
        //Sales_User = TestUtils.SALES_USER;
        setSalesUser();
        System.runAs(Sales_User)
        {
            List<ProcessInstanceStep> steps = new List<ProcessInstanceStep>();
            setup();
             c_code = getOppCurrencyCode();
            setUpPricebookInfo();
            AddProduct(c_code);
            //set the Opp Id in the page
            ApexPages.currentPage().getParameters().put('Id',Opp.id);
            ApexPages.currentPage().getParameters().put('buttoname','InitialPricing');
            ApexPages.StandardController Con = new ApexPages.StandardController(Opp);
            APP = new ApprovalProcessServices(Con);
    
            Opp.StageName = 'Pre-Pipeline';
            Opp.Approval_Type__c = 'Pricing Approval';
            Opp.Type = 'New Business';
            Test.startTest();
            update Opp;
            App.ProcessApprovalButtons();
            Test.stopTest();
            //select the WorkProcessItem to approvethe record
            List<ProcessInstance> process = [Select Id, Status from ProcessInstance where TargetObjectId = :Opp.Id];
            System.debug('Process are:' + process);
            
            System.assertEquals(0,process.size(),'There should only be 1 record for approval');
        }
	   
	}

	@isTest static void InitialPricingApproval_NoFirstApprover() {
        //Sales_User = TestUtils.SALES_USER;
        setSalesUser();
        System.runAs(Sales_User)
        {
            User TUser = TestUtils.CRM_USER;
            List<ProcessInstanceStep> steps = new List<ProcessInstanceStep>();
            setup();
             c_code = getOppCurrencyCode();
            setUpPricebookInfo();
            AddProduct(c_code);
            //set the Opp Id in the page
            ApexPages.currentPage().getParameters().put('Id',Opp.id);
            ApexPages.currentPage().getParameters().put('buttoname','InitialPricing');
            ApexPages.StandardController Con = new ApexPages.StandardController(Opp);
            APP = new ApprovalProcessServices(Con);
    
            Opp.StageName = 'Pre-Pipeline';
            Opp.Approval_Type__c = 'Pricing Approval';
            Opp.Type = 'New Business';
            Opp.OwnerId = TUser.Id;
            TUser.First_Approver__c = null;	
            Test.startTest();
            update Tuser;
            update Opp;
            App.ProcessApprovalButtons();
            Test.stopTest();
            //select the WorkProcessItem to approvethe record
            List<ProcessInstance> process = [Select Id, Status from ProcessInstance where TargetObjectId = :Opp.Id];
            System.debug('Process are:' + process);
           
            System.assertEquals(0,process.size(),'There should not be 1 record for approval');
        }
	   
	}


	@isTest static void InitialPricingApproval_OppTypeNull() {
        //Sales_User = TestUtils.SALES_USER;
        setSalesUser();
        System.runAs(Sales_User)
        {
            User TUser = TestUtils.CRM_USER;
            List<ProcessInstanceStep> steps = new List<ProcessInstanceStep>();
            setup();
             c_code = getOppCurrencyCode();
            setUpPricebookInfo();
            AddProduct(c_code);
            //set the Opp Id in the page
            ApexPages.currentPage().getParameters().put('Id',Opp.id);
            ApexPages.currentPage().getParameters().put('buttoname','InitialPricing');
            ApexPages.StandardController Con = new ApexPages.StandardController(Opp);
            APP = new ApprovalProcessServices(Con);
    
            Opp.StageName = 'Pre-Pipeline';
            Opp.Approval_Type__c = 'Pricing Approval';
            Opp.OwnerId = TUser.Id;
            Test.startTest();
            update Opp;
            App.ProcessApprovalButtons();
            Test.stopTest();
            //select the WorkProcessItem to approvethe record
            List<ProcessInstance> process = [Select Id, Status from ProcessInstance where TargetObjectId = :Opp.Id];
            System.debug('Process are:' + process);
            
            System.assertEquals(0,process.size(),'There should not be 1 record for approval');
        }
	   
	}*/

	@isTest static void FinanceApproval() {
        //Sales_User = TestUtils.SALES_USER;
        setSalesUser();
        System.runAs(Sales_User)
        {
            setup();
            c_code = getOppCurrencyCode();
            setUpPricebookInfo();
            AddProduct(c_code);
            //set the Opp Id in the page
            ApexPages.currentPage().getParameters().put('Id',Opp.id);
            ApexPages.currentPage().getParameters().put('buttoname','FinanceApproval');
            ApexPages.StandardController Con = new ApexPages.StandardController(Opp);
            APP = new ApprovalProcessServices(Con);
            Opp.Did_not_Fill_Defining_Need_Exit_Criteria__c = true;
            Opp.Did_not_Fill_Pre_Pipeline_Exit_Criteria__c = true;
            Opp.Did_not_Fill_Est_Value_Exit_Criteria__c = true;
            Opp.Did_not_Fill_Nego_Terms_Exit_Criteria__c = true;
            Opp.Did_not_Fill_Verbal_Commit_Exit_Criteria__c = true;
            
            Opp.StageName = 'Signature';
            Opp.Approval_Type__c = 'Finance Approval';
            Opp.Type = 'New Business';
            update Opp;
            
            Test.startTest();
            App.ProcessApprovalButtons();
            Test.stopTest();
            //select the WorkProcessItem to approvethe record
            List<ProcessInstance> process = [Select Id, Status from ProcessInstance where TargetObjectId = :Opp.Id];
            System.debug('Process are:' + process);
            System.assertEquals(1,process.size(),'There should only be 1 record for approval');
          
            List<ProcessInstanceStep> processInstanceSteps = [SELECT ActorId, OriginalActorId, StepStatus 
                                                                FROM ProcessInstanceStep WHERE ProcessInstanceId = :process[0].id];
            Opportunity TOpp = [Select Id, Name, Approval_Type__c, Awaiting_Approval__c, First_Approver__c from Opportunity where Id =:Opp.Id];
            List<ProcessInstanceWorkItem> WorkItems = [Select ActorId,OriginalActorId,ProcessInstanceId from ProcessInstanceWorkItem where ProcessInstanceId = :process[0].id];
            System.debug('Process Instance steps are:' + processInstanceSteps);				
            
             System.assertEquals(1,processInstanceSteps.size(),'This is the first steps in the Finance Approval');
            System.assertEquals(TOpp.First_Approver__c,WorkItems[0].ActorId,'We expect the Fist Approver to be the same as the Approver');
        }
	}



	@isTest static void FinanceApproval_Termination() {
       // Sales_User = TestUtils.SALES_USER;
       setSalesUser();
        System.runAs(Sales_User)
        {
            setup();
             c_code = getOppCurrencyCode();
            setUpPricebookInfo();
            AddProduct(c_code);
            //set the Opp Id in the page
            ApexPages.currentPage().getParameters().put('Id',Opp.id);
            ApexPages.currentPage().getParameters().put('buttoname','FinanceApproval');
            ApexPages.StandardController Con = new ApexPages.StandardController(Opp);
            APP = new ApprovalProcessServices(Con);
    
            Opp.StageName = 'Approval Required';
            Opp.RecordTypeId = TestUtils.TERMINATION_RECORD_TYPE.Id;
            Opp.Reasons_for_Lost_Opportunities__c = 'Lost';
            Opp.Approval_Type__c = 'Finance Approval';
            Opp.Type = 'New Business';
            Test.startTest();
            update Opp;
            App.ProcessApprovalButtons();
            Test.stopTest();
            //select the WorkProcessItem to approvethe record
            List<ProcessInstance> process = [Select Id, Status from ProcessInstance where TargetObjectId = :Opp.Id];
            System.debug('Process are:' + process);
            System.assertEquals(1,process.size(),'There should only be 1 record for approval');
           
            List<ProcessInstanceStep> processInstanceSteps = [SELECT ActorId, OriginalActorId, StepStatus 
                                                                FROM ProcessInstanceStep WHERE ProcessInstanceId = :process[0].id];
    
            System.debug('Process Instance steps are:' + processInstanceSteps);
            Opportunity TOpp = [Select Id, Name, Approval_Type__c, Awaiting_Approval__c, First_Approver__c from Opportunity where Id =:Opp.Id];				
            List<ProcessInstanceWorkItem> WorkItems = [Select ActorId,OriginalActorId,ProcessInstanceId from ProcessInstanceWorkItem where ProcessInstanceId = :process[0].id];
            
            System.assertEquals(1,processInstanceSteps.size(),'This is the first step in the Termination Approval');
            System.assertEquals(TOpp.First_Approver__c,WorkItems[0].ActorId,'We expect the Fist Approver to be the same as the Approver.');
        }
	}


	/*@isTest static void FinanceApproval_NoEntryCriteria() {
        Sales_User = TestUtils.SALES_USER;
        System.runAs(Sales_User)
        {
            setup();
            AddProduct();
            //set the Opp Id in the page
            ApexPages.currentPage().getParameters().put('Id',Opp.id);
            ApexPages.currentPage().getParameters().put('buttoname','FinanceApproval');
            ApexPages.StandardController Con = new ApexPages.StandardController(Opp);
            APP = new ApprovalProcessServices(Con);
    		Opp.Did_not_Fill_Defining_Need_Exit_Criteria__c = true;
            Opp.Did_not_Fill_Pre_Pipeline_Exit_Criteria__c = true;
            Opp.Did_not_Fill_Est_Value_Exit_Criteria__c = true;
            Opp.Did_not_Fill_Nego_Terms_Exit_Criteria__c = true;
            Opp.Did_not_Fill_Verbal_Commit_Exit_Criteria__c = true;
            Opp.StageName = 'Signature';
            Opp.Contract__r.Legal_Stage__c = 'No Review Needed';
            Opp.Reasons_for_Lost_Opportunities__c = 'Lost';
            Opp.Approval_Type__c = 'Pricing Approval';
            Opp.Type = 'New Business';
            
            Test.startTest();
            update Opp;
            App.ProcessApprovalButtons();
            Test.stopTest();
            //select the WorkProcessItem to approvethe record
            List<ProcessInstance> process = [Select Id, Status from ProcessInstance where TargetObjectId = :Opp.Id];
            System.debug('Process are:' + process);
            System.assertEquals(0,process.size(),'There should not be a record for approval');
            //Opportunity TOpp = [Select Id, Name, Approval_Type__c, Awaiting_Approval__c from Opportunity where Id =:Opp.Id];				
           
        }
	  }*/

	  @isTest static void FinanceApproval_NoFirstApprover() {
         // Sales_User = TestUtils.SALES_USER;
        setSalesUser();
        System.runAs(Sales_User)
        {
            setup();
             c_code = getOppCurrencyCode();
            setUpPricebookInfo();
            AddProduct(c_code);
            User TUser = TestUtils.CRM_USER;
            //set the Opp Id in the page
            ApexPages.currentPage().getParameters().put('Id',Opp.id);
            ApexPages.currentPage().getParameters().put('buttoname','FinanceApproval');
            ApexPages.StandardController Con = new ApexPages.StandardController(Opp);
            APP = new ApprovalProcessServices(Con);
    
            Opp.StageName = 'Approval Required';
            Opp.RecordTypeId = TestUtils.TERMINATION_RECORD_TYPE.Id;
            Opp.Reasons_for_Lost_Opportunities__c = 'Lost';
            Opp.Approval_Type__c = 'Pricing Approval';
            Opp.Type = 'New Business';
            Opp.OwnerId = TUser.Id;
            TUser.First_Approver__c = null;	
            Test.startTest();
            update 	TUser;
            update Opp;
            App.ProcessApprovalButtons();
            Test.stopTest();
            //select the WorkProcessItem to approvethe record
            List<ProcessInstance> process = [Select Id, Status from ProcessInstance where TargetObjectId = :Opp.Id];
            System.debug('Process are:' + process);
            System.assertEquals(0,process.size(),'There should not be 1 record for approval');
        }
            
	  }

	  @isTest static void FinanceApproval_OppTypeNull() {
        //Sales_User = TestUtils.SALES_USER;
        setSalesUser();
        System.runAs(Sales_User)
        {
            setup();
             c_code = getOppCurrencyCode();
            setUpPricebookInfo();
            AddProduct(c_code);
		   User TUser = TestUtils.CRM_USER;
    		//set the Opp Id in the page
    		ApexPages.currentPage().getParameters().put('Id',Opp.id);
            ApexPages.currentPage().getParameters().put('buttoname','FinanceApproval');
            ApexPages.StandardController Con = new ApexPages.StandardController(Opp);
            APP = new ApprovalProcessServices(Con);
        
            Opp.StageName = 'Approval Required';
            Opp.RecordTypeId = TestUtils.TERMINATION_RECORD_TYPE.Id;
            Opp.Reasons_for_Lost_Opportunities__c = 'Lost';
            Opp.Approval_Type__c = 'Finance Approval';
            Test.startTest();
            
            update Opp;
            App.ProcessApprovalButtons();
            Test.stopTest();
            //select the WorkProcessItem to approvethe record
            List<ProcessInstance> process = [Select Id, Status from ProcessInstance where TargetObjectId = :Opp.Id];
            System.debug('Process are:' + process);
            System.assertEquals(0,process.size(),'There should not be 1 record for approval');
        }
	  }


	
	
	@isTest static void GeneralReferralApproval_Negative()
	{
        //Sales_User = TestUtils.SALES_USER;
         setSalesUser();
         System.runAs(Sales_User)
         {
             setup();
             c_code = getOppCurrencyCode();
             setUpPricebookInfo();
             AddProduct(c_code);
            //set the Opp Id in the page
            ApexPages.currentPage().getParameters().put('Id',Opp.id);
            ApexPages.currentPage().getParameters().put('buttoname','General Referral');
            ApexPages.StandardController Con = new ApexPages.StandardController(Opp);
            APP = new ApprovalProcessServices(Con);
            Opp.Did_not_Fill_Defining_Need_Exit_Criteria__c = true;
            Opp.Did_not_Fill_Pre_Pipeline_Exit_Criteria__c = true;
           
            Opp.StageName = 'Establishing Value';
            Opp.Approval_Type__c = 'Gen Ref Approval';
            Opp.Type = 'New Business';
            Opp.Gen_Referral_Opp__c = true;
            Opp.Gen_Ref_CRM_Approved_Time_Stamp__c = system.today();
            Opp.Gen_Ref_Originated_By__c = UserInfo.getUserId();
            Test.startTest();
            update Opp;
            App.ProcessApprovalButtons();
            Test.stopTest();
            //select the WorkProcessItem to approvethe record
            List<ProcessInstance> process = [Select Id, Status from ProcessInstance where TargetObjectId = :Opp.Id];
            System.debug('Process are:' + process);
            System.assertEquals(0,process.size(),'There should not any record for approval as RD is missing on the Opp');
         }
	   
	}

	@isTest static void GeneralReferralApproval_Positive()
	{
        //Sales_User = TestUtils.SALES_USER;
        setSalesUser();
        System.runAs(Sales_User)
        {
            setup();
            c_code = getOppCurrencyCode();
           AddProduct(c_code);
            //set the Opp Id in the page
            ApexPages.currentPage().getParameters().put('Id',Opp.id);
            ApexPages.currentPage().getParameters().put('buttoname','General Referral');
            ApexPages.StandardController Con = new ApexPages.StandardController(Opp);
            APP = new ApprovalProcessServices(Con);
            Opp.Did_not_Fill_Defining_Need_Exit_Criteria__c = true;
            Opp.Did_not_Fill_Pre_Pipeline_Exit_Criteria__c = true;
            Opp.Did_not_Fill_Est_Value_Exit_Criteria__c = true;
            
            Opp.StageName = 'Establishing Value';
            Opp.Approval_Type__c = 'Gen Ref Approval';
            Opp.Type = 'New Business';
            Opp.Gen_Referral_Opp__c = true;
            Opp.Gen_Ref_CRM_Approved_Time_Stamp__c = system.today();
            Opp.Gen_Ref_Originated_By__c = UserInfo.getUserId();
            Opp.RD_for_Gen_Ref_Approval__c = UserInfo.getUserId();
            Test.startTest();
            update Opp;
            App.ProcessApprovalButtons();
            Test.stopTest();
            //select the WorkProcessItem to approvethe record
            List<ProcessInstance> process = [Select Id, Status from ProcessInstance where TargetObjectId = :Opp.Id];
            Opportunity TOpp = [Select Id, Name, Approval_Type__c, Awaiting_Approval__c,RD_for_Gen_Ref_Approval__c from Opportunity where Id =:Opp.Id];	
            List<ProcessInstanceStep> processInstanceSteps = [SELECT ActorId, OriginalActorId, StepStatus 
                                                                FROM ProcessInstanceStep WHERE ProcessInstanceId = :process[0].id];
            
            System.debug('Process are:' + process);
            System.assertEquals(1,process.size(),'There should be 1 record for approval.');
            System.assertEquals(TOpp.RD_for_Gen_Ref_Approval__c,processInstanceSteps[0].ActorId, 'We excpet the RD to be the same as the Approver.' );
        }
	}

	@isTest static void GeneralReferralApproval_NoEntryCriteria()
	{
        //Sales_User = TestUtils.SALES_USER;
        setSalesUser();
        System.runAs(Sales_User)
        {
            setup();
            c_code = getOppCurrencyCode();
            setUpPricebookInfo();
           AddProduct(c_code);
            //set the Opp Id in the page
            ApexPages.currentPage().getParameters().put('Id',Opp.id);
            ApexPages.currentPage().getParameters().put('buttoname','General Referral');
            ApexPages.StandardController Con = new ApexPages.StandardController(Opp);
            APP = new ApprovalProcessServices(Con);
    		Opp.Did_not_Fill_Defining_Need_Exit_Criteria__c = true;
            Opp.Did_not_Fill_Pre_Pipeline_Exit_Criteria__c = true;
            Opp.Did_not_Fill_Est_Value_Exit_Criteria__c = true;
            
            Opp.StageName = 'Establishing Value';
            Opp.Approval_Type__c = 'Pricing Approval';
            Opp.Type = 'New Business';
            Opp.Gen_Referral_Opp__c = true;
            Opp.Gen_Ref_CRM_Approved_Time_Stamp__c = system.today();
            Opp.Gen_Ref_Originated_By__c = UserInfo.getUserId();
            Opp.RD_for_Gen_Ref_Approval__c = UserInfo.getUserId();
            
            update Opp;
           
            Test.startTest();
            App.ProcessApprovalButtons();
            Test.stopTest();
            //select the WorkProcessItem to approvethe record
            List<ProcessInstance> process = [Select Id, Status from ProcessInstance where TargetObjectId = :Opp.Id];
            System.debug('Process are:' + process);
            System.assertEquals(0,process.size(),'There should not be a record for approval.');
        }
	   
	}

}