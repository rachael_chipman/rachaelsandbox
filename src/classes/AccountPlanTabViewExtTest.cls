@isTest
private class AccountPlanTabViewExtTest 
{
	static Account testAccount;
	static AccountPlanTabViewExt testController;
	static Account_Plan__c testAccountPlan;
	
	static testMethod void testAccountTabConstructor_acctPlanAvailable()
	{
		Test.startTest();
			setup();
		Test.stopTest();
		
		System.assert(testController.accountPlanAvailable, 'An account plan should be available');
		System.assertEquals(testAccount.Id, testController.myAccount.Id, 'We expect the account property to get set to the account the standard controller is referencing');
		System.assertEquals(testAccountPlan.Id, testController.acctPlan.Id, 'We expect any available account plan for the account to be set to the controllers account plan property');
		
	}
	
	static testMethod void testAccountTabConstructor_noAcctPlanAvailable()
	{
		Test.startTest();
			testAccount = TestUtils.createAccount(TestUtils.PARTNER_RECORD_TYPE.Id, true);
			testController = new AccountPlanTabViewExt(new ApexPages.StandardController(testAccount));
		Test.stopTest();
		
		System.assertEquals(null, testController.acctPlan, 'We expect no account plan to be available');
		System.assert(!testController.accountPlanAvailable, 'An account plan should not be available');
	}
	
	static void setup()
	{
		testAccount = TestUtils.createAccount(TestUtils.PARTNER_RECORD_TYPE.Id, true);
		testAccountPlan = TestUtils.createAccountPlan(testAccount.Id, true);
		testController = new AccountPlanTabViewExt(new ApexPages.StandardController(testAccount));
	}
	
}