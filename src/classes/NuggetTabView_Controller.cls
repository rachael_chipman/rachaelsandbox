public class NuggetTabView_Controller {

Public Nugget__c n;
Public String uId;


  public NuggetTabView_Controller(ApexPages.StandardController controller) {  
 
 n = [select Id, OwnerId, First_Opened_by_Sales_Rep__c, Owner_Role__c from Nugget__c where Id = :ApexPages.currentPage().getParameters().get('Id')];
 
 uId = UserInfo.getUserId();
 
 }
 
 
 
 public pageReference check(){
 
 if(uID == n.OwnerId && n.First_Opened_by_Sales_Rep__c == null && n.Owner_Role__c.contains('Sales Pod')){
 n.First_Opened_by_Sales_Rep__c = system.Now(); 
 update n;
 }
 
 PageReference ref = new PageReference('/apex/NuggetTabView?id='+n.Id);
    ref.setRedirect(true);
    return ref;
 }


}