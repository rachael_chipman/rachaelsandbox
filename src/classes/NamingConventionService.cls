public with sharing class NamingConventionService {

	   public static String accountString = '';
    public static String closeDateString = '';
    public static String partnerString = '';
    public static String abbreviation = '';
    public static String  OppName;
    public static Map<Id,Opportunity> Acc_Opp ;
    public static Map<Id,Opportunity> Partner_Opp;
    public static Map<Id,Opportunity> OppLineItems;
    public static Map<Id,String> OppId_OppName;

    

    public static Map<Id, String> buildAccountString(Set<Id> accountIds)
    {
        Map<Id, String> accountIdToName = new Map<Id, String>();
        List<Account> accounts = [SELECT Id, Name FROM Account WHERE Id IN : accountIds];
        for(Account acct : accounts)
        {
            if(acct.Name.length() > 20)
            {
                accountString = acct.Name.left(20);
            }
            else
            {
                accountString = acct.Name;
            }
            accountIdToName.put(acct.Id, accountString);
        }
        return accountIdToName;
    }

    public static Map<Id, String> buildPartnerAccountString(Set<Id> partnerIds)
    {
        Map<Id, String> partnerIdToName = new Map<Id, String>();
        List<Account> partners = [SELECT Id, Name FROM Account WHERE Id IN : partnerIds];
        for(Account partner : partners)
        {
            if(partner.Name.length() > 15)
            {
                partnerString = partner.Name.left(15);
            }
            else
            {
                partnerString = partner.Name;
            }
            partnerIdToName.put(partner.Id, partnerString);
        }
        return partnerIdToName;
    }
    
    public static Map<Id, String> buildCloseDate(Set<Id> Opportunities) 
    {
        Map<Id,String> OppToCloseDate = new Map<Id,String>();
        Acc_Opp = new Map<Id, Opportunity>();
        Partner_Opp = new Map<Id, Opportunity>();
        OppLineItems = new Map<Id, Opportunity>();
        List<Opportunity> Opplist = [Select Id, Name,PartnerLookup__c,AccountId, CloseDate ,(SELECT Id, PricebookEntry.Product2.Name,Opportunity.PartnerLookup__c, OpportunityId,PricebookEntry.Product2.Abbreviation__c FROM OpportunityLineItems) from Opportunity where Id In:Opportunities];
        if(Opplist!= null)
        {
            DateTime Closedt ; // = system.now();
            for(Opportunity Opp:Opplist)
            {
                Closedt = Opp.CloseDate;
                closeDateString = Closedt.format('MMM') + '-' + Closedt.format('yy');
                OppToCloseDate.put(Opp.Id,closeDateString);
                if(Opp.PartnerLookup__c != NULL)
                {
                    Partner_Opp.put(Opp.PartnerLookup__c,Opp);
                }
                Acc_Opp.put(Opp.AccountId,Opp);
                OppLineItems.put(Opp.Id,Opp);
            }
        }

        return OppToCloseDate;
    }

    public static void buildOppName(Set<Id> Opportunities)
    {
        List<Opportunity> Opplist = new List<Opportunity>();
        OppId_OppName = new Map<Id,String>();
        Map<Id,String> CloseDate = buildCloseDate(Opportunities);
        system.debug('Close Date Map is :' + CloseDate);
        Map<Id,String> AccountName = buildAccountString(Acc_Opp.keySet());
        system.debug('Account Name Map is :' + AccountName);
        Map<Id,String> PartnerName = buildPartnerAccountString(Partner_Opp.keySet());
        system.debug('Partner Name Map is : ' +PartnerName);
        Map<Id,String> ProdNames = new Map<Id,String>();
        List<OpportunityLineItem> OLI_List = new List<OpportunityLineItem>();
        system.debug('OppLine items Map is :' + OppLineItems.values());
        for(Opportunity TOpp: OppLineItems.values())
        {
            OLI_List.addAll(TOpp.OpportunityLineItems);
            
        }

        if(!OLI_List.isEmpty())
        {
            ProdNames = getProductNamesFromOlis(OLI_list);
        }

        system.debug('Prod name Ma is :' + ProdNames);
        for(Opportunity Opp:OppLineItems.values())
        {
            OppName = '';
            accountString = AccountName.get(Opp.AccountId);
            closeDateString = CloseDate.get(Opp.Id);
            partnerString = PartnerName.get(Opp.PartnerLookup__c);
            abbreviation = ProdNames.get(Opp.Id);
            OPPName = accountString + '_' + closeDateString ;
            if(abbreviation != null)
                OPPName += '_' + abbreviation;  
            if(partnerString != null)
            {
                if(abbreviation != NULL)
                {
                    OPPName +=  partnerString;
                }
                else
                {
                    OppName += '_' + partnerString;
                }
            }

            //check of OppName is more than 70 chars long
            if(OppName.length() > 70)
            {
                OppName = OppName.left(70);
            }

            Opp.Name = OppName;
            Opplist.add(Opp);
            
            //OppId_OppName.put(Opp.Id,OppName);
        }
        if( !Opplist.isEmpty())
        {
            try
            {
                system.debug('Updating the Opp ' + Opplist);
                update Opplist;
            }
            catch(DmlException ex)
            {
                //pass in a parameter to identifythe obj type and fix the error handling
                System.debug('DMl Exception occured :' +ex.getMessage());
            }
        }

    }


     public static void filterOpportunity(list<OpportunityLineItem> OLIList)
     {
        Set<Id> OppIds = pluck.Ids('OpportunityId',OLIList);
        System.debug('OppId is :' + OppIds);
        if(OppIds!= null)
        {
            buildOppName(OppIds);
        }
     
     }

     public static void filterAccountName(List<Account> Acclist, Map<Id,Account> AccOldMap)
     {
        Set<Id> AccountIds = new Set<Id>();
        Set<Id> OppIds = new Set<Id>();
        if(Acclist!= null && AccOldMap!= null)
        {
            for(Account TAcc: Acclist)
            {
                Account OldAcc = AccOldMap.get(TAcc.Id);
                if(OldAcc!= null)
                {
                    if(OldAcc.Name != TAcc.Name)
                    {
                        AccountIds.add(TAcc.Id);
                    }
                }
            }
        }

        if(!AccountIds.isEmpty())
        {
            List<Opportunity> Opplist = [Select Id,Name, AccountId from Opportunity where AccountId IN:AccountIds OR PartnerLookup__c IN:AccountIds];
            OppIds = pluck.Ids('Id', Opplist);
            if(!OppIds.isEmpty())
            {
                buildOppName(OppIds);
            }
        }
     }


     public static void filterCloseDateOpp(List<opportunity> Opplist, Map<Id, opportunity> OldOppMap)
     {
        Set<Id> OppIds = new Set<Id>();
        if(Opplist!= null && OldOppMap!= null)
        {
            for(Opportunity TOpp: Opplist)
            {
                Opportunity OldOpp = OldOppMap.get(Topp.Id);
                if(OldOpp!= null)
                {
                    System.debug('Partner Lookup is :' +OldOpp.PartnerLookup__c + '  ' +TOpp.PartnerLookup__c);
                    if((OldOpp.CloseDate!= TOpp.CloseDate) || (OldOpp.AccountId!= TOpp.AccountId)  || (OldOpp.PartnerLookup__c!= TOpp.PartnerLookup__c))
                    {
                        OppIds.add(TOpp.Id);
                    }
                }
            }
        }
        else if(OldOppMap == null) //that means a new opp has been inserted.
        {   
            for(Opportunity TOpp: Opplist)
            {
                OppIds.add(TOpp.Id);
            }
        }

        if(!OppIds.isEmpty())
        {
            buildOppName(OppIds);
        }
     }



    //Need first 20 characters of Account Name + close date + product string + first 15 characters of partner name - 
    //always make sure the total is 70 or less characters

    public static Map<Id, String> getProductNamesFromOlis(List<OpportunityLineItem> olisToProcess)
    {
        Map<Id, String> oppIdToProdAbbrev = new Map<Id, String>();
        //List<OpportunityLineItem> lineItemRecords = [SELECT Id, PricebookEntry.Product2.Name, OpportunityId, Opportunity.PartnerLookup__c FROM OpportunityLineItem WHERE Id IN : olisToProcess];
        system.debug('Show line items: ' + olisToProcess);
        Map<Id, List<OpportunityLineItem>> oppIdToLineItems = groupBy.Ids('OpportunityId', olisToProcess);
        Map<String, String> productNameToAbbreviation = getAbbreviationValues(olisToProcess);

        for(Id oppId : oppIdToLineItems.keySet())
        {
            Set<String> productNames = new Set<String>();
            Boolean partnerPresent = false;
            abbreviation = '';
            for(OpportunityLineItem oli : oppIdToLineItems.get(oppId))
            {
                system.debug('Show oli Name: ' + oli.PricebookEntry.Product2.Name);
                if(!productNames.contains(oli.PricebookEntry.Product2.Abbreviation__c) && productNameToAbbreviation.containsKey(oli.PricebookEntry.Product2.Name))
                {
                    abbreviation += productNameToAbbreviation.get(oli.PricebookEntry.Product2.Name) + '_';
                    productNames.add(oli.PricebookEntry.Product2.Abbreviation__c);
                    if(oli.Opportunity.PartnerLookup__c != NULL)
                    {
                        partnerPresent = true;
                    }
                }
            }
            if(!partnerPresent)
            {
                abbreviation = abbreviation.removeEnd('_');
            }
            //max length of the product is 26. trim after that 26 charc
            if(abbreviation.length() > 26)
            {
                abbreviation = abbreviation.left(26);
                abbreviation += '_';
            }

            oppIdToProdAbbrev.put(oppId, abbreviation);
            productNames.clear();
        }
        return oppIdToProdAbbrev;
    }

    public static Map<String, String> getAbbreviationValues(List<OpportunityLineItem> lineItemRecords)
    {
        Map<String, String> productNameToAbbreviation = new Map<String, String>();
        Set<String> productNames = new Set<String>();
        for(OpportunityLineItem oli : lineItemRecords)
        {
            productNames.add(oli.PricebookEntry.Product2.Name);
        }
        system.debug('Show oli Names: ' + productNames);
        List<Product2> queriedProducts = [SELECT Id, Name, Abbreviation__c FROM Product2 WHERE Name IN : productNames];

        for(Product2 prod : queriedProducts)
        {
            if(prod.Abbreviation__c != NULL)
            {
                productNameToAbbreviation.put(prod.Name, prod.Abbreviation__c);
            }
        }
        return productNameToAbbreviation;
    }
}