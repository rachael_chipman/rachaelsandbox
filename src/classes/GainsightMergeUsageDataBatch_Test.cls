@isTest

private class GainsightMergeUsageDataBatch_Test {

    static testMethod void GainsightMergeUsageDataBatch_Test() {

        Account acct = new Account(name = 'test account');
        insert acct;

        JBCXM__CustomerInfo__c CI = new JBCXM__CustomerInfo__c(JBCXM__Account__c = acct.Id);
        insert CI;

        JBCXM__UsageData__c UD = new JBCXM__UsageData__c(JBCXM__Account__c = acct.Id, JBCXM__Date__c = date.today());
        insert UD;
        

        Test.startTest();
        database.executebatch(new GainsightUpdateUsageDataBatch());
        Test.stopTest();
    }
}