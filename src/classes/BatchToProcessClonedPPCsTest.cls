@isTest
private class BatchToProcessClonedPPCsTest 
{
	 static List<Account> testAccts;
	 static List<Opportunity> testOpps;
	 static List<OpportunityLineItem> testOlis;
	 static final Integer NUM_OF_RECORDS = 20;
	 static final Integer OLIS_PER_OPP = 1;
	 static PricebookEntry testPbe;
	 //static List<PricebookEntry> testPbes;
	 static Product2 testProd;
	 
	 static void setup()
	 {	
	 	testOpps = new List<Opportunity>();
	 	testOlis = new List<OpportunityLineItem>();
	 	testAccts = TestUtils.createAccounts(NUM_OF_RECORDS, TestUtils.MARKETER_RECORD_TYPE.Id, true);
		
		for(Account testAccount: testAccts)
		{
			testOpps.addAll(TestUtils.createOpportunities(1, TestUtils.RESEARCH_RECORD_TYPE.Id, testAccount.Id,false));
		}
		
	 	for(Opportunity opp: testOpps)
	 	{
	 		opp.StageName = 'Terminated Contract';
	 		opp.closeDate = Date.Today();
	 	}
	 	
	    insert testOpps;

	    testProd = TestUtils.createProduct(true);
	    List<PricebookEntry> testPbes = new List<PricebookEntry>();
	    testPbe = TestUtils.createPricebookEntry(testProd.Id, Test.getStandardPriceBookId(), true);
	    testPbes.add(testPbe);

	    for(Opportunity opp: testOpps)
	    {
	    	List<OpportunityLineItem> olis = TestUtils.createOpportunityLineItems( opp.Id, testPbes, Test.getStandardPriceBookId(), OLIS_PER_OPP, false);
	    	testOlis.addAll(olis);
	    }
	    
	    insert testOlis;
	 }
	 
	 

	 
	 static testMethod void testBatch()
	 {
	 	setup();

	 	Test.startTest();
	 		BatchToProcessClonedPPCs newBatch = new BatchToProcessClonedPPCs();
	 		Database.executeBatch(newBatch);
	 	Test.stopTest();
	 	
	 	List<Customer_Product_Detail_Clone__c> result = [SELECT Id FROM Customer_Product_Detail_Clone__c];
	 	System.assertEquals(NUM_OF_RECORDS , result.size(), 'We expect a Customer Product Detail Clone to be created for each opportunity line item' );
	 	
	 }
}