public with sharing class GenRefCRMOwnerChangeLead {
    
    public static User genRefDefaultUser
    {
        get
        {
            if(genRefDefaultUser == NULL)
            {
                genRefDefaultUser = [SELECT Id, Gen_Ref_Default_User__c FROM User WHERE Gen_Ref_Default_User__c = true LIMIT 1];
            }
            return genRefDefaultUser;
        }
    }
    
    public static void processTrigger(List<Lead> updatedLeads, Map<Id, Lead> oldLeads)
    { 
        List<Lead> filteredLeads = filterLeadsonUpdate(updatedLeads, oldLeads);
        if(!filteredLeads.isEmpty())
        {
            processLeads(filteredLeads);
        }
    }
     
    public static void processLeads(List<Lead> filteredLeads)
    {
        Map<Id, Promo_Code__c> leadIdToPromoCode = leadIdToPromoCodeMap(filteredLeads);
        for(Lead lead : filteredLeads)
        {
            if(lead.SSC_Promo_Code__c == NULL || leadIdToPromoCode.isEmpty())
            {
                lead.Referring_CRM2__c = genRefDefaultUser.Id;
                lead.OwnerId = genRefDefaultUser.Id;
                lead.Send_Gen_Ref_Notification__c = true;
                lead.Gen_Ref_Lead__c = true;
                lead.Status = 'Open';
            }
            else if(lead.SSC_Promo_Code__c != NULL && !leadIdToPromoCode.isEmpty() && leadIdToPromoCode.containsKey(lead.Id))
            {
                lead.Referring_CRM2__c = leadIdToPromoCode.get(lead.Id).Account__r.CRM__c;
                lead.Referring_Partner__c = leadIdToPromoCode.get(lead.Id).Account__c;
                lead.OwnerId = leadIdToPromoCode.get(lead.Id).Account__r.CRM__c;
                lead.Send_Gen_Ref_Notification__c = true;
                lead.Gen_Ref_Lead__c = true;
                lead.Status = 'Open';
            }
        }
    }
    
    public static List<Lead> filterLeadsonUpdate(List<Lead> updatedLeads, Map<Id, Lead> oldLeads)
    {
        List<Lead> filteredLeads = new List<Lead>();
        for(Lead updatedLead : updatedLeads)
        {
            Lead oldLead = oldLeads.get(updatedLead.Id);
            if(updatedLead.Lead_Asset_Most_Recent_Detail__c != oldLead.Lead_Asset_Most_Recent_Detail__c && SObjectTriggerServices.isValidCampaign(updatedLead.Lead_Asset_Most_Recent_Detail__c) && updatedLead.Gen_Ref_Lead__c == false)
            {
                filteredLeads.add(updatedLead);
            }
        }
        return filteredLeads;
    }
    
    private static Map<Id, Promo_Code__c> leadIdToPromoCodeMap (List<Lead> filteredLeads)
    {
        Set<String> promoCodeNames = new Set<String>();
        Map<Id, Promo_Code__c> leadIdToPromoCode = new Map<Id, Promo_Code__c>();
        Promo_Code__c promoCode = new Promo_Code__c();
        
        for(Lead lead : filteredLeads)
        {
            if(lead.SSC_Promo_Code__c != NULL)
            {
                promoCodeNames.add(lead.SSC_Promo_Code__c);
            }
        }
        if(!promoCodeNames.isEmpty())
        {
            try
            {
            	promoCode = [SELECT Id, Name, Account__c, Account__r.CRM__c FROM Promo_Code__c WHERE Name IN : promoCodeNames LIMIT 1];
            }
            catch (Exception e)
            {}
        
            for(Lead lead : filteredLeads)
            {
                if(lead.SSC_Promo_Code__c == promoCode.Name)
                {
                    leadIdtoPromoCode.put(lead.Id, promoCode);
                }
            }
        }
        return leadIdtoPromoCode;
    }
    
}