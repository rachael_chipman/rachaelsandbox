@isTest (SeeAllData=true)
public with sharing class RevertOpptoPreviousStageControllerTest {
	
	/*public static Opportunity opp;
	
	public static void setUpOpp()
	{
	   Account acct = new Account();
       acct.name = 'RPTestAcct1';
       insert acct;  
        
       opp = new Opportunity();
       opp.Name = 'Test Opp';
       opp.AccountId = acct.Id;
       opp.CloseDate = system.today() + 7;
       opp.StageName = 'Obtaining Commitment';
       opp.Type = 'New Business';
       opp.Pricebook2Id = '01s000000004NS6AAM';
       opp.RecordTypeId = [SELECT Id FROM RecordType WHERE SObjectType='Opportunity' and DeveloperName='Obtaining_Commitment'].Id;
	}
	
	public static testMethod void ReverttoPreviousStageDemonstrating()
	{
		setupOpp();
		insert opp;
		
		ApexPages.StandardController sc = new ApexPages.standardController(opp);
   		System.CurrentPageReference().getParameters().put('id',opp.Id);
   		RevertOpptoPreviousStageController methods = new RevertOpptoPreviousStageController(sc);
   		
   		test.startTest();
   		methods.PreviousStage();
   		test.stopTest();
   		
   		List<Opportunity> updatedOpps = [SELECT Id, StageName, Approval_Type__c, RD_Pricing_Review__c, RecordTypeId FROM Opportunity WHERE Id = :opp.Id];
   		system.assertEquals('Demonstrating Capabilities', updatedOpps[0].StageName, 'The updated opportunity should have the correct stage');
   		system.assertEquals('Pricing Approval', updatedOpps[0].Approval_Type__c, 'The updated opportunity should have the correct approval type');
		system.assertEquals(NULL, updatedOpps[0].RD_Pricing_Review__c, 'The updated opportunity should have a null RD Pricing Review');
		
	}
	
	public static testMethod void ReverttoPreviousStageInvestigating()
	{
		setupOpp();
		opp.StageName = 'Demonstrating Capabilities';
		opp.RecordTypeId = [SELECT Id FROM RecordType WHERE SObjectType='Opportunity' and DeveloperName='Demonstrating_Capabilities'].Id;
		insert opp;
		
		ApexPages.StandardController sc = new ApexPages.standardController(opp);
   		System.CurrentPageReference().getParameters().put('id',opp.Id);
   		RevertOpptoPreviousStageController methods = new RevertOpptoPreviousStageController(sc);
   		
   		test.startTest();
   		methods.PreviousStage();
   		test.stopTest();
   		
   		List<Opportunity> updatedOpps = [SELECT Id, StageName, Approval_Type__c, RecordTypeId FROM Opportunity WHERE Id = :opp.Id];
   		system.assertEquals('Investigating', updatedOpps[0].StageName, 'The updated opportunity should have the correct stage');
   		system.assertEquals('Pricing Approval', updatedOpps[0].Approval_Type__c, 'The updated opportunity should have the correct approval type');
		
	}
	
	public static testMethod void ReverttoPreviousStageObtainingCommitment()
	{
		setupOpp();
		opp.StageName = 'Pending Certification Qualification';
		opp.RecordTypeId = [SELECT Id FROM RecordType WHERE SObjectType='Opportunity' and DeveloperName='Obtaining_Commitment'].Id;
		insert opp;
		
		ApexPages.StandardController sc = new ApexPages.standardController(opp);
   		System.CurrentPageReference().getParameters().put('id',opp.Id);
   		RevertOpptoPreviousStageController methods = new RevertOpptoPreviousStageController(sc);
   		
   		test.startTest();
   		methods.PreviousStage();
   		test.stopTest();
   		
   		List<Opportunity> updatedOpps = [SELECT Id, StageName, Approval_Type__c, RecordTypeId FROM Opportunity WHERE Id = :opp.Id];
   		system.assertEquals('Obtaining Commitment', updatedOpps[0].StageName, 'The updated opportunity should have the correct stage');
   		system.assertEquals('Finance Approval', updatedOpps[0].Approval_Type__c, 'The updated opportunity should have the correct approval type');
		
	}*/

}