@isTest
private class TestUpdatePeoplesSectionBatch {
    static Account acc;
    static User Tuser;
    static User TAM, CM,IM,EFP,PBD,RSE;
    static AccountTeamMember TM;
    static accountShare TSH;
    static List<User> userslist;
    static testMethod void testUpdatePeopleSection()
    {
        createTestData();
        UpdatePeoplesSectionBatch UPB = new UpdatePeoplesSectionBatch();
        Test.startTest();
        Database.executeBatch(UPB);
        Test.stopTest();
        //verify the data
        Account Tacc = [Select Id, Name, CRM__c,AM__c ,APS_Client_Services__c,Inbox_Insight_Client_Services__c,Original_Salesperson__c,Salesperson__c,
                        Regional_Director__c,CorpDev_Other__c, Technical_Account_Manager__c from Account where Id =:acc.Id];
        System.assertEquals(Tacc.AM__c, Tuser.Id);
       
        System.assertEquals(Tacc.APS_Client_Services__c, EFP.Id);
        System.assertEquals(Tacc.Inbox_Insight_Client_Services__c, IM.Id);
        System.assertEquals(Tacc.CRM__c, CM.Id);
        System.assertEquals(Tacc.CorpDev_Other__c, PBD.Id);
        System.assertEquals(Tacc.Salesperson__c, RSE.Id);
    }
    
    
    static testMethod void NegativetestUpdatePeopleSection()
    {
        createBadTestData();
        UpdatePeoplesSectionBatch UPB = new UpdatePeoplesSectionBatch();
        Test.startTest();
        Database.executeBatch(UPB);
        Test.stopTest();
        //verify the data
        Account Tacc = [Select Id, Name, CRM__c,AM__c ,APS_Client_Services__c,Inbox_Insight_Client_Services__c,Original_Salesperson__c,Salesperson__c,
                        Regional_Director__c,CorpDev_Other__c, Technical_Account_Manager__c from Account where Id =:acc.Id];
        System.assertEquals(Tacc.AM__c, Tuser.Id);
       
       // System.assertEquals(Tacc.APS_Client_Services__c, EFP.Id);
        //System.assertEquals(Tacc.Inbox_Insight_Client_Services__c, IM.Id);
       // System.assertEquals(Tacc.CRM__c, CM.Id);
       // System.assertEquals(Tacc.CorpDev_Other__c, PBD.Id);
        System.assertEquals(Tacc.Salesperson__c, null);
    }
   
   public static void getUser()
   {
       userslist = new list<User>();
       userslist = [SELECT Id FROM User WHERE Profile.Name = 'Sales' AND isActive = true LIMIT 10];
       System.debug('UserName is :' +userslist) ;
      
   }
    
    static void createBadTestData()
    {
         //Create and Insert new Account
        acc = new Account();
        acc.Name = 'Test acc';
        Insert acc;
        List<AccountTeamMember> TMlist = new List<AccountTeamMember>();
        List<AccountShare> AccSharelist = new List<AccountShare>();
        getUser();
        //Create and Insert new Account TeamMembers
               
        Tuser = userslist[6];
        TM = AccountService.createAccTeamMember(acc.Id,'Relationship Manager',Tuser.Id);
        TMlist.add(TM);
       
        //Insert Technical Account Manager
        TAM = userslist[1];
        if(TAM!=null)
        {
            TM = AccountService.createAccTeamMember(acc.Id,'Technical Account Manager',TAM.Id);
            TMlist.add(TM);
        }
        
       
        
         //Insert Salesperson RSE
        RSE = userslist[5];
        if(RSE!= null)
        {
           TM = AccountService.createAccTeamMember(acc.Id,'Regional Director',RSE.Id);
           TMlist.add(TM); 
        }
        
        insert TMlist;
        TSH =  AccountService.CreateAccountShare(acc.Id,Tuser.Id,'Edit','Edit','Edit','Edit');
        AccSharelist.add(TSH);
        
        TSH =  AccountService.CreateAccountShare(acc.Id,RSE.Id,'Edit','Edit','Edit','Edit');
        
        AccSharelist.add(TSH);
        TSH =  AccountService.CreateAccountShare(acc.Id,TAM.Id,'Edit','Edit','Edit','Edit');
       insert AccSharelist;
    }
    
    static void createTestData()
    {
         //Create and Insert new Account
        acc = new Account();
        acc.Name = 'Test acc';
        Insert acc;
        List<AccountTeamMember> TMlist = new List<AccountTeamMember>();
        List<AccountShare> AccSharelist = new List<AccountShare>();
        getUser();
        //Create and Insert new Account TeamMembers
               
        Tuser = userslist[6];
        TM = AccountService.createAccTeamMember(acc.Id,'Relationship Manager',Tuser.Id);
        TMlist.add(TM);
        //Insert Channel Manager
        CM = userslist[0];
        if(CM!=null)
        {
            TM = AccountService.createAccTeamMember(acc.Id,'Channel Manager',CM.Id);
            TMlist.add(TM);
        }
        
        //Insert Technical Account Manager
        TAM = userslist[1];
        if(TAM!=null)
        {
            TM = AccountService.createAccTeamMember(acc.Id,'Technical Account Manager',TAM.Id);
            TMlist.add(TM);
        }
        
        //Insert EFP Client Services
        EFP = userslist[2];
        if(EFP!=null)
        {
            TM = AccountService.createAccTeamMember(acc.Id,'EFP Client Services',EFP.Id);
            TMlist.add(TM);
        }
        
       IM = userslist[3];
        if(IM!= null)
        {
           TM = AccountService.createAccTeamMember(acc.Id,'Inbox Insight Client Services',IM.Id);
           TMlist.add(TM); 
        }
        
        //Insert Provider BD
        PBD = userslist[4];
        if(PBD!= null)
        {
           TM = AccountService.createAccTeamMember(acc.Id,'Provider BD',PBD.Id);
           TMlist.add(TM); 
        }
        
         //Insert Salesperson RSE
        RSE = userslist[5];
        if(RSE!= null)
        {
           TM = AccountService.createAccTeamMember(acc.Id,'RSE Sales Rep',RSE.Id);
           TMlist.add(TM); 
        }
        
        insert TMlist;
        TSH =  AccountService.CreateAccountShare(acc.Id,Tuser.Id,'Edit','Edit','Edit','Edit');
        AccSharelist.add(TSH);
        
        TSH =  AccountService.CreateAccountShare(acc.Id,RSE.Id,'Edit','Edit','Edit','Edit');
        AccSharelist.add(TSH);
        
        TSH =  AccountService.CreateAccountShare(acc.Id,PBD.Id,'Edit','Edit','Edit','Edit');
        AccSharelist.add(TSH);
        
        TSH =  AccountService.CreateAccountShare(acc.Id,IM.Id,'Edit','Edit','Edit','Edit');
        AccSharelist.add(TSH);
        
        TSH =  AccountService.CreateAccountShare(acc.Id,CM.Id,'Edit','Edit','Edit','Edit');
        AccSharelist.add(TSH);
        
        TSH =  AccountService.CreateAccountShare(acc.Id,EFP.Id,'Edit','Edit','Edit','Edit');
        AccSharelist.add(TSH);
        
        TSH =  AccountService.CreateAccountShare(acc.Id,TAM.Id,'Edit','Edit','Edit','Edit');
        AccSharelist.add(TSH);
        insert AccSharelist;
       
            
    }
    
    static testMethod void testScheduleMethod()
    {
        String CRON_EXP = '11 11 11 28 11 ? 2015';
        UpdatePeoplesSectionBatch PB = new UpdatePeoplesSectionBatch();
        Test.startTest();
        String jobId = System.schedule('TestPB', CRON_EXP, PB);
        CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, NextFireTime FROM CronTrigger WHERE id = :jobId];

       // Verify the expressions are the same
        System.assertEquals(CRON_EXP, ct.CronExpression);

       // Verify the job has not run
       System.assertEquals(0, ct.TimesTriggered);
       Test.stopTest();
        
    }

}