public with sharing class RevertOpptoPreviousStageController 
{
    //Will allow us to revert opps to their previous Stage
    
    public Opportunity o {get;set;}
    public Boolean OKtoRevert {get;set;}
    
    public RevertOpptoPreviousStageController(ApexPages.StandardController std)
    {
        o = [SELECT Id, StageName, Approval_Type__c, Type, RD_Pricing_Review__c, RecordTypeId FROM Opportunity WHERE Id = :ApexPages.currentPage().getParameters().get('id')];
        OKtoRevert = false;
    }
    
    /*public pageReference PreviousStage()
    {
        if(o.StageName == 'Obtaining Commitment')
        {
            OKtoRevert = true;
            o.StageName = 'Demonstrating Capabilities';
            o.RD_Pricing_Review__c = null; 
            o.Approval_Type__c = 'Pricing Approval';
            o.RecordTypeId = [SELECT Id FROM RecordType WHERE SObjectType='Opportunity' and DeveloperName='Demonstrating_Capabilities'].Id;
        }
        else if(o.StageName == 'Demonstrating Capabilities' && o.Type != 'Renewal' && o.Type != 'Extension')
        {
            OKtoRevert = true;
            o.StageName = 'Investigating';
            o.Approval_Type__c = 'Pricing Approval';
            o.RD_Pricing_Review__c = null; 
            o.RecordTypeId = [SELECT Id FROM RecordType WHERE SObjectType='Opportunity' and DeveloperName='Investigating'].Id;
        }
        else if(o.StageName == 'Pending Certification Qualification')
        {
            OKtoRevert = true;
            o.StageName = 'Obtaining Commitment';
            o.Approval_Type__c = 'Finance Approval';
            o.RecordTypeId = [SELECT Id FROM RecordType WHERE SObjectType='Opportunity' and DeveloperName='Obtaining_Commitment'].Id;
        }       
        update o;
        pageReference p = new PageReference('/apex/tabView?id='+o.Id);
        return p;       
    }*/

}