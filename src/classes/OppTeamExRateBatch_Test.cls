@IsTest (SeeAllData = true)
private class OppTeamExRateBatch_Test {
private static testmethod void testOppTeamExRateBatch(){

 User user = new user(Alias = 'standt', Email='tes@rp.com', 
        EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
        LocaleSidKey='en_US', ProfileId = '00e0000000734LT', 
        TimeZoneSidKey='America/Los_Angeles', UserName='test@rp.com.rp');
            insert user;
            
     User user2 = new user(Alias = 'standt', Email='tes2@rp.com', 
        EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
        LocaleSidKey='en_US', ProfileId = '00e0000000734LT', 
        TimeZoneSidKey='America/Los_Angeles', UserName='test2@rp.com.rp');
            insert user2;        
            
     Account acct = new Account();
        acct.OwnerId = user.Id;
        acct.name = 'RPTestAcct1';
            Insert acct;
            
    Opportunity opp = new Opportunity();
        opp.AccountId = acct.id; 
        opp.Name = 'RPTestOpp1'; 
        opp.CurrencyIsoCode = 'EUR';
        opp.StageName = 'Finance Approved'; 
        opp.CloseDate = Date.valueOf('2012-02-01');
        opp.OwnerId = user.Id; 
        opp.RecordTypeId = '012000000004u5b';
            insert opp;
            
      OpportunityTeamMember otm = new OpportunityTeamMember (
         OpportunityId = opp.Id,
         UserId = user2.Id,
         TeamMemberRole = 'Sales Rep');
         insert otm;   
         
         String oppid = Opp.Id;
         
      id batchinstanceid = database.executeBatch(new OppTeamExRateBatch('select Id, Exchange_Rate__c, CurrencyIsoCode from OpportunityTeamMember where OpportunityId = \'' + opp.Id + '\'', oppid));

}
}