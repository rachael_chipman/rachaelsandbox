@isTest(SeeAllData=true)
private class Z_SubscriptionChargeTriggerTest {
	
    private static Account testAccount_S;
    private static List<Account> testAccounts_S;
    private static Zuora__CustomerAccount__c testBillingAccount_S;
    private static Zuora__Subscription__c testSub_S;
    private static Zuora__Product__c testProduct_S;

    private static void setupExistingSubscriptions()
    {
        testAccounts_S = new List<Account>();

        testAccounts_S.add(testAccount_S = TestUtils.createAccount(TestUtils.MARKETER_RECORD_TYPE.Id, true));
        system.assert(testAccount_S != NULL);

        testBillingAccount_S = TestUtils.createBillingAccounts(testAccounts_S, true)[0];
        system.assert(testBillingAccount_S != NULL);

        testProduct_S = TestUtils.createZuoraProduct('Email Optimization', true);
        system.assert(testProduct_S != NULL);

        testSub_S = TestUtils.createZ_Subscription(testAccount_S.Id, testBillingAccount_S.Id, testBillingAccount_S.Id, 'Test Subscription', 'TERMED', true);
        system.assert(testSub_S != NULL);
        testSub_S.Zuora__Status__c = 'Active';
        testSub_S.Zuora__Zuora_Id__c = TestUtils.generateZuoraId(32);
        testSub_S.Zuora__TermStartDate__c = system.today().addDays(-1);
        update testSub_S;
    }

    @isTest static void test_valid_zuora_id_and_object() {
    
    setupExistingSubscriptions();

    Zuora__SubscriptionProductCharge__c charge1 = new Zuora__SubscriptionProductCharge__c();
    charge1.Zuora__Zuora_Id__c = '12345678901234567890123456789012';
    charge1.Zuora__Subscription__c = testSub_S.Id;
    charge1.Name = 'Email Intelligence Solutions - Gold - Annually';
    charge1.Zuora__RatePlanName__c = 'Email Intelligence Solutions - Gold - Annually';

    Zuora__SubscriptionProductCharge__c charge2 = new Zuora__SubscriptionProductCharge__c();
    charge2.Zuora__Zuora_Id__c = '12345678901234567890123456789022';
    charge2.Zuora__Subscription__c = testSub_S.Id;
    charge2.Name = 'Certification - License - Annually';
    charge2.Zuora__RatePlanName__c = 'Certification - License - Annually';
    charge2.Zuora__Quantity__c = 250000;



    Test.startTest();

    insert new List<Zuora__SubscriptionProductCharge__c>{ charge1, charge2};

    Test.stopTest();


    charge1 = [SELECT Id, IncludedUnits__c, OveragePrice__c, Product_Family__c, Zuora__Quantity__c from Zuora__SubscriptionProductCharge__c where Zuora__Zuora_Id__c = '12345678901234567890123456789012'];
    charge2 = [SELECT Id, IncludedUnits__c, OveragePrice__c, Product_Family__c, Zuora__Quantity__c from Zuora__SubscriptionProductCharge__c where Zuora__Zuora_Id__c = '12345678901234567890123456789022'];

    testSub_S = [SELECT Id, Certification_Tier__c FROM Zuora__Subscription__c WHERE Zuora__Account__c = :testAccount_S.Id];

    System.assertEquals(charge1.IncludedUnits__c, 100);
    System.assertEquals(charge1.OveragePrice__c, 10);

    System.assertEquals(charge2.IncludedUnits__c, 200);
    System.assertEquals(charge2.OveragePrice__c, 20);

    system.assertEquals('Tools', charge1.Product_Family__c, 'We expect the product family to be populated');
    system.assertEquals('Cert', charge2.Product_Family__c, 'We expect the product family to be populated' );
    system.assertEquals('Tier ' + CertificationOverageGeneration.roundTheTotalVolumeToNextTier(charge2.Zuora__Quantity__c), testSub_S.Certification_Tier__c,'Subscription Tier should be updated.');

	}


  @isTest static void test_invalid_zuora_id_and_object() {
    Zuora__SubscriptionProductCharge__c charge1 = new Zuora__SubscriptionProductCharge__c();
    charge1.Zuora__Zuora_Id__c = 'invalidId';

    Test.startTest();
    insert charge1;
    Test.stopTest();

    charge1 = [SELECT Id, IncludedUnits__c, OveragePrice__c from Zuora__SubscriptionProductCharge__c where Zuora__Zuora_Id__c = 'invalidId'];

    System.assertEquals(charge1.IncludedUnits__c, null);
    System.assertEquals(charge1.OveragePrice__c, null);
  }
	

	@isTest static void test_more_that_ninety_charges() {
		Zuora__SubscriptionProductCharge__c charge1 = new Zuora__SubscriptionProductCharge__c();
    charge1.Zuora__Zuora_Id__c = '12345678901234567890123456789012';

    Zuora__SubscriptionProductCharge__c charge2 = new Zuora__SubscriptionProductCharge__c();
    charge2.Zuora__Zuora_Id__c = '12345678901234567890123456789022';
    
    List<Zuora__SubscriptionProductCharge__c> chargeList = new List<Zuora__SubscriptionProductCharge__c>();
    chargeList.add(charge1);
    while (chargeList.size() < 80) {
      Zuora__SubscriptionProductCharge__c dummyCharge = new Zuora__SubscriptionProductCharge__c();
      dummyCharge.Zuora__Zuora_Id__c = 'dummyChargeId';
      chargeList.add(dummyCharge);
    }

    chargeList.add(charge2);

    Test.startTest();
    insert chargeList;
    Test.stopTest();

    charge1 = [SELECT Id, IncludedUnits__c, OveragePrice__c from Zuora__SubscriptionProductCharge__c where Zuora__Zuora_Id__c = '12345678901234567890123456789012'];
    charge2 = [SELECT Id, IncludedUnits__c, OveragePrice__c from Zuora__SubscriptionProductCharge__c where Zuora__Zuora_Id__c = '12345678901234567890123456789022'];

    System.assertEquals(charge1.IncludedUnits__c, 100);
    System.assertEquals(charge1.OveragePrice__c, 10);

    System.assertEquals(charge2.IncludedUnits__c, 200);
    System.assertEquals(charge2.OveragePrice__c, 20);


    chargeList = [SELECT Id, IncludedUnits__c, OveragePrice__c from Zuora__SubscriptionProductCharge__c where Zuora__Zuora_Id__c = 'dummyChargeId'];
    for (Zuora__SubscriptionProductCharge__c charge : chargeList) {
      System.assertEquals(charge.IncludedUnits__c, null);
      System.assertEquals(charge.OveragePrice__c, null);
    }
	}
	
}