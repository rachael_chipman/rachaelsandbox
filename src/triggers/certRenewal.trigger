/*  This trigger serves to update an account's "Renewed to Date" field when a cert deal closes. */

trigger certRenewal on Opportunity (after update) {
   /*list<account> toUpdate = new list<account>();

    // only look at financed approved opps with cert
    if( (trigger.new[0].product_certification__c == true || trigger.new[0].Product_SSC_Referral_Child__c == true) && trigger.new[0].stagename == 'Finance Approved' && trigger.new[0].type != 'New Business') {

        // grab the *earliest* end date from the cert product (in case of multi-year deals)
        list<opportunitylineitem> olis = [select end_date__c,product_code__c from opportunitylineitem WHERE (product_code__c = '004-CERT-FEE0' OR product_code__c = '005-CERT-REFC') and opportunityid = :trigger.new[0].id ORDER BY end_date__c ASC NULLS LAST];

        // only try to push the end date if there is one.. especially since cert dates are often unknown
        if(olis.size() > 0) {
        
            // if the line item's end date doesn't match the account's renewed to date, push it there
            account a = [select renewed_to_date__c from account where id = :trigger.new[0].accountid limit 1];
        
            if(a.renewed_to_date__c != olis[0].end_date__c) {
                a.renewed_to_date__c = olis[0].end_date__c;
                try { update a; }
                catch(exception e) { system.debug(e); }
            }

        }
    }*/

}