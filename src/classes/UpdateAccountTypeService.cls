public with sharing class UpdateAccountTypeService {
	
	public static void processTrigger ( List<Account> newAccounts, Map<Id, Account> oldAccounts )
	{
		List<Account> filteredAccts = filterAccountsonUpdate ( newAccounts, oldAccounts);
		if(!filteredAccts.isEmpty())
		{
			temporaryFix ( filteredAccts );
		}
	}
	
	/*public static void processAccounts ( List<Account> filteredAccounts , List<Account> newAccounts)
	{
		for(Account acct : filteredAccounts)
		{
			if(acct.Count_of_All_Opportunities__c == 0)
			{
				acct.Type = 'Lead Level Prospect';
			}
			if(acct.Count_of_Open_Opportunities__c > 0 && 
				acct.EFP_Active_Subscriptions__c == 0 && 
				acct.Cert_Active_Subscriptions__c == 0 && acct.EIS_Active_Subscriptions__c == 0 && 
				acct.Tools_Child_Active_Subscriptions__c == 0 && 
				acct.Partner_Framework_Active_Subscriptions__c == 0 && 
				acct.Insight_Active_Subscriptions__c == 0 && 
				acct.ProServ_Active_Subscriptions__c == 0 )
			{
				acct.Type = 'Active Prospect';
			}
			if(acct.EFP_Active_Subscriptions__c == 0 && 
				acct.Cert_Active_Subscriptions__c == 0 && 
				acct.EIS_Active_Subscriptions__c == 0 && 
				acct.Partner_Framework_Active_Subscriptions__c == 0 && 
				acct.Insight_Active_Subscriptions__c == 0 && 
				acct.ProServ_Active_Subscriptions__c == 0 && 
				acct.Count_of_Open_Opportunities__c == 0 && 
				acct.Count_of_All_Opportunities__c != 0)
			{
				acct.Type = 'Inactive Prospect';
			}
			if(acct.EFP_Active_Subscriptions__c > 0 || acct.Cert_Active_Subscriptions__c > 0 || 
			(acct.EIS_Active_Subscriptions__c > 0 && acct.Tools_Child_Active_Subscriptions__c == 0) || 
			acct.Partner_Framework_Active_Subscriptions__c > 0 || 
			acct.Insight_Active_Subscriptions__c > 0 || 
			acct.ProServ_Active_Subscriptions__c > 0)
			{
				acct.Type = 'Current Client/Partner';
			}
			if(acct.EFP_Active_Subscriptions__c == 0 && acct.Cert_Active_Subscriptions__c == 0 && (acct.EIS_Active_Subscriptions__c > 0 && acct.Tools_Child_Active_Subscriptions__c > 0) && acct.Partner_Framework_Active_Subscriptions__c == 0 && acct.Insight_Active_Subscriptions__c == 0 && acct.ProServ_Active_Subscriptions__c == 0)
			{
				acct.Type = 'Passive Client';
			}
		}
	}*/

	public static void temporaryFix(List<Account> filteredAccounts)
	{
		for(Account acct : filteredAccounts)
		{
			if(acct.Count_of_All_Opportunities__c == 0)
			{
				acct.Type = 'Lead Level Prospect';
			}
			else if(acct.Total_Active_Subscriptions__c > 0 && 
				(acct.Tools_Child_Active_Subscriptions__c == 0 ||
				((acct.Tools_Child_Active_Subscriptions__c + acct.EIS_Active_Subscriptions__c) < acct.Total_Active_Subscriptions__c)))
			{
				acct.Type = 'Current Client/Partner';
			}
			else if(acct.Total_Active_Subscriptions__c > 0 && 
				((acct.Tools_Child_Active_Subscriptions__c + acct.EIS_Active_Subscriptions__c) == acct.Total_Active_Subscriptions__c))
			{
				acct.Type = 'Passive Client';
			}
			else if(acct.Count_of_Open_Opportunities__c > 0 && acct.Total_Active_Subscriptions__c == 0)
			{
				acct.Type = 'Active Prospect';
			}
			else if(acct.Total_Active_Subscriptions__c == 0 &&
					acct.Count_of_Open_Opportunities__c == 0 &&
					acct.Count_of_All_Opportunities__c != 0)
			{
				acct.Type = 'Inactive Prospect';
			}
		}
	}
	
	public static List<Account> filterAccountsonUpdate ( List<Account> newAccounts, Map<Id, Account> oldAccounts )
	{
		List<Account> filteredAccounts = new List<Account>();
		
		for(Account acct : newAccounts)
		{
			Account oldAcct = oldAccounts.get(acct.Id);
			
			if( acct.Count_of_All_Opportunities__c == 0 || acct.Count_of_All_Opportunities__c != 0 )
			{
				filteredAccounts.add(acct);
			}
		}
		system.debug('Do we have an account in the filter ' + filteredAccounts);
		return filteredAccounts;
	}
	
	public static void populateAccountTypeonInsert ( List<Account> newAccounts )
	{
		for(Account newAccount : newAccounts)
		{
			newAccount.Type = 'Lead Level Prospect';
		}
	}

	public static Map<Id,Account> accountsForContactAddressUpdateMap(List<Account> newAccounts, Map<Id, Account> oldAccMap)
	{
		Map<Id, Account> accMap = new Map<Id, Account>();
		Account oldAccount;

		for(Account acc : newAccounts)
		{
			if(oldAccMap != NULL)
			{
				oldAccount = oldAccMap.get(acc.Id);
				if((acc.BillingStreet != oldAccount.BillingStreet) ||
					(acc.BillingCity != oldAccount.BillingCity) ||
					(acc.BillingState != oldAccount.BillingState) ||
					(acc.BillingCountry != oldAccount.BillingCountry) ||
					(acc.BillingPostalCode != oldAccount.BillingPostalCode) ||
					(acc.ShippingStreet != oldAccount.ShippingStreet) ||
					(acc.ShippingCity != oldAccount.ShippingCity) ||
					(acc.ShippingState != oldAccount.ShippingState) ||
					(acc.ShippingCountry != oldAccount.ShippingCountry) ||
					(acc.ShippingPostalCode != oldAccount.ShippingPostalCode) ||
					(acc.Zuora_Information_Locked__c != oldAccount.Zuora_Information_Locked__c))
				{
					accMap.put(acc.Id, acc);
				}
			}
			else if(oldAccMap == NULL &&
				(acc.BillingStreet != NULL) ||
				(acc.BillingCity != NULL) ||
				(acc.BillingState != NULL) ||
				(acc.BillingCountry != NULL) ||
				(acc.BillingPostalCode != NULL) ||
				(acc.ShippingStreet != NULL) ||
				(acc.ShippingState != NULL) ||
				(acc.ShippingCountry != NULL) ||
				(acc.ShippingPostalCode != NULL))
			{
				accMap.put(acc.Id, acc);
			}
		}
		return accMap;
	}

	public static void updateMailingAddressOnContact(Map<Id,Account> accMap)
	{
		if(accMap!= null)
		{
			String conRole;
			Set<Id> accIds = accMap.keySet();
			List<Contact> conlist = new List<Contact>();
			List<Contact> contactlist = [Select Id, Name, AccountId,Bypass_Address_Validation__c, MailingStreet,MailingCity,MailingState,MailingPostalCode,MailingCountry,Invoicing_Contact_Type__c, Account.BillingCity,Account.BillingState, Account.BillingStreet,
			                             Account.BillingPostalCode, Account.BillingCountry, Account.ShippingCity,Account.ShippingState,Account.ShippingPostalCode,
			                             Account.ShippingCountry,Account.ShippingStreet from Contact where AccountId in: accIds];
			if(contactlist!=null || !contactlist.isEmpty())                             
			{
		        for(Contact Tcon: contactlist )
		        {
		        	account tacc = accMap.get(Tcon.AccountId);
		        	if(tacc!= null)
		        	{
		        		conRole = Tcon.Invoicing_Contact_Type__c;
		        		if(conRole!= null && (conRole.containsIgnoreCase('Bill To') && (Tcon.MailingStreet!= tacc.BillingStreet || Tcon.MailingState != tacc.BillingState
		        			|| Tcon.MailingCountry!= tacc.BillingCountry || Tcon.MailingCity != tacc.BillingCity || 
		        			Tcon.MailingPostalCode!= tacc.BillingPostalCode)))
		        		{
		        			Contact tempcon = Tcon;
		        			tempcon.MailingCity = tacc.BillingCity;
		        			tempcon.MailingCountry = tacc.BillingCountry;
		        			tempcon.MailingState = tacc.BillingState;
		        			tempcon.MailingStreet = tacc.BillingStreet;
		        			tempcon.MailingPostalCode = tacc.BillingPostalCode;
                            tempcon.Bypass_Address_Validation__c = true;
		        			conlist.add(tempcon);
		        		}
		        		else if(conRole!= null && (conRole.containsIgnoreCase('Sold To') &&  !conRole.containsIgnoreCase('Bill To')) && (Tcon.MailingStreet!= tacc.ShippingStreet || Tcon.MailingState != tacc.ShippingState
		        			|| Tcon.MailingCountry!= tacc.ShippingCountry || Tcon.MailingCity != tacc.ShippingCity || 
		        			Tcon.MailingPostalCode!= tacc.ShippingPostalCode))
		        		{
		        			System.debug(' In the Sold To the contact' +Tcon);
		        			Contact tempcon = Tcon;
		        			tempcon.MailingCity = tacc.ShippingCity;
		        			tempcon.MailingCountry = tacc.ShippingCountry;
		        			tempcon.MailingState = tacc.ShippingState;
		        			tempcon.MailingStreet = tacc.ShippingStreet;
		        			tempcon.MailingPostalCode = tacc.ShippingPostalCode;
		        			tempcon.Bypass_Address_Validation__c = true;
		        			conlist.add(tempcon);
		        		}
		        	}
		        }
		        if(!conlist.isEmpty())
		        {
		        	try
		        	{
		        		//System.debug(' updating the contact' +conlist);
		        		update conlist;
		        	}
		        	catch(System.DMlException Ex)
		        	{
		        		for(Integer i=0;i<Ex.getNumDml();i++)
		        		{
		        			//System.debug('Error occurred in  updating the contact' );
		        			Id recId = Ex.getDmlId(i);
		        			System.debug('Id of the is contact' +recId );
		        			for(Contact tempcon: contactlist)
		        			{
		        				if(tempcon.Id == recId)
		        				{
		        					Account acc = accMap.get(tempcon.AccountId);
		        					acc.addError(Ex.getDmlMessage(i) + ' for Contact :' + tempcon.Name );
		        					System.debug('Error in updating the contact' + Ex.getDmlMessage(i));
		        				}
		        			}

		        		}

		        	}
		        }
		    }
	    }
	}

	public static void processContactAddressUpdates(List<Account> newAccounts, Map<Id, Account> oldAccountMap)
	{
		Map<Id, Account> accountIdToAccountMap = accountsForContactAddressUpdateMap(newAccounts, oldAccountMap);
		if(!accountIdToAccountMap.isEmpty() && accountIdToAccountMap != NULL)
		{
			updateMailingAddressOnContact(accountIdToAccountMap);
		}
	}

}