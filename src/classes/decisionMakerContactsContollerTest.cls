@isTest

private class decisionMakerContactsContollerTest {
    
    static Contact decisionMaker;
	static Account a;

    static void setUpAccount(){
        
        a = new Account(Name = 'Test Account');
        insert a;
        
        decisionMaker = new Contact ();
        decisionMaker.FirstName = 'Decision';
        decisionMaker.LastName = 'Maker';
        decisionMaker.ContactsRole__c = 'Decision Maker';
        decisionMaker.Contact_Status__c = 'Current';
        decisionMaker.AccountId = a.Id;
        
    }
    
    static testMethod void testDecisionMakers(){
        
        setUpAccount();
        
        ApexPages.StandardController sc = new ApexPages.standardController(a);
        System.currentPagereference().getParameters().put('id',a.Id);
        decisionMakerContactsController methods = new decisionMakerContactsController();
        
        test.startTest();
        insert decisionMaker;
        test.stopTest();
        
        Account acct = [SELECT Id, Name FROM Account WHERE Id =: a.Id Limit 1][0];
        
        Contact conts = [SELECT Id, ContactsRole__c, Contact_Status__c, AccountId FROM Contact WHERE Id=: decisionMaker.Id Limit 1][0];
        
        System.assertEquals(a.id, conts.AccountId, 'We expect Accounts to match');
        
        System.assertEquals(decisionMaker.ContactsRole__c, conts.ContactsRole__c, 'We expect the Contact Role to be the same');

    }

}