trigger SubmitFromTriggerCase on Case (after insert, after update) {

    list<Case> updateCases = new list<Case>();
    list<Approval.ProcessSubmitRequest> reqList = new list<Approval.ProcessSubmitRequest>();

    for(Case case1 : trigger.new){
        if( case1.submit_from_trigger__c == 'Yes'){
        
            Case placeHolderCase = new Case(id = case1.id, submit_from_trigger__c = 'No');
            updateCases.add(placeHolderCase);
            approval.ProcessSubmitRequest req1 = new Approval.ProcessSubmitRequest();
            req1.setObjectId(case1.id);
            reqList.add(req1);

        }
    }
    approval.ProcessResult[] result = Approval.process(reqList);
    Update updateCases;
       
}