public without sharing class AccountOwnerChange {
    
    public static User ELOQUA 
    {
        get
        {
            if(ELOQUA == NULL)
            {
                ELOQUA = [SELECT Id FROM User WHERE Name = 'Eloqua Automation' LIMIT 1];
            }
            return ELOQUA;
        }
        private set;
    }
    
   /*public static User CS_USER 
    {
        get
        {
            if( CS_USER == NULL )
            {
                CS_USER = [SELECT Id, isActive FROM User WHERE Name = 'Salesforce Automation' AND Profile.Name = 'Exec Admin' LIMIT 1];
            }
            return CS_USER;
        }
        private set;
    }*/
    
    public static void processTrigger(List<Account> newAccounts, Map<Id, Account> oldAccounts)
    {
        List<Account> filteredAccts = filterAccountsOnUpdateAndInsert(newAccounts, oldAccounts);
        if(!filteredAccts.isEmpty())
        {
            processAccounts(filteredAccts);
        }
    }
    
    public static void processAccounts(List<Account> filteredAccounts)
    {
        //Map<Id, String> acctIdtoPodNumber = getThePodNumber(filteredAccounts);
        Map<Id, ESP_Information__c> acctIdtoESPInformation = getTheESPInformation(filteredAccounts);
        //Map<String, Pod_Detail__c> podNameToPodDetail = new Map<String, Pod_Detail__c>();
        Map<Id, User> accountPersonToUser = getIsActiveInfo(filteredAccounts);
        
        /*if(!acctIdtoPodNumber.values().isEmpty())
        {
            podNameToPodDetail = FindThePodHelperClass.podNumberToPodDetail(new Set<String> (acctIdtoPodNumber.values()));
        }*/ 
		
        for(Account acc : filteredAccounts)
        {
            if( acc.RecordTypeId == TestUtils.MARKETER_RECORD_TYPE.Id || acc.RecordTypeId == TestUtils.PARTNER_RECORD_TYPE.Id )
            {
                if( acc.CorpDev_Other__c != null)
                {
                   if(!accountPersonToUser.isEmpty() &&
                   accountPersonToUser.containsKey(acc.CorpDev_Other__c) &&
                   accountPersonToUser.get(acc.CorpDev_Other__c).isActive)
                    {
                        acc.OwnerId = acc.CorpDev_Other__c;
                    }
                }                   
                else if(acc.Type == 'Current Client/Partner' || acc.Type == 'Passive Client')
                {
                    if( acc.AM__c != null)
                    {
                        if(!accountPersonToUser.isEmpty() &&
                           accountPersonToUser.containsKey(acc.AM__c) &&
                           accountPersonToUser.get(acc.AM__c).isActive)
                        {
                            acc.OwnerId = acc.AM__c;
                        }
                    }
                    else if( acc.CRM__c != null)
                    {
                        if(!accountPersonToUser.isEmpty() &&
                            accountPersonToUser.containsKey(acc.CRM__c) &&
                            accountPersonToUser.get(acc.CRM__c).isActive)
                        {
                            acc.OwnerId = acc.CRM__c;
                        }
                    }                        
                    else  if( acc.Salesperson__c != null)
                    {
                        if(!accountPersonToUser.isEmpty() &&
                             accountPersonToUser.containsKey(acc.Salesperson__c) &&
                             accountPersonToUser.get(acc.Salesperson__c).isActive)
                        {
                            acc.OwnerId = acc.Salesperson__c;
                        }
                    }                         
                    else if( acc.AM__c == null &&
                            acc.CRM__c == null && 
                            acc.Salesperson__c == null &&
                            acc.Regional_Director__c != null)
                        {
                            if(!accountPersonToUser.isEmpty() &&
                                accountPersonToUser.containsKey(acc.Regional_Director__c) &&
                                accountPersonToUser.get(acc.Regional_Director__c).isActive)
                        {
                            acc.OwnerId = acc.Regional_Director__c;
                        }
                    }                           
                    else if(acc.AM__c == null && 
                            acc.CRM__c == null && 
                            acc.Salesperson__c == null && 
                            acc.Regional_Director__c == null)
                        {
                             /*if(CS_USER.isActive)
                             {
                                 acc.OwnerId = CS_USER.Id;//Client Services
                             }*/
                             acc.OwnerId = TestUtils.SFORCE_AUTO.Id;
                        }
                }
                else if(acc.Type != 'Current Client/Partner' && acc.Type != 'Passive Client')
                {
                    if( acc.Salesperson__c != null)
                    {
                        if(!accountPersonToUser.isEmpty() &&
                        accountPersonToUser.containsKey(acc.Salesperson__c) &&
                        accountPersonToUser.get(acc.Salesperson__c).isActive)
                        {
                            acc.OwnerId = acc.Salesperson__c;
                        }
                    }                        
                    else if( acc.CRM__c != null)
                    {
                        if(!accountPersonToUser.isEmpty() &&
                            accountPersonToUser.containsKey(acc.CRM__c) &&
                            accountPersonToUser.get(acc.CRM__c).isActive)
                        {
                            acc.OwnerId = acc.CRM__c;
                        }
                    }                            
                    else if( acctIdtoESPInformation.containsKey(acc.Id) &&  
                            acc.CRM__c == null && 
                            acc.Salesperson__c == null &&
                            acctIdtoESPInformation.get(acc.Id).ESP__r.CRM__c != null)
                    {
                        if(acctIdtoESPInformation.get(acc.Id).ESP__r.CRM__r.isActive)
                        {
                            acc.OwnerId = acctIdtoESPInformation.get(acc.Id).ESP__r.CRM__c;
                        }
                    }                   
                    else if( acc.Salesperson__c == null && 
                            acc.CRM__c == null && 
                            acctIdtoESPInformation.isEmpty() && 
                            acc.Regional_Director__c != null)
                    {
                        if(!accountPersonToUser.isEmpty() && 
                            accountPersonToUser.containsKey(acc.Regional_Director__c) &&
                            accountPersonToUser.get(acc.Regional_Director__c).isActive)
                        {
                            acc.OwnerId = acc.Regional_Director__c;
                        }
                    }
                    else
                    {
                        acc.OwnerId = ELOQUA.Id;
                    }
                }               
                else
                {
                    acc.OwnerId = ELOQUA.Id;
                }
            }
        }
    }
    
    public static List<Account> filterAccountsOnUpdateAndInsert(List<Account> newAccounts, Map<Id, Account> oldAccounts)
    {
        List<Account> filteredAccounts = new List<Account>();
        
        for(Account acc : newAccounts)
        {
            Account oldAccount;
            if( oldAccounts != NULL )
            {
                oldAccount = oldAccounts.get(acc.Id);
            }
            if( ( oldAccount == NULL ) 
            || ( acc.Update__c == true && oldAccount.Update__c == false ) 
            || ( acc.AM__c != oldAccount.AM__c ) 
            || ( acc.CRM__c != oldAccount.CRM__c ) 
            || ( acc.Salesperson__c != oldAccount.Salesperson__c )
            || ( acc.Count_of_ESPs__c != oldAccount.Count_of_ESPs__c ) 
            || ( acc.Type != oldAccount.Type ) 
            || ( acc.CorpDev_Other__c != oldAccount.CorpDev_Other__c ) 
            || ( acc.Regional_Director__c != oldAccount.Regional_Director__c )
            || ( acc.OwnerId != oldAccount.OwnerId ) )
            {
                filteredAccounts.add(acc);
            }
        }
        system.debug('What account is in the filter?'+filteredAccounts+'Are there old Accounts?'+oldAccounts);
        return filteredAccounts;
    }
    
    /*private static Map<Id, String> getThePodNumber(List<Account> filteredAccounts)
    {
        Map<Id, String> accountIdtoPodNumber = new Map <Id, String>(); 
        for(Account acct : filteredAccounts)
        {
            accountIdtoPodNumber.put(acct.Id, acct.Internal_Pod_Number__c);
        }
        return accountIdtoPodNumber;
    }*/
    
    private static Map<Id, ESP_Information__c> getTheESPInformation (List<Account> filteredAccounts)
    {
        Map<Id, ESP_Information__c> accountIdtoESPInfo = new Map<Id, ESP_Information__c>();
        List<Account> espFilterAccounts = new List<Account>();
        for(Account acct : filteredAccounts)
        {
            if(acct.Id != NULL)
            {
                if(acct.Type != 'Current Client / Partner' && acct.Type != 'Passive Client')
                {
                    if(acct.Salesperson__c == NULL && acct.CRM__c == NULL && acct.Count_of_ESPs__c > 0)
                    {
                        espFilterAccounts.add(acct);
                    }
                }
            }
        }
        system.debug('Show the esp accounts: ' + espFilterAccounts);
        if(!espFilterAccounts.isEmpty())
        {
            List<ESP_Information__c> esps = [SELECT Id, Account__c, ESP__r.CRM__c, ESP__r.CRM__r.isActive FROM ESP_Information__c WHERE Account__c IN : espFilterAccounts];
        
            if(!esps.isEmpty())
            {
                for(Account acct : filteredAccounts)
                {
                    for(ESP_Information__c esp : esps)
                    {
                        if(esp.Account__c == acct.Id)
                        {
                            accountIdtoESPInfo.put(acct.Id, esp);
                        }
                    }
                }
            }
        }
        return accountIdtoESPInfo;
    }
    
    private static Map<Id, User> getIsActiveInfo (List<Account> filteredAccounts)
    {
        Map<Id, User> accountLookupToUser = new Map<Id, User>();
        Set<Id> userIds = new Set<Id>();
 
        for(Account acc : filteredAccounts)
        {
            if(acc.AM__c != NULL)
            {
                userIds.add(acc.AM__c);
            }
            if(acc.CRM__c != NULL)
            {
                userIds.add(acc.CRM__c);
            }
            if(acc.Salesperson__c != NULL)
            {
                userIds.add(acc.Salesperson__c);
            }
            if(acc.Regional_Director__c != NULL)
            {
                userIds.add(acc.Regional_Director__c);
            }
            if(acc.CorpDev_Other__c != NULL)
            {
                userIds.add(acc.CorpDev_Other__c);
            }
            
        }
        
        List<User> usersInAccounts = [SELECT Id, isActive FROM User WHERE Id IN : userIds];
        
        for(User person : usersInAccounts)
        {
            accountLookupToUser.put(person.Id, person);
        }
        return accountLookupToUser;
    }

}