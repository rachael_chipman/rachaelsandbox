public class channelInformationPageController {

    public opportunity o {get;set;}
    public Boolean editMode {get;set;}
    public id crm {get;set;}
    public list<selectOption> partnerContacts {get;set;}
    public list<selectOption> partnerContacts2 {get;set;}
    public list<selectOption> partnerAccounts {get;set;}
    public Boolean editAccess {get;set;}
    public Boolean inApproval {get;set;}
    public Boolean isAdmin {get;set;}
    public Boolean ETSupport {get;set;}
    public String aName {get;set;}
    public Boolean CRMUser {get;set;}
    

    public channelInformationPageController(ApexPages.standardController std) {

        editMode = false;
        if(o == null) o = [
        SELECT name,OwnerId,stagename,Awaiting_Approval__c,Account.Customer_Relationship_Manager_Primary__c,AccountId,account.crm__c,Account.Name,Customer_Relationship_Manager_Primary__c, partnerlookup__c, partner_contact__c,Deal_Closing_Through__c,
        Nature_of_Involvement__c, Type_of_Involvementtext__c, Partner_Commission__c,Who_is_Invoiced__c,CRM_User__c,partnerlookup__r.crm__c, partnerlookup__r.Name,Direct_Child_Link_Approved__c,Parent_Realm_ID__c,Channel_Child_Opp__c,Regional_director__c,Account_director__c,
        First_Approver__c, Second_Approver__c
            FROM opportunity WHERE id = :ApexPages.currentPage().getParameters().get('id')
        ];
        

        aName = (
        o.PartnerLookup__r.Name != null ? o.PartnerLookup__r.Name:
        o.PartnerLookup__r.Name == null ? 'No Partner': '');

        
        // determine if current user can make changes

        user u = [select id,profile.name, ET_Channel_Support__c from user where id = :userinfo.getuserid()];

        if(u.profile.name == 'Exec Admin' || u.profile.name == 'Finance') { 
            isAdmin = true;
        } else { isAdmin = false; }
        
        if(u.ET_Channel_Support__c == true){
            ETSupport = true;
        } else { ETSupport = false;}
        
        if(u.id == o.CRM_User__c)
        {
            CRMUser = true;
        } else { CRMUser = false;}
        
        if(isAdmin == true || (ETSupport == true && (aName.contains('ExactTarget')|| aName.contains('No Partner'))) || u.id == o.OwnerId || u.id == o.crm_user__c || 
        u.id == o.First_Approver__c || u.id == o.Second_Approver__c || u.id == '00500000006oySV' || u.id == '00500000006p4Xs' || u.Id == '005000000075d5V' || u.Id == '005000000073TAK' || u.Id == '005000000072CF6') { 
            editAccess = true; 
        } else { editAccess = false; }
        
        // determine if the opp is in an approval process
        
        inApproval = o.awaiting_approval__c;
        }

    // runs when the user hits edit or apply (they are the same button)
    public PageReference makeChange() {

        // only proceed if the user has edit rights and the opp isn't currently awaiting approval
        if((editAccess == true && inApproval == false) || isAdmin == true || CRMUser == true) {
        
            // editMode is used as a view state variable, initialized as false. once true, the page rerenders to display inputfields rather than outputfields.
            if(editMode == false) 
            {
                editMode = true;             
               if(o.partnerlookup__c != null)
              
               getPartnerContacts();
               
               return null; // run this method so that the list of contacts at the partner (if there is one populate) is available              
                
            } 
            else 
            {
                //  this part of the code is where any values that were entered by the user are saved
                editMode = false;
                try
                {
                    update o;
                }
                catch(Exception e)
                {
                	ApexPages.addMessages(e);
					return null;
                }
                //CheckPartner();
                //WhoisInvoiced();
               
                return new PageReference('/' + ApexPages.currentPage().getParameters().get('Id'));

            }
            
        } else {
            if(editAccess == false) ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'You are not authorized to make changes to this opportunity.'));
            if(inApproval == true && isAdmin == false) ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'This opportunity is currently awaiting approval; changes cannot be made until it is either approved or rejected.'));
            return null;
        }

    }

    /*public list<list<SelectOption>> getPartnerContactsNew()
    {
        List<List<SelectOption>> allOptions = new List<List<SelectOption>>();
        List<SelectOption> listOptions = getPartnerContacts();
        List<SelectOption> temp = new List<SelectOption>();
        
        if(listOptions != NULL)
        {
        
            for(SelectOption option : listOptions)
            {
                if(temp.size() == 1000)
                {
                    allOptions.add(temp);
                    temp = new List<SelectOption>();
                }
                temp.add(option);
            }
            if(temp.size() > 0)
            {
                allOptions.add(temp);
            }
            return allOptions;
        }
        else
        {
            return null;
        }
 
    }*/
    
    public list<selectOption> getPartnerContacts() {
        partnerContacts = new List<selectOption>();
        partnerContacts.add(new SelectOption('','--None--'));
        partnerContacts2 = new List<selectOption>();

        list<contact> c = new list<contact>();
        if(o.PartnerLookup__c != null)
        {
        
            c = [select name from contact where accountid = :o.partnerlookup__c order by name asc ];
            
            if(c.size() > 1000)
            {
                for( integer i = 0; i < 999; i++ )
                {
                    partnerContacts.add(new SelectOption(c[i].id,c[i].name)); 
                }
            
                for(integer i = 999; i < c.size(); i++) 
                {
                    partnerContacts2.add(new SelectOption(c[i].id,c[i].name));   
                }
            }
            else if(c.size() <= 1000)
            {
               for( Contact con : c)
                {
                    partnerContacts.add(new SelectOption(con.id,con.name)); 
                } 
            }
    
            return partnerContacts;
        }
        else
        {
            return null;
        }
    }


    public PageReference getPartnerData() {

        if(o.partnerlookup__c != null) {
            getPartnerContacts();
            account a = [select crm__c from account where id = :o.partnerlookup__c limit 1];
            o.crm_user__c = a.crm__c;
			/*try
			{
				//update o;
			}
			catch(Exception e)
			{
				ApexPages.addMessages(e);
				return null;
			}*/
            return null;

        }
        
        if(o.partnerlookup__c == null){
        o.Deal_Closing_Through__c = null;
        o.Who_is_Invoiced__c = null;
        o.Partner_Commission__c = null;
        o.CRM_User__c = null;
        o.Type_of_Involvementtext__c = null;
		/*try
		{
			//update o;
		}
		catch(Exception e)
		{
			ApexPages.addMessages(e);
			return null;
		}*/

        }

        return null;
    }

    public PageReference newContact() {
        return new PageReference('/003/e?retURL=%2Fapex%2TabView%3Fid%3D'+o.Id+'%26sfdc.override%3D1%26core.apexpages.devmode.url%3D1&accid='+o.PartnerLookup__c);
    }
    
     /*public PageReference apply() {
		try
		{
			update o;
		}
		catch(Exception e)
		{
			ApexPages.addMessages(e);
			return null;
		}
        return null;
    }*/
    
    public PageReference WhoisInvoiced(){                
		try
    	{
	        if(o.Deal_Closing_Through__c == 'Partner'){
	        o.Who_is_Invoiced__c = 'Partner is Invoiced';
	        o.Type_of_Involvementtext__c = 'Reseller';
	        //update o;
	        //return null;
	        }
	        if(o.Deal_Closing_Through__c == 'Return Path'){
	        o.Type_of_Involvementtext__c = 'Referral';
	        //update o;
	        //return null;
	        }
	        if(o.Deal_Closing_Through__c == null){
	        o.Type_of_Involvementtext__c = null;
	       // update o;
	        //return null;
	        }
    	}
    	catch(Exception e)
    	{
    		ApexPages.addMessages(e);
    	}
        return null;
    }

    public PageReference CheckPartner(){
    	try
    	{
	        if(o.PartnerLookup__c != null){
	        o.Nature_of_Involvement__c = 'Yes';
	        //update o;
	        }
	        else{
	        o.Nature_of_Involvement__c = 'No';
	        //update o;
	        }
    	}
    	catch(Exception e)
    	{
    		ApexPages.addMessages(e);
    	}
        return null;
    }
}