public  class ApprovalProcessServices {
	
	private Opportunity Opp;
    private String ButtonName {get;set;}
	private User CurrentUser;
	//private static Final String INITIAL_PRICING = 'InitialPricing';
	private static Final String GENERAL_REFERRAL = 'General Referral';
	private static Final String FINANCE_APPROVAL = 'FinanceApproval';
	private static Final String TERMINATION_APPROVAL = 'Termination Approval';
	private static Final String CERT_APPROVAL = 'Cert Approval';
    private static Final String APPROVAL_TYPE_CERTIFICATION = 'Certification Approval';

	private static  Final String OPP_FEE = 'Fee';
	private static  Final String OPP_SUPPLEMENT = 'Supplement';
	private static Final String OPP_EXT = 'Extension';
    private static Final String OPP_RENEWAL = 'Renewal';
	private static Final String TERMINATION_RECTYPE = 'Termination';
	private static Final String FINANCE_APPROVED = 'Finance Approved';
	private static Final String LOST_STAGE = 'Lost';
	private static Final String PENDING_CERT_QUAL = 'Pending Certification Qualification';
	private static Final String APPROVAL_TYPE_FINANCE = 'Finance Approval';
	private static Final String APPROVAL_TYPE_GEN_APPROAVL = 'Gen Ref Approval';
    private static Final String SIGNATURE = 'Signature';
    private static Final String APPROVAL_REQUIRED = 'Approval Required';
    private static Final String STAGENAME_EST_VALUE = 'Establishing Value';
    //private static Final String APPROVAL_TYPE_PRICING = 'Pricing Approval';


	
	
	public ApprovalProcessServices(ApexPages.StandardController std) 
	{
		this.Opp = (Opportunity) std.getRecord();
		String currentUserId = UserInfo.getUserId();
		CurrentUser = [Select Id,Name,First_Approver__c,Second_Approver__c, ManagerId from User where Id = :currentUserId];
	 	
	}


    public void SubmitForApproval(String ProcessName)
    {
    	System.debug('Opp Is is: ' + this.Opp.Id);
    	System.debug('ProcessName is: ' + ProcessName);
    	Approval.ProcessSubmitRequest req = new Approval.ProcessSubmitRequest();
    	req.setObjectId(this.Opp.Id);
    	req.setSubmitterId(CurrentUser.Id);
    	req.setProcessDefinitionNameOrId(ProcessName);
    	req.setComments('Submitted for Approval');
        if(ProcessName.contains('General_Referral_Process'))
        {
            List<Id> approverIds = new List<Id>{Opp.RD_for_Gen_Ref_Approval__c};
            req.setNextApproverIds(approverIds);
        }
    	
        //submit the Approval request for the opp
    	Approval.ProcessResult result = Approval.Process(req);
    	//return Result;

    }

    public PageReference OppPage()
    {
    	Id OppId =  ApexPages.currentPage().getParameters().get('Id');
    	 PageReference PageRef = new PageReference('/' +OppId);
    	 return PageRef;
    }

	public  PageReference ProcessApprovalButtons()
	{
        
		ButtonName = ApexPages.currentPage().getParameters().get('buttoname');
		String currentUserId = UserInfo.getUserId();
		Id OppId =  ApexPages.currentPage().getParameters().get('Id');
        PageReference PageRef = new PageReference('/' +OppId);
        PageReference ErrorPage = new PageReference('/apex/InitialPricingApproval');
        PageReference OppPage = new ApexPages.StandardController(Opp).view();
        String OppLink = 'https://' + System.URL.getSalesforceBaseUrl().getHost();

		
		CurrentUser = [Select Id,Name,First_Approver__c,Second_Approver__c, ManagerId from User where Id = :currentUserId];
		this.Opp = [Select Id,Tools_Child_ONLY__c,Cert_Ref_Child_ONLY__c,OwnerId,StageName,Approval_Type__c,RecordType.Name,Account.Cert_Active_Subscriptions__c,RD_for_Gen_Ref_Approval__c,Reasons_for_Lost_Opportunities__c,
                    RSE_for_Gen_Ref_Approval__c,Auto_Submit_WFR_Fired__c,Cert_Only_Opportunity__c,Submit_From_Trigger__c,Application_Result__c,Type,Campaign.Name,
                    Gen_Referral_Opp__c,Gen_Ref_CRM_Approved_Time_Stamp__c, Gen_Ref_CRM_Rejected_Time_Stamp__c,Gen_Ref_Originated_By__c from Opportunity where Id =:OppId ];

		User RecordOwnerInfo = [Select Id,Name,First_Approver__c,Second_Approver__c, ManagerId from User where Id = :Opp.OwnerId];
		
        
		if(Opp!=null && CurrentUser!= null )
		{
		   /*if(ButtonName.equalsIgnoreCase(INITIAL_PRICING) && RecordOwnerInfo!= null)
			{
                if(RecordOwnerInfo.First_Approver__c == null ) /*|| RecordOwnerInfo.Second_Approver__c == null || RecordOwnerInfo.ManagerId == null)*/
				/*{
                    System.debug('In the null block');
                     Apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,''+'First Approver is missing for current the opportunity owner"'));
					 Opp.addError('First Approver is missing for the current opportunity owner');
					return null;
                }
                if(Opp.Type == null)
                {
                	Apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'' + 'Please Fill in the Opportunity Type'));
                	Opp.addError('Please Fill in the Opportunity Type');
                	return null;
                }
                else if( (Opp.StageName.equalsIgnoreCase(STAGENAME_EST_VALUE) && Opp.Approval_Type__c.equalsIgnoreCase(APPROVAL_TYPE_PRICING) && 
                			Opp.Type.equalsIgnoreCase(OPP_FEE)) || (Opp.StageName.equalsIgnoreCase(STAGENAME_EST_VALUE) && Opp.Approval_Type__c.equalsIgnoreCase(APPROVAL_TYPE_PRICING)
                			&& Opp.Tools_Child_ONLY__c == false && Opp.Cert_Ref_Child_ONLY__c == false))
                {
                    //call the Approval Process for Initial Pricing.
                    SubmitForApproval('Initial_Pricing_Appr_06_2014_Release');
                }
                else
                {

                	//Apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,''+ 'This record does not meet the entry criteria or initial submitters of any active approval processes. Please contact your administrator for assistance.'));
                	Opp.addError('This record does not meet the entry criteria or initial submitters of any active approval processes. Please contact your administrator for assistance.');
                	
                	return null;
                }
			}*/
			if(ButtonName.equalsIgnoreCase(GENERAL_REFERRAL))
			{
                if(Opp.RD_for_Gen_Ref_Approval__c == null)
                {
                    Opp.addError('Please fill in the \'RD For Gen Ref Approval\' field on the Opportunity before Submitting for Gen Ref Approval');
                    return null;
                }
                else if(Opp.Gen_Referral_Opp__c == true && Opp.Gen_Ref_CRM_Approved_Time_Stamp__c != null && Opp.Gen_Ref_Originated_By__c!= null && Opp.Gen_Ref_CRM_Rejected_Time_Stamp__c == null
                	    && Opp.StageName!= FINANCE_APPROVED && Opp.StageName!= LOST_STAGE && Opp.Approval_Type__c == APPROVAL_TYPE_GEN_APPROAVL)
                {
                	SubmitForApproval('General_Referral_Process');
                }
                else
                {
                	Opp.addError('This record does not meet the entry criteria or initial submitters of any active approval processes. Please contact your administrator for assistance.');
                	return null;
                }
			}
			else if(ButtonName.equalsIgnoreCase(FINANCE_APPROVAL) && RecordOwnerInfo!= null)
			{
                System.debug('In the Finance Approval and the Opp is ' +Opp);
                if(RecordOwnerInfo.First_Approver__c == null ) /*|| RecordOwnerInfo.Second_Approver__c == null || RecordOwnerInfo.ManagerId == null)*/
				{
					Opp.addError('First Approver is missing for current the opportunity owner');
					return null;
                }
                if(Opp.Type == null)
                {
                	Opp.addError('Please Fill in the Opportunity Type');
                	return null;
                }
                
				if(((this.Opp.RecordType.Name == TERMINATION_RECTYPE)&&(this.Opp.StageName == APPROVAL_REQUIRED && this.Opp.Reasons_for_Lost_Opportunities__c!= null)) || (this.Opp.Submit_From_Trigger__c == 'Yes'))
				{				
					SubmitForApproval('Termination_Approval_06_2014_Release');
				}
                else if((Opp.StageName!= FINANCE_APPROVED && Opp.StageName!= PENDING_CERT_QUAL
                        && Opp.Approval_Type__c == APPROVAL_TYPE_FINANCE && Opp.Account.Cert_Active_Subscriptions__c == 0 
                        && (Opp.Application_Result__c == null || Opp.Application_Result__c == '') && Opp.Auto_Submit_WFR_Fired__c == false
                        && Opp.Type!= OPP_FEE && Opp.Type!= OPP_EXT && Opp.Type!= OPP_SUPPLEMENT && Opp.Type!= OPP_RENEWAL ) && (Opp.Campaign.Name == 'WEB: SSC Application Form'|| Opp.Cert_Only_Opportunity__c == 'Yes'))
                {
                         System.debug('In the new Cert Call call');
					    SubmitForApproval('Cert_Only_New_Client_06_2014');                
                }
				else if(((Opp.StageName == SIGNATURE) && (Opp.Approval_Type__c == APPROVAL_TYPE_FINANCE || Opp.Approval_Type__c == APPROVAL_TYPE_CERTIFICATION)
                         && (Opp.RecordType.Name != TERMINATION_RECTYPE )) || Opp.Submit_From_Trigger__c == 'Yes')
                {
                    System.debug('In the last call');
                    SubmitForApproval('Finance_Approval_10_2014_Release_v5');
                }
                else
                {
                	Opp.addError('This record does not meet the entry criteria or initial submitters of any active approval processes. Please contact your administrator for assistance.');
                	return null;
                }

			}
            
			

		}
		
        return PageRef;
	}

}