public class NuggetBacktoRD_Controller {

Public Nugget__c n;


  public NuggetBacktoRD_Controller(ApexPages.StandardController controller) {  
 
 n = [select Id, OwnerId, First_Opened_by_Sales_Rep__c, Pod_Calc__c, Returned_to_RD__c from Nugget__c where Id = :ApexPages.currentPage().getParameters().get('Id')];
 
 
 }
 
 
 
 public pageReference backtord(){
 
 Group g = [select Id from Group where Name = :n.Pod_Calc__c and Type = 'Queue'];
 n.OwnerId = g.Id;
 n.Returned_to_RD__c = true;
 n.Status__c = 'Unassigned';
 update n;
 
 
 PageReference ref = new PageReference('/apex/NuggetTabView?id='+n.Id);
    ref.setRedirect(true);
    return ref;
 }


}