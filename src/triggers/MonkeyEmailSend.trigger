trigger MonkeyEmailSend on Opportunity (before update) {
    
 //Organization orgInfo = [Select Id,IsSandbox from Organization limit 1];
    
    System.debug('executing Monkey Trigger');
    List<String> args = new String[]{'0','number','###,###,##0.00'};
    String nameAndRole = ' ';
    
    Opportunity o = Trigger.New[0];
    Opportunity oldOpp;
    if(trigger.oldMap != NULL && o.Id != NULL)
    {
        oldOpp = trigger.oldMap.get(o.Id);
    }
    if((o.Send_Monkey_Email__c == 'Send' && 
        oldOpp.Send_Monkey_Email__c != 'Send') &&
        o.Administrative__c == false && 
        o.Inc_ACV_Before_Partner_Comm__c >= 0 
        && o.Type != 'Fee') 
    {
        Organization orgInfo = [Select Id,IsSandbox from Organization limit 1];
        if(!orgInfo.IsSandbox || test.isRunningTest())
        {
            Opportunity op = [select name, OwnerId, owner.name,account.name,amount,CurrencyISOCode, AccountId,
                              Administrative__c,Type , Id ,Send_Monkey_Email__c, Inside_Sales_Rep__c, Buddy_Rep__r.Name,
                              Inc_ACV_Before_Partner_Comm__c, ACV_Before_Partner_Comm__c, PartnerLookup__r.Name,CRM_User__r.Name,
                              Prev_Recurring_ACV_Before_Partner_Comm__c, Recurring_ACV_Before_Partner_Comm__c, 
                              Type_of_InvolvementText__c, PartnerLookup__c, Type_of_Involvement_Formula__c, Original_SDR__c,
                              (Select UnitPrice, Previous_Value__c, Net_Incremental_ACV__c, Product_Family__c, Product2.name from OpportunityLineItems) 
                              FROM opportunity WHERE id in :trigger.newMap.keySet()];
            //checks if the values are null
            String involvementFormula = op.Type_of_Involvement_Formula__c == null ? 'N/A' :op.Type_of_Involvement_Formula__c;
            String originalSDR        = op.Original_SDR__c == null ? 'N/A' : op.Original_SDR__c;
            
            List<OpportunityTeamMember> teamMembers = [SELECT Id, Name, TeamMemberRole FROM OpportunityTeamMember WHERE OpportunityId = :op.Id AND 
                                                       TeamMemberRole != 'SVP - Global Sales/Service' AND UserId != :op.OwnerId ];
            for(OpportunityTeamMember member : teamMembers)
            {
                nameAndRole += member.Name + ' - ' + member.TeamMemberRole + '<br/>';
            }
            
            if(nameAndRole.length() > 2)
            {
                nameAndRole = nameAndRole.substring(0, nameAndRole.length() -2);
            }
            Account account = [SELECT Id, Technical_Account_Manager__c, Technical_Account_Manager__r.Name FROM Account WHERE Id = :op.AccountId];
            
            String withTAM = '';
            if(account.Technical_Account_Manager__c != NULL) withTAM = ' & '+account.Technical_Account_Manager__r.Name;
            
            String withMembers = '';
            if( !teamMembers.isEmpty())
            {
                withMembers = '<br/> Opportunity Team:' + nameAndRole + ' <br/>';
            }
            Messaging.reserveSingleEmailCapacity(2);                 
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            
            mail.setSenderDisplayName('Monkey Emails');
            mail.setToAddresses(new String[]{'newbiz@returnpath.com', 'newcontracts@returnpath.com'});
            //mail.setToAddresses(new String[]{'theo.tenia@bluewolfgroup.com'}); 
            mail.setSubject('Another Client Win by ' + op.Owner.Name + withTAM +' for ' + op.Account.Name + ' - ' + op.Type);
            
            String LGRClause = '';
            if(o.Inside_Sales_Rep__c <> null) LGRClause = ' This account was qualified by ' + op.Inside_Sales_Rep__c + ' on the Lead Gen Team.';
            
            String CommissionShareClause = '';
            if(o.Buddy_Rep__c <> null) CommissionShareClause = '<br/>Commission Share Owner: ' + op.Buddy_Rep__r.Name;
            
            String CRMClause = '';
            //if(o.CRM_User__c <> null) CRMClause = '<br/>Channel Relationship Manager: ' + op.CRM_User__r.Name;
            
            String PartnerClause = '';
            if(o.PartnerLookup__c <> null) PartnerClause = '<br/>Partner Name: ' + op.PartnerLookup__r.Name;
            
            String TAMClause = '';
            if(account.Technical_Account_Manager__c != null) TAMClause = '</br> Technical Account Manager: '+ account.Technical_Account_Manager__r.Name;
            system.debug('Is there a TAM?'+account.Technical_Account_Manager__c);
            
            String olis = '</br> <table border="1" style="width:100%">'
                +'<tr>'
                +'<th>Product Name</th><th>Product Family/LOB</th><th>Unit Price</th>'
                +'<th>Previous Value</th><th>Net Incremental ACV</th>'
                +'</tr>';
            
            for(OpportunityLineItem anOli: op.OpportunityLineItems)
            {
                olis +=
                    '<tr>'
                    +'<td>'+anOli.Product2.name+'</td><td>'+anOli.Product_Family__c
                    +'</td><td>'+anOli.UnitPrice+'</td><td>'+ anOli.Previous_Value__c +
                    +'</td><td>'+anOli.Net_Incremental_ACV__c+'</td>'
                    +'</tr>';
            }
            olis+='</table>';
            String sfdcURL = '<a href='+URL.getSalesforceBaseUrl().toExternalForm()+'/'+op.id+'>'; 
            system.debug('olis table '+olis);
            system.debug('=1'+withMembers
                         + '=1'+ TAMClause
                         + '=1'+ CRMClause
                         + '=1'+ PartnerClause
                         + '=1'+ CommissionShareClause);
            mail.setHTMLBody(
                '<html><body><img src="https://ssl.salesforce.com/servlet/servlet.ImageServer?id=01500000000ZrTv&oid=00D00000000hcub" height="275" width="275"/>' +
                + '<p>' + op.Owner.Name + ' closed a deal for ' + op.Account.Name + '.' + LGRClause + '<p/>'
                + '<b><u>Opportunity Details</b></u>'
                + '<br/>Name: ' + sfdcURL+op.Name+'</a>'+
                //+ '<p>ACV Before Partner Commissions: ' + op.currencyisocode + ' ' + String.format(op.ACV_Before_Partner_Comm__c.format(), args)
                //+ '<br/>Incremental ACV Before Partner Commissions: ' + op.currencyisocode + ' ' + String.format(op.Inc_ACV_Before_Partner_Comm__c.format(), args)
                + '<br/>Prev -Recurring ACV: ' + op.currencyisocode + ' ' + String.format(op.Prev_Recurring_ACV_Before_Partner_Comm__c.format(), args)
                + '<br/>Total ACV: ' + op.currencyisocode + ' ' + String.format(op.ACV_Before_Partner_Comm__c.format(), args)
                + '<br/>Recurring ACV: ' + op.currencyisocode + ' ' + String.format(op.Recurring_ACV_Before_Partner_Comm__c.format(), args)
                + '<br/>Incremental ACV: ' + op.currencyisocode + ' ' + String.format(op.Inc_ACV_Before_Partner_Comm__c.format(), args)
                + '<br/>Opportunity Type: ' + op.type+
                + '<br/>Type of Involvement: ' + op.Type_of_Involvement_Formula__c+'<br/>'
                //+ '<br/>Channel Partner: ' + op.PartnerLookup__c+'<br/>'
                //+ '<br/>Original SDR: ' + op.Original_SDR__c+'<br/>'
                + '<br/><b><u>Product Details</b></u>'
                + olis
                + '<br/><b><u>People Details</b></u>'
                + withMembers
                + TAMClause
                //+ CRMClause
                + '<br/>Original SDR: ' + originalSDR+
                + PartnerClause
                + CommissionShareClause+
                //+ '<p>To view the opportunity, please use the link below:'
                //+ '<br/>https://ssl.salesforce.com/' + op.id+
                '</html></body>'
            );
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
            o.Send_Monkey_Email__c = 'Monkey Sent';
        }    
    }
    else
    {
        if((o.Type == 'Fee' ||  o.Inc_ACV_Before_Partner_Comm__c < 0) && o.Send_Monkey_Email__c == 'Send')
        {               
            o.Send_Monkey_Email__c = 'No Monkey Needed';
            
        }
    }
}