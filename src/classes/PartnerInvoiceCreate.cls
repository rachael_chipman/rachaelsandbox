global class PartnerInvoiceCreate implements Schedulable {
global void execute(SchedulableContext ctx) {
    
    CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, NextFireTime
                FROM CronTrigger WHERE Id = :ctx.getTriggerId()];

      System.debug(ct.CronExpression);
      System.debug(ct.TimesTriggered);
      
  List<c2g__codaInvoice__c> scope = [select Id, c2g__Account__c from c2g__codaInvoice__c where c2g__Account__c != null and Partner_is_Billing_Agent__c = true and c2g__InvoiceDate__c = LAST_MONTH];

Set<Id> uniqueCustomObjectSet = new Set<Id>();
for(c2g__codaInvoice__c si: scope){
uniqueCustomObjectSet.add(si.c2g__Account__c);
}

List<Account> acct = [select Id, Name, Invoicing_Contact__c, CurrencyISOcode from Account where Id in :uniqueCustomObjectSet];
 

List<Partner_Invoice__c> inpi = new List<Partner_Invoice__c>();

for(Account partacct : acct ){
    Partner_Invoice__c pi = new Partner_Invoice__c (
    Partner_Account__c = partacct.Id,
    Invoice_Date__c = system.Today().addDays(-3),
    Invoice_Due_Date__c = system.Today().addDays(27),
    CurrencyISOcode = partacct.CurrencyISOcode,
    Invoicing_Contact__c = partacct.Invoicing_Contact__c); 
    
    c2g__codaAccountingCurrency__c cur = [select Id, Name from c2g__codaAccountingCurrency__c where Name = :partacct.CurrencyIsoCode and c2g__OwnerCompany__c = 'a2I00000000CaqE'];
    pi.Invoice_Currency__c = cur.Id; 
    inpi.add(pi);    
    }
    insert inpi;


id batchinstanceid = database.executeBatch(new PartnerInvoiceCreateBatch(inpi, 'select Id, c2g__Account__c from c2g__codaInvoice__c where c2g__Account__c != null and Partner_is_Billing_Agent__c = true and c2g__InvoiceStatus__c = \''+'Complete'+'\' and c2g__InvoiceDate__c = Last_Month'),1);  



}}