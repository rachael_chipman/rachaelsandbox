@ isTest (SeeAllData = true)
public class OppPostPartnerInvoice_Test {
    /*static testMethod void TestOppPostAnnual1() {
        
        Account acct = new Account();
        acct.name = 'RPTestacct1';
        acct.BillingCountry = 'USA';
        acct.c2g__CODAAccountsReceivableControl__c = 'a2Z00000000H8ou';
        Insert acct;
        
         Account pacct = new Account();
        pacct.name = 'RPPartner';
        pacct.CurrencyISOcode = 'USD';
        pacct.c2g__CODAAccountsReceivableControl__c = 'a2Z00000000H8ou';        
        Insert pacct;
        
        Promo_Code__c pc = new Promo_Code__c();
        pc.Name = 'TESTPC';
        pc.Account__c = pacct.Id;
        pc.Promo_Type__c = 'Referral';
        insert pc;
        
        pacct.Promo_Code__c = pc.Id;
        update pacct;
              
        
        Opportunity opp1 = new Opportunity(
        AccountId = acct.id, 
        Name = 'RPTestOpp1', 
        Pricebook2Id = '01s000000004NS6AAM',
        StageName = 'Signature', 
         CloseDate = Date.Today(), 
        Type = 'New Business',
        Billing_Terms__c = 'Net 30',
        Invoice_Created__c = false,
        Billing_Type__c = 'Annual',
        currencyISOcode = 'USD',
        PartnerLookup__c = pacct.Id,
        Who_is_Invoiced__c = 'Partner is Invoiced', 
        Partner_Commission__c = 10        
        );
        
        Insert opp1;
        
        OpportunityLineItem prod1 = new OpportunityLineItem();
        prod1.OpportunityId = opp1.id;
        prod1.PricebookEntryId = '01u00000000FGwcAAG';
        prod1.Quantity = 12;
        prod1.UnitPrice = 1200;
        prod1.of_MM_Events_Allowed__c = 20;
        prod1.of_CP_Events_Allowed__c = 20;
        prod1.MM_Overage_Fee__c = 25;
        prod1.CP_Overage_Fee__c = 25;
        prod1.of_BLA_IPs__c = 10;
        prod1.of_Rep_Mon_IPs__c = 10;
        prod1.Product_Status__c = 'Current';
        prod1.Description__c = 'Deliverability Tools';
        prod1.Indirect__c = false;
        prod1.Active__c = true;
        prod1.Previous_Value__c = 0;
        prod1.Bundle__c = false;
        prod1.Recurring__c = true;
        prod1.Start_Date__c = Date.valueOf('2013-01-01');
        prod1.End_Date__c = Date.valueOf('2013-12-31');
        Insert prod1;
        
          OpportunityLineItem prod2 = new OpportunityLineItem();
        prod2.OpportunityId = opp1.id;
        prod2.PricebookEntryId = '01u00000000FGwcAAG';
        prod2.Quantity = 12;
        prod2.UnitPrice = 1200;
        prod2.of_MM_Events_Allowed__c = 20;
        prod2.of_CP_Events_Allowed__c = 20;
        prod2.MM_Overage_Fee__c = 25;
        prod2.CP_Overage_Fee__c = 25;
        prod2.of_BLA_IPs__c = 10;
        prod2.of_Rep_Mon_IPs__c = 10;
        prod2.Product_Status__c = 'Current';
        prod2.Description__c = 'Deliverability Tools';
        prod2.Indirect__c = false;
        prod2.Active__c = true;
        prod2.Previous_Value__c = 0;
        prod2.Bundle__c = false;
        prod2.Recurring__c = true;
        prod2.Start_Date__c = Date.valueOf('2013-01-01');
        prod2.End_Date__c = Date.valueOf('2013-6-15');
        Insert prod2;
        
        
       opp1.StageName = 'Finance Approved';
       opp1.Bypass_Product_Portfolio__c = true;
       opp1.Begin_Automation__c = true;
       update opp1; 

}


    static testMethod void TestOppPostAnnual2() {

       Account acct = new Account();
        acct.name = 'RPTestacct1';
        acct.BillingCountry = 'USA';
        acct.c2g__CODAAccountsReceivableControl__c = 'a2Z00000000H8ou';
        Insert acct;
        
        Account pacct = new Account();
        pacct.name = 'RPPartner';
        pacct.CurrencyISOcode = 'USD';
        pacct.c2g__CODAAccountsReceivableControl__c = 'a2Z00000000H8ou';        
        Insert pacct;
        
        Promo_Code__c pc = new Promo_Code__c();
        pc.Name = 'TESTPC';
        pc.Account__c = pacct.Id;
        pc.Promo_Type__c = 'Referral';
        insert pc;
        
        pacct.Promo_Code__c = pc.Id;
        update pacct;
        
        Opportunity opp1 = new Opportunity(
        AccountId = acct.id, 
        Name = 'RPTestOpp1', 
        Pricebook2Id = '01s000000004NS6AAM',
        StageName = 'Signature', 
        CloseDate = Date.Today() ,
        Type = 'New Business',
        Billing_Terms__c = 'Net 30',
        Invoice_Created__c = false,
        Billing_Type__c = 'Annual',
        currencyISOcode = 'GBP',
        PartnerLookup__c = pacct.Id,
        Deal_Closing_Through__c = 'Return Path',
        Who_is_Invoiced__c = 'Partner is Invoiced', 
        Partner_Commission__c = 10        
        );
        
        Insert opp1;
        
        OpportunityLineItem prod1 = new OpportunityLineItem();
        prod1.OpportunityId = opp1.id;
        prod1.PricebookEntryId = '01u00000000FGwdAAG';
        prod1.Quantity = 12;
        prod1.UnitPrice = 1200;
        prod1.of_MM_Events_Allowed__c = 20;
        prod1.of_CP_Events_Allowed__c = 20;
        prod1.MM_Overage_Fee__c = 25;
        prod1.CP_Overage_Fee__c = 25;
        prod1.of_BLA_IPs__c = 10;
        prod1.of_Rep_Mon_IPs__c = 10;
        prod1.Product_Status__c = 'Current';
        prod1.Description__c = 'Deliverability Tools';
        prod1.Indirect__c = false;
        prod1.Active__c = true;
        prod1.Previous_Value__c = 0;
        prod1.Bundle__c = false;
        prod1.Recurring__c = true;
        prod1.Start_Date__c = Date.valueOf('2013-04-01');
        prod1.End_Date__c = Date.valueOf('2014-03-31');
        Insert prod1;
        
        
       opp1.StageName = 'Finance Approved';
       opp1.Bypass_Product_Portfolio__c = true;
       opp1.Begin_Automation__c = true;
       update opp1; 
}

      static testMethod void TestOppPostSemiAnnual1() {
        
        Account acct = new Account();
        acct.name = 'RPTestacct1';
        acct.BillingCountry = 'USA';
        acct.c2g__CODAAccountsReceivableControl__c = 'a2Z00000000H8ou';
        Insert acct;
        
        Account pacct = new Account();
        pacct.name = 'RPPartner';
        pacct.CurrencyISOcode = 'USD';
        pacct.c2g__CODAAccountsReceivableControl__c = 'a2Z00000000H8ou';        
        Insert pacct;
        
        Promo_Code__c pc = new Promo_Code__c();
        pc.Name = 'TESTPC';
        pc.Account__c = pacct.Id;
        pc.Promo_Type__c = 'Referral';
        insert pc;
        
        pacct.Promo_Code__c = pc.Id;
        update pacct;
        
        Opportunity opp1 = new Opportunity(
        AccountId = acct.id, 
        Name = 'RPTestOpp1', 
        Pricebook2Id = '01s000000004NS6AAM',
        StageName = 'Signature', 
        CloseDate = Date.Today(), 
        Type = 'New Business',
        Billing_Terms__c = 'Net 30',
        Invoice_Created__c = false,
        Billing_Type__c = 'Semi-Annual',
        currencyISOcode = 'USD',
        PartnerLookup__c = pacct.Id,
        Who_is_Invoiced__c = 'Partner is Invoiced', 
        Partner_Commission__c = 10        
        );
        
        Insert opp1;
        
        OpportunityLineItem prod1 = new OpportunityLineItem();
        prod1.OpportunityId = opp1.id;
        prod1.PricebookEntryId = '01u00000000FGwcAAG';
        prod1.Quantity = 12;
        prod1.UnitPrice = 1200;
        prod1.of_MM_Events_Allowed__c = 20;
        prod1.of_CP_Events_Allowed__c = 20;
        prod1.MM_Overage_Fee__c = 25;
        prod1.CP_Overage_Fee__c = 25;
        prod1.of_BLA_IPs__c = 10;
        prod1.of_Rep_Mon_IPs__c = 10;
        prod1.Product_Status__c = 'Current';
        prod1.Description__c = 'Deliverability Tools';
        prod1.Indirect__c = false;
        prod1.Active__c = true;
        prod1.Previous_Value__c = 0;
        prod1.Bundle__c = false;
        prod1.Recurring__c = true;
        prod1.Start_Date__c = Date.valueOf('2013-04-01');
        prod1.End_Date__c = Date.valueOf('2014-03-31');
        Insert prod1;
        
        
       opp1.StageName = 'Finance Approved';
       opp1.Bypass_Product_Portfolio__c = true;
       opp1.Begin_Automation__c = true;
       update opp1; 

}


   static testMethod void TestOppPostSemiAnnual2() {
        
       Account acct = new Account();
        acct.name = 'RPTestacct1';
        acct.BillingCountry = 'USA';
        acct.c2g__CODAAccountsReceivableControl__c = 'a2Z00000000H8ou';
        Insert acct;
        
        Account pacct = new Account();
        pacct.name = 'RPPartner';
        pacct.CurrencyISOcode = 'USD';
        pacct.c2g__CODAAccountsReceivableControl__c = 'a2Z00000000H8ou';        
        Insert pacct;
        
        Promo_Code__c pc = new Promo_Code__c();
        pc.Name = 'TESTPC';
        pc.Account__c = pacct.Id;
        pc.Promo_Type__c = 'Referral';
        insert pc;
        
        pacct.Promo_Code__c = pc.Id;
        update pacct;
        
        Opportunity opp1 = new Opportunity(
        AccountId = acct.id, 
        Name = 'RPTestOpp1', 
        Pricebook2Id = '01s000000004NS6AAM',
        StageName = 'Signature', 
        CloseDate = Date.Today(), 
         Type = 'New Business',
        Billing_Terms__c = 'Net 30',
        Invoice_Created__c = false,
        Billing_Type__c = 'Semi-Annual',
        currencyISOcode = 'GBP',
        PartnerLookup__c = pacct.Id,
        Deal_Closing_Through__c = 'Return Path',
        Who_is_Invoiced__c = 'Partner is Invoiced', 
        Partner_Commission__c = 10        
        );
        
        Insert opp1;
        
       OpportunityLineItem prod1 = new OpportunityLineItem();
        prod1.OpportunityId = opp1.id;
        prod1.PricebookEntryId = '01u00000000FGwdAAG';
        prod1.Quantity = 12;
        prod1.UnitPrice = 1200;
        prod1.of_MM_Events_Allowed__c = 20;
        prod1.of_CP_Events_Allowed__c = 20;
        prod1.MM_Overage_Fee__c = 25;
        prod1.CP_Overage_Fee__c = 25;
        prod1.of_BLA_IPs__c = 10;
        prod1.of_Rep_Mon_IPs__c = 10;
        prod1.Product_Status__c = 'Current';
        prod1.Description__c = 'Deliverability Tools';
        prod1.Indirect__c = false;
        prod1.Active__c = true;
        prod1.Previous_Value__c = 0;
        prod1.Bundle__c = false;
        prod1.Recurring__c = true;
        prod1.Start_Date__c = Date.valueOf('2013-04-01');
        prod1.End_Date__c = Date.valueOf('2014-03-31');
        Insert prod1;
        
       OpportunityLineItem prod2 = new OpportunityLineItem();
        prod2.OpportunityId = opp1.id;
        prod2.PricebookEntryId = '01u00000000FGwdAAG';
        prod2.Quantity = 12;
        prod2.UnitPrice = 1200;
        prod2.of_MM_Events_Allowed__c = 20;
        prod2.of_CP_Events_Allowed__c = 20;
        prod2.MM_Overage_Fee__c = 25;
        prod2.CP_Overage_Fee__c = 25;
        prod2.of_BLA_IPs__c = 10;
        prod2.of_Rep_Mon_IPs__c = 10;
        prod2.Product_Status__c = 'Current';
        prod2.Description__c = 'Deliverability Tools';
        prod2.Indirect__c = false;
        prod2.Active__c = true;
        prod2.Previous_Value__c = 0;
        prod2.Bundle__c = false;
        prod2.Recurring__c = true;
        prod2.Start_Date__c = Date.valueOf('2013-04-01');
        prod2.End_Date__c = Date.valueOf('2013-06-15');
        Insert prod2;
        
        
       opp1.StageName = 'Finance Approved';
       opp1.Bypass_Product_Portfolio__c = true;
       opp1.Begin_Automation__c = true;
       update opp1;       
       
}

    static testMethod void TestOppPostQuarterly1() {
        
        Account acct = new Account();
        acct.name = 'RPTestacct1';
        acct.BillingCountry = 'USA';
        acct.c2g__CODAAccountsReceivableControl__c = 'a2Z00000000H8ou';
        Insert acct;
        
        Account pacct = new Account();
        pacct.name = 'RPPartner';
        pacct.CurrencyISOcode = 'USD';
        pacct.c2g__CODAAccountsReceivableControl__c = 'a2Z00000000H8ou';        
        Insert pacct;
        
        Promo_Code__c pc = new Promo_Code__c();
        pc.Name = 'TESTPC';
        pc.Account__c = pacct.Id;
        pc.Promo_Type__c = 'Referral';
        insert pc;
        
        pacct.Promo_Code__c = pc.Id;
        update pacct;
        
        Opportunity opp1 = new Opportunity(
        AccountId = acct.id, 
        Name = 'RPTestOpp1', 
        Pricebook2Id = '01s000000004NS6AAM',
        StageName = 'Signature', 
        CloseDate = Date.Today(), 
        Type = 'New Business',
        Billing_Terms__c = 'Net 30',
        Invoice_Created__c = false,
        Billing_Type__c = 'Quarterly',
        currencyISOcode = 'USD',
        PartnerLookup__c = pacct.Id,
        Who_is_Invoiced__c = 'Partner is Invoiced', 
        Partner_Commission__c = 10        
        );
        
        Insert opp1;
        
        OpportunityLineItem prod1 = new OpportunityLineItem();
        prod1.OpportunityId = opp1.id;
        prod1.PricebookEntryId = '01u00000000FGwcAAG';
        prod1.Quantity = 12;
        prod1.UnitPrice = 1200;
        prod1.of_MM_Events_Allowed__c = 20;
        prod1.of_CP_Events_Allowed__c = 20;
        prod1.MM_Overage_Fee__c = 25;
        prod1.CP_Overage_Fee__c = 25;
        prod1.of_BLA_IPs__c = 10;
        prod1.of_Rep_Mon_IPs__c = 10;
        prod1.Product_Status__c = 'Current';
        prod1.Description__c = 'Deliverability Tools';
        prod1.Indirect__c = false;
        prod1.Active__c = true;
        prod1.Previous_Value__c = 0;
        prod1.Bundle__c = false;
        prod1.Recurring__c = true;
        prod1.Start_Date__c = Date.valueOf('2013-04-01');
        prod1.End_Date__c = Date.valueOf('2014-03-31');
        Insert prod1;
        
        
       opp1.StageName = 'Finance Approved';
       opp1.Bypass_Product_Portfolio__c = true;
       opp1.Begin_Automation__c = true;
       update opp1; 

}


    static testMethod void TestOppPostQuarterly2() {
        
       Account acct = new Account();
        acct.name = 'RPTestacct1';
        acct.BillingCountry = 'USA';
        acct.c2g__CODAAccountsReceivableControl__c = 'a2Z00000000H8ou';
        Insert acct;
        
        Account pacct = new Account();
        pacct.name = 'RPPartner';
        pacct.CurrencyISOcode = 'USD';
        pacct.c2g__CODAAccountsReceivableControl__c = 'a2Z00000000H8ou';        
        Insert pacct;
        
        Promo_Code__c pc = new Promo_Code__c();
        pc.Name = 'TESTPC';
        pc.Account__c = pacct.Id;
        pc.Promo_Type__c = 'Referral';
        insert pc;
        
        pacct.Promo_Code__c = pc.Id;
        update pacct;
        
        Opportunity opp1 = new Opportunity(
        AccountId = acct.id, 
        Name = 'RPTestOpp1', 
        Pricebook2Id = '01s000000004NS6AAM',
        StageName = 'Signature', 
        CloseDate = Date.Today(),
        Type = 'New Business',
        Billing_Terms__c = 'Net 30',
        Invoice_Created__c = false,
        Billing_Type__c = 'Quarterly',
        currencyISOcode = 'GBP',
        PartnerLookup__c = pacct.Id,
        Deal_Closing_Through__c = 'Return Path',
        Who_is_Invoiced__c = 'Partner is Invoiced', 
        Partner_Commission__c = 10        
        );
        
        Insert opp1;
        
         OpportunityLineItem prod1 = new OpportunityLineItem();
        prod1.OpportunityId = opp1.id;
        prod1.PricebookEntryId = '01u00000000FGwdAAG';
        prod1.Quantity = 12;
        prod1.UnitPrice = 1200;
        prod1.of_MM_Events_Allowed__c = 20;
        prod1.of_CP_Events_Allowed__c = 20;
        prod1.MM_Overage_Fee__c = 25;
        prod1.CP_Overage_Fee__c = 25;
        prod1.of_BLA_IPs__c = 10;
        prod1.of_Rep_Mon_IPs__c = 10;
        prod1.Product_Status__c = 'Current';
        prod1.Description__c = 'Deliverability Tools';
        prod1.Indirect__c = false;
        prod1.Active__c = true;
        prod1.Previous_Value__c = 0;
        prod1.Bundle__c = false;
        prod1.Recurring__c = true;
        prod1.Start_Date__c = Date.valueOf('2013-04-01');
        prod1.End_Date__c = Date.valueOf('2014-03-31');
        Insert prod1;
        
       OpportunityLineItem prod2 = new OpportunityLineItem();
        prod2.OpportunityId = opp1.id;
        prod2.PricebookEntryId = '01u00000000FGwdAAG';
        prod2.Quantity = 12;
        prod2.UnitPrice = 1200;
        prod2.of_MM_Events_Allowed__c = 20;
        prod2.of_CP_Events_Allowed__c = 20;
        prod2.MM_Overage_Fee__c = 25;
        prod2.CP_Overage_Fee__c = 25;
        prod2.of_BLA_IPs__c = 10;
        prod2.of_Rep_Mon_IPs__c = 10;
        prod2.Product_Status__c = 'Current';
        prod2.Description__c = 'Deliverability Tools';
        prod2.Indirect__c = false;
        prod2.Active__c = true;
        prod2.Previous_Value__c = 0;
        prod2.Bundle__c = false;
        prod2.Recurring__c = true;
        prod2.Start_Date__c = Date.valueOf('2013-04-01');
        prod2.End_Date__c = Date.valueOf('2013-06-15');
        Insert prod2;
        
        
       opp1.StageName = 'Finance Approved';
       opp1.Bypass_Product_Portfolio__c = true;
       opp1.Begin_Automation__c = true;
       update opp1; 
}

    static testMethod void TestOppPostMonthly1() {
        
        Account acct = new Account();
        acct.name = 'RPTestacct1';
        acct.BillingCountry = 'USA';
        acct.c2g__CODAAccountsReceivableControl__c = 'a2Z00000000H8ou';
        Insert acct;
        
        Account pacct = new Account();
        pacct.name = 'RPPartner';
        pacct.CurrencyISOcode = 'USD';
        pacct.c2g__CODAAccountsReceivableControl__c = 'a2Z00000000H8ou';        
        Insert pacct;
        
        Promo_Code__c pc = new Promo_Code__c();
        pc.Name = 'TESTPC';
        pc.Account__c = pacct.Id;
        pc.Promo_Type__c = 'Referral';
        insert pc;
        
        pacct.Promo_Code__c = pc.Id;
        update pacct;
        
        Opportunity opp1 = new Opportunity(
        AccountId = acct.id, 
        Name = 'RPTestOpp1', 
        Pricebook2Id = '01s000000004NS6AAM',
        StageName = 'Signature', 
        CloseDate = Date.Today(), 
        Type = 'New Business',
        Billing_Terms__c = 'Net 30',
        Invoice_Created__c = false,
        Billing_Type__c = 'Monthly',
        currencyISOcode = 'USD',
        PartnerLookup__c = pacct.Id,
        Who_is_Invoiced__c = 'Partner is Invoiced', 
        Partner_Commission__c = 10        
        );
        
        Insert opp1;
        
        OpportunityLineItem prod1 = new OpportunityLineItem();
        prod1.OpportunityId = opp1.id;
        prod1.PricebookEntryId = '01u00000000FGwcAAG';
        prod1.Quantity = 12;
        prod1.UnitPrice = 1200;
        prod1.of_MM_Events_Allowed__c = 20;
        prod1.of_CP_Events_Allowed__c = 20;
        prod1.MM_Overage_Fee__c = 25;
        prod1.CP_Overage_Fee__c = 25;
        prod1.of_BLA_IPs__c = 10;
        prod1.of_Rep_Mon_IPs__c = 10;
        prod1.Product_Status__c = 'Current';
        prod1.Description__c = 'Deliverability Tools';
        prod1.Indirect__c = false;
        prod1.Active__c = true;
        prod1.Previous_Value__c = 0;
        prod1.Bundle__c = false;
        prod1.Recurring__c = true;
        prod1.Start_Date__c = Date.valueOf('2013-04-01');
        prod1.End_Date__c = Date.valueOf('2014-03-31');
        Insert prod1;
        
        
       opp1.StageName = 'Finance Approved';
       opp1.Bypass_Product_Portfolio__c = true;
       opp1.Begin_Automation__c = true;
       update opp1; 

}


    static testMethod void TestOppPostMonthly2() {

       Account acct = new Account();
        acct.name = 'RPTestacct1';
        acct.BillingCountry = 'USA';
        acct.c2g__CODAAccountsReceivableControl__c = 'a2Z00000000H8ou';
        Insert acct;
        
        Account pacct = new Account();
        pacct.name = 'RPPartner';
        pacct.CurrencyISOcode = 'USD';
        pacct.c2g__CODAAccountsReceivableControl__c = 'a2Z00000000H8ou';        
        Insert pacct;
        
        Promo_Code__c pc = new Promo_Code__c();
        pc.Name = 'TESTPC';
        pc.Account__c = pacct.Id;
        pc.Promo_Type__c = 'Referral';
        insert pc;
        
        pacct.Promo_Code__c = pc.Id;
        update pacct;
        
        Opportunity opp1 = new Opportunity(
        AccountId = acct.id, 
        Name = 'RPTestOpp1', 
        Pricebook2Id = '01s000000004NS6AAM',
        StageName = 'Signature', 
        CloseDate = Date.Today() ,
        Type = 'New Business',
        Billing_Terms__c = 'Net 30',
        Invoice_Created__c = false,
        Billing_Type__c = 'Monthly',
        currencyISOcode = 'GBP',
        PartnerLookup__c = pacct.Id,
        Deal_Closing_Through__c = 'Return Path',
        Who_is_Invoiced__c = 'Partner is Invoiced', 
        Partner_Commission__c = 10        
        );
        
        Insert opp1;
        
         OpportunityLineItem prod1 = new OpportunityLineItem();
        prod1.OpportunityId = opp1.id;
        prod1.PricebookEntryId = '01u00000000FGwdAAG';
        prod1.Quantity = 12;
        prod1.UnitPrice = 1200;
        prod1.of_MM_Events_Allowed__c = 20;
        prod1.of_CP_Events_Allowed__c = 20;
        prod1.MM_Overage_Fee__c = 25;
        prod1.CP_Overage_Fee__c = 25;
        prod1.of_BLA_IPs__c = 10;
        prod1.of_Rep_Mon_IPs__c = 10;
        prod1.Product_Status__c = 'Current';
        prod1.Description__c = 'Deliverability Tools';
        prod1.Indirect__c = false;
        prod1.Active__c = true;
        prod1.Previous_Value__c = 0;
        prod1.Bundle__c = false;
        prod1.Recurring__c = true;
        prod1.Start_Date__c = Date.valueOf('2013-04-01');
        prod1.End_Date__c = Date.valueOf('2014-03-31');
        Insert prod1;
        
       OpportunityLineItem prod2 = new OpportunityLineItem();
        prod2.OpportunityId = opp1.id;
        prod2.PricebookEntryId = '01u00000000FGwdAAG';
        prod2.Quantity = 12;
        prod2.UnitPrice = 1200;
        prod2.of_MM_Events_Allowed__c = 20;
        prod2.of_CP_Events_Allowed__c = 20;
        prod2.MM_Overage_Fee__c = 25;
        prod2.CP_Overage_Fee__c = 25;
        prod2.of_BLA_IPs__c = 10;
        prod2.of_Rep_Mon_IPs__c = 10;
        prod2.Product_Status__c = 'Current';
        prod2.Description__c = 'Deliverability Tools';
        prod2.Indirect__c = false;
        prod2.Active__c = true;
        prod2.Previous_Value__c = 0;
        prod2.Bundle__c = false;
        prod2.Recurring__c = true;
        prod2.Start_Date__c = Date.valueOf('2013-04-01');
        prod2.End_Date__c = Date.valueOf('2013-06-15');
        Insert prod2;
        
        
       opp1.StageName = 'Finance Approved';
       opp1.Bypass_Product_Portfolio__c = true;
       opp1.Begin_Automation__c = true;
       Test.startTest();
       update opp1; 
       Test.stopTest(); 
}*/

}