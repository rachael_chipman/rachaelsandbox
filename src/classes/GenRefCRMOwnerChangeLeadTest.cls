@isTest
private with sharing class GenRefCRMOwnerChangeLeadTest {
	
	private static testMethod void testLeadAssignmentWithPromoCode()
	{
        Id marketerRecordTypeId = [SELECT Id From RecordType Where Name = 'Marketer' AND sObjectType = 'Account'].Id;
        User testUser = TestUtils.getTestUser( 'TestUser123', 'Exec Admin');
        
        System.runAs( TestUtils.ADMIN_USER ){
            insert testUser;
        }
        
        Account testAccount = TestUtils.createAccount( marketerRecordTypeId, false );
        testAccount.OwnerId = testUser.Id;
        testAccount.Customer_Relationship_Manager_Primary__c = 'Test';
        testAccount.CRM__c = testUser.Id;
        insert testAccount;
        
        Promo_Code__c testPromo = TestUtils.createPromoCode( 'TestCode', 'Referral', testAccount.Id, false );
        testPromo.Account__c = testAccount.Id;
        insert testPromo;
        
        Lead testLead = TestUtils.createLeads( 'TestLead', 'TestLead', 'SDR Accept', false );
        testLead.Gen_Ref_Lead__c = false;
        testLead.OwnerId = testUser.Id;
        testLead.SSC_Promo_Code__c = 'TestCode';
        insert testLead;
        
        Test.startTest();
        testLead.Lead_Asset_Most_Recent_Detail__c = '2012_GL_PRO_WF_.NET Deliverability Channel Referral Form NEW';
        update testLead;
        Test.stopTest();
        
        List<Lead> updatedLead = [SELECT Id, Referring_Partner__c, Referring_CRM2__c, OwnerId FROM Lead];
        
        System.assertEquals ( 1, updatedLead.size(), 'There should be one contact created in the test');
        System.assertEquals ( testAccount.Id, updatedLead[0].Referring_Partner__c, 'Referring_Partner__c should be populated with the accountId');
        System.assertEquals ( testUser.Id, updatedLead[0].Referring_CRM2__c, 'Referring CRM should be the CRM from the promo code account');
        
    }
    
    private static testMethod void testLeadAssignmentWithoutPromoCode()
	{
        Id marketerRecordTypeId = [SELECT Id From RecordType Where Name = 'Marketer' AND sObjectType = 'Account'].Id;
        User testUser = TestUtils.getTestUser( 'TestUser123', 'Exec Admin');
        
        System.runAs( TestUtils.ADMIN_USER ){
            insert testUser;
        }
        
        Lead testLead = TestUtils.createLeads( 'TestLead', 'TestLead', 'SDR Accept', false );
        testLead.Gen_Ref_Lead__c = false;
        testLead.OwnerId = testUser.Id;
        insert testLead;
        
        Test.startTest();
        testLead.Lead_Asset_Most_Recent_Detail__c = '2012_GL_PRO_WF_.NET Deliverability Channel Referral Form NEW';
        update testLead;
        Test.stopTest();
        
        List<Lead> updatedLead = [SELECT Id, Referring_Partner__c, Referring_CRM2__c, OwnerId FROM Lead];
        
        System.assertEquals ( 1, updatedLead.size(), 'There should be one contact created in the test');
        System.assertEquals ( GenRefCRMOwnerChangeLead.genRefDefaultUser.Id, updatedLead[0].Referring_CRM2__c, 'Referring CRM should be the default Gen Ref User');
        
    }
}