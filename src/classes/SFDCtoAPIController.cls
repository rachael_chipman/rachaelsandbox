public with sharing class SFDCtoAPIController {
    
    /*public Opportunity o {get;set;}
    public List<OpportunityLineItem> olis;
    public RealmID__c realmId;
    public Boolean CertOpp {get;set;} 
    public Boolean EISOpp {get;set;}
    public Boolean APSOpp {get;set;}
    public Boolean InboxOpp {get;set;}
    public Boolean BLAActive {get;set;}
    public Boolean RepMonActive {get;set;}
    public Boolean IMActive {get;set;}
    public Boolean IPActive {get;set;}
    public Boolean EMClientActive {get;set;}
    public List<OpportunityLineItem> CertProds;
    public List<OpportunityLineItem> APSProds;
    public List<OpportunityLineItem> EISProds;
    public List<OpportunityLineItem> InboxProds;
    public Boolean BlkListAlert {get;set;} 
    public Boolean RepMonitor {get;set;} 
    public Boolean IbMonitor {get;set;} 
    public Boolean IbPreview {get;set;} 
    public Boolean Certification {get;set;} 
    public Boolean APSolutions {get;set;} 
    public Boolean IbInsight {get;set;} 
    public Double IPLimits {get;set;} 
    public Double IMLimits {get;set;} 
    public String Tier {get;set;} 
    public Decimal CertMaxVol {get;set;}
    public Boolean EmailClient {get;set;}
    public Boolean EMBMonitor {get;set;}
    public Boolean DomProt {get;set;}
    public Boolean DomSec {get;set;}
    public Double MonitoredDomains {get;set;}
    public Double CompetitorDomains {get;set;}
    public Double RepMonIPs {get;set;} 
    public Double EmailClientLimit {get;set;}
    public Decimal totalMonitorLimits {get;set;}
    public Decimal totalPreviewLimits {get;set;}
    public Decimal totalRepMon {get;set;}
    public Decimal totalMonitored {get;set;}
    public Decimal totalCompetitor {get;set;}
    public Decimal totalECM {get;set;}
    public Boolean nameEMB {get;set;}
    public Boolean nameDomProt {get;set;}
    public Boolean nameDomSec {get;set;}
    
    public SFDCtoAPIController(ApexPages.StandardController std)
    {
        o = [SELECT Id, Type, StageName, AccountId, Discount_Total__c, Who_is_Invoiced__c, Number_of_Domains__c, Total_of_Products__c, of_Recurring_Product_Line_Items__c, of_IPs__c, Monthly_Send_Volume__c,
                Monthly_Campaigns__c, Number_of_Platinum_Contacts__c, Certification_Max_Volume__c, Certification_Tier_Sold__c, Renewal_Created_del__c, Previous_Opportunity_Lookup__c, RecordTypeId, Updated_by_Cert_App__c,
                Nature_of_Involvement__c, PartnerLookup__c, RealmID__c, Realm_Id_Created__c, RealmId_Fake__c, Account.ParentId, Synced_With_Internal_Tools_On__c 
                FROM Opportunity 
                WHERE Id = :ApexPages.currentPage().getParameters().get('id')];
                
        olis = [SELECT Id, Product_Code__c, Product_Family__c, Account_Id__c, Product_Status__c, Start_Date__c, End_Date__c, Previous_Value__c, Recurring__c, UnitPrice, Quantity, 
                    TotalPrice, PricebookEntry.Product2Id, PricebookEntry.Product2.Name, PricebookEntry.Product2.Family, Description__c, Product_Detail_Created__c, 
                    of_MM_Events_Allowed__c, of_CP_Events_Allowed__c, MM_Overage_Fee__c, CP_Overage_Fee__c, of_Rep_Mon_IPs__c, of_BLA_IPS__c, Opportunity_Type__c, Service_Level__c, OpportunityId,
                    Blacklist_Alert__c, Reputation_Monitor__c, Inbox_Monitor__c, Inbox_Preview__c, Email_Client_Monitor__c, Number_of_Competitor_Domains__c, Number_of_Monitored_Domains__c 
                    FROM OpportunityLineItem 
                    WHERE OpportunityId = :o.Id];
                    
        try
        {
            realmId = [SELECT Id, Account__c, Anti_Phishing_Solutions__c, Blacklist_Alert__c, Certification__c, Certification_Max_Volume__c, Certification_Tier__c, Company_Abbrev__c, 
                        Email_Intelligence_Suite_Active__c, Inbox_Insight__c, Inbox_Monitor__c, Inbox_Monitor_Monthly_Events_Limit__c, Inbox_Preview__c, Inbox_Preview_Monthly_Events_Limit__c, Number_of_Competitive_Domains__c, 
                        Number_of_Monitored_Domains__c, Opportunity__c, Parent_RealmID__c, RealmID_Type__c, Reputation_Monitor__c, Email_Client_Monitor__c, Blacklist_Alert_IPs__c, Reputation_Monitor_IPs__c, Email_Brand_Monitor__c,
                        Domain_Protect__c, Domain_Secure__c, Email_Client_Monitor_Limit__c
                        FROM RealmID__c
                        WHERE Name = :o.RealmID__c];
        }
        catch (Exception e) {}
                        
        CertOpp = false;
        EISOpp = false;
        APSOpp = false;
        InboxOpp = false;
        BLAActive = false;
        RepMonActive = false;
        IMActive = false;
        IPActive = false;
        EMClientActive = false;
        
        CertProds = new list<OpportunityLineItem>();
        APSProds = new list<OpportunityLineItem>();
        EISProds = new list<OpportunityLineItem>();
        InboxProds = new list<OpportunityLineItem>();
        
        for(OpportunityLineItem oli : olis)
        {
            if(oli.Product_Family__c == 'Certification.EQ')
            {
                CertProds.add(oli);
            }
            if(oli.Product_Family__c == 'Anti-Phishing Solutions')
            {
                APSProds.add(oli);
                if(oli.PricebookEntry.Product2.Name.contains('Email Brand Monitor'))
                {
                    nameEMB = true;
                }
                else if(oli.PricebookEntry.Product2.Name.contains('Domain Protect'))
                {
                    nameDomProt = true;
                }
                else if(oli.PricebookEntry.Product2.Name.contains('Domain Secure'))
                {
                    nameDomSec = true;
                }
            }
            if(oli.Product_Family__c == 'Email Intelligence for Marketers')
            {
                EISProds.add(oli);
                if(oli.Blacklist_Alert__c == 'Yes')
                {
                    BLAActive = true;
                }
                if(oli.Reputation_Monitor__c == 'Yes')
                {
                    RepMonActive = true;
                }
                if(oli.Inbox_Monitor__c == 'Yes')
                {
                    IMActive = true;
                }
                if(oli.Inbox_Preview__c == 'Yes')
                {
                    IPActive = true;
                }
                if(oli.Email_Client_Monitor__c == 'Yes')
                {
                    EMClientActive = true;
                }
            }
            if(oli.Product_Family__c == 'Insight.EQ')
            {
                InboxProds.add(oli);
            }
        }

                for(AggregateResult monitorLimits : [SELECT SUM(of_MM_Events_Allowed__c)IMEvents FROM OpportunityLineItem WHERE OpportunityId = :o.Id])
                {
                    totalMonitorLimits = (Decimal)monitorLimits.get('IMEvents');
                }
                for(AggregateResult previewLimits : [SELECT SUM(of_CP_Events_Allowed__c)IPEvents FROM OpportunityLineItem WHERE OpportunityId = :o.Id])
                {
                    totalPreviewLimits = (Decimal)previewLimits.get('IPEvents');
                }
                for(AggregateResult repMonIPs : [SELECT SUM(of_Rep_Mon_IPs__c)RepMonIPs FROM OpportunityLineItem WHERE OpportunityId = :o.Id])
                {
                    totalRepMon = (Decimal)repMonIPs.get('RepMonIPs');
                }
                for(AggregateResult ECMLimits : [SELECT SUM(Email_Client_Monitor_Limit__c)EmailClient FROM OpportunityLineItem WHERE OpportunityId = :o.Id])
                {
                    totalECM = (Decimal)ECMLimits.get('EmailClient');
                }
                for(AggregateResult monitoredDomains : [SELECT SUM(Number_of_Monitored_Domains__c)MonDomains FROM OpportunityLineItem WHERE OpportunityId = :o.Id])
                {
                    totalMonitored = (Decimal)monitoredDomains.get('MonDomains');
                }
                for(AggregateResult competitorDomains : [SELECT SUM(Number_of_Competitor_Domains__c)CompDomains FROM OpportunityLineItem WHERE OpportunityId = :o.Id])
                {
                    totalCompetitor = (Decimal)competitorDomains.get('CompDomains');
                }
                

        if(!CertProds.isEmpty())
        {
            CertOpp = true;
        }
        if(!APSProds.isEmpty())
        {
            APSOpp = true;
        }
        if(!EISProds.isEmpty())
        {
            EISOpp = true;
        }
        if(!InboxProds.isEmpty())
        {
            InboxOpp = true; 
        }
        
    }
    
    public RealmID__c CreateUpdateRealmId()
    {
        //set variables IAW current RealmID info
        if(realmId != null)
        {
            BlkListAlert = realmId.Blacklist_Alert__c;
            RepMonitor = RealmId.Reputation_Monitor__c;
            IbMonitor = RealmId.Inbox_Monitor__c;
            IbPreview = RealmId.Inbox_Preview__c;
            Certification = RealmId.Certification__c;
            APSolutions = RealmId.Anti_Phishing_Solutions__c;
            IbInsight = RealmId.Inbox_Insight__c;
            IPLimits = RealmId.Inbox_Preview_Monthly_Events_Limit__c;
            IMLimits = RealmId.Inbox_Monitor_Monthly_Events_Limit__c;
            Tier = RealmID.Certification_Tier__c;
            CertMaxVol = RealmId.Certification_Max_Volume__c;
            EmailClient = RealmId.Email_Client_Monitor__c;
            EMBMonitor = RealmId.Email_Brand_Monitor__c;
            DomProt = RealmId.Domain_Protect__c;
            DomSec = RealmId.Domain_Secure__c;
            MonitoredDomains = RealmId.Number_of_Monitored_Domains__c;
            CompetitorDomains = RealmId.Number_of_Competitive_Domains__c;
            RepMonIPs = RealmId.Reputation_Monitor_IPs__c;
            EmailClientLimit = RealmId.Email_Client_Monitor_Limit__c;
        }
        else
        {
            BlkListAlert = false;
            RepMonitor = false;
            IbMonitor = false;
            IbPreview = false;
            Certification = false;
            APSolutions = false;
            IbInsight = false;
            IPLimits = 0;
            IMLimits = 0;
            Tier = null;
            CertMaxVol = null;
            EmailClient = false;
            EMBMonitor = false;
            DomProt = false;
            DomSec = false;
            MonitoredDomains = 0;
            CompetitorDomains = 0;
            RepMonIPs = 0;  
            EmailClientLimit = 0;   
        }
        RealmID__c newRealm = new RealmID__c();
        newrealm.Account__c = o.AccountId;
        newrealm.Name = o.RealmID__c;
        newrealm.Realm_Id_External_Id__c = newrealm.name;
        newrealm.Opportunity__c = o.Id;
        //Certification
        if(CertOpp == true && o.StageName == 'Finance Approved') newrealm.Certification__c = true;
        else if(CertOpp == true && o.StageName == 'Terminated Contract') newrealm.Certification__c = false;
        else newrealm.Certification__c = Certification;
        //Blacklist Alert
        if(EISOpp == true && o.StageName == 'Finance Approved') newrealm.Blacklist_Alert__c = BLAActive;
        else if(EISOpp == true && o.StageName == 'Terminated Contract') newrealm.Blacklist_Alert__c = false;
        else newrealm.Blacklist_Alert__c = BlkListAlert;
        //Inbox Monitor
        if(EISOpp == true && o.StageName == 'Finance Approved') newrealm.Inbox_Monitor__c = IMActive;
        else if(EISOpp == true && o.StageName == 'Terminated Contract') newrealm.Inbox_Monitor__c = false;
        else newrealm.Inbox_Monitor__c = IbMonitor;
        //Inbox Preview
        if(EISOpp == true && o.StageName == 'Finance Approved') newrealm.Inbox_Preview__c = IPActive;
        else if(EISOpp == true && o.StageName == 'Terminated Contract') newrealm.Inbox_Preview__c = false;
        else newrealm.Inbox_Preview__c = IbPreview;
        //Reputation Monitor
        if(EISOpp == true && o.StageName == 'Finance Approved') newrealm.Reputation_Monitor__c = RepMonActive;
        else if(EISOpp == true && o.StageName == 'Terminated Contract') newrealm.Reputation_Monitor__c = false;
        else newrealm.Reputation_Monitor__c = RepMonitor;
        //Email Client Monitor
        if(EISOpp == true && o.StageName == 'Finance Approved') newrealm.Email_Client_Monitor__c = EMClientActive;
        else if(EISOpp == true && o.StageName == 'Terminated Contract') newrealm.Email_Client_Monitor__c = false;
        else newrealm.Email_Client_Monitor__c = EmailClient;
        //APS
        if(APSOpp == true && o.StageName == 'Finance Approved') newrealm.Anti_Phishing_Solutions__c = true;
        else if (APSOpp == true && o.StageName == 'Terminated Contract') newrealm.Anti_Phishing_Solutions__c = false;
        else newrealm.Anti_Phishing_Solutions__c = APSolutions;
        //Inbox Insight
        if(InboxOpp == true && o.StageName == 'Finance Approved') newrealm.Inbox_Insight__c = true;
        else if(InboxOpp == true && o.StageName == 'Terminated Contract') newrealm.Inbox_Insight__c = false;
        else newrealm.Inbox_Insight__c = IbInsight;
        //Monitor Limits
        if(EISOpp == true && o.StageName == 'Finance Approved' && o.Type == 'Supplement') newrealm.Inbox_Monitor_Monthly_Events_Limit__c = totalMonitorLimits + IMLimits;
        else if(EISOpp == true && o.StageName == 'Finance Approved') newrealm.Inbox_Monitor_Monthly_Events_Limit__c = totalMonitorLimits;
        else newrealm.Inbox_Monitor_Monthly_Events_Limit__c = IMLimits;
        //Preview Limits
        if(EISOpp == true && o.StageName == 'Finance Approved' && o.Type == 'Supplement') newrealm.Inbox_Preview_Monthly_Events_Limit__c = totalPreviewLimits + IPLimits;
        else if(EISOpp == true && o.StageName == 'Finance Approved' && o.Type != 'Fee' && o.Type != 'Supplement') newrealm.Inbox_Preview_Monthly_Events_Limit__c = totalPreviewLimits;
        else newrealm.Inbox_Preview_Monthly_Events_Limit__c = IPLimits;
        //Rep Mon IPs
        if(EISOpp == true && o.StageName == 'Finance Approved' && o.Type == 'Supplement') newrealm.Reputation_Monitor_IPs__c = totalRepMon + RepMonIPs;
        else if(EISOpp == true && o.StageName == 'Finance Approved' && o.Type != 'Fee' && o.Type != 'Supplement') newrealm.Reputation_Monitor_IPs__c = totalRepMon;
        else newrealm.Reputation_Monitor_IPs__c = RepMonIPs;
        //EMC Limits
        if(EISOpp == true && o.StageName == 'Finance Approved' && o.Type == 'Supplement') newrealm.Email_Client_Monitor_Limit__c = totalECM + EmailClientLimit;
        else if(EISOpp == true && o.StageName == 'Finance Approved' && o.Type != 'Fee' && o.Type != 'Supplement') newrealm.Email_Client_Monitor_Limit__c = totalECM;
        else newrealm.Email_Client_Monitor_Limit__c = EmailClientLimit;
        //Cert Tier
        if(CertOpp == true && o.StageName == 'Finance Approved' && o.Type != 'Fee') newrealm.Certification_Tier__c = o.Certification_Tier_Sold__c;
        else newrealm.Certification_Tier__c = Tier;
        //Cert Max Vol
        if(CertOpp == true && o.StageName == 'Finance Approved' && o.Type != 'Fee') newrealm.Certification_Max_Volume__c = o.Certification_Max_Volume__c;
        else newrealm.Certification_Max_Volume__c = CertMaxVol;
        //Monitored Domains
        if(APSOpp == true && o.StageName == 'Finance Approved' && o.Type == 'Supplement') newrealm.Number_of_Monitored_Domains__c = totalMonitored + MonitoredDomains;
        else if (APSOpp == true && o.StageName == 'Finance Approved' && o.Type != 'Fee' && o.Type != 'Supplement') newrealm.Number_of_Monitored_Domains__c = totalMonitored;
        else newrealm.Number_of_Monitored_Domains__c = MonitoredDomains;
        //Competitor Domains
        if(InboxOpp == true && o.StageName == 'Finance Approved' && o.Type == 'Supplement') newrealm.Number_of_Competitive_Domains__c = totalCompetitor + CompetitorDomains;
        else if(InboxOpp == true && o.StageName == 'Finance Approved' && o.Type != 'Supplement' && o.Type != 'Fee') newrealm.Number_of_Competitive_Domains__c = totalCompetitor;
        else newrealm.Number_of_Competitive_Domains__c = CompetitorDomains;
        //Email Brand Monitor
        if(APSOpp == true && o.StageName == 'Finance Approved' && nameEMB == true) newrealm.Email_Brand_Monitor__c = true;
        else if(APSOpp == true && o.StageName == 'Terminated Contract' && nameEMB == true) newrealm.Email_Brand_Monitor__c = false;
        else newrealm.Email_Brand_Monitor__c = EMBMonitor;
        //Domain Protect
        if(APSOpp == true && o.StageName == 'Finance Approved' && nameDomProt == true) newrealm.Domain_Protect__c = true;
        else if(APSOpp == true && o.StageName == 'Terminated Contract' && nameDomProt == true) newrealm.Domain_Protect__c = false;
        else newrealm.Domain_Protect__c = DomProt;
        //Domain Secure
        if(APSOpp == true && o.StageName == 'Finance Approved' && nameDomSec == true) newrealm.Domain_Secure__c = true;
        else if(APSOpp == true && o.StageName == 'Terminated Contract' && nameDomSec == true) newrealm.Domain_Secure__c = false;
        else newrealm.Domain_Secure__c = DomSec;
        
        newrealm.Last_Successful_Sync__c = system.now().format(); 
        return newrealm;
    }
    
    public static HttpResponse MakeHTTPCall(RealmID__c newrealm, Opportunity o)
    {
        String baseEndpoint = 'http://w3apidev.returnpath.net/v1/salesforce/realms/';
        String endpointParams = '?api_key=12340c9ec9dd31a6e805393c045996bf67722bd9&api_hash=014fd14e27d0d11a4b8ed6e7489806a786acc13cd2c96802c348a0723abef3a1&api_salt=123';
        String jsonbody;
        Http h=new Http();
        
        //Instantiate a new HTTP request, specify the method (POST) as well as the endpoint
            HttpRequest req= new HttpRequest();

            req.setEndpoint(baseEndpoint+newrealm.Name+endpointParams);
            req.setMethod('POST');
            req.setTimeout(60000);

            // Set data to send over into post JSON
            req.setBody(JSON.serialize(newrealm));
            system.debug('%^&*'+JSON.serialize(newrealm));
            
            // Send the request, and return a response
            HttpResponse res;
            try
            {
                res = h.send(req);
            }
            catch (System.CalloutException e)
            {
                throw new HTTPException('Your request has timed out, please try again in a few minutes.');
            }
            
            // Get Response to make sure it's okay
            o.API_Call_Response_Code__c = res.getStatusCode();
            update o;
            
            
            //Get JSON body - for debug purposes
            jsonbody = res.getBody();
            
            try
            {
                JSONParser jp = JSON.createParser(jsonbody);
                while (jp.nextToken() != null) 
                {
                    System.debug('Current token: ' +
                    jp.getCurrentToken());
                    jp.getDecimalValue();
                    system.debug('Decimal Value' + jp.getDecimalValue()); 
                    if (jp.getCurrentToken() .equals('Inbox_Monitor_Monthly_Event_Limit__c') && jp.getDecimalValue() != newrealm.Inbox_Monitor_Monthly_Events_Limit__c) 
                    { 
                        throw new HTTPException('Inbox Monitor Event Limit Discrepancy'); 
                    }
                    if (jp.getCurrentToken() .equals('Certification__c') && jp.getBooleanValue() != newrealm.Certification__c) 
                    { 
                        throw new HTTPException('Certification Active/Inactive Discrepancy'); 
                    }
                    if (jp.getCurrentToken() .equals('Blacklist_Alert__c') && jp.getBooleanValue() != newrealm.Blacklist_Alert__c) 
                    { 
                        throw new HTTPException('BLA Active/Inactive Discrepancy'); 
                    }
                    if (jp.getCurrentToken() .equals('Inbox_Monitor__c') && jp.getBooleanValue() != newrealm.Inbox_Monitor__c) 
                    { 
                        throw new HTTPException('IM Active/Inactive Discrepancy'); 
                    }
                    if (jp.getCurrentToken() .equals('Inbox_Preview__c') && jp.getBooleanValue() != newrealm.Inbox_Preview__c) 
                    { 
                        throw new HTTPException('IP Active/Inactive Discrepancy'); 
                    }
                    if (jp.getCurrentToken() .equals('Reputation_Monitor__c') && jp.getBooleanValue() != newrealm.Reputation_Monitor__c) 
                    { 
                        throw new HTTPException('RepMon Active/Inactive Discrepancy'); 
                    }
                    if (jp.getCurrentToken() .equals('Email_Client_Monitor__c') && jp.getBooleanValue() != newrealm.Email_Client_Monitor__c) 
                    { 
                        throw new HTTPException('Email Client Monitor Active/Inactive Discrepancy'); 
                    }
                    if (jp.getCurrentToken() .equals('Anti_Phishing_Solutions__c') && jp.getBooleanValue() != newrealm.Anti_Phishing_Solutions__c) 
                    { 
                        throw new HTTPException('APS Active/Inactive Discrepancy'); 
                    }
                    if (jp.getCurrentToken() .equals('Inbox_Insight__c') && jp.getBooleanValue() != newrealm.Inbox_Insight__c) 
                    { 
                        throw new HTTPException('Insight Active/Inactive Discrepancy'); 
                    }
                    if (jp.getCurrentToken() .equals('Inbox_Preview_Monthly_Event_Limit__c') && jp.getDecimalValue() != newrealm.Inbox_Preview_Monthly_Events_Limit__c) 
                    { 
                        throw new HTTPException('Inbox Preview Event Limit Discrepancy'); 
                    }
                    if (jp.getCurrentToken() .equals('Certification_Tier__c') && jp.getValue != newrealm.Certification_Tier__c) 
                    { 
                        throw new HTTPException('Cert Tier Discrepancy'); 
                    }
                    if (jp.getCurrentToken() .equals('Certification_Max_Volume__c') && jp.getDecimalValue() != newrealm.Certification_Max_Volume__c) 
                    { 
                        throw new HTTPException('Cert Max Vol Discrepancy'); 
                    }
                }
            }
            catch(Exception e){}
            
            system.debug('!@#$'+jsonbody);
            if(res.getStatusCode() != 200)
            {
                throw new HTTPException('Error occured while sending HTTP request, your administrator has been notified:'+ res.getStatus()+'-'+res.getStatusCode()+'+'+jsonbody);
            
            }
            return res;
            
    }
    
    public void SaveRealmId()
    {
        List<Account> childAccounts = [SELECT Id, ParentId FROM Account WHERE ParentId = :o.AccountId];
        if(childAccounts.isEmpty() && o.Account.ParentId == null && o.Type == 'New Business')
        {
            RealmID__c newrealm = CreateUpdateRealmId();
            try
            {
                MakeHTTPCall(newrealm, o);
                upsert newrealm Realm_Id_External_Id__c;
                ApexPages.Message msg = new ApexPages.Message(ApexPages.severity.info, 'Your update was successful!');
                ApexPages.addMessage(msg);
                o.Synced_With_Internal_Tools_On__c = system.now().format();
                update o;
                
            }
            catch (HTTPException e)
            {
                ApexPages.addMessages(e);
            }
        }
        else
        {
            ApexPages.Message msg2 = new ApexPages.Message(ApexPages.severity.ERROR, 'Only New Business Opportunities that have no parent and no children accounts can be updated at this time.');
            ApexPages.addMessage(msg2);
        }
        
    }
 */   
}