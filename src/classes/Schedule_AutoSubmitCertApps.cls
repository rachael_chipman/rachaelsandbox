global class Schedule_AutoSubmitCertApps implements Schedulable {
	global void execute( SchedulableContext ctx)
	{
		OpportunityServices.autoSubmitCertApps();
    }
}