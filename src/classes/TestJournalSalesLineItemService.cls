@isTest
private class TestJournalSalesLineItemService {
    public static Account testAccount;
    public static product2 testProduct;
    public static c2g__codaGeneralLedgerAccount__c testGeneralLedgerAccount;
    public static integer NUM_OF_OPPORTUNITIES = 10;
    public static List<c2g__codaJournal__c> testJournals = new List<c2g__codaJournal__c>();
    public static integer NUM_OF_JOURNALS = 10;
    public static List<c2g__codaGroupingReference__c> testGroups = new List<c2g__codaGroupingReference__c>();
        
    static void datasetup(){
        testGeneralLedgerAccount = TestUtils.createGeneralLedgerAccount(true);
        testAccount = TestUtils.createAccount_FinancialForce( TestUtils.PARTNER_RECORD_TYPE.Id, testGeneralLedgerAccount.Id, true );
        testProduct = TestUtils.createProduct_FinancialForce( testGeneralLedgerAccount.Id, true );
        PriceBookEntry testPBE = TestUtils.createPricebookEntry( testProduct.Id, TestUtils.STANDARD_PRICE_BOOK.Id, true );
    }
    
    @isTest(seeAllData=true)    
    static void testSalesJournalInserts() 
    {
        datasetup();
        //List<Opportunity> testOpportunities = TestUtils.createOpportunities( NUM_OF_OPPORTUNITIES, TestUtils.AWARENESS_RECORD_TYPE.Id, testAccount.Id, true );
        List<Opportunity> testOpportunities = TestUtils.createOpportunities( NUM_OF_OPPORTUNITIES, TestUtils.RESEARCH_RECORD_TYPE.Id, testAccount.Id, true );
        List<c2g__codaInvoice__c> testSalesInvoices = new List<c2g__codaInvoice__c>();
        List<c2g__codaInvoiceLineItem__c> testSalesInvoiceLineItems = new List<c2g__codaInvoiceLineItem__c>();
        
        for( Opportunity anOpportunity : testOpportunities )
        {
            TestUtils.createSalesInvoice( testAccount.Id, new Map<String,Object>{ 'c2g__Opportunity__c' => anOpportunity.Id }, false );
            testSalesInvoices.add( TestUtils.createSalesInvoice( testAccount.Id, new Map<String,Object>{ 'c2g__Opportunity__c' => anOpportunity.Id }, false ) );
        }

        insert testSalesInvoices;
        
        for( c2g__codaInvoice__c testSalesInvoice: testSalesInvoices )
        {
            testSalesInvoiceLineItems.add( TestUtils.createSalesInvoiceLineItem( testSalesInvoice.Id, testProduct.Id, false ) );
        }
        testJournals.addAll(TestUtils.createJournals(NUM_OF_JOURNALS,null,false));
        testGroups = testUtils.createGroupReferences(NUM_OF_JOURNALS,null,true);
        for (integer i=0;i<NUM_OF_OPPORTUNITIES;i++){
            testSalesInvoiceLineItems[i].c2g__IncomeScheduleGroup__c = testGroups[i].id;
            testJournals[i].c2g__IncomeScheduleGroup__c = testGroups[i].id;
        }
        insert testSalesInvoiceLineItems; 
        test.startTest();
            insert testJournals;
        test.stopTest();

        List<c2g__codaJournal__c> insertedJournals = [ SELECT id, c2g__IncomeScheduleGroup__c, Opportunity__c FROM c2g__codaJournal__c WHERE Id IN :testJournals ];
        Map<ID,c2g__codaInvoiceLineItem__c> lineItemsMap = new Map<ID,c2g__codaInvoiceLineItem__c>([Select Id, c2g__IncomeScheduleGroup__c,c2g__Invoice__r.c2g__Opportunity__c From  c2g__codaInvoiceLineItem__c where c2g__IncomeScheduleGroup__c in : testGroups ]); 
        Map<ID,ID> invoiceLineItemGroupIdOppIdMap = new Map<ID,ID>();
        for (c2g__codaInvoiceLineItem__c item :lineItemsMap.values()){
            invoiceLineItemGroupIdOppIdMap.put(item.c2g__IncomeScheduleGroup__c,item.c2g__Invoice__r.c2g__Opportunity__c);
        }
        System.assertEquals(NUM_OF_JOURNALS,insertedJournals.size(),'All journals have been retrieved');
        System.assertEquals(NUM_OF_OPPORTUNITIES,lineItemsMap.size(),'All sales invoice line items have been retrieved');
        for( c2g__codaJournal__c insertedJournal: insertedJournals )
        {
            System.assertNotEquals( null,insertedJournal.Opportunity__c, 'The journal opportunity is populated' );
            System.assertEquals( insertedJournal.Opportunity__c, invoiceLineItemGroupIdOppIdMap.get(insertedJournal.c2g__IncomeScheduleGroup__c), 'The journal is populated with opportunity lookup from the invoice line item with the same group reference' );
        }

    }
    
    @isTest(seeAllData=true)    
    static void testSalesJournalInserts_withoutJournalGroupId_negativeTest() 
    {
        datasetup();
        //List<Opportunity> testOpportunities = TestUtils.createOpportunities( NUM_OF_OPPORTUNITIES, TestUtils.AWARENESS_RECORD_TYPE.Id, testAccount.Id, true );
        List<Opportunity> testOpportunities = TestUtils.createOpportunities( NUM_OF_OPPORTUNITIES, TestUtils.RESEARCH_RECORD_TYPE.Id, testAccount.Id, true );
        List<c2g__codaInvoice__c> testSalesInvoices = new List<c2g__codaInvoice__c>();
        List<c2g__codaInvoiceLineItem__c> testSalesInvoiceLineItems = new List<c2g__codaInvoiceLineItem__c>();
        
        for( Opportunity anOpportunity : testOpportunities )
        {
            TestUtils.createSalesInvoice( testAccount.Id, new Map<String,Object>{ 'c2g__Opportunity__c' => anOpportunity.Id }, false );
            testSalesInvoices.add( TestUtils.createSalesInvoice( testAccount.Id, new Map<String,Object>{ 'c2g__Opportunity__c' => anOpportunity.Id }, false ) );
        }

        insert testSalesInvoices;
        
        for( c2g__codaInvoice__c testSalesInvoice: testSalesInvoices )
        {
            testSalesInvoiceLineItems.add( TestUtils.createSalesInvoiceLineItem( testSalesInvoice.Id, testProduct.Id, false ) );
        }
        testJournals.addAll(TestUtils.createJournals(NUM_OF_JOURNALS,null,false));
        testGroups = testUtils.createGroupReferences(NUM_OF_JOURNALS,null,true);
        for (integer i=0;i<NUM_OF_OPPORTUNITIES;i++){
            testSalesInvoiceLineItems[i].c2g__IncomeScheduleGroup__c = testGroups[i].id;
        }
        insert testSalesInvoiceLineItems; 
        test.startTest();
            insert testJournals;
        test.stopTest();

        List<c2g__codaJournal__c> insertedJournals = [ SELECT id, c2g__IncomeScheduleGroup__c, Opportunity__c FROM c2g__codaJournal__c WHERE Id IN :testJournals ];
        Map<ID,c2g__codaInvoiceLineItem__c> lineItemsMap = new Map<ID,c2g__codaInvoiceLineItem__c>([Select Id, c2g__IncomeScheduleGroup__c,c2g__Invoice__r.c2g__Opportunity__c From  c2g__codaInvoiceLineItem__c where c2g__IncomeScheduleGroup__c in : testGroups ]); 
        Map<ID,ID> invoiceLineItemGroupIdOppIdMap = new Map<ID,ID>();
        System.assertEquals(NUM_OF_JOURNALS,insertedJournals.size(),'All journals have been retrieved');
        System.assertEquals(NUM_OF_OPPORTUNITIES,lineItemsMap.size(),'All sales invoice line items have been retrieved');
        for (c2g__codaInvoiceLineItem__c item :lineItemsMap.values()){
            invoiceLineItemGroupIdOppIdMap.put(item.c2g__IncomeScheduleGroup__c,item.c2g__Invoice__r.c2g__Opportunity__c);
        }
        for( c2g__codaJournal__c insertedJournal: insertedJournals )
        {
            System.assertEquals( null,insertedJournal.Opportunity__c, 'The journal opportunity is empty' );
        }

    }
    
    @isTest(seeAllData=true)    
    static void testSalesJournalInserts_withoutInvoiceGroupId_negativeTest() 
    {
        datasetup();
        //List<Opportunity> testOpportunities = TestUtils.createOpportunities( NUM_OF_OPPORTUNITIES, TestUtils.AWARENESS_RECORD_TYPE.Id, testAccount.Id, true );
        List<Opportunity> testOpportunities = TestUtils.createOpportunities( NUM_OF_OPPORTUNITIES, TestUtils.RESEARCH_RECORD_TYPE.Id, testAccount.Id, true );
        List<c2g__codaInvoice__c> testSalesInvoices = new List<c2g__codaInvoice__c>();
        List<c2g__codaInvoiceLineItem__c> testSalesInvoiceLineItems = new List<c2g__codaInvoiceLineItem__c>();
        
        for( Opportunity anOpportunity : testOpportunities )
        {
            TestUtils.createSalesInvoice( testAccount.Id, new Map<String,Object>{ 'c2g__Opportunity__c' => anOpportunity.Id }, false );
            testSalesInvoices.add( TestUtils.createSalesInvoice( testAccount.Id, new Map<String,Object>{ 'c2g__Opportunity__c' => anOpportunity.Id }, false ) );
        }

        insert testSalesInvoices;
        
        for( c2g__codaInvoice__c testSalesInvoice: testSalesInvoices )
        {
            testSalesInvoiceLineItems.add( TestUtils.createSalesInvoiceLineItem( testSalesInvoice.Id, testProduct.Id, false ) );
        }
        testJournals.addAll(TestUtils.createJournals(NUM_OF_JOURNALS,null,false));
        testGroups = testUtils.createGroupReferences(NUM_OF_JOURNALS,null,true);
        for (integer i=0;i<NUM_OF_JOURNALS;i++){
            testJournals[i].c2g__IncomeScheduleGroup__c = testGroups[i].id;
        }
        insert testSalesInvoiceLineItems;
        test.startTest();
            insert testJournals;
        test.stopTest();

        List<c2g__codaJournal__c> insertedJournals = [ SELECT id, c2g__IncomeScheduleGroup__c, Opportunity__c FROM c2g__codaJournal__c WHERE Id IN :testJournals ];
//        System.assert(false,'testJournals '+testJournals +'insertedJournals '+insertedJournals); 
        // need pluck as soql would not pick up ids from testSalesInvoiceLineItems alone
        List<c2g__codaInvoiceLineItem__c> lines = [Select Id, c2g__IncomeScheduleGroup__c,c2g__Invoice__r.c2g__Opportunity__c From  c2g__codaInvoiceLineItem__c where id in : testSalesInvoiceLineItems];
        Map<ID,c2g__codaInvoiceLineItem__c> lineItemsMap = new Map<ID,c2g__codaInvoiceLineItem__c>([Select Id, c2g__IncomeScheduleGroup__c,c2g__Invoice__r.c2g__Opportunity__c From  c2g__codaInvoiceLineItem__c where c2g__IncomeScheduleGroup__c in : Pluck.ids(testSalesInvoiceLineItems) ]); 
        Map<ID,ID> invoiceLineItemGroupIdOppIdMap = new Map<ID,ID>(); 
        System.assertEquals(NUM_OF_JOURNALS,insertedJournals.size(),'All journals have been retrieved');
        System.assertEquals(NUM_OF_OPPORTUNITIES,lines.size(),'All sales invoice line items have been retrieved'+'lineItemsMap '+lineItemsMap+ 'lines '+lines+' testSalesInvoiceLineItems '+testSalesInvoiceLineItems+ ' pluck '+ Pluck.ids(testSalesInvoiceLineItems));
        for (c2g__codaInvoiceLineItem__c item :lineItemsMap.values()){
            invoiceLineItemGroupIdOppIdMap.put(item.c2g__IncomeScheduleGroup__c,item.c2g__Invoice__r.c2g__Opportunity__c);
        }
        for( c2g__codaJournal__c insertedJournal: insertedJournals )
        {
            System.assertEquals( null,insertedJournal.Opportunity__c, 'The journal opportunity is empty' );
        }

    }
}