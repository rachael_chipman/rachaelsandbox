public without sharing class CampaignFieldUpdate {

    public static FINAL String SOURCE = 'Source';
    public static FINAL String ASSET = 'Asset';
    public static Map<Id, Lead> updateLeadMap = new Map<Id, Lead>();
    public static Map<Id, Contact> updateContactMap = new Map<Id, Contact>();
    public static Map<Id, Lead> oldLeadMap = new Map<Id, Lead>();
    public static Map<Id, Contact> oldContactMap = new Map<Id, Contact>();
    public static List<CampaignMember> filterCampaignMembersOnInsert ( List<campaignMember> newMembers, Map<Id, CampaignMember> oldMembers )
    {
        List<CampaignMember> filteredMembers = new List<CampaignMember>();
        for( CampaignMember member : newMembers )
        {
            CampaignMember oldMember;
            if( oldMembers != NULL )
            {
                oldMember = oldMembers.get( member.Id );
            }
            if((member.Campaign_Sub_Type__c == SOURCE || member.Campaign_Sub_Type__c == ASSET) &&
                ((oldMember == NULL && member.Run_First_Most_Recent_Campaign_Sync__c) || 
                (member.Run_First_Most_Recent_Campaign_Sync__c && !oldMember.Run_First_Most_Recent_Campaign_Sync__c )))
            {
                filteredMembers.add(member);
            }
        }
        system.debug('What is in filtered Members?'+filteredMembers);
        return filteredMembers;
    }

    private static Map<Id, Lead> memberIdToLead (List<CampaignMember> filteredMembers)
    {
        Map<Id, Lead> campaignMemberToLead = new Map<Id, Lead>();
        Set<String> leadIds = pluck.strings('LeadId', filteredMembers);
        List<Lead> leads = [SELECT Id, OwnerId, Status, First_Campaign__c, Campaign_Name__c, Campaign_Original__c, Campaign_Source_Most_Recent__c, Campaign_Asset_Original__c, Campaign_Asset_Most_Recent__c,
                            Lead_Asset_Most_Recent_Detail__c, Lead_Asset_Most_Recent__c, Lead_Asset_Original_Detail__c, Lead_Asset_Original__c, Most_Recent_Lead_Source__c, Lead_Source_Most_Recent_Detail__c, LeadSource, 
                            isConverted, Lead_Source_Original_Detail__c FROM Lead WHERE Id IN : leadIds ];
        Map<Id, Lead> leadIdToLead = new Map<Id, Lead>(leads);
        for(CampaignMember member : filteredMembers)
        {
            if(leadIdToLead.containsKey(member.LeadId))
            {
                campaignMemberToLead.put(member.Id, leadIdToLead.get(member.LeadId));
            }
        }
        system.debug('What is in the memberToLead map?'+campaignMemberToLead);
        return campaignMemberToLead;
    }

    private static Map<Id, Contact> memberIdtoContact (List<CampaignMember> filteredMembers)
    {
        Map<Id, Contact> campaignMemberToContact = new Map<Id, Contact>();
        Set<String> contactIds = pluck.strings('ContactId', filteredMembers);
        List<Contact> contacts = [SELECT Id, First_Campaign__c, Campaign_Name__c, First_Campaign_Id__c, Most_Recent_Campaign_Id__c, Most_Recent_Lead_Source__c, Lead_Asset_Most_Recent__c FROM Contact WHERE Id IN: contactIds];
        Map<Id, Contact> contactIdToContact = new Map<Id, Contact>(contacts);
        for(CampaignMember member : filteredMembers)
        {
            if(contactIdToContact.containsKey(member.ContactId))
            {
                campaignMemberToContact.put(member.Id, contactIdToContact.get(member.ContactId));
            }
        }
        return campaignMemberToContact;
    }

    private static Map<Id, List<Id>> memberIdToOppIds(List<CampaignMember> filteredMembers)
    {
        Map<Id, List<Id>> memberIdToOpportunityIds = new Map<Id,List<Id>>();    
        Set<String> contactIds = pluck.strings('ContactId', filteredMembers);
        List<OpportunityContactRole> contactRoles = [SELECT Id, ContactId, OpportunityId, Opportunity.Most_Recent_Campaign__c FROM OpportunityContactRole WHERE ContactId IN: contactIds AND Opportunity.isClosed = false];
        Map<Id, List<OpportunityContactRole>> contactIdToOppContactRoles = groupBy.ids('ContactId', contactRoles);
        Set<String> oppIds = pluck.strings('OpportunityId', contactRoles);
        List<Opportunity> oppsWithContactRoles = [SELECT Id, Most_Recent_Campaign__c, ( SELECT Id, OpportunityId, ContactId FROM OpportunityContactRoles WHERE ContactId IN: contactIds) FROM Opportunity WHERE isClosed = false and Id IN: oppIds];

        for(CampaignMember member : filteredMembers)
        {
            if(contactIdToOppContactRoles.containsKey(member.ContactId))
            {
                memberIdToOpportunityIds.put(member.Id, new List<Id>());

                for(Opportunity opp : oppsWithContactRoles)
                {
                    memberIdToOpportunityIds.get(member.Id).add(opp.Id);
                }
            }
            
        }
        return memberIdToOpportunityIds;
    }


    private static Map<Id, Campaign> memberIdToCampaignType(List<CampaignMember> filteredMembers)
    {
        Map<Id, Campaign> memberIdToCampaignTypeMap = new Map<Id, Campaign>();
        Set<String> campaignIds = pluck.strings('CampaignId', filteredMembers);
        List<Campaign> camplist = [Select Id, Name,Type,Campaign_Sub_Type__c from Campaign where ID in:campaignIds];
        Map<Id, Campaign> campaignIdToCampaign = new Map<Id, Campaign>(camplist);
        for(CampaignMember member : filteredMembers)
        {
            if(campaignIdToCampaign.containsKey(member.CampaignId))
            {
                memberIdToCampaignTypeMap.put(member.Id, campaignIdToCampaign.get(member.CampaignId));
            }
        }
        return memberIdToCampaignTypeMap;
    }

    public static Contact buildContact(Contact aContact, Campaign relatedAssetCampaign, Campaign relatedSourceCampaign)
    {
        Contact contact = aContact;
        
        if(relatedAssetCampaign != NULL)
        {
            //Always populate the 'Most Recent' fields
            //Campaign Asset field is a lookup to the campaign object, pass in campaign id to this field
            contact.Campaign_Asset_Most_Recent__c = relatedAssetCampaign.Id;
            //Lead Asset field is the Type field from the campaign
            contact.Lead_Asset_Most_Recent__c = relatedAssetCampaign.Type;
            //Lead Asset Detail is the Name of the campaign
            contact.Lead_Asset_Most_Recent_Detail__c = relatedAssetCampaign.Name;
            //The label for this field is Campaign: Most Recent and it is the Name of the campaign (Keeping this behind the scenes because other functionality relies on it)
            contact.Campaign_Name__c = relatedAssetCampaign.Name;
            //Legacy field that we are going to continue populating so as not to break other functionality
            contact.Most_Recent_Campaign_Id__c = relatedAssetCampaign.Id;
            contact.Lead_Asset_Most_Recent_Time_Date_Stamp__c = system.now();
            //Only populate the 'Original' fields if they are null 
            if( contact.Campaign_Asset_Original__c == NULL )
            {
                //Campaign Asset field is a lookup to the campaign object, pass in campaign id to this field
                contact.Campaign_Asset_Original__c = relatedAssetCampaign.Id;
                //Lead Asset field is the Type field from the campaign
                contact.Lead_Asset_Original__c = relatedAssetCampaign.Type;
                //Lead Asset Detail is the Name of the campaign
                contact.Lead_Asset_Original_Detail__c = relatedAssetCampaign.Name;
                contact.Lead_Asset_Most_Recent_Time_Date_Stamp__c = system.now();
                if(contact.First_Campaign__c == null)
                {
                    //The label for this field is Campaign: Original and it is the name of the campaign (Keeping this behind the scenes because other functionality relies on it)
                    contact.First_Campaign__c = relatedAssetCampaign.Name;
                    //Legacy field that we are going to continue populating so as not to break other functionality
                    contact.First_Campaign_Id__c = relatedAssetCampaign.Id;
                }
            }
        }
        if(relatedSourceCampaign != NULL)
        {
            //Always populate the 'Most Recent' fields
            //Campaign Source field is a lookup to the campaign object, pass in campaign id to this field
            contact.Campaign_Source_Most_Recent__c = relatedSourceCampaign.Id;
            //Lead Source field is the Type field from the campaign
            contact.Most_Recent_Lead_Source__c = relatedSourceCampaign.Type;
            //Lead Source Detail is the Name of the campaign
            contact.Lead_Source_Most_Recent_Detail__c = relatedSourceCampaign.Name;
            //The label for this field is Campaign: Most Recent and it is the Name of the campaign (Keeping this behind the scenes because other functionality relies on it)
            contact.Campaign_Name__c = relatedSourceCampaign.Name;
            //Legacy field that we are going to continue populating so as not to break other functionality
            contact.Most_Recent_Campaign_Id__c = relatedSourceCampaign.Id;
            contact.Lead_Source_Most_Recent_TimeStamp__c = system.now();
            //Only populate the 'Original' fields if they are null 
            
            if( contact.Campaign_Source_Original__c == NULL )
            {
                //Campaign Source field is a lookup to the campaign object, pass in campaign id to this field
                contact.Campaign_Source_Original__c = relatedSourceCampaign.Id;
                //Lead Source field is the Type field from the campaign
                contact.LeadSource = relatedSourceCampaign.Type;
                //Lead Source Detail is the Name of the campaign
                contact.Lead_Source_Original_Detail__c = relatedSourceCampaign.Name;
                contact.Lead_Source_Original_Time_Date_Stamp__c = system.now();
                if(contact.First_Campaign__c == NULL)
                {
                    //The label for this field is Campaign: Original and it is the name of the campaign (Keeping this behind the scenes because other functionality relies on it)
                    contact.First_Campaign__c = relatedSourceCampaign.Name;
                    //Legacy field that we are going to continue populating so as not to break other functionality
                    contact.First_Campaign_Id__c = relatedSourceCampaign.Id;
                }
            }
        }
        return contact;
    }

    public static Lead buildLead(Lead aLead, Campaign relatedAssetCampaign, Campaign relatedSourceCampaign)
    {
        Lead lead = aLead;
        
        if(relatedAssetCampaign != NULL)
        {
            //Always populate the 'Most Recent' fields
            //Campaign Asset field is a lookup to the campaign object, pass in campaign id to this field
            lead.Campaign_Asset_Most_Recent__c = relatedAssetCampaign.Id;
            //Lead Asset field is the Type field from the campaign
            lead.Lead_Asset_Most_Recent__c = relatedAssetCampaign.Type;
            //Lead Asset Detail is the Name of the campaign
            lead.Lead_Asset_Most_Recent_Detail__c = relatedAssetCampaign.Name;
            //The label for this field is Campaign: Most Recent and it is the Name of the campaign (Keeping this behind the scenes because other functionality relies on it)
            lead.Campaign_Name__c = relatedAssetCampaign.Name;
            //Legacy field that we are going to continue populating so as not to break other functionality
            lead.Most_Recent_Campaign_ID__c = relatedAssetCampaign.Id;
            lead.Lead_Asset_Most_Recent_Time_Date_Stamp__c = system.now();
            //Only populate the 'Original' fields if they are null 
            if( lead.Campaign_Asset_Original__c == null)
            {
                //Campaign Asset field is a lookup to the campaign object, pass in campaign id to this field
                lead.Campaign_Asset_Original__c = relatedAssetCampaign.Id;
                //Lead Asset field is the Type field from the campaign
                lead.Lead_Asset_Original__c = relatedAssetCampaign.Type;
                //Lead Asset Detail is the Name of the campaign
                lead.Lead_Asset_Original_Detail__c = relatedAssetCampaign.Name;
                lead.Lead_Asset_Original_Time_Date_Stamp__c = system.now();
                if(lead.First_Campaign__c == NULL)
                {
                    //The label for this field is Campaign: Original and it is the name of the campaign (Keeping this behind the scenes because other functionality relies on it)
                    lead.First_Campaign__c = relatedAssetCampaign.Name;
                }
            }
        }
        if(relatedSourceCampaign != NULL)
        {
            //Always populate the 'Most Recent' fields
            //Campaign Source field is a lookup to the campaign object, pass in campaign id to this field
            lead.Campaign_Source_Most_Recent__c = relatedSourceCampaign.Id;
            //Lead Source field is the Type field from the campaign (Label is Lead Source: Most Recent)
            lead.Most_Recent_Lead_Source__c = relatedSourceCampaign.Type;
            //Lead Source Detail is the Name of the campaign
            lead.Lead_Source_Most_Recent_Detail__c = relatedSourceCampaign.Name;
            //The label for this field is Campaign: Most Recent and it is the Name of the campaign (Keeping this behind the scenes because other functionality relies on it)
            lead.Campaign_Name__c = relatedSourceCampaign.Name;
            //Legacy field that we are going to continue populating so as not to break other functionality
            lead.Most_Recent_Campaign_ID__c = relatedSourceCampaign.Id;
            //Lead Source field is the Type field from the campaign (Label is Lead Source: Most Recent)
            lead.Most_Recent_Lead_Source__c = relatedSourceCampaign.Type;
            lead.Lead_Source_Most_Recent_TimeSTamp__c = system.now();
            //Only populate the 'Original' fields if they are null 
                             
            if( lead.Campaign_Original__c == null)
            {
                //Campaign Source field is a lookup to the campaign object, pass in campaign id to this field (Label is Campaign Source: Original)
                lead.Campaign_Original__c = relatedSourceCampaign.Id;
                lead.Lead_Source_Original_Time_Date_Stamp__c = system.now();
                //Lead Source field is the Type field from the campaign (Label is Lead Source: Original)
                if(lead.LeadSource == NULL)
                {
                    lead.LeadSource = relatedSourceCampaign.Type;
                }
                //Lead Source Detail is the Name of the campaign
                lead.Lead_Source_Original_Detail__c = relatedSourceCampaign.Name;
                if(lead.First_Campaign__c == NULL)
                {
                    //The label for this field is Campaign: Most Recent and it is the Name of the campaign (Keeping this behind the scenes because other functionality relies on it)
                    lead.First_Campaign__c = relatedSourceCampaign.Name;
                }
            }
        }
        return lead;
    }

    public static void processMembers (List<CampaignMember> filteredMembers, List<CampaignMember> newMembers)
    {
        Map<Id, Lead> memberIdToLeadObject = memberIdToLead(filteredMembers);
        Map<Id, Contact> memberIdToContactObject = memberIdtoContact(filteredMembers);
        //Map<Id, Id> memberIdToOppId = memberIdToOpportunityId(filteredMembers);
        Map<Id, List<Id>> memberIdToListOfOppIds = memberIdToOppIds(filteredMembers);
        system.debug('Is there anything in the Opp map' + memberIdToListOfOppIds);
        Map<Id, Campaign> campaignTypeMap = memberIdToCampaignType(filteredMembers);
        List<CampaignMember> membersToUpdate = new List<CampaignMember>();
        List<Lead> leadsToUpdate = new List<Lead>();
        List<Lead> leadsToAssign = new List<Lead>();
        List<Contact> contactsToUpdate = new List<Contact>();
        Map<Id, Opportunity> oppsToUpdate = new Map<Id, Opportunity>();
        List<Id> opportunityIds = new List<Id>();
        Map<Id, CampaignMember> oppIdToMember = new Map<Id, CampaignMember>();
        
        Map<Id, Lead> updateSourceCampaignLeads = new Map<Id, Lead>();
        Map<Id, Lead> updateAssetCampaignLeads = new Map<Id, Lead>();
        Map<Id, Contact> updateSourceCampaignContacts = new Map<Id, Contact>();
        Map<Id, Contact> updateAssetCampaignContacts = new Map<Id, Contact>();
        /*Map<Id, Lead> updateLeadMap = new Map<Id, Lead>();
        Map<Id, Contact> updateContactMap = new Map<Id, Contact>();
        Map<Id, Lead> oldLeadMap = new Map<Id, Lead>();
        Map<Id, Contact> oldContactMap = new Map<Id, Contact>();*/

        for( CampaignMember member : filteredMembers )
        {
            Campaign relatedAssetCampaign = NULL;
            Campaign relatedSourceCampaign = NULL;
            
            //Assign the campaign from the member to campaign map to the campaign variable
            if(!campaignTypeMap.isEmpty() && campaignTypeMap.containsKey(member.Id))
            {
                if(campaignTypeMap.get(member.Id).Campaign_Sub_Type__c == ASSET)
                {
                    relatedAssetCampaign = campaignTypeMap.get(member.Id);
                }
                else if(campaignTypeMap.get(member.Id).Campaign_Sub_Type__c == SOURCE)
                {
                    relatedSourceCampaign = campaignTypeMap.get(member.Id);
                }
            }
            //Second if block determines if the campaign member is a Lead and performs update
            if( member.LeadId != NULL && member.ContactId == NULL && !memberIdToLeadObject.isEmpty() && memberIdToLeadObject.containsKey(member.Id))
            {
                oldLeadMap.put(memberIdToLeadObject.get(member.Id).Id, memberIdToLeadObject.get(member.Id));
                Lead lead;
                if(updateLeadMap.containsKey(member.LeadId))
                {
                     lead= buildLead(updateLeadMap.get(member.LeadId), relatedAssetCampaign, relatedSourceCampaign);
                     updateLeadMap.put(lead.Id, lead);
                }
                else
                {
                    lead = buildLead(memberIdToLeadObject.get(member.Id), relatedAssetCampaign, relatedSourceCampaign);
                    updateLeadMap.put(lead.Id, lead);
                } 
            }
            //Else block here is if the member is a Contact and perfoms update
            else if( member.ContactId != NULL && !memberIdToContactObject.isEmpty() && memberIdToContactObject.containsKey(member.Id))
            {
                oldContactMap.put(memberIdToContactObject.get(member.Id).Id, memberIdToContactObject.get(member.Id));
                Contact contact; 
                if(updateContactMap.containsKey(member.LeadId))
                {
                    contact = buildContact(updateContactMap.get(member.Id), relatedAssetCampaign, relatedSourceCampaign);
                    updateContactMap.put(contact.Id, contact);
                }
                else
                {
                    contact = buildContact(memberIdToContactObject.get(member.Id), relatedAssetCampaign, relatedSourceCampaign);
                    updateContactMap.put(contact.Id, contact);
                }
            }
            //Check for related opportunities and update       
            if(!memberIdToListOfOppIds.isEmpty() && memberIdToListOfOppIds.containsKey(member.Id))
            {
                
                for(Id oppIdToUpdate : memberIdToListOfOppIds.get(member.Id))
                {
                    Opportunity opp = new Opportunity (id = oppIdToUpdate);
                    opp.Most_Recent_Campaign__c = member.Campaign_Name__c;
                    oppsToUpdate.put(opp.Id,  opp );
                    oppIdToMember.put(opp.Id, member);
                }
                
            }
            //update the campaign member record from trigger.new
            CampaignMember placeholderMember = new CampaignMember (Id = member.Id);
            placeholderMember.Run_First_Most_Recent_Campaign_Sync__c = false;
            membersToUpdate.add( placeholderMember );
        }
        //Begin DML
        try
        {
            //system.debug('What is in leadsToUpdate'+leadsToUpdate);
            //Set<String> firsrelatedCampaigns = pluck.strings('First_Campaign__c', leadsToUpdate);
            //update leadsToUpdate;
            update updateLeadMap.values();
            LeadOwnerChange_QualifyingStatus.processLeads(updateLeadMap.values(), oldLeadMap);

            //update updateSourceCampaignLeads.values();
            //system.debug('What are the first Campaigns'+firsrelatedCampaigns+'& how many leads are there '+leadsToUpdate.size());
            //Map<Id, Lead> updateLeadMap = new Map<Id, Lead>();
            //leadsToUpdate = [ SELECT Id, OwnerId, Status FROM Lead WHERE Id IN :updateLeadMap.keyset()];
            for(Lead updatedLead : updateLeadMap.values())
            {   system.debug('What is the Owner Id'+updatedLead.OwnerId+' and What is the Lead Status? '+ updatedLead.Status);
                if(updatedLead.OwnerId == AccountOwnerChange.ELOQUA.Id && updatedLead.Status == 'Pending Assignment')
                {
                     Database.DMLOptions dmo = new Database.DMLOptions();
                     dmo.assignmentRuleHeader.useDefaultRule= true;
                     updatedLead.setOptions(dmo);
                     leadsToAssign.add( updatedLead );
                }
            }
        }
        catch (system.DmlException e)
        {
            Map<Id,List<CampaignMember>> leadIdToMember = groupBy.Ids('LeadId',filteredMembers);
            for(Integer i = 0; i < e.getNumDml(); i++)
            {
                Id errorId = updateLeadMap.values()[e.getDmlIndex(i)].Id;
                for(CampaignMember member : leadIdToMember.get(errorId))
                {
                    member.addError(e.getDmlMessage(i));
                    system.debug('What is the error message - leadsToUpdate'+ e.getDmlMessage(i));
                }   
            }
            
        }
        try
        {
            update leadsToAssign;
        }
        catch (system.DmlException e)
        {
            Map<Id,List<CampaignMember>> leadIdToMember = groupBy.Ids('LeadId',filteredMembers);
            for(Integer i = 0; i < e.getNumDml(); i++)
            {
                Id errorId = leadsToAssign[e.getDmlIndex(i)].Id;
                for(CampaignMember member : leadIdToMember.get(errorId))
                {
                    member.addError(e.getDmlMessage(i));
                    system.debug('What is the error message - leadsToAssign'+ e.getDmlMessage(i));
                }   
            }
        }
        try
        {
            //update contactsToUpdate;
            update updateContactMap.values();
            ContactOwnerChange_QualifyingStatus.processContacts(updateContactMap.values(), oldContactMap);
            //update updateAssetCampaignContacts.values();
            //List<Contact> updatedContacts = [SELECT Id FROM Contact WHERE Id IN : updateAssetCampaignContacts.keyset() OR Id IN : updateSourceCampaignContacts.keyset()];
            //Map<Id, Contact> updateContactMap = new Map<Id, Contact>();
        }
        catch (system.DmlException e)
        {
            Map<Id, List<CampaignMember>> contactIdToMember = groupBy.Ids('ContactId', filteredMembers);
            for(Integer i = 0; i < e.getNumDml(); i++)
            {
                Id errorId = updateContactMap.values()[e.getDmlIndex(i)].Id;
                for(CampaignMember member : contactIdToMember.get(errorId))
                {
                    member.addError(e.getDmlMessage(i));
                    system.debug('What is the error message - contacts to update' + e.getDmlMessage(i));
                }
            }
        }
        try
        {
            update oppsToUpdate.values();
        }
        catch (Exception e)
        {
            for(Integer i = 0; i < e.getNumDml(); i++)
            {
                Id errorId = oppsToUpdate.values()[e.getDmlIndex(i)].Id;           
                oppIdToMember.get(errorId).addError(e.getDmlMessage(i));
                system.debug('What is the error message - opps to update' + e.getDmlMessage(i));
            }
        }
        try
        {
            update membersToUpdate;
        }
        catch (system.DmlException e)
        {
            Map<Id, CampaignMember> members = new Map<Id,CampaignMember>(membersToUpdate);
            for(Integer i = 0; i < e.getNumDml(); i++)
            {
                Id errorId = membersToUpdate[e.getDmlIndex(i)].Id;
                members.get(errorId).addError(e.getDmlMessage(i));
                system.debug('What is the error message - members to update' + e.getDmlMessage(i));
            }
        }
    }

    public static void processTrigger(List<CampaignMember> newMembers, Map<Id, CampaignMember> oldMembers)
    {
        List<CampaignMember> filteredMembers = filterCampaignMembersOnInsert(newMembers, oldMembers);
        if(!filteredMembers.isEmpty())
        {
            processMembers(filteredMembers, newMembers);
        }
    }

}