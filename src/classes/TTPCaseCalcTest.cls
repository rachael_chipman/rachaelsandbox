@isTest
public class TTPCaseCalcTest {
    
    static Case testCase;
    static DateTime cd = DateTime.newInstance(2015, 3, 16, 9, 0, 0);
    static DateTime od = DateTime.newInstance(2015, 3, 16, 11, 0, 0);
    static DateTime frd = DateTime.newInstance(2015, 3, 17, 11, 0, 0);
    static DateTime cld = DateTime.newInstance(2015, 3, 18, 9, 0, 0);
    
    //setup method that builds the case
    static void setUpCase(){
        
        Account a = new Account(Name='Test Account');
    	insert a;
        
        Contact c = new Contact();
        c.FirstName = 'test';
        c.LastName = 'contact';
        c.AccountId = a.Id;
        insert c;
        
        testCase = new Case(
        ContactId = c.id,
        Subject = 'test',
		Origin = 'test origin',
        Status = 'New'
        );
        
        insert testCase;

    }
    
    //test TTP Calculation on Open & In Progress Cases
    static testMethod void updateStatusOpen() {
        
        test.startTest();
        setUpCase();
        testCase.Status = 'Open';
        testCase.Created_Date_Time__c = cd;
        testCase.Open_Date_Time__c = od;
        testCase.On_Hold_Date_Time__c = frd;
        update testCase;
        test.stopTest();
        
        List <Case> insertedCases = [SELECT id, TTP_New_to_Assigned__c, TTP_Assigned_to_First_Response__c
                                     FROM Case 
                                     WHERE Id =: testCase.Id];
        
        system.assertEquals(2, insertedCases[0].TTP_New_to_Assigned__c, 'We expect TTP New to Assigned to be 2');     
        system.assertEquals(12, insertedCases[0].TTP_Assigned_to_First_Response__c, 'We expect TTP Assigned to First Response to be 12');
        
    }
    
    //test TTP Calculation when case is Closed
    static testMethod void updateStatusClosed(){
        
        setUpCase();
        testCase.Status = 'Open';
        testCase.Created_Date_Time__c = cd;
        testCase.Open_Date_Time__c = od;
        update testCase;
        
        test.startTest();
        testCase.Status = 'Closed';
        testCase.Closed_Date_Time__c = cld;
        testCase.On_Hold_Date_Time__c = frd;
        update testCase;
        test.stopTest();
        
        List <Case> insertedCases = [SELECT id, TTP_First_Response_to_Closed__c, TTP_New_To_Closed__c, Closed_Date_Time__c, On_Hold_Date_Time__c
                                     FROM Case 
                                     WHERE Id =: testCase.Id];
        
        system.debug('what is on hold date time'+ insertedCases[0].On_Hold_Date_Time__c);
        system.debug('what is closed date time'+ insertedCases[0].Closed_Date_Time__c);
        
        system.assertEquals(10, insertedCases[0].TTP_First_Response_to_Closed__c, 'We expect TTP First Response to Closed to be 10');
        system.assertEquals(24, insertedCases[0].TTP_New_to_Closed__c, 'We expect TTP New to Closed to be 24');
        
    }
    
    //test when a Closed case is reopened that the TTP First Response to Closed & TTP New to Closed fields are updated to NULL
    static testMethod void updateStatusFromClosedToOpen(){
        
        setUpCase();
        testCase.Status = 'Closed';
        testCase.Created_Date_Time__c = cd;
        testCase.On_Hold_Date_Time__c = frd;
        testCase.Closed_Date_Time__c = cld;
        update testCase;
        
        test.startTest();
        testCase.Status = 'Open';
        update testCase;
        test.StopTest();
        
        List <Case> insertedCases = [SELECT id, TTP_First_Response_to_Closed__c, TTP_New_To_Closed__c
                                     FROM Case 
                                     WHERE Id =: testCase.Id];
        
        system.assertEquals(Null, insertedCases[0].TTP_First_Response_to_Closed__c, 'If a Case is reopened we expect TTP First Response to Closed to be Null');
        system.assertEquals(Null, insertedCases[0].TTP_New_to_Closed__c, 'If a case is reopened we expect TTP New to Closed to be Null');
        
        
    }
    
    

}