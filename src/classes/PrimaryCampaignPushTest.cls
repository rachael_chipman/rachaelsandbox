@isTest
private class PrimaryCampaignPushTest {
    static Opportunity testOpp;
    static Campaign testCampaign;
    static Account testAccount;
    static list <Opportunity> testOpps;
    
    static void SetupMethod()
    {
        testAccount = TestUtils.createAccount(TestUtils.MARKETER_RECORD_TYPE.id, true);
        testOpp = TestUtils.createOpportunity(TestUtils.RESEARCH_RECORD_TYPE.id, testaccount.Id, true);
        testCampaign = new Campaign(isactive = true, Name = 'Test Source');
        insert testCampaign;
        
        testOpps = TestUtils.createOpportunities(201, TestUtils.RESEARCH_RECORD_TYPE.id, testaccount.Id, true);
    }
    static testMethod void testSingleOppUpdate()
    {
        SetupMethod();
        testOpp.Opportunity_Source__c = 'Test Source';
        test.startTest();
        update testOpp;
        test.stopTest();
        
        Opportunity updatedOpp = [SELECT Id, Opportunity_Source__c FROM Opportunity WHERE Id =: testOpp.Id];
        system.assertEquals(testCampaign.Name, updatedOpp.Opportunity_Source__c, 'We expect the Campaign Name to be the same as the Opportunity Source');
                
    }
    static testMethod void testMultipleOppUpdate()
    {
        SetupMethod();
        list <Opportunity> updateTestOpps = new list <Opportunity>();  
        for(Opportunity testOpp1: testOpps)
        {
            testOpp1.Opportunity_Source__c = 'Test Source';
            updateTestOpps.add(testOpp1);
        }
        test.startTest();
        update updateTestOpps;
        test.stopTest();
        
        List <Opportunity> updateOpps = [SELECT Id, Opportunity_Source__c FROM Opportunity WHERE Id IN: updateTestOpps];
        for(Opportunity updatedOpp : updateTestOpps)
        {
            system.assertEquals(testCampaign.Name, updatedOpp.Opportunity_Source__c, 'We expect the Campaign Name to be the same as the Opportunity Source');
        }
        
    }

}