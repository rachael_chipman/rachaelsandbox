trigger SalesInvoice on c2g__codaInvoice__c ( after insert, after update ) 
{
    if( trigger.isAfter && trigger.isInsert )
    {
        //SalesInvoiceOppService.createInvoiceLineItems(Trigger.newMap);
    }
    
  if( trigger.isAfter && trigger.isUpdate )
    {
        List<c2g__codaInvoice__c> postedSalesInvoices = TransactionServices.filterPostedSObjects( trigger.new, trigger.oldMap, c2g__codaInvoice__c.c2g__InvoiceStatus__c, c2g__codaInvoice__c.c2g__Opportunity__c );
        if( !postedSalesInvoices.isEmpty() )
        {
            List<c2g__codaTransaction__c> relatedTransactions = TransactionServices.populateOpportunity( postedSalesInvoices, c2g__codaInvoice__c.c2g__Opportunity__c );
            if( !relatedTransactions.isEmpty() )
            {
                TransactionServices.updateTransactions( relatedTransactions, trigger.new );
            }
        }
    }

}