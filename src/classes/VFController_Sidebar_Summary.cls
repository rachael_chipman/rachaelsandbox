public class VFController_Sidebar_Summary {

public Integer getNewLeads() {
return [
select count() from Lead
where (CreatedDate = Last_N_DAYS:7
OR CreatedDate = TODAY)
AND Status != 'Converted'
LIMIT 100
];
} 


public Integer getNewCases() {
return [
select count() from Case
where (CreatedDate = Last_N_DAYS:30
OR CreatedDate = TODAY) 
LIMIT 100
];
}

public Integer getOpptyClosedLast30Days() {
return [
select count() from Opportunity
where IsClosed = TRUE
AND IsWon = TRUE
AND (CloseDate = Last_N_DAYS:30 OR CloseDate = TODAY)
LIMIT 100
];
}

public Integer getNewAcctsLast30Days() {
return [
select count() from Account
where (CreatedDate = Last_N_DAYS:30
OR CreatedDate = TODAY)
LIMIT 100
];
}

static testMethod void testVFController_Sidebar_Summary() {
Test.setCurrentPageReference(new PageReference('Page.SidebarSummary'));
VFController_Sidebar_Summary controller = new VFController_Sidebar_Summary();
Integer i1 = controller.getNewLeads();
Integer i2 = controller.getNewCases();
Integer i3 = controller.getOpptyClosedLast30Days();
Integer i4 = controller.getNewAcctsLast30Days();
}

}